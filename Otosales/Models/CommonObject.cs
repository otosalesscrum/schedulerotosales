﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otosales.Models
{
    #region General

    public class MasterGeneral
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }

    #endregion

    public class SurveyImage
    {
        public string SurveyNo { get; set; }
        public string FileName { get; set; }
        public string ImageType { get; set; }
        public string ImageCategory { get; set; }
        public string CoreImageID { get; set; }
    }

    #region Customer
    public class PersonalProspect1
    {
        public string ProspectID { get; set; }
        public string IDNumber { get; set; }
        public string IDType { get; set; }
        public string ProspectType { get; set; }
        public string ProspectName { get; set; }
        public string BirthDate { get; set; }
        public string Gender { get; set; }
        public string Religion { get; set; }
        public string HomeAddress { get; set; }
        public string HomePostalCode { get; set; }
        public string HomePostalCodeDesc { get; set; }
        public string HomeRT { get; set; }
        public string HomeRW { get; set; }
        public string HomeFax1 { get; set; }
        public string HomeFax2 { get; set; }
        public string Income { get; set; }
        public string MaritalStatus { get; set; }
        public string JobTitle { get; set; }
        public string CompanyName { get; set; }
        public string Email { get; set; }
        public string MobilePhone1 { get; set; }
        public string MobilePhone2 { get; set; }
        public string MobilePhone3 { get; set; }
        public string MobilePhone4 { get; set; }
        public string HomePhone1 { get; set; }
        public string HomePhone2 { get; set; }
        public string OfficePhone1 { get; set; }
        public string OfficeExt1 { get; set; }
        public string OfficeAddress { get; set; }
        public string OfficePostalCode { get; set; }
        public string OfficePostalCodeDesc { get; set; }
        //public List<Images> ImageList { get; set; }
        public string Facebook { get; set; }
        public string Instagram { get; set; }
        public string Twitter { get; set; }
    }

    public class CustomerPersonal1
    {
        public string CustomerID { get; set; }
        public string ProspectID { get; set; }
        public string IDNumber { get; set; }
        public string IDType { get; set; }
        public string CustomerType { get; set; }
        public string CustomerName { get; set; }
        public string BirthDate { get; set; }
        public string Gender { get; set; }
        public string Religion { get; set; }
        public string HomeAddress { get; set; }
        public string HomePostalCode { get; set; }
        public string HomePostalCodeDesc { get; set; }
        public string HomeRT { get; set; }
        public string HomeRW { get; set; }
        public string HomeFax1 { get; set; }
        public string HomeFax2 { get; set; }
        public string Income { get; set; }
        public string MaritalStatus { get; set; }
        public string JobTitle { get; set; }
        public string CompanyName { get; set; }
        public string Email { get; set; }
        public string MobilePhone1 { get; set; }
        public string MobilePhone2 { get; set; }
        public string MobilePhone3 { get; set; }
        public string MobilePhone4 { get; set; }
        public string HomePhone1 { get; set; }
        public string HomePhone2 { get; set; }
        public string OfficePhone1 { get; set; }
        public string OfficeExt1 { get; set; }
        public string OfficeAddress { get; set; }
        public string OfficePostalCode { get; set; }
        public string OfficePostalCodeDesc { get; set; }
        public string PreferredCustomerStatus { get; set; }
        //public List<Images> ImageList { get; set; }
        //public int IsVIP { get; set; }
        public string IsVIP { get; set; }
        public string Facebook { get; set; }
        public string Instagram { get; set; }
        public string Twitter { get; set; }
    }

    public class CorporateProspect1
    {
        public string ProspectID { get; set; }
        public string ProspectType { get; set; }
        public string ProspectName { get; set; }
        public string NameOnNPWP { get; set; }
        public string NPWPNo { get; set; }
        public string NPWPDate { get; set; }
        public string NPWPAddress { get; set; }
        public string SIUPNo { get; set; }
        public string PKPNo { get; set; }
        public string PKPDate { get; set; }
        public string BusinessType { get; set; }
        public string ClientType { get; set; }
        public string CompanyGroup { get; set; }
        public string AssetOwner { get; set; }
        public string OfficeAddress { get; set; }
        public string OfficePostalCode { get; set; }
        public string OfficePostalCodeDesc { get; set; }
        public string OfficePhone1 { get; set; }
        public string OfficePhone2 { get; set; }
        public string OfficePhoneExt { get; set; }
        public string OfficeRT { get; set; }
        public string OfficeRW { get; set; }
        public string OfficeFax1 { get; set; }
        public string OfficeFax2 { get; set; }
        public string PIC { get; set; }
        public string Email { get; set; }
        public string MobilePhone1 { get; set; }
        public string MobilePhone2 { get; set; }
        public string HomeAddress { get; set; }
        public string HomePostalCode { get; set; }
        public string HomePostalCodeDesc { get; set; }
        public string HomePhone1 { get; set; }
        public string HomePhone2 { get; set; }
        //public List<Images> ImageList { get; set; }
    }

    public class CustomerCoorporate1
    {
        public string CustomerID { get; set; }
        public string ProspectID { get; set; }
        public string CustomerType { get; set; }
        public string CustomerName { get; set; }
        public string NameOnNPWP { get; set; }
        public string NPWPNo { get; set; }
        public string NPWPDate { get; set; }
        public string NPWPAddress { get; set; }
        public string SIUPNo { get; set; }
        public string PKPNo { get; set; }
        public string PKPDate { get; set; }
        public string BusinessType { get; set; }
        public string ClientType { get; set; }
        public string CompanyGroup { get; set; }
        public string AssetOwner { get; set; }
        public string OfficeAddress { get; set; }
        public string OfficePostalCode { get; set; }
        public string OfficePostalCodeDesc { get; set; }
        public string OfficePhone1 { get; set; }
        public string OfficePhone2 { get; set; }
        public string OfficePhoneExt { get; set; }
        public string OfficeRT { get; set; }
        public string OfficeRW { get; set; }
        public string OfficeFax1 { get; set; }
        public string OfficeFax2 { get; set; }
        public string PIC { get; set; }
        public string Email { get; set; }
        public string MobilePhone1 { get; set; }
        public string MobilePhone2 { get; set; }
        public string HomeAddress { get; set; }
        public string HomePostalCode { get; set; }
        public string HomePostalCodeDesc { get; set; }
        public string HomePhone1 { get; set; }
        public string HomePhone2 { get; set; }
        //public List<Images> ImageList { get; set; }
        //public int IsVIP { get; set; }
        public string IsVIP { get; set; }
    }

    public class AdditionalInfo1
    {
        public string AdditionalCode { get; set; }
        public string AdditionalInfoValue { get; set; }
    }
    #endregion

    #region Payment
    public class OrderPaymentCustom
    {
        public decimal TotalBayar { get; set; }
        public DateTime MaxTransactionDate { get; set; }
        public decimal TotalBill { get; set; }
    }
    #endregion
}
