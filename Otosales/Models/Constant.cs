﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Otosales.Models
{
    public class Constant
    {
        public class AutoApproveOrderResultStatus
        {
            public const int NeedNSAApproval = 0;
            public const int PolicyApproved = 1;
            public const int WaitingApprovalLimit = 2;
            public const int WaitingSOApproval = 3; // untuk order dari otosales harusnya udah ga ada lagi yang ini karena dipindahin ke scheduler Otosales.Approval

        }
        
        public class MstOrderMobileApprovalStatus
        {
            public const string Approve = "1";
            public const string Reject = "2";
        }
        public enum MstOrderMobileApprovalState
        {
            INPROGRESS,
            APPROVED,
            REJECTED
        }

        public class ApprovalAdjustmentReason
        {
            public const string PolicyTerbitSebelumBayar = "PSTB";
            public const string PolicyDeliveryAddress = "DELIVR";
            public const string PerubahanDataSalesman = "PDSLS";
        }

        public class ApprovalType
        {
            public const string CommissionSameDay = "COMSAMD"; 
            public const string Adjustment = "ADJUST";
            public const string NextLimit = "NEXTLMT";
            public const string Commission = "KOMISI";
        }

        public class ApprovalStatus
        {
            public const int WAITING = 0;
            public const int APPROVE = 1;
            public const int REJECT_OR_REVISE = 2;
            
        }
        /// <summary>
        /// Based on AABMobile.dbo.FollowUpStatusInfo
        /// </summary>
        public class FollowUpInfo
        {
            public const string PolicyApproved = "50";
            public const string PolicyDelivered = "51";
            public const string PolicyReceived = "52";
            public const string PolicyApprovedNeedDocRep = "53";
            public const string PolicyDeliveredNeedDocRep = "54";
            public const string PolicyReceivedNeedDocRep = "55";
            public const string DocumentCompletion = "56";
            public const string FUPayment = "57";
            public const string SendToSurveyor = "58"; 
            public const string FollowUpSurveyResult = "59";
            public const string NeedDataRevision = "60";
            public const string SendToSA = "61"; 
            public const string NeedApproval = "62";
            public const string PolicyWO = "63";
            public const string Recreate = "64";
            public const string PolicyCancel = "92";
            public const string PolicyEndorse = "93";
        }
        public class FollowUpStatus
        {
            public const string NeedFUStatusCode = "1"; 
            public const string DealStatusCode = "13"; 
            public const string OrderRejectedStatusCode = "14";
            public const string Potensial = "2";
            public const string NotDeal = "5";
        }

        public class SAState
        {
            //SAState = null -> Send To Surveyor
            public const string SendToSurveyor = null;
            public const string BackToAO = "0";
            public const string SendToSA = "1";
            public const string SubmitBySA = "2";
            public const string RenewalNoSA = "3";
        }

        

        public class WorkState
        {
            public const string CUSTOMERCREATED = "CustomerCreated";
            public const string POLICYCREATED = "PolicyCreated";
            public const string OBJECTCREATED = "ObjectCreated";
        }

        public class ErrorMessages
        {
            public const string MVAlreadyInActivePolicy = "One of ordered object is active in other policy";
            public const string ProgrammingError = "Error :";
            public const string GetTransactionNoError = "Failed To Get Transaction No";
            public const string SaveTransactionError = "Failed To Save Transaction";
            public const string GetPolicyNoError = "Failed To Get Policy No";
            public const string GetPolicyIDError = "Failed To Get Policy ID";
            public const string InsertMstOrderError = "Failed To Insert Mst Order";
            public const string InsertMstOrderGODigitalError = "Failed To Insert Mst Order GODigital";
            public const string InsertObjectDetailError = "Failed To Insert Detail Object";
            public const string InsertOrderAddressError = "Failed To Insert Order Detail Address";
            public const string InsertOrderInstallmentError = "Failed To Insert Installment Info";
            public const string InsertOrderDocumentError = "Failed To Insert Document";
            public const string InsertProspectError = "Failed To Insert Customer Prospect";
            public const string InvalidPayment = "InvalidPayment";
            public const string InsertSurveyError = "Failed To Insert Survey Data";
            public const string InsertTransactionAccountError = "Failed To Insert Transaction Account";
            public const string InsertObjectClauseError = "Failed To Insert Object Clause ";
            public const string InsertPolicyClauseError = "Failed To Insert Policy Clause ";
        }

        public class ReferenceType
        {
            public const string Customer = "CUSTMR";
            public const string Prospect = "PRSPCT";
        }

        public class InterestType
        {
            public const string Primary = "P";
            public const string Accessories = "A";
            public const string Liability = "L";
            public const string Service = "S";
        }

        public class CustomerDataType
        {
            public const string PROSPECT = "PROSPECT";
            public const string CUSTOMER = "CUSTOMER";
        }

        public class DateFormat {
            public const string ddmmyyy = "dd/MM/yyyy";
            public const string mddyyyy = "M/dd/yyyy hh:mm:ss tt";
        }
        public class InsFactor
        {
            public const string GeoArea = "GEOGRA";
            public const string MVUsage = "MVUSAG";
            public const string NewVehicle = "NEWVHC";
            public const string VehicleBrand = "VBRAND";
            public const string VehicleType = "VHCTYP";
            public const string VehicleModel = "VMODEL";            
        }
        public class MVNewStatus
        {
            public const string New = "000001";
            public const string Old = "000000";
        }

        public class NoCoverType
        {
            public const string Part = "Part";
            public const string Accessories = "Accessories";
        }

        public static class Constants
        {
            public const string SURVEYTYPE_ORDER = "SVPLCY";
            public const string SURVEYOR_ROLE = "SVOFFC";
            public const string BRANCH_HEAD_ROLE = "BRHEAD";
            public const string AREA_HEAD_ROLE = "ARHEAD";
            public const string SENIOR_UNDERWRITER_ROLE = "SNUDWR";
            public const string POLICY_SURVEYOR_DONE = "98";
            public const string POLICY_SURVEYOR_OVER_LIMIT = "97";
            public const string POLICY_BRANCH_HEAD_OVER_LIMIT = "96";
            public const string NEED_FOLLOW_UP = "95";
            public const string POLICY_APPROVED = "9";
            public const string COVER_NOTE_PRINTED = "10";
            public const string POLICY_UNDERWRITING = "3";
            public const string POLICY_REJECTED = "6";
            public const string POLICY_APPROVAL_TYPE = "SVPAPP";
            public const string NEED_APPROVAL_SO = "50";
        }
        public class SurveyStatus
        {
            public const string Complete = "7";
            public const string Reject = "6";
        }
        public class DeliveryStatus
        {
            public const string Delivered = "OTW";
            public const string Recived = "REC";
        }

    }
}