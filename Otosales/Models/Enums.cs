﻿
using System;

namespace Otosales.Models
{
    public enum PushNotificationCategory
    {
        NEEDAPPROVALCOMMISSIONSAMEDAY,
        NEEDAPPROVALADJUSTMENT,
        NEEDAPPROVALNEXTLIMIT,
        FOLLOWUPSTATUSCHANGEDBYOTHERS
    }

    public enum CIFType
    {
        Personal = 1,
        Corporate = 2,
        FinancialInstitution = 3,
        Payer = 4,
        Salesman = 5,
        Reinsrurer=6,
        SalesmanDealer=7,
        TitanAgent=8,
        PolicyHolder=9,
        Dealer=10
    }

    public enum ResultForPolicy
    {
        tNone,
        tObject,
        tObjectNew,
        tObjectMarineCargo,
        tObjectMarineHull,
        tObjectLiability,
        tObjectMisc,
        tObjectPA,
        tObjectHE,
        tObjectEngineering,
        tObjectMoney,
        tObjectLocation,
        tSubContractorData,
        tObjectFire,
        tInterest,
        tInterestItem,
        tCoverage,
        tDiscount,
        tCommision,
        tAddress,
        tPremium,
        tInsured,
        tPayment,
        tDetailMCTrip,
        tPolicyClause,
        tObjectClause,
        tDataIndirectInsurance,
        tDataIndirectInsuranceMember,
        tRenewObjectMV,
        tRenewDetailAddress,
        tRenewInterest,
        tRenewCoverage,
        tRenewDiscount,
        tRenewPolicy,
        tObjectDescription,
        tDeletedPolicyClause,
        tDeletedObjectClause
    }

    public class ErrorMessage { 
        public const string ProductDisabled = "Sorry, this product/MOU has been disabled.";
        public const string ProductNotActive = "Sorry, this product/MOU is not active yet.";
        public const string ProductExpired = "Sorry, this product/MOU has expired.";
    }

    public class AcceptanceType {
        public const string DIRECT = "DIRECT";
        public const string CoinsuranceOutward = "DCOOUT";
        public const string CoinsuranceInward = "COINSR";
    }

    public class Enum_Ndays
    {
        public const string nActualYear = "366";
        public const string n360days = "360";
    }

    public class PremiumMethodEnum
    {
        public const int pmProRate = 0;
        public const int pmScale = 1;
    }

    public class CalculationMethodEnum
    {
        public const int Prorate = 1;
        public const int Scale = 2;
        public const int Fixed = -1;
    }
    public class AddressType
    {
        public const string PolicyAddress = "POLADR";
        public const string DeliverPolicyAddress = "DELIVR";
        public const string BillingPolicyAddress = "BILADR";
    }

    public class CreatePolicyImageType
    {
        public const int PGCreatePersImage = 0;
        public const int PGCreateCorpImage = 1;
        public const int PGCreatePolicyImage = 2;
        public const int PGObjectImage = 3;
        public const int PGAddEditObjectImage = 4;
        public const int PGOriginalDefectImage = 5;
        public const int PGAccessoriesImage = 6;
        public const int PGNoCoverImage = 6;
    }

    public class ReferenceTypeImage
    {
        public const string KTPSURVEY = "KTP/KITAS";
        public const string KTP = "KTP";
        public const string SIM = "SIM";
        public const string KITAS = "KITAS";
        public const string NPWP = "NPWP";
        public const string SIUP = "IDC";
        public const string SPPAKB = "SPPAKB";
        public const string FAKTUR = "FAKTUR";
        public const string STNK = "STNK";
        public const string BSTB = "BSTB";
        public const string CHECKLISTSURVEY = "CLSV";
        public const string ADJUSTMENT = "ADJS";
        public const string NSA = "NSA"; 
        public const string DOCREP = "DOCREP"; 
        public const string CONFIRMATIONCUSTOMER = "CSCF";
        public const string DOCPENDUKUNG = "DOCPKG";
        public const string BUKTIBAYAR = "BKTBYR";
        public const string RENEWALNOTICE = "RENNOT";
    }
    public class InterestCode
    {
        public const string CASCO = "CASCO";
        public const string ACCESSORIES = "ACCESS";
        public const string PADRIVER = "PADRVR";
        public const string PAPASSANGER = "PAPASS";
        public const string PLL = "PLL";
        public const string TPLPER = "TPLPER";


    }

    public class OrderType
    {
        public const string New = "1";
        public const string Renew = "2";
        public const string Endorse = "3";
        public const string Cancel = "4";
    }

    public class OrderStatus
    {
        public const string New = "01";
        public const string WaitingNSA = "02";
        public const string WaitingSurvey = "03";
        public const string WaitingApproval = "04";
        public const string Approved = "05";
        public const string Rejected = "06";
        public const string Printed = "07";
        public const string ReadyDeliver = "08";
        public const string DeliveredCourier = "09";
        public const string Received = "10";
        public const string Returned = "11";
    }

    public class OrderStatusGroup
    {
        public const string Approval = "APPROVAL";
        public const string Delivery = "DELIVERY";
        public const string General = "GENERAL";
        public const string Printing = "PRINTING";
    }

    public class MessageType
    {
        public const string Email = "MAIL";
        public const string SMS = "SMS";
    }

    public class DateFormatCustom
    {
        public const string Format1 = "MM/dd/yyyy";
        public const string Format2 = "dd/MM/yyyy";
        public const string Format3 = "yyyy-MM-dd";
        public const string Format4 = "MM/dd/yyyy hh:mm";

    }

    public class AutoApplyFlag {
        public const string NonAutoApply = "0";
        public const string TLOBundling = "2";
        public const string CompreBundling = "1";
        public const string TLOExtendedGroup = "4";
        public const string CompreExtendedGroup = "3";
    }

    public class ApplicationParameters
    {
        public int ID { get; set; }
        public string ParamName { get; set; }
        public string ParamValue { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
    }
    public class OrdDtlAddress
    {
        public string OrderNo { get; set; }  // char(15)
        public string AddressType { get; set; }  // char(6)
        public string SourceType { get; set; }  // char(2)
        public int RenewalNotice = 0;  // bit
        public string DeliveryCode { get; set; }  // char(6)
        public string Address { get; set; }  //{get;set;} // varchar(152)
        public string RT { get; set; }  // char(3)
        public string RW { get; set; }  // char(3)
        public string PostalCode { get; set; }  // char(8)
        public string Phone { get; set; }  //{get;set;} // varchar(16)
        public string Fax { get; set; }  //{get;set;} // varchar(16)
        public string EntryUsr { get; set; }  // char(5)          
        public string Handphone { get; set; }  //{get;set;} // varchar(20)
        public string ContactPerson { get; set; }  //{get;set;} // varchar(30)
    }
    public class PaymentCategory
    {
        public const string DirectDebet = "DD";
        public const string CreditCard = "CC";
        public const string VirtualAccount = "VA";
    }

}