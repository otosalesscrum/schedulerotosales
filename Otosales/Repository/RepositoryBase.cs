﻿using a2is.Framework.DataAccess;
using Otosales.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otosales.Repository
{
    public class RepositoryBase : CommonLogic
    {
        private const string AAB_CONNECTIONSTRING = "AAB";
        private const string AABMobile_CONNECTIONSTRING = "AABMobile";
        private const string GardaMobileCMS_CONNECTIONSTRING = "GardaMobileCMS";
        private const string a2isImageDB_CONNECTIONSTRING = "a2isImageDB";
        private const string a2isBeyondDB_CONNECTIONSTRING = "a2isBeyondDB";
        private const string a2isMobileSurveyDB_CONNECTIONSTRING = "a2isMobileSurveyDB";
        private const string a2isTitanDB_CONNECTIONSTRING = "a2isTitanDB";
        private const string a2isSurveyManagementDB_CONNECTIONSTRING = "a2isSurveyManagementDB";
        private const string AsuransiAstra_CONNECTIONSTRING = "AsuransiAstra";


        public static a2isDBHelper.Database GetAABDB()
        {
            return new a2isDBHelper.Database(AAB_CONNECTIONSTRING);
        }

        public static a2isDBHelper.Database GetAABMobileDB()
        {
            return new a2isDBHelper.Database(AABMobile_CONNECTIONSTRING);
        }

        public static a2isDBHelper.Database Geta2isImageDB()
        {
            return new a2isDBHelper.Database(a2isImageDB_CONNECTIONSTRING);
        }

        public static a2isDBHelper.Database Geta2isBeyondDB()
        {
            return new a2isDBHelper.Database(a2isBeyondDB_CONNECTIONSTRING);
        }

        public static a2isDBHelper.Database Geta2isMobileSurveyDB()
        {
            return new a2isDBHelper.Database(a2isMobileSurveyDB_CONNECTIONSTRING);
        }

        public static a2isDBHelper.Database Geta2isTitanDB()
        {
            return new a2isDBHelper.Database(a2isTitanDB_CONNECTIONSTRING);
        }

        public static a2isDBHelper.Database Geta2isSurveyManagementDB()
        {
            return new a2isDBHelper.Database(a2isSurveyManagementDB_CONNECTIONSTRING);
        }

        public static a2isDBHelper.Database GetGardaMobileCMS()
        {
            return new a2isDBHelper.Database(GardaMobileCMS_CONNECTIONSTRING);
        }
        public static a2isDBHelper.Database GetAsuransiAstraDB()
        {
            return new a2isDBHelper.Database(AsuransiAstra_CONNECTIONSTRING);
        }
    }
}
