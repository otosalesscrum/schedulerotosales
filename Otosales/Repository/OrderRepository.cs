﻿using Otosales.Repository;
using System.Collections.Generic;
using Otosales.Models;
using System;
using System.Linq;
using Otosales.Logic;
using System.Globalization;
using static Otosales.Models.Constant;
using a2is.Framework.Monitoring;

namespace Otosales.Repository
{
    public class OrdRepository : RepositoryBase
    {
        private static a2isLogHelper logger = new a2isLogHelper();
        public bool UpdateOrderData(string OrderNo)
        {
            string CurrentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            var MobleDB = RepositoryBase.GetAABMobileDB();
            var AABDB = RepositoryBase.GetAABDB();
            bool res = false;
            string qGetOrderMobile = @"SELECT PolicyOrderNo, ProspectID, f.FollowUpNo, CAST(COALESCE(IsRenewal,0) AS BIT) IsRenewal,
									os.SalesDealer, os.DealerCode, os.SalesOfficerID, CustIDAAB
                                    FROM dbo.OrderSimulation os
                                    INNER JOIN dbo.ProspectCustomer ps
                                    ON ps.CustID = os.CustID
                                    INNER JOIN dbo.FollowUp f
                                    ON f.FollowUpNo = os.FollowUpNo AND f.CustID = ps.CustID
                                    WHERE OrderNo = @0 AND ApplyF = 1";
            try
            {
                List<dynamic> ordMobileData = MobleDB.Fetch<dynamic>(qGetOrderMobile, OrderNo);
                if (ordMobileData.Count > 0)
                {
                    List<OrdDtlAddress> address = GetDtlAddress(OrderNo, ordMobileData.First().PolicyOrderNo);
                    string qGetStatus = @"SELECT b.FollowUpNo, os.PolicyOrderNo, os.OrderNo, mom1.SA_State, b.FollowUpInfo, 
                                        CAST(COALESCE(IsRenewal,0) AS BIT) IsRenewal
                                        FROM (
                                        select Order_No, max(Order_ID) [Order_ID]
                                        from beyondreport.aab.dbo.mst_order_mobile mom WITH (NOLOCK) 
                                        group by Order_No
                                        ) a 
                                        INNER JOIN beyondreport.aab.dbo.mst_order_mobile mom1 WITH (NOLOCK) ON a.Order_ID = mom1.Order_ID 
                                        INNER JOIN OrderSimulation os ON os.PolicyOrderNo = a.Order_No and os.ApplyF = 1
                                        INNER JOIN FollowUp b ON os.FollowUpNo = b.FollowUpNo and os.OrderNo = @0 and b.RowStatus = 1";
                    List<dynamic> StatusOrd = MobleDB.Fetch<dynamic>(qGetStatus, OrderNo);
                    if (StatusOrd.Count > 0)
                    {
                        if ((StatusOrd.First().SA_State == 1)
                            || (StatusOrd.First().IsRenewal && StatusOrd.First().SA_State == 3))
                        {
                            if (address.Count > 0)
                            {
                                string qDelete = @"DELETE FROM dbo.Ord_Dtl_Address WHERE Order_No = @0";
                                AABDB.Execute(qDelete, StatusOrd.First().PolicyOrderNo);
                                AddDetailAddress(address, CommonLogic.GetSettingUserID());
                            }
                            string qUpdateBrokerCode = @"UPDATE dbo.Mst_Order SET Broker_Code = @1, UpdateUsr = @2, UpdateDt = GETDATE() WHERE Order_No = @0";
                            dynamic SalesDealer = MobleDB.ExecuteScalar<dynamic>("SELECT COALESCE(CustID,'') CustID FROM dbo.SalesmanDealer WHERE SalesmanCode = @0", ordMobileData.First().SalesDealer);
                            dynamic DealerCode = MobleDB.ExecuteScalar<dynamic>("SELECT COALESCE(CustID,'') CustID FROM dbo.Dealer WHERE DealerCode = @0", ordMobileData.First().DealerCode);
                            string BrokerCode = (Convert.ToString(ordMobileData.First().SalesOfficerID)).Length == 3 ?
                            SalesDealer != null && Convert.ToString(SalesDealer) != "" ?
                            Convert.ToString(SalesDealer) : (DealerCode != null &&
                            Convert.ToString(DealerCode) != "") ?
                            DealerCode : null : Convert.ToString(ordMobileData.First().SalesOfficerID);
                            string updateUsr = Convert.ToString(ordMobileData.First().SalesOfficerID);
                            AABDB.Execute(qUpdateBrokerCode, ordMobileData.First().PolicyOrderNo, BrokerCode, updateUsr.Length > 3 ? "OTOSL" : updateUsr);
                            //0219URF2019
                            UpdateOtosalesCustomerToProspect(ordMobileData.First().FollowUpNo, updateUsr.Length > 3 ? "OTOSL" : updateUsr);
                        }
                    }
                    string custID = "";
                    if (!string.IsNullOrEmpty(ordMobileData.First().CustIDAAB) && !string.IsNullOrWhiteSpace(ordMobileData.First().CustIDAAB))
                    {
                        custID = ordMobileData.First().CustIDAAB;
                    }
                    else {
                        custID = ordMobileData.First().ProspectID;
                    }
                    ImageLogic.CopyImageDataToMstImage(ordMobileData.First().FollowUpNo, "", ordMobileData.First().PolicyOrderNo);
                    ImageLogic.CopyImageDataToMstImage(ordMobileData.First().FollowUpNo, custID, "");
                    res = true;
                }

                return res;
            }
            catch (Exception e)
            {
                logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.StackTrace));
                throw e;
            }
        }

        private static List<OrdDtlAddress> GetDtlAddress(string orderNo, string PolicyOrderNo)
        {
            List<OrdDtlAddress> result = new List<OrdDtlAddress>();
            var db = RepositoryBase.GetAABMobileDB();
            try
            {
                string qDtlAddress = @";SELECT DISTINCT x.Data AddressType, 
case x.Data 
	when 'DELIVR' THEN PolicyDeliveryPostalCode
	ELSE IIF(COALESCE(pc.IsCompany,0) = 0, pc.PostalCode, pcm.PostalCode)
end [PostalCode],
case x.Data 
	when 'DELIVR' THEN 
		CASE a.PolicySentTo WHEN 'GC' THEN gc.Address
		ELSE PolicyDeliveryAddress END
	ELSE IIF(COALESCE(pc.IsCompany,0) = 0, pc.custAddress, pcm.NPWPaddress)
end [Address],  
case x.Data 
	when 'DELIVR' THEN 
		case a.PolicySentTo when 'CS' then IIF(COALESCE(pc.IsCompany,0) = 0, PolicyDeliveryName, CompanyName) 
		when 'CH' then IIF(COALESCE(pc.IsCompany,0) = 0, PolicyDeliveryName, CompanyName) 
		else COALESCE(PolicyDeliveryName,'') end 
	ELSE IIF(COALESCE(pc.IsCompany,0) = 0, PolicyDeliveryName, CompanyName)
end [ContactPerson], 
case x.Data 
	when 'DELIVR' THEN 
		case a.PolicySentTo when 'CS' then IIF(COALESCE(pc.IsCompany,0) = 0, pc.Phone1, pcm.PICPhoneno) 
		when 'CH' then IIF(COALESCE(pc.IsCompany,0) = 0, pc.Phone1, pcm.PICPhoneno) 
		when 'BR' then ''
		else IIF(COALESCE(pc.IsCompany,0) = 0, pc.Phone1, pcm.PICPhoneno) end 
	ELSE IIF(COALESCE(pc.IsCompany,0) = 0, pc.Phone1, pcm.PICPhoneno)
end [Handphone],
case x.Data 
	when 'DELIVR' THEN 
		case a.PolicySentTo when 'CS' then IIF(COALESCE(pc.IsCompany,0) = 0, 'CH', 'CO') 
		when 'CH' then IIF(COALESCE(pc.IsCompany,0) = 0, 'CH', 'CO') 
		when 'BR' then '' 
		WHEN 'GC' THEN gc.GCCode
		else IIF(COALESCE(pc.IsCompany,0) = 0, 'CH', 'CO') end 
	ELSE IIF(COALESCE(pc.IsCompany,0) = 0, 'CH', 'CO')
end [SourceType],
case x.Data 
	when 'DELIVR' THEN 
		case a.PolicySentTo when 'CS' then 'CS' 
		when 'CH' then 'CH'
		when 'BR' then 'BR' 
		WHEN 'GC' THEN 'GC'
		else 'OH' end 
end [DeliveryCode]
FROM 
[dbo].[Fn_AABSplitString] ('POLADR,BILADR,DELIVR',',') x 
inner join dbo.OrderSimulation a on 1=1
inner join FollowUp b on a.FollowUpNo = b.FollowUpNo 
inner join ProspectCustomer pc on pc.CustID = b.CustID
left join mst_policysentto pst on a.PolicySentTo = pst.PolicySentToDes and pst.rowstatus = 1
left join ProspectCompany pcm on pcm.CustId = a.CustID
LEFT JOIN BEYONDREPORT.AAB.dbo.GardaCenter gc ON gc.Name = a.PolicyDeliveryAddress
where orderno = @0";

                List<dynamic> DtlAddress = db.Fetch<dynamic>(qDtlAddress, orderNo);
                foreach (var addr in DtlAddress)
                {
                    OrdDtlAddress Addr = new OrdDtlAddress
                    {
                        OrderNo = PolicyOrderNo,
                        Address = addr.Address,
                        PostalCode = addr.PostalCode,
                        ContactPerson = addr.ContactPerson,
                        EntryUsr = CommonLogic.GetSettingUserID(),
                        //Phone = IsNA(WTAddr.Phone),
                        Handphone = addr.Handphone,
                        AddressType = addr.AddressType, // POLADR, BILADR : info dari JSW ini disamain sama POLADR, DELIVR
                        SourceType = addr.SourceType, //(WTAddr.SourceType != null) ? WTAddr.SourceType.Trim().ToUpper() == "OH" ? "" : WTAddr.SourceType : "",// kalo customerhome : CH , kalo office : CO
                        RT = "", //IsNA(WTAddr.RT),
                        RW = "", //IsNA(WTAddr.RW),
                        DeliveryCode = addr.DeliveryCode, // null 
                        Fax = "",//IsNA(WTAddr.Fax),
                        //RenewalNotice = IsNA(WTAddr.RenewalNotice) // null
                    };
                    result.Add(Addr);
                }
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public bool AddDetailAddress(List<OrdDtlAddress> Addr, string EntryUser)
        {
            string query = @"INSERT  INTO dbo.Ord_Dtl_Address
                                ( Order_No ,
                                  Address_Type ,
                                  Source_Type ,
                                  Renewal_Notice ,
                                  Delivery_Code ,
                                  Address ,
                                  RT ,
                                  RW ,
                                  Postal_Code ,
                                  Phone ,
                                  Fax ,
                                  EntryUsr ,
                                   EntryDt ,
                                  Handphone ,
                                  ContactPerson	,
                                  UpdateUsr                                         			
                                )
                        VALUES  ( @0 , -- Order_No - char(15)
                                  @1 , -- Address_Type - char(6)
                                  @2 , -- Source_Type - char(2)
                                  @3 , -- Renewal_Notice - bit
                                  @4 , -- Delivery_Code - char(6)
                                  @5 , -- Address - varchar(152)
                                  @6 , -- RT - char(3)
                                  @7 , -- RW - char(3)
                                  @8 , -- Postal_Code - char(8)
                                  @9 , -- Phone - varchar(16)
                                  @10 , -- Fax - varchar(16)
                                  @11 , -- EntryUsr - char(5)
                                  GETDATE() ,  -- EntryDt - datetime          
                                  @12 , -- Handphone - varchar(20)
                                  @13,  -- ContactPerson - varchar(30)				
                                 ''
                                )";
            int result = 0;
            using (var db = GetAABDB())
            {
                for (int i = 0; i < Addr.Count; i++)
                {
                    int maxLength = 30;
                    string ContactPerson = IsNA(Addr[i].ContactPerson).Length <= maxLength ? IsNA(Addr[i].ContactPerson) : IsNA(Addr[i].ContactPerson).Substring(0, maxLength);
                    result += db.Execute(query, IsNA(Addr[i].OrderNo), IsNA(Addr[i].AddressType), IsNA(Addr[i].SourceType), IsNA(Addr[i].RenewalNotice), IsNA(Addr[i].DeliveryCode), IsNA(Addr[i].Address), IsNA(Addr[i].RT), IsNA(Addr[i].RW), IsNA(Addr[i].PostalCode), IsNA(Addr[i].Phone), IsNA(Addr[i].Fax), EntryUser, IsNA(Addr[i].Handphone), ContactPerson);
                }

            }
            return result >= Addr.Count;
        }

        public int UpdateOrderMobile(string OrderNo)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            string query = "";
            var AABMobileDB = GetAABMobileDB();
            var AABDB = GetAABDB();
            int res = 0;
            try
            {
                query = @"SELECT PolicyOrderNo FROM dbo.OrderSimulation WHERE OrderNo = @0";
                string PolicyOrderNo = AABMobileDB.ExecuteScalar<dynamic>(query, OrderNo);
                if (!string.IsNullOrEmpty(PolicyOrderNo))
                {
                    query = @"SELECT Object_No AS ObjectNo,Interest_No AS InterestNo,Interest_Code AS InterestID,
DATEDIFF(YEAR,Period_From,Period_To) AS [YEAR], Period_From AS PeriodFrom,
Period_To AS PeriodTo, Deductible_Code 
FROM dbo.Ord_Dtl_Interest WHERE Order_no = @0";
                    List<dynamic> newOSI = AABDB.Fetch<dynamic>(query, PolicyOrderNo);
                    if (newOSI.Count > 0)
                    {
                        query = "DELETE FROM dbo.OrderSimulationCoverage WHERE OrderNo = @0";
                        AABMobileDB.Execute(query, OrderNo);

                        query = "DELETE FROM dbo.OrderSimulationInterest WHERE OrderNo = @0";
                        AABMobileDB.Execute(query, OrderNo);

                        #region ordersimulationInterest
                        foreach (dynamic d in newOSI)
                        {
                            query = @"SELECT odc.Object_No AS ObjectNo, odc.Interest_No AS InterestNo, Coverage_No AS CoverageNo, 
                            Coverage_Id AS CoverageID, Rate, odc.Sum_Insured AS SumInsured, Net_Premium AS Premium, 
                            Loading AS LoadingRate, Entry_Pct, Ndays, Excess_Rate, odc.Calc_Method, Cover_Premium, 
                            Gross_Premium, Max_si, Net1, Net2, Net3, odc.Deductible_Code, Begin_Date AS BeginDate, 
                            End_Date AS EndDate, Interest_Code, Product_Code
                            FROM dbo.Ord_Dtl_Coverage odc WITH (NOLOCK) 
							INNER JOIN dbo.Ord_Dtl_Interest odi WITH (NOLOCK) 
							ON odi.Order_no = odc.Order_No 
                            AND odi.Interest_No = odc.Interest_No
							INNER JOIN dbo.Mst_Order mo WITH (NOLOCK) 
							ON mo.Order_No = odc.Order_No 
							WHERE odc.Order_no = @0 AND odc.Interest_No = @1";
                            List<dynamic> newOSC = AABDB.Fetch<dynamic>(query, PolicyOrderNo, d.InterestNo);

                            decimal Premium = AABDB.ExecuteScalar<decimal>(
                                            @"SELECT COALESCE(SUM(Gross_Premium),0) FROM dbo.Ord_Dtl_Coverage WITH (NOLOCK) 
                                            WHERE Order_No = @0
                                            AND Interest_No = @1", PolicyOrderNo, d.InterestNo);
                            query = @"INSERT INTO dbo.OrderSimulationInterest
        ( OrderNo ,
          ObjectNo ,
          InterestNo ,
          InterestID ,
          Year ,
          Premium ,
          PeriodFrom ,
          PeriodTo ,
          LastUpdatedTime ,
          RowStatus ,
          Deductible_Code
        )
VALUES  ( @0 , -- OrderNo - varchar(100)
          @1 , -- ObjectNo - numeric
          @2 , -- InterestNo - numeric
          @3 , -- InterestID - char(6)
          @4 , -- Year - char(4)
          @5 , -- Premium - numeric
          @6 , -- PeriodFrom - datetime
          @7 , -- PeriodTo - datetime
          GETDATE() , -- LastUpdatedTime - datetime
          1 , -- RowStatus - bit
          @8  -- Deductible_Code - char(6)
        )";
                            AABMobileDB.Execute(query
                                , OrderNo
                                , d.ObjectNo
                                , d.InterestNo
                                , d.InterestID
                                , d.YEAR
                                , Premium
                                , d.PeriodFrom
                                , d.PeriodTo
                                , d.Deductible_Code);
                            #endregion

                            #region ordersimulationcoverage
                            query = @";IF NOT EXISTS (SELECT * FROM dbo.OrderSimulationCoverage WHERE OrderNo = @0 
AND ObjectNo = @1 AND InterestNo = @2 AND CoverageNo = @3 AND CoverageID = @4)
BEGIN
INSERT INTO dbo.OrderSimulationCoverage
                                        ( OrderNo ,
                                          ObjectNo ,
                                          InterestNo ,
                                          CoverageNo ,
                                          CoverageID ,
                                          Rate ,
                                          SumInsured ,
                                          LoadingRate ,
                                          Loading ,
                                          Premium ,
                                          BeginDate ,
                                          EndDate ,
                                          LastUpdatedTime ,
                                          RowStatus ,
                                          IsBundling ,
                                          Entry_Pct ,
                                          Ndays ,
                                          Excess_Rate ,
                                          Calc_Method ,
                                          Cover_Premium ,
                                          Gross_Premium ,
                                          Max_si ,
                                          Net1 ,
                                          Net2 ,
                                          Net3 ,
                                          Deductible_Code
                                        )
                                VALUES  ( @0 , -- OrderNo - varchar(100)
                                          @1 , -- ObjectNo - numeric
                                          @2 , -- InterestNo - numeric
                                          @3 , -- CoverageNo - numeric
                                          @4 , -- CoverageID - char(6)
                                          @5 , -- Rate - numeric
                                          @6 , -- SumInsured - numeric
                                          @7 , -- LoadingRate - numeric
                                          @8 , -- Loading - numeric
                                          @9 , -- Premium - numeric
                                          @10 , -- BeginDate - datetime
                                          @11 , -- EndDate - datetime
                                          GETDATE() , -- LastUpdatedTime - datetime
                                          1 , -- RowStatus - bit
                                          @12 , -- IsBundling - bit
                                          @13 , -- Entry_Pct - numeric
                                          @14 , -- Ndays - numeric
                                          @15 , -- Excess_Rate - numeric
                                          @16 , -- Calc_Method - varchar(2)
                                          @17 , -- Cover_Premium - numeric
                                          @18 , -- Gross_Premium - numeric
                                          @19 , -- Max_si - numeric
                                          @20 , -- Net1 - numeric
                                          @21 , -- Net2 - numeric
                                          @22 , -- Net3 - numeric
                                          @23  -- Deductible_Code - char(6)
                                        )
END";
                            foreach (dynamic osc in newOSC)
                            {
                                string squery = "";
                                decimal loading = (Convert.ToDecimal(osc.LoadingRate) / 100) * Convert.ToDecimal(osc.Premium);
                                int IsBundling = 0;
                                string interestCode = Convert.ToString(osc.Interest_Code);
                                if (interestCode.Contains("CASCO") || interestCode.Contains("ACCESS"))
                                {
                                    squery = @"SELECT a.Coverage_Id, a.Scoring_Code, b.Auto_Apply, 1 Year  FROM 
                                         Prd_Interest_Coverage a INNER JOIN B2B_Interest_Coverage b ON 
                                         a.Coverage_Id=b.Coverage_Id AND a.Interest_ID=b.Interest_Id AND 
                                         a.Product_Code=b.Product_Code WHERE a.Product_Code=@0
										 AND a.Coverage_Id = @1 
										 AND b.Vehicle_Type='ALL' 
										 AND b.Interest_Id='CASCO'";
                                    List<dynamic> ListTmp = AABMobileDB.Fetch<dynamic>(squery, osc.Product_Code, osc.CoverageID);
                                    if (ListTmp.Count > 0)
                                    {
                                        int AutoApply = Convert.ToInt32(ListTmp.First().Auto_Apply);
                                        if (AutoApply > 0)
                                        {
                                            IsBundling = 1;
                                        }
                                    }
                                }

                                AABMobileDB.Execute(query
                                    , OrderNo
                                    , osc.ObjectNo
                                    , osc.InterestNo
                                    , osc.CoverageNo
                                    , osc.CoverageID
                                    , osc.Rate
                                    , osc.SumInsured
                                    , osc.LoadingRate
                                    , loading
                                    , osc.Premium
                                    , osc.BeginDate
                                    , osc.EndDate
                                    , IsBundling
                                    , osc.Entry_Pct
                                    , osc.Ndays
                                    , osc.Excess_Rate
                                    , osc.Calc_Method
                                    , osc.Cover_Premium
                                    , osc.Gross_Premium
                                    , osc.Max_si
                                    , osc.Net1
                                    , osc.Net2
                                    , osc.Net3
                                    , osc.Deductible_Code);
                            }
                        }
                        #endregion
                    }

                    #region OrdersimulationMV
                    query = @"SELECT Vehicle_Code AS VehicleCode, Brand_Code AS BrandCode, 
                            Model_Code AS ModelCode, Series, Vehicle_Type AS Type, 
                            Sitting_Capacity AS Sitting, Mfg_Year AS [Year], 
                            Geo_Area_Code AS CityCode, Usage_Code AS UsageCode, 
                            Registration_Number AS RegistrationNumber, 
                            Engine_Number AS EngineNumber, Chasis_Number AS ChasisNumber,
                            New_Car AS IsNew, Color_On_BPKB AS ColorOnBPKB
                            FROM dbo.Ord_Dtl_MV
                            WHERE Order_No = @0";
                    List<dynamic> newOSMV = AABDB.Fetch<dynamic>(query, PolicyOrderNo);
                    query = @"SELECT * FROM dbo.Region WHERE RegionCode = @0";
                    string CityCode = "";
                    List<dynamic> isExist = AABMobileDB.Fetch<dynamic>(query, newOSMV.First().CityCode);
                    if (isExist.Count > 0)
                    {
                        CityCode = newOSMV.First().CityCode;
                    }
                    else
                    {
                        query = @"SELECT aab_code AS Code FROM dbo.partner_aab_map 
                                WHERE partner_code =@0 AND type = 'GEO' AND partner_id = 'OJK'";
                        List<dynamic> geoAreaCode = AABDB.Fetch<dynamic>(query, newOSMV.First().CityCode);
                        CityCode = geoAreaCode.First().Code;
                    }
                    query = @"SELECT Sum_Insured FROM dbo.Ord_Dtl_Interest where Order_no = @0 AND Interest_Code = 'CASCO' ORDER BY Period_From";
                    List<dynamic> ls = AABDB.Fetch<dynamic>(query, PolicyOrderNo);
                    decimal SI = ls.First().Sum_Insured;
                    query = @"UPDATE dbo.OrderSimulationMV
                            SET VehicleCode = @1, BrandCode = @2,
                            ModelCode = @3, Series = @4, [Type] = @5,
                            Sitting = @6, [Year] = @7, 
                            CityCode = @8, UsageCode = @9,
                            RegistrationNumber = @10, 
                            EngineNumber = @11, ChasisNumber = @12,
                            IsNew = @13, ColorOnBPKB = @14, SumInsured = @15
                            WHERE OrderNo = @0";
                    AABMobileDB.Execute(query, OrderNo
                        , newOSMV.First().VehicleCode, newOSMV.First().BrandCode
                        , newOSMV.First().ModelCode, newOSMV.First().Series, newOSMV.First().Type
                        , newOSMV.First().Sitting, newOSMV.First().Year
                        , CityCode, newOSMV.First().UsageCode
                        , newOSMV.First().RegistrationNumber
                        , newOSMV.First().EngineNumber, newOSMV.First().ChasisNumber
                        , newOSMV.First().IsNew, newOSMV.First().ColorOnBPKB, SI);
                    #endregion

                    #region OrderSimulation
                    query = @"SELECT CEILING(CAST(DATEDIFF(MONTH,Period_From, Period_To)/12.0 AS DECIMAL(10,2))) YearCoverage,
                            Branch_Id AS BranchCode, Product_Code AS ProductCode,
                            VANumber, '' AS SurveyNo, mo.Policy_No AS PolicyNo,
                            Policy_Id AS PolicyID, Segment_Code AS SegmentCode,
                            Broker_Code AS BrokerCode
                            FROM dbo.Mst_Order mo WITH (NOLOCK) 
                            WHERE Order_No = @0";
                    List<dynamic> newOS = AABDB.Fetch<dynamic>(query, PolicyOrderNo);
                    query = @";DECLARE @@AdminFee DECIMAL (20,4)
                            SELECT @@AdminFee=AdminFee FROM dbo.OrderSimulation
                            WHERE OrderNo = @0
                            SELECT COALESCE(SUM(Premium)+@@AdminFee,0) FROM dbo.OrderSimulationCoverage
                            WHERE OrderNo = @0";
                    decimal TotalPremium = AABMobileDB.ExecuteScalar<decimal>(query, OrderNo);
                    query = @"SELECT COALESCE(SUM(Year),0) FROM (
                            SELECT DISTINCT CAST(DATEDIFF(MONTH,Begin_Date,End_Date)/12.0 AS DECIMAL(10,2)) AS Year,Begin_Date, End_Date FROM dbo.Ord_Dtl_Coverage WITH (NOLOCK) 
                            WHERE Order_No = @0
                            AND Coverage_Id = 'ALLRIK' and Interest_Type = 'P') a";
                    decimal ComprePeriod = AABDB.ExecuteScalar<decimal>(query, PolicyOrderNo);
                    query = @"SELECT COALESCE(SUM(Year),0) FROM (
                            SELECT DISTINCT CAST(DATEDIFF(MONTH,Begin_Date,End_Date)/12.0 AS DECIMAL(10,2)) AS Year,Begin_Date, End_Date FROM dbo.Ord_Dtl_Coverage WITH (NOLOCK) 
                            WHERE Order_No = @0
                            AND Coverage_Id = 'TLO' and Interest_Type = 'P') a";
                    decimal TLOPeriod = AABDB.ExecuteScalar<decimal>(query, PolicyOrderNo);
                    string BrokerCode = Convert.ToString(newOS.First().BrokerCode);
                    string SalesmanCode = "";
                    string DealerCode = "";
                    if (!string.IsNullOrEmpty(BrokerCode))
                    {
                        if (BrokerCode.Trim().Length != 3)
                        {
                            List<dynamic> salesmancode = new List<dynamic>();
                            List<dynamic> dealercode = new List<dynamic>();
                            string querySalesman = "SELECT SalesmanCode, DealerCode FROM dbo.SalesmanDealer WHERE CustID = @0";
                            string queryDealer = "SELECT DealerCode FROM dbo.Dealer WHERE DealerCode = @0";
                            salesmancode = AABMobileDB.Fetch<dynamic>(querySalesman, BrokerCode);
                            if (salesmancode.Count > 0)
                            {
                                SalesmanCode = Convert.ToString(salesmancode.First().SalesmanCode);
                                dealercode = AABMobileDB.Fetch<dynamic>(queryDealer, salesmancode.First().DealerCode);
                                if (dealercode.Count > 0)
                                {
                                    DealerCode = Convert.ToString(dealercode.First().DealerCode);
                                }
                            }
                            else
                            {
                                queryDealer = "SELECT DealerCode FROM dbo.Dealer WHERE CustID = @0";
                                dealercode = AABMobileDB.Fetch<dynamic>(queryDealer, BrokerCode);
                                if (dealercode.Count > 0)
                                {
                                    DealerCode = Convert.ToString(dealercode.First().DealerCode);
                                }
                            }
                            if (string.IsNullOrEmpty(SalesmanCode))
                            {
                                query = @"SELECT SalesDealer, DealerCode FROM  dbo.OrderSimulation WHERE OrderNo = @0";
                                List<dynamic> tmpls = AABMobileDB.Fetch<dynamic>(query, OrderNo);
                                SalesmanCode = Convert.ToString(tmpls.First().SalesDealer);
                                DealerCode = Convert.ToString(tmpls.First().DealerCode);
                            }
                        }
                    }
                    query = @"UPDATE dbo.OrderSimulation 
                            SET YearCoverage = @1, TLOPeriod  = @2, ComprePeriod = @3,
                            BranchCode = @4, ProductCode = @5, TotalPremium = @6, 
                            SegmentCode = @7, SalesDealer = @8, DealerCode = @9
                            WHERE OrderNo = @0 ";
                    AABMobileDB.Execute(query, OrderNo
                        , newOS.First().YearCoverage
                        , TLOPeriod % 1 == 0 ? TLOPeriod : 0
                        , ComprePeriod % 1 == 0 ? ComprePeriod : 0
                        , newOS.First().BranchCode
                        , newOS.First().ProductCode
                        , TotalPremium
                        , newOS.First().SegmentCode
                        , SalesmanCode
                        , DealerCode);

                    query = @"SELECT PolicySentToId AS PolicySentTo,
                            ContactPerson AS PolicyDeliveryName,
                            CASE Delivery_Code WHEN 'GC' THEN
							gc.Name ELSE oda.Address END PolicyDeliveryAddress,
                            Postal_Code AS PolicyDeliveryPostalCode
                            FROM dbo.Ord_Dtl_Address oda
                            LEFT JOIN BEYONDMOSS.AABMobile.dbo.Mst_PolicySentTo mpt
                            ON mpt.PolicySentToId = oda.Delivery_Code
							LEFT JOIN dbo.GardaCenter gc ON gc.GCCode = oda.Source_Type
                            where Order_no=@0 AND Address_Type = 'DELIVR'";
                    List<dynamic> PolicyDeliver = AABDB.Fetch<dynamic>(query, PolicyOrderNo);
                    if (PolicyDeliver.Count > 0)
                    {
                        query = @"UPDATE dbo.OrderSimulation 
                            SET PolicySentTo = @1, PolicyDeliveryName = @2,
							PolicyDeliveryAddress = @3, PolicyDeliveryPostalCode = @4
                            WHERE OrderNo = @0";
                        AABMobileDB.Execute(query, OrderNo
                            , PolicyDeliver.First().PolicySentTo, PolicyDeliver.First().PolicyDeliveryName
                            , PolicyDeliver.First().PolicyDeliveryAddress, PolicyDeliver.First().PolicyDeliveryPostalCode);
                    }
                    #endregion
                    #region Customer
                    query = @"SELECT ProspectID, CAST(COALESCE(isCompany,0) AS BIT) isCompany, os.CustID, os.SalesOfficerID SID1, f.SalesOfficerID SID2, f.FollowUpNo
                            FROM dbo.ProspectCustomer ps
                            INNER JOIN dbo.OrderSimulation os
                            ON os.CustID = ps.CustID
							INNER JOIN dbo.FollowUp f
							ON f.FollowUpNo = os.FollowUpNo
                            WHERE OrderNo = @0";
                    List<dynamic> Prospect = AABMobileDB.Fetch<dynamic>(query, OrderNo);
                    dynamic ProspectID = Prospect.First().ProspectID;
                    dynamic isCompany = Prospect.First().isCompany;
                    dynamic CustID = Prospect.First().CustID;
                    string fNo = Prospect.First().FollowUpNo;
                    List<dynamic> infoCust = new List<dynamic>();
                    List<dynamic> CustInfo = new List<dynamic>();
                    query = "SELECT Prospect_Id,Cust_Id FROM dbo.Mst_Order where Order_No = @0";
                    infoCust = AABDB.Fetch<dynamic>(query, PolicyOrderNo);
                    if (infoCust.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(infoCust.First().Cust_Id)&& !string.IsNullOrWhiteSpace(infoCust.First().Cust_Id))
                        {
                            query = @"SELECT Name,HP Phone1,HP_2 Phone2,Email Email1,mp.Cust_Id CustIDAAB,
                                    mp.Prospect_Id ProspectID,Birth_Date CustBirthDay,IIF(Sex='1','M',IIF(Sex='2','F',NULL)) CustGender, mp.Home_Address CustAddress, 
                                    Home_PostCode PostalCode,id_card IdentityNo,IIF(Cust_Type=1,0,1) isCompany, Salesman_Id FROM dbo.Mst_Customer mp
                                    INNER JOIN dbo.Mst_Cust_Personal mpp ON mpp.Cust_Id = mp.Cust_Id
                                    INNER JOIN dbo.Mst_Order mo ON mo.Cust_Id = mp.Cust_Id
                                    WHERE Order_No = @0";
                            CustInfo = AABDB.Fetch<dynamic>(query, PolicyOrderNo);
                        }
                        else
                        {
                            query = @"SELECT Name,HP Phone1,HP_2 Phone2,Email Email1,mp.Cust_Id CustIDAAB,
                            mp.Prospect_Id ProspectID,Birth_Date CustBirthDay,IIF(Sex='1','M',IIF(Sex='2','F',NULL)) CustGender, mp.Home_Address CustAddress, 
                            Home_PostCode PostalCode,id_card IdentityNo,IIF(Prospect_Type=1,0,1) isCompany, Salesman_Id FROM dbo.Mst_Prospect mp
                            INNER JOIN dbo.Mst_Prospect_Personal mpp ON mpp.Prospect_Id = mp.Prospect_Id
                            INNER JOIN dbo.Mst_Order mo ON mo.Prospect_Id = mp.Prospect_Id
                            WHERE Order_No = @0";
                            CustInfo = AABDB.Fetch<dynamic>(query, PolicyOrderNo);
                        }
                        if (CustInfo.Count > 0)
                        {
                            query = @"UPDATE dbo.ProspectCustomer SET
                                Name = @1,	Phone1 = @2, Phone2 = @3, Email1 = @4,
                                CustIDAAB = @5, DealerCode = @6, SalesDealer = @7,	
                                BranchCode = @8, CustBirthDay = @9, CustGender = @10,
                                CustAddress = @11, PostalCode = @12, IdentityNo = @13, 
								isCompany = @14, LastUpdatedTime = GETDATE() WHERE CustID = @0";
                            AABMobileDB.Execute(query
                                , CustID, CustInfo.First().Name, CustInfo.First().Phone1, CustInfo.First().Phone2, CustInfo.First().Email1
                                , CustInfo.First().CustIDAAB, DealerCode, SalesmanCode
                                , newOS.First().BranchCode, CustInfo.First().CustBirthDay, CustInfo.First().CustGender
                                , CustInfo.First().CustAddress, CustInfo.First().PostalCode, CustInfo.First().IdentityNo, CustInfo.First().isCompany);
                        }
                        List<dynamic> Company = new List<dynamic>();
                        if (isCompany)
                        {
                            if (!string.IsNullOrEmpty(infoCust.First().Cust_Id) && !string.IsNullOrWhiteSpace(infoCust.First().Cust_Id))
                            {
                                query = @"SELECT mc.Name CompanyName,
                                            (SELECT AdditionalInfoValue FROM dbo.Mst_Customer_AdditionalInfo m
                                            WHERE AdditionalCode = 'NPWN' AND m.CustID = @0) NPWPno,
                                            NULLIF((SELECT AdditionalInfoValue FROM dbo.Mst_Customer_AdditionalInfo m
                                            WHERE AdditionalCode = 'NPDT' AND m.CustID = @0),'') NPWPdate,
                                            (SELECT AdditionalInfoValue FROM dbo.Mst_Customer_AdditionalInfo m 
                                            WHERE AdditionalCode = 'NPAD' AND m.CustID = @0) NPWPaddress,
                                            Office_Address OfficeAddress,
                                            Office_PostCode PostalCode,
                                            (SELECT AdditionalInfoValue FROM dbo.Mst_Customer_AdditionalInfo m 
                                            WHERE AdditionalCode = 'MPC1' AND m.CustID = @0) PICPhoneNo,
                                            mcp.Name AS PICname,
                                            Email
                                            FROM dbo.Mst_Customer mc
                                            LEFT JOIN dbo.Mst_Cust_Pic mcp
                                            ON mcp.Cust_Id = mc.Cust_Id
                                            WHERE mc.Cust_Id = @0 ORDER BY COALESCE(mcp.updatedt,mcp.entrydt) DESC";
                                Company = AABDB.Fetch<dynamic>(query, infoCust.First().Cust_Id);
                            }
                            else
                            {
                                query = @"SELECT mc.Name CompanyName,
                                    (SELECT AdditionalInfoValue FROM dbo.Mst_Prospect_AdditionalInfo m
                                    WHERE AdditionalCode = 'NPWN' AND m.ProspectID = @0) NPWPno,
                                    NULLIF((SELECT AdditionalInfoValue FROM dbo.Mst_Prospect_AdditionalInfo m
                                    WHERE AdditionalCode = 'NPDT' AND m.ProspectID = @0),'') NPWPdate,
                                    (SELECT AdditionalInfoValue FROM dbo.Mst_Prospect_AdditionalInfo m 
                                    WHERE AdditionalCode = 'NPAD' AND m.ProspectID = @0) NPWPaddress,
                                    Office_Address OfficeAddress,
                                    Office_PostCode PostalCode,
                                    (SELECT AdditionalInfoValue FROM dbo.Mst_Prospect_AdditionalInfo m 
                                    WHERE AdditionalCode = 'MPC1' AND m.ProspectID = @0) PICPhoneNo,
                                    mcp.Name AS PICname,
                                    Email, Cust_Id
                                    FROM dbo.Mst_Prospect mc
                                    LEFT JOIN dbo.Mst_Prospect_Pic mcp
                                    ON mcp.Prospect_Id = mc.Prospect_Id
                                    WHERE mc.Prospect_Id = @0 ORDER BY COALESCE(mcp.updatedt,mcp.entrydt) DESC";
                                Company = AABDB.Fetch<dynamic>(query, ProspectID);
                            }
                            if (Company.Count > 0)
                            {
                                dynamic datenpwp = null;
                                dynamic minsqlDate = AABDB.ExecuteScalar<dynamic>("SELECT CONVERT(VARCHAR,CAST('' AS DATE),103)");
                                if (!string.IsNullOrWhiteSpace(Company.First().NPWPdate) && !string.IsNullOrEmpty(Company.First().NPWPdate) && Company.First().NPWPdate != minsqlDate) {
                                    try
                                    {
                                        datenpwp = AABDB.ExecuteScalar<DateTime>("SELECT convert(datetime, @0, 103)", Company.First().NPWPdate);
                                    }
                                    catch (Exception e)
                                    {
                                    }
                                }
                                query = @"UPDATE dbo.ProspectCompany 
                                        SET CompanyName = @0, NPWPno = @1, NPWPdate = @2, NPWPaddress = @3,	
                                        OfficeAddress = @4,	PostalCode = @5, PICPhoneNo = @6, 
                                        PICname = @7, Email = @8
                                        WHERE CustID = @9";
                                AABMobileDB.Execute(query
                                    , Company.First().CompanyName, Company.First().NPWPno, datenpwp, Company.First().NPWPaddress
                                    , Company.First().OfficeAddress, Company.First().PostalCode, Company.First().PICPhoneNo
                                    , Company.First().PICname, Company.First().Email
                                    , CustID);
                            }
                        }
                    }
                    #region Agency
                    string osSalesOfficerID = Prospect.First().SID1;
                    string fSalesOfficerID = Prospect.First().SID2;
                    if (osSalesOfficerID.Length > 3 && fSalesOfficerID.Length > 3) {
                        string qGetSID = @"SELECT * FROM dbo.SalesOfficer WHERE SalesOfficerID = @0";
                        List<dynamic> sid = AABMobileDB.Fetch<dynamic>(qGetSID, osSalesOfficerID);
                        if (sid.Count > 0) {
                            string newSID = AABDB.ExecuteScalar<dynamic>("SELECT COALESCE(Broker_Code,'') Broker_Code FROM dbo.Mst_Order where Order_No = @0",PolicyOrderNo);
                            string qUpdateSID = @";UPDATE dbo.OrderSimulation SET SalesOfficerID = @0 WHERE OrderNo = @1
                                                ;UPDATE dbo.FollowUp SET SalesOfficerID = @0 WHERE FollowUpNo = @2";
                            AABMobileDB.Execute(qUpdateSID, newSID, OrderNo, fNo);
                        }
                    }
                    #endregion

                    #endregion
                    query = @"SELECT os.CustID,os.FollowUpNo,pc.ProspectID,COALESCE(FollowUpInfo,0) FollowUpInfo,CustIDAAB
						FROM dbo.OrderSimulation os
                        INNER JOIN dbo.ProspectCustomer pc
                        ON pc.CustID = os.CustID
						INNER JOIN dbo.FollowUp f
						ON f.FollowUpNo = os.FollowUpNo
                        WHERE OrderNo = @0";
                    List<dynamic> tmp = AABMobileDB.Fetch<dynamic>(query, OrderNo);
                    string SalesOfficerID = "";
                    string DeviceID = "";
                    if (tmp.Count > 0 && Convert.ToInt32(tmp.First().FollowUpInfo) != Convert.ToInt32(FollowUpInfo.FollowUpSurveyResult))
                    {
                        #region ImageData
                        string prosID = "";
                        if (!string.IsNullOrEmpty(tmp.First().CustIDAAB) && !string.IsNullOrEmpty(tmp.First().CustIDAAB)) {
                            prosID = Convert.ToString(tmp.First().CustIDAAB);
                        }
                        else
                        {
                            prosID = Convert.ToString(tmp.First().ProspectID);
                        }
                        query = @"SELECT Reference_Type, [Description], [Image], Thumbnail, di.Image_Id  
                            FROM dbo.Dtl_Image di WITH (NOLOCK) 
                            INNER JOIN dbo.Mst_Image mi WITH (NOLOCK) 
                            ON mi.Image_Id = di.Image_Id
                            WHERE (di.Reference_No = @0 OR di.Reference_No = @1)
							AND Reference_Type NOT IN ('NSA','BKTBYR')";
                        List<dynamic> ImageAAB = AABDB.Fetch<dynamic>(query, PolicyOrderNo, prosID);
                        query = @"SELECT a.FollowUpNo, id.ImageID, a.ImageName, a.ImageType, id.Data, COALESCE(id.CoreImage_ID, '') CoreImage_ID FROM 
                                (
                                SELECT 
	                                FollowUpNo,ImageName, ImageType
                                FROM 
                                (
                                select fu.FollowUpNo, IdentityCard, NPWP, SIUP, STNK, SPPAKB, BSTB1, Faktur, KonfirmasiCust, DocRep, DocPendukung, RenewalNotice, Survey --DocRep, CustomerConfirmation, *
                                from FollowUp fu WITH (NOLOCK)
                                LEFT JOIN ProspectCompany pcm WITH (NOLOCK) ON fu.CustID = pcm.CustID
                                INNER JOIN dbo.OrderSimulation os WITH (NOLOCK) ON os.FollowUpNo = fu.FollowUpNo
                                where fu.followupno = @0 AND os.ApplyF = 1 AND os.RowStatus = 1
                                )a 
                                UNPIVOT (
	                                ImageName FOR ImageType IN (
		                                IdentityCard, NPWP, SIUP, STNK, SPPAKB, BSTB1, Faktur, KonfirmasiCust, DocRep, DocPendukung, RenewalNotice, Survey
	                                )
                                ) unpvt
                                where ImageName <> ''
                                ) a 
                                INNER JOIN ImageData id WITH (NOLOCK) on ImageName = id.PathFile ";
                        List<dynamic> ImageMobile = AABMobileDB.Fetch<dynamic>(query, tmp.First().FollowUpNo);
                        string qInsertImg = @"IF NOT EXISTS (SELECT * FROM dbo.ImageData WHERE PathFile = @3 AND CoreImage_ID = @6)
                                            BEGIN
                                            INSERT INTO dbo.ImageData
                                                    ( ImageID ,
                                                        DeviceID ,
                                                        SalesOfficerID ,
                                                        PathFile ,
                                                        EntryDate ,
                                                        RowStatus ,
                                                        Data ,
                                                        ThumbnailData ,
			                                            CoreImage_ID
                                                    )
                                            VALUES  ( @0 , -- ImageID - varchar(100)
                                                        @1 , -- DeviceID - varchar(100)
                                                        @2 , -- SalesOfficerID - varchar(20)
                                                        @3 , -- PathFile - varchar(512)
                                                        GETDATE() , -- EntryDate - datetime
                                                        1 , -- RowStatus - bit
                                                        @4 , -- Data - varbinary(max)
                                                        @5 ,  -- ThumbnailData - varbinary(max)
			                                            @6
                                                    )
                                            END";
                        string qUpdateFollowUp = @"UPDATE dbo.FollowUp SET {0} = @0 WHERE FollowUpNo = @1";
                        string qUpdateProspect = @"UPDATE dbo.ProspectCompany SET {0} = @0 WHERE FollowUpNo = @1";
                        List<string> imgMobileTmp = new List<string>();
                        foreach (dynamic mbl in ImageMobile)
                        {
                            try
                            {
                                var referenceImageType = ImageLogic.GetMappedReferenceTypeImage(mbl.ImageType, false);
                                List<dynamic> tempAAB = new List<dynamic>();
                                if (referenceImageType != "NSA" || referenceImageType != "BKTBYR")
                                {
                                    imgMobileTmp.Add(referenceImageType);
                                    List<string> ListImageTypeAAB = new List<string>();
                                    foreach (dynamic aab in ImageAAB)
                                    {
                                        string type = Convert.ToString(aab.Reference_Type).Trim();
                                        if (type.Equals(referenceImageType))
                                        {
                                            tempAAB.Add(aab);
                                        }
                                    }
                                }
                                if (tempAAB.Count > 0)
                                {
                                    foreach (var a in tempAAB)
                                    {
                                        string s = Convert.ToString(a.Description);
                                        if (!string.IsNullOrEmpty(s) && s.Contains("(") && !string.IsNullOrEmpty(mbl.ImageType))
                                        {
                                            string[] parts = s.Split('(');
                                            if (a.Image_Id != mbl.CoreImage_ID)
                                            {
                                                dynamic imgdata = AABMobileDB.First<dynamic>("SELECT * FROM dbo.ImageData WHERE PathFile=@0 AND CoreImage_ID = @1"
                                                    , mbl.ImageName, mbl.CoreImage_ID);
                                                AABMobileDB.Execute("UPDATE dbo.ImageData SET RowStatus=0 WHERE PathFile=@0 AND CoreImage_ID = @1"
                                                    , mbl.ImageName, mbl.CoreImage_ID);
                                                string[] parts2 = parts[0].Split('.');
                                                string ImageID = Guid.NewGuid().ToString();

                                                byte[] imgData = ImageLogic.DecodeFromAAB2000ImageBytes(a.Image);
                                                byte[] thumbnail = ImageLogic.DecodeFromAAB2000ImageBytes(a.Thumbnail);
                                                SalesOfficerID = imgdata.SalesOfficerID;
                                                DeviceID = imgdata.DeviceID;
                                                AABMobileDB.Execute(qInsertImg
                                                    , ImageID
                                                    , imgdata.DeviceID
                                                    , imgdata.SalesOfficerID
                                                    , ImageID + "." + parts2[1]
                                                    , imgData
                                                    , thumbnail
                                                    , a.Image_Id);
                                                string type = Convert.ToString(mbl.ImageType);
                                                if (type.ToUpper().Equals("NPWP") || type.ToUpper().Equals("SIUP"))
                                                {
                                                    query = string.Format(qUpdateProspect, mbl.ImageType);
                                                    AABMobileDB.Execute(query, ImageID + "." + parts2[1], tmp.First().FollowUpNo);
                                                }
                                                else
                                                {
                                                    query = string.Format(qUpdateFollowUp, mbl.ImageType);
                                                    AABMobileDB.Execute(query, ImageID + "." + parts2[1], tmp.First().FollowUpNo);
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if (referenceImageType != "NSA" || referenceImageType != "BKTBYR")
                                    {
                                        if (!string.IsNullOrEmpty(mbl.ImageType))
                                        {
                                            AABMobileDB.Execute("UPDATE dbo.ImageData SET RowStatus=0 WHERE PathFile=@0 AND CoreImage_ID = @1"
                                            , mbl.ImageName, mbl.CoreImage_ID);
                                            string type = Convert.ToString(mbl.ImageType);
                                            if (type.ToUpper().Equals("NPWP") || type.ToUpper().Equals("SIUP"))
                                            {
                                                AABMobileDB.Execute(string.Format(qUpdateProspect, mbl.ImageType), "", tmp.First().FollowUpNo);
                                            }
                                            else
                                            {
                                                AABMobileDB.Execute(string.Format(qUpdateFollowUp, mbl.ImageType), "", tmp.First().FollowUpNo);
                                            }

                                        }                                    }
                                }
                            }
                            catch (Exception e)
                            {
                                logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", actionName, e.ToString(), e.StackTrace));
                            }
                        }
                        foreach (dynamic item in ImageAAB)
                        {
                            try
                            {
                                string RefType = item.Reference_Type.Trim();
                                if (!RefType.Equals("NSA") || RefType.Equals("BKTBYR"))
                                {
                                    if (!imgMobileTmp.Contains(RefType))
                                    {
                                        dynamic imgdata = AABMobileDB.First<dynamic>("SELECT * FROM dbo.ImageData WHERE PathFile=@0 AND CoreImage_ID = @1"
                                            , ImageMobile.First().ImageName, ImageMobile.First().CoreImage_ID);
                                        string[] parts = item.Description.Split('(');
                                        string[] parts2 = parts[0].Split('.');

                                        var ImageType = ImageLogic.GetRevertMappedReferenceTypeImage(item.Reference_Type.Trim());
                                        if (!string.IsNullOrEmpty(ImageType)) {
                                            string ImageID = Guid.NewGuid().ToString();
                                            byte[] imgData = ImageLogic.DecodeFromAAB2000ImageBytes(item.Image);
                                            byte[] thumbnail = ImageLogic.DecodeFromAAB2000ImageBytes(item.Thumbnail);
                                            SalesOfficerID = imgdata.SalesOfficerID;
                                            DeviceID = imgdata.DeviceID;
                                            AABMobileDB.Execute(qInsertImg
                                                , ImageID
                                                , imgdata.DeviceID
                                                , imgdata.SalesOfficerID
                                                , ImageID + "." + parts2[1]
                                                , imgData
                                                , thumbnail
                                                , item.Image_Id);
                                            string type = Convert.ToString(ImageType);
                                            if (type.ToUpper().Equals("NPWP") || type.ToUpper().Equals("SIUP"))
                                            {
                                                AABMobileDB.Execute(string.Format(qUpdateProspect, ImageType), ImageID + "." + parts2[1], tmp.First().FollowUpNo);
                                            }
                                            else
                                            {
                                                AABMobileDB.Execute(string.Format(qUpdateFollowUp, ImageType), ImageID + "." + parts2[1], tmp.First().FollowUpNo);
                                            }
                                        }
                                    }
                                }
                            }
                            catch (Exception e)
                            {
                                logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", actionName, e.ToString(), e.StackTrace));
                            }
                        }
                        string qGetBktByr = @"SELECT Image_Id FROM dbo.Dtl_Image where Reference_No = @0 AND Reference_Type = 'BKTBYR'";
                        List<string> listBktByr = AABDB.Fetch<string>(qGetBktByr, PolicyOrderNo);
                        string qGetDeletedImage = @";SELECT a.FollowUpNo, id.ImageID, a.ImageName, a.ImageType, id.Data, COALESCE(id.CoreImage_ID, '') CoreImage_ID, 
CASE WHEN CoreImage_ID IN (@1) THEN 0
ELSE 1 END isDelete 
FROM 
(
SELECT 
	FollowUpNo,ImageName, ImageType
FROM 
(
select fu.FollowUpNo, BuktiBayar, BuktiBayar2, BuktiBayar3, BuktiBayar4, BuktiBayar5
from FollowUp fu WITH (NOLOCK)
LEFT JOIN ProspectCompany pcm WITH (NOLOCK) ON fu.CustID = pcm.CustID
INNER JOIN dbo.OrderSimulation os WITH (NOLOCK) ON os.FollowUpNo = fu.FollowUpNo
where fu.followupno = @0 AND os.ApplyF = 1 AND os.RowStatus = 1
)a 
UNPIVOT (
	ImageName FOR ImageType IN (
		BuktiBayar, BuktiBayar2, BuktiBayar3, BuktiBayar4, BuktiBayar5
	)
) unpvt
where ImageName <> ''
) a 
INNER JOIN ImageData id WITH (NOLOCK) on ImageName = id.PathFile";
                        List<dynamic> listDelete = AABMobileDB.Fetch<dynamic>(qGetDeletedImage, tmp.First().FollowUpNo
                            ,listBktByr.Count==0? new List<string>(new string[] {""}):listBktByr);
                        List<string> existImg = new List<string>();
                        foreach (dynamic dlt in listDelete)
                        {
                            if (Convert.ToInt32(dlt.isDelete) == 1)
                            {
                                string qUpdate = @";UPDATE dbo.ImageData SET RowStatus=0 WHERE PathFile=@0 AND CoreImage_ID = @1
                                                UPDATE dbo.FollowUp SET {0} = '' WHERE FollowUpNo = @2";
                                AABMobileDB.Execute(string.Format(qUpdate, dlt.ImageType)
                                , dlt.ImageName, dlt.CoreImage_ID, tmp.First().FollowUpNo);
                            }
                            else { existImg.Add(dlt.CoreImage_ID); }
                            dynamic imgdata = AABMobileDB.First<dynamic>("SELECT * FROM dbo.ImageData WHERE PathFile=@0 AND CoreImage_ID = @1"
                                , dlt.ImageName, dlt.CoreImage_ID);
                            SalesOfficerID = imgdata.SalesOfficerID;
                            DeviceID = imgdata.DeviceID;
                        }
                        string qSelectPath = @"SELECT COALESCE(BuktiBayar,'') BuktiBayar, COALESCE(BuktiBayar2,'') BuktiBayar2, 
                                            COALESCE(BuktiBayar3,'') BuktiBayar3, COALESCE(BuktiBayar4,'') BuktiBayar4, COALESCE(BuktiBayar5,'') BuktiBayar5
                                            , SalesOfficerID 
                                            FROM dbo.FollowUp WHERE FollowUpNo = @0";
                        List<dynamic> lsPath = AABMobileDB.Fetch<dynamic>(qSelectPath, tmp.First().FollowUpNo);
                        List<string> lsBktByrNull = new List<string>();
                        if (lsPath.Count > 0) {
                            if (string.IsNullOrEmpty(lsPath.First().BuktiBayar)) {
                                lsBktByrNull.Add("BuktiBayar");
                            }
                            if (string.IsNullOrEmpty(lsPath.First().BuktiBayar2))
                            {
                                lsBktByrNull.Add("BuktiBayar2");
                            }
                            if (string.IsNullOrEmpty(lsPath.First().BuktiBayar3))
                            {
                                lsBktByrNull.Add("BuktiBayar3");
                            }
                            if (string.IsNullOrEmpty(lsPath.First().BuktiBayar4))
                            {
                                lsBktByrNull.Add("BuktiBayar4");
                            }
                            if (string.IsNullOrEmpty(lsPath.First().BuktiBayar5))
                            {
                                lsBktByrNull.Add("BuktiBayar5");
                            }
                            SalesOfficerID = lsPath.First().SalesOfficerID;
                        }
                        if (string.IsNullOrEmpty(DeviceID)) {
                            DeviceID = Guid.NewGuid().ToString();
                        }
                        string qSelectNewImg = @"SELECT mi.Image_Id,Image,Thumbnail,[File_Name] FROM dbo.Dtl_Image di  
                                                    INNER JOIN dbo.Mst_Image mi 
                                                    ON mi.Image_Id = di.Image_Id
                                                    where Reference_No = @0 AND Reference_Type = 'BKTBYR'
                                                    AND di.Image_Id NOT IN(@1)";
                        List<dynamic> newImg = AABDB.Fetch<dynamic>(qSelectNewImg, PolicyOrderNo, existImg.Count==0 ? new List<string>(new string[] { "" }) :existImg);
                        for (int i = 0; i < lsBktByrNull.Count; i++)
                        {
                            if (newImg.Count >= i+1) {
                                string fname = Convert.ToString(newImg[i].File_Name);
                                string[] parts = fname.Split('.');
                                string ImageID = Guid.NewGuid().ToString();
                                string qUpdate = "UPDATE dbo.FollowUp SET {0} = @1 WHERE FollowUpNo = @0";
                                AABMobileDB.Execute(string.Format(qUpdate, lsBktByrNull[i]), tmp.First().FollowUpNo, ImageID + "." + parts[1]);
                                byte[] imgData = ImageLogic.DecodeFromAAB2000ImageBytes(newImg[i].Image);
                                byte[] thumbnail = ImageLogic.DecodeFromAAB2000ImageBytes(newImg[i].Thumbnail);
                                
                                AABMobileDB.Execute(qInsertImg
                                    , ImageID
                                    , DeviceID
                                    , SalesOfficerID
                                    , ImageID + "." + parts[1]
                                    , imgData
                                    , thumbnail
                                    , newImg[i].Image_Id);
                            }
                        }
                        #endregion
                    }
                    #region PayerCompany
                    query = @";SELECT Cust_Type FROM dbo.Mst_Order mo
                                                            INNER JOIN dbo.Mst_Customer mc 
                                                            ON mo.Payer_Code = mc.Cust_Id
                                                            WHERE Order_No = @0";
                    List<dynamic> lsCustType = AABDB.Fetch<dynamic>(query, PolicyOrderNo);
                    if (lsCustType.Count > 0)
                    {
                        if (Convert.ToInt32(lsCustType.First().Cust_Type) == 2)
                        {
                            AABMobileDB.Execute(@"UPDATE dbo.ProspectCustomer SET IsPayerCompany = 1 WHERE CustID = @0", tmp.First().CustID);
                        }
                    }
                    #endregion
                    res++;
                }
                return res;
            }
            catch (Exception e)
            {
                logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", actionName, e.ToString(), e.StackTrace));
                return 0;
            }
        }

        public static void UpdateOtosalesCustomerToProspect(string followUpNo, string updateUsr)
        {
            // get ProspectCustomer
            dynamic c = GetFollowUpCustomer(followUpNo);

            if (!string.IsNullOrEmpty(c.PolicyOrderNo))
            {
                PersonalProspect1 ps = null;
                CorporateProspect1 cp = null;
                bool isPersonalProspect = !Convert.ToBoolean(c.isCompany);

                #region PersonalProspect

                ps = new PersonalProspect1()
                {
                    IDNumber = c.IdentityNo,
                    IDType = "KTP", //KTP & KITAS/KITAP jadi satu di Otosales, tp di core beda.
                    ProspectType = c.isCompany ? "2" : "1", // Personal  = 1, Company = 2
                    ProspectName = isPersonalProspect ? c.Name : c.CompanyName,
                    BirthDate = c.CustBirthDay == null ? null : c.CustBirthDay.ToString(DateFormatCustom.Format3),
                    Gender = c.CustGender == null ? null : Convert.ToString(c.CustGender).ToUpper().Equals("M") ? "1" : Convert.ToString(c.CustGender).ToUpper().Equals("F") ? "2" : null, // Male = 1, Female = 2
                    HomeAddress = c.CustAddress, // ga ada
                    HomePostalCode = c.PostalCode,
                    HomePostalCodeDesc = "",
                    HomeRT = "",
                    HomeRW = "",
                    HomeFax1 = "",
                    HomeFax2 = "",
                    MaritalStatus = "",
                    JobTitle = "",
                    CompanyName = "",
                    Email = c.Email1,
                    MobilePhone1 = c.Phone1,
                    MobilePhone2 = c.Phone2,
                    MobilePhone3 = "",
                    MobilePhone4 = "",
                    HomePhone1 = "",
                    HomePhone2 = "",
                    OfficePhone1 = "",
                    OfficeExt1 = "",
                    OfficeAddress = isPersonalProspect ? "" : c.NPWPaddress,
                    OfficePostalCode = isPersonalProspect ? "" : c.OfficePostalCode,
                    OfficePostalCodeDesc = "",
                    Facebook = "",
                    Twitter = "",
                    Instagram = ""
                };
                #endregion

                #region CorporateProspect
                cp = new CorporateProspect1()
                {
                    ProspectType = "2",
                    ProspectName = c.CompanyName,
                    NameOnNPWP = c.CompanyName,
                    NPWPNo = c.NPWPno,
                    NPWPDate = c.NPWPdate == null ? "" : c.NPWPdate,
                    NPWPAddress = c.NPWPaddress,
                    OfficeAddress = c.OfficeAddress,
                    OfficePostalCode = c.PostalCode,
                    PIC = c.PICname,
                    MobilePhone1 = c.PICPhoneNo,
                    Email = c.Email,
                    AssetOwner = "1",
                    CompanyGroup = "",
                    PKPNo = c.NPWPno,
                    PKPDate = c.NPWPdate == null ? "" : c.NPWPdate.ToString(CultureInfo.InvariantCulture),
                    BusinessType = "",
                    ClientType = "",
                    OfficePostalCodeDesc = "",
                    OfficePhone1 = "",
                    OfficePhone2 = "",
                    OfficePhoneExt = "",
                    OfficeRT = "",
                    OfficeRW = "",
                    OfficeFax1 = "",
                    OfficeFax2 = "",
                    MobilePhone2 = "",
                    HomeAddress = c.CustAddress,
                    HomePhone1 = "",
                    HomePhone2 = "",
                    HomePostalCode = "",
                    HomePostalCodeDesc = ""
                };

                #endregion

                if (isPersonalProspect) {
                    #region updatepersonal
                    if (!string.IsNullOrEmpty(c.CustIDAAB) && !string.IsNullOrWhiteSpace(c.CustIDAAB))
                    {
                        using (var db = GetAABDB())
                        {
                            db.Execute(@";UPDATE dbo.Mst_Customer
                                        SET Name = @1, Birth_Date = @2, Home_Address = @3, Email = @4,
                                        Home_PostCode = @10, Office_PostCode = @11, Home_Phone1 = @5,
                                        updatedt = GETDATE() ,updateusr = @9
                                        WHERE Cust_Id = @0
                                        
                                        UPDATE dbo.Mst_Order SET Name_On_Policy = @1 WHERE Order_No = @12

                                        UPDATE dbo.Mst_Cust_Personal
                                        SET HP = @5, HP_2 = @6, id_card = @7, Sex = @8,
                                        updatedt = GETDATE() ,updateusr = @9
                                        WHERE Cust_Id = @0",
                                        c.CustIDAAB, ps.ProspectName, ps.BirthDate, ps.HomeAddress, ps.Email,
                                        ps.MobilePhone1, ps.MobilePhone2, ps.IDNumber, ps.Gender, 
                                        updateUsr.Length > 3 ? "OTOSL" : updateUsr, ps.HomePostalCode, ps.OfficePostalCode, c.PolicyOrderNo);
                        }
                    }
                    else
                    {
                        using (var db = GetAABDB())
                        {
                            db.Execute(@";UPDATE dbo.Mst_Prospect 
                                        SET Name = @1, Birth_Date = @2, Home_Address = @3, Email = @4,
                                        Home_PostCode = @10, Office_PostCode = @11, Home_Phone1 = @5,
                                        updatedt = GETDATE() ,updateusr = @9
                                        WHERE Prospect_Id = @0

                                        UPDATE dbo.Mst_Order SET Name_On_Policy = @1, Name_On_Card = @1 WHERE Order_No = @12

                                        UPDATE dbo.Mst_Prospect_Personal
                                        SET HP = @5, HP_2 = @6, id_card = @7, Sex = @8,
                                        updatedt = GETDATE() ,updateusr = @9
                                        WHERE Prospect_Id = @0 ",
                                        c.ProspectID, ps.ProspectName, ps.BirthDate, ps.HomeAddress, ps.Email,
                                        ps.MobilePhone1, ps.MobilePhone2, ps.IDNumber, ps.Gender, 
                                        updateUsr.Length > 3 ? "OTOSL" : updateUsr, ps.HomePostalCode, ps.OfficePostalCode, c.PolicyOrderNo);
                        }
                    }
                    #endregion
                }
                else {
                    #region updatecompany
                    if (!string.IsNullOrEmpty(c.CustIDAAB) && !string.IsNullOrWhiteSpace(c.CustIDAAB))
                    {
                        using (var db = GetAABDB())
                        {
                            db.Execute(@";UPDATE dbo.Mst_Customer
                                        SET Name = @1, Birth_Date = @2, Office_Address = @3, Email = @4,
                                        Home_PostCode = @6, Office_PostCode = @7,
                                        updatedt = GETDATE() ,updateusr = @5
                                        WHERE Cust_Id = @0

                                        UPDATE dbo.Mst_Order SET Name_On_Policy = @1, Name_On_Card = @1 WHERE Order_No = @8",
                                        c.CustIDAAB, ps.ProspectName, ps.BirthDate, ps.OfficeAddress, ps.Email,
                                        updateUsr.Length > 3 ? "OTOSL" : updateUsr, ps.HomePostalCode, ps.OfficePostalCode, c.PolicyOrderNo);
                            if (!string.IsNullOrEmpty(cp.PIC) && !string.IsNullOrEmpty(cp.PIC)) {
                                db.Execute(@";IF EXISTS(SELECT * FROM dbo.Mst_Cust_Pic WHERE Cust_Id = @0)
										BEGIN
											DELETE FROM dbo.Mst_Cust_Pic WHERE Cust_Id = @0
	                                        INSERT INTO dbo.Mst_Cust_Pic
	                                                ( Cust_Id ,
	                                                  Name ,
	                                                  entryusr ,
	                                                  entrydt
	                                                )
	                                        VALUES  ( @0 , -- Prospect_Id - char(11)
	                                                  @1 , -- Name - varchar(50)
	                                                  @2 , -- entryusr - char(5)
	                                                  GETDATE()  -- entrydt - datetime
	                                                )
										END
										ELSE
                                        BEGIN
	                                        INSERT INTO dbo.Mst_Cust_Pic
	                                                ( Cust_Id ,
	                                                  Name ,
	                                                  entryusr ,
	                                                  entrydt
	                                                )
	                                        VALUES  ( @0 , -- Prospect_Id - char(11)
	                                                  @1 , -- Name - varchar(50)
	                                                  @2 , -- entryusr - char(5)
	                                                  GETDATE()  -- entrydt - datetime
	                                                )
                                        END", c.CustIDAAB, cp.PIC, updateUsr.Length > 3 ? "OTOSL" : updateUsr);
                            }
                            string qGetMstAdditonalInfo = @"SELECT AdditionalCode FROM dbo.Mst_Additional_Cust AS mac
                                                WHERE CustType = @0";
                            var MstAdditionalInfo = db.Query<dynamic>(qGetMstAdditonalInfo, 2).ToList();
                            string qUpdateCustAddInf = @";UPDATE dbo.Mst_Customer_AdditionalInfo
                                                        SET AdditionalInfoValue = @1, ModifiedBy = @3, ModifiedDate = GETDATE()
                                                        WHERE AdditionalCode = @2 AND CustID = @0";
                            List<AdditionalInfo1> ListAdditionalInfo = new List<AdditionalInfo1>();
                            for (int i = 0; i < MstAdditionalInfo.Count; i++)
                            {
                                #region add additional data
                                AdditionalInfo1 AI = new AdditionalInfo1();
                                AI.AdditionalCode = MstAdditionalInfo[i].AdditionalCode;
                                switch (i)
                                {
                                    case 0:
                                        AI.AdditionalInfoValue = cp.NPWPNo != null ? (cp.NPWPNo.Replace(".", "").Replace("-", "")) : "";
                                        db.Execute(qUpdateCustAddInf, c.CustIDAAB, AI.AdditionalInfoValue, AI.AdditionalCode, updateUsr.Length > 3 ? "OTOSL" : updateUsr);
                                        break;
                                    case 1:
                                        AI.AdditionalInfoValue = cp.MobilePhone1;
                                        db.Execute(qUpdateCustAddInf, c.CustIDAAB, AI.AdditionalInfoValue, AI.AdditionalCode, updateUsr.Length > 3 ? "OTOSL" : updateUsr);
                                        break;
                                    case 2:
                                        AI.AdditionalInfoValue = cp.MobilePhone2;
                                        db.Execute(qUpdateCustAddInf, c.CustIDAAB, AI.AdditionalInfoValue, AI.AdditionalCode, updateUsr.Length > 3 ? "OTOSL" : updateUsr);
                                        break;
                                    case 3:
                                        AI.AdditionalInfoValue = cp.NameOnNPWP != null ? cp.NameOnNPWP.ToUpper() : "";
                                        db.Execute(qUpdateCustAddInf, c.CustIDAAB, AI.AdditionalInfoValue, AI.AdditionalCode, updateUsr.Length > 3 ? "OTOSL" : updateUsr);
                                        break;
                                    case 4:
                                        AI.AdditionalInfoValue = cp.NPWPAddress;
                                        db.Execute(qUpdateCustAddInf, c.CustIDAAB, AI.AdditionalInfoValue, AI.AdditionalCode, updateUsr.Length > 3 ? "OTOSL" : updateUsr);
                                        break;
                                    case 5:
                                        AI.AdditionalInfoValue = cp.NPWPDate;
                                        db.Execute(qUpdateCustAddInf, c.CustIDAAB, AI.AdditionalInfoValue, AI.AdditionalCode, updateUsr.Length > 3 ? "OTOSL" : updateUsr);
                                        break;
                                    case 6:
                                        AI.AdditionalInfoValue = cp.PKPDate;
                                        db.Execute(qUpdateCustAddInf, c.CustIDAAB, AI.AdditionalInfoValue, AI.AdditionalCode, updateUsr.Length > 3 ? "OTOSL" : updateUsr);
                                        break;
                                    case 7:
                                        AI.AdditionalInfoValue = cp.PKPNo != null ? (cp.PKPNo.Replace(".", "").Replace("-", "")) : "";
                                        db.Execute(qUpdateCustAddInf, c.CustIDAAB, AI.AdditionalInfoValue, AI.AdditionalCode, updateUsr.Length > 3 ? "OTOSL" : updateUsr);
                                        break;
                                }
                                #endregion
                            }
                        }
                    }
                    else
                    {
                        using (var db = GetAABDB())
                        {
                            db.Execute(@";UPDATE dbo.Mst_Prospect
                                        SET Name = @1, Birth_Date = @2, Office_Address = @3, Email = @4,
                                        Home_PostCode = @6, Office_PostCode = @7,
                                        updatedt = GETDATE() ,updateusr = @5
                                        WHERE Prospect_Id = @0

                                        UPDATE dbo.Mst_Order SET Name_On_Policy = @1, Name_On_Card = @1 WHERE Order_No = @8",
                                        c.ProspectID, ps.ProspectName, ps.BirthDate, ps.OfficeAddress, ps.Email,
                                        updateUsr.Length > 3 ? "OTOSL" : updateUsr, ps.HomePostalCode, ps.OfficePostalCode, c.PolicyOrderNo);
                            if (!string.IsNullOrEmpty(cp.PIC) && !string.IsNullOrEmpty(cp.PIC))
                            {
                                db.Execute(@";IF NOT EXISTS(SELECT * FROM dbo.Mst_Prospect_Pic WHERE Prospect_Id = @0 AND Name = @1)
                                        BEGIN
	                                        INSERT INTO dbo.Mst_Prospect_Pic
	                                                ( Prospect_Id ,
	                                                  Name ,
	                                                  entryusr ,
	                                                  entrydt
	                                                )
	                                        VALUES  ( @0 , -- Prospect_Id - char(11)
	                                                  @1 , -- Name - varchar(50)
	                                                  @2 , -- entryusr - char(5)
	                                                  GETDATE()  -- entrydt - datetime
	                                                )
                                        END", c.ProspectID, cp.PIC, updateUsr.Length > 3 ? "OTOSL" : updateUsr);
                            }
                            string qGetMstAdditonalInfo = @"SELECT AdditionalCode FROM dbo.Mst_Additional_Cust AS mac
                                                WHERE CustType = @0";
                            var MstAdditionalInfo = db.Query<dynamic>(qGetMstAdditonalInfo, 2).ToList();
                            string qUpdateCustAddInf = @";UPDATE dbo.Mst_Prospect_AdditionalInfo
                                                        SET AdditionalInfoValue = @1, ModifiedBy = @3, ModifiedDate = GETDATE() 
                                                        WHERE AdditionalCode = @2 AND ProspectID = @0";
                            List<AdditionalInfo1> ListAdditionalInfo = new List<AdditionalInfo1>();
                            for (int i = 0; i < MstAdditionalInfo.Count; i++)
                            {
                                #region add additional data
                                AdditionalInfo1 AI = new AdditionalInfo1();
                                AI.AdditionalCode = MstAdditionalInfo[i].AdditionalCode;
                                switch (i)
                                {
                                    case 0:
                                        AI.AdditionalInfoValue = cp.NPWPNo != null ? (cp.NPWPNo.Replace(".", "").Replace("-", "")) : "";
                                        db.Execute(qUpdateCustAddInf, c.ProspectID, AI.AdditionalInfoValue, AI.AdditionalCode, updateUsr.Length > 3 ? "OTOSL" : updateUsr);
                                        break;
                                    case 1:
                                        AI.AdditionalInfoValue = cp.MobilePhone1;
                                        db.Execute(qUpdateCustAddInf, c.ProspectID, AI.AdditionalInfoValue, AI.AdditionalCode, updateUsr.Length > 3 ? "OTOSL" : updateUsr);
                                        break;
                                    case 2:
                                        AI.AdditionalInfoValue = cp.MobilePhone2;
                                        db.Execute(qUpdateCustAddInf, c.ProspectID, AI.AdditionalInfoValue, AI.AdditionalCode, updateUsr.Length > 3 ? "OTOSL" : updateUsr);
                                        break;
                                    case 3:
                                        AI.AdditionalInfoValue = cp.NameOnNPWP != null ? cp.NameOnNPWP.ToUpper() : "";
                                        db.Execute(qUpdateCustAddInf, c.ProspectID, AI.AdditionalInfoValue, AI.AdditionalCode, updateUsr.Length > 3 ? "OTOSL" : updateUsr);
                                        break;
                                    case 4:
                                        AI.AdditionalInfoValue = cp.NPWPAddress;
                                        db.Execute(qUpdateCustAddInf, c.ProspectID, AI.AdditionalInfoValue, AI.AdditionalCode, updateUsr.Length > 3 ? "OTOSL" : updateUsr);
                                        break;
                                    case 5:
                                        AI.AdditionalInfoValue = cp.NPWPDate;
                                        db.Execute(qUpdateCustAddInf, c.ProspectID, AI.AdditionalInfoValue, AI.AdditionalCode, updateUsr.Length > 3 ? "OTOSL" : updateUsr);
                                        break;
                                    case 6:
                                        AI.AdditionalInfoValue = cp.PKPDate;
                                        db.Execute(qUpdateCustAddInf, c.ProspectID, AI.AdditionalInfoValue, AI.AdditionalCode, updateUsr.Length > 3 ? "OTOSL" : updateUsr);
                                        break;
                                    case 7:
                                        AI.AdditionalInfoValue = cp.PKPNo != null ? (cp.PKPNo.Replace(".", "").Replace("-", "")) : "";
                                        db.Execute(qUpdateCustAddInf, c.ProspectID, AI.AdditionalInfoValue, AI.AdditionalCode, updateUsr.Length > 3 ? "OTOSL" : updateUsr);
                                        break;
                                }
                                #endregion
                            }
                        }
                    }
                    #endregion
                }
            }
        }

        public static dynamic GetFollowUpCustomer(string followUpNo)
        {
            string query = @"select os.PolicyOrderNo, pcs.CustID, pcs.Name, pcs.Phone1, pcs.Email1, pcs.PostalCode, pcs.CustAddress, pcm.CompanyName,
pcm.PostalCode AS OfficePostalCode, pcm.NPWP, convert(NVARCHAR, pcm.NPWPdate, 103) AS NPWPdate, pcm.NPWPaddress, pcm.OfficeAddress, pcm.PICPhoneNo, COALESCE(pcm.PICname,'') PICname,pcm.Email, CAST(COALESCE(pcs.isCompany,0) AS BIT) isCompany, pcs.IdentityNo,
pcs.CustBirthDay, pcs.CustGender, pcs.Phone2, fu.Remark, os.SalesOfficerID, fu.FollowUpInfo, pcm.NPWPno, COALESCE(CustIDAAB,'') CustIDAAB, COALESCE(ProspectID,'') ProspectID
from FollowUP fu 
INNER JOIN OrderSimulation os on fu.FollowUpNo = os.FollowUpNo and os.ApplyF = 1
INNER JOIN ProspectCustomer pcs on fu.CustID = pcs.CustID
LEFT JOIN ProspectCompany pcm on pcs.CustID = pcm.CustID
where fu.FollowUpNo = @0";

            using (var db = GetAABMobileDB())
            {
                return db.First<dynamic>(query, followUpNo);
            }
        }

        public OrderPaymentCustom GetTotalBayarNonVA(string OrderNo)
        {
            string CurrentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            using (var db = RepositoryBase.GetAABMobileDB())
            {
                try
                {
                    string cSQLGetTotalBayarNonVA = @"SELECT  SUM(ISNULL(CASE WHEN (a.IsAuthorized=1)
											        Then a.SettledAmount
											        ELSE a.PaidAmount   
										        END, 0)) AS TotalBayar ,
                                                MAX(a.TransactionDate) AS MaxTransactionDate ,
                                                0 AS TotalBill
                                        FROM    AABMobile.dbo.OrderSimulationPayment a
                                                INNER JOIN AABMobile.dbo.ordersimulation b ON a.OrderNo = b.OrderNo
                                                INNER JOIN Asuransiastra.GODigital.BankPaymentMethod c ON a.BankPaymentMethodCode = c.BankPaymentMethodCode
                                                INNER JOIN Asuransiastra.godigital.PaymentMethod d ON c.PaymentMethodCode = d.PaymentMethodCode
                                                INNER JOIN Asuransiastra.GODigital.PaymentGatewayRequestStatusMapping e ON e.PaymentRequestParameterCode = c.PaymentRequestParameterCode
                                                                                                      AND e.IsAuthorizedF = a.IsAuthorized
                                                                                                      AND a.PaymentStatus = e.PaymentRequestStatusCode
                                                INNER JOIN Asuransiastra.GODigital.MstPaymentStatus f ON f.PaymentStatusCode = e.PaymentStatusCode
                                        WHERE   a.ResultCD = '0000'
                                                AND a.ResultCD2 = '0000'
                                                AND a.IsProceedByDBUrl = 1
                                                AND a.RowStatus = 1
                                                AND b.RowStatus = 1
                                                AND c.RowStatus = 0
                                                AND d.rowstatus = 0
                                                AND e.rowstatus = 0
                                                AND f.rowstatus = 0
                                                AND ( ( d.PaymentMethodCode = '02'
                                                        AND COALESCE(a.ReceiptCode, '') = '0000'
                                                        AND COALESCE(a.AuthNo, '') = '0000'
                                                        AND a.IsAuthorized = 1
                                                      )
                                                      OR ( d.PaymentMethodCode = '04'
                                                           AND ( COALESCE(a.ReceiptCode, '') <> ''
                                                                 OR COALESCE(a.mRefNo, '') <> ''
                                                               )
                                                           AND a.IsAuthorized = 1
                                                         )
                                                      OR ( d.PaymentMethodCode = '01'
                                                           AND COALESCE(a.AuthNo, '') <> ''
                                                           AND ( ( c.PaymentType = 'F' )
                                                                 OR ( c.PaymentType = 'I'
                                                                      AND a.IsAuthorized = 1
                                                                    )
                                                               )
                                                         )
                                                    )
                                                AND f.PaymentStatusCode IN ( 'PD', 'HA', 'NA' )
                                                AND COALESCE(a.CancelCode, '') = ''		
		                                        AND b.PolicyOrderNo=@0
		                                        GROUP BY a.OrderNo";
                    return db.Query<OrderPaymentCustom>(cSQLGetTotalBayarNonVA, OrderNo).FirstOrDefault();
                }
                catch (Exception e)
                {
                    logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.StackTrace));
                    return null;
                }
            }
        }
        public OrderPaymentCustom GetTotalBayarVA(string orderno)
        {
            var aabdb = GetAABDB();
            DateTime transactionDate = DateTime.Now;
            string VANumber = string.Empty;
            List<dynamic> ordls = aabdb.Fetch<dynamic>(@"SELECT EntryDt, VANumber FROM dbo.Mst_Order where Order_No = @0", orderno);
            if (ordls.Count > 0) {
                transactionDate = ordls.First().EntryDt;
                VANumber = ordls.First().VANumber;
                using (var mbldb = GetAABMobileDB()) {
                    string qGetDate = @";DECLARE @@IsRenewal INT
            SELECT @@IsRenewal=COALESCE(IsRenewal,0) FROM dbo.OrderSimulation os 
            INNER JOIN dbo.FollowUp f 
            ON f.FollowUpNo = os.FollowUpNo
            WHERE PolicyOrderNo = @0
            IF(@@IsRenewal=0)
            BEGIN
	            SELECT EntryDate StartDate, DATEADD(DAY,CAST(ParamValue AS INT),EntryDate) EndDate FROM dbo.OrderSimulation os
	            INNER JOIN dbo.ApplicationParameters ap ON 1=1 AND ParamName = 'END-CHECKVAPAYMENT'
	            WHERE PolicyOrderNo = @0
            END
            ELSE
            BEGIN
	            SELECT DATEADD(DAY,CAST(ap1.ParamValue AS INT),p.Period_To) StartDate, 
	            DATEADD(DAY,CAST(ap2.ParamValue AS INT),DATEADD(DAY,CAST(ap1.ParamValue AS INT),p.Period_To)) EndDate
	            FROM BEYONDREPORT.AAB.dbo.Mst_Order mo
	            INNER JOIN BEYONDREPORT.AAB.dbo.Policy p ON p.Policy_No = mo.Old_Policy_No
	            INNER JOIN dbo.ApplicationParameters ap1 ON 1=1 AND ap1.ParamName = 'START-CHECKVAPAYMENT'
	            INNER JOIN dbo.ApplicationParameters ap2 ON 1=1 AND ap2.ParamName = 'END-CHECKVAPAYMENT'
	            WHERE mo.Order_No = @0
            END";
                    List<dynamic> lsDate = mbldb.Fetch<dynamic>(qGetDate, orderno);
                    using (var db = Geta2isBeyondDB())
                    {
                        string trnsDate = "";
                        string exprDate = "";

                        if (transactionDate != null)
                        {
                            string dayadd = "14"; //GeneralAsuransiAstraAccess.GetApplicationParameters("ORDER-EXPIRED-DAY-ADD");
                            string hour = "00";//GeneralAsuransiAstraAccess.GetApplicationParameters("ORDER-EXPIRED-HOUR");
                            string minute = "00";//GeneralAsuransiAstraAccess.GetApplicationParameters("ORDER-EXPIRED-MINUTE");
                            string second = "00";//GeneralAsuransiAstraAccess.GetApplicationParameters("ORDER-EXPIRED-SECOND");

                            // BEGIN 0043 Hotfix 2019 by BLO :  - Bugfix exception when the transactionDate.Day = last day of month (example : 31 ) 
                            //DateTime expDate = new DateTime(transactionDate.Year, transactionDate.Month,
                            //    transactionDate.Day + Int32.Parse(dayadd), Int32.Parse(hour), Int32.Parse(minute), Int32.Parse(second), 0);
                            DateTime expDate = new DateTime(transactionDate.Year, transactionDate.Month,
                                transactionDate.Day, Int32.Parse(hour), Int32.Parse(minute), Int32.Parse(second), 0);
                            expDate = expDate.AddDays(Convert.ToDouble(dayadd));
                            // END 0043 Hotfix 2019 by BLO - Bugfix exception when the transactionDate.Day = last day of month (example : 31 ) 

                            trnsDate = lsDate.Count > 0 ? lsDate.First().StartDate.ToString("yyyy-MM-dd HH:mm:ss.ff") : transactionDate.ToString("yyyy-MM-dd HH:mm:ss.ff");
                            exprDate = lsDate.Count > 0 ? lsDate.First().EndDate.ToString("yyyy-MM-dd HH:mm:ss.ff") : expDate.ToString("yyyy-MM-dd HH:mm:ss.ff");

                        }

                        string cSQLGetTotalBayarVA = @";WITH    cte
                                                        AS ( SELECT PermataVirtualAccountSettlementID ,
                                                                    a.VI_TraceNo
                                                            FROM     PolicyInsurance.PermataVirtualAccountSettlement a
                                                                    INNER JOIN PolicyInsurance.PermataVirtualAccount b ON a.PermataVirtualAccountID = b.PermataVirtualAccountID
                                                            WHERE    b.VI_VANumber IN (
                                                                        SELECT  value
                                                                        FROM    PolicyInsurance.fn_Split(@0, ',') 
                                                                     )
                                                        --AND a.PermataVirtualAccountPaymentSourceCode = '02'
		                                                AND a.VI_TransactionDate BETWEEN @1 AND @2
                                                            )
                                                SELECT  MAX(pvas.PermataVirtualAccountSettlementID) as PermataVirtualAccountSettlementID,
                                                        SUM(pvas.VI_Amount) AS TotalBayar ,
                                                        MAX(pvas.VI_TransactionDate) AS MaxTransactionDate ,
                                                        MAX(pva.BillAmount) AS TotalBill
                                                FROM    cte
                                                        INNER JOIN PolicyInsurance.PermataVirtualAccountSettlement AS pvas ON pvas.PermataVirtualAccountSettlementID = cte.PermataVirtualAccountSettlementID
                                                                                                            AND pvas.VI_TraceNo = cte.VI_TraceNo
                                                        INNER JOIN PolicyInsurance.PermataVirtualAccount AS pva ON pva.PermataVirtualAccountID = pvas.PermataVirtualAccountID
                                                WHERE   IsReversal = 0";
                        return db.Query<OrderPaymentCustom>(cSQLGetTotalBayarVA, VANumber, trnsDate, exprDate).FirstOrDefault();
                    }
                }
            }
            return null;
        }

        public bool CheckSettlement(string VoucherNo) {
            string CurrentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            bool Settled = false;
            string qCekSettlement = @";SELECT  v.VoucherID
                                        FROM    Finance.Voucher AS v
                                                INNER JOIN Finance.VoucherDetail AS vd ON vd.VoucherID = v.VoucherID
                                                                                          AND v.RowStatus = 0
                                                                                          AND vd.RowStatus = 0
                                                INNER JOIN General.VoucherType AS vt ON vt.VoucherTypeID = v.VoucherTypeID
                                                                                        AND vt.RowStatus = 0
                                        WHERE   VoucherNo = @0
                                                AND VoucherStatusID = 18";
            try
            {
                using (var db = RepositoryBase.Geta2isBeyondDB())
                {
                    dynamic result = db.Query<dynamic>(qCekSettlement, VoucherNo).ToList();
                    if (result.Count > 0)
                    {
                        Settled = true;
                    }
                }
            }
            catch (Exception e)
            {
                logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.StackTrace));
            }
            return Settled;
        }
    }
}