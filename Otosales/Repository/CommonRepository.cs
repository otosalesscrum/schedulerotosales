﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Otosales.Models;
using static Otosales.Models.Constant;

namespace Otosales.Repository
{
    public class CommonRepository : RepositoryBase
    {
        private string qGetBranch = @"
            Select A.Branch_id,
	            A.Nama
	            From Mst_Branch A
		            INNER JOIN Mst_Branch_Area B On A.Branch_id = B.Branch_id
	            Where Biz_type = (
		            Select COALESCE(Biz_type, 1)
			            From Mst_Product
			            Where Product_Code = @0
	            )
        ";

        private string qGetPostalCode = @";SELECT  *
                                            FROM    ( SELECT    LTRIM(RTRIM(p.PostCode)) AS PostCode ,
                                                                RTRIM(p.PostCode) + ' - ' +RTRIM(p.Description) + ', ' + RTRIM(p1.Description) + ', '
                                                                + RTRIM(p2.Description) + ', ' + RTRIM(p3.Description) AS Description
                                                      FROM      dbo.Mst_Postal p
                                                                LEFT JOIN dbo.Mst_General p1 ON p1.TYPE = 'CTY'
                                                                                                AND p.City = p1.code
                                                                LEFT JOIN dbo.Mst_General p2 ON p2.TYPE = 'PRV'
                                                                                                AND p.Province = p2.code
                                                                LEFT JOIN dbo.Mst_General p3 ON p3.TYPE = 'CON'
                                                                                                AND p.Country = p3.code
                                                    WHERE p.Status = 1 
                                            ) X 
                                        ";

        public dynamic GetPostalCode(int pageNo, int pageSize, string searchBy, string keyword, string sortOption, string sortDirection)
        {
            switch (searchBy)
            {
                case "0":
                    qGetPostalCode += @" WHERE X.PostCode LIKE '%' + @0 + '%'  ";
                    break;
                case "1":
                    qGetPostalCode += @" WHERE X.Description LIKE '%' + @0 + '%'";
                    break;
                default:
                    qGetPostalCode += @" WHERE X.PostCode LIKE '%' + @0 + '%' ";
                    break;
            }

            switch (sortOption)
            {
                case "0":
                    qGetPostalCode += @" Order By X.PostCode ";
                    break;
                case "1":
                    qGetPostalCode += @" Order By X.Description ";
                    break;
                default:
                    qGetPostalCode += @" Order By X.PostCode ";
                    break;
            }

            qGetPostalCode += sortDirection;

            using (var db = GetAABDB())
            {
                dynamic result = db.Page<dynamic>(pageNo, pageSize, qGetPostalCode, keyword);
                return result;
            }
        }
        public dynamic GetPostalCode(string postalCode)
        {
            var query = qGetPostalCode + " WHERE X.PostCode = @0";
            using (var db = GetAABDB())
            {
                var result = db.Query<dynamic>(query, postalCode);
                if (result != null)
                    return result.FirstOrDefault();
                return null;
            }
        }

        public dynamic GetBranch(string productCode)
        {
            using (var db = GetAABDB())
            {
                dynamic result = db.Fetch<dynamic>(qGetBranch, productCode);
                return result;
            }
        }

        public static string IDGenereation(string IDType, int IDLength)
        {
            string CurrentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            string qInsertID = @"INSERT  INTO dtl_urut ( code, no_urut, entrydt ) VALUES  ( @0, @1, GETDATE() )";
            string qInsertDuplicate = @"insert into dtl_urut_duplicate (code, no_urut, entrydt) values (@0,@1,getdate())";
            string spGenerateID = @";EXEC sp_IDGeneration @0";
            var db = GetAABDB();
            int IDNo = 0;
            try
            {
                var temp = db.FirstOrDefault<dynamic>(spGenerateID, IDType);
                IDNo = Convert.ToInt32(temp.no_urut);
                db.Execute(qInsertID, IDType, IDNo);
                return IDNo.ToString().PadLeft(IDLength, '0');
            }
            catch (SqlException sqle)
            {
                int code = sqle.ErrorCode;
                if (code == -2147217873)
                {
                    db.Execute(qInsertDuplicate, IDType, IDNo);
                }
                return "";
            }
        }

        public static string GetBookingVANo()
        {
            string qGetVANo = @";DECLARE @@VANumber VARCHAR(MAX)
                                EXEC usp_GetVANumber @@VANumber OUTPUT
                                SELECT @@VANumber VirtualAccountNo";
            using (var db = GetAABDB())
            {
                var result = db.Query<dynamic>(qGetVANo).FirstOrDefault();
                return result.VirtualAccountNo;
            }
        }
        

        public static string GetQuotationNo(string UserID, string BranchID="000")
        {
            DateTime now = DateTime.Now;
            string yearnow = Convert.ToString(now.Year);
            string monthnow = Convert.ToString(now.Month);

            string Code = string.Format("WTORDER{0}{1}", yearnow.Substring(yearnow.Length-2,2), monthnow);
            using (var db = GetAABDB())
            {
                int no_urut = db.Query<int>(";EXEC sp_IDGeneration @0", Code).SingleOrDefault();
                string OrderNo = no_urut.ToString("X");
                if (OrderNo.Length < 5)
                    OrderNo = OrderNo.PadLeft(5, '0');
                string s_OrderNo = OrderNo.Substring(OrderNo.Length - 5, 5);
                string ReportNo = string.Format("{0}/{1}/{2}/{3}/{4}",s_OrderNo,UserID.TrimEnd().ToUpper(),BranchID,monthnow,yearnow) ;
                db.Execute("insert into dtl_urut (code, no_urut, entrydt) values (@0,@1,getdate())", Code, no_urut);
                return ReportNo;
            }
        }
        

        public dynamic GetRegionCategory(string CustomerID)
        {
            string query = @"  SELECT c.insurance_code AS geocode ,
                                a.geoname ,
                                ( SELECT    geoName
                                  FROM      aab_geo_area
                                  WHERE     geocode = a.geocodeProvince
                                ) AS GeoCodeProvince ,
                                a.Region AS Wilayah ,
		                        a.GeoCodeProvince AS GeoCodeProvince,		
                                a.GeoType	
                         FROM   aab_geo_area a
                                INNER JOIN dtl_ins_Factor c ON a.geocode = c.Insurance_Code
                         WHERE  LTRIM(RTRIM(a.GeoType)) = 'C'
                                AND c.factor_code = 'GEOGRA'
                                AND A.GeoCode =@0";
            using (var db = GetAABDB())
            {
                return db.Query<dynamic>(query, CustomerID).FirstOrDefault();
            }
        }
        
        public dynamic GetDailyRate(DateTime dtDate, string CurrencyID)
        {
            string query = @"SELECT TOP 1 DateRate, Curr_ID as CurrencyID, Exchange_Rate as ExchangeRate, Mbl_Rate as MblRate,  
                            Yearly_Rate as YearlyRate, Risk_Block as RiskBlock, Tax_Rate as TaxRate 
                        FROM MST_DAILY_RATE 
                        WHERE DATERATE <= @0
                        AND CURR_ID= @1
                        ORDER BY DATERATE DESC";
            using (var db = GetAABDB())
            {
                return db.Query<dynamic>(query, dtDate, CurrencyID).FirstOrDefault();
            }
        }




        public static string GetGlobalParameter(string type, string paramName, string referenceID)
        {
            string qGetConfiguration = "";

            if (type == "BRANCH")
            {
                qGetConfiguration = @"
                    Select GC.ConfigurationValue As ConfigurationValue
	                    From Mst_User U
		                    INNER JOIN GeneralConfiguration GC On U.Branch_Id = GC.ReferenceID
	                    Where GC.RowStatus = 0
                            AND GC.ReferenceType = @0
		                    AND GC.ConfigurationName = @1
		                    AND U.User_Id = @2
                ";
            }
            else
            {
                qGetConfiguration = @"
                    Select GC.ConfigurationValue As ConfigurationValue
	                    From GeneralConfiguration GC
	                    Where GC.RowStatus = 0
                            AND GC.ReferenceType = @0
		                    AND GC.ConfigurationName = @1
		                    AND GC.ReferenceID = @2
                ";
            }

            //'Reengineering.AllowSAAccquisition'

            string isAllowSAAccquisition = "1";
            using (var db = GetAABDB())
            {
                var result = db.Query<dynamic>(qGetConfiguration, type, paramName, referenceID).ToList();

                if (result.Count > 0)
                {
                    if (result[0].ConfigurationValue == "1") isAllowSAAccquisition = "1";
                    else isAllowSAAccquisition = "0";
                }
            }

            return isAllowSAAccquisition;
        }

        public static string GetGlobalParameter(string Parameter)
        {
            string query = @";Select top 1 rtrim(value) as Value from GLOBAL_PARAMETER with(nolock) Where Parameter=@0";

            using (var db = GetAABDB())
            {
                return db.Query<string>(query, Parameter).SingleOrDefault();
            }
        }

        public static List<string> GetGeneralConfig(string ConfigName, string RefType, string RefID, string ConfigValue="")
        {
            string query = @";EXEC [dbo].usp_GetGeneralConfig
                            @@ConfigurationName =@0,
                            @@ReferenceType =@1,
                            @@ReferenceID =@2,                            
	                        @@ConfigurationValue=@3";

            using (var db = GetAABDB())
            {
                return db.Query<string>(query, ConfigName, RefType,RefID, ConfigValue).ToList();
            }
        }

        public static List<dynamic> GetGeneralConfigListNumeric(string ConfigName)
        {
            string query = @"SELECT  ReferenceType ,
                                    ReferenceID ,
                                    ConfigurationName ,
                                    CONVERT(NUMERIC(15,6), ConfigurationValue) ConfigurationValue
                            FROM    dbo.GeneralConfiguration AS gc
                            WHERE   ReferenceType = @0
                                    AND RowStatus = 0";

            using (var db = GetAABDB())
            {
                return db.Query<dynamic>(query, ConfigName).ToList();
            }
        }

        public static List<string> GetGeneralConfigByName(string ConfigName)
        {
            string query = @"SELECT  RTRIM(ConfigurationValue) ConfigurationValue
                        FROM    dbo.GeneralConfiguration AS gc
                        WHERE   ConfigurationName = @0
                                AND RowStatus = 0 ";

            using (var db = GetAABDB())
            {
                return db.Query<string>(query, ConfigName).ToList();
            }
        }

        public static List<MasterGeneral> GetMasterGeneral(string Type)
        {
            string query = @"; DECLARE @@Type AS VARCHAR(20)
                                SELECT  @@Type = @0
                                SELECT RTRIM(Code)Code,
                                        RTRIM(Description) Description
                                FROM    dbo.mst_general
                                WHERE[type] = @@Type
                                        AND status = 1 ";

            using (var db = GetAABDB())
            {
                return db.Query<MasterGeneral>(query, Type).ToList();
            }
        }
        public static List<MasterGeneral> GetMasterGeneral(string Type, string Code)
        {
            string query = @"; DECLARE @@Type AS VARCHAR(20)
                                DECLARE @@Code AS VARCHAR(6)
                                SELECT  @@Type = @0
                                SELECT  @@Code = @1
                                SELECT RTRIM(Code)Code,
                                        RTRIM(Description) Description
                                FROM    dbo.mst_general
                                WHERE[type] = @@Type
                                        AND Code = @1 
                                        AND status = 1 ";

            using (var db = GetAABDB())
            {
                return db.Query<MasterGeneral>(query, Type, Code).ToList();
            }
        }

        public static int GetTitanOrderNo() {
            int orderNo = 0;
            string strSQL = @"SELECT SeqNo AS SeqNo FROM dbo.SysSeqNo WHERE ObjName = 'Orders'";
            using (var db = Geta2isTitanDB())
            {
                var result = db.Fetch<dynamic>(strSQL);
                if (result.Count > 0) {
                    orderNo = result[0].SeqNo;
                }
            }
            return orderNo+1;
        }

        public static string GetUserLoginMap(string UserID, string ProductCode)
        {

            string query = @"Select Top 1 Coalesce(User_ID, @0) UserID 
	                            From Mst_Product mp 
	                            LEFT Join mst_user_login_Map mulm on mp.Biz_type=mulm.Biz_Type AND mulm.Login_Id=@0
	                            Where  mp.Product_Code=@1 AND mp.Status=1";

            using (var db = GetAABDB())
            {

                string result = db.Query<string>(query, UserID, ProductCode).SingleOrDefault();

                return result;
            }
        }

        public static dynamic GetFCMToken(string userId)
        {
            string queryUser = @"
SELECT distinct a.DeviceToken,b.UserID 
FROM Device a 
INNER JOIN userdevice b ON a.DeviceID=b.DeviceID AND b.Status=1 and a.Status = 1 and a.ApplicationID = 4 AND b.ApplicationID=4 
WHERE b.userid=@0";
            using (var db = GetGardaMobileCMS())
            {
                var que = db.FirstOrDefault<dynamic>(queryUser, userId);
                return que;
            }
        }

        public static dynamic GetSuperior(string userId)
        {
            string queryUser = @";exec usp_getOtosalesSuperior @0";
            using (var db = GetAABMobileDB())
            {
                return db.Fetch<dynamic>(queryUser, userId);
            }
        }

        public static dynamic GetSalesOfficerID(string policyOrderNo)
        {
            string queryUser = @"SELECT SalesOfficerID FROM OrderSimulation WHERE ApplyF = 1 and PolicyOrderNo = @0";
            using (var db = GetAABMobileDB())
            {
                List<dynamic> ls =  db.Fetch<dynamic>(queryUser, policyOrderNo);
                if (ls.Count > 0)
                {
                    return ls.First().SalesOfficerID;
                }
                else {
                    return "";
                }
            }
        }

        public static dynamic GetUserIDApprovalNextLimit(string policyOrderNo)
        {
            string queryUser = @"
select Data [UserID] from udf_SplitString(
(select TOP 1 Reason from mst_order_mobile_approval where order_no =@0 and ApprovalType = @1)
,',')a";
            using (var db = GetAABDB())
            {
                return db.Fetch<dynamic>(queryUser, policyOrderNo, ApprovalType.NextLimit);
            }
        }

        public static dynamic GetOrderSimulation(string OrderNo) {
            string query = @"SELECT PolicyNo, SalesOfficerID, CustID, FollowUpNo FROM dbo.OrderSimulation WHERE OrderNo=@0 and RowStatus = 1 and ApplyF = 1";
            using (var db = GetAABMobileDB()) {
                return db.FirstOrDefault<dynamic>(query, OrderNo);
            }
        }

        public static dynamic GetProspectCustomer(string custID)
        {
            string query = @"SELECT isCompany, Name FROM dbo.ProspectCustomer WHERE CustID=@0 ";
            using (var db = GetAABMobileDB())
            {
                return db.FirstOrDefault<dynamic>(query, custID);
            }
        }

        public static dynamic GetProspectCompany(string custID)
        {
            string query = @"SELECT CompanyName FROM dbo.ProspectCompany WHERE CustID=@0 ";
            using (var db = GetAABMobileDB())
            {
                return db.FirstOrDefault<dynamic>(query, custID);
            }
        }

        public static dynamic GetSalesOfficerBranch(string userID)
        {
            string query = @"SELECT so.Name, ab.* FROM dbo.SalesOfficer so INNER JOIN AABBranch ab ON so.BranchCode = ab.BranchCode WHERE SalesOfficerID=@0";
            using (var db = GetAABMobileDB())
            {
                return db.FirstOrDefault<dynamic>(query, userID);
            }
        }

        public static int InsertPushNotification(string userId, string fcmToken, string subject, string bodyMsg, string dataMsg, string clickAction)
        {
            string queryInsert = @"INSERT INTO dbo.PushNotificationOtosales
                                                                            (UserID, 
                                                                            FCMToken, 
                                                                            Subject, 
                                                                            Body, 
                                                                            Data, 
                                                                            Status, 
                                                                            ClickAction, 
                                                                            EntryDate)
                                   VALUES(@0,
                                          @1, 
                                          @2, 
                                          @3, 
                                          @4, 
                                          1, 
                                          @5, 
                                          GETDATE())";
            using (var db = GetAABMobileDB())
            {
                return db.Execute(queryInsert,
                                     userId,
                                     fcmToken,
                                     subject,
                                     bodyMsg,
                                     dataMsg,
                                     clickAction);
            }
        }

        public dynamic getSalesOfficerApproval(string OrderNo) {
            string query = @"SELECT a.SalesOfficerID AS [SalesOrderSimulation], 
                             b.SalesOfficerID AS [SalesFollowUp], 
                             a.BranchCode AS [BranchCode], 
                             a.PolicyOrderNo AS [PolicyNo] FROM OrderSimulation a
                             INNER JOIN FollowUp b On a.FollowUpNo = b.FollowUpNo
                             WHERE a.OrderNo=@0
                            ";
            using (var db = GetAABMobileDB()) {
                var que = db.FirstOrDefault<dynamic>(query, OrderNo);
                return que;
            }
        }

        public List<dynamic> getNewDataNeedApproval()
        {
            string query = "SELECT a.OrderNo as [OrderNumber], a.CustID as [Customerid], a.FollowUpNo as [FollowUpNo], a.SalesOfficerID as [SalesOri], b.SalesOfficerID as [SalesChange] FROM OrderSimulation a INNER JOIN FollowUp b ON a.FollowUpNo = b.FollowUpNo WHERE a.FollowUpNo = b.FollowUpNo AND b.FollowUpInfo = (SELECT StatusCode as [FollowUpInfo] FROM FollowUpStatusInfo WHERE Description = 'Need Approval')";

            using (var db = GetAABDB())
            {
                return db.Fetch<dynamic>(query);
            }
        }

        public static bool IsPushNotifExist(string Orderno, string UserID)
        {
            var db = GetAABMobileDB();
            string query = "SELECT * FROM dbo.PushNotificationOtosales WHERE Data LIKE '%{0}%' AND UserID = @0";
            List<dynamic> ls = db.Fetch<dynamic>(string.Format(query,Orderno), UserID);
            if (ls.Count > 0)
            {
                return true;
            }
            return false;
        }

        public static void InsertOtosalesErrorLog(string RefNo, string ActionName, string ErrorMessage, int AppId) {
            try
            {
                var db = GetAABMobileDB();
                string query = @"";
                switch (AppId)
                {
                    //1 -- OtosalesCreateOrder
                    //2 -- OtosalesApproval
                    case 1:
                        query = @";INSERT INTO GardaMobile.Otosales.CreateOrderLog
                                        ( ReferenceNo ,
                                          ErrorDate ,
                                          ActionName ,
                                          ErrorMessage
                                        )
                                VALUES  ( @0 , -- OrderNo - varchar(100)
                                          GETDATE() , -- ErrorDate - datetime
                                          @1 , -- ActionName - varchar(255)
                                          @2  -- ErrorMessage - varchar(max)
                                        )";
                        db.Execute(query
                            ,RefNo, ActionName, ErrorMessage);
                        break;
                    case 2:
                        query = @";INSERT INTO GardaMobile.Otosales.ApprovalLog
                                        ( ReferenceNo ,
                                          ErrorDate ,
                                          ActionName ,
                                          ErrorMessage
                                        )
                                VALUES  ( @0 , -- OrderNo - varchar(100)
                                          GETDATE() , -- ErrorDate - datetime
                                          @1 , -- ActionName - varchar(255)
                                          @2  -- ErrorMessage - varchar(max)
                                        )";
                        db.Execute(query
                            , RefNo, ActionName, ErrorMessage);
                        break;
                    default:
                        break;
                }
            }
            catch (Exception e)
            {

            }
        }

        public static List<string> GetParamValue(string ParamName) {
            string query = "";
            List<string> result = new List<string>();
            var db = GetAABMobileDB();
            try
            {
                query = @"SELECT ParamValue, ParamValue2 FROM dbo.ApplicationParameters
                        WHERE RowStatus = 1 AND ParamName = @0";
                List<dynamic> listVal = db.Fetch<dynamic>(query, ParamName);
                if (listVal.Count > 0)
                {
                    result.Add(listVal.First().ParamValue);
                    result.Add(listVal.First().ParamValue2);
                }
                else {
                    result.Add("");
                }
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}