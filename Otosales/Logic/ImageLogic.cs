﻿
using a2is.Framework.DataAccess;
using a2is.Framework.Monitoring;
using Otosales.Repository;
using Otosales.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace Otosales.Logic
{
    public class ImageLogic 
    {
        private static readonly a2isLogHelper logger = new a2isLogHelper();

        public static bool UploadImage(byte[] image, string imageFilename, string referenceImageType, int objectNo, string UserID, string referenceNo, out string outImageID)
        {
            string CurrentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                string qInsertImage = @"INSERT INTO dbo.Mst_Image
                                            ( Image_Id ,
                                              Image ,
                                              Description ,
                                              EntryUsr ,
                                              EntryDt ,
                                              Thumbnail ,
                                              Status ,
                                              File_Name ,
                                              File_Size
                                            )
                                    VALUES  ( @0 , -- Image_Id - char(12)
                                              @1 , -- Image - image
                                              @2 , -- Description - varchar(255)
                                              @3 , -- EntryUsr - char(5)
                                              GETDATE() , -- EntryDt - datetime
                                              @4 , -- Thumbnail - image
                                              0 , -- Status - smallint
                                              @5 , -- File_Name - varchar(512)
                                              @6 -- File_Size - int
                                    ) ";
                string qInsertDtlImage = @"INSERT INTO dbo.Dtl_Image
                                                ( Reference_No ,
                                                  Reference_Type ,
                                                  Number ,
                                                  Object_No ,
                                                  Image_Id ,
                                                  EntryUsr ,
                                                  EntryDt
                                                )
                                        VALUES  ( @0 , -- Reference_No - varchar(20)
                                                  @1 , -- Reference_Type - char(6)
                                                  @2 , -- Number - int
                                                  @3 , -- Object_No - int
                                                  @4 , -- Image_Id - char(12)
                                                  @5 , -- EntryUsr - char(5)
                                                  GETDate()  -- EntryDt - datetime
                                                )";
                List<bool> listResult = new List<bool>();
                string referenceNoTemp = referenceNo;

                int newNumber = 0;
                if (!string.IsNullOrEmpty(referenceNo))
                    newNumber = GetNewNumberImage(referenceNo);

                string imageID = CommonLogic.GetImageID();
                if (string.IsNullOrEmpty(referenceNoTemp))
                    referenceNoTemp = "TempRef-" + imageID;
                byte[] byteImgThum = null;
                byte[] byteImg = null;

                byte[] imageFileCode = image;
                byteImgThum = EncodeToAAB2000ImageBytes(getResizedImage(imageFileCode, 95, 71));
                byteImg = EncodeToAAB2000ImageBytes(getResizedImage(imageFileCode));


                using (var db = RepositoryBase.Geta2isImageDB())
                {
                    var mst = 0;
                    mst = db.Execute(qInsertImage,
                                            imageID,
                                            byteImg,
                                            imageFilename + "(" + referenceImageType + ")",
                                            UserID,
                                            byteImgThum,
                                            imageFilename,
                                            byteImg.Length);
                    if (mst > 0)
                    {
                        var dtl = db.Execute(qInsertDtlImage,
                                                referenceNoTemp,
                                                referenceImageType,
                                                newNumber,
                                                objectNo,
                                                imageID,
                                                UserID);
                        if (dtl > 0)
                        {
                            listResult.Add(true);
                        }
                        newNumber++;
                    }
                }
                //referenceNoOut = referenceNoTemp;
                outImageID = imageID;
                if (!listResult.Contains(false))
                {
                    return true;
                }
                else
                    return false;

            }
            catch (Exception e)
            {
                logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.StackTrace));
                throw e;
            }        }
        
        public static int GetNewNumberImage(string refNo)
        {
            int ret = 0;
            using (var db = RepositoryBase.Geta2isImageDB())
            {
                string q = @"SELECT TOP 1 Number FROM dbo.Dtl_Image WHERE Reference_No = @0 ORDER BY Number DESC";
                var dtl = db.Fetch<dynamic>(q, refNo);
                if (dtl.Count() > 0) {
                    int l = dtl.FirstOrDefault().Number != null ? dtl.FirstOrDefault().Number : 0;
                    ret = l+1;
                }
            }
            return ret;
        }
        
        public static bool DeleteImage(string imgID)
        {
            string qDeleteMstImg = @" DELETE dbo.Mst_Image WHERE Image_Id = @0";
            string qDeleteDtlImg = @" DELETE dbo.Dtl_Image WHERE Image_Id = @0";
            string qDeleteImage_Document = @" DELETE dbo.Image_Document WHERE Image_Id = @0";
            using (var db = RepositoryBase.Geta2isImageDB())
            {
                var dtl = db.Execute(qDeleteDtlImg, imgID);
                var mst = db.Execute(qDeleteMstImg, imgID);
                using (var db1 = RepositoryBase.GetAABDB())
                {
                    var file = db1.Execute(qDeleteImage_Document, imgID);
                }
                return true;
            }
        }

        public static bool UpdateReferenceImage(string referenceNo, string newReferenceNo, string UserID)
        {
            string qIUpdateDtl = @" UPDATE dbo.Dtl_Image SET Reference_No = @0 WHERE Reference_No = @1 ";
            string qUpdateMst = @" UPDATE  dbo.Mst_Image
                                    SET     UpdateDt = GETDATE() ,
                                            UpdateUsr = @1
                                    WHERE   Image_Id IN ( SELECT    Image_Id
                                                          FROM      Dtl_Image
                                                          WHERE     Reference_No = @0 )";
            using (var db = RepositoryBase.Geta2isImageDB())
            {
                var mst = db.Execute(qUpdateMst, referenceNo, UserID);
                var dtl = db.Execute(qIUpdateDtl, newReferenceNo, referenceNo);
                return true;
            }
        }

        public static dynamic GetImageListByRefNo(string referenceNo, int objNo)
        {
            string query = @"SELECT  RTRIM(ISNULL(b.Image_Id,'')) AS ImageId ,
                                    RTRIM(ISNULL(a.Reference_Type,'')) AS ImageType,
                                    RTRIM(ISNULL(b.[Description],'')) AS [Description]
                            FROM    dbo.Dtl_Image a
                                    INNER JOIN dbo.Mst_Image b ON b.Image_Id = a.Image_Id
                            WHERE   a.Reference_No = @0 AND a.Object_No = @1";
            using (var db = RepositoryBase.GetAABDB())
            {
                return db.Fetch<dynamic>(query, referenceNo, objNo);
            }
        }

        public static dynamic GetObjectImageList(string referenceNo, int objNo = 0)
        {
            //string query = @"SELECT  b.Image_Id AS ImageId ,
            //                        a.Reference_Type AS ImageType,
            //                        b.[Description] AS [Description]
            //                FROM    dbo.Dtl_Image a
            //                        INNER JOIN dbo.Mst_Image b ON b.Image_Id = a.Image_Id
            //                WHERE   a.Reference_No = @0 AND a.Reference_Type IN (SELECT code FROM dbo.Mst_General WHERE type = 'IPO' AND status = 1)";
            string query = @"SELECT RTRIM(ISNULL(b.Image_Id,'')) AS ImageId ,
                                    RTRIM(ISNULL(a.Reference_Type,'')) AS ImageType ,
                                    RTRIM(ISNULL(b.[Description],'')) AS Description
                            FROM dbo.Dtl_Image a
                                    INNER JOIN dbo.Mst_Image b ON b.Image_Id = a.Image_Id
                            WHERE a.Reference_No = @0
                                    AND a.Reference_Type IN (SELECT    code
                                                              FROM      dbo.Mst_General
                                                              WHERE (type = 'IPO' OR(type = 'IPF' AND code = 'SPPAKB'))
                                                                        AND status = 1
											                            )";
            using (var db = RepositoryBase.GetAABDB())
            {
                if (objNo > 0)
                {
                    query += " AND (Object_No = @1 OR Reference_Type='SPPAKB')";
                    return db.Fetch<dynamic>(query, referenceNo, objNo);
                }
                else
                    return db.Fetch<dynamic>(query, referenceNo);
            }
        }

        public static dynamic GetImageWhenBackToObjectList(string referenceNo, int objNo)
        {
            string query = @"SELECT RTRIM(ISNULL(b.Image_Id,'')) AS ImageId
                            FROM dbo.Dtl_Image a
                                    INNER JOIN dbo.Mst_Image b ON b.Image_Id = a.Image_Id
                            WHERE a.Reference_No = @0
                                    AND a.Reference_Type IN (SELECT    code
                                                                FROM      dbo.Mst_General
                                                                WHERE type = 'IPO' AND status = 1 )
                            AND Object_No = @1 ";
            using (var db = RepositoryBase.GetAABDB())
            {
                return db.Fetch<dynamic>(query, referenceNo, objNo);
            }
        }

        public static dynamic GetImageType(string referenceNo, string ImageID)
        {
           
            string query = @"SELECT Top 1
                                    RTRIM(a.Reference_Type) AS ImageType 
                            FROM dbo.Dtl_Image a                                    
                            WHERE a.Reference_No = @0
                                    AND a.Image_ID =@1";
            using (var db = RepositoryBase.GetAABDB())
            {
                    return db.Query<string>(query, referenceNo, ImageID).SingleOrDefault();                
            }
        }

        public static dynamic GetPolicyImageList(string referenceNo)
        {
            string query = @"SELECT  RTRIM(ISNULL(b.Image_Id,'')) AS ImageId ,
                                    RTRIM(ISNULL(a.Reference_Type,'')) AS ImageType,
                                    RTRIM(ISNULL(b.[Description],'')) AS Description
                            FROM    dbo.Dtl_Image a
                                    INNER JOIN dbo.Mst_Image b ON b.Image_Id = a.Image_Id
                            WHERE   a.Reference_No = @0 AND a.Reference_Type IN (SELECT code FROM dbo.Mst_General WHERE type = 'IPF' AND status = 1)";
            using (var db = RepositoryBase.GetAABDB())
            {
                return db.Fetch<dynamic>(query, referenceNo);
            }
        }

        public static dynamic GetPersonalImageList(string referenceNo)
        {
            string query = @"SELECT  RTRIM(ISNULL(b.Image_Id,'')) AS ImageId ,
                                    RTRIM(ISNULL(a.Reference_Type,'')) AS ImageType,
                                    RTRIM(ISNULL(b.[Description],'')) AS [Description]
                            FROM    dbo.Dtl_Image a
                                    INNER JOIN dbo.Mst_Image b ON b.Image_Id = a.Image_Id
                            WHERE   a.Reference_No = @0 AND a.Reference_Type IN (SELECT code FROM dbo.Mst_General WHERE type = 'IMP' AND status = 1)";
            using (var db = RepositoryBase.GetAABDB())
            {
                return db.Fetch<dynamic>(query, referenceNo);
            }
        }

        public static dynamic GetCorporateImageList(string referenceNo)
        {
            string query = @"SELECT  RTRIM(ISNULL(b.Image_Id,'')) AS ImageId ,
                                    RTRIM(ISNULL(a.Reference_Type,'')) AS ImageType,
                                    RTRIM(ISNULL(b.[Description],'')) AS [Description]
                            FROM    dbo.Dtl_Image a
                                    INNER JOIN dbo.Mst_Image b ON b.Image_Id = a.Image_Id
                            WHERE   a.Reference_No = @0 AND a.Reference_Type IN (SELECT code FROM dbo.Mst_General WHERE type = 'IMC' AND status = 1)";
            using (var db = RepositoryBase.GetAABDB())
            {
                return db.Fetch<dynamic>(query, referenceNo);
            }
        }

        public static dynamic GetRelatedImage(string referenceNo, string ImageType)
        {
            string query = @"SELECT  RTRIM(ISNULL(b.Image_Id,'')) AS ImageId ,
                                RTRIM(ISNULL(a.Reference_Type,'')) AS ImageType ,
                                RTRIM(ISNULL(b.[Description],'')) AS [Description]
                        FROM    dbo.Dtl_Image a
                                INNER JOIN dbo.Mst_Image b ON b.Image_Id = a.Image_Id
                        WHERE   a.Reference_No = @0
                                AND a.Reference_Type IN ( 
                                                            SELECT  DISTINCT mg2.Code
                                                            FROM    dbo.Mst_General AS mg
		                                                            INNER Join dbo.Mst_General AS mg2 ON mg2.type = mg.type
                                                            WHERE   mg.code = @1
                                                                    AND mg.Status = 1
                                                                    AND mg2.Status = 1
                                                            )";
            using (var db = RepositoryBase.GetAABDB())
            {
                return db.Fetch<dynamic>(query, referenceNo, ImageType);
            }
        }

        private static byte[] getResizedImage(Byte[] data, int width=0, int height=0)
        {
            Bitmap imgIn = null;
            Bitmap imgOut = null;
            try
            {
                using (var ms = new MemoryStream(data))
                {
                    imgIn = new Bitmap(ms);
                }

                double y = imgIn.Height;
                double x = imgIn.Width;

                double factor = 1;
                if (width > 0)
                {
                    factor = width / x;
                }
                else if (height > 0)
                {
                    factor = height / y;
                }
                System.IO.MemoryStream outStream = new System.IO.MemoryStream();
                imgOut = new Bitmap((int)(x * factor), (int)(y * factor));

                // Set DPI of image (xDpi, yDpi)
                imgOut.SetResolution(72, 72);

                Graphics g = Graphics.FromImage(imgOut);
                g.Clear(Color.White);
                g.DrawImage(imgIn, new Rectangle(0, 0, (int)(factor * x), (int)(factor * y)),
                  new Rectangle(0, 0, (int)x, (int)y), GraphicsUnit.Pixel);

                //imgOut.Save(outStream, getImageFormat(""));
                imgOut.Save(outStream, ImageFormat.Jpeg);
                return outStream.ToArray();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
                imgIn.Dispose();
                imgOut.Dispose();
                imgIn = null;
                imgOut = null;
            }

        }

        private static byte[] EncodeToAAB2000ImageBytes(byte[] sourceBytes)
        {
            byte[] encodedBytes = Encoding.Convert(Encoding.Default, Encoding.Unicode, sourceBytes);
            return encodedBytes;
        }

        public static List<SurveyImage> GetTransferImageSurvey(string Chasis, string Engine)
        {
            List<SurveyImage> result = new List<SurveyImage>();
            string strQuery = @"SELECT  a.ChassisEngine AS ChassisEngine ,
                                    b.FileName AS FileName ,
                                    b.ImageType AS ImageType
                            FROM    AdditionalOrderAcquisitionSurvey a
                                    INNER JOIN dbo.AdditionalOrderAcquisitionSurveyImage b ON b.SurveyID = a.ID
                            WHERE   ChassisEngine = @0";

            using (a2isDBHelper.Database db = new a2isDBHelper.Database("a2isMobileSurveyDB"))
            {
                var check1 = db.Fetch<SurveyImage>(strQuery, Chasis);
                if (check1.Count <= 0)
                {
                    var check2 = db.Fetch<SurveyImage>(strQuery, Engine);
                    if (check2.Count <= 0)
                        return result;
                    else
                        result = check2;
                }
                else
                    result = check1;
            }
            return result;
        }

        public static  bool CheckPDF(string imageID, out byte[] pdfByte)
        {
            pdfByte = new byte[] { };
            bool result = false;
            string strQuery = @"SELECT  TOP 1 CASE WHEN ( Image IS NULL ) THEN 1
                                             ELSE 0
                                        END AS IsPDF,
                                        Image AS ImageByte
                                FROM    Mst_Image
                                WHERE   Image_Id = @0";
            string strQueryPdf = @"SELECT TOP 1
                                            Document AS Document
                                    FROM    Image_Document
                                    WHERE   Image_Id = @0
                                            AND Status = 1
                                            AND Row_Status = 0";

            using (var db = RepositoryBase.Geta2isImageDB())
            {
                var check = db.Fetch<dynamic>(strQuery, imageID);
                if (check.Count > 0) {
                    if (check[0].IsPDF == 1) {
                        result = true;
                        using (var db1 = RepositoryBase.GetAABDB())
                        {
                            var p = db1.Fetch<dynamic>(strQueryPdf, imageID);
                            if (p.Count > 0) {
                                pdfByte = p[0].Document;
                            }
                        }
                    }
                }
            }
            return result;
        }

        private static byte[] GetDefaultPdfThumbnail() {
            byte[] data = null;
            try
            {
                string pathConfigPdf = System.Configuration.ConfigurationManager.AppSettings["PDFPathDefault"];
                string path = System.IO.Path.Combine(pathConfigPdf, "PDF.png");
                FileInfo fInfo = new FileInfo(path);
                long numBytes = fInfo.Length;
                FileStream fStream = new FileStream(path, FileMode.Open, FileAccess.Read);

                BinaryReader br = new BinaryReader(fStream);
                data = br.ReadBytes((int)numBytes);
            }
            catch (Exception e) {
                logger.Debug("Failed to load PDF.png!" + e);
            }
            return data;
        }

        #region Copy Image Otosales

        public static string GetMappedReferenceTypeImage(string imageTypeOtosales, bool IsSurvey)
        {
            switch (imageTypeOtosales)
            {
                case "IdentityCard": return IsSurvey ? ReferenceTypeImage.KTPSURVEY : ReferenceTypeImage.KTP;
                case "STNK": return ReferenceTypeImage.STNK;
                case "SPPAKB": return ReferenceTypeImage.SPPAKB;
                case "BSTB1": return ReferenceTypeImage.BSTB;
                case "NPWP": return ReferenceTypeImage.NPWP;
                case "SIUP": return ReferenceTypeImage.SIUP;
                case "Faktur": return ReferenceTypeImage.FAKTUR;
                case "KonfirmasiCust": return ReferenceTypeImage.CONFIRMATIONCUSTOMER;
                case "DocNSA1": return ReferenceTypeImage.NSA;
                case "DocNSA2": return ReferenceTypeImage.NSA;
                case "DocNSA3": return ReferenceTypeImage.NSA;
                case "DocNSA4": return ReferenceTypeImage.NSA;
                case "DocNSA5": return ReferenceTypeImage.NSA;
                case "DocNSA6": return ReferenceTypeImage.NSA;
                case "DocRep": return ReferenceTypeImage.DOCREP;
                case "DocPendukung": return ReferenceTypeImage.DOCPENDUKUNG;
                case "BuktiBayar": return ReferenceTypeImage.BUKTIBAYAR;
                case "BuktiBayar2": return ReferenceTypeImage.BUKTIBAYAR;
                case "BuktiBayar3": return ReferenceTypeImage.BUKTIBAYAR;
                case "BuktiBayar4": return ReferenceTypeImage.BUKTIBAYAR;
                case "BuktiBayar5": return ReferenceTypeImage.BUKTIBAYAR;
                case "RenewalNotice": return ReferenceTypeImage.RENEWALNOTICE;
                case "Survey": return ReferenceTypeImage.CHECKLISTSURVEY;
                default: return string.Empty;
            }
        }

        public static string GetRevertMappedReferenceTypeImage(string imageTypeAAB)
        {
            switch (imageTypeAAB)
            {
                case ReferenceTypeImage.KTP: return "IdentityCard";
                case ReferenceTypeImage.KTPSURVEY: return "IdentityCard";
                case ReferenceTypeImage.STNK: return "STNK";
                case ReferenceTypeImage.SPPAKB: return "SPPAKB";
                case ReferenceTypeImage.BSTB: return "BSTB1";
                case ReferenceTypeImage.NPWP: return "NPWP";
                case ReferenceTypeImage.SIUP: return "SIUP";
                case ReferenceTypeImage.FAKTUR: return "Faktur";
                case ReferenceTypeImage.CONFIRMATIONCUSTOMER: return "KonfirmasiCust";
                case ReferenceTypeImage.DOCREP: return "DocRep";
                case ReferenceTypeImage.DOCPENDUKUNG: return "DocPendukung";
                case ReferenceTypeImage.RENEWALNOTICE: return "RenewalNotice";
                case ReferenceTypeImage.CHECKLISTSURVEY: return "Survey";
                default: return string.Empty;
            }
        }

        public static void CopyImageDataToMstImage(string followUpNo, string prospectId, string orderNo)
        {
            string UserID = CommonLogic.GetSettingUserID();
            //UPLOAD IMAGE

            string referenceNo = "";
            List<dynamic> followUpImages = new List<dynamic>();
            if (!string.IsNullOrEmpty(prospectId))
            {
                followUpImages = GetNotUploadedFollowUpPersonalImages(followUpNo);
                referenceNo = prospectId;
            }
            else {
                followUpImages = GetNotUploadedFollowUpOrderImages(followUpNo);
                referenceNo = orderNo;
            }
            foreach (var fui in followUpImages)
            {
                //tambahin dlu validasi
                string referenceImageType = GetMappedReferenceTypeImage(fui.ImageType,false);
                string outImageID = null;
                if (ImageLogic.UploadImage(fui.Data, fui.ImageName, referenceImageType, 1, UserID, referenceNo, out outImageID) && outImageID != null)
                {
                    // setelah upload, update ke AABMobile.dbo.Mst_Image kolom CoreImage_Id, 
                    UpdateImageData(fui.ImageID, outImageID);
                }
            }
            DeleteUnusedImages(followUpNo);
        }

        public static int UpdateImageData(string imageID, string CoreImage_ID)
        {
            string qUpdate = @"UPDATE ImageData SET LastUpdatedTime = GETDATE(), CoreImage_ID = @0 WHERE ImageID = @1";
            using (var db = RepositoryBase.GetAABMobileDB())
            {
                return db.Execute(qUpdate, CoreImage_ID, imageID);
            }
        }
        
        public static List<dynamic> GetNotUploadedFollowUpPersonalImages(string followUpNo)
        {
            string q = @"
SELECT a.FollowUpNo, id.ImageID, a.ImageName, a.ImageType, id.Data FROM 
(
SELECT 
	FollowUpNo,ImageName, ImageType
FROM 
(
select fu.FollowUpNo, IdentityCard, NPWP, SIUP
from FollowUp fu WITH (NOLOCK)
LEFT JOIN ProspectCompany pcm WITH (NOLOCK) ON fu.CustID = pcm.CustID
where fu.followupno = @0
)a 
UNPIVOT (
	ImageName FOR ImageType IN (
		IdentityCard, NPWP, SIUP
	)
) unpvt
where ImageName <> ''
) a 
INNER JOIN ImageData id WITH (NOLOCK) on ImageName = id.PathFile 
and CoreImage_ID is null ";
            using (var db = RepositoryBase.GetAABMobileDB())
            {
                return db.Fetch<dynamic>(q, followUpNo);
            }
        }

        public static List<dynamic> GetNotUploadedFollowUpOrderImages(string followUpNo)
        {
            string q = @"
SELECT a.FollowUpNo, id.ImageID, a.ImageName, a.ImageType, id.Data FROM 
(
SELECT 
	FollowUpNo,ImageName, ImageType
FROM 
(
select fu.FollowUpNo, STNK, SPPAKB, BSTB1, Faktur, KonfirmasiCust, DocNSA1, DocNSA2, DocNSA3, DocRep, BuktiBayar, DocPendukung, RenewalNotice, Survey, BuktiBayar2, BuktiBayar3, BuktiBayar4, BuktiBayar5, DocNSA4, DocNSA5, DocNSA6 --DocRep, CustomerConfirmation, *
from FollowUp fu WITH (NOLOCK)
LEFT JOIN ProspectCompany pcm WITH (NOLOCK) ON fu.CustID = pcm.CustID
where fu.followupno = @0
)a 
UNPIVOT (
	ImageName FOR ImageType IN (
		STNK, SPPAKB, BSTB1, Faktur, KonfirmasiCust, DocNSA1, DocNSA2, DocNSA3, DocRep, BuktiBayar, DocPendukung, RenewalNotice, Survey, BuktiBayar2, BuktiBayar3, BuktiBayar4, BuktiBayar5, DocNSA4, DocNSA5, DocNSA6
	)
) unpvt
where ImageName <> ''
) a 
INNER JOIN ImageData id WITH (NOLOCK) on ImageName = id.PathFile 
and CoreImage_ID is null ";
            using (var db = RepositoryBase.GetAABMobileDB())
            {
                return db.Fetch<dynamic>(q, followUpNo);
            }
        }

        public static bool DeleteUnusedImages(string FollowUpNo) {
            var AABDB = RepositoryBase.GetAABDB();
            var MobileDB = RepositoryBase.GetAABMobileDB();
            string query = @"";
            int res = 0;
            string CurrentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                query = @"SELECT PolicyOrderNo,ProspectID,CustIDAAB FROM dbo.OrderSimulation os
                        INNER JOIN dbo.ProspectCustomer p
                        ON p.CustID = os.CustID
                        WHERE os.FollowUpNo = @0
                        AND os.ApplyF = 1 AND os.RowStatus = 1";
                List<dynamic> data = MobileDB.Fetch<dynamic>(query, FollowUpNo);
                if (data.Count > 0) {
                    string PolicyOrderNo = data.First().PolicyOrderNo;
                    string ProspectID = data.First().ProspectID;
                    string CustID = data.First().CustIDAAB;
                    string IDcust = "";
                    if (!string.IsNullOrEmpty(CustID) && !string.IsNullOrWhiteSpace(CustID)) {
                        IDcust = CustID;
                    }
                    else {
                        IDcust = ProspectID;
                    }
                    if (!string.IsNullOrEmpty(PolicyOrderNo)) {
                        query = @"SELECT DISTINCT di.Image_Id, id.RowStatus
                                FROM dbo.Dtl_Image di
                                INNER JOIN dbo.Mst_Image 
                                mi ON mi.Image_Id = di.Image_Id
							    INNER JOIN BEYONDMOSS.AABMobile.dbo.ImageData id
							    ON id.CoreImage_ID = di.Image_Id
                                WHERE RowStatus = 0 
								AND (di.Reference_No =@0 OR di.Reference_No =@1)";
                        List<dynamic> ListImage = AABDB.Fetch<dynamic>(query, PolicyOrderNo, IDcust);
                        foreach (dynamic item in ListImage)
                        {
                            query = @";
                                DELETE FROM AAB_Image.dbo.Dtl_Image WHERE Image_Id = @0
                                DELETE FROM AAB_Image.dbo.Mst_Image WHERE Image_Id = @0";
                            AABDB.Execute(query, item.Image_Id);
                            res++;
                        }
                    }
                }
                return res > 0;
            }
            catch (Exception e)
            {
                logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.Message));
                throw e;
            }
        }

        public static void UploadImageRenewal(string FollowUpNo) {
            string CurrentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                var MobileDB = RepositoryBase.GetAABMobileDB();
                var AABDB = RepositoryBase.GetAABDB();
                string qGetProspect = @"SELECT CustIDAAB, ProspectID FROM dbo.ProspectCustomer pc
                                    INNER JOIN dbo.FollowUp f ON f.CustID = pc.CustID
                                    WHERE FollowUpNo = @0";
                List<dynamic> ps = MobileDB.Fetch<dynamic>(qGetProspect, FollowUpNo);
                if (ps.Count > 0)
                {
                    string CustIDAAB = ps.First().CustIDAAB;
                    string ProspectID = ps.First().ProspectID;
                    string qOldImageAAB = @"SELECT di.Image_Id, Reference_Type FROM dbo.Mst_Image mi 
                                        INNER JOIN dbo.Dtl_Image di ON di.Image_Id = mi.Image_Id
                                        WHERE di.Reference_No = @0";
                    List<dynamic> imgList = GetNotUploadedFollowUpPersonalImages(FollowUpNo);

                    List<dynamic> oldimg = new List<dynamic>();
                    string qDelete = @";
                                DELETE FROM AAB_Image.dbo.Dtl_Image WHERE Image_Id = @0
                                DELETE FROM AAB_Image.dbo.Mst_Image WHERE Image_Id = @0";
                    if (!string.IsNullOrEmpty(CustIDAAB))
                    {
                        oldimg = AABDB.Fetch<dynamic>(qOldImageAAB, CustIDAAB);
                        if (oldimg.Count > 0)
                        {
                            foreach (dynamic old in oldimg)
                            {
                                foreach (dynamic nw in imgList)
                                {
                                    string oldtype = Convert.ToString(old.Reference_Type) == "" ? "" : Convert.ToString(old.Reference_Type).Trim();
                                    string newtype = GetMappedReferenceTypeImage(nw.ImageType, false);
                                    if (oldtype.Equals(newtype))
                                    {
                                        AABDB.Execute(qDelete, old.Image_Id);
                                    }
                                }
                            }
                        }
                        ImageLogic.CopyImageDataToMstImage(FollowUpNo, CustIDAAB, "");
                    }
                    else if (!string.IsNullOrEmpty(ProspectID))
                    {
                        oldimg = AABDB.Fetch<dynamic>(qOldImageAAB, ProspectID);
                        if (oldimg.Count > 0)
                        {
                            foreach (dynamic old in oldimg)
                            {
                                foreach (dynamic nw in imgList)
                                {
                                    string oldtype = Convert.ToString(old.Reference_Type) == "" ? "" : Convert.ToString(old.Reference_Type).Trim();
                                    string newtype = GetMappedReferenceTypeImage(nw.ImageType, false);
                                    if (oldtype.Equals(newtype))
                                    {
                                        AABDB.Execute(qDelete, old.Image_Id);
                                    }
                                }
                            }
                        }
                        ImageLogic.CopyImageDataToMstImage(FollowUpNo, ProspectID, "");
                    }
                }

            }
            catch (Exception e)
            {
                logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.Message));
                throw e;
            }
        }

        public static byte[] DecodeFromAAB2000ImageBytes(byte[] sourceBytes)
        {
            byte[] encodedBytes = Encoding.Convert(Encoding.Unicode, Encoding.Default, sourceBytes);
            return encodedBytes;
        }

        #endregion


    }
}