﻿using a2is.Framework.Monitoring;
using Otosales.Models;
using Otosales.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otosales.Logic
{
    public class OrdLogic
    {
        private static a2isLogHelper logger = new a2isLogHelper();
        private static string EntryUser = CommonLogic.GetSettingUserID();

        public static bool UpdateAABOrderData(string OrderNo)
        {
            string CurrentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                OrdRepository OR = new OrdRepository();
                return OR.UpdateOrderData(OrderNo);
            }
            catch (Exception e)
            {
                logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.Message));
                return false;
            }
        }

        public static int UpdateOrderMobile(string OrderNo)
        {
            string CurrentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                OrdRepository OR = new OrdRepository();
                return OR.UpdateOrderMobile(OrderNo);
            }
            catch (Exception e)
            {
                logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.Message));
                return 0;
            }
        }

        public static decimal GetTotalPaidPayment(string PolicyOrderNo, bool IsNeedSettlement)
        {
            string CurrentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            decimal totalBayar = 0;
            try
            {
                OrdRepository OR = new OrdRepository();
                OrderPaymentCustom objNonVA = OR.GetTotalBayarNonVA(PolicyOrderNo);
                OrderPaymentCustom objVA = OR.GetTotalBayarVA(PolicyOrderNo);
                List<OrderPaymentCustom> listPayment = new List<OrderPaymentCustom>();
                if (IsNeedSettlement)
                {
                    using (var db = RepositoryBase.GetAABMobileDB())
                    {
                        string VoucheRO = db.ExecuteScalar<string>(@";
DECLARE @@VRO VARCHAR(100) = ''
SELECT @@VRO = VoucherRO FROM dbo.VoucherPayment WHERE OrderNo = @0
SELECT @@VRO", PolicyOrderNo);
                        if (!string.IsNullOrEmpty(VoucheRO) && !string.IsNullOrWhiteSpace(VoucheRO))
                        {
                            if (OR.CheckSettlement(VoucheRO))
                            {
                                if (objNonVA != null) { listPayment.Add(objNonVA); }
                            }
                        }
                    }
                }
                else {
                    if (objNonVA != null) { listPayment.Add(objNonVA); }
                }

                if (objVA != null) { listPayment.Add(objVA); }
                totalBayar = listPayment.Sum(xx => xx.TotalBayar);
            }
            catch (Exception e)
            {
                totalBayar = 0;
                logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.Message));
            }
            return totalBayar;
        }
        public static bool CekSettlement(string VoucherNo)
        {
            string CurrentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            bool Settled = false;
            OrdRepository OR = new OrdRepository();
            Settled = OR.CheckSettlement(VoucherNo);
            return Settled;
        }
    }
}
