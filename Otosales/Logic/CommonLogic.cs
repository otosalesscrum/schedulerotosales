﻿using Otosales.Repository;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Configuration;
using static Otosales.Models.Constant;
using Otosales.Models;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Otosales.Logic
{
    public class CommonLogic
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #region PushNotification Otosales
        public static void CreatePushNotification(PushNotificationCategory category, string policyOrderNo, string updateBy = "")
        {
            var icon = ConfigurationManager.AppSettings["IconUrl"];
            string subject = "Otosales";
            string bodyMessage = "";
            var data = JsonConvert.SerializeObject(new { PolicyOrderNo = policyOrderNo });
            string clickAction = "", fcmToken = "";
            dynamic token;
            //tasklist
            switch (category)
            {
                case PushNotificationCategory.NEEDAPPROVALCOMMISSIONSAMEDAY:
                    string userIdCommissionSameDay = CommonRepository.GetSalesOfficerID(policyOrderNo);
                    if (!string.IsNullOrEmpty(userIdCommissionSameDay)) {
                        token = CommonRepository.GetFCMToken(userIdCommissionSameDay);
                        fcmToken = token != null ? token.DeviceToken : null;
                        if (fcmToken != null) {
                            bodyMessage = "New Data Need Approval!";
                            clickAction = "orderapproval";
                            if (!CommonRepository.IsPushNotifExist(policyOrderNo, userIdCommissionSameDay)) {
                                CommonRepository.InsertPushNotification(userIdCommissionSameDay, fcmToken, subject, bodyMessage, data, clickAction);
                            }
                        }
                    }
                    break;
                case PushNotificationCategory.NEEDAPPROVALADJUSTMENT:
                    string userIdOrder = CommonRepository.GetSalesOfficerID(policyOrderNo);
                    if (!string.IsNullOrEmpty(userIdOrder)) {
                        List<dynamic> superiors = CommonRepository.GetSuperior(userIdOrder);
                        if (superiors != null)
                            foreach (var item in superiors)
                            {
                                token = CommonRepository.GetFCMToken(item.UserID);
                                fcmToken = token != null ? token.DeviceToken : null;
                                bodyMessage = "New Data Need Approval!";
                                clickAction = "orderapproval";
                                if (!string.IsNullOrEmpty(fcmToken))
                                    if (!CommonRepository.IsPushNotifExist(policyOrderNo, item.UserID))
                                    {
                                        CommonRepository.InsertPushNotification(item.UserID, fcmToken, subject, bodyMessage, data, clickAction);
                                    }
                            }
                    }
                    break;
                case PushNotificationCategory.NEEDAPPROVALNEXTLIMIT:
                    List<dynamic> users = CommonRepository.GetUserIDApprovalNextLimit(policyOrderNo);
                    if (users.Count>0)
                        foreach (var item in users)
                        {
                            token = CommonRepository.GetFCMToken(item.UserID);
                            fcmToken = token != null ? token.DeviceToken : null;
                            bodyMessage = "New Data Need Approval!";
                            clickAction = "orderapproval";
                            if (!string.IsNullOrEmpty(fcmToken))
                                if (!CommonRepository.IsPushNotifExist(policyOrderNo, item.UserID))
                                {
                                    CommonRepository.InsertPushNotification(item.UserID, fcmToken, subject, bodyMessage, data, clickAction);
                                }
                        }
                    break;
                case PushNotificationCategory.FOLLOWUPSTATUSCHANGEDBYOTHERS:
                    dynamic orderSimulation = CommonRepository.GetOrderSimulation(policyOrderNo);
                    dynamic prospectCustomer = CommonRepository.GetProspectCustomer(orderSimulation.CustID);
                    dynamic updateUserBranch = CommonRepository.GetSalesOfficerBranch(updateBy);
                    if (orderSimulation != null && orderSimulation.SalesOfficerID != null)
                    {
                        token = CommonRepository.GetFCMToken(orderSimulation.SalesOfficerID);
                        fcmToken = token != null ? token.DeviceToken : null;
                        data = JsonConvert.SerializeObject(new { CustID = orderSimulation.CustID, FollowUpNo = orderSimulation.FollowUpNo });
                    }
                    string OrderName = prospectCustomer.Name;
                    if (Convert.ToBoolean(prospectCustomer.isCompany))
                    {
                        dynamic prospectCompany = CommonRepository.GetProspectCompany(orderSimulation.CustID);
                        OrderName = prospectCompany.CompanyName;
                    }
                    string branch = updateUserBranch != null ? updateUserBranch.Description : null;
                    string updatebyName = updateUserBranch != null ? updateUserBranch.Name : null;
                    bodyMessage = string.Format("Order for {0} has been updated by {1} from {2}", (OrderName+"").Trim(), updatebyName, branch);
                    clickAction = "tasklist-details";
                    if ((!string.IsNullOrEmpty(fcmToken) && !string.IsNullOrEmpty(OrderName) && !string.IsNullOrEmpty(branch)) || (!string.IsNullOrEmpty(updatebyName)&& !string.IsNullOrEmpty(fcmToken)))
                        CommonRepository.InsertPushNotification(orderSimulation.SalesOfficerID, fcmToken, subject, bodyMessage, data, clickAction );
                    break;
            }

            //var icon = ConfigurationManager.AppSettings["IconUrl"];
            //var entryDate = DateTime.Now;
            //var updateDate = DateTime.Now;
            //bool compare = false;
            ////var userId = GetSettingUserID();
            //var userId = "bts";
            //var getUser = datacollect.getLoginUser(userId);
            ////var userToken = getUser.DeviceToken;
            //var userToken = "fwngGdVTBqQ:APA91bHwZHnY7zkWiY7uikHHo8zPuluWOmoeTN5qIlj4e8d5l3DbQLsdkFInvArFQxd7E_JFvr4fL5UPp836JXIrEMgiRsp6jGZWlK9Rzy6iknuk6WzBWk_KxhbMyADUJYQKGi6Rjz9M";
            //var subject = "";
            //var bodyMsg = "";
            //var clickAction = "";
            //var status = 0;
            ////var orderNo = GetOrderNo();
            //var orderNo = "67660f4d-7def-4708-85af-356ec747ba71";
            //var orderSimulation = datacollect.getOrderSimulation(orderNo);
            //var followUpNo = orderSimulation.FollowUpNo;
            ////var custId = orderSimulation.CustID;
            //var custId = "154a4367-473c-449e-89af-e6037de52d1d";
            //var data = "{\"CustID\": \"" + custId + "\",\"FollowUpNo\": \"" + followUpNo + "\"}";
            //List<dynamic> lists = null;


            //var salesOfficers = datacollect.getSalesOfficerApproval(orderNo);
            //switch (category)
            //{
            //    case FollowUpInfo.FollowUpSurveyResult:
            //        //TODO : cara buat push notification ?;

            //        subject = "Subject FollowUpSurveyResult";
            //        bodyMsg = "Body FollowUpSurveyResult";
            //        clickAction = "tasklist-details";
            //        compare = true;
            //        break;

            //    case FollowUpInfo.NeedDataRevision:

            //        subject = "Subject BackToAO";
            //        bodyMsg = "Body BackToAO";
            //        clickAction = "tasklist-details";
            //        compare = true;
            //        break;

            //    case FollowUpStatus.OrderRejectedStatusCode:

            //        subject = "Subject OrderRejectedStatusCode";
            //        bodyMsg = "Body OrderRejectedStatusCode";
            //        clickAction = "tasklist-details";
            //        compare = true;
            //        break;

            //    case FollowUpInfo.PolicyApprovedNeedDocRep:
            //        compare = String.Equals(salesOfficers.SalesOrderSimulation.ToString(), salesOfficers.SalesFollowUp.ToString())
            //                   ? false : true;
            //        subject = "Subject PolicyApprovedNeeddocRep";
            //        bodyMsg = "Policy " + salesOfficers.PolicyNo.ToString() 
            //                    + " has been updated by " 
            //                    + salesOfficers.SalesOrderSimulation.ToString() 
            //                    + " from " + salesOfficers.BranchCode.ToString();
            //        clickAction = "tasklist-details";
            //        compare = true;
            //        break;

            //    case FollowUpInfo.PolicyApproved:
            //        compare = String.Equals(salesOfficers.SalesOrderSimulation.ToString(), salesOfficers.SalesFollowUp.ToString()) 
            //                   ? false : true;
            //        subject = "Subject PolicyApproved";
            //        bodyMsg = "Policy " + salesOfficers.PolicyNo.ToString() 
            //                  + " has been updated by " 
            //                  + salesOfficers.SalesOrderSimulation.ToString() 
            //                  + " from " + salesOfficers.BranchCode.ToString();
            //        clickAction = "tasklist-details";
            //        break;

            //    case FollowUpInfo.NeedApproval:

            //        lists = datacollect.getNewDataNeedApproval();

            //        var icon = ConfigurationManager.AppSettings["IconUrl"];
            //        var entryDate = DateTime.Now;
            //        var updateDate = DateTime.Now;
            //        bool compare = false;
            //        //var userId = GetSettingUserID();
            //        var userId = "bts";
            //        var getUser = datacollect.getLoginUser(userId);
            //        //var userToken = getUser.DeviceToken;
            //        var userToken = "fwngGdVTBqQ:APA91bHwZHnY7zkWiY7uikHHo8zPuluWOmoeTN5qIlj4e8d5l3DbQLsdkFInvArFQxd7E_JFvr4fL5UPp836JXIrEMgiRsp6jGZWlK9Rzy6iknuk6WzBWk_KxhbMyADUJYQKGi6Rjz9M";
            //        var subject = "";
            //        var bodyMsg = "";
            //        var clickAction = "";
            //        var status = 0;
            //        //var orderNo = GetOrderNo();
            //        var orderNo = "67660f4d-7def-4708-85af-356ec747ba71";
            //        var orderSimulation = datacollect.getOrderSimulation(orderNo);
            //        var followUpNo = orderSimulation.FollowUpNo;
            //        //var custId = orderSimulation.CustID;
            //        var custId = "154a4367-473c-449e-89af-e6037de52d1d";
            //        var data = "{\"CustID\": \"" + custId + "\",\"FollowUpNo\": \"" + followUpNo + "\"}";

            //        foreach (var i in lists) {
            //            orderNo = i.OrderNumber;
            //            orderSimulation = datacollect.getOrderSimulation(orderNo);
            //        }

            //        subject = "New Data Need Approval";
            //        bodyMsg = "New Data Need Approval!";
            //        clickAction = "";
            //        break;

            //    default:
            //        break;
            //}

            //try {
            //    if (compare) {
            //        datacollect.InsertPushNotifSalesOfficer(userId, userToken, subject, bodyMsg, icon, data, status, clickAction, entryDate, updateDate);
            //    }
            //}
            //catch (Exception e) {
            //    Console.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + " : ERROR-SendPushNotifCommonLogic: " + e.Message);
            //    log.Error(e.Message);
            //    ResponseSuccess er = new ResponseSuccess();
            //    er.success = 0;
            //    er.failure = 1;
            //}


        }
        #endregion

   

        #region AppConfig
        private static string GetSettings(string name)
        {
            return ConfigurationManager.AppSettings[name];
        }

        public static string GetSettingAreaCodeNasional()
        {
            return GetSettings("AreaCodeNasional");
        }

        public static string GetSettingUserID()
        {
            return GetSettings("UserID");
        }

        public static string GetSettingAgencySalesmanId()
        {
            return GetSettings("AgencySalesmanId");
        }
        public static string GetSettingAgencyBranchId()
        {
            return GetSettings("AgencyBranchId");
        }
        public static string GetSettingNasionalGeoAreaCode()
        {
            return GetSettings("NasionalGeoAreaCode");
        }
        public static string GetSettingNasionalSubGeoAreaCode()
        {
            return GetSettings("NasionalSubGeoAreaCode");
        }
        public static string GetSettingNasionalRegionCode()
        {
            return GetSettings("NasionalRegionCode");
        }
        public static string GetSettingSurveyDalamZipCode()
        {
            return GetSettings("SurveyDalamZipCode");
        }

        public static string GetSettingAutoApprovalOrderURL()
        {
            return GetSettings("AutoApprovalOrderURL");
        }
        public static string GetSettingLoginURL()
        {
            return GetSettings("LoginURL");
        }
        public static string GetSettingLoginAcc()
        {
            return GetSettings("LoginAcc");
        }

        public static string GetSettingIsCallAPILoggingEnabled()
        {
            return GetSettings("IsCallAPILoggingEnabled");
        }
        public static string GetSettingExcludeOrderStatusNextLimit()
        {
            return GetSettings("ExcludeOrderStatusNextLimit");
        }
        public static string GetSettingDropOrderURL()
        {
            return GetSettings("DropOrderUrl");
        }
        public static string GetSettingOrderType()
        {
            return GetSettings("OrderType");
        }
        public static string GetSettingSubOrderType()
        {
            return GetSettings("SubOrderType");
        }
        public static string GetSettingQuotationLeadType()
        {
            return GetSettings("QuotationLeadType");
        }
        public static string GetSettingQuotationLeadTime()
        {
            return GetSettings("QuotationLeadTime");
        }
        #endregion

        public static string GetPolicyId()
        {
            string Code = "POLICY_ID";
            string PolicyID = CommonRepository.IDGenereation(Code, 12);
            return PolicyID;
        }

        public static string GetVANo()
        {
            return CommonRepository.GetBookingVANo();
        }
        
        public static string GetPolicyNo()
        {
            DateTime now = DateTime.Now;
            string yearnow = Convert.ToString(now.Year);
            string Code = string.Format("{0}{1}", "POLICY_NO", yearnow.Substring(yearnow.Length - 2));
            string TmpPolicyNo = CommonRepository.IDGenereation(Code, 7);
            string PolicyNo = string.Format("{0}{1}{2}", yearnow.Substring(yearnow.Length - 2), TmpPolicyNo, CheckDigit(TmpPolicyNo));
            return PolicyNo;
        }

        public static string GetSurveyNo()
        {
            DateTime now = DateTime.Now;
            string yearnow = Convert.ToString(now.Year);
            string Code = string.Format("{0}{1}", "SURVEY", yearnow.Substring(yearnow.Length - 2));
            string TmpSurveyNo = CommonRepository.IDGenereation(Code, 6);
            string SurveyNo = string.Format("{0}{1}", yearnow.Substring(yearnow.Length - 2), TmpSurveyNo);
            return SurveyNo;
        }

        public static string GetOrderNo()
        {
            DateTime now = DateTime.Now;
            string yearnow = Convert.ToString(now.Year);
            string Code = string.Format("{0}{1}", "ORDER", yearnow.Substring(yearnow.Length - 2));
            string TmpOrderNo = CommonRepository.IDGenereation(Code, 7);
            string OrderNo = string.Format("{0}{1}{2}", 1, yearnow.Substring(yearnow.Length - 2), TmpOrderNo);
            return OrderNo;

        }

        public static string GetTransactionNo()
        {
            DateTime now = DateTime.Now;
            string yearnow = Convert.ToString(now.Year);
            string Code = string.Format("{0}{1}", "TRANSACTION", yearnow.Substring(yearnow.Length - 2));
            string TmpTransNo = CommonRepository.IDGenereation(Code, 8);
            string TransNo = string.Format("{0}{1}{2}", yearnow.Substring(yearnow.Length - 2), TmpTransNo, CheckDigit(yearnow.Substring(yearnow.Length - 2)));
            return TransNo;
        }
        public static string GetProspectID()
        {
            string ProspectID = CommonRepository.IDGenereation("PROSPECT_ID", 11);
            return ProspectID;
        }
        public static string GetImageID()
        {
            string imageID = CommonRepository.IDGenereation("IMAGE", 12);
            return imageID;
        }
        public static string IsNA(string value)
        {
            return string.IsNullOrWhiteSpace(value) ? "" : value.TrimEnd();
        }
        public static bool IsNA(bool value)
        {
            //return string.IsNullOrEmpty(value.ToString()) == false ? false : value;
            return value;
        }
        public static int IsNA(int value)
        {
            return string.IsNullOrEmpty(value.ToString()) ? 0 : value;
        }
        public static decimal IsNA(decimal value)
        {
            return string.IsNullOrEmpty(value.ToString()) ? 0 : value;
        }
        public static DateTime IsNA(DateTime value)
        {
            DateTime dev = default(DateTime);
            return value == null ? dev : value;
        }
        
        public static Nullable<DateTime> IsValidSurveyDate(Nullable<DateTime> date)
        {
            if (date == default(DateTime) || date == DateTime.MinValue || date == null)
            {
                return DateTime.Now;
            }
            else
            {
                return date;
            }
        }

        public static Nullable<DateTime> IsNullableDate(Nullable<DateTime> date)
        {
            if (date == default(DateTime) || date == DateTime.MinValue || date == null)
            {
                return null;
            }
            else
            {
                return date;
            }
        }

        private static string CheckDigit(string Number)
        {
            int lEven = 0, lOdd = 0, lNumber = 0, i = 0, retVal = 0;
            for (i = 0; i < (Number.Length - 1); i++)
            {
                string a = Number.Substring((Number.Length - 1) - i, 1);
                lNumber = string.IsNullOrWhiteSpace(a) ? 0 : Convert.ToInt32(a);
                if ((i + 1) % 2 == 0)
                    lEven = lEven + lNumber;
                else
                    lOdd = lOdd + lNumber;
            }
            lNumber = lOdd * 3 + lEven;
            lNumber = lNumber % 10;
            if (lNumber == 0)
                retVal = lNumber;
            else
                retVal = 10 - lNumber;
            return Convert.ToString(retVal);
        }

        
        private byte[] GetImageResize(Byte[] data, int width, int height)
        {
            Bitmap imgIn;
            using (var ms = new MemoryStream(data))
            {
                imgIn = new Bitmap(ms);
            }

            double y = imgIn.Height;
            double x = imgIn.Width;

            double factor = 1;
            if (width > 0)
            {
                factor = width / x;
            }
            else if (height > 0)
            {
                factor = height / y;
            }
            System.IO.MemoryStream outStream = new System.IO.MemoryStream();
            Bitmap imgOut = new Bitmap((int)(x * factor), (int)(y * factor));

            // Set DPI of image (xDpi, yDpi)
            imgOut.SetResolution(72, 72);

            Graphics g = Graphics.FromImage(imgOut);
            g.Clear(Color.White);
            g.DrawImage(imgIn, new Rectangle(0, 0, (int)(factor * x), (int)(factor * y)),
              new Rectangle(0, 0, (int)x, (int)y), GraphicsUnit.Pixel);

            //imgOut.Save(outStream, getImageFormat(""));
            imgOut.Save(outStream, ImageFormat.Jpeg);
            return outStream.ToArray();
        }

        public static void InsertErrorLog(string RefNo, string ActionName, string ErrorMessage, int AppId) {
            CommonRepository.InsertOtosalesErrorLog(RefNo, ActionName, ErrorMessage, AppId);
        }

        public static List<string> GetApplicationParametersValue(string ParamName)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                return CommonRepository.GetParamValue(ParamName);
            }
            catch (Exception e)
            {
                log.Error("Error at Function " + actionName + " :" + e.StackTrace);
                return new List<string>();
            }
        }

    }
}