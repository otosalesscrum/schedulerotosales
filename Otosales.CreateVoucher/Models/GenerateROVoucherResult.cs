﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otosales.CreateVoucher.Models
{
    public class GenerateROVoucherResult
    {
        public string VoucherNo { get; set; }
        public string ErrorMessage { get; set; }
    }
}
