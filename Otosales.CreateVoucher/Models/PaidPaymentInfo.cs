﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otosales.CreateVoucher.Models
{
    public class PaidPaymentInfo
    {
        public string PolicyOrderNo { get; set; }
        public string PolicyNo { get; set; }
        public decimal PaidAmount { get; set; }
        public string BankCode { get; set; }
        public string BankPaymentMethodCode { get; set; }
        public string AABAccountCode { get; set; }
        public string PaymentCategory { get; set; }
        public string PaymentMethodDescription { get; set; }
        public string BankName { get; set; }
        public DateTime TransactionDate { get; set; }
        public string PaymentType { get; set; }
        public int IsAuthorized { get; set; }
        public int InstallmentIntervalTimes { get; set; }
        public string PaymentStatus { get; set; }
    }
}
