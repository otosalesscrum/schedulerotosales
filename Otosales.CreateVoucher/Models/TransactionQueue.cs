﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otosales.CreateVoucher.Models
{
    public class TransactionQueue
    {
        public int TransactionQueueID { get; set; }
        public string RefferenceNo { get; set; }
        public string RefferenceTypeCode { get; set; }
        public string ProcessDate { get; set; }
        public int ProcessStatus { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string RowStatus { get; set; }

        //Additional
        public string PaymentCategory { get; set; }
        public Int64 OrderSimulationPaymentID { get; set; }
        public string PolicyOrderNo { get; set; }
        public string PolicyNo { get; set; }
        public decimal PaidAmount { get; set; }
        public string AABAccountCode { get; set; }
        public string VoucherRC { get; set; }
        public string VoucherPS { get; set; }
        public string VoucherRO { get; set; }
        public string PaymentType { get; set; }
        public string OrderStatus { get; set; }
        public string OrderNo { get; set; }
        public string BankName { get; set; }
        public string TransactionID { get; set; }
        public int InstallmentIntervalTimes { get; set; }
        public string BankCode { get; set; }
    }
}
