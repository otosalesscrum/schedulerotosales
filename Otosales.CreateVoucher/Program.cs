﻿using Otosales.CreateVoucher.Logic;
using Otosales.CreateVoucher.Models;
using Otosales.Models;
using Otosales.Repository;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otosales.CreateVoucher
{
    class Program
    {
        #region Properties
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger
  (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly string ApplicationType = ConfigurationManager.AppSettings["ApplicationType"];
        #endregion
        static void Main(string[] args)
        {
            #region Transaction Queue
            string qGetTransQueue = @"SELECT RefferenceNo TransactionID FROM dbo.TransactionQueue WHERE RefferenceTypeCode = @0 AND ProcessStatus = 0 AND RowStatus = 1";
            string cSQlTransactionQueue = @"
                SELECT  x.* ,
                        v.VoucherPS ,
                        v.VoucherRC ,
                        v.VoucherRO,
                        v.OrderNo
                FROM    ( SELECT    tq.CreatedDate ,
                                    a.TransactionID,
                                    a.OrderSimulationPaymentID ,
                                    b.PolicyOrderNo,
                                    b.PolicyNo ,
                                    CASE WHEN a.IsAuthorized = 1 THEN a.SettledAmount
                                            ELSE a.PaidAmount
                                    END PaidAmount ,
                                    c.AABAccountCode ,
                                    d.PaymentCategory ,
                                    c.PaymentType,
                                    tq.RefferenceTypeCode,
                                    h.BankCode, 
					                c.InstallmentIntervalTimes
                            FROM    AABMobile.dbo.TransactionQueue tq
                                    INNER JOIN AABMobile.dbo.OrderSimulationPayment a ON a.TransactionID = tq.RefferenceNo                                                              
                                    INNER JOIN AABMobile.dbo.ordersimulation b ON a.OrderNo = b.OrderNo
                                    INNER JOIN AABMobile.dbo.FollowUp fu ON b.FollowUpNo = fu.FollowUpNo
                                    INNER JOIN GODigital.BankPaymentMethod c ON a.BankPaymentMethodCode = c.BankPaymentMethodCode
                                    INNER JOIN godigital.PaymentMethod d ON c.PaymentMethodCode = d.PaymentMethodCode
                                    INNER JOIN GODigital.PaymentGatewayRequestStatusMapping e ON e.PaymentRequestParameterCode = c.PaymentRequestParameterCode
                                                                                AND e.IsAuthorizedF = a.IsAuthorized
                                                                                AND a.PaymentStatus = e.PaymentRequestStatusCode
                                    INNER JOIN GODigital.MstPaymentStatus f ON f.PaymentStatusCode = e.PaymentStatusCode
                                    
                                    INNER JOIN GODigital.Bank h ON h.BankCode = c.BankCode
                            WHERE     a.ResultCD = '0000'
                                    AND a.ResultCD2 = '0000'
                                    AND a.IsProceedByDBUrl = 1
                                    AND a.RowStatus = 1
                                    AND b.RowStatus = 1
									AND b.ApplyF = 1
                                    AND c.RowStatus = 0
                                    AND d.rowstatus = 0
                                    AND e.rowstatus = 0
                                    AND f.rowstatus = 0
                                    AND ( ( d.PaymentMethodCode = '04'
                                            AND ( COALESCE(a.ReceiptCode, '') <> ''
                                                    OR COALESCE(a.mRefNo, '') <> ''
                                                )
                                            AND a.IsAuthorized = 1
                                            )
                                            OR ( d.PaymentMethodCode = '01'
                                                AND COALESCE(a.AuthNo, '') <> ''
                                                AND ( ( c.PaymentType = 'F' )
                                                        OR ( c.PaymentType = 'I'
                                                            AND a.IsAuthorized = 1
                                                        )
                                                    )
                                                )
                                        )
                                    AND f.PaymentStatusCode IN ( 'PD', 'HA', 'NA' )
                                    AND COALESCE(a.CancelCode, '') = ''
                        ) x
                        LEFT JOIN AABMobile.dbo.VoucherPayment v ON x.TransactionID = v.TransactionID
                WHERE   ( ( x.PaymentCategory = 'CC'
                                AND x.PaymentType = 'F'
                                AND ( COALESCE(v.VoucherRC, '') = '' )
                                AND ( COALESCE(v.VoucherPS, '') = '' )
                                )
                                OR 
			                    ( x.PaymentCategory = 'DD'
                                    AND ( COALESCE(v.VoucherRC, '') = '' )
                                    AND ( COALESCE(v.VoucherPS, '') = '' )
                                    AND ( COALESCE(v.VoucherRO, '') <> '' )
                                    )
                                OR ( x.PaymentCategory = 'CC'
                                    AND x.PaymentType = 'I'
                                    AND ( COALESCE(v.VoucherRC, '') = '' )
                                    AND ( COALESCE(v.VoucherPS, '') = '' )
                                    AND ( COALESCE(v.VoucherRO, '') <> '' )
                                    )
                            )
                        AND x.RefferenceTypeCode = @0
                ";

            #endregion
            using (var db = RepositoryBase.GetAABMobileDB())
            {
                List<dynamic> queueList = db.Fetch<dynamic>(qGetTransQueue, "PMT");
                foreach (dynamic q in queueList)
                {
                    ManageVoucher(q.TransactionID, "PMT", "OTOSL");
                }
            }


            #region SQL

            string cSQLCreateVoucher = @";EXEC Finance.usp_GenerateVoucherByGOAutomatic @0, -- OrderNo varchar(20)
                                        @1, -- PolicyNo varchar(20)
                                        @2, -- PaidAmount decimal
                                        @3, -- AABAccountCode varchar(6)
                                        @4, -- PaymentCategory varchar(50)
                                        @5, -- BankCode
                                        @6, -- TransactionID 
                                        @7, -- InstallmentInterval
                                        @8 -- ApplicationType";


            string cSQLInsertVoucherPayment = @";INSERT INTO AABMobile.dbo.VoucherPayment
		                    ( OrderSimulationPaymentID ,
		                      OrderNo ,
		                      VoucherRC ,
		                      VoucherPS ,
		                      VoucherRO ,
		                      ErrorMessage ,
		                      CreatedBy ,
		                      CreatedDate,
                              TransactionID,
							  RowStatus
		                    )
		            VALUES  ( @0 , -- OrderSimulationPaymentID - bigint
		                      @1 , -- OrderNo - varchar(100)
		                      @2 , -- VoucherRC - varchar(100)
		                      @3 , -- VoucherPS - varchar(100)
		                      @4 , -- VoucherRO - varchar(100)
		                      @5 , -- ErrorMessage - varchar(8000)
		                      'OTOSL' , -- CreatedBy - varchar(50)
		                      GETDATE(), -- CreatedDate - datetime		         
                              @6,
							  1
		                    )";

            string cSQLUpdatePSVouhcerPayment = @";UPDATE AABMobile.dbo.VoucherPayment SET VoucherPS = @0, ModifiedBy ='OTOSL', ModifiedDate=GetDate() WHERE OrderNo = @1 AND TransactionID = @2";
            string cSQLUpdateRCVouhcerPayment = @";UPDATE AABMobile.dbo.VoucherPayment SET VoucherRC = @0, ModifiedBy ='OTOSL', ModifiedDate=GetDate() WHERE OrderNo = @1 AND TransactionID = @2";
            string cSQLUpdateROVouhcerPayment = @";UPDATE AABMobile.dbo.VoucherPayment SET VoucherRO = @0, ModifiedBy ='OTOSL', ModifiedDate=GetDate() WHERE OrderNo = @1 AND TransactionID = @2";
            string cSQLUpdateErrVouhcerPayment = @";UPDATE AABMobile.dbo.VoucherPayment SET ErrorMessage = @0, ModifiedBy ='OTOSL', ModifiedDate=GetDate() WHERE OrderNo = @1 AND TransactionID = @2";

            #endregion

            //dynamic resultCrtVouhcer = null;
            List<TransactionQueue> obj = PaymentRepository.GetTransactionQueueByType(cSQlTransactionQueue, "PMT");
            //List<TransactionQueue> obj = VoucherCreationAccess.GetTransactionQueueByType(cSQlTransactionQueueForTest, "PMT");
            foreach (TransactionQueue o in obj)
            {
                List<dynamic> resultCrtVouhcer = new List<dynamic>();
                try
                {
                    if (o.PaymentCategory == "CC" && o.PaymentType == "F")
                    {
                        if (string.IsNullOrWhiteSpace(o.OrderNo) || (string.IsNullOrWhiteSpace(o.VoucherRC) && string.IsNullOrWhiteSpace(o.VoucherPS)))
                        {
                            //penambahan pengecekan apakah voucher sudah settle
                            if (!string.IsNullOrEmpty(o.VoucherRO))
                            {
                                if (Otosales.Logic.OrdLogic.CekSettlement(o.VoucherRO))
                                {
                                    resultCrtVouhcer = PaymentRepository.CreateVoucher(cSQLCreateVoucher, o.PolicyOrderNo, o.PolicyNo, o.PaidAmount, o.AABAccountCode, o.PaymentCategory, o.BankCode, o.TransactionID, o.InstallmentIntervalTimes, ApplicationType);
                                }
                            }
                            else
                            {
                                resultCrtVouhcer = PaymentRepository.CreateVoucher(cSQLCreateVoucher, o.PolicyOrderNo, o.PolicyNo, o.PaidAmount, o.AABAccountCode, o.PaymentCategory, o.BankCode, o.TransactionID, o.InstallmentIntervalTimes, ApplicationType);
                            }
                            if (resultCrtVouhcer.Count > 0)
                            {
                                string VoucherTypeCode = "";
                                if (string.IsNullOrWhiteSpace(o.OrderNo))
                                {
                                    if (string.IsNullOrWhiteSpace(resultCrtVouhcer[0].ErrorMessage.ToString()))
                                    {
                                        VoucherTypeCode = resultCrtVouhcer[0].VoucherTypeCode.ToString();
                                        //insert
                                        switch (VoucherTypeCode)
                                        {
                                            case "RC":
                                                PaymentRepository.InsertVoucherPayment(cSQLInsertVoucherPayment, o.OrderSimulationPaymentID, o.PolicyOrderNo, resultCrtVouhcer[0].VoucherNo.ToString(), "", "", "", o.TransactionID);
                                                break;
                                            case "PS":
                                                PaymentRepository.InsertVoucherPayment(cSQLInsertVoucherPayment, o.OrderSimulationPaymentID, o.PolicyOrderNo, "", resultCrtVouhcer[0].VoucherNo.ToString(), "", "", o.TransactionID);
                                                break;
                                            case "RO":
                                                PaymentRepository.InsertVoucherPayment(cSQLInsertVoucherPayment, o.OrderSimulationPaymentID, o.PolicyOrderNo, "", "", resultCrtVouhcer[0].VoucherNo.ToString(), "", o.TransactionID);
                                                break;
                                        }
                                        //insert ke table untuk keperluan K2
                                        if (VoucherTypeCode == "RO" || VoucherTypeCode == "RC")
                                        {
                                            PaymentRepository.InsertVoucherQueue(resultCrtVouhcer[0].VoucherNo.ToString(), "VS5", "N", o.PolicyNo);
                                        }
                                    }
                                    else
                                    {
                                        PaymentRepository.InsertVoucherPayment(cSQLInsertVoucherPayment, o.OrderSimulationPaymentID, o.PolicyOrderNo, "", "", "", resultCrtVouhcer[0].ErrorMessage.ToString(), o.TransactionID);
                                    }
                                }
                                else
                                {
                                    if (string.IsNullOrWhiteSpace(resultCrtVouhcer[0].ErrorMessage.ToString()))
                                    {
                                        VoucherTypeCode = resultCrtVouhcer[0].VoucherTypeCode.ToString();
                                        //update
                                        switch (VoucherTypeCode)
                                        {
                                            case "RC":
                                                PaymentRepository.UpdateVoucherPayment(cSQLUpdateRCVouhcerPayment, resultCrtVouhcer[0].VoucherNo.ToString(), o.PolicyOrderNo, o.TransactionID);
                                                break;
                                            case "PS":
                                                PaymentRepository.UpdateVoucherPayment(cSQLUpdatePSVouhcerPayment, resultCrtVouhcer[0].VoucherNo.ToString(), o.PolicyOrderNo, o.TransactionID);
                                                break;
                                            case "RO":
                                                PaymentRepository.UpdateVoucherPayment(cSQLUpdateROVouhcerPayment, resultCrtVouhcer[0].VoucherNo.ToString(), o.PolicyOrderNo, o.TransactionID);
                                                break;
                                        }
                                        if (VoucherTypeCode == "RO" || VoucherTypeCode == "RC")
                                        {
                                            if (PaymentRepository.CheckVoucherQueue(resultCrtVouhcer[0].VoucherNo.ToString()) == false)
                                            {
                                                PaymentRepository.InsertVoucherQueue(resultCrtVouhcer[0].VoucherNo.ToString(), "VS5", "N", o.PolicyNo);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        PaymentRepository.UpdateVoucherPayment(cSQLUpdateErrVouhcerPayment, resultCrtVouhcer[0].ErrorMessage.ToString(), o.PolicyOrderNo, o.TransactionID);
                                        //log.Error(resultCrtVouhcer[0].ErrorMessage.ToString());
                                    }
                                }
                            }
                        }
                    }
                    else if (o.PaymentCategory == "DD" || (o.PaymentCategory == "CC" && o.PaymentType == "I"))
                    {
                        if ((string.IsNullOrWhiteSpace(o.VoucherRC) || string.IsNullOrWhiteSpace(o.VoucherPS)) && !string.IsNullOrWhiteSpace(o.VoucherRO))
                        {
                            if (!string.IsNullOrEmpty(o.VoucherRO))
                            {
                                if (Otosales.Logic.OrdLogic.CekSettlement(o.VoucherRO))
                                {
                                    resultCrtVouhcer = PaymentRepository.CreateVoucher(cSQLCreateVoucher, o.PolicyOrderNo, o.PolicyNo, o.PaidAmount, o.AABAccountCode, o.PaymentCategory, o.BankCode, o.TransactionID, o.InstallmentIntervalTimes, ApplicationType);
                                }
                            }
                            else
                            {
                                resultCrtVouhcer = PaymentRepository.CreateVoucher(cSQLCreateVoucher, o.PolicyOrderNo, o.PolicyNo, o.PaidAmount, o.AABAccountCode, o.PaymentCategory, o.BankCode, o.TransactionID, o.InstallmentIntervalTimes, ApplicationType);
                            }
                            if (resultCrtVouhcer.Count > 0)
                            {
                                if (string.IsNullOrWhiteSpace(resultCrtVouhcer[0].ErrorMessage.ToString()))
                                {
                                    //udpate
                                    string VoucherTypeCode = resultCrtVouhcer[0].VoucherTypeCode.ToString();
                                    switch (VoucherTypeCode)
                                    {
                                        case "RC":
                                            PaymentRepository.UpdateVoucherPayment(cSQLUpdateRCVouhcerPayment, resultCrtVouhcer[0].VoucherNo.ToString(), o.PolicyOrderNo, o.TransactionID);
                                            break;
                                        case "PS":
                                            PaymentRepository.UpdateVoucherPayment(cSQLUpdatePSVouhcerPayment, resultCrtVouhcer[0].VoucherNo.ToString(), o.PolicyOrderNo, o.TransactionID);
                                            break;
                                        case "RO":
                                            PaymentRepository.UpdateVoucherPayment(cSQLUpdateROVouhcerPayment, resultCrtVouhcer[0].VoucherNo.ToString(), o.PolicyOrderNo, o.TransactionID);
                                            break;
                                    }
                                    if ((o.PaymentCategory == "CC" && o.PaymentType == "I") && (VoucherTypeCode == "RO" || VoucherTypeCode == "RC"))
                                    {
                                        if (PaymentRepository.CheckVoucherQueue(resultCrtVouhcer[0].VoucherNo.ToString()) == false)
                                        {
                                            PaymentRepository.InsertVoucherQueue(resultCrtVouhcer[0].VoucherNo.ToString(), "VS5", "N", o.PolicyNo);
                                        }
                                    }
                                }
                                else
                                {
                                    PaymentRepository.UpdateVoucherPayment(cSQLUpdateErrVouhcerPayment, resultCrtVouhcer[0].ErrorMessage.ToString(), o.PolicyOrderNo, o.TransactionID);
                                    //log.Error(resultCrtVouhcer[0].ErrorMessage.ToString());
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.Error(ex.ToString());
                }
            }
        }

        static void ManageVoucher(string RefferenceNo, string RefferenceTypeCode, string Actor)
        {
            int PaymentStatus = 0;
            bool tResult = false;
            bool CaptureCCResult = false;
            string CreatedROVoucher = string.Empty;
            string AppServiceResult = string.Empty;
            int ProcessStatus = 0;
            string PaymentCategoryCode = string.Empty;

            CreatedROVoucher = PaymentRepository.GetCreatedVoucherRO(RefferenceNo);
            PaidPaymentInfo PPI = PaymentRepository.GetPaidPaymentInfo(RefferenceNo);
            if (PPI != null)
            {
                if (string.IsNullOrWhiteSpace(CreatedROVoucher))
                {
                    //Jika Voucher RO Belum Terbentuk                                        
                    PaymentCategoryCode = PPI.PaymentCategory;
                    if (PPI.PaymentCategory.Trim().ToUpper() == PaymentCategory.DirectDebet || (PPI.PaymentCategory.Trim().ToUpper() == PaymentCategory.CreditCard
                        && PPI.PaymentType.Trim().ToUpper() == "I") || (PPI.PaymentCategory.Trim().ToUpper() == PaymentCategory.CreditCard
                        && PPI.PaymentType.Trim().ToUpper() == "F" && PPI.IsAuthorized == 1))
                    {
                        PaymentRepository.InsertVoucherPaymentIfNotExist(RefferenceNo, PPI.PolicyOrderNo, Actor);
                        GenerateROVoucherResult RO = PaymentRepository.GenerateROVoucher(PPI.PolicyOrderNo, PPI.PolicyNo, PPI.PaidAmount, PPI.AABAccountCode, PPI.PaymentCategory, 
                            PPI.BankCode, RefferenceNo, PPI.InstallmentIntervalTimes, ApplicationType);
                        PaymentRepository.UpdateVoucherPaymentWithErrorMessage(RefferenceNo, RO.VoucherNo, Actor, RO.ErrorMessage);
                        if ((PPI.PaymentCategory.Trim().ToUpper() == PaymentCategory.CreditCard && PPI.PaymentType.Trim().ToUpper() == "I") || (PPI.PaymentCategory.Trim().ToUpper() == PaymentCategory.CreditCard && PPI.PaymentType.Trim().ToUpper() == "F" && PPI.IsAuthorized == 1)
                            || (PPI.PaymentCategory.Trim().ToUpper() == PaymentCategory.DirectDebet))
                        {
                            PaymentRepository.InsertVoucherQueue(RO.VoucherNo, "VS5", "N", PPI.PolicyNo);
                        }
                        //AsuransiAstraAccess.UpdateManageOrder(RefferenceNo, OrderSimulationStatus.AddPaymentCompleted, "", Actor);
                        tResult = PaymentRepository.UpdateTransactionQueueAsProcessed(RefferenceTypeCode, RefferenceNo, "VOU");
                        ProcessStatus = tResult == true ? 1 : 0;
                        CreatedROVoucher = RO.VoucherNo;
                    }
                    else if (PPI.PaymentCategory.Trim().ToUpper() == PaymentCategory.VirtualAccount)
                    {
                        ProcessStatus = tResult == true ? 1 : 0;

                    }

                }
            }
        }
    }
}
