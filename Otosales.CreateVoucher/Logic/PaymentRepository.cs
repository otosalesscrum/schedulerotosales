﻿using a2is.Framework.DataAccess;
using a2is.Framework.Monitoring;
using Otosales.CreateVoucher.Models;
using Otosales.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otosales.CreateVoucher.Logic
{
    class PaymentRepository
    {
        private static a2isLogHelper logger = new a2isLogHelper();
        public static List<TransactionQueue> GetTransactionQueueByType(string cSQL, string types, string applicationCode = "")
        {
            string CurrentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
                List<TransactionQueue> obj = new List<TransactionQueue>();
            try
            {
                using (a2isDBHelper.Database db = RepositoryBase.GetAsuransiAstraDB())
                {
                    obj = db.Fetch<TransactionQueue>(cSQL, types, applicationCode).ToList();
                }

            }
            catch (Exception e)
            {
                logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.StackTrace));
            }
            return obj;
        }

        public static dynamic CreateVoucher(string cSQL, string OrderNo, string PolicyNo, decimal PaidAmount, string AABAccountCode, string PaymentCategory, string BankDescription, string TransactionID, int InstallmentIntervalTimes, string ApplicationType)
        {
            dynamic result = null;
            string CurrentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                using (a2isDBHelper.Database db = RepositoryBase.Geta2isBeyondDB())
                {
                    db.CommandTimeout = 3600;
                    result = db.Query<dynamic>(cSQL, OrderNo, PolicyNo, PaidAmount, AABAccountCode, PaymentCategory, BankDescription, TransactionID, InstallmentIntervalTimes, ApplicationType).ToList();
                }
            }
            catch (Exception e)
            {
                logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.StackTrace));
            }
            return result;
        }
        public static bool InsertVoucherPayment(string cSQL, Int64 OrderSimulationPaymentID, string OrderNo, string VoucerRC = "", string voucerPS = "", string voucerRO = "", string ErrorMsg = "", string TransactionID = "")
        {
            bool result = false;
            string CurrentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                using (a2isDBHelper.Database db = RepositoryBase.GetAsuransiAstraDB())
                {
                    int hsl = db.Execute(cSQL, OrderSimulationPaymentID, OrderNo, VoucerRC, voucerPS, voucerRO, ErrorMsg, TransactionID);
                    if (hsl > 0)
                    {
                        result = true;
                    }
                }

            }
            catch (Exception e)
            {

                logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.StackTrace));
            }
            return result;
        }
        public static dynamic InsertVoucherQueue(string VoucherNo, string ExpectedStatus, string QueueStatus, string PolicyNo)
        {
            dynamic result = null;
            string CurrentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                using (a2isDBHelper.Database db = RepositoryBase.Geta2isBeyondDB())
                {
                    #region
                    string SQL = @";DECLARE @@VoucherNo VARCHAR(50) = @0 ,
                                    @@ExpectedStatus VARCHAR(5) = @1 ,
                                    @@QueueStatus VARCHAR(1) = @2 ,
                                    @@CreatedBy VARCHAR(10) = @3 ,
                                    @@PolicyNo VARCHAR(20) = @4
                                INSERT  INTO Finance.VoucherGODigitalQueue
                                        ( VoucherNo ,
                                          ExpectedStatusCode ,
                                          ProcessedDate ,
                                          QueueStatus ,
                                          CreatedBy ,
                                          CreatedDate ,
                                          RowStatus ,
                                          PolicyNo ,
                                          ProcessedStatus
                                        )
                                VALUES  ( @@VoucherNo , -- VoucherNo - varchar(50)
                                          @@ExpectedStatus , -- ExpectedStatusCode - varchar(5)
                                          GETDATE() , -- ProcessedDate - datetime
                                          @@QueueStatus , -- QueueStatus - varchar(1)
                                          @@CreatedBy , -- CreatedBy - varchar(50)
                                          GETDATE() , -- CreatedDate - datetime
                                          0 , -- RowStatus - smallint
                                          @@PolicyNo ,
                                          0
                                        )";
                    #endregion
                    result = db.Execute(SQL, VoucherNo, ExpectedStatus, QueueStatus, "OTOSL", PolicyNo);
                }
            }
            catch (Exception e)
            {

                logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.StackTrace));
            }
            return result;
        }
        public static bool UpdateVoucherPayment(string cSQL, string Voucher, string OrderNo, string TransactionID)
        {
            bool result = false;
            string CurrentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                using (a2isDBHelper.Database db = RepositoryBase.GetAsuransiAstraDB())
                {
                    int hsl = db.Execute(cSQL, Voucher, OrderNo, TransactionID);
                    if (hsl > 0)
                    {
                        result = true;
                    }
                }
            }
            catch (Exception e)
            {

                logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.StackTrace));
            }
            return result;
        }
        public static bool CheckVoucherQueue(string VoucherNo)
        {
            string CurrentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            dynamic result;
            bool isExist = false;
            try
            {
                using (a2isDBHelper.Database db = RepositoryBase.Geta2isBeyondDB())
                {
                    #region
                    string SQL = @";DECLARE @@VoucherNo VARCHAR(50) = @0
                                SELECT VoucherNo FROM Finance.VoucherGODigitalQueue AS vgdq
                                WHERE VoucherNo = @@VoucherNo";
                    #endregion
                    result = db.Query<dynamic>(SQL, VoucherNo).ToList();
                    if (result.Count > 0)
                    {
                        isExist = true;
                    }
                }
            }
            catch (Exception e)
            {
                logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.StackTrace));
            }
            return isExist;
        }
        public static string GetCreatedVoucherRO(string TransactionID)
        {
            using (a2isDBHelper.Database db = RepositoryBase.GetAABMobileDB())
            {
                string query = @"SELECT TOP 1
                                        VoucherRO
                                FROM    dbo.VoucherPayment
                                WHERE   RowStatus = 1
                                        AND TransactionID = @0";
                return db.Query<string>(query, TransactionID).SingleOrDefault();
            }
        }
        public static PaidPaymentInfo GetPaidPaymentInfo(string TransactionID)
        {
            string aUpdt = @"SELECT     b.PolicyOrderNo AS PolicyOrderNo,
                                        CASE WHEN (a.IsAuthorized=1)
											Then a.SettledAmount
											ELSE a.PaidAmount   
										END PaidAmount,
                                        c.BankCode AS BankCode ,
                                        c.BankPaymentMethodCode AS BankPaymentMethodCode ,
                                        c.AABAccountCode AS AABAccountCode ,
                                        d.PaymentCategory ,
                                        a.TransactionDate ,
                                        g.BankName ,
                                        d.PaymentMethodDescription ,
                                        a.OrderNo ,
                                        a.ReferenceNo ,
                                        a.TransactionID ,
                                        a.ReceiptCode ,
                                        a.AuthNo ,
                                        d.PaymentMethodCode ,
                                        a.PaymentStatus ,
                                        e.PaymentStatusCode ,
                                        f.PaymentStatusDescription ,
                                        a.ResultCD ,
                                        a.ResultMsg ,
                                        a.ResultCD2 ,
                                        a.ResultMsg2 ,
                                        a.IsAuthorized ,
                                        a.IsProceedByDBUrl,
		                                b.PolicyNo,
                                        c.PaymentType,
                                        c.InstallmentIntervalTimes
                                FROM    AABMobile.dbo.OrderSimulationPayment a
                                        INNER JOIN AABMobile.dbo.ordersimulation b ON a.OrderNo = b.OrderNo
                                        INNER JOIN GODigital.BankPaymentMethod c ON a.BankPaymentMethodCode = c.BankPaymentMethodCode
                                        INNER JOIN godigital.PaymentMethod d ON c.PaymentMethodCode = d.PaymentMethodCode
                                        INNER JOIN GODigital.PaymentGatewayRequestStatusMapping e ON e.PaymentRequestParameterCode = c.PaymentRequestParameterCode
                                                                                              AND e.IsAuthorizedF = a.IsAuthorized
                                                                                              AND a.PaymentStatus = e.PaymentRequestStatusCode
                                        INNER JOIN GODigital.MstPaymentStatus f ON f.PaymentStatusCode = e.PaymentStatusCode
                                        LEFT JOIN GODigital.Bank g ON g.BankCode = c.BankCode
                                                            AND g.RowStatus = 0
                                WHERE   a.ResultCD = '0000'
                                        AND a.ResultCD2 = '0000'
                                        AND a.IsProceedByDBUrl = 1
                                        AND a.RowStatus = 1
                                        AND b.RowStatus = 1
                                        AND c.RowStatus = 0
                                        AND d.rowstatus = 0
                                        AND e.rowstatus = 0
                                        AND f.rowstatus = 0
                                        AND ( ( d.PaymentMethodCode = '02'
                                                AND COALESCE(a.ReceiptCode, '') = '0000'
                                                AND COALESCE(a.AuthNo, '') = '0000'
                                                AND a.IsAuthorized = 1
                                              )
                                              OR ( d.PaymentMethodCode = '04'
                                                   AND ( COALESCE(a.ReceiptCode, '') <> ''
                                                         OR COALESCE(a.mRefNo, '') <> ''
                                                       )

                                                   AND a.IsAuthorized = 1
                                                 )
                                              OR ( d.PaymentMethodCode = '01'
                                                   AND COALESCE(a.AuthNo, '') <> ''
                                                   AND ( ( c.PaymentType = 'F' )
                                                         OR ( c.PaymentType = 'I'
                                                              AND a.IsAuthorized = 1
                                                            )
                                                       )
                                                 )
                                            )
                                        AND f.PaymentStatusCode IN ( 'PD', 'HA', 'NA' )
                                        AND COALESCE(a.CancelCode, '') = ''		                                
		                                AND a.TransactionID=@0";
            using (a2isDBHelper.Database db = RepositoryBase.GetAsuransiAstraDB())
            {
                var result = db.Query<PaidPaymentInfo>(aUpdt, TransactionID).FirstOrDefault();
                return result;
            }
        }
        public static bool InsertVoucherPaymentIfNotExist(string TransactionID, string OrderNo, string Actor, string VoucherRC = "", string VoucherPS = "", string VoucherRO = "", string ErrorMessage = "")
        {
            string query = @"IF NOT EXISTS ( SELECT  1
                                        FROM    dbo.VoucherPayment
                                        WHERE   TransactionID = @0 AND RowStatus = 1)
                            BEGIN	
                                INSERT  INTO dbo.VoucherPayment
                                        ( TransactionID ,
                                          OrderNo ,
                                          VoucherRC ,
                                          VoucherPS ,
                                          VoucherRO ,
                                          CreatedBy ,
                                          CreatedDate ,
                                          ErrorMessage,
										  RowStatus
                                        )
                                VALUES  ( @0 , -- OrderSimulationPaymentID - bigint
                                          @1 , -- OrderNo - varchar(100)
                                          @2 ,
                                          @3 ,
                                          @4 ,
                                          @5 , -- CreatedBy - varchar(50)
                                          GETDATE() , -- CreatedDate - datetime         
                                          @6,
										  1  
                                        )
                            END";

            using (a2isDBHelper.Database db = RepositoryBase.GetAABMobileDB())
            {
                var result = db.Execute(query, TransactionID, OrderNo, VoucherRC, VoucherPS, VoucherRO, Actor, ErrorMessage);
                return result > 0;
            }
        }
        public static GenerateROVoucherResult GenerateROVoucher(string OrderNo, string PolicyNo, decimal Amount, string AABAccountCode, string PaymentCategory, 
            string BankName, string TransactionID, int InstallmentInterval, string ApplicationType)
        {

            using (a2isDBHelper.Database db = RepositoryBase.Geta2isBeyondDB())
            {
                db.CommandTimeout = 3600;
                string query = @";EXEC [Finance].[usp_GenerateVoucherByGOAutomaticReceiptOther]
                                @@OrderNo = @0,
                                @@PaidAmountBill = @1,
                                @@PolicyNo = @2, 
                                @@AABAccountCode = @3,
                                @@PaymentCategory = @4,
                                @@BankCode = @5,
                                @@TransactionID = @6,
                                @@InstallmentInterval =@7,
                                @@ApplicationType =@8";
                return db.Query<GenerateROVoucherResult>(query, OrderNo, Amount, PolicyNo, AABAccountCode, PaymentCategory, BankName, TransactionID, InstallmentInterval, ApplicationType).FirstOrDefault();
            }
        }
        public static bool UpdateVoucherPaymentWithErrorMessage(string OrderSimulationPaymentID, string VoucherRO, string Actor, string ErrorMessage = "")
        {
            string query = @"UPDATE  dbo.VoucherPayment
                            SET     VoucherRO = @0 ,
                                    ErrorMessage = @1,
		                            ModifiedBy = @2,
		                            ModifiedDate =GETDATE()
                            WHERE   TransactionID = @3 AND RowStatus = 1";
            using (a2isDBHelper.Database db = RepositoryBase.GetAABMobileDB())
            {
                var result = db.Execute(query, VoucherRO, ErrorMessage, Actor, OrderSimulationPaymentID);
                return result > 0;
            }
        }
        public static bool UpdateTransactionQueueAsProcessed(string ReferenceTypeCode, string ReferenceNo, string Actor)
        {
            string query = @"UPDATE  dbo.TransactionQueue
                            SET     ProcessStatus = 1 ,
                                    ProcessDate = GETDATE() ,
                                    ModifiedDate = GETDATE() ,
                                    ModifiedBy = @0
                            WHERE   RowStatus = 1
                                    AND RefferenceTypeCode = @1
                                    AND RefferenceNo = @2";
            using (a2isDBHelper.Database db = RepositoryBase.GetAABMobileDB())
            {
                int result = db.Execute(query, Actor, ReferenceTypeCode, ReferenceNo);
                return result > 0;
            }
        }

    }
}
