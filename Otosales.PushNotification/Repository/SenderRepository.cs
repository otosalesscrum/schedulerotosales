﻿using Otosales.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otosales.PushNotification.Repository
{
    class SenderRepository : RepositoryBase
    {
        public dynamic getDataPushNotif() {
            string query = @"SELECT * FROM PushNotificationOtosales WHERE [Status] = 1";
            using (var db = GetAABMobileDB()) {
                var que = db.Query<dynamic>(query);
                return que;
            }
        }

        public int updateDataPushNotif(int pushId) {
            string query = @" UPDATE dbo.PushNotificationOtosales SET [Status] = 0, UpdateDate = GETDATE() WHERE PushNotificationID = @0 ";
            using ( var db = GetAABMobileDB()) {
                var update = db.Execute(query, pushId);
                return update;
            }
        }


        public dynamic GetTotalFollowUp()
        {
            string query = @"
SELECT SalesOfficerID, TotalNew, TotalRenew, DeviceToken FROM
(
	SELECT SalesOfficerID, SUM(IIF(COALESCE(IsRenewal,0) = 1, 1, 0)) AS [TotalRenew], SUM(IIF(COALESCE(IsRenewal,0) = 0, 1, 0)) AS [TotalNew] 
	FROM FollowUp
	WHERE RowStatus = 1 
	GROUP BY SalesOfficerID
) fu
INNER JOIN GardaMobileCMS.dbo.UserDevice b ON b.Status=1 AND b.ApplicationID=4 AND b.UserID = fu.SalesOfficerID
INNER JOIN GardaMobileCMS.dbo.Device a ON a.DeviceID=b.DeviceID AND  a.Status = 1 and a.ApplicationID = 4 
";
            using (var db = GetAABMobileDB())
            {
                return db.Fetch<dynamic>(query);
            }
        }

    }
}
