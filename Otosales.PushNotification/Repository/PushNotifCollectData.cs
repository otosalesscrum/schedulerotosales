﻿using Otosales.Repository;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otosales.PushNotification.Repository
{
    class PushNotifCollectData : RepositoryBase
    {
        public dynamic getQueuePushNotifNewRenew(string userId) {
            string query = @"SELECT SUM(IIF(COALESCE(IsRenewal,0) = 1, 1, 0)) AS [TotalRenew], SUM(IIF(COALESCE(IsRenewal,0) = 0, 1, 0)) AS [TotalNew] FROM FollowUp WHERE SalesOfficerID=@0";
            using ( var db = GetGardaMobileCMS()) {
                return db.Query<dynamic>(query, userId);
            }
        }
      
    }
}
