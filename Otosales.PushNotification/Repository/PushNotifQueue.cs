﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Otosales.Logic;

namespace Otosales.PushNotification.Repository
{
    class PushNotifQueue
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        public static void insertDataSales() {

            try {

                 //CommonLogic.CreatePushNotification("59");

                //PushNotifCollectData dataCollect = new PushNotifCollectData();
                //var salesToken = dataCollect.getLoginUser();
                //var subject = "Data To Be Follow Up";
                //var icon = "https://otosalesapp-dev.asuransiastra.com/img/logo.png";
                //var data = "{}";
                //var status = 0;
                //var clickAction = "taslisk";
                //var entryDate = DateTime.Now;
                //var updateDate = DateTime.Now;


                //foreach (var i in salesToken) {
                //    var prosNew = dataCollect.getQueuePushNotifNewRenew(i.userid).TotalNew.ToString();
                //    var prosRenew = dataCollect.getQueuePushNotifNewRenew(i.userid).TotalRenew.ToString();
                //    var body = prosNew + " New & " + prosRenew + " Renew Data to be Follow Up!";
                //    var userId = i.userid;
                //    var tokenUser = i.DeviceToken;
                //    dataCollect.InsertPushNotifSalesOfficer(userId, tokenUser, subject, body, icon, data, status, clickAction, entryDate, updateDate);
                //}
                
            }
            catch (Exception e) {
                Console.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + " : ERROR-SendPushNotifPushNotifQueue: " + e.Message);
                log.Error(e.Message);
                ResponseSuccess er = new ResponseSuccess();
                er.success = 0;
                er.failure = 1;
            }
        }

    }
}
