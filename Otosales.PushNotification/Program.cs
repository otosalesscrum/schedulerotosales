﻿using System;
using System.Configuration;
using Otosales.PushNotification.Repository;
using static Otosales.Models.Constant;
using Otosales.Logic;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using Otosales.PushNotification.Models;

namespace Otosales.PushNotification
{
    class Program
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        static void Main(string[] args)
        {
            //CommonLogic.CreatePushNotification(FollowUpInfo.FollowUpSurveyResult);
            //CommonLogic.CreatePushNotification(FollowUpInfo.NeedDataRevision);
            //CommonLogic.CreatePushNotification(FollowUpStatus.OrderRejectedStatusCode);
            //CommonLogic.CreatePushNotification(FollowUpInfo.PolicyApprovedNeedDocRep);
            //CommonLogic.CreatePushNotification(FollowUpInfo.PolicyApproved);
            //ResponseSuccess res = SendPushNotification();
            SendPushNotification();
        }

        private static void SendPushNotification()
        {
            try
            {
                log.Info(string.Format("BEGIN SendPushNotification()"));
                SenderRepository sender = new SenderRepository();
                ResponseSuccess result = new ResponseSuccess();
                var push = sender.getDataPushNotif();
                string icon = ConfigurationManager.AppSettings["IconURL"].ToString();

                foreach (var i in push)
                {
                    int pushId = Convert.ToInt32(i.PushNotificationID);
                    string userId = i.UserID;
                    string fcmToken = i.FCMToken;
                    string subject = i.Subject;
                    string bodyMsg = i.Body;
                    string data = i.Data;
                    int status = Convert.ToInt32(i.Status);
                    string clickAction = i.ClickAction;
                    DateTime entryDate = i.EntryDate;

                    string basicURL = ConfigurationManager.AppSettings["BaseOtosalesUrl"].ToString();
                    var body = new
                    {
                        to = fcmToken,
                        notification = new
                        {
                            title = subject,
                            body = bodyMsg,
                            click_action = basicURL + clickAction,
                            icon = icon
                        },
                        data = new { data }
                    };
                    
                    var httpWebRequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["FCMURL"]);
                    httpWebRequest.ContentType = "application/json";
                    httpWebRequest.Headers.Add("Authorization:key=" + ConfigurationManager.AppSettings["serverKey"]);
                    httpWebRequest.Method = "POST";
                    if (ConfigurationManager.AppSettings["ConnectionUsingProxy"].ToString().Equals("1"))
                    {
                        WebProxy proxy = new WebProxy(ConfigurationManager.AppSettings["ConnectionProxyIP"], Int32.Parse(ConfigurationManager.AppSettings["ConnectionProxyPort"]));
                        string proxyUsername = ConfigurationManager.AppSettings["ConnectionProxyUsername"];
                        string proxyPassword = ConfigurationManager.AppSettings["ConnectionProxyPassword"];
                        proxy.Credentials = new NetworkCredential(proxyUsername, proxyPassword);
                        httpWebRequest.Proxy = proxy;
                    }
                    using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                    {
                        string json = JsonConvert.SerializeObject(body);
                        streamWriter.Write(json);
                        streamWriter.Flush();
                    }
                    var httpResponse = (HttpWebResponse)httpWebRequest.GetResponseAsync().Result;
                    FCMResponse fcmResponse = null; 
                    using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                    {
                        fcmResponse = JsonConvert.DeserializeObject<FCMResponse>(streamReader.ReadToEnd());
                    }
                    if (fcmResponse != null)
                    {
                        result.success += fcmResponse.success;
                        result.failure += fcmResponse.failure;
                    }
                    sender.updateDataPushNotif(pushId);
                }
                log.Info(string.Format("Complete SendPushNotification() with success : {0} , failed : {1}", result.success, result.failure));
            }
            catch (Exception ex)
            {
                log.Error("An error occured when executing SendPushNotification() at Otosales.PushNotification : "+ ex.Message, ex);
            }
        }
    }
}
