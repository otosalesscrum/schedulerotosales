﻿using System.Collections.Generic;

namespace Otosales.PushNotification.Models
{
    //{"multicast_id":7960419016457200952,"success":0,"failure":1,"canonical_ids":0,"results":[{"error":"NotRegistered"}]}
    class FCMResponse
    {
        public string multicast_id { get; set; }
        public int success { get; set; }
        public int failure { get; set; }
        public int canonical_ids { get; set; }
        public List<FCMError> results { get; set; }

    }

    class FCMError
    {
        public string Error { get; set; }
    }


}
