﻿using a2is.Framework.Monitoring;
using Otosales.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otosales.MonitorVATransaction.Repository
{
    class AABRepository : RepositoryBase
    {
        private static a2isLogHelper logger = new a2isLogHelper();
        public static List<dynamic> GetExpiredVA() {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                string qGetExpVATrans = @"SELECT VANumber
                                        INTO #TempVAOrderSimulation
                                        FROM dbo.OrderSimulation os
                                        WHERE COALESCE(PolicyOrderNo,'') = '' AND COALESCE(PolicyNo,'') = '' 
										AND COALESCE(OldPolicyNo,'') = '' AND COALESCE(VANumber,'') <> '' 
                                        AND RowStatus = 1 AND ApplyF = 1

                                        SELECT AAB2000VirtualAccountID, VirtualAccountNo
                                        INTO #TempAAB2000VA
                                        FROM BEYONDAPP.BeyondDB.Finance.AAB2000VirtualAccount
                                        WHERE CAST(COALESCE(ModifiedDate,CreatedDate) AS DATE) <= CAST(GETDATE()-@0 AS DATE) AND RowStatus = 0

                                        SELECT * FROM #TempAAB2000VA va INNER JOIN #TempVAOrderSimulation os
                                        ON os.VANumber = va.VirtualAccountNo

                                        DROP TABLE #TempVAOrderSimulation
                                        DROP TABLE #TempAAB2000VA";
                var mblDB = GetAABMobileDB();
                int expDay = Convert.ToInt32(GetApplicationParametersValue("EXPIREDDAY-VANEW").First());
                List<dynamic> res = mblDB.Fetch<dynamic>(qGetExpVATrans, expDay);
                return res;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }
        public static void SetVAExpired(dynamic ID, string VaNo) {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                var BYDdb = Geta2isBeyondDB();
                string cSQLGetTotalBayarVA = @";WITH cte
                                                    AS ( SELECT PermataVirtualAccountSettlementID ,
                                                                a.VI_TraceNo
                                                        FROM     PolicyInsurance.PermataVirtualAccountSettlement a
                                                                INNER JOIN PolicyInsurance.PermataVirtualAccount b ON a.PermataVirtualAccountID = b.PermataVirtualAccountID
                                                        WHERE    b.VI_VANumber IN (
                                                                    SELECT  value
                                                                    FROM    PolicyInsurance.fn_Split(@0, ',') 
                                                                    )
                                                        )
                                            SELECT  MAX(pvas.PermataVirtualAccountSettlementID) as PermataVirtualAccountSettlementID,
                                                    SUM(pvas.VI_Amount) AS TotalBayar ,
                                                    MAX(pvas.VI_TransactionDate) AS MaxTransactionDate ,
                                                    MAX(pva.BillAmount) AS TotalBill
                                            FROM    cte
                                                    INNER JOIN PolicyInsurance.PermataVirtualAccountSettlement AS pvas ON pvas.PermataVirtualAccountSettlementID = cte.PermataVirtualAccountSettlementID
                                                                                                        AND pvas.VI_TraceNo = cte.VI_TraceNo
                                                    INNER JOIN PolicyInsurance.PermataVirtualAccount AS pva ON pva.PermataVirtualAccountID = pvas.PermataVirtualAccountID
                                            WHERE   IsReversal = 0";
                List<dynamic> ls = BYDdb.Fetch<dynamic>(cSQLGetTotalBayarVA, VaNo);
                if (ls.Count > 0)
                {
                    decimal TotalBayar = ls.First().TotalBayar == null ? 0 : Convert.ToDecimal(ls.First().TotalBayar);
                    if (TotalBayar == 0)
                    {
                        string qUpdate = @"UPDATE Finance.AAB2000VirtualAccount SET RowStatus = 1
                                WHERE AAB2000VirtualAccountID = @0";
                        BYDdb.Execute(qUpdate, ID);
                    }
                }

            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }

        public static List<string> GetApplicationParametersValue(string ParamName)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            string query = "";
            List<string> result = new List<string>();
            var db = GetAABMobileDB();
            try
            {
                query = @"SELECT ParamValue, ParamValue2 FROM dbo.ApplicationParameters
                        WHERE RowStatus = 1 AND ParamName = @0";
                List<dynamic> listVal = db.Fetch<dynamic>(query, ParamName);
                if (listVal.Count > 0)
                {
                    result.Add(listVal.First().ParamValue);
                    result.Add(listVal.First().ParamValue2);
                }
                return result;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }
    }
}
