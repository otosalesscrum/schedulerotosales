﻿using a2is.Framework.Monitoring;
using Otosales.MonitorVATransaction.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otosales.MonitorVATransaction
{
    class Program
    {
        private static a2isLogHelper logger = new a2isLogHelper();
        static void Main(string[] args)
        {
            MonitorExpiredVA();
        }
        static void MonitorExpiredVA()
        {
            string CurrentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                logger.Debug(string.Format("Function {0} BEGIN:'", CurrentMethod));
                List<dynamic> ls = AABRepository.GetExpiredVA();
                foreach (dynamic va in ls)
                {
                    AABRepository.SetVAExpired(va.AAB2000VirtualAccountID, va.VirtualAccountNo);
                }
                logger.Debug(string.Format("Function {0} END:'", CurrentMethod));
            }
            catch (Exception e)
            {
                logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.Message));
            }
        }

    }
}
