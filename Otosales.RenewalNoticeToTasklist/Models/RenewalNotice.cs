﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otosales.RenewalNoticeToTasklist.Models
{
    public class RenewalNotice
    {
        public string PolicyNo { get; set; }
        public string FUDate { get; set; }
        public int EndorsementNo { get; set; }
        public string NewPolicyNo { get; set; }
        public string NewPolicyId { get; set; }
        public string VANumber { get; set; }
    }
}
