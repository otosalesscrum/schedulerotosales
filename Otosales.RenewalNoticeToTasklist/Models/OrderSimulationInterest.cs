﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otosales.RenewalNoticeToTasklist.Models
{
    public class OrderSimulationInterest
    {
        public int ObjectNo { get; set; }
        public int InterestNo { get; set; }
        public string InterestID { get; set; }
        public string Year { get; set; }
        public double Premium { get; set; }
        public string PeriodFrom { get; set; }
        public string PeriodTo { get; set; }
        public string DeductibleCode { get; set; }
    }
}
