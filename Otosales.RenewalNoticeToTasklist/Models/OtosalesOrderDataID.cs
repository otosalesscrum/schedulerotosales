﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otosales.RenewalNoticeToTasklist.Models
{
    public class OtosalesOrderDataID
    {
        public string CustID { get; set; }
        public string FollowUpID { get; set; }
        public string OrderID { get; set; }
    }
}
