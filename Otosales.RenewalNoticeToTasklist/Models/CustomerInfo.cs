﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otosales.RenewalNoticeToTasklist.Models
{
    public class CustomerInfo
    {
        public string Name { get; set; }
        public string Email1 { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string CustIDAAB { get; set; }
        public string SalesOfficerID { get; set; }
        public string SalesDealer { get; set; }
        public string DealerCode { get; set; }
        public string BranchCode { get; set; }
        public int isCompany { get; set; }
        public string IdentityNo { get; set; }
        public string Gender { get; set; }
        public string ProspectID { get; set; }
        public string BirthDate { get; set; }
        public string Address { get; set; }
        public string PostCode { get; set; }
        public string CompanyNPWPAddress { get; set; }
        public string CompanyNPWPNo { get; set; }
        public string CompanyNPWPDate { get; set; }
        public string PIC { get; set; }
        public string PICPhone { get; set; }
        public string OrderNo { get; set; }
    }
}
