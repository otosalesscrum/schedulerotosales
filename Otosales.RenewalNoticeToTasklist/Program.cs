﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using a2is.Framework.Monitoring;
using Otosales.RenewalNoticeToTasklist.Repository;
using Otosales.RenewalNoticeToTasklist.Models;
using System.Configuration;
using a2is.Framework.DataAccess;
using Otosales.RenewalNoticeToTasklist.General;

namespace Otosales.RenewalNoticeToTasklist
{
    class Program
    {
        private static a2isLogHelper logger = new a2isLogHelper();
        public static string SchedulerType = ConfigurationManager.AppSettings["SchedulerType"];
        static string GetCurrentMethodName()
        {
            return System.Reflection.MethodBase.GetCurrentMethod().Name;
        }
        static void Main(string[] args)
        {
            logger.Info("Renewal Notice To Tasklist : Start");
            try
            {
                int OldPolicyNoExist = 0;
                #region Get Data Renewal Notice
                /* 
                 * Data yang di Get Adalah Data-Data Rennot yang Berstatus ('0','1','10') dan is_send_otosales = 1
                 */
                List<RenewalNotice> listPolicyNo = AABRepository.GetDataRenewalNotice();
                #endregion

                #region Process Data Renewal Notice
                /*
                 * Proses yang sama seperti input prospect dan akan jadi tasklist dengan status NEED FU
                 */
                CustomerInfo tempCustInfo = new CustomerInfo();
                if (SchedulerType.Equals("InitialVersion1", StringComparison.InvariantCultureIgnoreCase) ||
                    SchedulerType.Equals("InitialVersion2", StringComparison.InvariantCultureIgnoreCase))
                {
                    OrderSimulation tempOrdSimulation = new OrderSimulation();
                    OrderSimulationMV tempOrdSimulationmv = new OrderSimulationMV();
                    List<OrderSimulationInterest> tempOrdSimulationInterest = new List<OrderSimulationInterest>();
                    List<OrderSimulationCoverage> tempOrdSimulationCoverage = new List<OrderSimulationCoverage>();
                    OtosalesOrderDataID otosalesOrderDataID = new OtosalesOrderDataID();
                    List<ImageData> listImageData = new List<ImageData>();
                    if (listPolicyNo != null || listPolicyNo.Count > 0)
                    {
                        foreach (RenewalNotice data in listPolicyNo)
                        {
                            string RenStatusCode = AABRepository.GetRenewalStatus(data.PolicyNo);
                            if (!RenStatusCode.Equals("S0002") && !RenStatusCode.Equals("R0002") && !string.IsNullOrEmpty(RenStatusCode)) {
                                tempCustInfo = AABRepository.GetDataCustomer(data.PolicyNo);
                                tempOrdSimulation = AABRepository.GetDataOrderSimulation(data);
                                tempOrdSimulationmv = AABRepository.GetDataOrderSimulationMV(data.PolicyNo);
                                tempOrdSimulationInterest = AABRepository.GetDataOrderSimulationInterest(data.PolicyNo);
                                tempOrdSimulationCoverage = AABRepository.GetDataOrderSimulationCoverage(data.PolicyNo);
                                otosalesOrderDataID = AABRepository.GetDataOtosalesOrderDataID(data.PolicyNo);

                                if (tempCustInfo == null || tempOrdSimulation == null || tempOrdSimulationmv == null
                                    || tempOrdSimulationInterest == null || tempOrdSimulationCoverage == null)
                                {
                                    using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobileDB"))
                                    {
                                        db.Execute(QueryCollection.qInsertRenewalLog, data.PolicyNo, "Any Null Value");
                                    }
                                    //do nothing
                                }
                                else
                                {
                                    try
                                    {
                                        listImageData = AABRepository.GetDataImageAAB(tempCustInfo);
                                        OtosalesOrderDataID oData = MobileRepository.GenerateTasklist(tempCustInfo, tempOrdSimulation, tempOrdSimulationmv, tempOrdSimulationInterest, tempOrdSimulationCoverage, data.PolicyNo, otosalesOrderDataID, listImageData, RenStatusCode);

                                        AABRepository.InsertDataTasklistOtosales(data, oData);
                                    }
                                    catch (Exception e)
                                    {
                                        logger.Error("Error Renewal Notice To Tasklist : " + e.Message);
                                    }
                                }
                            }

                            //AABRepository.UpdateStatusOtosalesRenNot(policyNo);
                        }
                    }
                }
                #endregion
            }
            catch (Exception e)
            {
                logger.Error("Error Renewal Notice To Tasklist : "+ e.Message);
            }

            logger.Info("Renewal Notice To Tasklist : End");
        }
    }
}
