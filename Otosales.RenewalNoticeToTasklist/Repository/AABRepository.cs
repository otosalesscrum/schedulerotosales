﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using a2is.Framework.Monitoring;
using Otosales.RenewalNoticeToTasklist.General;
using a2is.Framework.DataAccess;
using Otosales.RenewalNoticeToTasklist.Models;
using Otosales.Repository;
using System.Configuration;

namespace Otosales.RenewalNoticeToTasklist.Repository
{
    class AABRepository : RepositoryBase
    {
        private static a2isLogHelper logger = new a2isLogHelper();
        public static string SchedulerType = ConfigurationManager.AppSettings["SchedulerType"];
        static string GetCurrentMethodName()
        {
            return System.Reflection.MethodBase.GetCurrentMethod().Name;
        }

        public static List<RenewalNotice> GetDataRenewalNotice()
        {
            logger.Info("GetDataRenewalNotice : Start");
            List<RenewalNotice> listPolicyNo = new List<RenewalNotice>();
            try
            {
                string baseQuery = string.Empty;
                if (SchedulerType.Equals("InitialVersion1",StringComparison.InvariantCultureIgnoreCase))
                    baseQuery = QueryCollection.qGetDataRenewalNoticeV1;

                if (SchedulerType.Equals("InitialVersion2", StringComparison.InvariantCultureIgnoreCase))
                    baseQuery = QueryCollection.qGetDataRenewalNoticeV2;

                using (a2isDBHelper.Database db = new a2isDBHelper.Database("a2isRetailDB"))
                {
                    listPolicyNo = db.Fetch<RenewalNotice>(baseQuery);
                }
            }
            catch (Exception e)
            {
                logger.Error("Error GetDataRenewalNotice : " + e.Message);
            }
            logger.Info("GetDataRenewalNotice : End");
            return listPolicyNo;
        }

        public static CustomerInfo GetDataCustomer(string policyNo)
        {
            logger.Info("GetDataCustomer : Start");
            CustomerInfo customerInfo = new CustomerInfo();
            try
            {
                string baseQuery = string.Empty;
                baseQuery = QueryCollection.qGetDataCustomer;

                using (a2isDBHelper.Database db = new a2isDBHelper.Database("a2isRetailDB"))
                {
                    customerInfo = db.FirstOrDefault<CustomerInfo>(baseQuery, policyNo);
                }
            }
            catch (Exception e)
            {
                logger.Error("Error GetDataCustomer : " + e.Message);
            }

            logger.Info("GetDataCustomer : End");
            return customerInfo;
        }

        public static OrderSimulation GetDataOrderSimulation(RenewalNotice data)
        {
            logger.Info("GetDataOrderSimulation : Start");
            OrderSimulation orderSimulation = new OrderSimulation();
            try
            {
                string baseQuery = QueryCollection.qGetOrderSimulation;
                using (a2isDBHelper.Database db = new a2isDBHelper.Database("a2isRetailDB"))
                {
                    orderSimulation = db.FirstOrDefault<OrderSimulation>(baseQuery, data.PolicyNo, data.NewPolicyId
                        ,data.NewPolicyNo,data.VANumber);
                }
            }
            catch (Exception e)
            {
                logger.Error("Error GetDataOrderSimulation : " + e.Message);
            }

            logger.Info("GetDataOrderSimulation : End");
            return orderSimulation;
        }

        public static OrderSimulationMV GetDataOrderSimulationMV(string policyNo)
        {
            logger.Info("GetDataOrderSimulationMV : Start");
            OrderSimulationMV orderSimulation = new OrderSimulationMV();
            try
            {
                string baseQuery = QueryCollection.qGetOrdSimMV;
                using (a2isDBHelper.Database db = new a2isDBHelper.Database("a2isRetailDB"))
                {
                    orderSimulation = db.FirstOrDefault<OrderSimulationMV>(baseQuery, policyNo);
                }
            }
            catch (Exception e)
            {
                logger.Error("Error GetDataOrderSimulationMV : " + e.Message);
            }

            logger.Info("GetDataOrderSimulationMV : End");
            return orderSimulation;
        }

        public static List<OrderSimulationInterest> GetDataOrderSimulationInterest(string policyNo)
        {
            logger.Info("GetDataOrderSimulationInterest : Start");
            List<OrderSimulationInterest> orderSimulationinterest = new List<OrderSimulationInterest>();
            try
            {
                string baseQuery = QueryCollection.qGetOrdSimInterest;
                using (a2isDBHelper.Database db = new a2isDBHelper.Database("a2isRetailDB"))
                {
                    orderSimulationinterest = db.Fetch<OrderSimulationInterest>(baseQuery, policyNo);
                }
            }
            catch (Exception e)
            {
                logger.Error("Error GetDataOrderSimulationInterest : " + e.Message);
            }

            logger.Info("GetDataOrderSimulationInterest : End");
            return orderSimulationinterest;
        }

        public static List<OrderSimulationCoverage> GetDataOrderSimulationCoverage(string policyNo)
        {
            logger.Info("GetDataOrderSimulationCoverage : Start");
            List<OrderSimulationCoverage> orderSimulationcoverage = new List<OrderSimulationCoverage>();
            try
            {
                string baseQuery = QueryCollection.qGetOrdSimCoverage;
                using (a2isDBHelper.Database db = new a2isDBHelper.Database("a2isRetailDB"))
                {
                    orderSimulationcoverage = db.Fetch<OrderSimulationCoverage>(baseQuery, policyNo);
                }
            }
            catch (Exception e)
            {
                logger.Error("Error GetDataOrderSimulationCoverage : " + e.Message);
            }

            logger.Info("GetDataOrderSimulationCoverage : End");
            return orderSimulationcoverage;
        }

        public static OtosalesOrderDataID GetDataOtosalesOrderDataID(string policyNo)
        {
            logger.Info("GetDataOtosalesOrderDataID : Start");
            OtosalesOrderDataID otosalesOrderID = new OtosalesOrderDataID();
            try
            {
                string baseQuery = QueryCollection.qGetTasklistOtosales;
                using (a2isDBHelper.Database db = new a2isDBHelper.Database("a2isRetailDB"))
                {
                    otosalesOrderID = db.Query<OtosalesOrderDataID>(baseQuery, policyNo).FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                logger.Error("Error GetDataOtosalesOrderDataID : " + e.Message);
            }

            logger.Info("GetDataOtosalesOrderDataID : End");
            return otosalesOrderID;
        }

        public static void InsertDataTasklistOtosales(RenewalNotice data, OtosalesOrderDataID data2)
        {
            logger.Info("InsertDataTasklistOtosales : Start");
            try
            {
                string baseQuery = QueryCollection.qInsertTasklistOtosales;
                using (a2isDBHelper.Database db = new a2isDBHelper.Database("a2isRetailDB"))
                {
                    db.Execute(baseQuery,data.PolicyNo,data.EndorsementNo,data.FUDate,data2.CustID,data2.FollowUpID,data2.OrderID);
                }
            }
            catch (Exception e)
            {
                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobileDB"))
                {
                    db.Execute(QueryCollection.qInsertRenewalLog, data.PolicyNo, "Error InsertDataTasklistOtosales : " + e.Message);
                }
                logger.Error("Error InsertDataTasklistOtosales : " + e.Message);
            }

            logger.Info("InsertDataTasklistOtosales : End");
        }

        public static List<ImageData> GetDataImageAAB(CustomerInfo custInfo)
        {
            logger.Info("GetDataImageAAB : Start");
            List<dynamic> listImage = new List<dynamic>();
            List<ImageData> listImageresult = new List<ImageData>();
            ImageData imageResult;
            try
            {
                if(!string.IsNullOrEmpty(custInfo.CustIDAAB) && !string.IsNullOrWhiteSpace(custInfo.CustIDAAB))
                {
                    string baseQuery = QueryCollection.qGetDataImageAAB;
                    using (a2isDBHelper.Database db = new a2isDBHelper.Database("a2isRetailDB"))
                    {
                        baseQuery = String.Format(baseQuery, Convert.ToString(ConfigurationManager.AppSettings["ImageType"]),"'"+custInfo.CustIDAAB+"','"+custInfo.OrderNo+"'");
                        listImage = db.Fetch<dynamic>(baseQuery);
                    }

                    if(listImage != null)
                    {
                        foreach (dynamic d in listImage)
                        {
                            imageResult = new ImageData();
                            imageResult.ImageId = Convert.ToString(d.ImageId);
                            imageResult.ImageName = Convert.ToString(d.ImageName);
                            imageResult.ImageType = Convert.ToString(d.ImageType);
                            imageResult.ProspectId = Convert.ToString(d.ProspectId);
                            imageResult.ImageThumbnailData = DecodeFromAAB2000ImageBytes(d.ImageThumbnailData);
                            imageResult.ImageMasterData = DecodeFromAAB2000ImageBytes(d.ImageMasterData);
                            listImageresult.Add(imageResult);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                logger.Error("Error GetDataImageAAB : " + e.Message);
            }

            logger.Info("GetDataImageAAB : End");
            return listImageresult;
        }

        private static byte[] DecodeFromAAB2000ImageBytes(byte[] sourceBytes)
        {
            byte[] encodedBytes = Encoding.Convert(Encoding.Unicode, Encoding.Default, sourceBytes);
            return encodedBytes;
        }

        //public static List<String> GetDataRenewalNotice()
        //{
        //    logger.Info("GetDataRenewalNotice : Start");
        //    try
        //    {
        //    }
        //    catch (Exception e)
        //    {
        //        logger.Error("Error GetDataRenewalNotice : " + e.Message);
        //    }

        //    logger.Info("GetDataRenewalNotice : End");
        //}

        public static string GetRenewalStatus(string PolicyNo) {
            logger.Info("GetRenewalStatus : Start");
            string result = string.Empty;
            var aabDB = new a2isDBHelper.Database("a2isRetailDB");
            var mblDB = new a2isDBHelper.Database("AABMobileDB");
            try
            {
                string baseQuery = QueryCollection.qGetRenewalStatus;
                List<dynamic> OrderInfo = aabDB.Fetch<dynamic>(baseQuery, PolicyNo);
                if (OrderInfo.Count > 0) {
                    string modApp = string.IsNullOrEmpty(OrderInfo.First().ModApp) ? "" : OrderInfo.First().ModApp;
                    List<string> listModAppByPinseg = ConfigurationManager.AppSettings["ModAPPByPinseg"].Split(',').ToList();
                    baseQuery = QueryCollection.qGetFollowUpStatus;
                    int FUpStatus = OrderInfo.First().FollowUpID != null ? mblDB.ExecuteScalar<int>(baseQuery, OrderInfo.First().FollowUpID) : 0;
                    if (listModAppByPinseg.Contains(modApp.Trim().ToUpper()))
                    {
                        if (!string.IsNullOrEmpty(OrderInfo.First().OrderNo) && !string.IsNullOrWhiteSpace(OrderInfo.First().OrderNo))
                        {
                            if (FUpStatus == 5)
                            {
                                result = "R0001"; //Sudah terbentuk order dan not deal, data diturunkan, status menjadi data baru diinput
                            }
                            else
                            {
                                result = "R0002"; //Sudah terbentuk order dan bukan not deal, data tidak diturunkan dan tidak update status
                            }
                        }
                        else {
                            result = "R0003"; //data tetap diturunkan, status menjadi data baru diinput walau status lamanya not deal
                        }
                    }
                    else {
                        if (!string.IsNullOrEmpty(OrderInfo.First().OrderNo) && !string.IsNullOrWhiteSpace(OrderInfo.First().OrderNo))
                        {
                            if (FUpStatus == 5)
                            {
                                result = "S0001"; //data diturunkan, status tidak terupdate
                            }
                            else {
                                result = "S0002"; //data tidak diturunkan, status tidak terupdate
                            }
                        }
                        else {
                            if (FUpStatus == 5)
                            {
                                result = "S0003"; //data diturunkan, status tidak terupdate
                            }
                            else {
                                result = "S0004"; //data diturunkan, status diupdate menjadi data baru diinput
                            }
                        }
                    }
                }
                return result;
            }
            catch (Exception e) 
            {
                logger.Error("Error GetRenewalStatus : " + e.Message);
                throw e;
            }
        }
    }
}
