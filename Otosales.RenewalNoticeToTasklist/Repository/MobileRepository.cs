﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using a2is.Framework.Monitoring;
using Otosales.RenewalNoticeToTasklist.Models;
using Otosales.RenewalNoticeToTasklist.General;
using a2is.Framework.DataAccess;
using Otosales.Repository;

namespace Otosales.RenewalNoticeToTasklist.Repository
{
    class MobileRepository : RepositoryBase
    {
        private static a2isLogHelper logger = new a2isLogHelper();
        static string GetCurrentMethodName()
        {
            return System.Reflection.MethodBase.GetCurrentMethod().Name;
        }

        public static OtosalesOrderDataID GenerateTasklist(CustomerInfo custInfo, OrderSimulation orderSimulation, OrderSimulationMV orderSimulationMV
            ,List<OrderSimulationInterest> listOSInterest, List<OrderSimulationCoverage> listOSCoverage, string policyNo
            ,OtosalesOrderDataID otosalesOrderDataID, List<ImageData> listImageData, string RenStatusCode)
        {
            logger.Info("GenerateTasklist : Start");
            string custID = string.Empty;
            string followUpID = string.Empty;
            string orderID = string.Empty;
            bool isEdit = false;
            List<dynamic> prevFollowUpStatus = new List<dynamic>();
            try
            {                    
                if(otosalesOrderDataID != null)
                {
                    custID = otosalesOrderDataID.CustID != null ? otosalesOrderDataID.CustID : System.Guid.NewGuid().ToString();
                    followUpID = otosalesOrderDataID.FollowUpID != null ? otosalesOrderDataID.FollowUpID : System.Guid.NewGuid().ToString();
                    orderID = otosalesOrderDataID.OrderID != null ? otosalesOrderDataID.OrderID : System.Guid.NewGuid().ToString();
                    if (RenStatusCode.Equals("S0001")|| RenStatusCode.Equals("S0003"))
                    {
                        using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobileDB"))
                        {
                            prevFollowUpStatus = db.Fetch<dynamic>("SELECT FollowUpStatus, FollowUpInfo FROM dbo.FollowUp WHERE FollowUpNo = @0 AND RowStatus = 1", followUpID);
                        }
                    }
                    ClearFailedData(custID, followUpID, orderID);
                    custID = System.Guid.NewGuid().ToString();
                    isEdit = true;
                }
                else
                {
                    custID = System.Guid.NewGuid().ToString();
                    followUpID = System.Guid.NewGuid().ToString();
                    orderID = System.Guid.NewGuid().ToString();
                }

                #region Sales Dealer dan Dealer Code
                int SalesmanCode = 0;
                int DealerCode = 0;
                int salesmancode;
                int dealercode;
                string querySalesman = "SELECT SalesmanCode FROM dbo.SalesmanDealer WHERE CustID = @0";
                string queryDealer = "SELECT DealerCode FROM dbo.Dealer WHERE CustID = @0";
                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobileDB"))
                {
                    if (custInfo.SalesDealer != null)
                        salesmancode = db.First<int>(querySalesman, custInfo.SalesDealer);
                    else
                        salesmancode = 0;

                    if (custInfo.DealerCode != null)
                        dealercode = db.First<int>(queryDealer, custInfo.DealerCode);
                    else
                        dealercode = 0;
                }
                SalesmanCode = salesmancode;
                
                DealerCode = dealercode;
                #endregion

                #region Insert Prospect Data
                string insertDataCustomer1 = QueryCollection.qInsertDataCustomer1; //

                string insertDataCustomer2 = QueryCollection.qInsertDataCustomer2; //ProspectCompany
                string insertFollowUpOrder = QueryCollection.qInsertFollowUpOrder; //FollowUp
                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobileDB"))
                {
                        db.Execute(insertDataCustomer1, custID, custInfo.Name, custInfo.Phone1, custInfo.Phone2, custInfo.Email1
                       , "", custInfo.CustIDAAB, custInfo.SalesOfficerID, DealerCode, SalesmanCode, custInfo.BranchCode
                       , custInfo.isCompany, custInfo.IdentityNo, custInfo.Gender, custInfo.ProspectID, custInfo.BirthDate, custInfo.Address
                       , custInfo.PostCode);

                        if (custInfo.isCompany == 1)
                            db.Execute(insertDataCustomer2, custID, custInfo.Name, followUpID, custInfo.Email1
                                , custInfo.CompanyNPWPNo, custInfo.CompanyNPWPDate, custInfo.CompanyNPWPAddress, custInfo.Address, custInfo.PostCode
                                , custInfo.PICPhone, custInfo.PIC);
                    if (prevFollowUpStatus.Count > 0)
                    {
                        db.Execute(insertFollowUpOrder, followUpID, 0, custID, custInfo.Name, custInfo.Phone1, custInfo.Phone2
                            , custInfo.SalesOfficerID, custInfo.Name, prevFollowUpStatus.First().FollowUpStatus, prevFollowUpStatus.First().FollowUpInfo, "Renewal Data From Renewal Notice", custInfo.BranchCode);
                    }
                    else
                    {
                        db.Execute(insertFollowUpOrder, followUpID, 0, custID, custInfo.Name, custInfo.Phone1, custInfo.Phone2
                            , custInfo.SalesOfficerID, custInfo.Name, 1, 1, "Renewal Data From Renewal Notice", custInfo.BranchCode);
                    }
                }
                #endregion

                #region Insert Order Simulation
                string insertOrderSimulation = QueryCollection.qInsertOrderSimulation;
                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobileDB"))
                {
                    db.Execute(insertOrderSimulation,orderID,custID,followUpID,orderSimulation.QuotationNo,orderSimulation.MultiYearF
                        ,orderSimulation.YearCoverage,orderSimulation.TLOPeriod,orderSimulation.ComprePeriod, orderSimulation.BranchCode
                        ,orderSimulation.SalesOfficerID,orderSimulation.PhoneSales, DealerCode, SalesmanCode
                        ,orderSimulation.ProductTypeCode,orderSimulation.AdminFee,orderSimulation.TotalPremium,orderSimulation.Phone1
                        ,orderSimulation.Phone2,orderSimulation.Email1,orderSimulation.Email2,orderSimulation.SendStatus
                        ,orderSimulation.InsuranceType,orderSimulation.ApplyF,orderSimulation.SendF,orderSimulation.LastInterestNo
                        ,orderSimulation.LastCoverageNo, policyNo, orderSimulation.NewPolicyNo,orderSimulation.NewPolicyId
                        ,orderSimulation.VANumber,orderSimulation.ProductCode,orderSimulation.PeriodFrom,orderSimulation.PeriodTo
                        ,orderSimulation.SegmentCode,orderSimulation.PolicyDeliverAddress, orderSimulation.PolicyDeliverPostalCode
                        , orderSimulation.PolicyDeliveryType, orderSimulation.PolicyDeliveryName);
                }
                #endregion

                #region Insert Order Simulation MV
                string insertOrderSimulationMV = QueryCollection.qInsertOrdSimMV;
                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobileDB"))
                {
                    db.Execute(insertOrderSimulationMV, orderID,orderSimulationMV.ObjectNo, orderSimulationMV.ProductTypeCode
                        , orderSimulationMV.VehicleCode, orderSimulationMV.BrandCode, orderSimulationMV.ModelCode
                        , orderSimulationMV.Series, orderSimulationMV.Type, orderSimulationMV.Sitting
                        , orderSimulationMV.Year, orderSimulationMV.CityCode, orderSimulationMV.UsageCode
                        , orderSimulationMV.SumInsured, orderSimulationMV.AccessSI, orderSimulationMV.RegistrationNumber
                        , orderSimulationMV.EngineNumber, orderSimulationMV.ChasisNumber, orderSimulationMV.IsNew
                        , orderSimulationMV.Color);
                }
                #endregion

                #region Insert Order Simulation Interest
                string insertOSInterest = QueryCollection.qInsertOrdSimInterest;
                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobileDB"))
                {
                    foreach (OrderSimulationInterest osI in listOSInterest)
                    {
                        db.Execute(insertOSInterest, orderID, osI.ObjectNo,osI.InterestNo,osI.InterestID,osI.Year
                            ,osI.Premium,osI.PeriodFrom,osI.PeriodTo ,osI.DeductibleCode);
                    }
                }
                #endregion

                #region Insert Order Simulation Coverage
                string insertOSCoverage = QueryCollection.qInsertOrdSimCoverage;
                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobileDB"))
                {
                    foreach (OrderSimulationCoverage osC in listOSCoverage)
                    {
                        db.Execute(insertOSCoverage, orderID, osC.ObjectNo, osC.InterestNo, osC.CoverageNo
                            , osC.CoverageID, osC.Rate, osC.SumInsured, osC.LoadingRate, osC.Loading
                            , osC.Premium, osC.IsBundling, osC.EntryPct,osC.Ndays,osC.ExcessRate,osC.CalcMethod
                            , osC.CoverPremium,osC.GrossPremium,osC.MaxSI,osC.Net1,osC.Net2,osC.Net3,osC.DeductibleCode
                            , osC.BeginDate,osC.EndDate);
                    }
                }
                #endregion

                
                OtosalesOrderDataID oData = new OtosalesOrderDataID();
                oData.CustID = custID;
                oData.FollowUpID = followUpID;
                oData.OrderID = orderID;

                #region Sync Image AAB
                ImageSyncProcess(listImageData,oData,custInfo.SalesOfficerID,policyNo);
                #endregion

                return oData;
            }
            catch (Exception e)
            {
                ClearFailedData(custID,followUpID,orderID);
                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobileDB"))
                {
                    db.Execute(QueryCollection.qInsertRenewalLog, policyNo, "Error GenerateTasklist : " + e.Message);
                }
                logger.Error("Error GenerateTasklist : " + e.Message);
                throw (e);
            }

            logger.Info("GenerateTasklist : End");
        }

        public static void ImageSyncProcess(List<ImageData> listImageData, OtosalesOrderDataID order, string salesman, string policyNo)
        {
            logger.Info("ImageSyncProcess : Start");
            try
            {
                string query = QueryCollection.qInsertImageData;
                string imageId = "";
                string[] imageList;
                foreach (ImageData i in listImageData)
                {
                    using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobileDB"))
                    {
                        query = QueryCollection.qInsertImageData;
                        imageList = i.ImageName.Split('.');
                        imageId = System.Guid.NewGuid().ToString();
                        db.Execute(query, imageId, order.FollowUpID, salesman, imageId + "." + imageList[1], i.ImageMasterData, i.ImageThumbnailData);
                    
                        switch (i.ImageType.ToUpper().Trim())
                        {
                            case "KTP":
                                query = String.Format(QueryCollection.qUpdateFU," IdentityCard = @0");
                                db.Execute(query, imageId + "." + imageList[1], order.FollowUpID);
                                break;
                            case "STNK":
                                query = String.Format(QueryCollection.qUpdateFU, " STNK = @0");
                                db.Execute(query, imageId + "." + imageList[1], order.FollowUpID);
                                break;
                            case "NPWP":
                                query = String.Format(QueryCollection.qUpdateProspectCompany, " NPWP = @0");
                                db.Execute(query, imageId + "." + imageList[1], order.FollowUpID);
                                break;
                            case "SIUP":
                                query = String.Format(QueryCollection.qUpdateFU, " SIUP = @0");
                                db.Execute(query, imageId + "." + imageList[1], order.FollowUpID);
                                break;
                            default:
                                Console.WriteLine(". . .");
                                break;
                        }
                    }
                }
                
            }
            catch (Exception e)
            {
                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobileDB"))
                {
                    db.Execute(QueryCollection.qInsertRenewalLog, policyNo, "Error ImageSyncProcess : " + e.Message);
                }
                logger.Error("Error ImageSyncProcess : " + e.Message);
                throw (e);
            }

            logger.Info("ImageSyncProcess : End");
        }

        private static string ConvertToDateTimeSQLServer(string data)
        {
            DateTime oDate = Convert.ToDateTime(data);
            string dateFormat = oDate.ToString("yyyy-MM-dd hh:mm:ss.fff");

            return dateFormat;
        }
        
        private static void ClearFailedData(string custID,string followUpID, string orderID)
        {
            using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobileDB"))
            {
                db.Execute(QueryCollection.qDeleteFailGT, custID,followUpID,orderID);
            }
        }
    }
}
