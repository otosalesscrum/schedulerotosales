﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otosales.RenewalNoticeToTasklist.General
{
    class QueryCollection
    {
        #region Query AAB Rennot

        #region Query Get Data Renewal Notice
        #region v1 old
        //      public static string qGetDataRenewalNoticeV1 = @"SELECT rn.new_policy_no AS NewPolicyNo,
        //NULL AS NewPolicyId,
        //a.Policy_No AS PolicyNo,
        //rn.VANumber AS VANumber,
        //rn.EntryDt AS FUDate,
        //rn.Endorsement_No AS EndorsementNo
        //      FROM renewal_notice AS rn WITH(NOLOCK)
        //      INNER JOIN policy a WITH ( NOLOCK ) ON a.policy_no = rn.Policy_No
        //INNER JOIN beyondmoss.AABMobile.dbo.AABBranch ab ON ab.branchcode = rn.branch_distribution
        //      LEFT OUTER JOIN Document f WITH ( NOLOCK ) ON a.policy_no = f.reference_no
        //                                    AND f.Document_Type_ID = 'RENNOT'
        //      LEFT OUTER JOIN Mst_state g WITH ( NOLOCK ) ON f.delivery_state = g.state
        //                                     AND g.type = 'DELIVR' 
        //LEFT JOIN TasklistOtosales tos WITH ( NOLOCK ) ON tos.policyno = rn.policy_no
        //                                      AND tos.RowStatus = 1
        //AND tos.Endorsementno = rn.endorsement_no 
        //INNER JOIN Mst_Salesman ms WITH ( NOLOCK ) ON ms.salesman_id=rn.salesman_id
        //LEFT JOIN beyondmoss.gardamobile.[dbo].[RenewalToTasklistOtosales] rto on rto.policyno = rn.policy_no
        //LEFT JOIN Mst_Order mo WITH(NOLOCK) ON mo.old_policy_no = rn.policy_no 
        //      inner join mst_product AS b on  a.product_code = b.product_code
        //      inner join mst_customer AS c on a.cust_id = c.cust_id
        //      inner join mst_customer AS d on a.policy_holder_code = d.cust_id
        //      inner join mst_customer AS e on a.payer_code = e.cust_id
        //left JOIN beyondmoss.AABMobile.dbo.SalesOfficer so on (so.SalesOfficerID = ms.user_id_otosales AND b.product_type != 6) OR 
        //(so.SalesOfficerID = rn.broker_code AND b.product_type = 6)
        //      WHERE  (CAST(a.period_to AS DATE) <= DATEADD(DD, 30, CAST(GETDATE() AS DATE))
        //	AND CAST(a.period_to AS DATE) >= CAST(GETDATE() AS DATE) ) 
        //      AND ISNULL(b.New_Product_code, '') <> ''
        //      AND a.Status = 'A'
        //      AND A.ORDER_STATUS IN ( '11', '9')
        //      AND A.RENEWAL_STATUS = 0
        //      AND rn.Status IN ('0', '1' )
        //      AND ((a.endorsement_no = 0 and A.ORDER_STATUS IN ('11','9')) or a.endorsement_no <> 0 ) 
        //AND (DATEDIFF(MINUTE, tos.FUDate , rn.EntryDt) > 10  
        //OR tos.policyno is null
        //)
        //AND rn.new_policy_no is not null
        //--AND mo.Order_No is null
        //AND (
        //	(b.product_type = 6 AND so.salesofficerid is not null) 
        //	OR 
        //	(ISNULL(ms.user_id_otosales, '') <> ''  AND b.product_type != 6 AND so.salesofficerid is not null)
        //	)
        //AND rto.policyno is null
        //      AND Modified_app is not null";
        #endregion

        public static string qGetDataRenewalNoticeV1 = @";SELECT * 
INTO #tempRenewthirtydays
FROM (SELECT p.Policy_No,rn.new_policy_no, rn.VANumber, rn.EntryDt, rn.Endorsement_No, ms.salesman_id, ms.User_Id_Otosales
,mp.Product_Code, mp.Product_Type, mp.New_Product_code, p.cust_id, p.policy_holder_code, p.payer_code, so.SalesOfficerID, p.period_to 
FROM dbo.Renewal_Notice rn 
LEFT JOIN TasklistOtosales tos WITH ( NOLOCK ) ON tos.policyno = rn.policy_no AND tos.RowStatus = 1
	AND tos.Endorsementno = rn.endorsement_no 
INNER JOIN dbo.Mst_Salesman ms WITH (NOLOCK) ON ms.Salesman_Id = rn.Salesman_Id --AND ms.Status = 1
INNER JOIN beyondmoss.AABMobile.dbo.AABBranch ab ON ab.branchcode = rn.branch_distribution
INNER JOIN policy p WITH ( NOLOCK ) ON p.policy_no = rn.Policy_No
inner join mst_product AS mp on p.product_code = mp.product_code 
	AND mp.Product_Type = '6' AND rn.Broker_Code IS NOT NULL AND rn.Broker_Code <> ''
INNER JOIN BEYONDMOSS.AABMobile.dbo.SalesOfficer so ON so.SalesOfficerID = rn.Broker_Code
WHERE rn.Status IN ('0', '1' )
AND rn.new_policy_no is not null
AND rn.MODIFIED_APP is not NULL
AND (DATEDIFF(MINUTE, tos.FUDate , rn.EntryDt) > 10  
		OR tos.policyno is null)
AND p.Status = 'A'
AND p.ORDER_STATUS IN ( '11', '9')
AND p.RENEWAL_STATUS = 0
AND ((p.endorsement_no = 0 and p.ORDER_STATUS IN ('11','9')) or p.endorsement_no <> 0 ) 
UNION
SELECT p.Policy_No,rn.new_policy_no, rn.VANumber, rn.EntryDt, rn.Endorsement_No, ms.salesman_id, ms.User_Id_Otosales
,mp.Product_Code, mp.Product_Type, mp.New_Product_code, p.cust_id, p.policy_holder_code, p.payer_code, so.SalesOfficerID, p.period_to  
FROM dbo.Renewal_Notice rn 
LEFT JOIN TasklistOtosales tos WITH ( NOLOCK ) ON tos.policyno = rn.policy_no AND tos.RowStatus = 1
	AND tos.Endorsementno = rn.endorsement_no 
INNER JOIN dbo.Mst_Salesman ms ON rn.Salesman_Id = ms.Salesman_Id --AND ms.Status = 1
INNER JOIN beyondmoss.AABMobile.dbo.AABBranch ab ON ab.branchcode = rn.branch_distribution
INNER JOIN policy p WITH ( NOLOCK ) ON p.policy_no = rn.Policy_No
inner join mst_product AS mp on p.product_code = mp.product_code 
	AND mp.Product_Type <> '6' AND ms.User_Id_Otosales IS NOT NULL AND ms.User_Id_Otosales <> ''
INNER JOIN BEYONDMOSS.AABMobile.dbo.SalesOfficer so ON so.SalesOfficerID = ms.User_Id_Otosales
WHERE rn.Status IN ('0', '1' )
AND rn.new_policy_no is not null
AND rn.MODIFIED_APP is not NULL
AND (DATEDIFF(MINUTE, tos.FUDate , rn.EntryDt) > 10  
		OR tos.policyno is null)
AND p.Status = 'A'
AND p.ORDER_STATUS IN ( '11', '9')
AND p.RENEWAL_STATUS = 0
AND ((p.endorsement_no = 0 and p.ORDER_STATUS IN ('11','9')) or p.endorsement_no <> 0 ) 
) a

SELECT tmp.new_policy_no AS NewPolicyNo,
		NULL AS NewPolicyId,
		tmp.Policy_No AS PolicyNo,
		tmp.VANumber AS VANumber,
		tmp.EntryDt AS FUDate,
		tmp.Endorsement_No AS EndorsementNo
        FROM #tempRenewthirtydays tmp
        LEFT OUTER JOIN Document f WITH ( NOLOCK ) ON tmp.policy_no = f.reference_no
                                      AND f.Document_Type_ID = 'RENNOT'
        LEFT OUTER JOIN Mst_state g WITH ( NOLOCK ) ON f.delivery_state = g.state
                                       AND g.type = 'DELIVR' 
		LEFT JOIN beyondmoss.gardamobile.[dbo].[RenewalToTasklistOtosales] rto on rto.policyno = tmp.policy_no
		--LEFT JOIN Mst_Order mo WITH(NOLOCK) ON mo.old_policy_no = tmp.policy_no 
        inner join mst_product AS b on  tmp.product_code = b.product_code
        --inner join mst_customer AS c on tmp.cust_id = c.cust_id
        --inner join mst_customer AS d on tmp.policy_holder_code = d.cust_id
        --inner join mst_customer AS e on tmp.payer_code = e.cust_id
        WHERE (CAST(tmp.period_to AS DATE) <= DATEADD(DD, 30, CAST(GETDATE() AS DATE))
			AND CAST(tmp.period_to AS DATE) >= CAST(GETDATE() AS DATE) ) 
        AND ISNULL(tmp.New_Product_code, '') <> ''
		AND (
			(b.product_type = 6 AND tmp.salesofficerid is not null) 
			OR 
			(ISNULL(tmp.user_id_otosales, '') <> ''  AND b.product_type != 6 AND tmp.salesofficerid is not null)
			)
		AND rto.policyno is null

DROP TABLE #tempRenewthirtydays";

        #region query v2 old
        //      public static string qGetDataRenewalNoticeV2 = @"SELECT rn.new_policy_no AS NewPolicyNo,
        //NULL AS NewPolicyId,
        //a.Policy_No AS PolicyNo,
        //rn.VANumber AS VANumber,
        //rn.EntryDt AS FUDate,
        //rn.Endorsement_No AS EndorsementNo
        //      FROM renewal_notice AS rn WITH(NOLOCK)
        //      INNER JOIN policy a WITH ( NOLOCK ) ON a.policy_no = rn.Policy_No
        //INNER JOIN beyondmoss.AABMobile.dbo.AABBranch ab ON ab.branchcode = rn.branch_distribution
        //      LEFT OUTER JOIN Document f WITH ( NOLOCK ) ON a.policy_no = f.reference_no
        //                                    AND f.Document_Type_ID = 'RENNOT'
        //      LEFT OUTER JOIN Mst_state g WITH ( NOLOCK ) ON f.delivery_state = g.state
        //                                     AND g.type = 'DELIVR' 
        //LEFT JOIN TasklistOtosales tos WITH ( NOLOCK ) ON tos.policyno = rn.policy_no
        //                                      AND tos.RowStatus = 1
        //AND tos.Endorsementno = rn.endorsement_no 
        //INNER JOIN Mst_Salesman ms WITH ( NOLOCK ) ON ms.salesman_id=rn.salesman_id
        //LEFT JOIN beyondmoss.gardamobile.[dbo].[RenewalToTasklistOtosales] rto on rto.policyno = rn.policy_no
        //LEFT JOIN Mst_Order mo WITH(NOLOCK) ON mo.old_policy_no = rn.policy_no 
        //      inner join mst_product AS b on  a.product_code = b.product_code
        //      inner join mst_customer AS c on a.cust_id = c.cust_id
        //      inner join mst_customer AS d on a.policy_holder_code = d.cust_id
        //      inner join mst_customer AS e on a.payer_code = e.cust_id
        //left JOIN beyondmoss.AABMobile.dbo.SalesOfficer so on (so.SalesOfficerID = ms.user_id_otosales AND b.product_type != 6) OR 
        //(so.SalesOfficerID = rn.broker_code AND b.product_type = 6)
        //      WHERE   (CAST(a.period_to AS DATE) < CAST(GETDATE() AS DATE)
        //	AND (DATEDIFF(day,getdate(),a.period_to) < 0 AND DATEDIFF(day,getdate(),a.period_to) >= -365) )
        //      AND ISNULL(b.New_Product_code, '') <> ''
        //      AND a.Status = 'A'
        //      AND A.ORDER_STATUS IN ( '11', '9' )
        //      AND A.RENEWAL_STATUS = 0
        //      AND rn.Status IN ('0', '1' )
        //      AND ((a.endorsement_no = 0 and A.ORDER_STATUS IN ('11','9')) or a.endorsement_no <> 0 ) 
        //AND (DATEDIFF(MINUTE, tos.FUDate , rn.EntryDt) > 10  
        //OR tos.policyno is null
        //)
        //AND rn.new_policy_no is not null
        //--AND mo.Order_No is null
        //AND (
        //	(b.product_type = 6 AND so.salesofficerid is not null) 
        //	OR 
        //	(ISNULL(ms.user_id_otosales, '') <> ''  AND b.product_type != 6 AND so.salesofficerid is not null)
        //	)
        //AND rto.policyno is null
        //      AND Modified_app is not null";
        #endregion
        public static string qGetDataRenewalNoticeV2 = @";SELECT * 
INTO #tempRenewOneYear
FROM (SELECT p.Policy_No,rn.new_policy_no, rn.VANumber, rn.EntryDt, rn.Endorsement_No, ms.salesman_id, ms.User_Id_Otosales
,mp.Product_Code, mp.Product_Type, mp.New_Product_code, p.cust_id, p.policy_holder_code, p.payer_code, so.SalesOfficerID, p.period_to 
FROM dbo.Renewal_Notice rn 
LEFT JOIN TasklistOtosales tos WITH ( NOLOCK ) ON tos.policyno = rn.policy_no AND tos.RowStatus = 1
	AND tos.Endorsementno = rn.endorsement_no 
INNER JOIN dbo.Mst_Salesman ms WITH (NOLOCK) ON ms.Salesman_Id = rn.Salesman_Id --AND ms.Status = 1
INNER JOIN beyondmoss.AABMobile.dbo.AABBranch ab ON ab.branchcode = rn.branch_distribution
INNER JOIN policy p WITH ( NOLOCK ) ON p.policy_no = rn.Policy_No
inner join mst_product AS mp on p.product_code = mp.product_code 
	AND mp.Product_Type = '6' AND rn.Broker_Code IS NOT NULL AND rn.Broker_Code <> ''
INNER JOIN BEYONDMOSS.AABMobile.dbo.SalesOfficer so ON so.SalesOfficerID = rn.Broker_Code
WHERE rn.Status IN ('0', '1' )
AND rn.new_policy_no is not null
AND rn.MODIFIED_APP is not NULL
AND (DATEDIFF(MINUTE, tos.FUDate , rn.EntryDt) > 10  
		OR tos.policyno is null)
AND p.Status = 'A'
AND p.ORDER_STATUS IN ( '11', '9')
AND p.RENEWAL_STATUS = 0
AND ((p.endorsement_no = 0 and p.ORDER_STATUS IN ('11','9')) or p.endorsement_no <> 0 ) 
UNION
SELECT p.Policy_No,rn.new_policy_no, rn.VANumber, rn.EntryDt, rn.Endorsement_No, ms.salesman_id, ms.User_Id_Otosales
,mp.Product_Code, mp.Product_Type, mp.New_Product_code, p.cust_id, p.policy_holder_code, p.payer_code, so.SalesOfficerID, p.period_to  
FROM dbo.Renewal_Notice rn 
LEFT JOIN TasklistOtosales tos WITH ( NOLOCK ) ON tos.policyno = rn.policy_no AND tos.RowStatus = 1
	AND tos.Endorsementno = rn.endorsement_no 
INNER JOIN dbo.Mst_Salesman ms ON rn.Salesman_Id = ms.Salesman_Id --AND ms.Status = 1
INNER JOIN beyondmoss.AABMobile.dbo.AABBranch ab ON ab.branchcode = rn.branch_distribution
INNER JOIN policy p WITH ( NOLOCK ) ON p.policy_no = rn.Policy_No
inner join mst_product AS mp on p.product_code = mp.product_code 
	AND mp.Product_Type <> '6' AND ms.User_Id_Otosales IS NOT NULL AND ms.User_Id_Otosales <> ''
INNER JOIN BEYONDMOSS.AABMobile.dbo.SalesOfficer so ON so.SalesOfficerID = ms.User_Id_Otosales
WHERE rn.Status IN ('0', '1' )
AND rn.new_policy_no is not null
AND rn.MODIFIED_APP is not NULL
AND (DATEDIFF(MINUTE, tos.FUDate , rn.EntryDt) > 10  
		OR tos.policyno is null)
AND p.Status = 'A'
AND p.ORDER_STATUS IN ( '11', '9')
AND p.RENEWAL_STATUS = 0
AND ((p.endorsement_no = 0 and p.ORDER_STATUS IN ('11','9')) or p.endorsement_no <> 0 ) 
) a

SELECT tmp.new_policy_no AS NewPolicyNo,
		NULL AS NewPolicyId,
		tmp.Policy_No AS PolicyNo,
		tmp.VANumber AS VANumber,
		tmp.EntryDt AS FUDate,
		tmp.Endorsement_No AS EndorsementNo
        FROM #tempRenewOneYear tmp
        LEFT OUTER JOIN Document f WITH ( NOLOCK ) ON tmp.policy_no = f.reference_no
                                      AND f.Document_Type_ID = 'RENNOT'
        LEFT OUTER JOIN Mst_state g WITH ( NOLOCK ) ON f.delivery_state = g.state
                                       AND g.type = 'DELIVR' 
		LEFT JOIN beyondmoss.gardamobile.[dbo].[RenewalToTasklistOtosales] rto on rto.policyno = tmp.policy_no
		--LEFT JOIN Mst_Order mo WITH(NOLOCK) ON mo.old_policy_no = tmp.policy_no 
        inner join mst_product AS b on  tmp.product_code = b.product_code
        --inner join mst_customer AS c on tmp.cust_id = c.cust_id
        --inner join mst_customer AS d on tmp.policy_holder_code = d.cust_id
        --inner join mst_customer AS e on tmp.payer_code = e.cust_id
        WHERE   (CAST(tmp.period_to AS DATE) < CAST(GETDATE() AS DATE)
			AND (DATEDIFF(day,getdate(),tmp.period_to) < 0 AND DATEDIFF(day,getdate(),tmp.period_to) >= -365) )
        AND ISNULL(tmp.New_Product_code, '') <> ''
		AND (
			(b.product_type = 6 AND tmp.salesofficerid is not null) 
			OR 
			(ISNULL(tmp.user_id_otosales, '') <> ''  AND b.product_type != 6 AND tmp.salesofficerid is not null)
			)
		AND rto.policyno is null

DROP TABLE #tempRenewOneYear";
        #endregion

        #region Get Data Customer
        public static string qGetDataCustomer = @"SELECT mc.Name AS Name,
		    mc.Email AS Email1,
		    mcp.HP AS Phone1,
		    mcp.HP_2 AS Phone2,
		    mc.Cust_Id AS CustIDAAB,
		    CASE WHEN mpr.product_type = 6 
		    THEN 
		    CASE WHEN rn.broker_code is null THEN CASE WHEN ms.User_Id_Otosales is null THEN LTRIM(RTRIM(rn.Salesman_Id)) ELSE LTRIM(RTRIM(ms.User_Id_Otosales)) END 
		    ELSE rn.broker_code
		    END
		    ELSE
            CASE WHEN ms.User_Id_Otosales is null THEN LTRIM(RTRIM(rn.Salesman_Id)) ELSE LTRIM(RTRIM(ms.User_Id_Otosales)) END 
		    END AS SalesOfficerID,
		    rn.Salesman_Dealer_Code AS SalesDealer,
		    rn.Dealer_Code AS DealerCode,
		    rn.Branch_distribution AS BranchCode,
		    CASE WHEN mc.Cust_Type = 2 THEN 1 ELSE 0 END AS isCompany, 
		    isnull(mcp.id_card,'') AS IdentityNo,
		    CASE WHEN isnull(mcp.sex , '') = '' THEN '' 
		    WHEN isnull(mcp.sex , '') = '1' THEN 'M'
		    ELSE 'F' END AS Gender,
		    mp.Prospect_Id AS ProspectID,
		    mc.Birth_Date AS BirthDate,
		    CASE WHEN mc.Cust_Type = 2 THEN mc.Office_Address ELSE mc.Home_Address END AS [Address],
		    CASE WHEN mc.Cust_Type = 2 THEN mc.Office_PostCode ELSE mc.Home_PostCode END AS PostCode,
		    isnull(mca.AdditionalInfoValue,'') AS CompanyNPWPAddress,
		    isnull(mca2.AdditionalInfoValue,'') AS CompanyNPWPNo,
		    CASE WHEN mca3.AdditionalInfoValue is not null AND mca3.AdditionalInfoValue <> '' 
            THEN SUBSTRING(mca3.AdditionalInfoValue, 7, 4)+'-'+SUBSTRING(mca3.AdditionalInfoValue, 4, 2)+'-'+SUBSTRING(mca3.AdditionalInfoValue, 1, 2) 
            ELSE NULL END  AS CompanyNPWPDate,
			CASE WHEN mc.Cust_Type = 2 THEN ISNULL(mca4.AdditionalInfoValue,'') 
			ELSE '' END AS PICPhone,
			CASE WHEN mc.Cust_Type = 2 THEN ISNULL(mcpi.Name,'') 
			ELSE '' END AS PIC,
            a.order_no AS OrderNo
    FROM Policy AS a WITH(NOLOCK)
    INNER JOIN renewal_notice rn WITH ( NOLOCK ) ON a.policy_no = rn.Policy_No
    INNER JOIN mst_customer mc WITH ( NOLOCK ) ON a.policy_holder_code = mc.cust_id
    LEFT JOIN mst_prospect mp WITH ( NOLOCK ) ON mp.cust_id = a.policy_holder_code
    LEFT JOIN mst_salesman ms WITH ( NOLOCK ) ON ms.salesman_id = rn.Salesman_Id 	
    LEFT JOIN mst_cust_personal mcp WITH ( NOLOCK ) ON mcp.cust_id = a.policy_holder_code
    inner JOIN mst_product mpr WITH ( NOLOCK ) ON mpr.product_code = rn.new_product_code
    left join Mst_Customer_AdditionalInfo mca WITH ( NOLOCK ) ON mca.custid = a.policy_holder_code 
    AND mca.AdditionalCode IN ('NPAD')
    left join Mst_Customer_AdditionalInfo mca2 WITH ( NOLOCK ) ON mca2.custid = a.policy_holder_code 
    AND mca2.AdditionalCode IN ('NPWN')
    left join Mst_Customer_AdditionalInfo mca3 WITH ( NOLOCK ) ON mca3.custid = a.policy_holder_code 
    AND mca3.AdditionalCode IN ('NPDT')
    left join Mst_Customer_AdditionalInfo mca4 WITH ( NOLOCK ) ON mca4.custid = a.policy_holder_code 
    AND mca4.AdditionalCode IN ('MPC1')
	left join Mst_cust_pic mcpi WITH ( NOLOCK ) ON mcpi.cust_id = a.policy_holder_code
    WHERE a.policy_no = @0
		    AND a.Status = 'A'
            AND A.ORDER_STATUS IN ( '11','9' )
            AND A.RENEWAL_STATUS = 0
            AND rn.Status IN ('0', '1' )
            AND ((a.endorsement_no = 0 and A.ORDER_STATUS IN ('11','9')) or a.endorsement_no <> 0 )";
        #endregion

        #region Get Order Simulation
        public static string qGetOrderSimulation = @"SELECT 
		@1 AS NewPolicyId,
		@2 AS NewPolicyNo,
		@3 AS VANumber,
		'' AS QuotationNo,
		0 AS MultiYearF,
		1 AS YearCoverage,
		CASE WHEN rdc1.Coverage_Id is null THEN 0 ELSE 1 END AS TLOPeriod,
		CASE WHEN rdc2.Coverage_Id is null THEN 0 ELSE 1 END AS ComprePeriod,
		rn.Branch_distribution AS BranchCode,
		CASE WHEN mpr.product_type = 6 
		THEN 
		CASE WHEN rn.broker_code is null THEN CASE WHEN ms.User_Id_Otosales is null THEN LTRIM(RTRIM(rn.Salesman_Id)) ELSE LTRIM(RTRIM(ms.User_Id_Otosales)) END 
		ELSE rn.broker_code
		END
		ELSE
        CASE WHEN ms.User_Id_Otosales is null THEN LTRIM(RTRIM(rn.Salesman_Id)) ELSE LTRIM(RTRIM(ms.User_Id_Otosales)) END 
		END AS SalesOfficerID,
		'' AS PhoneSales,
		rn.Dealer_Code AS DealerCode,
		rn.Salesman_Dealer_Code AS SalesDealer,
		'GARDAOTO' AS ProductTypeCode,
		rp.Policy_Fee AS AdminFee,
		rp.gross_premium AS TotalPremium,
		mc.Home_Phone1 AS Phone1,
		mc.Home_Phone2 AS Phone2,
		mc.Email AS Email1,
		'' AS Email2,
		0 AS SendStatus,
		CASE WHEN mp.cob_id = '404' THEN 2 
		WHEN PDT.product_code is not null THEN 3 
        WHEN mp.biz_type = 2 AND mp.cob_id = '403' THEN 4
        ELSE 1 END AS InsuranceType,
		1 AS ApplyF,
		0 AS SendF,
		(SELECT top 1 Interest_no from Ren_Dtl_Interest where policy_no = @0) AS LastInterestNo,
		(SELECT top 1 coverage_no from Ren_Dtl_Coverage where policy_no = @0) AS LastCoverageNo,
		rn.New_Product_code AS ProductCode,
        rn.Due_Date AS PeriodFrom,
        DATEADD(YEAR, 1,rn.Due_Date) AS PeriodTo,
        rn.segment_code AS SegmentCode,
da.Address AS PolicyDeliverAddress,
da.Postal_Code AS PolicyDeliverPostalCode,
da.Delivery_Code AS PolicyDeliveryType,
da.ContactPerson AS PolicyDeliveryName
FROM Policy AS a WITH(NOLOCK)
INNER JOIN renewal_notice rn WITH ( NOLOCK ) ON a.policy_no = rn.Policy_No
INNER JOIN mst_customer mc WITH ( NOLOCK ) ON a.policy_holder_code = mc.cust_id
LEFT JOIN mst_salesman ms WITH ( NOLOCK ) ON ms.salesman_id = rn.Salesman_Id 
LEFT JOIN Ren_Dtl_Coverage rdc1 WITH ( NOLOCK ) ON  rdc1.policy_no = rn.policy_no
AND rdc1.interest_id = 'CASCO' AND rdc1.coverage_id = 'TLO'
LEFT JOIN Ren_Dtl_Coverage rdc2 WITH ( NOLOCK ) ON  rdc2.policy_no = rn.policy_no
AND rdc2.interest_id = 'CASCO' AND rdc2.coverage_id = 'ALLRIK'
LEFT JOIN Ren_premium rp WITH ( NOLOCK ) ON  rp.policy_no = rn.policy_no
INNER JOIN mst_product mp on mp.product_code = rn.new_product_code AND mp.Status = 1
LEFT JOIN dbo.Partner_Document_Template PDT ON PDT.Product_Code = mp.Product_Code
 AND PDT.Partner_ID = 'LEX' 
inner JOIN mst_product mpr WITH ( NOLOCK ) ON mpr.product_code = rn.new_product_code
left join dtl_address da on da.policy_id = a.policy_id AND da.address_type ='DELIVR'
WHERE a.policy_no = @0
		AND a.Status = 'A'
        AND A.ORDER_STATUS IN ( '11','9' )
        AND A.RENEWAL_STATUS = 0
        AND rn.Status IN ('0', '1' )
        AND ((a.endorsement_no = 0 and A.ORDER_STATUS IN ('11','9')) or a.endorsement_no <> 0 )";
        #endregion

        #region Get Order Simulation MV
        public static string qGetOrdSimMV = @"SELECT rdm.Object_No AS ObjectNo,
		'GARDAOTO' AS ProductTypeCode,
		rdm.Vehicle_Code AS VehicleCode,
		rdm.Brand_Code AS BrandCode,
		rdm.Model_Code AS ModelCode,
		rdm.Series  AS Series,
		rdm.Vehicle_Type AS [Type],
		rdm.Sitting_Capacity AS Sitting,
		rdm.Mfg_Year AS [Year],
		rdm.Geo_Area_Code AS CityCode,
		rdm.Usage_Code AS UsageCode,
		rdi.Sum_Insured AS SumInsured,
		ISNULL((SELECT SUM(Sum_Insured) FROM [dbo].[Ren_Dtl_Non_Standard_Accessories] where policy_no = @0),0) AS AccessSI,
		rdm.Registration_Number AS RegistrationNumber,
		rdm.Engine_Number AS EngineNumber,
		rdm.Chasis_Number AS ChasisNumber,
		rdm.New_Car AS IsNew,
        rdm.Color_On_BPKB AS Color
FROM Policy AS a WITH(NOLOCK)
INNER JOIN renewal_notice rn WITH ( NOLOCK ) ON a.policy_no = rn.Policy_No
INNER JOIN Ren_Dtl_Mv rdm WITH ( NOLOCK ) ON rdm.policy_no = rn.Policy_No
INNER JOIN Ren_Dtl_Interest rdi WITH ( NOLOCK ) ON rdi.policy_no = rn.policy_no 
AND rdi.Object_no = rdm.object_no
AND rdi.interest_id = 'CASCO'
WHERE a.policy_no = @0
		AND a.Status = 'A'
        AND A.ORDER_STATUS IN ( '11','9' )
        AND A.RENEWAL_STATUS = 0
        AND rn.Status IN ('0', '1' )
        AND ((a.endorsement_no = 0 and A.ORDER_STATUS IN ('11','9')) or a.endorsement_no <> 0 )";
        #endregion

        #region Get Order Simulation Interest
        public static string qGetOrdSimInterest = @"SELECT rdi.Object_No AS ObjectNo,
		rdi.Interest_No AS InterestNo,
		rdi.Interest_Id AS InterestID,
		1 AS [Year],
		(SELECT SUM(rdc.net_premium) FROM [dbo].[Ren_Dtl_Coverage] rdc WHERE rdc.policy_no = @0 
		and rdc.interest_no = rdi.Interest_No) AS Premium,
		rn.Due_Date AS PeriodFrom,
		DATEADD(YEAR, 1,rn.Due_Date) AS PeriodTo,
		rdi.Deductible_Code AS DeductibleCode
FROM Policy AS a WITH(NOLOCK)
INNER JOIN renewal_notice rn WITH ( NOLOCK ) ON a.policy_no = rn.Policy_No
INNER JOIN [Ren_Dtl_Interest] rdi WITH ( NOLOCK ) ON rdi.policy_no = rn.Policy_No
WHERE a.policy_no = @0
		AND a.Status = 'A'
        AND A.ORDER_STATUS IN ( '11','9' )
        AND A.RENEWAL_STATUS = 0
        AND rn.Status IN ('0', '1' )
        AND ((a.endorsement_no = 0 and A.ORDER_STATUS IN ('11','9')) or a.endorsement_no <> 0 )";
        #endregion

        #region Get Order Simulation Coverage
        public static string qGetOrdSimCoverage = @"SELECT rdc.Object_No AS ObjectNo,
		rdc.Interest_No AS InterestNo,
		rdc.Coverage_no AS CoverageNo,
		rdc.Coverage_ID AS CoverageID,
		rdc.Rate AS Rate,
		rdi.sum_insured AS SumInsured,
		rdc.Loading AS LoadingRate,
		(rdc.Loading*rdc.net_premium)/100 AS Loading,
		rdc.net_premium AS Premium,
		rn.Due_Date AS BeginDate,
		DATEADD(YEAR, 1,rn.Due_Date) AS EndDate,
		rdc.IsBundlingF AS IsBundling,
		100 AS EntryPct,
		DATEDIFF(day, rn.Due_Date, DATEADD(YEAR, 1,rn.Due_Date)) AS Ndays,
		rdc.Excess_Rate AS ExcessRate,
		CASE WHEN DATEDIFF(MONTH, rn.Due_Date, DATEADD(YEAR, 1,rn.Due_Date)) < 12 
		THEN 1 ELSE 0 END CalcMethod,
		rdc.Cover_Premium AS CoverPremium,
		rdc.Gross_Premium AS GrossPremium,
		rdc.Max_SI AS MaxSI,
		rdc.Net1 AS Net1,
		rdc.Net2 AS Net2,
		rdc.Net3 AS Net3,
		rdi.Deductible_Code AS DeductibleCode
FROM Policy AS a WITH(NOLOCK)
INNER JOIN renewal_notice rn WITH ( NOLOCK ) ON a.policy_no = rn.Policy_No
INNER JOIN [Ren_dtl_Interest] rdi WITH ( NOLOCK ) ON rdi.policy_no = rn.Policy_No
INNER JOIN [Ren_Dtl_Coverage] rdc WITH ( NOLOCK ) ON rdc.policy_no = rn.Policy_No AND rdi.interest_no = rdc.interest_no
WHERE a.policy_no = @0
		AND a.Status = 'A'
        AND A.ORDER_STATUS IN ( '11','9' )
        AND A.RENEWAL_STATUS = 0
        AND rn.Status IN ('0', '1' )
        AND ((a.endorsement_no = 0 and A.ORDER_STATUS IN ('11','9')) or a.endorsement_no <> 0 )";
        #endregion

        #region GetID TaskList Otosales
        public static string qGetTasklistOtosales = @"SELECT [CustID],[FollowUpID],[OrderID] FROM [dbo].[TasklistOtosales] WHERE PolicyNo = @0";
        #endregion

        #region Insert Tasklist Otosales
        public static string qInsertTasklistOtosales = @"DELETE FROM [dbo].[TasklistOtosales] WHERE [PolicyNo] = @0
INSERT INTO [dbo].[TasklistOtosales]
           ([PolicyNo]
           ,[EndorsementNo]
           ,[FUDate]
           ,[CustID]
           ,[FollowUpID]
           ,[OrderID]
           ,[CreatedBy]
           ,[CreatedDate]
           ,[ModifiedBy]
           ,[ModifiedDate]
           ,[RowStatus])
     VALUES
           (@0
           ,@1
           ,@2
           ,@3
           ,@4
           ,@5
           ,'sysol'
           ,GETDATE()
           ,NULL
           ,NULL
           ,1)";
        #endregion

        #region Get Data Image AAB
        public static string qGetDataImageAAB = @"SELECT dt1.Reference_No AS ProspectId, dt1.Reference_Type AS ImageType, MI.[File_Name] AS ImageName, 
        MI.Image_Id AS ImageId, MI.[Image] AS ImageMasterData, MI.Thumbnail AS ImageThumbnailData
        FROM
        (SELECT DI.Reference_No, DI.Reference_Type, MAX(DI.Image_Id) AS ImageId
        FROM dbo.Dtl_Image DI
        WHERE DI.Reference_No in ({1})
        AND DI.Reference_Type in ({0})
        GROUP BY DI.Reference_No, DI.Reference_Type) dt1
        INNER JOIN dbo.Mst_Image MI ON dt1.ImageId = MI.Image_Id";
        #endregion

        public static string qInsertRenewalLog = @"INSERT INTO [GardaMobile].[dbo].[RenewalToTasklistOtosales]
([PolicyNo],[ErrorDate],[ErrorMessage])
VALUES (@0--<PolicyNo, varchar(20),>
,GETDATE()--<ErrorDate, datetime,>
,@1--<ErrorMessage, varchar(max),>
)";
        #region Get RenewalStatus
        public static string qGetRenewalStatus = @"SELECT MODIFIED_APP ModApp, Order_No OrderNo, FollowUpID FROM dbo.Renewal_Notice rn
LEFT JOIN dbo.TasklistOtosales tot ON rn.Policy_No = tot.PolicyNo AND tot.RowStatus = 1 
LEFT JOIN dbo.Mst_Order mo ON mo.Old_Policy_No = rn.Policy_No
WHERE rn.Status IN ('0', '1' ) AND rn.new_policy_no is not NULL AND rn.Policy_No = @0";
        #endregion

        #endregion

        #region Query AABMobile

        #region Insert Data Customer
        public static string qInsertDataCustomer1 = @";
INSERT INTO ProspectCustomer([CustID]
      , [Name] --*
      , [Phone1] --*
      , [Phone2]
      , [Email1] --*
      , [Email2]
      , [CustIDAAB] --*
      , [SalesOfficerID] --*
      , [DealerCode] --*
      , [SalesDealer] --*
      , [BranchCode] --*
      , [isCompany] --*
      , [EntryDate]
      , [LastUpdatedTime]
      , [RowStatus]
      , [IdentityNo]
      , [CustGender]
      , [ProspectID]
      , [CustBirthDay]
      , [CustAddress]
      , [PostalCode])
      SELECT @0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,GETDATE(),GETDATE(),1,@12,@13,@14,@15,@16,@17";

        public static string qInsertDataCustomer2 = @"
INSERT INTO ProspectCompany ([CustID]
      ,[CompanyName] --*
      ,[FollowUpNo] --*
      ,[Email] --*
      ,[ModifiedDate]
      ,[RowStatus]
      ,[NPWPno]
      ,[NPWPdate]
      ,[NPWPaddress]
      ,[OfficeAddress]
      ,[PostalCode]
      ,[PICPhoneNo]
      ,[PICname]) VALUES(@0,@1,@2,@3,GETDATE(),1,@4,@5,@6,@7,@8,@9,@10)";
        #endregion

        #region Insert Follow Up
        public static string qInsertFollowUpOrder = @"
    INSERT INTO FollowUp ([FollowUpNo]
      ,[LastSeqNo]
      ,[CustID]
      ,[ProspectName]
      ,[Phone1]
      ,[Phone2]
      ,[SalesOfficerID]
      ,[EntryDate]
      ,[FollowUpName]
      ,[FollowUpStatus]
      ,[FollowUpInfo]
      ,[Remark]
      ,[BranchCode]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[IsRenewal]) 
SELECT @0,@1,@2,@3,@4,@5,@6,GETDATE(),@7,@8,@9,@10,@11,GETDATE(),1,1";
        #endregion

        #region Insert Order Simulation
        public static string qInsertOrderSimulation = @"
    INSERT INTO OrderSimulation ([OrderNo]
      ,[CustID]
      ,[FollowUpNo]
      ,[QuotationNo]
      ,[MultiYearF]
      ,[YearCoverage]
      ,[TLOPeriod]
      ,[ComprePeriod]
      ,[BranchCode]
      ,[SalesOfficerID]
      ,[PhoneSales]
      ,[DealerCode]
      ,[SalesDealer]
      ,[ProductTypeCode]
      ,[AdminFee]
      ,[TotalPremium]
      ,[Phone1]
      ,[Phone2]
      ,[Email1]
      ,[Email2]
      ,[SendStatus]
      ,[SendDate]
      ,[EntryDate]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[InsuranceType]
      ,[ApplyF]
      ,[SendF]
      ,[LastInterestNo]
      ,[LastCoverageNo]
      ,[OldPolicyNo]
      ,[PolicyNo]
      ,[PolicyID]
      ,[VANumber]
      ,[ProductCode]
      ,[PeriodFrom]
      ,[PeriodTo]
      ,[SegmentCode]
      ,[PolicyDeliveryAddress]
      ,[PolicyDeliveryPostalCode]
      ,[PolicySentTo]
      ,[PolicyDeliveryName]) 
	  SELECT @0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10, @11,@12,
        @13,@14,@15,@16,@17,@18,@19,@20,NULL,GETDATE(),GETDATE(),1,@21,@22,@23,@24,@25,@26,@27,@28,@29,@30,@31,@32,@33,@34,@35,@36,@37";
        #endregion

        #region Insert Order Simulation MV
        public static string qInsertOrdSimMV = @"DELETE FROM OrdersimulationMV WHERE OrderNo=@0
    INSERT INTO OrdersimulationMV ( [OrderNo]
      ,[ObjectNo]
      ,[ProductTypeCode]
      ,[VehicleCode]
      ,[BrandCode]
      ,[ModelCode]
      ,[Series]
      ,[Type]
      ,[Sitting]
      ,[Year]
      ,[CityCode]
      ,[UsageCode]
      ,[SumInsured]
      ,[AccessSI]
      ,[RegistrationNumber]
      ,[EngineNumber]
      ,[ChasisNumber]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[IsNew]
      ,[ColorOnBPKB]) 
	  SELECT @0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,GETDATE(),1,@17,@18";
        #endregion

        #region Insert Order Simulation Interest
        public static string qInsertOrdSimInterest = @"DELETE FROM OrderSimulationInterest WHERE OrderNo = @0 AND ObjectNo = @1 AND InterestNo = @2
INSERT INTO OrderSimulationInterest ([OrderNo]
      ,[ObjectNo]
      ,[InterestNo]
      ,[InterestID]
      ,[Year]
      ,[Premium]
      ,[PeriodFrom]
      ,[PeriodTo]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[Deductible_Code]) 
	  SELECT @0,@1,@2,@3,@4,@5,@6,@7,GETDATE(),1,@8";
        #endregion

        #region Insert Order Simulation Coverage
        public static string qInsertOrdSimCoverage = @"DELETE FROM OrderSimulationCoverage WHERE OrderNo = @0 AND ObjectNo = @1 AND InterestNo = @2 AND CoverageNo = @3
    INSERT INTO OrderSimulationCoverage ([OrderNo]
      ,[ObjectNo]
      ,[InterestNo]
      ,[CoverageNo]
      ,[CoverageID]
      ,[Rate]
      ,[SumInsured]
      ,[LoadingRate]
      ,[Loading]
      ,[Premium]
      ,[BeginDate]
      ,[EndDate]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[IsBundling]
      ,[Entry_Pct]
      ,[Ndays]
      ,[Excess_Rate]
      ,[Calc_Method]
      ,[Cover_Premium]
      ,[Gross_Premium]
      ,[Max_si]
      ,[Net1]
      ,[Net2]
      ,[Net3]
      ,[Deductible_Code]) 
	  SELECT @0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@22,@23,GETDATE(),1,@10,
            @11,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21";
        #endregion

        #region Delete Fail Generate Tasklist
        public static string qDeleteFailGT = @"DELETE FROM OrderSimulationCoverage WHERE OrderNo=@2
                                               DELETE FROM OrderSimulationInterest WHERE OrderNo=@2
                                               DELETE FROM OrdersimulationMV WHERE OrderNo=@2
                                               DELETE FROM OrderSimulation WHERE CustId=@0 AND FollowUpNo=@1 AND OrderNo=@2
                                               DELETE FROM FollowUp WHERE CustId=@0 AND FollowUpNo=@1
                                               ";
        #endregion

        #region Insert Image data
        public static string qInsertImageData = @"INSERT INTO dbo.ImageData
                                                ([ImageID],[DeviceID],[SalesOfficerID],[PathFile],[EntryDate]
                                                ,[LastUpdatedTime],[RowStatus],[Data],[ThumbnailData],[CoreImage_ID])
                                                SELECT @0,@1,@2,@3,GETDATE(),GETDATE(),1,@4,@5,NULL";
        #endregion

        #region Update Image Followup + ProspectCompany
        public static string qUpdateFU = @"UPDATE dbo.FollowUp 
                                        SET {0}
                                        WHERE FollowUpNo =@1";

        public static string qUpdateProspectCompany = @"UPDATE dbo.ProspectCompany 
                                                        SET {0}
                                                        WHERE FollowUpNo =@1";
        #endregion

        public static string qCheckRenewalData = "SELECT count(*) from ordersimulation where oldpolicyno = @0";

        #region GetFollowUpStatus
        public static string qGetFollowUpStatus = @"DECLARE @@Status INT
SELECT @@Status=FollowUpStatus FROM dbo.FollowUp WHERE FollowUpNo = @0 AND RowStatus = 1
IF(@@Status IS NOT NULL)
BEGIN
	SELECT @@Status
END
ELSE 
BEGIN
	SELECT 0
END";
        #endregion

        #endregion
    }
}
