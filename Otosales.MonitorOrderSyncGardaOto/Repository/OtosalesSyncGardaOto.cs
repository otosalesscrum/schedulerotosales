﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Otosales.Repository;
using Otosales.Logic;
using a2is.Framework.Monitoring;

namespace Otosales.MonitorOrderSyncGardaOto.Repository
{
    class OtosalesSyncGardaOto : RepositoryBase
    {
        private static a2isLogHelper logger = new a2isLogHelper();
        public static void SyncManageOrderGodigital()
        {

            string query = @"SELECT ParamValue FROM dbo.ApplicationParameters
                            WHERE ParamName='OTOSALES-MONITOR-ORDER-GODIGITAL-LAST-SYNC' AND RowStatus=1";

            string spSyncManageOrder = @";EXEC sp_GetSyncManageOrderOtoSales @0";
            string queryUpdateLastSync = @"UPDATE dbo.ApplicationParameters
                                           SET ParamValue = (SELECT convert(varchar, GETDATE(), 121))
                    WHERE ParamName='OTOSALES-MONITOR-ORDER-GODIGITAL-LAST-SYNC' AND RowStatus=1";

            using (var db = GetAABMobileDB())
            {
                string LastSync = db.ExecuteScalar<String>(query);
                db.FirstOrDefault<dynamic>(spSyncManageOrder, LastSync);
                db.Execute(queryUpdateLastSync);
            }
        }

        public static void UpdateLinkRenewalGodigital()
        {
            string query = @"SELECT ParamValue FROM dbo.ApplicationParameters
                            WHERE ParamName='OTOSALES-UPDATE-LINK-RENEWAL-GODIGITAL-LAST-SYNC' AND RowStatus=1";

            string spUpdateLinkRenewal = @";EXEC sp_GetUpdateLinkRenewalGodig @0";

            string queryUpdateLastSync = @"UPDATE dbo.ApplicationParameters
                                           SET ParamValue = (SELECT convert(varchar, GETDATE(), 121))
                    WHERE ParamName='OTOSALES-UPDATE-LINK-RENEWAL-GODIGITAL-LAST-SYNC' AND RowStatus=1";
            using (var db = GetAABMobileDB())
            {
                string LastSync = db.ExecuteScalar<String>(query);
                db.FirstOrDefault<dynamic>(spUpdateLinkRenewal, LastSync);
                db.Execute(queryUpdateLastSync);
            }
        }
        public static void SyncOrderGodigitalToOtosales() {
            string CurrentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            var db = GetAABMobileDB();
            try
            {
                string qGetOrderToSync = @";SELECT f.ReferenceNo FROM dbo.FollowUp f 
                                            INNER JOIN Asuransiastra.GODigital.OrderSimulation os
                                            ON os.TempPenawaranGuid = f.ReferenceNo
                                            WHERE COALESCE(f.ReferenceNo,'') <> ''
                                            AND os.ModifiedDate > (SELECT CAST(ParamValue AS DATETIME) 
                                            FROM dbo.ApplicationParameters WHERE ParamName = 'LASTSYNC-GODIGTOOTOSALES')
											AND f.RowStatus = 1 ORDER BY os.ModifiedDate";
                List<string> lsRefNo = db.Fetch<string>(qGetOrderToSync);
                foreach (string refno in lsRefNo)
                {
                    try
                    {
                        db.Execute("EXEC dbo.usp_SyncOrderGodigToOtosales @0 -- varchar(100)", refno);

                    }
                    catch (Exception e)
                    {
                        logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), "Error EXEC dbo.usp_SyncOrderGodigToOtosales"));
                    }
                }
                db.Execute("UPDATE dbo.ApplicationParameters SET ParamValue=GETDATE() WHERE ParamName = 'LASTSYNC-GODIGTOOTOSALES'");
            }
            catch (Exception e)
            {
                logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.Message));
            }
        }
    }
}
