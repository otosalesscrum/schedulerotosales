﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using a2is.Framework.Monitoring;
using Otosales.Logic;
using Otosales.MonitorOrderSyncGardaOto.Repository;

namespace Otosales.MonitorOrderSyncGardaOto
{
    class Program
    {
        private static a2isLogHelper logger = new a2isLogHelper();

        static void Main(string[] args)
        {
            MonitorSyncOrderGodigitalToOtosales();
            MonitorSyncManageOrderGodigitaln();
            UpdateLinkRenewalGodigital();
        }

        static void MonitorSyncOrderGodigitalToOtosales()
        {
            string CurrentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {

                logger.Debug(string.Format("Function {0} BEGIN:'", CurrentMethod));
                OtosalesSyncGardaOto.SyncOrderGodigitalToOtosales();
                logger.Debug(string.Format("Function {0} END:'", CurrentMethod));
            }
            catch (Exception e)
            {
                logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.Message));
            }
        }
        static void MonitorSyncManageOrderGodigitaln()
        {
            string CurrentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                
                logger.Debug(string.Format("Function {0} BEGIN:'", CurrentMethod));
                OtosalesSyncGardaOto.SyncManageOrderGodigital();
                logger.Debug(string.Format("Function {0} END:'", CurrentMethod));
            }
            catch (Exception e)
            {
                logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.Message));
            }
        }

        static void UpdateLinkRenewalGodigital()
        {
            string CurrentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {

                logger.Debug(string.Format("Function {0} BEGIN:'", CurrentMethod));
                OtosalesSyncGardaOto.UpdateLinkRenewalGodigital();
                logger.Debug(string.Format("Function {0} END:'", CurrentMethod));
            }
            catch (Exception e)
            {
                logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.Message));
            }
        }

    }
}
