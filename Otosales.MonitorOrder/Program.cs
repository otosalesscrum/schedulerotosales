﻿using a2is.Framework.Monitoring;
using Otosales.Logic;
using Otosales.MonitorOrder.Repository;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Otosales.Models.Constant;

namespace Otosales.MonitorOrder
{
    class Program
    {
        private static a2isLogHelper logger = new a2isLogHelper();

        static void Main(string[] args)
        {
            int schedulertype = Convert.ToInt32(ConfigurationManager.AppSettings["SchedulerType"]);
            if (schedulertype == 1)
            {
                MonitorOrder();
            }
            else if (schedulertype == 2)
            {
                MonitorOrderRenew();
            }

            //MonitorSendToSA();

        }

        static void MonitorOrder() {
            MonitorNeedSurveyStatus(); //OK
            MonitorBackToAOStatus(); //OK
            MonitorPolicyCreatedStatus(); //OK
            MonitorPolicyDeliveredStatus();
            MonitorOrderApprovalWithStatusRevise();
            MonitorOrderApprovalWithStatusReject();
        }

        static void MonitorOrderRenew() {
            MonitorOrderStatuIsRenewableDigital();
            MonitorOrderWithStatusWO();
            MonitorOrderWithStatusReCreate();
            MonitorOrderWithStatusCancel();
            MonitorOrderWithStatusEndorse();
            MonitorPotentialFUPembayaran();
            MonitorOrderRenewWithStatusNotDeal();
            //MonitorFUPembayaran();
        }
        /// <summary>
        /// 
        /// </summary>
        static void MonitorFUPembayaran()
        {
            string CurrentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                logger.Debug(string.Format("Function {0} BEGIN:'", CurrentMethod));
                OtosalesOrderRepository OOR = new OtosalesOrderRepository();
                string entryUsr = CommonLogic.GetSettingUserID();
                var followUp = OOR.GetOrderRenewSendToSAAndSubmitBySA();
                foreach (var fu in followUp)
                {
                    try
                    {
                        //TODO : jalanin proses delete & update foto dari otosales ke mst_order. perhatiin kalo udah jadi order, reference_no-nya apa, kalo gambar personal apa?
                        OOR.UpdateFollowUpToFUPembayaran(fu.PolicyOrderNo);
                        OrdLogic.UpdateOrderMobile(fu.OrderNo);
                        // TODO : create push notification

                    }
                    catch (Exception e)
                    {
                        logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.Message));
                    }
                }
                logger.Debug(string.Format("Function {0} END:'", CurrentMethod));
            }
            catch (Exception e)
            {
                logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.Message));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        static void MonitorPolicyCreatedStatus()
        {
            string CurrentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                logger.Debug(string.Format("Function {0} BEGIN:'", CurrentMethod));
                OtosalesOrderRepository OOR = new OtosalesOrderRepository();
                string entryUsr = CommonLogic.GetSettingUserID();
                //NEW
                var followUp = OOR.GetFollowUpStatusSendToSAorFUPaymentWithNeedApprovalAndOrderStatusPolicyCreated();
                foreach (var fu in followUp)
                {
                    try
                    {
                        OOR.UpdateFollowUpToPolicyCreated(fu.PolicyOrderNo);
                        OOR.UpdateRenewalStatusPolicy(fu.PolicyOrderNo);
                        OrdLogic.UpdateOrderMobile(fu.OrderNo);
                        // TODO : create push notification

                    }
                    catch (Exception e)
                    {
                        logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.Message));
                    }
                }
                logger.Debug(string.Format("Function {0} END:'", CurrentMethod));
            }
            catch (Exception e)
            {
                logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.Message));
            }
        }



        /// <summary>
        /// Support untuk 'SendBackToSA', untuk 'SendToSA' pertama kali ada di Otosales.CreateOrder karena disana yang handle pembentukan order.
        /// </summary>
        static void MonitorSendToSA()
        {
            string CurrentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                //TODO: tanya faishol, skrg sendtosa insert ke mst_order_mobile ? review logicnya gmn? karena disini juga ada tapi untuk policyorderNo yang udah ada.
                logger.Debug(string.Format("Function {0} BEGIN:'", CurrentMethod));
                OtosalesOrderRepository OOR = new OtosalesOrderRepository();
                string entryUsr = CommonLogic.GetSettingUserID();
                var followUp = OOR.GetFollowUpWithSendToSAStatus();
                foreach (var fu in followUp)
                {
                    //TODO : jalanin proses delete & update foto dari otosales ke mst_order. perhatiin kalo udah jadi order, reference_no-nya apa, kalo gambar personal apa?
                    OOR.AddToMstOrderMobile(fu.FollowUpNo, entryUsr, fu.RemarkToSA);
                }
                logger.Debug(string.Format("Function {0} END:'", CurrentMethod));
            }
            catch (Exception e)
            {
                logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.Message));
            }
        }

        static void MonitorBackToAOStatus()
        {
            string CurrentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                logger.Debug(string.Format("Function {0} BEGIN:'", CurrentMethod));
                OtosalesOrderRepository OOR = new OtosalesOrderRepository();
                var followUp = OOR.GetMstOrderMobileWithBackToAOStatus();
                foreach (var fu in followUp)
                {
                    try
                    {
                        OOR.UpdateFollowUpInfoToBackToAO(fu.FollowUpNo, fu.Remarks);
                        OrdLogic.UpdateOrderMobile(fu.OrderNo);

                    }
                    catch (Exception e)
                    {
                        logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.Message));
                    }
                }
                logger.Debug(string.Format("Function {0} END:'", CurrentMethod));
            }
            catch (Exception e)
            {
                logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.Message));
            }
        }

        static void MonitorNeedSurveyStatus()
        {
            string CurrentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                logger.Debug(string.Format("Function {0} BEGIN:'", CurrentMethod));
                OtosalesOrderRepository OOR = new OtosalesOrderRepository();
                var followUp = OOR.GetFollowUpWithSurveyStatusCompleted();
                foreach (var fu in followUp)
                {
                    OOR.UpdateFollowUpInfoToFollowUpSurveyResult(fu.FollowUpNo);
                }
                logger.Debug(string.Format("Function {0} END:'", CurrentMethod));
            }
            catch (Exception e)
            {
                logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.Message));
            }
        }

        static void MonitorPolicyDeliveredStatus()
        {
            string CurrentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                logger.Debug(string.Format("Function {0} BEGIN:'", CurrentMethod));
                OtosalesOrderRepository OOR = new OtosalesOrderRepository();
                var followUp = OOR.GetFollowUpWithStatusPolicyCreated();
                foreach (var fu in followUp)
                {
                    OOR.UpdateFollowUpInfoToFollowUpPolicyDeliveredOrRecived(fu.FollowUpNo, fu.Delivery_State);
                }
                logger.Debug(string.Format("Function {0} END:'", CurrentMethod));
            }
            catch (Exception e)
            {
                logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.Message));
            }
        }

        static void MonitorPotentialFUPembayaran()
        {
            string CurrentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                logger.Debug(string.Format("Function {0} BEGIN:'", CurrentMethod));
                OtosalesOrderRepository.UpdateRenewalOrderStatus();
                logger.Debug(string.Format("Function {0} END:'", CurrentMethod));
            }
            catch (Exception e)
            {
                logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.Message));
            }
        }

        static void MonitorOrderApprovalWithStatusRevise()
        {
            string CurrentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                logger.Debug(string.Format("Function {0} BEGIN:'", CurrentMethod));
                OtosalesOrderRepository OOR = new OtosalesOrderRepository();
                var order = OOR.GetMstOrderMobileWithStatusRevise();
                foreach (var ord in order)
                {
                    OOR.UpdateApprovalStatus(ord.Order_No, ord.ApprovalType, CommonLogic.GetSettingUserID());
                }
                logger.Debug(string.Format("Function {0} END:'", CurrentMethod));
            }
            catch (Exception e)
            {
                logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.Message));
            }
        }

        static void MonitorOrderApprovalWithStatusReject()
        {
            string CurrentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                logger.Debug(string.Format("Function {0} BEGIN:'", CurrentMethod));
                OtosalesOrderRepository OOR = new OtosalesOrderRepository();
                var order = OOR.GetMstOrderMobileWithStatusReject();
                foreach (var ord in order)
                {
                    OOR.UpdateFollowUpStatusReject(ord.Order_No);
                }
                logger.Debug(string.Format("Function {0} END:'", CurrentMethod));
            }
            catch (Exception e)
            {
                logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.Message));
            }
        }

        static void MonitorOrderStatuIsRenewableDigital()
        {
            string CurrentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                logger.Debug(string.Format("Function {0} BEGIN:'", CurrentMethod));
                OtosalesOrderRepository OOR = new OtosalesOrderRepository();
                var order = OOR.GetOrderRenewableDigital();
                foreach (var ord in order)
                {
                    OOR.UpdateStatusRenewableDigital(ord.FollowUpNo, true);
                }
                order = OOR.GetOrderNonRenewableDigital();
                foreach (var ord in order)
                {
                    OOR.UpdateStatusRenewableDigital(ord.FollowUpNo, false);
                }
                logger.Debug(string.Format("Function {0} END:'", CurrentMethod));
            }
            catch (Exception e)
            {
                logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.Message));
            }
        }

        static void MonitorOrderWithStatusWO()
        {
            string CurrentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                logger.Debug(string.Format("Function {0} BEGIN:'", CurrentMethod));
                OtosalesOrderRepository OOR = new OtosalesOrderRepository();
                var order = OOR.GetMstOrderWithStatusWO();
                foreach (var ord in order)
                {
                    OOR.ReGenerateOrder(ord.Old_Policy_No, ord.FollowUpNo, ord.Policy_No, Convert.ToInt32(FollowUpStatus.NotDeal), Convert.ToInt32(FollowUpInfo.PolicyWO));
                }
                logger.Debug(string.Format("Function {0} END:'", CurrentMethod));
            }
            catch (Exception e)
            {
                logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.Message));
            }
        }

        static void MonitorOrderWithStatusReCreate()
        {
            string CurrentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                logger.Debug(string.Format("Function {0} BEGIN:'", CurrentMethod));
                OtosalesOrderRepository OOR = new OtosalesOrderRepository();
                var order = OOR.GetMstOrderWithStatusRecreate();
                foreach (var ord in order)
                {
                    OOR.ReGenerateOrder(ord.OldPolicyNo, ord.FollowUpNo, ord.PolicyNo, Convert.ToInt32(FollowUpStatus.OrderRejectedStatusCode), Convert.ToInt32(FollowUpInfo.Recreate));
                }
                logger.Debug(string.Format("Function {0} END:'", CurrentMethod));
            }
            catch (Exception e)
            {
                logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.Message));
            }
        }

        static void MonitorOrderWithStatusCancel()
        {
            string CurrentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                logger.Debug(string.Format("Function {0} BEGIN:'", CurrentMethod));
                OtosalesOrderRepository OOR = new OtosalesOrderRepository();
                var order = OOR.GetMstOrderWithStatusCancel();
                foreach (var ord in order)
                {
                    OOR.ReGenerateOrder(ord.Old_Policy_No, ord.FollowUpNo, ord.Policy_No, Convert.ToInt32(FollowUpStatus.DealStatusCode), Convert.ToInt32(FollowUpInfo.PolicyCancel));
                }
                logger.Debug(string.Format("Function {0} END:'", CurrentMethod));
            }
            catch (Exception e)
            {
                logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.Message));
            }
        }
        static void MonitorOrderWithStatusEndorse()
        {
            string CurrentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                logger.Debug(string.Format("Function {0} BEGIN:'", CurrentMethod));
                OtosalesOrderRepository OOR = new OtosalesOrderRepository();
                var order = OOR.GetMstOrderWithStatusEndorse();
                foreach (var ord in order)
                {
                    OOR.UpdateEndorseOrder(ord.FollowUpNo, ord.OrderNo, ord.Order_No);
                }
                logger.Debug(string.Format("Function {0} END:'", CurrentMethod));
            }
            catch (Exception e)
            {
                logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.Message));
            }
        }
        static void MonitorOrderRenewWithStatusNotDeal()
        {
            string CurrentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                logger.Debug(string.Format("Function {0} BEGIN:'", CurrentMethod));
                OtosalesOrderRepository OOR = new OtosalesOrderRepository();
                var order = OOR.GetOrderRenewNotDealToRegenerate();
                foreach (var ord in order)
                {
                    OOR.ReGenerateOrderNotDeal(ord.OrderNo, ord.OldPolicyNo);
                }
                logger.Debug(string.Format("Function {0} END:'", CurrentMethod));
            }
            catch (Exception e)
            {
                logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.Message));
            }
        }

    }
}
