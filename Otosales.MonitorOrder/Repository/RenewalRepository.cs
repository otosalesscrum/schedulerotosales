﻿using a2is.Framework.Monitoring;
using Otosales.MonitorOrder.General;
using Otosales.MonitorOrder.Model;
using Otosales.Repository;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otosales.MonitorOrder.Repository
{
    class RenewalRepository : RepositoryBase
    {
        private static a2isLogHelper logger = new a2isLogHelper();
        public static CustomerInfo GetDataCustomer(string policyNo)
        {
            logger.Info("GetDataCustomer : Start");
            CustomerInfo customerInfo = new CustomerInfo();
            try
            {
                string baseQuery = string.Empty;
                baseQuery = QueryCollection.qGetDataCustomer;

                using (var db = GetAABDB())
                {
                    customerInfo = db.FirstOrDefault<CustomerInfo>(baseQuery, policyNo);
                }
            }
            catch (Exception e)
            {
                logger.Error("Error GetDataCustomer : " + e.Message);
            }

            logger.Info("GetDataCustomer : End");
            return customerInfo;
        }

        public static OrderSimulation GetDataOrderSimulation(string OldPolicyNo, string PolicyNo)
        {
            logger.Info("GetDataOrderSimulation : Start");
            OrderSimulation orderSimulation = new OrderSimulation();
            try
            {
                string baseQuery = QueryCollection.qGetOrderSimulation;
                using (var db = GetAABDB())
                {
                    orderSimulation = db.FirstOrDefault<OrderSimulation>(baseQuery, OldPolicyNo, PolicyNo);
                }
            }
            catch (Exception e)
            {
                logger.Error("Error GetDataOrderSimulation : " + e.Message);
            }

            logger.Info("GetDataOrderSimulation : End");
            return orderSimulation;
        }

        public static OrderSimulationMV GetDataOrderSimulationMV(string policyNo)
        {
            logger.Info("GetDataOrderSimulationMV : Start");
            OrderSimulationMV orderSimulation = new OrderSimulationMV();
            try
            {
                string baseQuery = QueryCollection.qGetOrdSimMV;
                using (var db = GetAABDB())
                {
                    orderSimulation = db.FirstOrDefault<OrderSimulationMV>(baseQuery, policyNo);
                }
            }
            catch (Exception e)
            {
                logger.Error("Error GetDataOrderSimulationMV : " + e.Message);
            }

            logger.Info("GetDataOrderSimulationMV : End");
            return orderSimulation;
        }

        public static List<OrderSimulationInterest> GetDataOrderSimulationInterest(string policyNo)
        {
            logger.Info("GetDataOrderSimulationInterest : Start");
            List<OrderSimulationInterest> orderSimulationinterest = new List<OrderSimulationInterest>();
            try
            {
                string baseQuery = QueryCollection.qGetOrdSimInterest;
                using (var db = GetAABDB())
                {
                    orderSimulationinterest = db.Fetch<OrderSimulationInterest>(baseQuery, policyNo);
                }
            }
            catch (Exception e)
            {
                logger.Error("Error GetDataOrderSimulationInterest : " + e.Message);
            }

            logger.Info("GetDataOrderSimulationInterest : End");
            return orderSimulationinterest;
        }

        public static List<OrderSimulationCoverage> GetDataOrderSimulationCoverage(string policyNo)
        {
            logger.Info("GetDataOrderSimulationCoverage : Start");
            List<OrderSimulationCoverage> orderSimulationcoverage = new List<OrderSimulationCoverage>();
            try
            {
                string baseQuery = QueryCollection.qGetOrdSimCoverage;
                using (var db = GetAABDB())
                {
                    orderSimulationcoverage = db.Fetch<OrderSimulationCoverage>(baseQuery, policyNo);
                }
            }
            catch (Exception e)
            {
                logger.Error("Error GetDataOrderSimulationCoverage : " + e.Message);
            }

            logger.Info("GetDataOrderSimulationCoverage : End");
            return orderSimulationcoverage;
        }

        public static OtosalesOrderDataID GetDataOtosalesOrderDataID(string policyNo)
        {
            logger.Info("GetDataOtosalesOrderDataID : Start");
            OtosalesOrderDataID otosalesOrderID = new OtosalesOrderDataID();
            try
            {
                string baseQuery = QueryCollection.qGetTasklistOtosales;
                using (var db = GetAABDB())
                {
                    otosalesOrderID = db.Query<OtosalesOrderDataID>(baseQuery, policyNo).FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                logger.Error("Error GetDataOtosalesOrderDataID : " + e.Message);
            }

            logger.Info("GetDataOtosalesOrderDataID : End");
            return otosalesOrderID;
        }

        public static void InsertDataTasklistOtosales(string PolicyNo, OtosalesOrderDataID data)
        {
            logger.Info("InsertDataTasklistOtosales : Start");
            try
            {
                string baseQuery = QueryCollection.qInsertTasklistOtosales;
                using (var db = GetAABDB())
                {
                    db.Execute(baseQuery, PolicyNo, data.CustID, data.FollowUpID, data.OrderID);
                }
            }
            catch (Exception e)
            {
                using (var db = GetAABMobileDB())
                {
                    db.Execute(QueryCollection.qInsertRenewalLog, PolicyNo, "Error InsertDataTasklistOtosales : " + e.Message);
                }
                logger.Error("Error InsertDataTasklistOtosales : " + e.Message);
            }

            logger.Info("InsertDataTasklistOtosales : End");
        }

        public static List<ImageData> GetDataImageAAB(CustomerInfo custInfo)
        {
            logger.Info("GetDataImageAAB : Start");
            List<dynamic> listImage = new List<dynamic>();
            List<ImageData> listImageresult = new List<ImageData>();
            ImageData imageResult;
            try
            {
                if (!string.IsNullOrEmpty(custInfo.CustIDAAB) && !string.IsNullOrWhiteSpace(custInfo.CustIDAAB))
                {
                    string baseQuery = QueryCollection.qGetDataImageAAB;
                    using (var db = GetAABDB())
                    {
                        baseQuery = String.Format(baseQuery, Convert.ToString(ConfigurationManager.AppSettings["ImageType"]), "'" + custInfo.CustIDAAB + "','" + custInfo.OrderNo + "'");
                        listImage = db.Fetch<dynamic>(baseQuery);
                    }

                    if (listImage != null)
                    {
                        foreach (dynamic d in listImage)
                        {
                            imageResult = new ImageData();
                            imageResult.ImageId = Convert.ToString(d.ImageId);
                            imageResult.ImageName = Convert.ToString(d.ImageName);
                            imageResult.ImageType = Convert.ToString(d.ImageType);
                            imageResult.ProspectId = Convert.ToString(d.ProspectId);
                            imageResult.ImageThumbnailData = DecodeFromAAB2000ImageBytes(d.ImageThumbnailData);
                            imageResult.ImageMasterData = DecodeFromAAB2000ImageBytes(d.ImageMasterData);
                            listImageresult.Add(imageResult);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                logger.Error("Error GetDataImageAAB : " + e.Message);
            }

            logger.Info("GetDataImageAAB : End");
            return listImageresult;
        }

        private static byte[] DecodeFromAAB2000ImageBytes(byte[] sourceBytes)
        {
            byte[] encodedBytes = Encoding.Convert(Encoding.Unicode, Encoding.Default, sourceBytes);
            return encodedBytes;
        }

        public static OtosalesOrderDataID GenerateTasklist(CustomerInfo custInfo, OrderSimulation orderSimulation, OrderSimulationMV orderSimulationMV
    , List<OrderSimulationInterest> listOSInterest, List<OrderSimulationCoverage> listOSCoverage, string policyNo
    , OtosalesOrderDataID otosalesOrderDataID, List<ImageData> listImageData)
        {
            logger.Info("GenerateTasklist : Start");
            string custID = string.Empty;
            string followUpID = string.Empty;
            string orderID = string.Empty;
            bool isEdit = false;
            try
            {
                if (otosalesOrderDataID != null)
                {
                    custID = otosalesOrderDataID.CustID != null ? otosalesOrderDataID.CustID : System.Guid.NewGuid().ToString();
                    followUpID = otosalesOrderDataID.FollowUpID != null ? otosalesOrderDataID.FollowUpID : System.Guid.NewGuid().ToString();
                    orderID = otosalesOrderDataID.OrderID != null ? otosalesOrderDataID.OrderID : System.Guid.NewGuid().ToString();
                    ClearFailedData(custID, followUpID, orderID);
                    custID = System.Guid.NewGuid().ToString();
                    isEdit = true;
                }
                else
                {
                    custID = System.Guid.NewGuid().ToString();
                    followUpID = System.Guid.NewGuid().ToString();
                    orderID = System.Guid.NewGuid().ToString();
                }

                #region Sales Dealer dan Dealer Code
                int SalesmanCode = 0;
                int DealerCode = 0;
                int salesmancode;
                int dealercode;
                string querySalesman = "SELECT SalesmanCode FROM dbo.SalesmanDealer WHERE CustID = @0";
                string queryDealer = "SELECT DealerCode FROM dbo.Dealer WHERE CustID = @0";
                using (var db = GetAABMobileDB())
                {
                    if (custInfo.SalesDealer != null)
                        salesmancode = db.First<int>(querySalesman, custInfo.SalesDealer);
                    else
                        salesmancode = 0;

                    if (custInfo.DealerCode != null)
                        dealercode = db.First<int>(queryDealer, custInfo.DealerCode);
                    else
                        dealercode = 0;
                }
                SalesmanCode = salesmancode;

                DealerCode = dealercode;
                #endregion

                #region Insert Prospect Data
                string insertDataCustomer1 = QueryCollection.qInsertDataCustomer1; //

                string insertDataCustomer2 = QueryCollection.qInsertDataCustomer2; //ProspectCompany
                string insertFollowUpOrder = QueryCollection.qInsertFollowUpOrder; //FollowUp
                using (var db = GetAABMobileDB())
                {
                    db.Execute(insertDataCustomer1, custID, custInfo.Name, custInfo.Phone1, custInfo.Phone2, custInfo.Email1
                   , "", custInfo.CustIDAAB, custInfo.SalesOfficerID, DealerCode, SalesmanCode, custInfo.BranchCode
                   , custInfo.isCompany, custInfo.IdentityNo, custInfo.Gender, custInfo.ProspectID, custInfo.BirthDate, custInfo.Address
                   , custInfo.PostCode);

                    if (custInfo.isCompany == 1)
                        db.Execute(insertDataCustomer2, custID, custInfo.Name, followUpID, custInfo.Email1
                            , custInfo.CompanyNPWPNo, custInfo.CompanyNPWPDate, custInfo.CompanyNPWPAddress, custInfo.Address, custInfo.PostCode
                            , custInfo.PICPhone, custInfo.PIC);

                    db.Execute(insertFollowUpOrder, followUpID, 0, custID, custInfo.Name, custInfo.Phone1, custInfo.Phone2
                        , custInfo.SalesOfficerID, custInfo.Name, 1, 1, "Renewal Data From Renewal Notice", custInfo.BranchCode);
                }
                #endregion

                #region Insert Order Simulation
                string insertOrderSimulation = QueryCollection.qInsertOrderSimulation;
                using (var db = GetAABMobileDB())
                {
                    db.Execute(insertOrderSimulation, orderID, custID, followUpID, orderSimulation.QuotationNo, orderSimulation.MultiYearF
                        , orderSimulation.YearCoverage, orderSimulation.TLOPeriod, orderSimulation.ComprePeriod, orderSimulation.BranchCode
                        , orderSimulation.SalesOfficerID, orderSimulation.PhoneSales, DealerCode, SalesmanCode
                        , orderSimulation.ProductTypeCode, orderSimulation.AdminFee, orderSimulation.TotalPremium, orderSimulation.Phone1
                        , orderSimulation.Phone2, orderSimulation.Email1, orderSimulation.Email2, orderSimulation.SendStatus
                        , orderSimulation.InsuranceType, orderSimulation.ApplyF, orderSimulation.SendF, orderSimulation.LastInterestNo
                        , orderSimulation.LastCoverageNo, policyNo, orderSimulation.NewPolicyNo, orderSimulation.NewPolicyId
                        , orderSimulation.VANumber, orderSimulation.ProductCode, orderSimulation.PeriodFrom, orderSimulation.PeriodTo
                        , orderSimulation.SegmentCode, orderSimulation.PolicyDeliverAddress, orderSimulation.PolicyDeliverPostalCode
                        , orderSimulation.PolicyDeliveryType, orderSimulation.PolicyDeliveryName);
                }
                #endregion

                #region Insert Order Simulation MV
                string insertOrderSimulationMV = QueryCollection.qInsertOrdSimMV;
                using (var db = GetAABMobileDB())
                {
                    db.Execute(insertOrderSimulationMV, orderID, orderSimulationMV.ObjectNo, orderSimulationMV.ProductTypeCode
                        , orderSimulationMV.VehicleCode, orderSimulationMV.BrandCode, orderSimulationMV.ModelCode
                        , orderSimulationMV.Series, orderSimulationMV.Type, orderSimulationMV.Sitting
                        , orderSimulationMV.Year, orderSimulationMV.CityCode, orderSimulationMV.UsageCode
                        , orderSimulationMV.SumInsured, orderSimulationMV.AccessSI, orderSimulationMV.RegistrationNumber
                        , orderSimulationMV.EngineNumber, orderSimulationMV.ChasisNumber, orderSimulationMV.IsNew
                        , orderSimulationMV.Color);
                }
                #endregion

                #region Insert Order Simulation Interest
                string insertOSInterest = QueryCollection.qInsertOrdSimInterest;
                using (var db = GetAABMobileDB())
                {
                    foreach (OrderSimulationInterest osI in listOSInterest)
                    {
                        db.Execute(insertOSInterest, orderID, osI.ObjectNo, osI.InterestNo, osI.InterestID, osI.Year
                            , osI.Premium, osI.PeriodFrom, osI.PeriodTo, osI.DeductibleCode);
                    }
                }
                #endregion

                #region Insert Order Simulation Coverage
                string insertOSCoverage = QueryCollection.qInsertOrdSimCoverage;
                using (var db = GetAABMobileDB())
                {
                    foreach (OrderSimulationCoverage osC in listOSCoverage)
                    {
                        db.Execute(insertOSCoverage, orderID, osC.ObjectNo, osC.InterestNo, osC.CoverageNo
                            , osC.CoverageID, osC.Rate, osC.SumInsured, osC.LoadingRate, osC.Loading
                            , osC.Premium, osC.IsBundling, osC.EntryPct, osC.Ndays, osC.ExcessRate, osC.CalcMethod
                            , osC.CoverPremium, osC.GrossPremium, osC.MaxSI, osC.Net1, osC.Net2, osC.Net3, osC.DeductibleCode
                            , osC.BeginDate, osC.EndDate);
                    }
                }
                #endregion


                OtosalesOrderDataID oData = new OtosalesOrderDataID();
                oData.CustID = custID;
                oData.FollowUpID = followUpID;
                oData.OrderID = orderID;

                #region Sync Image AAB
                ImageSyncProcess(listImageData, oData, custInfo.SalesOfficerID, policyNo);
                #endregion

                return oData;
            }
            catch (Exception e)
            {
                ClearFailedData(custID, followUpID, orderID);
                using (var db = GetAABMobileDB())
                {
                    db.Execute(QueryCollection.qInsertRenewalLog, policyNo, "Error GenerateTasklist : " + e.Message);
                }
                logger.Error("Error GenerateTasklist : " + e.Message);
                throw (e);
            }

            logger.Info("GenerateTasklist : End");
        }

        public static void ImageSyncProcess(List<ImageData> listImageData, OtosalesOrderDataID order, string salesman, string policyNo)
        {
            logger.Info("ImageSyncProcess : Start");
            try
            {
                string query = QueryCollection.qInsertImageData;
                string imageId = "";
                string[] imageList;
                foreach (ImageData i in listImageData)
                {
                    using (var db = GetAABMobileDB())
                    {
                        query = QueryCollection.qInsertImageData;
                        imageList = i.ImageName.Split('.');
                        imageId = System.Guid.NewGuid().ToString();
                        db.Execute(query, imageId, order.FollowUpID, salesman, imageId + "." + imageList[1], i.ImageMasterData, i.ImageThumbnailData);

                        switch (i.ImageType.ToUpper().Trim())
                        {
                            case "KTP":
                                query = String.Format(QueryCollection.qUpdateFU, " IdentityCard = @0");
                                db.Execute(query, imageId + "." + imageList[1], order.FollowUpID);
                                break;
                            case "STNK":
                                query = String.Format(QueryCollection.qUpdateFU, " STNK = @0");
                                db.Execute(query, imageId + "." + imageList[1], order.FollowUpID);
                                break;
                            case "NPWP":
                                query = String.Format(QueryCollection.qUpdateProspectCompany, " NPWP = @0");
                                db.Execute(query, imageId + "." + imageList[1], order.FollowUpID);
                                break;
                            case "SIUP":
                                query = String.Format(QueryCollection.qUpdateFU, " SIUP = @0");
                                db.Execute(query, imageId + "." + imageList[1], order.FollowUpID);
                                break;
                            default:
                                Console.WriteLine(". . .");
                                break;
                        }
                    }
                }

            }
            catch (Exception e)
            {
                using (var db = GetAABMobileDB())
                {
                    db.Execute(QueryCollection.qInsertRenewalLog, policyNo, "Error ImageSyncProcess : " + e.Message);
                }
                logger.Error("Error ImageSyncProcess : " + e.Message);
                throw (e);
            }

            logger.Info("ImageSyncProcess : End");
        }

        private static void ClearFailedData(string custID, string followUpID, string orderID)
        {
            using (var db = GetAABMobileDB())
            {
                db.Execute(QueryCollection.qDeleteFailGT, custID, followUpID, orderID);
            }
        }

    }
}
