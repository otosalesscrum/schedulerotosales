﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Otosales.Models.Constant;
using Otosales.Repository;
using Otosales.Logic;
using Newtonsoft.Json.Linq;
using a2is.Framework.Monitoring;
using Otosales.MonitorOrder.Model;

namespace Otosales.MonitorOrder.Repository
{
    class OtosalesOrderRepository : RepositoryBase
    {
        private static a2isLogHelper logger = new a2isLogHelper();

        private static string GetCurrentMethod() {
            return System.Reflection.MethodBase.GetCurrentMethod().Name;
        }

        public List<dynamic> GetFollowUpWithSurveyStatusCompleted()
        {
            using (var db = GetAABMobileDB())
            {
                bool isProcess = true;
                int itr = 0;
                List<dynamic> res = new List<dynamic>();
                while (isProcess)
                {
                    try
                    {
                        string query = @";
SELECT fu.FollowUpNo, os.PolicyOrderNo, os.SurveyNo, os.OrderNo
INTO #temp  
FROM dbo.FollowUp fu
INNER JOIN OrderSimulation os ON fu.FollowUpNo = os.FollowUpNo and os.ApplyF = 1
INNER JOIN OrderSimulationSurvey oss ON os.OrderNo = oss.OrderNo
INNER JOIN FollowUpStatusInfo fusi ON fu.FollowUpInfo = fusi.InfoCode
where COALESCE(oss.IsNeedSurvey,0) = 1 and COALESCE(os.SurveyNo,'') <> '' AND fusi.InfoCode = @2

SELECT t.FollowUpNo, t.PolicyOrderNo, t.SurveyNo, t.OrderNo 
FROM beyondreport.aab.dbo.survey s WITH (NOLOCK) 
INNER JOIN #temp t ON s.Survey_No = t.SurveyNo
WHERE s.Status IN (@0,@1)

DROP TABLE #temp";
                        res = db.Fetch<dynamic>(query, SurveyStatus.Complete, SurveyStatus.Reject, FollowUpInfo.SendToSurveyor);
                        isProcess = false;
                    }
                    catch (Exception)
                    {
                        itr++;
                        if (itr > 3) {
                            isProcess = false;
                        }
                    }
                }
                return res;
            }
        }

        public int UpdateFollowUpInfoToFollowUpSurveyResult(string followUpNo)
        {
            string CurrentMethod = GetCurrentMethod();
            try
            {
                string qUpdate = "UPDATE FollowUp SET FollowUpInfo = @0, LastUpdatedTime = GETDATE() WHERE FollowUpNo = @1";
                using (var db = GetAABMobileDB())
                {
                    return db.Execute(qUpdate, FollowUpInfo.FollowUpSurveyResult, followUpNo);
                }

            }
            catch (Exception e)
            {
                logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.Message));
                return 0;
            }
        }
        
        public int UpdateFollowUpToPolicyCreated(string orderNo)
        {
            int res =  0;
            string qGetOrder = @"SELECT CAST(COALESCE(IsNeedDocRep,0) AS BIT) IsNeedDocRep, DocRep 
                                FROM dbo.OrderSimulation os
                                INNER JOIN dbo.FollowUp f
                                ON f.FollowUpNo = os.FollowUpNo
                                WHERE PolicyOrderNo = @0";
            string qUpdateFollowUpToPolicyCreated = @"
UPDATE fu
SET FollowUpStatus = @1, FollowUpInfo = @2, LastUpdatedTime = GETDATE()
FROM FollowUp fu 
INNER JOIN OrderSimulation os ON fu.FollowUpNo = os.FollowUpNo and os.ApplyF = 1
WHERE os.PolicyOrderNo = @0";
            using (var db = GetAABMobileDB())
            {
                try
                {
                    List<dynamic> ord = db.Fetch<dynamic>(qGetOrder, orderNo);
                    if (ord.Count > 0)
                    {
                        if (ord.First().IsNeedDocRep)
                        {
                            if (string.IsNullOrEmpty(ord.First().DocRep))
                            {
                                db.Execute(qUpdateFollowUpToPolicyCreated, orderNo, FollowUpStatus.DealStatusCode, FollowUpInfo.PolicyApprovedNeedDocRep);
                                res++;
                            }
                            else
                            {
                                db.Execute(qUpdateFollowUpToPolicyCreated, orderNo, FollowUpStatus.DealStatusCode, FollowUpInfo.PolicyApproved);
                                res++;
                            }
                        }
                        else
                        {
                            db.Execute(qUpdateFollowUpToPolicyCreated, orderNo, FollowUpStatus.DealStatusCode, FollowUpInfo.PolicyApproved);
                            res++;
                        }
                    }
                }
                catch (Exception)
                {
                    return 0;
                }
                return res;
            }
        }
        public int UpdateFollowUpToFUPembayaran(string orderNo)
        {
            string qUpdateFollowUpToPolicyCreated = @"
UPDATE fu
SET FollowUpInfo = @1, LastUpdatedTime = GETDATE()
FROM FollowUp fu 
INNER JOIN OrderSimulation os ON fu.FollowUpNo = os.FollowUpNo and os.ApplyF = 1
WHERE os.PolicyOrderNo = @0 
";
            using (var db = GetAABDB())
            {
                try
                {
                    return db.Execute(qUpdateFollowUpToPolicyCreated, orderNo, FollowUpInfo.FUPayment);
                }
                catch (Exception)
                {
                    return 0;
                }
            }
        }



        public List<dynamic> GetMstOrderMobileWithBackToAOStatus()
        {
            using (var db = GetAABMobileDB())
            {
                string query = @"
SELECT b.FollowUpNo, os.PolicyOrderNo, os.OrderNo, mom1.Remarks
FROM (
select Order_No, max(Order_ID) [Order_ID]
from beyondreport.aab.dbo.mst_order_mobile mom WITH (NOLOCK) 
group by Order_No
) a 
INNER JOIN beyondreport.aab.dbo.mst_order_mobile mom1 WITH (NOLOCK) ON a.Order_ID = mom1.Order_ID and mom1.SA_State = @1
INNER JOIN OrderSimulation os ON os.PolicyOrderNo = a.Order_No and os.ApplyF = 1
INNER JOIN FollowUp b ON os.FollowUpNo = b.FollowUpNo and b.FollowUpInfo = @0 and b.RowStatus = 1
INNER JOIN BEYONDREPORT.AAB.dbo.Mst_Order mo ON mo.Order_No = mom1.Order_No AND mo.Order_Status NOT IN (@2,@3)
AND mom1.EntryDt > mo.EntryDt";
                bool isProcess = true;
                int itr = 0;
                List<dynamic> res = new List<dynamic>();
                while (isProcess)
                {
                    try
                    {
                        res = db.Fetch<dynamic>(query, FollowUpInfo.SendToSA, SAState.BackToAO, Constants.POLICY_APPROVED, Constants.POLICY_REJECTED); 
                        isProcess = false;
                    }
                    catch (Exception)
                    {
                        itr++;
                        if (itr > 3)
                        {
                            isProcess = false;
                        }
                    }
                }
                return res;
            }
        }

        public int UpdateFollowUpInfoToBackToAO(string followUpNo, string Remarks)
        {
            try
            {
                string qUpdate = "UPDATE FollowUp SET FollowUpInfo = @0, LastUpdatedTime = GETDATE(), RemarkFromSA = @2 WHERE FollowUpNo = @1";
                using (var db = GetAABMobileDB())
                {
                    return db.Execute(qUpdate, FollowUpInfo.NeedDataRevision, followUpNo, Remarks);
                }
            }
            catch (Exception e)
            {

                return 0;
            }
        }


        public List<dynamic> GetFollowUpWithSendToSAStatus()
        {
            using (var db = GetAABMobileDB())
            {
                string query = @"SELECT b.FollowUpNo, os.PolicyOrderNo, os.OrderNo, b.RemarkToSA
FROM (
select Order_No, max(Order_ID) [Order_ID]
from beyondreport.aab.dbo.mst_order_mobile mom WITH (NOLOCK) 
group by Order_No
) a 
INNER JOIN beyondreport.aab.dbo.mst_order_mobile mom1 WITH (NOLOCK) ON a.Order_ID = mom1.Order_ID and mom1.SA_State <> @1
INNER JOIN OrderSimulation os ON os.PolicyOrderNo = a.Order_No and os.ApplyF = 1
INNER JOIN FollowUp b ON os.FollowUpNo = b.FollowUpNo and b.FollowUpInfo = @0 and b.RowStatus = 1 AND COALESCE(os.PolicyOrderNo,'') <> ''";
                return db.Fetch<dynamic>(query, FollowUpInfo.SendToSA, SAState.BackToAO);
            }
        }
        
        public List<dynamic> GetFollowUpStatusSendToSAorFUPaymentWithNeedApprovalAndOrderStatusPolicyCreated()
        {
            //TODO : ini hanya berlaku untuk NEW, karena kalo RENEW statusnya FU Pembayaran yang ditarik.
            using (var db = GetAABMobileDB())
            {
                string query = @"SELECT fu.FollowUpNo, os.PolicyOrderNo, os.OrderNo
FROM FollowUp fu 
INNER JOIN OrderSimulation os ON os.FollowUpNo = fu.FollowUpNo and os.ApplyF = 1
INNER JOIN beyondreport.aab.dbo.mst_order mo ON os.PolicyOrderNo = mo.Order_No and mo.Order_Status IN ('9','11')
WHERE 1 = case when ISNULL(fu.IsRenewal,0) = 0 then IIF(fu.FollowUpInfo IN (@0,@2),1,0) else IIF(fu.FollowUpInfo IN (@1,@2),1,0) end ";
                bool isProcess = true;
                int itr = 0;
                List<dynamic> res = new List<dynamic>();
                while (isProcess)
                {
                    try
                    {
                        res = db.Fetch<dynamic>(query, FollowUpInfo.SendToSA, FollowUpInfo.FUPayment, FollowUpInfo.NeedApproval);
                        isProcess = false;
                    }
                    catch (Exception)
                    {
                        itr++;
                        if (itr > 3)
                        {
                            isProcess = false;
                        }
                    }
                }
                return res;
            }
        }

        public List<dynamic> GetOrderRenewSendToSAAndSubmitBySA()
        {
            //TODO : ini hanya berlaku untuk NEW, karena kalo RENEW statusnya FU Pembayaran yang ditarik.
            using (var db = GetAABMobileDB())
            {
                string query = @"
SELECT fu.FollowUpNo, os.PolicyOrderNo, os.OrderNo
FROM FollowUp fu 
INNER JOIN OrderSimulation os ON os.FollowUpNo = fu.FollowUpNo and os.ApplyF = 1
INNER JOIN (
select Order_No, max(Order_ID) [Order_ID]
from beyondreport.aab.dbo.mst_order_mobile mom WITH (NOLOCK) 
group by Order_No
) a ON os.PolicyOrderNo = a.Order_No
INNER JOIN beyondreport.aab.dbo.mst_order_mobile mom1 WITH (NOLOCK) ON a.Order_ID = mom1.Order_ID and mom1.SA_State = @1
INNER JOIN beyondreport.aab.dbo.mst_order mo ON os.PolicyOrderNo = mo.Order_No AND mo.Order_Type = '2' and mo.Order_Status = '50'
WHERE fu.FollowUpInfo = @0 AND COALESCE(os.PolicyOrderNo,'') <> '' ";              
                bool isProcess = true;
                int itr = 0;
                List<dynamic> res = new List<dynamic>();
                while (isProcess)
                {
                    try
                    {
                        res = db.Fetch<dynamic>(query, FollowUpInfo.SendToSA, SAState.SubmitBySA);
                        isProcess = false;
                    }
                    catch (Exception)
                    {
                        itr++;
                        if (itr > 3)
                        {
                            isProcess = false;
                        }
                    }
                }
                return res;
            }
        }
        public int AddToMstOrderMobile(string followUpNo, string entryUsr, string remarkToSA)
        {
            string qUpdate = @"
INSERT INTO [dbo].[Mst_Order_Mobile](
Order_No,
Account_Info_BankName,
Account_Info_AccountNo,
Account_Info_AccountHolder,
Approval_Status,
Approval_Process,
isSO,
Remarks,
Email_SA,
Approval_Type,
Actual_Date,
EntryDt,
EntryUsr,
UpdateDt,
UpdateUsr,
SA_State)
SELECT os.PolicyOrderNo, null,null,null, 0,0, 0, @0, '', 0, GETDATE(), GETDATE(), @1, null, null, 1
FROM BEYONDMOSS.AABMobile.dbo.FollowUp fu
INNER JOIN BEYONDMOSS.AABMobile.dbo.OrderSimulation os ON fu.FollowUpNo = os.FollowUpNo and os.ApplyF = 1
WHERE fu.FollowUpNo = @2";
            using (var db = GetAABDB())
            {
                return db.Execute(qUpdate, remarkToSA, entryUsr, followUpNo);
            }
        }

        public bool UpdateRenewalStatusPolicy(string OrderNo) {
            string query = @"";
            int res = 0;
            using (var db = GetAABDB()) {
                try
                {
                    query = @"SELECT Old_Policy_No,Order_Type FROM dbo.Mst_Order WHERE Order_No = @0";
                    List<dynamic> info = db.Fetch<dynamic>(query, OrderNo);
                    if (info.Count > 0)
                    {
                        if (Convert.ToInt32(info.First().Order_Type) == 2)
                        {
                            query = @"UPDATE dbo.Policy SET Renewal_Status = 1, Renewal_Date = GETDATE() WHERE Order_No = @0";
                            db.Execute(query, info.First().Old_Policy_No);
                            res++;
                        }
                    }
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return res>0;
        }

        public List<dynamic> GetFollowUpWithStatusPolicyCreated()
        {
            using (var db = GetAABMobileDB())
            {
                string query = @";SELECT fu.FollowUpNo, os.PolicyOrderNo, os.SurveyNo,
								CONCAT(RTRIM(mo.Policy_No),'-',mo.Endorsement_No) RefDocNo
								INTO #tempOrderDoc
                                FROM dbo.FollowUp fu
                                INNER JOIN OrderSimulation os 
								ON fu.FollowUpNo = os.FollowUpNo 
								and os.ApplyF = 1 AND os.RowStatus = 1
								INNER JOIN BEYONDREPORT.AAB.dbo.Mst_Order mo
								ON mo.Order_No = os.PolicyOrderNo
                                WHERE fu.FollowUpInfo IN (50,51,53)
								
								SELECT FollowUpNo,PolicyOrderNo,SurveyNo,Delivery_State 
								FROM BEYONDREPORT.AAB.dbo.Document d
								INNER JOIN #tempOrderDoc t ON d.Reference_No = t.RefDocNo
								DROP TABLE #tempOrderDoc";              
                bool isProcess = true;
                int itr = 0;
                List<dynamic> res = new List<dynamic>();
                while (isProcess)
                {
                    try
                    {
                        res = db.Fetch<dynamic>(query, FollowUpInfo.PolicyApproved, FollowUpInfo.PolicyApprovedNeedDocRep,
                            FollowUpInfo.PolicyDelivered);
                        isProcess = false;
                    }
                    catch (Exception)
                    {
                        itr++;
                        if (itr > 3)
                        {
                            isProcess = false;
                        }
                    }
                }
                return res;
            }
        }

        public int UpdateFollowUpInfoToFollowUpPolicyDeliveredOrRecived(string followUpNo, dynamic Delivery_state)
        {
            string CurrentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                string query = @"SELECT FollowUpInfo, os.PolicyNo
                                FROM dbo.FollowUp fu
                                INNER JOIN OrderSimulation os 
								ON fu.FollowUpNo = os.FollowUpNo 
								and os.ApplyF = 1 AND os.RowStatus = 1
                                WHERE fu.FollowUpNo = @0 AND os.PolicyNo IS NOT NULL";
                int res = 0;
                using (var MobileDB = GetAABMobileDB())
                {
                    try
                    {
                        List<dynamic> Info = MobileDB.Fetch<dynamic>(query, followUpNo);
                        if (Info.Count > 0)
                        {
                            string InfoCode = Convert.ToString(Info.First().FollowUpInfo);
                            string PolicyNo = Convert.ToString(Info.First().PolicyNo);
                            query = @"IF(@0=@1)
	                        BEGIN
		                        UPDATE dbo.FollowUp SET FollowUpInfo = @2 WHERE FollowUpNo = @3
	                        END
	                        ELSE IF(@0=@4)
	                        BEGIN
		                        UPDATE dbo.FollowUp SET FollowUpInfo = @5 WHERE FollowUpNo = @3
	                        END
	                        ELSE IF(@0=@6)
	                        BEGIN
		                        UPDATE dbo.FollowUp SET FollowUpInfo = @2 WHERE FollowUpNo = @3
	                        END
	                        ELSE IF(@0=@7)
	                        BEGIN
		                        UPDATE dbo.FollowUp SET FollowUpInfo = @5 WHERE FollowUpNo = @3
	                        END";
                            string state = Convert.ToString(Delivery_state);
                            if (!string.IsNullOrEmpty(state))
                            {
                                if (state.Trim().ToUpper().Equals(DeliveryStatus.Delivered))
                                {
                                    MobileDB.Execute(query, Info.First().FollowUpInfo, FollowUpInfo.PolicyApproved
                                        , FollowUpInfo.PolicyDelivered, followUpNo
                                        , FollowUpInfo.PolicyApprovedNeedDocRep
                                        , FollowUpInfo.PolicyDeliveredNeedDocRep
                                        , FollowUpInfo.PolicyDelivered
                                        , FollowUpInfo.PolicyDeliveredNeedDocRep);
                                }
                                else if (state.Trim().ToUpper().Equals(DeliveryStatus.Recived))
                                {
                                    MobileDB.Execute(query, Info.First().FollowUpInfo, FollowUpInfo.PolicyApproved
                                        , FollowUpInfo.PolicyReceived, followUpNo
                                        , FollowUpInfo.PolicyApprovedNeedDocRep
                                        , FollowUpInfo.PolicyReceivedNeedDocRep
                                        , FollowUpInfo.PolicyDelivered
                                        , FollowUpInfo.PolicyDeliveredNeedDocRep);
                                }
                            }
                        }
                        res++;
                    }
                    catch (Exception)
                    {
                        return 0;
                    }
                }
                return res;

            }
            catch (Exception e)
            {
                logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.Message));
                return 0;
            }
        }

        public static void UpdateRenewalOrderStatus()
        {
            string CurrentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            List<dynamic> listOrderNoAAB = new List<dynamic>();
            string query = @"SELECT b.FollowUpNo, os.PolicyOrderNo, os.OrderNo, b.Remark
                            FROM (
                            select Order_No, max(Order_ID) [Order_ID]
                            from beyondreport.aab.dbo.mst_order_mobile mom WITH (NOLOCK) 
                            group by Order_No
                            ) a 
                            INNER JOIN beyondreport.aab.dbo.mst_order_mobile mom1 WITH (NOLOCK) ON a.Order_ID = mom1.Order_ID 
                            and mom1.SA_State = @0
                            INNER JOIN OrderSimulation os ON os.PolicyOrderNo = a.Order_No and os.ApplyF = 1
                            INNER JOIN FollowUp b ON os.FollowUpNo = b.FollowUpNo and b.RowStatus = 1 
                            AND b.IsRenewal = 1 AND b.followupstatus = @1 AND b.followupinfo = @2
                            AND COALESCE(os.PolicyOrderNo,'') <> ''";
            using (var db = GetAABMobileDB())
            {
                listOrderNoAAB = db.Fetch<dynamic>(query,SAState.SubmitBySA,FollowUpStatus.Potensial,FollowUpInfo.SendToSA);
                foreach (dynamic a in listOrderNoAAB)
                {
                    try
                    {
                        query = @"UPDATE fu
                        SET FollowUpInfo = @1, LastUpdatedTime = GETDATE()
                        FROM FollowUp fu 
                        INNER JOIN OrderSimulation os ON fu.FollowUpNo = os.FollowUpNo and os.ApplyF = 1
                        WHERE os.PolicyOrderNo = @0";
                        db.Execute(query, a.PolicyOrderNo, FollowUpInfo.FUPayment);
                        OrdLogic.UpdateOrderMobile(a.OrderNo);

                    }
                    catch (Exception e)
                    {
                        logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.Message));
                    }
                }
            }
        }

        public List<dynamic> GetMstOrderMobileWithStatusRevise()
        {
            using (var db = GetAABMobileDB())
            {
                string query = @";
SELECT moma.Order_No, moma.ApprovalType
FROM (
select Order_No, max(Order_ID) [Order_ID]
from beyondreport.aab.dbo.mst_order_mobile mom WITH (NOLOCK) 
group by Order_No
) a 
INNER JOIN beyondreport.aab.dbo.mst_order_mobile mom1 WITH (NOLOCK) 
ON a.Order_ID = mom1.Order_ID and mom1.SA_State = @0 AND mom1.Approval_Status = @1
INNER JOIN OrderSimulation os ON os.PolicyOrderNo = a.Order_No and os.ApplyF = 1
INNER JOIN FollowUp b ON os.FollowUpNo = b.FollowUpNo and b.RowStatus = 1
INNER JOIN BEYONDREPORT.AAB.dbo.Mst_Order_Mobile_Approval moma 
ON moma.Order_No = mom1.Order_No AND moma.ApprovalType IN (@2,@3,@4)";
                bool isProcess = true;
                int itr = 0;
                List<dynamic> res = new List<dynamic>();
                while (isProcess)
                {
                    try
                    {
                        res = db.Fetch<dynamic>(query, SAState.SubmitBySA, ApprovalStatus.REJECT_OR_REVISE, ApprovalType.Adjustment, ApprovalType.CommissionSameDay, ApprovalType.Commission);
                        isProcess = false;
                    }
                    catch (Exception)
                    {
                        itr++;
                        if (itr > 3)
                        {
                            isProcess = false;
                        }
                    }
                }
                return res;
            }
        }

        public void UpdateApprovalStatus(string OrderNo, string ApprovalType, string EntryUsr) {
            using (var db = GetAABDB()) {
                try
                {
                    string qUpdate = @"UPDATE Mst_Order_Mobile_Approval
                                SET	UpdateUsr = @2, ApprovalStatus = 0, UpdateDt = GETDATE()
                                WHERE Order_No = @0 and ApprovalType = @1 and ApprovalStatus NOT IN (0)";
                    db.Execute(qUpdate, OrderNo, ApprovalType, EntryUsr);

                }
                catch (Exception)
                {

                    throw;
                }
            }
        }

        public List<dynamic> GetMstOrderMobileWithStatusReject()
        {
            using (var db = GetAABDB())
            {
                string query = @";
SELECT mo.Order_No FROM dbo.Mst_Order_Mobile_Approval moma
INNER JOIN dbo.Mst_Order mo ON mo.Order_No = moma.Order_No
WHERE moma.ApprovalType = @0 AND moma.ApprovalStatus = @1
AND mo.Order_Status <> '6'";
                bool isProcess = true;
                int itr = 0;
                List<dynamic> res = new List<dynamic>();
                while (isProcess)
                {
                    try
                    {
                        res = db.Fetch<dynamic>(query, ApprovalType.NextLimit, ApprovalStatus.REJECT_OR_REVISE);
                        isProcess = false;
                    }
                    catch (Exception)
                    {
                        itr++;
                        if (itr > 3)
                        {
                            isProcess = false;
                        }
                    }
                }
                return res;
            }
        }
        #region updateunsued
        //public void UpdateFollowUpStatusReject(string OrderNo)
        //{
        //    string ApprovalUser = GetApprovalUser(OrderNo);
        //    string DropOrderURL = GetSettingDropOrderURL();
        //    string body = string.Format("pOrderNo={0}", OrderNo);
        //    string json = NetworkLogic.POST(DropOrderURL, body, NetworkLogic.GetToken());
        //    JToken jToken = JToken.Parse(json);
        //    if (jToken != null && !string.IsNullOrEmpty(jToken["status"].ToString()) && Convert.ToString(jToken["status"]).ToLower().Equals("true"))
        //    {
        //        //var r = JsonConvert.DeserializeObject<AutoApproveOrderResult>(jToken["data"].ToString());
        //        using (var db = GetAABMobileDB())
        //        {
        //            string qGet = @"SELECT FollowUpStatus FROM dbo.FollowUp f
        //                    INNER JOIN dbo.OrderSimulation os
        //                    ON os.FollowUpNo = f.FollowUpNo
        //                    WHERE os.PolicyOrderNo = @0";
        //            List<dynamic> list = db.Fetch<dynamic>(qGet, OrderNo);
        //            if (list.Count > 0)
        //            {
        //                string qUpdate = @";DECLARE @@FollowUpNo VARCHAR(100)
        //                        SELECT @@FollowUpNo=FollowUpNo FROM dbo.OrderSimulation WHERE PolicyOrderNo = @0
        //                        UPDATE dbo.FollowUp SET FollowUpStatus = @1, LastUpdatedTime = GETDATE() 
        //WHERE FollowUpNo = @@FollowUpNo";
        //                db.Execute(qUpdate, OrderNo, FollowUpStatus.OrderRejectedStatusCode);
        //            }
        //        }
        //    }
        //}

        //public string GetApprovalUser(string OrderNo)
        //{
        //    //Tanya JSW, mst_oder_mobile.approval_status :  0 : ?, 1 : approved, 2 : ? 
        //    string qGet = @"SELECT COALESCE(ApprovalUsr,'') AS ApprovalUsr 
        //                    FROM dbo.Mst_Order_Mobile_Approval 
        //                    WHERE ApprovalType = @1
        //                    AND Order_No = @0";
        //    using (var db = GetAABDB())
        //    {
        //        string res = "";
        //        List<dynamic> list = db.Fetch<dynamic>(qGet, OrderNo, ApprovalType.NextLimit);
        //        if (list.Count > 0)
        //        {
        //            res = list.First().ApprovalUsr;
        //        }
        //        return res;
        //    }
        //}

        #endregion
        public void UpdateFollowUpStatusReject(string OrderNo)
        {
            try
            {
                using (var db = GetAABDB())
                {
                    string qUpdate = @"UPDATE dbo.Mst_Order SET Order_Status = '6'
                                WHERE Order_No = @0";
                    db.Execute(qUpdate, OrderNo);
                }
                using (var db = GetAABMobileDB())
                {
                    string qGet = @"SELECT FollowUpStatus FROM dbo.FollowUp f
                            INNER JOIN dbo.OrderSimulation os
                            ON os.FollowUpNo = f.FollowUpNo
                            WHERE os.PolicyOrderNo = @0";
                    List<dynamic> list = db.Fetch<dynamic>(qGet, OrderNo);
                    if (list.Count > 0)
                    {
                        if (list.First().FollowUpStatus != Convert.ToInt32(FollowUpStatus.OrderRejectedStatusCode))
                        {
                            string qUpdate = @";DECLARE @@FollowUpNo VARCHAR(100)
                                SELECT @@FollowUpNo=FollowUpNo FROM dbo.OrderSimulation WHERE PolicyOrderNo = @0
                                UPDATE dbo.FollowUp SET FollowUpStatus = @1, LastUpdatedTime = GETDATE(), FollowUpInfo = 0 
								WHERE FollowUpNo = @@FollowUpNo";
                            db.Execute(qUpdate, OrderNo, FollowUpStatus.OrderRejectedStatusCode);
                        }
                    }
                }

            }
            catch (Exception)
            {
            }
        }

        public List<dynamic> GetOrderRenewableDigital()
        {
            using (var db = GetAABMobileDB())
            {
                string query = @";
SELECT f.FollowUpNo FROM dbo.FollowUp f
INNER JOIN dbo.OrderSimulation os
ON os.FollowUpNo = f.FollowUpNo
INNER JOIN dbo.Fn_AABSplitString(
(SELECT ParamValue 
FROM Asuransiastra.GODigital.ApplicationParameters 
WHERE ParamName = 'godig-segment-code'),',') gdg
ON gdg.Data = os.SegmentCode
INNER JOIN dbo.ProspectCustomer pc ON pc.CustID = os.CustID	
INNER JOIN dbo.OrderSimulationMV omv ON omv.OrderNo = os.OrderNo
WHERE IsRenewal = 1 AND IsRenewalDigital IS NULL
AND os.ApplyF = 1 AND os.RowStatus = 1 AND COALESCE(pc.isCompany,0) = 0
AND os.InsuranceType = 1 AND omv.UsageCode IN ('U00002','U00006','U00011')
AND ((LEFT(os.SegmentCode,3) = 'P3C' AND os.BranchCode 
	IN (SELECT Data FROM dbo.Fn_AABSplitString((SELECT TOP 1 ParamValue 
		FROM Asuransiastra.GODigital.ApplicationParameters 
		WHERE ParamName='GODIG-BRANCH-CODE-P3C'),','))) 
	OR LEFT(os.SegmentCode,3) <> 'P3C')";
                bool isProcess = true;
                int itr = 0;
                List<dynamic> res = new List<dynamic>();
                while (isProcess)
                {
                    try
                    {
                        res = db.Fetch<dynamic>(query);
                        isProcess = false;
                    }
                    catch (Exception)
                    {
                        itr++;
                        if (itr > 3)
                        {
                            isProcess = false;
                        }
                    }
                }
                return res;
            }
        }

        public void UpdateStatusRenewableDigital(string followupno, bool flag)
        {
            using (var db = GetAABMobileDB())
            {
                try
                {
                    string qUpdate = @"UPDATE dbo.FollowUp SET IsRenewalDigital = @1 WHERE FollowUpNo = @0";
                    if (flag)
                    {
                        db.Execute(qUpdate, followupno, flag);
                    }
                    else
                    {
                        db.Execute(qUpdate, followupno, null);
                    }
                }
                catch (Exception)
                {
                }
            }
        }

        public List<dynamic> GetOrderNonRenewableDigital()
        {
            using (var db = GetAABMobileDB())
            {
                string query = @";
SELECT a.FollowUpNo FROM dbo.FollowUp a WHERE FollowUpNo NOT IN(
SELECT f.FollowUpNo FROM dbo.FollowUp f
INNER JOIN dbo.OrderSimulation os
ON os.FollowUpNo = f.FollowUpNo
INNER JOIN dbo.Fn_AABSplitString(
(SELECT ParamValue 
FROM Asuransiastra.GODigital.ApplicationParameters 
WHERE ParamName = 'godig-segment-code'),',') gdg
ON gdg.Data = os.SegmentCode
INNER JOIN dbo.ProspectCustomer pc ON pc.CustID = os.CustID	
INNER JOIN dbo.OrderSimulationMV omv ON omv.OrderNo = os.OrderNo
WHERE IsRenewal = 1 --AND IsRenewalDigital IS NULL
AND os.ApplyF = 1 AND os.RowStatus = 1 AND COALESCE(pc.isCompany,0) = 0
AND os.InsuranceType = 1 AND omv.UsageCode IN ('U00002','U00006','U00011')
AND ((LEFT(os.SegmentCode,3) = 'P3C' AND os.BranchCode 
	IN (SELECT Data FROM dbo.Fn_AABSplitString((SELECT TOP 1 ParamValue 
		FROM Asuransiastra.GODigital.ApplicationParameters 
		WHERE ParamName='GODIG-BRANCH-CODE-P3C'),','))) 
	OR LEFT(os.SegmentCode,3) <> 'P3C')
) AND a.IsRenewalDigital = 1 AND a.RowStatus = 1";
                bool isProcess = true;
                int itr = 0;
                List<dynamic> res = new List<dynamic>();
                while (isProcess)
                {
                    try
                    {
                        res = db.Fetch<dynamic>(query);
                        isProcess = false;
                    }
                    catch (Exception)
                    {
                        itr++;
                        if (itr > 3)
                        {
                            isProcess = false;
                        }
                    }
                }
                return res;
            }
        }

        public List<dynamic> GetMstOrderWithStatusWO()
        {
            using (var db = GetAABDB())
            {
                string query = @";
SELECT mo1.Policy_No, os1.FollowUpNo, os1.OrderNo, mo1.Old_Policy_No, os1.CustID
FROM dbo.Mst_Order mo1 INNER JOIN 
(SELECT MAX(mo.Order_No) Order_No FROM dbo.Mst_Order mo 
INNER JOIN BEYONDMOSS.AABMobile.dbo.OrderSimulation os
on os.PolicyNo = mo.Policy_No 
INNER JOIN BEYONDMOSS.AABMobile.dbo.FollowUp f 
ON f.FollowUpNo = os.FollowUpNo 
GROUP BY mo.Policy_No) a ON a.Order_No = mo1.Order_No
INNER JOIN dbo.Policy p ON p.Order_No = a.Order_No
INNER JOIN BEYONDMOSS.AABMobile.dbo.OrderSimulation os1
ON os1.PolicyNo = mo1.Policy_No
INNER JOIN BEYONDMOSS.AABMobile.dbo.FollowUp f 
ON f.FollowUpNo = os1.FollowUpNo
WHERE mo1.Order_Type = 6 AND p.Status = 'W'
AND f.FollowUpStatus = 13 ORDER BY mo1.EntryDt";
                bool isProcess = true;
                int itr = 0;
                List<dynamic> res = new List<dynamic>();
                while (isProcess)
                {
                    try
                    {
                        res = db.Fetch<dynamic>(query);
                        isProcess = false;
                    }
                    catch (Exception)
                    {
                        itr++;
                        if (itr > 3)
                        {
                            isProcess = false;
                        }
                    }
                }
                return res;
            }
        }

        public bool RollBackOrderWO(string OrderNo, string OldPolicyNo, string FollowUpNo, string CustID) {
            string CurrentMethod = GetCurrentMethod();
            try
            {
                var aabDB = GetAABDB();
                var mblDB = GetAABMobileDB();
                if (RollBackImageWO(FollowUpNo)) {
                    string policyNo = "";
                    //mblDB.Execute(@";EXEC dbo.usp_ReOrderOtosalesWO @0, @1",
                    //    OrderNo, policyNo); //TimeOut
                    #region without SP
                    string NewOrderNo = Guid.NewGuid().ToString();
                    string NewFollowUpNo = Guid.NewGuid().ToString();
                    string NewCustID = Guid.NewGuid().ToString();
                    List<dynamic> lsOrderDt = mblDB.Fetch<dynamic>(@";
SELECT CustID, FollowUpNo, OrderNo, COALESCE(OldPolicyNo,'') OldPolicyNo
FROM dbo.OrderSimulation 
WHERE OrderNo = @0 AND RowStatus = 1 AND ApplyF = 1",OrderNo);
                    if (lsOrderDt.Count > 0) {
                        if (!string.IsNullOrEmpty(lsOrderDt.First().OldPolicyNo) && !string.IsNullOrWhiteSpace(lsOrderDt.First().OldPolicyNo))
                        {
                            policyNo = CommonLogic.GetPolicyNo();
                        }                            
                        List<dynamic> lsExc = aabDB.Fetch<dynamic>(@";SELECT * FROM dbo.Excluded_Renewal_Policy WHERE Policy_No = @0", OldPolicyNo);
                        if (lsExc.Count > 0)
                        {
                            aabDB.Execute(@";
UPDATE dbo.Excluded_Renewal_Policy 
SET RowStatus = 0, 
UpdateDt = GETDATE(), UpdateUsr = 'OTOSL'
WHERE Policy_No = @0", OldPolicyNo);
                        }
                        else {
                            #region cust
                            string qGetCust = @";DECLARE @@NewCustID VARCHAR(100) = @1
SELECT 
	@@NewCustID CustID,
	Name ,
	Phone1 ,
	Phone2 ,
	Email1 ,
	Email2 ,
	CustIDAAB ,
	SalesOfficerID ,
	DealerCode ,
	SalesDealer ,
	BranchCode ,
	GETDATE() EntryDate,
	1 RowStatus,
	ProspectID ,
	CustBirthDay ,
	CustGender ,
	CustAddress ,
	PostalCode ,
	IdentityNo ,
	isCompany ,
	IsPayerCompany
FROM dbo.ProspectCustomer WHERE CustID = @0";
                            List<dynamic> lsCust = mblDB.Fetch<dynamic>(qGetCust, CustID, NewCustID);
                            foreach (dynamic c in lsCust)
                            {
                                string qInsertCust = @";
IF NOT EXISTS(SELECT * FROM dbo.ProspectCustomer WHERE CustID = @0)
BEGIN
	INSERT INTO dbo.ProspectCustomer
		( CustID ,
			Name ,
			Phone1 ,
			Phone2 ,
			Email1 ,
			Email2 ,
			CustIDAAB ,
			SalesOfficerID ,
			DealerCode ,
			SalesDealer ,
			BranchCode ,
			EntryDate ,
			RowStatus ,
			ProspectID ,
			CustBirthDay ,
			CustGender ,
			CustAddress ,
			PostalCode ,
			IdentityNo ,
			isCompany ,
			IsPayerCompany
		)
	VALUES( 
	@0,
	@1 ,
	@2 ,
	@3 ,
	@4 ,
	@5 ,
	@6 ,
	@7 ,
	@8 ,
	@9 ,
	@10 ,
	@11 ,
	@12 ,
	@13 ,
	@14 ,
	@15 ,
	@16 ,
	@17 ,
	@18 ,
	@19 ,
	@20)
END";
                                mblDB.Execute(qInsertCust,
                                    c.CustID,
                                    c.Name,
                                    c.Phone1,
                                    c.Phone2,
                                    c.Email1,
                                    c.Email2,
                                    c.CustIDAAB,
                                    c.SalesOfficerID,
                                    c.DealerCode,
                                    c.SalesDealer,
                                    c.BranchCode,
                                    c.EntryDate,
                                    c.RowStatus,
                                    c.ProspectID,
                                    c.CustBirthDay,
                                    c.CustGender,
                                    c.CustAddress,
                                    c.PostalCode,
                                    c.IdentityNo,
                                    c.isCompany,
                                    c.IsPayerCompany);
                            }
                            #endregion
                            #region company
                            string qGetCompany = @";
DECLARE @@NewCustID VARCHAR(100) = @1
DECLARE @@FollowUpNo VARCHAR(100) = @2
SELECT 
	@@NewCustID CustID,
	@@FollowUpNo FollowUpNo ,
	CompanyName ,
	NPWPno ,
	NPWPdate ,
	NPWPaddress ,
	OfficeAddress ,
	PostalCode ,
	PICPhoneNo ,
	PICname ,
	Email ,
	NPWP ,
	SIUP ,
	1 RowStatus,
	CreatedBy ,
	GETDATE() CreatedDate
FROM dbo.ProspectCompany WHERE CustID = @0";
                            List<dynamic> lsCompany = mblDB.Fetch<dynamic>(qGetCompany, CustID, NewCustID, NewFollowUpNo);
                            foreach (dynamic cp in lsCompany)
                            {
                                string qInsertCompany = @";
IF NOT EXISTS(SELECT * FROM dbo.ProspectCompany WHERE CustID = @0)
BEGIN
	INSERT INTO dbo.ProspectCompany
			( CustID ,
				FollowUpNo ,
				CompanyName ,
				NPWPno ,
				NPWPdate ,
				NPWPaddress ,
				OfficeAddress ,
				PostalCode ,
				PICPhoneNo ,
				PICname ,
				Email ,
				NPWP ,
				SIUP ,
				RowStatus ,
				CreatedBy ,
				CreatedDate
			)
	VALUES( 
		@0,
		@1 ,
		@2 ,
		@3 ,
		@4 ,
		@5 ,
		@6 ,
		@7 ,
		@8 ,
		@9 ,
		@10 ,
		@11 ,
		@12 ,
		@13,
		@14 ,
		@15)
END";
                                mblDB.Execute(qInsertCompany,
                                        cp.CustID,
                                        cp.FollowUpNo,
                                        cp.CompanyName,
                                        cp.NPWPno,
                                        cp.NPWPdate,
                                        cp.NPWPaddress,
                                        cp.OfficeAddress,
                                        cp.PostalCode,
                                        cp.PICPhoneNo,
                                        cp.PICname,
                                        cp.Email,
                                        cp.NPWP,
                                        cp.SIUP,
                                        cp.RowStatus,
                                        cp.CreatedBy,
                                        cp.CreatedDate);
                            }
                            #endregion
                            #region followup
                            string qGetFollowUp = @";
DECLARE @@NewFollowUpNo VARCHAR(100) = @1
DECLARE @@NewCustID VARCHAR(100) = @2
DECLARE @@PolicyNo VARCHAR(100) = @3
SELECT 
	@@NewFollowUpNo FollowUpNo,
	LastSeqNo ,
	@@NewCustID CustID,
	ProspectName ,
	Phone1 ,
	Phone2 ,
	SalesOfficerID ,
	GETDATE() EntryDate,
	FollowUpName ,
	NextFollowUpDate ,
	LastFollowUpDate ,
	1 FollowUpStatus,
	1 FollowUpInfo,
	Remark ,
	BranchCode ,
	1 RowStatus,
	NULL PolicyId,
	@@PolicyNo PolicyNo,
	TransactionNo ,
	SalesAdminID ,
	SendDocDate ,
	IdentityCard ,
	STNK ,
	SPPAKB ,
	BSTB1 ,
	BSTB2 ,
	BSTB3 ,
	BSTB4 ,
	CheckListSurvey1 ,
	CheckListSurvey2 ,
	CheckListSurvey3 ,
	CheckListSurvey4 ,
	PaymentReceipt1 ,
	PaymentReceipt2 ,
	PaymentReceipt3 ,
	PaymentReceipt4 ,
	PremiumCal1 ,
	PremiumCal2 ,
	PremiumCal3 ,
	PremiumCal4 ,
	FormA1 ,
	FormA2 ,
	FormA3 ,
	FormA4 ,
	FormB1 ,
	FormB2 ,
	FormB3 ,
	FormB4 ,
	FormC1 ,
	FormC2 ,
	FormC3 ,
	FormC4 ,
	FUBidStatus ,
	FAKTUR ,
	KonfirmasiCust ,
	DocNSA1 ,
	DocNSA2 ,
	DocNSA3 ,
	DocRep ,
	IsRenewal ,
	RemarkToSA ,
	BuktiBayar ,
	FollowUpNotes ,
	PICWA ,
	DocPendukung ,
	RenewalNotice ,
	Survey ,
	RemarkFromSA ,
	IsRenewalDigital ,
	Receiver ,
	BuktiBayar2 ,
	BuktiBayar3 ,
	BuktiBayar4 ,
	BuktiBayar5 ,
	DocNSA4 ,
	DocNSA5 ,
	DocNSA6 
FROM dbo.FollowUp WHERE FollowUpNo = @0";
                            List<dynamic> lsFollowUp = mblDB.Fetch<dynamic>(qGetFollowUp, FollowUpNo, NewFollowUpNo, NewCustID, policyNo);
                            foreach (dynamic f in lsFollowUp)
                            {
                                string qInsertFollowUp = @";
IF NOT EXISTS(SELECT * FROM dbo.FollowUp WHERE FollowUpNo = @0)
BEGIN
	INSERT INTO dbo.FollowUp
			( FollowUpNo ,
				LastSeqNo ,
				CustID ,
				ProspectName ,
				Phone1 ,
				Phone2 ,
				SalesOfficerID ,
				EntryDate ,
				FollowUpName ,
				NextFollowUpDate ,
				LastFollowUpDate ,
				FollowUpStatus ,
				FollowUpInfo ,
				Remark ,
				BranchCode ,
				RowStatus ,
				PolicyId ,
				PolicyNo ,
				TransactionNo ,
				SalesAdminID ,
				SendDocDate ,
				IdentityCard ,
				STNK ,
				SPPAKB ,
				BSTB1 ,
				BSTB2 ,
				BSTB3 ,
				BSTB4 ,
				CheckListSurvey1 ,
				CheckListSurvey2 ,
				CheckListSurvey3 ,
				CheckListSurvey4 ,
				PaymentReceipt1 ,
				PaymentReceipt2 ,
				PaymentReceipt3 ,
				PaymentReceipt4 ,
				PremiumCal1 ,
				PremiumCal2 ,
				PremiumCal3 ,
				PremiumCal4 ,
				FormA1 ,
				FormA2 ,
				FormA3 ,
				FormA4 ,
				FormB1 ,
				FormB2 ,
				FormB3 ,
				FormB4 ,
				FormC1 ,
				FormC2 ,
				FormC3 ,
				FormC4 ,
				FUBidStatus ,
				FAKTUR ,
				KonfirmasiCust ,
				DocNSA1 ,
				DocNSA2 ,
				DocNSA3 ,
				DocRep ,
				IsRenewal ,
				RemarkToSA ,
				BuktiBayar ,
				FollowUpNotes ,
				PICWA ,
				DocPendukung ,
				RenewalNotice ,
				Survey ,
				RemarkFromSA ,
				IsRenewalDigital ,
				Receiver ,
				BuktiBayar2 ,
				BuktiBayar3 ,
				BuktiBayar4 ,
				BuktiBayar5 ,
				DocNSA4 ,
				DocNSA5 ,
				DocNSA6
			)
	VALUES( 
		@0,
		@1 ,
		@2,
		@3 ,
		@4 ,
		@5 ,
		@6 ,
		@7,
		@8 ,
		@9 ,
		@10 ,
		@11,
		@12,
		@13 ,
		@14 ,
		@15,
		@16 ,
		@17 ,
		@18 ,
		@19 ,
		@20 ,
		@21 ,
		@22 ,
		@23 ,
		@24 ,
		@25 ,
		@26 ,
		@27 ,
		@28 ,
		@29 ,
		@30 ,
		@31 ,
		@32 ,
		@33 ,
		@34 ,
		@35 ,
		@36 ,
		@37 ,
		@38 ,
		@39 ,
		@40 ,
		@41 ,
		@42 ,
		@43 ,
		@44 ,
		@45 ,
		@46 ,
		@47 ,
		@48 ,
		@49 ,
		@50 ,
		@51 ,
		@52 ,
		@53 ,
		@54 ,
		@55 ,
		@56 ,
		@57 ,
		@58 ,
		@59 ,
		@60 ,
		@61 ,
		@62 ,
		@63 ,
		@64 ,
		@65 ,
		@66 ,
		@67 ,
		@68 ,
		@69 ,
		@70 ,
		@71 ,
		@72 ,
		@73 ,
		@74 ,
		@75 ,
		@76)
END";
                                mblDB.Execute(qInsertFollowUp,
                                        f.FollowUpNo,
                                        f.LastSeqNo,
                                        f.CustID,
                                        f.ProspectName,
                                        f.Phone1,
                                        f.Phone2,
                                        f.SalesOfficerID,
                                        f.EntryDate,
                                        f.FollowUpName,
                                        f.NextFollowUpDate,
                                        f.LastFollowUpDate,
                                        f.FollowUpStatus,
                                        f.FollowUpInfo,
                                        f.Remark,
                                        f.BranchCode,
                                        f.RowStatus,
                                        f.PolicyId,
                                        f.PolicyNo,
                                        f.TransactionNo,
                                        f.SalesAdminID,
                                        f.SendDocDate,
                                        f.IdentityCard,
                                        f.STNK,
                                        f.SPPAKB,
                                        f.BSTB1,
                                        f.BSTB2,
                                        f.BSTB3,
                                        f.BSTB4,
                                        f.CheckListSurvey1,
                                        f.CheckListSurvey2,
                                        f.CheckListSurvey3,
                                        f.CheckListSurvey4,
                                        f.PaymentReceipt1,
                                        f.PaymentReceipt2,
                                        f.PaymentReceipt3,
                                        f.PaymentReceipt4,
                                        f.PremiumCal1,
                                        f.PremiumCal2,
                                        f.PremiumCal3,
                                        f.PremiumCal4,
                                        f.FormA1,
                                        f.FormA2,
                                        f.FormA3,
                                        f.FormA4,
                                        f.FormB1,
                                        f.FormB2,
                                        f.FormB3,
                                        f.FormB4,
                                        f.FormC1,
                                        f.FormC2,
                                        f.FormC3,
                                        f.FormC4,
                                        f.FUBidStatus,
                                        f.FAKTUR,
                                        f.KonfirmasiCust,
                                        f.DocNSA1,
                                        f.DocNSA2,
                                        f.DocNSA3,
                                        f.DocRep,
                                        f.IsRenewal,
                                        f.RemarkToSA,
                                        f.BuktiBayar,
                                        f.FollowUpNotes,
                                        f.PICWA,
                                        f.DocPendukung,
                                        f.RenewalNotice,
                                        f.Survey,
                                        f.RemarkFromSA,
                                        f.IsRenewalDigital,
                                        f.Receiver,
                                        f.BuktiBayar2,
                                        f.BuktiBayar3,
                                        f.BuktiBayar4,
                                        f.BuktiBayar5,
                                        f.DocNSA4,
                                        f.DocNSA5,
                                        f.DocNSA6);
                            }
                            #endregion
                            #region OrderSimulation
                            string qGetOS = @";
DECLARE @@NewOrderNo VARCHAR(100) = @1
DECLARE @@NewCustID VARCHAR(100) = @2
DECLARE @@NewFollowUpNo VARCHAR(100) = @3
DECLARE @@PolicyNo VARCHAR(100) = @4
SELECT
@@NewOrderNo OrderNo,
@@NewCustID CustID,
@@NewFollowUpNo FollowUpNo,
QuotationNo ,
MultiYearF ,
YearCoverage ,
TLOPeriod ,
ComprePeriod ,
BranchCode ,
SalesOfficerID ,
PhoneSales ,
DealerCode ,
SalesDealer ,
ProductTypeCode ,
ProductCode ,
AdminFee ,
TotalPremium ,
Phone1 ,
Phone2 ,
Email1 ,
Email2 ,
SendStatus ,
SendDate ,
GETDATE() EntryDate,
1 RowStatus,
InsuranceType ,
1 ApplyF,
SendF ,
LastInterestNo ,
LastCoverageNo ,
PolicySentTo ,
PolicyDeliveryName ,
NULL PolicyOrderNo ,
PolicyDeliveryAddress ,
PolicyDeliveryPostalCode ,
VANumber ,
NULL SurveyNo,
PeriodFrom ,
PeriodTo ,
@@PolicyNo PolicyNo,
AmountRep ,
NULL PolicyID,
SegmentCode ,
IsORDefectsRepair ,
IsAccessoriesChange ,
IsPolicyIssuedBeforePaying ,
ORDefectsDesc ,
Remarks ,
OldPolicyNo ,
IsNeedDocRep ,
isVAReactive
FROM dbo.OrderSimulation 
WHERE OrderNo = @0";
                            List<dynamic> lsOS = mblDB.Fetch<dynamic>(qGetOS, OrderNo, NewOrderNo, NewCustID, NewFollowUpNo, policyNo);
                            foreach (dynamic o in lsOS)
                            {
                                string qInsertOS = @";IF NOT EXISTS(SELECT * FROM dbo.OrderSimulation WHERE OrderNo = @0 AND CustID = @1)
BEGIN
	INSERT INTO dbo.OrderSimulation
			( OrderNo ,
				CustID ,
				FollowUpNo ,
				QuotationNo ,
				MultiYearF ,
				YearCoverage ,
				TLOPeriod ,
				ComprePeriod ,
				BranchCode ,
				SalesOfficerID ,
				PhoneSales ,
				DealerCode ,
				SalesDealer ,
				ProductTypeCode ,
				ProductCode ,
				AdminFee ,
				TotalPremium ,
				Phone1 ,
				Phone2 ,
				Email1 ,
				Email2 ,
				SendStatus ,
				SendDate ,
				EntryDate ,
				RowStatus ,
				InsuranceType ,
				ApplyF ,
				SendF ,
				LastInterestNo ,
				LastCoverageNo ,
				PolicySentTo ,
				PolicyDeliveryName ,
				PolicyOrderNo ,
				PolicyDeliveryAddress ,
				PolicyDeliveryPostalCode ,
				VANumber ,
				SurveyNo ,
				PeriodFrom ,
				PeriodTo ,
				PolicyNo ,
				AmountRep ,
				PolicyID ,
				SegmentCode ,
				IsORDefectsRepair ,
				IsAccessoriesChange ,
				IsPolicyIssuedBeforePaying ,
				ORDefectsDesc ,
				Remarks ,
				OldPolicyNo ,
				IsNeedDocRep ,
				isVAReactive
			)
	VALUES(
	@0,
	@1,
	@2,
	@3 ,
	@4 ,
	@5 ,
	@6 ,
	@7 ,
	@8 ,
	@9 ,
	@10 ,
	@11 ,
	@12 ,
	@13 ,
	@14 ,
	@15 ,
	@16 ,
	@17 ,
	@18 ,
	@19 ,
	@20 ,
	@21 ,
	@22 ,
	@23,
	@24,
	@25 ,
	@26 ,
	@27 ,
	@28 ,
	@29 ,
	@30 ,
	@31 ,
	@32 ,
	@33 ,
	@34 ,
	@35 ,
	@36,
	@37 ,
	@38 ,
	@39 ,
	@40 ,
	@41,
	@42 ,
	@43 ,
	@44 ,
	@45 ,
	@46 ,
	@47 ,
	@48 ,
	@49 ,
	@50)
END";
                                mblDB.Execute(qInsertOS,
                                    o.OrderNo,
o.CustID,
o.FollowUpNo,
o.QuotationNo,
o.MultiYearF,
o.YearCoverage,
o.TLOPeriod,
o.ComprePeriod,
o.BranchCode,
o.SalesOfficerID,
o.PhoneSales,
o.DealerCode,
o.SalesDealer,
o.ProductTypeCode,
o.ProductCode,
o.AdminFee,
o.TotalPremium,
o.Phone1,
o.Phone2,
o.Email1,
o.Email2,
o.SendStatus,
o.SendDate,
o.EntryDate,
o.RowStatus,
o.InsuranceType,
o.ApplyF,
o.SendF,
o.LastInterestNo,
o.LastCoverageNo,
o.PolicySentTo,
o.PolicyDeliveryName,
o.PolicyOrderNo,
o.PolicyDeliveryAddress,
o.PolicyDeliveryPostalCode,
o.VANumber,
o.SurveyNo,
o.PeriodFrom,
o.PeriodTo,
o.PolicyNo,
o.AmountRep,
o.PolicyID,
o.SegmentCode,
o.IsORDefectsRepair,
o.IsAccessoriesChange,
o.IsPolicyIssuedBeforePaying,
o.ORDefectsDesc,
o.Remarks,
o.OldPolicyNo,
o.IsNeedDocRep,
o.isVAReactive);
                            }
                            #endregion
                            #region OrderSimulationMV
                            string qGetMV = @";DECLARE @@NewOrderNo VARCHAR(100) = @1
SELECT 
	@@NewOrderNo OrderNo,
	ObjectNo ,
	ProductTypeCode ,
	VehicleCode ,
	BrandCode ,
	ModelCode ,
	Series ,
	Type ,
	Sitting ,
	Year ,
	CityCode ,
	UsageCode ,
	SumInsured ,
	AccessSI ,
	RegistrationNumber ,
	EngineNumber ,
	ChasisNumber ,
	RowStatus ,
	IsNew ,
	ColorOnBPKB 
FROM dbo.OrderSimulationMV WHERE OrderNo = @0";
                            List<dynamic> lsMV = mblDB.Fetch<dynamic>(qGetMV, OrderNo, NewOrderNo);
                            foreach (dynamic mv in lsMV)
                            {
                                string qInsertMV = @";
IF NOT EXISTS(SELECT * FROM dbo.OrderSimulationMV WHERE OrderNo = @0)
BEGIN
	INSERT INTO dbo.OrderSimulationMV
			( OrderNo ,
				ObjectNo ,
				ProductTypeCode ,
				VehicleCode ,
				BrandCode ,
				ModelCode ,
				Series ,
				Type ,
				Sitting ,
				Year ,
				CityCode ,
				UsageCode ,
				SumInsured ,
				AccessSI ,
				RegistrationNumber ,
				EngineNumber ,
				ChasisNumber ,
				RowStatus ,
				IsNew ,
				ColorOnBPKB
			)
	VALUES( 
		@0,
		@1 ,
		@2 ,
		@3 ,
		@4 ,
		@5 ,
		@6 ,
		@7 ,
		@8 ,
		@9 ,
		@10 ,
		@11 ,
		@12 ,
		@13 ,
		@14 ,
		@15 ,
		@16 ,
		@17 ,
		@18 ,
		@19)
END";
                                mblDB.Execute(qInsertMV,
mv.OrderNo,
mv.ObjectNo,
mv.ProductTypeCode,
mv.VehicleCode,
mv.BrandCode,
mv.ModelCode,
mv.Series,
mv.Type,
mv.Sitting,
mv.Year,
mv.CityCode,
mv.UsageCode,
mv.SumInsured,
mv.AccessSI,
mv.RegistrationNumber,
mv.EngineNumber,
mv.ChasisNumber,
mv.RowStatus,
mv.IsNew,
mv.ColorOnBPKB);
                            }
                            #endregion
                            #region OSInterest
                            string qGetInterest = @";
DECLARE @@NewOrderNo VARCHAR(100) = @1
SELECT 
@@NewOrderNo OrderNo,
ObjectNo ,
InterestNo ,
InterestID ,
Year ,
Premium ,
PeriodFrom ,
PeriodTo ,
GETDATE() LastUpdatedTime,
1 RowStatus,
Deductible_Code
FROM dbo.OrderSimulationInterest WHERE OrderNo = @0";
                            List<dynamic> lsInterest = mblDB.Fetch<dynamic>(qGetInterest, OrderNo, NewOrderNo);
                            foreach (dynamic i in lsInterest)
                            {
                                string qInsertInterest = @";
IF NOT EXISTS(SELECT * FROM dbo.OrderSimulationInterest WHERE OrderNo = @0 AND ObjectNo = @1 AND InterestNo = @2 AND InterestID = @3)
BEGIN
	INSERT INTO dbo.OrderSimulationInterest
			( OrderNo ,
				ObjectNo ,
				InterestNo ,
				InterestID ,
				Year ,
				Premium ,
				PeriodFrom ,
				PeriodTo ,
				LastUpdatedTime ,
				RowStatus ,
				Deductible_Code
			)
	VALUES (
	@0 ,
	@1 ,
	@2 ,
	@3 ,
	@4 ,
	@5 ,
	@6 ,
	@7 ,
	@8 ,
	@9 ,
	@10)
END";
                                mblDB.Execute(qInsertInterest,
i.OrderNo,
i.ObjectNo,
i.InterestNo,
i.InterestID,
i.Year,
i.Premium,
i.PeriodFrom,
i.PeriodTo,
i.LastUpdatedTime,
i.RowStatus,
i.Deductible_Code);
                            }
                            #endregion
                            #region OSCoverage 
                            string qGetOSCoverage = @";
DECLARE @@NewOrderNo VARCHAR(100) = @1
SELECT 
	@@NewOrderNo OrderNo,
	ObjectNo ,
	InterestNo ,
	CoverageNo ,
	CoverageID ,
	Rate ,
	SumInsured ,
	LoadingRate ,
	Loading ,
	Premium ,
	BeginDate ,
	EndDate ,
	GETDATE() LastUpdatedTime,
	1 RowStatus,
	IsBundling ,
	Entry_Pct ,
	Ndays ,
	Excess_Rate ,
	Calc_Method ,
	Cover_Premium ,
	Gross_Premium ,
	Max_si ,
	Net1 ,
	Net2 ,
	Net3 ,
	Deductible_Code 
FROM dbo.OrderSimulationCoverage WHERE OrderNo = @0";
                            List<dynamic> lsCoverage = mblDB.Fetch<dynamic>(qGetOSCoverage, OrderNo, NewOrderNo);
                            foreach (dynamic cvg in lsCoverage)
                            {
                                string qInsertCoverage = @";IF NOT EXISTS (SELECT * FROM dbo.OrderSimulationCoverage WHERE OrderNo = @0 AND ObjectNo = @1 
AND InterestNo = @2 AND CoverageNo = @3 AND CoverageID = @4)
BEGIN		
	INSERT INTO dbo.OrderSimulationCoverage
			( OrderNo ,
				ObjectNo ,
				InterestNo ,
				CoverageNo ,
				CoverageID ,
				Rate ,
				SumInsured ,
				LoadingRate ,
				Loading ,
				Premium ,
				BeginDate ,
				EndDate ,
				LastUpdatedTime ,
				RowStatus ,
				IsBundling ,
				Entry_Pct ,
				Ndays ,
				Excess_Rate ,
				Calc_Method ,
				Cover_Premium ,
				Gross_Premium ,
				Max_si ,
				Net1 ,
				Net2 ,
				Net3 ,
				Deductible_Code
			)
	VALUES( 
		@0 ,
		@1 ,
		@2 ,
		@3 ,
		@4 ,
		@5 ,
		@6 ,
		@7 ,
		@8 ,
		@9 ,
		@10 ,
		@11 ,
		@12 ,
		@13 ,
		@14 ,
		@15 ,
		@16 ,
		@17 ,
		@18 ,
		@19 ,
		@20 ,
		@21 ,
		@22 ,
		@23 ,
		@24 ,
		@25)
END";
                                mblDB.Execute(qInsertCoverage,
	cvg.OrderNo,
    cvg.ObjectNo,
    cvg.InterestNo,
    cvg.CoverageNo,
    cvg.CoverageID,
    cvg.Rate,
    cvg.SumInsured,
    cvg.LoadingRate,
    cvg.Loading,
    cvg.Premium,
    cvg.BeginDate,
    cvg.EndDate,
    cvg.LastUpdatedTime,
    cvg.RowStatus,
    cvg.IsBundling,
    cvg.Entry_Pct,
    cvg.Ndays,
    cvg.Excess_Rate,
    cvg.Calc_Method,
    cvg.Cover_Premium,
    cvg.Gross_Premium,
    cvg.Max_si,
    cvg.Net1,
    cvg.Net2,
    cvg.Net3,
    cvg.Deductible_Code);
                            }
                            #endregion
                            #region OSSurvey
                            string qGetOSSurvey = @";DECLARE @@NewOrderNo VARCHAR(100) = @1
SELECT 
	@@NewOrderNo OrderNo ,
	CityCode ,
	LocationCode ,
	SurveyAddress ,
	SurveyPostalCode ,
	SurveyDate ,
	ScheduleTimeID ,
	IsNeedSurvey ,
	IsNSASkipSurvey ,
	1 RowStatus,
	CreatedBy ,
	GETDATE() CreatedDate,
	IsManualSurvey
FROM dbo.OrderSimulationSurvey WHERE OrderNo = @0";
                            List<dynamic> lsSurvey = mblDB.Fetch<dynamic>(qGetOSSurvey, OrderNo, NewOrderNo);
                            foreach (dynamic sv in lsSurvey)
                            {
                                string qInsertOSSurvey = @";IF NOT EXISTS(SELECT * FROM dbo.OrderSimulationMV WHERE OrderNo = @0)
BEGIN
INSERT INTO dbo.OrderSimulationSurvey
		( OrderNo ,
			CityCode ,
			LocationCode ,
			SurveyAddress ,
			SurveyPostalCode ,
			SurveyDate ,
			ScheduleTimeID ,
			IsNeedSurvey ,
			IsNSASkipSurvey ,
			RowStatus ,
			CreatedBy ,
			CreatedDate ,
			IsManualSurvey
		)
VALUES( 
	@0 ,
	@1 ,
	@2 ,
	@3 ,
	@4 ,
	@5 ,
	@6 ,
	@7 ,
	@8 ,
	@9 ,
	@10 ,
	@11,
	@12)
END";
                                mblDB.Execute(qInsertOSSurvey,
	sv.OrderNo,
    sv.CityCode,
    sv.LocationCode,
    sv.SurveyAddress,
    sv.SurveyPostalCode,
    sv.SurveyDate,
    sv.ScheduleTimeID,
    sv.IsNeedSurvey,
    sv.IsNSASkipSurvey,
    sv.RowStatus,
    sv.CreatedBy,
    sv.CreatedDate,
    sv.IsManualSurvey
                                    );
                            }
                            #endregion
                        }
                        if (!string.IsNullOrEmpty(lsOrderDt.First().OldPolicyNo) && !string.IsNullOrWhiteSpace(lsOrderDt.First().OldPolicyNo)) {
                            aabDB.Execute(@";UPDATE dbo.Renewal_Notice 
SET New_Policy_No = @1, 
UpdateDt = GETDATE(), UpdateUsr = 'OTOSL'
WHERE Policy_No = @0",OldPolicyNo, policyNo);
                        }
                        //mblDB.Execute(@";UPDATE dbo.OrderSimulation 
                        //SET ApplyF = 0
                        //WHERE OrderNo = @0",OrderNo);
                        mblDB.Execute(@";UPDATE dbo.FollowUp 
                                        SET FollowUpStatus = 5, FollowUpInfo = 63 
                                        WHERE FollowUpNo = @0", FollowUpNo);
                    }
                    #endregion
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.Message));
                return false;
            }
        }
        public bool RollBackImageWO(string FollowUpNo) {
            string CurrentMethod = GetCurrentMethod();
            var mbldb = GetAABMobileDB();
            try
            {
                string qGetOrdData = @";SELECT COALESCE(CustIDAAB,'') CustIDAAB, COALESCE(ProspectID,'') ProspectID
                                        FROM dbo.OrderSimulation os
                                        INNER JOIN dbo.FollowUp f
                                        ON f.FollowUpNo = os.FollowUpNo
                                        INNER JOIN dbo.ProspectCustomer ps
                                        ON ps.CustID = os.CustID
						                WHERE os.FollowUpNo = @0
                                        AND os.ApplyF = 1 AND os.RowStatus = 1";
                List<dynamic> ordData = mbldb.Fetch<dynamic>(qGetOrdData, FollowUpNo);
                if (ordData.Count > 0) {
                    if (string.IsNullOrEmpty(ordData.First().CustIDAAB) || string.IsNullOrWhiteSpace(ordData.First().CustIDAAB)) {
                        string qGetImageIdPersonal = @";SELECT id.CoreImage_ID, id.PathFile FROM 
(
SELECT 
	FollowUpNo,ImageName, ImageType
FROM 
(
select fu.FollowUpNo, IdentityCard, NPWP, SIUP
from FollowUp fu WITH (NOLOCK)
LEFT JOIN ProspectCompany pcm WITH (NOLOCK) ON fu.CustID = pcm.CustID
where fu.followupno = @0
)a 
UNPIVOT (
	ImageName FOR ImageType IN (
		IdentityCard, NPWP, SIUP
	)
) unpvt
where ImageName <> ''
) a 
INNER JOIN ImageData id WITH (NOLOCK) on ImageName = id.PathFile";
                        List<dynamic> ListImageIDPersonal = mbldb.Fetch<dynamic>(qGetImageIdPersonal, FollowUpNo);
                        foreach (dynamic d in ListImageIDPersonal)
                        {
                            if (!string.IsNullOrEmpty(d.CoreImage_ID))
                            {
                                mbldb.Execute(@";UPDATE dbo.ImageData 
                                            SET CoreImage_ID = NULL
                                            WHERE CoreImage_ID = @0", d.CoreImage_ID);
                            }
                        }
                    }
                    string qGetImageIdOrder = @";SELECT id.CoreImage_ID, id.PathFile FROM 
(
SELECT 
	FollowUpNo,ImageName, ImageType
FROM 
(
select fu.FollowUpNo, STNK, SPPAKB, BSTB1, Faktur, KonfirmasiCust, DocNSA1, DocNSA2, DocNSA3, DocRep, BuktiBayar, DocPendukung, RenewalNotice, Survey, BuktiBayar2, BuktiBayar3, BuktiBayar4, BuktiBayar5, DocNSA4, DocNSA5, DocNSA6 --DocRep, CustomerConfirmation, *
from FollowUp fu WITH (NOLOCK)
LEFT JOIN ProspectCompany pcm WITH (NOLOCK) ON fu.CustID = pcm.CustID
where fu.followupno = @0
)a 
UNPIVOT (
	ImageName FOR ImageType IN (
		STNK, SPPAKB, BSTB1, Faktur, KonfirmasiCust, DocNSA1, DocNSA2, DocNSA3, DocRep, BuktiBayar, DocPendukung, RenewalNotice, Survey, BuktiBayar2, BuktiBayar3, BuktiBayar4, BuktiBayar5, DocNSA4, DocNSA5, DocNSA6
	)
) unpvt
where ImageName <> ''
) a 
INNER JOIN ImageData id WITH (NOLOCK) on ImageName = id.PathFile";
                    List<dynamic> ListImageIDOrder = mbldb.Fetch<dynamic>(qGetImageIdOrder, FollowUpNo);
                    foreach (dynamic d in ListImageIDOrder)
                    {
                        if (!string.IsNullOrEmpty(d.CoreImage_ID)) {
                            mbldb.Execute(@";UPDATE dbo.ImageData 
                                            SET CoreImage_ID = NULL
                                            WHERE CoreImage_ID = @0", d.CoreImage_ID);
                        }
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.Message));
                return false;
            }
        }
        public bool ReGenerateOrder(string OldPolicyNo, string FollowUpNo, string PolicyNo, int StatusCode, int InfoCode)
        {
            string CurrentMethod = GetCurrentMethod();
            var aabDB = GetAABDB();
            var mblDB = GetAABMobileDB();
            try
            {
                if (!string.IsNullOrEmpty(OldPolicyNo) && !string.IsNullOrWhiteSpace(OldPolicyNo))
                {
                    string NewPolicyNo = "";
                    if ((StatusCode == Convert.ToInt32(FollowUpStatus.NotDeal) && InfoCode == Convert.ToInt32(FollowUpInfo.PolicyWO))||(
                        StatusCode == Convert.ToInt32(FollowUpStatus.DealStatusCode) && InfoCode == Convert.ToInt32(FollowUpInfo.PolicyCancel)))
                    {
                        NewPolicyNo = CommonLogic.GetPolicyNo();
                        aabDB.Execute(@";UPDATE dbo.Renewal_Notice 
                                        SET New_Policy_No = @1, 
                                        UpdateDt = GETDATE(), UpdateUsr = 'OTOSL'
                                        WHERE Policy_No = @0", OldPolicyNo, NewPolicyNo);
                    }
                    else {
                        NewPolicyNo = PolicyNo;
                    }
                    List<dynamic> lsExc = aabDB.Fetch<dynamic>(@";SELECT * FROM dbo.Excluded_Renewal_Policy WHERE Policy_No = @0", OldPolicyNo);
                    if (lsExc.Count > 0)
                    {
                        aabDB.Execute(@";
UPDATE dbo.Excluded_Renewal_Policy 
SET IsSendToSA = 0, 
UpdateDt = GETDATE(), UpdateUsr = 'OTOSL'
WHERE Policy_No = @0", OldPolicyNo);
                    }
                    else {
                        CustomerInfo tempCustInfo = new CustomerInfo();
                        OrderSimulation tempOrdSimulation = new OrderSimulation();
                        OrderSimulationMV tempOrdSimulationmv = new OrderSimulationMV();
                        List<OrderSimulationInterest> tempOrdSimulationInterest = new List<OrderSimulationInterest>();
                        List<OrderSimulationCoverage> tempOrdSimulationCoverage = new List<OrderSimulationCoverage>();
                        OtosalesOrderDataID otosalesOrderDataID = new OtosalesOrderDataID();
                        List<ImageData> listImageData = new List<ImageData>();
                        tempCustInfo = RenewalRepository.GetDataCustomer(OldPolicyNo);
                        tempOrdSimulation = RenewalRepository.GetDataOrderSimulation(OldPolicyNo, NewPolicyNo);
                        tempOrdSimulationmv = RenewalRepository.GetDataOrderSimulationMV(OldPolicyNo);
                        tempOrdSimulationInterest = RenewalRepository.GetDataOrderSimulationInterest(OldPolicyNo);
                        tempOrdSimulationCoverage = RenewalRepository.GetDataOrderSimulationCoverage(OldPolicyNo);

                        listImageData = RenewalRepository.GetDataImageAAB(tempCustInfo);
                        OtosalesOrderDataID oData = RenewalRepository.GenerateTasklist(tempCustInfo, tempOrdSimulation, tempOrdSimulationmv, tempOrdSimulationInterest, tempOrdSimulationCoverage, OldPolicyNo, otosalesOrderDataID, listImageData);

                        RenewalRepository.InsertDataTasklistOtosales(OldPolicyNo, oData);

                    }
                }
                mblDB.Execute(@";UPDATE dbo.FollowUp 
                                        SET FollowUpStatus = @1, FollowUpInfo = @2 
                                        WHERE FollowUpNo = @0", FollowUpNo, StatusCode, InfoCode);
                return true;
            }
            catch (Exception e)
            {
                logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.Message));
                return false;
            }
        }

        public List<dynamic> GetMstOrderWithStatusRecreate()
        {
            using (var db = GetAABMobileDB())
            {
                string query = @";
SELECT os.FollowUpNo, os.OldPolicyNo, os.PolicyNo, gos.OrderStatus, f.FollowUpStatus, f.FollowUpInfo
FROM dbo.OrderSimulation os 
INNER JOIN (SELECT MAX(grn1.RenewalNoticeID) RenewalNoticeID, PolicyNo FROM Asuransiastra.GODigital.RenewalNotice grn1 
WHERE RowStatus = 0 GROUP BY PolicyNo) a ON a.PolicyNo = os.OldPolicyNo
INNER JOIN Asuransiastra.GODigital.RenewalNotice grn
ON os.OldPolicyNo = grn.PolicyNo AND grn.RenewalNoticeID = a.RenewalNoticeID
INNER JOIN Asuransiastra.GODigital.RenewableData rd ON rd.RenewalNoticeID = grn.RenewalNoticeID
LEFT JOIN Asuransiastra.GODigital.OrderSimulation gos ON gos.TempPenawaranGuid = rd.TempPenawaranGuid
INNER JOIN (SELECT MAX(f1.LastUpdatedTime) LastUpdatedTime, os1.OldPolicyNo FROM dbo.OrderSimulation os1 
INNER JOIN dbo.FollowUp f1 ON f1.FollowUpNo = os1.FollowUpNo
WHERE os1.ApplyF = 1 AND os1.RowStatus = 1 GROUP BY os1.OldPolicyNo) b ON b.OldPolicyNo = os.OldPolicyNo
INNER JOIN dbo.FollowUp f 
ON f.FollowUpNo = os.FollowUpNo AND f.LastUpdatedTime = b.LastUpdatedTime
WHERE grn.IsRecreateF = 1 AND grn.RowStatus = 0
AND f.FollowUpStatus = 14 AND COALESCE(f.FollowUpInfo,0) = 0";
                bool isProcess = true;
                int itr = 0;
                List<dynamic> res = new List<dynamic>();
                while (isProcess)
                {
                    try
                    {
                        res = db.Fetch<dynamic>(query);
                        isProcess = false;
                    }
                    catch (Exception)
                    {
                        itr++;
                        if (itr > 3)
                        {
                            isProcess = false;
                        }
                    }
                }
                return res;
            }
        }

        public List<dynamic> GetMstOrderWithStatusCancel()
        {
            using (var db = GetAABDB())
            {
                string query = @";
SELECT mo.Policy_No, mo.Order_No, mo.Old_Policy_No, Endorsement_No, mo.Order_Type,
ROW_NUMBER() OVER (ORDER BY mo.Policy_No, mo.Old_Policy_No, mo.Endorsement_No DESC) AS RowNumber 
INTO #tempOtosalesOrderCancel
FROM dbo.Mst_Order mo 
INNER JOIN BEYONDMOSS.AABMobile.dbo.OrderSimulation os
on os.PolicyNo = mo.Policy_No 
INNER JOIN BEYONDMOSS.AABMobile.dbo.FollowUp f 
ON f.FollowUpNo = os.FollowUpNo 
AND os.RowStatus = 1 AND os.ApplyF = 1 AND f.RowStatus = 1
AND f.FollowUpStatus = 13 AND FollowUpInfo <> 92

SELECT mo1.Policy_No, os1.FollowUpNo, os1.OrderNo, mo1.Old_Policy_No, os1.CustID
FROM dbo.Mst_Order mo1 
INNER JOIN dbo.Policy p ON p.Order_No = mo1.Order_No
INNER JOIN BEYONDMOSS.AABMobile.dbo.OrderSimulation os1
ON os1.PolicyNo = mo1.Policy_No
INNER JOIN BEYONDMOSS.AABMobile.dbo.FollowUp f 
ON f.FollowUpNo = os1.FollowUpNo
WHERE mo1.Order_No = (SELECT TOP 1 Order_No FROM #tempOtosalesOrderCancel 
					WHERE Policy_No = mo1.Policy_No ORDER BY RowNumber)
AND mo1.Order_Type = 4 AND p.Status = 'C'

DROP TABLE #tempOtosalesOrderCancel";
                bool isProcess = true;
                int itr = 0;
                List<dynamic> res = new List<dynamic>();
                while (isProcess)
                {
                    try
                    {
                        res = db.Fetch<dynamic>(query);
                        isProcess = false;
                    }
                    catch (Exception)
                    {
                        itr++;
                        if (itr > 3)
                        {
                            isProcess = false;
                        }
                    }
                }
                return res;
            }
        }
        public List<dynamic> GetMstOrderWithStatusEndorse()
        {
            using (var db = GetAABDB())
            {
                string query = @";
SELECT mo.Policy_No, mo.Order_No, mo.Old_Policy_No, Endorsement_No, mo.Order_Type,
ROW_NUMBER() OVER (ORDER BY mo.Policy_No, mo.Old_Policy_No, mo.Endorsement_No DESC) AS RowNumber 
INTO #tempOtosalesOrderEndorse
FROM dbo.Mst_Order mo 
INNER JOIN BEYONDMOSS.AABMobile.dbo.OrderSimulation os
on os.PolicyNo = mo.Policy_No 
INNER JOIN BEYONDMOSS.AABMobile.dbo.FollowUp f 
ON f.FollowUpNo = os.FollowUpNo 
AND os.RowStatus = 1 AND os.ApplyF = 1 AND f.RowStatus = 1
AND f.FollowUpStatus = 13 AND FollowUpInfo <> 93 AND Endorsement_No > 0

SELECT mo1.Policy_No, os1.FollowUpNo, os1.OrderNo, mo1.Old_Policy_No, os1.CustID, mo1.Order_No
FROM dbo.Mst_Order mo1 
INNER JOIN BEYONDMOSS.AABMobile.dbo.OrderSimulation os1
ON os1.PolicyNo = mo1.Policy_No
INNER JOIN BEYONDMOSS.AABMobile.dbo.FollowUp f 
ON f.FollowUpNo = os1.FollowUpNo
WHERE mo1.Order_No = (SELECT TOP 1 Order_No FROM #tempOtosalesOrderEndorse 
					WHERE Policy_No = mo1.Policy_No ORDER BY RowNumber)
AND mo1.Order_Type = 3 AND PolicyOrderNo IS NOT NULL

DROP TABLE #tempOtosalesOrderEndorse";
                bool isProcess = true;
                int itr = 0;
                List<dynamic> res = new List<dynamic>();
                while (isProcess)
                {
                    try
                    {
                        res = db.Fetch<dynamic>(query);
                        isProcess = false;
                    }
                    catch (Exception)
                    {
                        itr++;
                        if (itr > 3)
                        {
                            isProcess = false;
                        }
                    }
                }
                return res;
            }
        }

        public bool UpdateEndorseOrder(string FollowUpNo, string OrderNo, string NewPolicyOrderNo) {
            string CurrentMethod = GetCurrentMethod();
            try
            {
                var db = GetAABMobileDB();
                int res = 0;
                int tryCount = 0;
                #region updateOrder
                //db.Execute(@"UPDATE dbo.OrderSimulation SET PolicyOrderNo = @1 WHERE OrderNo = @0 AND RowStatus = 1 AND ApplyF = 1",
                //    OrderNo, NewPolicyOrderNo);
                //                while (res == 0)
                //                {
                //                    res = OrdLogic.UpdateOrderMobile(OrderNo);
                //                    tryCount++;
                //                    if (tryCount == 3)
                //                    {
                //                        res = 1;
                //                        db.Execute(@"UPDATE dbo.FollowUp 
                //SET Remark = Remark+' ""Failed Update EndorseOrder""'
                //WHERE FollowUpNo = @0 AND RowStatus = 1", FollowUpNo);
                //                    }
                //                }
                #endregion
                res = 1;
                if (res > 0) {
                    db.Execute(@";UPDATE dbo.FollowUp 
                                        SET FollowUpStatus = @1, FollowUpInfo = @2 
                                        WHERE FollowUpNo = @0", FollowUpNo, FollowUpStatus.DealStatusCode, FollowUpInfo.PolicyEndorse);
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.Message));
                return false;
            }
        }

        public bool ReGenerateOrderNotDeal(string OrderNo,string OldPolicyNo)
        {
            string CurrentMethod = GetCurrentMethod();
            var aabDB = GetAABDB();
            var mblDB = GetAABMobileDB();
            try
            {
                if (!string.IsNullOrEmpty(OldPolicyNo) && !string.IsNullOrWhiteSpace(OldPolicyNo))
                {
                    string NewPolicyNo = CommonLogic.GetPolicyNo();
                    aabDB.Execute(@";UPDATE dbo.Renewal_Notice 
                                        SET New_Policy_No = @1, 
                                        UpdateDt = GETDATE(), UpdateUsr = 'OTOSL'
                                        WHERE Policy_No = @0", OldPolicyNo, NewPolicyNo);

                    List<dynamic> lsExc = aabDB.Fetch<dynamic>(@";SELECT * FROM dbo.Excluded_Renewal_Policy WHERE Policy_No = @0", OldPolicyNo);
                    if (lsExc.Count > 0)
                    {
                        aabDB.Execute(@";
UPDATE dbo.Excluded_Renewal_Policy 
SET IsSendToSA = 0, 
UpdateDt = GETDATE(), UpdateUsr = 'OTOSL'
WHERE Policy_No = @0", OldPolicyNo);
                    }
                    else
                    {
                        CustomerInfo tempCustInfo = new CustomerInfo();
                        OrderSimulation tempOrdSimulation = new OrderSimulation();
                        OrderSimulationMV tempOrdSimulationmv = new OrderSimulationMV();
                        List<OrderSimulationInterest> tempOrdSimulationInterest = new List<OrderSimulationInterest>();
                        List<OrderSimulationCoverage> tempOrdSimulationCoverage = new List<OrderSimulationCoverage>();
                        OtosalesOrderDataID otosalesOrderDataID = new OtosalesOrderDataID();
                        List<ImageData> listImageData = new List<ImageData>();
                        tempCustInfo = RenewalRepository.GetDataCustomer(OldPolicyNo);
                        tempOrdSimulation = RenewalRepository.GetDataOrderSimulation(OldPolicyNo, NewPolicyNo);
                        tempOrdSimulationmv = RenewalRepository.GetDataOrderSimulationMV(OldPolicyNo);
                        tempOrdSimulationInterest = RenewalRepository.GetDataOrderSimulationInterest(OldPolicyNo);
                        tempOrdSimulationCoverage = RenewalRepository.GetDataOrderSimulationCoverage(OldPolicyNo);

                        listImageData = RenewalRepository.GetDataImageAAB(tempCustInfo);
                        OtosalesOrderDataID oData = RenewalRepository.GenerateTasklist(tempCustInfo, tempOrdSimulation, tempOrdSimulationmv, tempOrdSimulationInterest, tempOrdSimulationCoverage, OldPolicyNo, otosalesOrderDataID, listImageData);

                        RenewalRepository.InsertDataTasklistOtosales(OldPolicyNo, oData);

                    }
                }
                mblDB.Execute(@";UPDATE dbo.RenewalNotDealToNewData 
SET IsProcess = 1, UpdateUser = 'OTOSL', UpdateDate = GETDATE()
WHERE OrderNo = @0 AND OldPolicyNo = @1 AND IsProcess = 0", OrderNo, OldPolicyNo);
                return true;
            }
            catch (Exception e)
            {
                logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.Message));
                return false;
            }
        }

        public List<dynamic> GetOrderRenewNotDealToRegenerate()
        {
            using (var db = GetAABMobileDB())
            {
                string query = @";
SELECT OrderNo, OldPolicyNo FROM dbo.RenewalNotDealToNewData WHERE IsProcess = 0 AND RowStatus = 1";
                bool isProcess = true;
                int itr = 0;
                List<dynamic> res = new List<dynamic>();
                while (isProcess)
                {
                    try
                    {
                        res = db.Fetch<dynamic>(query);
                        isProcess = false;
                    }
                    catch (Exception)
                    {
                        itr++;
                        if (itr > 3)
                        {
                            isProcess = false;
                        }
                    }
                }
                return res;
            }
        }

    }
}
