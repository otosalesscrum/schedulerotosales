﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otosales.MonitorOrder.Model
{
    class OrderSimulationCoverage
    {
        public int ObjectNo { get; set; }
        public int InterestNo { get; set; }
        public int CoverageNo { get; set; }
        public string CoverageID { get; set; }
        public double Rate { get; set; }
        public double SumInsured { get; set; }
        public double LoadingRate { get; set; }
        public double Loading { get; set; }
        public double Premium { get; set; }
        public string BeginDate { get; set; }
        public string EndDate { get; set; }
        public int IsBundling { get; set; }
        public double EntryPct { get; set; }
        public double Ndays { get; set; }
        public double ExcessRate { get; set; }
        public double CalcMethod { get; set; }
        public double CoverPremium { get; set; }
        public double GrossPremium { get; set; }
        public double MaxSI { get; set; }
        public double Net1 { get; set; }
        public double Net2 { get; set; }
        public double Net3 { get; set; }
        public string DeductibleCode { get; set; }
    }
}
