﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otosales.MonitorOrder.Model
{
    class ImageData
    {
        public string ProspectId { get; set; }
        public string ImageType { get; set; }
        public string ImageName { get; set; }
        public string ImageId { get; set; }
        public byte[] ImageMasterData { get; set; }
        public byte[] ImageThumbnailData { get; set; }
    }
}
