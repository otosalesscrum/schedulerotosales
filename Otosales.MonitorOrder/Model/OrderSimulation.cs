﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otosales.MonitorOrder.Model
{
    class OrderSimulation
    {
        public string QuotationNo { get; set; }
        public int MultiYearF { get; set; }
        public int YearCoverage { get; set; }
        public int TLOPeriod { get; set; }
        public int ComprePeriod { get; set; }
        public string BranchCode { get; set; }
        public string SalesOfficerID { get; set; }
        public string PhoneSales { get; set; }
        public string DealerCode { get; set; }
        public string SalesDealer { get; set; }
        public string ProductTypeCode { get; set; }
        public string AdminFee { get; set; }
        public string TotalPremium { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Email1 { get; set; }
        public string Email2 { get; set; }
        public string SendStatus { get; set; }
        public string InsuranceType { get; set; }
        public string ApplyF { get; set; }
        public string SendF { get; set; }
        public string LastInterestNo { get; set; }
        public string LastCoverageNo { get; set; }
        public string NewPolicyNo { get; set; }
        public string NewPolicyId { get; set; }
        public string VANumber { get; set; }
        public string ProductCode { get; set; }
        public DateTime? PeriodFrom { get; set; }
        public DateTime? PeriodTo { get; set; }
        public string SegmentCode { get; set; }
        public string PolicyDeliverAddress { get; set; }
        public string PolicyDeliverPostalCode { get; set; }
        public string PolicyDeliveryType { get; set; }
        public string PolicyDeliveryName { get; set; }
    }
}
