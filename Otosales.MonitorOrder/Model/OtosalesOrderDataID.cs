﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otosales.MonitorOrder.Model
{
    class OtosalesOrderDataID
    {
        public string CustID { get; set; }
        public string FollowUpID { get; set; }
        public string OrderID { get; set; }
    }
}
