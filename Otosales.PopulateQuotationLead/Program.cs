﻿using a2is.Framework.Monitoring;
using Otosales.Logic;
using Otosales.QuotationLead.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otosales.QuotationLead
{
    class Program
    {
        private static a2isLogHelper logger = new a2isLogHelper();
        static void Main(string[] args)
        {
            if (CommonLogic.GetSettingQuotationLeadType().ToUpper().Equals("POPULATE"))
            {
                PopulateQuotationLead();
            }
            else if (CommonLogic.GetSettingQuotationLeadType().ToUpper().Equals("RETRACT"))
            {
                RetractQuotationLead();
            }
        }
        static void PopulateQuotationLead()
        {
            string CurrentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                logger.Debug(string.Format("Function {0} BEGIN:'", CurrentMethod));
                OtosalesOrderRepository OOR = new OtosalesOrderRepository();
                OOR.PopulateQuotationLead();
                logger.Debug(string.Format("Function {0} END:'", CurrentMethod));
            }
            catch (Exception e)
            {
                logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.Message));
            }
        }
        static void RetractQuotationLead()
        {
            string CurrentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                logger.Debug(string.Format("Function {0} BEGIN:'", CurrentMethod));
                OtosalesOrderRepository OOR = new OtosalesOrderRepository();
                OOR.RetractQuotationLead();
                logger.Debug(string.Format("Function {0} END:'", CurrentMethod));
            }
            catch (Exception e)
            {
                logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.Message));
            }
        }

    }
}
