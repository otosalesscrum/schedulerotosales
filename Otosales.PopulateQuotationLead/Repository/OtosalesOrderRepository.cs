﻿using a2is.Framework.Monitoring;
using Otosales.Logic;
using Otosales.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otosales.QuotationLead.Repository
{
    class OtosalesOrderRepository : RepositoryBase
    {
        private static a2isLogHelper logger = new a2isLogHelper();
        private static string GetCurrentMethod()
        {
            return System.Reflection.MethodBase.GetCurrentMethod().Name;
        }
        public bool PopulateQuotationLead()
        {
            string CurrentMethod = GetCurrentMethod();
            var AsuransiAstraDB = GetAsuransiAstraDB();
            var AABMobileDB = GetAABMobileDB();
            try
            {
                //string qwaktu = @"";
                //if (CommonLogic.GetSettingQuotationLeadTime().ToUpper().Equals("TIME1"))
                //{
                //    qwaktu = "CAST('00:01' AS TIME) AND CAST('12:00' AS TIME)";
                //}
                //else if (CommonLogic.GetSettingQuotationLeadTime().ToUpper().Equals("TIME2"))
                //{
                //    qwaktu = "CAST('12:01' AS TIME) AND CAST('23:59:59.9999999' AS TIME)";
                //}

                string qGetQuotationLead = @";use asuransiastra

DECLARE @@CutOffDate DATETIME
SELECT @@CutOffDate = CAST(ParamValue AS DATETIME) FROM GODigital.ApplicationParameters WHERE ParamName = 'OTOSALES-CUTOFFDATEQUOTATIONLEAD'

SELECT ReferenceNo, OrderSource, CustName, PolicyNo, ChasisNumber, EngineNumber, CustPhone 
INTO #tempQotationLead
FROM(SELECT tp.Guid ReferenceNo, 1 OrderSource, tp.customerName CustName, ov.PolicyNo PolicyNo, 
tp.VehicleChasisNumber ChasisNumber, tp.VehicleEngineNumber EngineNumber, tp.customerPhone CustPhone
FROM dbo.TempPenawaran tp 
LEFT JOIN GODigital.OrderSimulation os 
ON os.TempPenawaranGuid = tp.Guid
LEFT JOIN AABMobile.dbo.QuotationLead ql
ON tp.Guid = ql.ReferenceNo
LEFT JOIN GODigital.OrderVABooking ov
ON ov.VirtualAccountNo = os.VANumber
WHERE CAST(tp.modifiedDate AS DATE) >= CAST(@@CutOffDate AS DATE) AND 
tp.IsQuotationF = 1 AND (os.OrderStatus = '01' OR os.OrderStatus IS NULL) 
AND tp.IsRenewalF = 0 AND tp.RowStatus = 0 AND tp.IsNeedFU = 1
AND Guid NOT LIKE 'ORDEREDIT%' AND ql.SalesOfficerID IS NULL
--AND CAST(tp.modifiedDate AS TIME) BETWEEN #waktu
UNION 
SELECT tp.Guid ReferenceNo, 2 OrderSource, tp.customerName CustName, ov.PolicyNo PolicyNo, 
tp.VehicleChasisNumber ChasisNumber, tp.VehicleEngineNumber EngineNumber, tp.customerPhone CustPhone
FROM dbo.TempPenawaran tp 
LEFT JOIN GODigital.OrderSimulation os 
ON os.TempPenawaranGuid = tp.Guid
LEFT JOIN AABMobile.dbo.QuotationLead ql
ON tp.Guid = ql.ReferenceNo
LEFT JOIN GODigital.OrderVABooking ov
ON ov.VirtualAccountNo = os.VANumber
WHERE CAST(tp.modifiedDate AS DATE) >= CAST(@@CutOffDate AS DATE) AND 
(os.OrderStatus = '01' OR os.OrderStatus IS NULL) AND
(os.CreatedBy = 'CHBOT' OR os.CreatedBy IS NULL) AND
tp.IsQuotationF = 0 AND tp.IsRenewalF = 0 AND tp.RowStatus = 0 AND IsLinkPaymentF = 0
AND Guid NOT LIKE 'ORDEREDIT%' AND ql.SalesOfficerID IS NULL
--AND CAST(tp.modifiedDate AS TIME) BETWEEN #waktu
UNION
SELECT ufq.FileQuotationID ReferenceNo, 3 OrderSource, ufq.CustName CustName, '' PolicyNo, 
'' ChasisNumber, '' EngineNumber, ufq.CustPhone CustPhone
FROM AABMobile.dbo.UploadFileQuotation ufq
INNER JOIN AABMobile.dbo.UploadFileQuotationHeader ufqh
ON ufqh.FileQuotationHeaderID = ufq.FileQuotationHeaderID
LEFT JOIN AABMobile.dbo.QuotationLead ql
ON ufq.FileQuotationID = ql.ReferenceNo
WHERE ql.SalesOfficerID IS NULL AND ufq.RowStatus = 1 AND ufqh.UploadStatus = 2
--AND CAST(ufq.CreatedDate AS TIME) BETWEEN #waktu
) a ORDER BY CustPhone

SELECT *,(SELECT COUNT(CustPhone) FROM #tempQotationLead b WHERE b.CustPhone = a.CustPhone) cnt
INTO #tempQotationLead2
FROM #tempQotationLead a 

SELECT * FROM #tempQotationLead2 ORDER BY cnt DESC, CustPhone

DROP TABLE #tempQotationLead
DROP TABLE #tempQotationLead2";
                //qGetQuotationLead = qGetQuotationLead.Replace("#waktu", qwaktu);
                List<dynamic> ListQuotationLead = AsuransiAstraDB.Fetch<dynamic>(qGetQuotationLead);
                List<dynamic> ListOnlineUser = AABMobileDB.Fetch<dynamic>(@"SELECT SalesOfficerID, SkillLevel FROM dbo.MstTelesalesUser WHERE IsOnline = 1 ORDER BY SkillLevel DESC");
                if (ListOnlineUser.Count > 0) {
                    int TotalSkillLevel = AABMobileDB.ExecuteScalar<int>(@";SELECT SUM(SkillLevel) FROM dbo.MstTelesalesUser WHERE IsOnline = 1");
                    Dictionary<string, int> DicUsr = new Dictionary<string, int>();
                    foreach (dynamic usr in ListOnlineUser)
                    {
                        decimal dTotalOrd = ((decimal) usr.SkillLevel / TotalSkillLevel) * ListQuotationLead.Count;
                        int iTotalOrd = Convert.ToInt32(Math.Round(dTotalOrd, MidpointRounding.AwayFromZero));
                        DicUsr.Add(usr.SalesOfficerID, iTotalOrd);
                    }
                    string qInsert = @";IF EXISTS(SELECT * FROM dbo.QuotationLead WHERE ReferenceNo = @0)
BEGIN
	UPDATE dbo.QuotationLead SET SalesOfficerID = @7, 
	CustName = @2, CustPhone = @3, PolicyNo = @4,
	ChasisNumber = @5, EngineNumber = @6,
	ModifiedBy = 'OTOSL', ModifiedDate = GETDATE(),
	RowStatus = 1 WHERE ReferenceNo = @0
	IF EXISTS(SELECT * FROM dbo.FollowUp WHERE ReferenceNo = @0)
	BEGIN
		DECLARE @@BranchCode VARCHAR(100);
		DECLARE @@OrderNo VARCHAR(100);
		DECLARE @@CustID VARCHAR(100);
		SELECT @@BranchCode=BranchCode FROM dbo.SalesOfficer WHERE SalesOfficerID = @7
		IF EXISTS(SELECT * FROM dbo.FollowUp f
		INNER JOIN dbo.OrderSimulation os ON os.FollowUpNo = f.FollowUpNo
		WHERE ReferenceNo = @0 AND os.RowStatus = 1 AND os.ApplyF = 1)
		BEGIN
			SELECT @@OrderNo = os.OrderNo, @@CustID = os.CustID FROM dbo.FollowUp f
			INNER JOIN dbo.OrderSimulation os ON os.FollowUpNo = f.FollowUpNo
			WHERE ReferenceNo = @0 AND os.RowStatus = 1 AND os.ApplyF = 1
			UPDATE dbo.FollowUp SET RowStatus = 1, 
			LastUpdatedTime = GETDATE(), SalesOfficerID = @7, BranchCode = @@BranchCode
			WHERE ReferenceNo = @0
			UPDATE dbo.OrderSimulation SET SalesOfficerID = @7, BranchCode = @@BranchCode,
			LastUpdatedTime = GETDATE() WHERE OrderNo = @@OrderNo
			UPDATE dbo.ProspectCustomer SET SalesOfficerID = @7, BranchCode = @@BranchCode,
			LastUpdatedTime = GETDATE() WHERE CustID = @@CustID
		END
	END 
END
ELSE
BEGIN
	INSERT INTO dbo.QuotationLead
			( ReferenceNo ,
				OrderSource ,
				CustName ,
				CustPhone,
				PolicyNo ,
				ChasisNumber ,
				EngineNumber ,
				SalesOfficerID,
				RowStatus ,
				CreatedBy ,
				CreatedDate
			)
	VALUES  ( @0 , -- ReferenceNo - varchar(100)
				@1 , -- OrderSource - int
				@2 , -- CustName - varchar(100)
				@3 , -- CustPhone - varchar(30)
				@4 , -- PolicyNo - varchar(16)
				@5 , -- ChasisNumber - varchar(30)
				@6 , -- EngineNumber - varchar(30)
				@7 , -- SalesOfficerID - varchar(20)
				1 , -- RowStatus - bit
				'OTOSL' , -- CreatedBy - varchar(50)
				GETDATE()  -- CreatedDate - datetime
			)
END";
                    int IdxDic = 0;
                    int IdxOrd = 0;
                    foreach (var ql in ListQuotationLead)
                    {
                        if (IdxDic < DicUsr.Count) {
                            string SalesOfficerID = DicUsr.ElementAt(IdxDic).Key;
                            int OrdCount = DicUsr.ElementAt(IdxDic).Value;
                            if (IdxOrd < OrdCount)
                            {
                                AABMobileDB.Execute(qInsert
                                    , ql.ReferenceNo
                                    , ql.OrderSource
                                    , ql.CustName
                                    , ql.CustPhone
                                    , ql.PolicyNo
                                    , ql.ChasisNumber
                                    , ql.EngineNumber
                                    , SalesOfficerID);
                                IdxOrd++;
                            }
                            else
                            {
                                IdxOrd = 0;
                                IdxDic++;
                                SalesOfficerID = DicUsr.ElementAt(IdxDic).Key;
                                OrdCount = DicUsr.ElementAt(IdxDic).Value;
                                AABMobileDB.Execute(qInsert
                                    , ql.ReferenceNo
                                    , ql.OrderSource
                                    , ql.CustName
                                    , ql.CustPhone
                                    , ql.PolicyNo
                                    , ql.ChasisNumber
                                    , ql.EngineNumber
                                    , SalesOfficerID);
                                IdxOrd++;
                            }
                        }
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.Message));
                return false;
            }
        }

        public bool RetractQuotationLead() {
            string CurrentMethod = GetCurrentMethod();
            var AABMobileDB = GetAABMobileDB();
            try
            {
                string qGetQuotationLead = @";SELECT ql.ReferenceNo, f.FollowUpStatus, f.FollowUpInfo FROM dbo.QuotationLead ql
LEFT JOIN dbo.FollowUp f
ON f.ReferenceNo = ql.ReferenceNo
WHERE (f.FollowUpInfo = 1 AND f.FollowUpStatus = 1) 
OR (f.FollowUpInfo IS NULL AND f.FollowUpStatus IS NULL) 
AND ql.SalesOfficerID IS NOT NULL AND f.RowStatus = 1 OR f.RowStatus IS NULL";
                List<dynamic> ListQuotationLead = AABMobileDB.Fetch<dynamic>(qGetQuotationLead);
                foreach (dynamic ql in ListQuotationLead)
                {
                    if (ql.FollowUpStatus == 1 && ql.FollowUpInfo == 1) {
                        AABMobileDB.Execute(@";UPDATE dbo.FollowUp SET RowStatus = 0, LastUpdatedTime = GETDATE()  WHERE ReferenceNo = @0", ql.ReferenceNo);
                    }
                    AABMobileDB.Execute(@";UPDATE dbo.QuotationLead SET SalesOfficerID = NULL, RowStatus = 0, ModifiedBy = 'OTOSL', 
                                        ModifiedDate = GETDATE() WHERE ReferenceNo = @0", ql.ReferenceNo);
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.Message));
                return false;
            }
        }
    }
}
