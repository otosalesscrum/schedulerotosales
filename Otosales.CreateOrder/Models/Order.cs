﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otosales.Models
{
    #region Products

    
    #endregion

    public class Order
    {
        public OrderData OrderData { get; set; }
        public PersonalCustomer PersonalProspect { get; set; }
        public List<ObjectOrderData> ObjectData { get; set; }
        public List<ObjectOrderDetailData> ObjectDetailData { get; set; }
        public List<InterestData> InterestData { get; set; }
        public List<CoverageData> CoverageData { get; set; }
        public List<InsuredData> InsuredData { get; set; }
        public List<DocumentData> DocumentData { get; set; }
        public List<InstallmentInfo> InstallmentInfo { get; set; }
        public List<ScoringData> ScoringData { get; set; }
        public List<CoverCommision> CoverCommision { get; set; }
        public List<InterestItemData> InterestItemData { get; set; }
        public List<PolicyClause> PolicyClause { get; set; }
        public List<ObjectClause> ObjectClause { get; set; }
        public List<AddressDetail> AddressDetail { get; set; }
        public List<TransactionAccount> TransactionAccount { get; set; }
        public List<SurveyInfo> SurveyInfo { get; set; }
        public IndirectInsurance IndirectInsurance { get; set; }
        public List<IndirectMember> IndirectMemberList { get; set; }
        public LinkOrderAgency LinkOrderAgency { get; set; }
        public AgencyInfo AgencyInfo { get; set; }
        public SalesmanDealerInfo SalesmanDealerInfo { get; set; }
        public List<OriginalDefectOrdData> OriginalDefectList { get; set; }
        public List<NoCoverOrdData> NoCoverList { get; set; }
        public List<SurveyImage> SurveyImageList { get; set; }
        public List<NonStandardAccessoriesOrdData> NonStandardAccessoriesList { get; set; }
        public PendingOrderInfo PendingOrderInfo { get; set; }
        public List<BookingSurvey> BookingSurvey { get; set; }
        public List<SurveyOrder> SurveyOrder { get; set; }
    }

    public class SalesmanDealerInfo
    {

        public string CustomerCode { get; set; }

        public string CustomerName { get; set; }

        public string DealerCode { get; set; }

        public string DealerName { get; set; }

        public string SupervisorCode { get; set; }

        public string SupervisorName { get; set; }

        public string BranchHeadCode { get; set; }

        public string BranchHeadName { get; set; }
        public string CustomerID { get; set; }
        public string DealerID { get; set; }
        public string BrachHeadID { get; set; }
        public string SupervisorIDID { get; set; }
    }

    public class AgencyInfo
    {
        public string AgentType { get; set; }
        public string AgentID { get; set; }
        public string UplinerID { get; set; }
        public string LeaderID { get; set; }
    }

    public class OrderData
    {
        public string OrderNo { get; set; }
        public int TitanOrderNo { get; set; }
        public int EndorsementNo = 0;
        public int OrgEndorsementNo = 0;
        public string OrderType { get; set; }
        public string PolicyId { get; set; }
        public string PolicyNo { get; set; }
        public string RefNo { get; set; }
        public DateTime OrderDate { get; set; }
        public DateTime TransactionDate { get; set; }
        public DateTime MarketingDate { get; set; }
        public DateTime EffectiveDate { get; set; }
        public string EndorsementType { get; set; }
        public string EndorsementCode { get; set; }
        public string OldPolicyNo { get; set; }
        public string BranchId { get; set; }
        public string COBId { get; set; }
        public string MOUID { get; set; }
        public string ProductCode { get; set; }
        public string DeptId { get; set; }
        public string SalesmanId { get; set; }
        public string SegmentCode { get; set; }
        public string AcceptanceId { get; set; }
        public string ProspectId { get; set; }
        public string CustId { get; set; }
        public string FinInstitutionCode { get; set; }
        public string BrokerCode { get; set; }
        public string PolicyHolderCode { get; set; }
        public string PayerCode { get; set; }
        public string NameOnPolicy { get; set; }
        public string NameOnCard { get; set; }
        public string FormCode { get; set; }
        public DateTime? PeriodFrom { get; set; }
        public DateTime? PeriodTo { get; set; }
        public bool AutoRenewal = false;
        public int GracePeriod { get; set; }
        public int RL1 = 0;
        public int RL2 = 0;
        public bool PrintPolicy = false;
        public bool PrintCoverNote = false;
        public bool MOApproval = false;
        public string MOApprovalId { get; set; }
        public string InwardId { get; set; }
        public string OutWardId { get; set; }
        public string SurveyNo { get; set; }
        public int? CalcMethod { get; set; }
        public decimal PremiumEntryPct = 0;
        public decimal SharePct { get; set; }
        public decimal OurSharePct { get; set; }
        public bool RenewalStatus = false;
        public string Status { get; set; }
        public string OrderStatus { get; set; }
        public string Analysis { get; set; }
        public string TermCode { get; set; }
        public string UserId { get; set; }
        public string FeeCurrency { get; set; }
        public string FeePayment { get; set; }
        public int CommissionPayment = 0;
        public decimal PolicyFee { get; set; }
        public decimal StampDuty { get; set; }
        public string Notes { get; set; }
        public string RejectRemarks { get; set; }
        public string SchemeCode { get; set; }
        public int NonStandard = 0;
        public string PeriodApproval { get; set; }
        public string EntryUsr { get; set; }
        public string SalesAIID { get; set; }
        public string MstPolicyNo { get; set; }
        public string VANumber { get; set; }
        public string PreOrderedVANumber { get; set; }
        //public string BankCode { get; set; }
        //public decimal PaidAmount { get; set; }
        //public string PaymentMethodCode { get; set; }
        //public string PaymentCategory { get; set; }
        //public string AABAccountCode { get; set; }
    }

    public class ObjectOrderData
    {
        public string OrderNo { get; set; }
        public int ObjectNo { get; set; }
        public string RIStatus { get; set; }
        //public string Notes {get; set;}
        public string Status { get; set; }
        public DateTime? PeriodFrom { get; set; }
        //public DateTime CancelDate { get; set; }
        public DateTime? PeriodTo { get; set; }
        public string Description { get; set; }
        public decimal BCOwnRetention = 0;
        public decimal BCFacultativeIn = 0;
        public decimal BCSumInsured = 0;
        public decimal BCCoinsuranceIn = 0;
        public decimal BCOriginalSI { get; set; }
        public decimal BCTotalFacultative = 0;
        public decimal BCCoinsuranceOut = 0;
        public decimal BCTotalTreaty = 0;
        public int StandardReferenceId = 0;
        public string EntryUsr { get; set; }

        public int SurveyStatus = 0;
    }

    public class ObjectOrderDetailData
    {
        public string OrderNo { get; set; } // char(15)
        public int ObjectNo { get; set; } // numeric
        public string VehicleCode { get; set; } // char(6)
        public string VehicleType { get; set; } // char(6)
        public string BrandCode { get; set; } // char(6)
        public string ModelCode { get; set; } // char(6)
        public string Series { get; set; } // char(50)
        public int TransmissionType { get; set; } // numeric
        public int MfgYear { get; set; } // numeric
        public bool NewCar { get; set; } // bit
        public int Kilometers { get; set; } // numeric
        public string Color { get; set; } // char(6)
        public bool LightColor { get; set; } // bit
        public bool Metalic { get; set; } // bit
        public string ColorOnBPKB { get; set; } // char(50)
        public string ContractNo { get; set; } // char(50)
        public string ClientNo { get; set; } // char(50)
        public int SittingCapacity { get; set; } // numeric
        public string UsageCode { get; set; } // char(6)
        public string GeoAreaCode { get; set; } // char(6)
        public string ChasisNumber { get; set; } // char(30)
        public string EngineNumber { get; set; } // char(30)
        public string RegistrationNumber { get; set; } //{get;set;} // varchar(16)
        public decimal MarketPrice { get; set; } // numeric
        public string CurrId { get; set; } // char(3)
        public string EntryUsr { get; set; } // char(5)        
        //public int IsNeedSurvey { get; set; } // smallint
        //public int IsLimitedAcceptation { get; set; } // smallint
        //public int IsCBU { get; set; } // smallint
    }

    public class PersonalCustomer
    {
        public string IDNumber { get; set; }
        public string IDType { get; set; }
        public string CustomerType { get; set; }
        public string CustomerName { get; set; }
        public string GODIGCustID { get; set; }
        public DateTime BirthDate { get; set; }
        public string Gender { get; set; }
        public string Religion { get; set; }
        public string HomeAddress { get; set; }
        public string HomePostalCode { get; set; }
        public string OfficeAddress { get; set; }
        public string OfficePostalCode { get; set; }
        public string MaritalStatus { get; set; }
        public string JobTitle { get; set; }
        public string CompanyName { get; set; }
        public string Income { get; set; }
        public string Email { get; set; }
        public string HomePhoneNumber1 { get; set; }
        public string HomePhoneNumber2 { get; set; }
        public string HomeExt1 { get; set; }
        public string HomeExt2 { get; set; }
        public string OfficePhoneNumber1 { get; set; }
        public string OfficePhoneNumber2 { get; set; }
        public string OfficeExt1 { get; set; }
        public string OfficeExt2 { get; set; }
    }

    public class InterestData
    {
        public string Orderno { get; set; }
        public int ObjectNo { get; set; }
        public int InterestNo { get; set; }
        public string AdjustableClause { get; set; }
        public string Description { get; set; }
        public string InsuranceType { get; set; }
        public decimal DedFlatAmount { get; set; }
        public DateTime PeriodFrom { get; set; }
        public string Status { get; set; }
        public DateTime PeriodTo { get; set; }
        public string InterestType { get; set; }
        public string InterestCode { get; set; }
        public decimal OriginalSI { get; set; }
        public decimal CoinsuranceIn { get; set; }
        public decimal ClaimableAmount { get; set; }
        public decimal SumInsured { get; set; }
        public decimal FacultativeIn { get; set; }
        public decimal ExchangeRate { get; set; }
        public decimal StockMaximumPossible { get; set; }
        public string CurrId { get; set; }
        public string DedFlatCurrency { get; set; }
        public string DeductibleCode { get; set; }
        public int ParNo { get; set; }
        public int StandardReferenceId { get; set; }
        public string EntryUsr { get; set; }
        public List<CoverageData> CoverageList { get; set; }
        public bool Protected { get; set; }
    }

    public class InterestItemData
    {
        public string PolicyId { get; set; } 
        public string OrderNo { get; set; } //char(15)
        public decimal? ObjectNo { get; set; } //numeric
        public decimal? InterestNo { get; set; } //numeric
        public decimal? IntItemNo { get; set; } //numeric
        public string InsuranceType { get; set; } //char(6)
        public string IntItemCode { get; set; } //char(4)
        public string ItemName { get; set; } //varchar(50)
        public string Description { get; set; } //varchar(50)
        public decimal? Capacity { get; set; } //numeric
        public decimal OriginalSI { get; set; } //numeric
        public decimal SumInsured { get; set; } //numeric
        public decimal DedAmount { get; set; } //numeric
        public decimal? MFGYear { get; set; } //numeric
        public DateTime? Birthdate { get; set; } //datetime
        public decimal? ColumnA { get; set; } //numeric
        public decimal? ColumnB { get; set; } //numeric
        public decimal? ColumnC { get; set; } //numeric
        public decimal? ColumnD { get; set; } //numeric
        public decimal? ColumnE { get; set; } //numeric
        public DateTime? Date1 { get; set; } //datetime
        public DateTime? Date2 { get; set; } //datetime
        public bool Flag1 { get; set; } //bit
        public bool Flag2 { get; set; } //bit
        public string EntryUsr { get; set; } //char(5)
        public decimal? EndorsementNo { get; set; } //numeric
        public decimal? OrgEndorsementNo { get; set; } //numeric
        public decimal? MfgYear { get; set; } //numeric
    }

    public class CoverageData
    {
        public string OrderNo { get; set; } // char(15)
        public int ObjectNo { get; set; } // numeric
        public int InterestNo { get; set; } // numeric
        public string InterestID { get; set; }
        public int CoverageNo { get; set; } // numeric
        public string InsuranceType { get; set; } // char(6)
        public string COBId { get; set; } // char(3)
        public string CurrId { get; set; } // char(3)
        public string Status { get; set; } // char(1)
        public string InterestType { get; set; } // char(1)
        public string CoverageId { get; set; } // char(6)
        public decimal NetPremium { get; set; } // numeric
        public DateTime BeginDate { get; set; } // datetime
        public DateTime EndDate { get; set; } // datetime

        public decimal Ndays { get; set; } // numeric
        public decimal Net1 { get; set; } // numeric
        public decimal Net2 { get; set; } // numeric
        public decimal Net3 { get; set; } // numeric
        public decimal GrossPremium { get; set; } // numeric
        public decimal CoverPremium { get; set; } // numeric
        public decimal SumInsured { get; set; } // numeric
        public decimal Loading { get; set; } // numeric
        public int? CalcMethod { get; set; } //{get;set;} // varchar(2)
        public decimal DedFlatAmount { get; set; } // numeric
        public string DedFlatCurrency { get; set; } // char(3)
        public string DeductibleCode { get; set; } // char(6)
        public decimal EntryPct { get; set; } // numeric
        public decimal Rate { get; set; } // numeric
        public decimal Discount { get; set; } // numeric
        public decimal Maxsi { get; set; } // numeric
        public decimal ExcessRate { get; set; } // numeric
        public decimal OriginalRate { get; set; } // numeric
        public int StandardReferenceId { get; set; } // int
        public string EntryUsr { get; set; } // char(5)                   
        public decimal AcquisitionCost { get; set; } // numeric
        public string CoverType { get; set; }
    }

    public class InsuredData
    {

        public string OrderNo { get; set; } // char(15)
        public int ObjectNo { get; set; } // numeric
        public int InterestNo { get; set; } // numeric
        public int PeriodNo { get; set; } // numeric
        public DateTime BeginDate { get; set; } // datetime
        public DateTime EndDate { get; set; } // datetime
        public decimal SumInsured { get; set; } // numeric
        public string Status { get; set; } // char(1)
        public string EntryUsr { get; set; } // char(5)

    }

    public class ScoringData
    {
        public string OrderNo { get; set; } // char(15)
        public int ObjectNo { get; set; } // numeric
        public string FactorCode { get; set; } // char(6)
        public string InsuranceCode { get; set; } // char(6)
        public string EntryUsr { get; set; } //char(5)		          
    }

    public class CoverCommision
    {
        public string OrderNo { get; set; } // char(15)
        public int ObjectNo { get; set; } // numeric
        public int InterestNo { get; set; } // numeric
        public int DiscCommNo { get; set; } // numeric
        public string CoverageId { get; set; } // char(6)
        public string PayToCode { get; set; } // char(11)
        public string ApplyCode { get; set; } // char(1)
        public int CommissionType { get; set; } // numeric
        public string CommissionCode { get; set; } // char(6)
        public int LevelDisc { get; set; } // numeric
        public int CoverageNo { get; set; } // numeric
        public decimal Percentage { get; set; } // numeric
        public string CurrId { get; set; } // char(3)
        public decimal Amount { get; set; } // numeric
        public string Status { get; set; } // char(1)
        public string EntryUsr { get; set; } // char(5)          

    }


    public class OrderResult
    {
        public string OrderNo { get; set; }
        public string ErrorInfo { get; set; }
    }

    public class SaveOrderResult
    {
        public bool IsSuccess { get; set; }
        public string ErrorInfo { get; set; }
    }

    public class Transaction
    {
        public string TransactionNo { get; set; }
        public string ReferenceId { get; set; }
        public string ReferenceType { get; set; }
        public string EntryUser { get; set; }
    }

    public class TransactionDetail
    {
        public string TransactionNo { get; set; }
        public string ReferenceNo { get; set; }
        public string TransactionType { get; set; }
        public string EntryUser { get; set; }
    }

    public class DocumentData
    {
        public string OrderNo { get; set; } // char(15)
        public string DocumentNo { get; set; } // numeric
        public string DocumentId { get; set; } // char(6)
        public string Description { get; set; } //{get;set;} // varchar(50)
        public string Origin { get; set; } // bit
        public string PLY { get; set; } // numeric
        public string ReceivedDate { get; set; } // datetime
        public string ExpiredDate { get; set; } // datetime
        public string EntryUsr { get; set; } // char(5)
    }

    public class InstallmentInfo
    {
        public string OrderNo { get; set; } // char(15)
        public int InstallNo { get; set; } // numeric
        public string CardNo { get; set; } //{get;set;} // varchar(20)
        public string CardName { get; set; } //{get;set;} // varchar(30)
        public DateTime ValidThru { get; set; } // datetime
        public string CVC { get; set; } // char(4)
        public string BankName { get; set; } //{get;set;} // varchar(50)
        public string FeeCurrency { get; set; } // char(3)
        public DateTime DueDate { get; set; } // datetime
        public decimal InstallPct { get; set; } // numeric
        public decimal InterestPct { get; set; } // numeric
        public decimal CommPct { get; set; } // numeric
        public decimal StampDuty { get; set; } // numeric
        public decimal PolicyFee { get; set; } // numeric
        public string PaymentMode { get; set; } // char(6)
        public string EntryUsr { get; set; } // char(5)
    }

    public class AddressDetail
    {
        public string OrderNo { get; set; }  // char(15)
        public string AddressType { get; set; }  // char(6)
        public string SourceType { get; set; }  // char(2)
        public int RenewalNotice = 0;  // bit
        public string DeliveryCode { get; set; }  // char(6)
        public string Address { get; set; }  //{get;set;} // varchar(152)
        public string RT { get; set; }  // char(3)
        public string RW { get; set; }  // char(3)
        public string PostalCode { get; set; }  // char(8)
        public string Phone { get; set; }  //{get;set;} // varchar(16)
        public string Fax { get; set; }  //{get;set;} // varchar(16)
        public string EntryUsr { get; set; }  // char(5)          
        public string Handphone { get; set; }  //{get;set;} // varchar(20)
        public string ContactPerson { get; set; }  //{get;set;} // varchar(30)
    }

    public class PolicyClause
    {
        public string OrderNo { get; set; }
        public string ClauseId { get; set; }
        public string Description { get; set; }
        public string EntryUsr { get; set; }
    }

    public class ObjectClause
    {
        public string OrderNo { get; set; }
        public decimal ObjectNo { get; set; }
        public decimal ClauseNo { get; set; }
        public string ClauseId { get; set; }
        public string Description { get; set; }
        public string EntryUsr { get; set; }
    }

    public class WTInwardReinsurance
    {
        public string OrderNo { get; set; } //char(15)
        public string AcceptanceId { get; set; } //char(6)
        public int FullReceived { get; set; } //bit
        public string CedingCoy { get; set; } //char(11)
        public string RefPolicyNo { get; set; } //varchar(30)
        public DateTime OfferingLetterDate { get; set; } //datetime
        public string OfferingLetterNo { get; set; } //varchar(30)
        public decimal OurShareAmt { get; set; } //numeric
        public decimal OurSharePct { get; set; } //numeric
        public decimal HandlingFeePct { get; set; } //numeric
        public DateTime RISlipDate { get; set; } //datetime
        public DateTime SlipReceiveDate { get; set; } //datetime
        public DateTime WPCDate { get; set; } //datetime
        public DateTime PeriodFrom { get; set; } //datetime
        public DateTime PeriodTo { get; set; } //datetime
        public decimal OriginalRate { get; set; } //numeric
        public string CalcMethod { get; set; } //char(1)
        public string EntryUsr { get; set; } //char(5)          
    }

    public class WTInwardMember
    {
        public string OrderNo { get; set; } //char(15)
        public string MemberCode { get; set; } //char(11)
        public string MemberType { get; set; } //char(1)
        public decimal MemberSharePct { get; set; } //numeric
        public string EntryUsr { get; set; } //char(5)         
    }

    public class OutwardReinsurance
    {
        public string OutwardId { get; set; } //char(15)
        public DateTime EffectiveDate { get; set; } //datetime
        public string OrderBlockId { get; set; } //varchar(15)
        public string BlockId { get; set; } //char(10)
        public decimal BlockEndorsementNo { get; set; } //numeric
        public DateTime Responsedate { get; set; } //datetime
        public string Reinsurancecode { get; set; } //char(11)
        public string CedingCode { get; set; } //char(11)
        public DateTime OfferingLetterdate { get; set; } //datetime
        public string OfferingLetterNo { get; set; } //char(30)
        public decimal HandlingFeePct { get; set; } //numeric
        public decimal HandlingfeeAmt { get; set; } //numeric
        public decimal Acceptancepct { get; set; } //numeric
        public decimal AcceptanceAmt { get; set; } //numeric
        public DateTime Acceptancedate { get; set; } //datetime
        public string AcceptanceNo { get; set; } //char(30)
        public string Acceptanceby { get; set; } //char(30)
        public DateTime RISlipDate { get; set; } //datetime
        public DateTime WPCDate { get; set; } //datetime
        public int FullReceived { get; set; } //bit
        public DateTime PeriodFrom { get; set; } //datetime
        public decimal BCTotalSI { get; set; } //numeric
        public decimal BCYourShare { get; set; } //numeric
        public decimal BCYourSharePct { get; set; } //numeric
        public decimal BCYourSharePremium { get; set; } //numeric
        public DateTime RIPeriodFrom { get; set; } //datetime
        public DateTime RIPeriodTo { get; set; } //datetime
        public string RIPremiumCalc { get; set; } //char(1)
        public decimal RIEntrypct { get; set; } //numeric
        public DateTime MaxRIPeriodTo { get; set; } //datetime
        public string Status { get; set; } //char(1)
        public string ApprovalStatus { get; set; } //char(1)
        public string Remaks { get; set; } //varchar(255)
        public string ReferenceNo { get; set; } //char(30)
        public string EntryUsr { get; set; } //char(5)          
    }

    public class TransactionAccount
    {
        public string ReferenceNo { get; set; } //varchar(30)
        public string ReferenceType { get; set; } //char(3)
        public string AccountNo { get; set; } //varchar(30)
        public string AccountType { get; set; } //char(3)
        public string AccountName { get; set; } //varchar(30)
        public string ValidThru { get; set; } //char(10)
        public string CVC { get; set; } //char(4)
        public string BankName { get; set; } //varchar(50)
        public string EntryUsr { get; set; } //char(5)
    }

    public class SurveyInfo
    {
        public string SurveyNo { get; set; } // char(8)
        public int ObjectNo { get; set; } // numeric
        public bool ExternalSurveyor { get; set; } // bit
        public string EXTSURVEYORID { get; set; } // char(11)
        public string SurveyType { get; set; } // char(6)
        public string Surveyor { get; set; } // char(5)
        public Nullable<DateTime> SurveyDate { get; set; } // datetime
        public Nullable<DateTime> FromDate { get; set; } // datetime
        public Nullable<DateTime> ToDate { get; set; } // datetime
        public string PicName { get; set; } //{get;set;} // varchar(100)
        public string PaymentMode { get; set; } // char(6)
        public string CurrId { get; set; } // char(3)
        public bool Amount { get; set; } // numeric
        public string PhoneNo { get; set; } //{get;set;} // varchar(16)
        public string Location { get; set; } //{get;set;} // varchar(152)
        public string RT { get; set; } // char(3)
        public string RW { get; set; } // char(3)
        public string PostalCode { get; set; } // char(8)
        public string Description { get; set; } //{get;set;} // varchar(50)
        public string FormId { get; set; } // char(6)
        public int Status = 1; // char(1)
        public string Notes { get; set; } // text
        public string ReferenceNo { get; set; } // char(30)
        public string AreaCode { get; set; } // char(6)
        public string OldAreaCode { get; set; } // char(6)
        public string SurveyMode { get; set; } // char(1)
        public string RegionCode { get; set; } // char(6)
        public string BestTimeCall { get; set; } //{get;set;} // varchar(128)
        public string RejectReason { get; set; } //{get;set;} // varchar(128)
        public string Result { get; set; } // text
        public string PolicyNo { get; set; } // char(16)
        public string RegistrationNo { get; set; } // char(16)
        public int Duration = 0; // smallint
        public int TravelPeriod = 0; // smallint
        public string OnCall { get; set; } // char(1)
        public string SurveyReference { get; set; } // char(20)
        public string SubAreaCode { get; set; } // char(6)
        public string AdjusterCode { get; set; } // char(11)
        public string SubConCode { get; set; } // char(11)
        public string PayerCode { get; set; } // char(11)
        public string PayerFlag { get; set; } // char(1)
        public string SurveyResult { get; set; } // char(1)
        public int ImageCount = 0;  // int
        public string EntryUsr { get; set; } // char(5)         
        public string newsurveytype { get; set; } //{get;set;} // varchar(6)
        public string newsubsurveytype { get; set; } //{get;set;} // varchar(6)
        public string remarks { get; set; } //{get;set;} // varchar(256)
        public string nonschedule { get; set; } //{get;set;} // varchar(1)
        public string SubGeoAreaCode { get; set; } //{get;set;} // varchar(10)
        public string AddressFromSurvey { get; set; } //{get;set;} // varchar(10)
        public string City { get; set; } //{get;set;} // varchar(50)
        public int SurveyObject = 0; // int
        public DateTime ActualSurveyDate { get; set; } // datetime
        public string StickerDescription { get; set; } //{get;set;} // varchar(500)
        public string ResultRecommendation { get; set; } //{get;set;} // varchar(max)
        /*Sprint 13 Otosurvey Additional Acquisition Order ID*/
        public string AdditionalOrderAcquisitionSurveyID { get; set; }
    }

    public class VATRansaction
    {
        public string PolicyNo { get; set; }   // varchar(16),
        public string VirtualAccountNo { get; set; } // varchar(30),
        public string CurrencyCode { get; set; } // varchar(3),
        public decimal PremiumAmount { get; set; }// NUMERIC(20,4),
        public string PolicyStatusCode { get; set; } // varchar(3),
        public string InsuredCode { get; set; } // varchar(11),
        public string InsuredName { get; set; } // varchar(200),
        public string MarketingCode { get; set; } // varchar(5),
        public string SalesmanAdminLogID { get; set; } // varchar(5),
        public string InternalDistributionCode { get; set; } // varchar(6),
        public DateTime PolicyPeriodFrom { get; set; }//DATETIME,
        public DateTime PolicyPeriodTo { get; set; }//DATETIME,
        public int GracePeriod { get; set; }//int,
        public DateTime PaymentDueDate { get; set; }//datetime,
        public string PolicyNoRenewalBeyond { get; set; } // varchar(20),	
        public string OrderNo { get; set; } // varchar(50),
        public string ChasisNo { get; set; } // varchar(50),
        public string EngineNo { get; set; } // varchar(50),
        public string PolicyStatus { get; set; } // varchar(50),
        public string PoliceNo { get; set; } // varchar(50),
        public string CustomerName { get; set; } // varchar(200),	    
        public string ConnectionString { get; set; }//	VARCHAR(200),
    }

    public class GenerateROVoucherResult
    {
        public string VoucherNo { get; set; }
        public string ErrorMessage { get; set; }
    }

    public class Product
    {
        public Product()
        {
            PayToPartyList = new List<string>();
        }
        public string COBName { get; set; }
        public string COBDescription { get; set; }
        public string MOUNumber { get; set; }
        public string MaximumCustomerObject { get; set; }
        public string UsedLiability { get; set; }
        public string MaximumLiablity { get; set; }
        public string CheckLimit { get; set; }
        public string DefaultCurrencyId { get; set; }
        public string BaseCurrencyPolicyFee { get; set; }
        public string BaseCurrencyMinimumPremium { get; set; }
        public string MOApprove { get; set; }
        public string ProfitSharingCalculation { get; set; }
        public string RenewalDiscountCalculation { get; set; }
        public string IssueCardTo { get; set; }
        public string CardType { get; set; }
        public string PolicyTo { get; set; }
        public string PrintCoverNote { get; set; }
        public string PrintNote { get; set; }
        public string AllBranch { get; set; }
        public string SchemeID { get; set; }
        public string DepatmentID { get; set; }
        public string SalesmanID { get; set; }
        public int BackDateAllowance { get; set; }
        public string CustomerID { get; set; }
        public string CustomerGroup { get; set; }
        public string CustomerPos { get; set; }
        public string FinancialInstitutionID { get; set; }
        public string FinancialInstitutionGroup { get; set; }
        public string FinancialInstitutionParty { get; set; }
        public string FinancialInstitutionPos { get; set; }
        public string BrokerID { get; set; }
        public string BrokerGroup { get; set; }
        public string BrokerParty { get; set; }
        public string BrokerPos { get; set; }
        public string PolicyHolderParty { get; set; }
        public string PolicyHolderPos { get; set; }
        public string PayerParty { get; set; }
        public string PayerPos { get; set; }
        public string StockMaximumPossible { get; set; }
        public string StockRefundLimit { get; set; }
        public string StockAdjustibleClause1 { get; set; }
        public string StockAdjustibleClause2 { get; set; }
        public int CustomerPremiumCalculationFlags { get; set; }
        public int InternalPremiumCalculationFlags { get; set; }
        public int OtherPremiumCalculationFlags { get; set; }
        public int PrintedPlyFlags { get; set; }
        public int NewPolicyPrintFlags { get; set; }
        public int RenewPolicyPrintFlags { get; set; }
        public int EndorsementPrintFlags { get; set; }
        public int CancellationPrintFlags { get; set; }
        public string DayCalculationMethod { get; set; }
        public int RenewalFlags { get; set; }
        public int PaymentFlags { get; set; }
        public int ProductionFlags { get; set; }
        public int NonStandardFlags { get; set; }
        public int ProductType { get; set; }
        public int ClaimFlags { get; set; }
        public int OwnerFlags { get; set; }
        public int GracePeriodFlag { get; set; }
        public int GracePeriodMax { get; set; }
        public string AcquisitionMethod { get; set; }
        public decimal AcquisitionCost { get; set; }
        public int MaxAcquisition { get; set; }
        public int BonusExpiredPeriod { get; set; }
        public int BonusCalculationPeriod { get; set; }
        public string BizType { get; set; }
        public int ReceiptFlag { get; set; }
        public bool isScoringAreaOJK { get; set; }

        public string ProductCode { get; set; }
        public string COBId { get; set; }
        public int RemainderLetter1 { get; set; }
        public int RemainderLetter2 { get; set; }
        public int GracePeriod { get; set; }
        public string MouID { get; set; }

        public DateTime ValidFrom { get; set; }
        public DateTime ValidTo { get; set; }
        public bool Status { get; set; }
        public long CoverageFlags { get; set; }
        public int LoadingType { get; set; }
        public string Description { get; set; }
        public int IsLexus { get; set; }
        public List<string> PayToPartyList { get; set; }
    }
    public class ProductDiscount
    {
        public string ProductCode { get; set; }
        public string DiscountID { get; set; }
        public string ApplyFlags { get; set; }
        public string DiscountName { get; set; }
        public decimal DiscountPercentage { get; set; }
        public int DiscountLevel { get; set; }
        public int DiscountType { get; set; }
        public int CommissionFlag { get; set; }
    }

    public class ProductInterestCoverageDeductibleClasue
    {
        public string ProductGroup { get; set; }
        public string ProductCode { get; set; }
        public string InterestID { get; set; }
        public string CoverageID { get; set; }
        public string DeductibleCode { get; set; }
        public string ClauseID { get; set; }
        public int IsGroup { get; set; }
        public List<string> GroupMember { get; set; }
        public string InterestCoverageGroupID { get; set; }

    }

    public class ProductDeductible
    {
        public string ProductCode { get; set; }
        public string DeductibleCode { get; set; }
        public string DeductibleType { get; set; }
        public string BaseCurrencyFromSumInsured { get; set; }
        public string BaseCurrencyToSumInsured { get; set; }
        public string CoverDiscountPercentage { get; set; }
        public string FlatCurrency { get; set; }
        public decimal FlatAmount { get; set; }
        public string PercentSumInsured { get; set; }
        public string PercentClaim { get; set; }
        public string Bc_MinimumAmount { get; set; }
        public string Bc_MaximumAmount { get; set; }
        public string Description { get; set; }
    }
    public class NSAAutomatic
    {
        public string OrderNo { get; set; }
        public string Description { get; set; }
        public string Reason { get; set; }
        public string NSACategoryCode { get; set; }
        public string Status { get; set; }
        public string NSAStatus { get; set; }
    }

    public class AutoCoverage
    {
        public int CalculationMethod { get; set; }
        List<dynamic> CoverList { get; set; }
        public decimal EntryPercentage { get; set; }
        public int NDays { get; set; }
        public DateTime CoverFrom { get; set; }
        public DateTime CoverTo { get; set; }
        public decimal ActualPremium { get; set; }
        public decimal TotalPremiAuto { get; set; }
        public decimal CoverPremium { get; set; }
        public decimal NetPremium { get; set; }
        public decimal Rate { get; set; }
        public decimal MaxSI { get; set; }
        public decimal ExcessRate { get; set; }
        public decimal GivenValue { get; set; }
        public string GivenCurrencyID { get; set; }
        public decimal Loading { get; set; }

        public List<dynamic> DiscountList { get; set; }
        public List<dynamic> CommissionList { get; set; }
        public string CoverageID { get; set; }
        public int Auto_apply { get; set; }
        public string Description { get; set; }
        public string CoverType { get; set; }
    }

    public class Interest
    {
        public bool Additional { get; set; }
        public List<CoverageData> CoverageList { get; set; }
        public string CurrencyID { get; set; }
        public string DeductiobleAmount { get; set; }
        public string DeductibleCode { get; set; }
        public string Description { get; set; }
        public int EndorsementNumber { get; set; }
        public int ExchangeRate { get; set; }
        public DateTime FromPeriod { get; set; }
        public DateTime ToPeriod { get; set; }
        public string InterestID { get; set; }
        public string InsuranceType { get; set; }
        public string InterestType { get; set; }
    }
    public class IndirectInsurance
    {
        public string OrderNo { get; set; }// char(15)
        public string AcceptanceId { get; set; }//- char(6)
        public int FullReceived { get; set; }//- bit
        public string CedingCoy { get; set; }//- char(11)
        public string RefPolicyNo { get; set; }//- varchar(30)
        public DateTime? OfferingLetterDate { get; set; }//- datetime
        public string OfferingLetterNo { get; set; }//- varchar(30)
        public decimal OurShareAmt { get; set; }//- numeric
        public Decimal OurSharePct { get; set; }//- numeric
        public decimal HandlingFeePct { get; set; }//- numeric
        public DateTime? RISlipDate { get; set; }//- datetime
        public DateTime? SlipReceiveDate { get; set; }//- datetime
        public DateTime? WPCDate { get; set; }//- datetime
        public DateTime? PeriodFrom { get; set; }//- datetime
        public DateTime? PeriodTo { get; set; }//- datetime
        public decimal OriginalRate { get; set; }//- numeric
        public string CalcMethod { get; set; }//- char(1)
    }
    public class IndirectMember
    {
        public string OrderNo { get; set; }// char(15)
        public string MemberCode { get; set; }//- char(6)
        public string MemberType { get; set; }//- bit
        public decimal MemberSharePct { get; set; }//- char(11)        
    }

    public class LinkOrderAgency
    {
        public string OrderNo { get; set; } // char(15)
        public string ReferenceNo { get; set; } //{get;set;} // varchar(50)
        public string ReferenceType { get; set; } // char(6)
        public string OtherNo { get; set; } //{get;set;} // varchar(50)
        public string ClientCode { get; set; } //{get;set;} // varchar(22)
        public string UplinerClientCode { get; set; } //{get;set;} // varchar(22)
        public string LeaderClientCode { get; set; } //{get;set;} // varchar(22)                  
        public int SendStatus = 0;
        public int ExpiredSendStatus = 0;
    }

    public class TitanAgentInfo
    {
        public string CustomerID { get; set; }
        public string ClientCode { get; set; }
        public string ClientName { get; set; }
        public string UplinerCustomerID { get; set; }
        public string UplinerCode { get; set; }
        public string UplinerName { get; set; }
        public string LeaderCustomerID { get; set; }
        public string LeaderCode { get; set; }
        public string LeaderName { get; set; }
    }

    public class NoCoverOrdData
    {
        public string OrderNo { get; set; }
        public int ObjectNo { get; set; }
        public int EndorsementNo { get; set; }
        public string Type { get; set; }
        public string NoCoverID { get; set; }
        public string Name { get; set; }
        public string DemageType { get; set; }
        public string Description { get; set; }
        public int Quantity { get; set; }
    }

    public class OriginalDefectOrdData
    {
        public string OrderNo { get; set; }
        public int ObjectNo { get; set; }
        public int EndorsementNo { get; set; }
        public string PartID { get; set; }
        public string Name { get; set; }
        public string DamageCategory { get; set; }
        public string DamageCategoryDesc { get; set; }
        public string Description { get; set; }
    }

    

    public class NonStandardAccessoriesOrdData
    {
        public string OrderNo { get; set; }
        public int ObjectNo { get; set; }
        public int EndorsementNo { get; set; }
        public string AccsCode { get; set; }
        public string AccsCatCode { get; set; }
        public string AccsPartCode { get; set; }
        public int IncludeTsi { get; set; }
        public string Brand { get; set; }
        public decimal SumInsured { get; set; }
        public int Quantity { get; set; }
        public decimal Premi { get; set; }
        public int Category { get; set; }
    }

    public class PendingOrderInfo
    {
        public string OrderNo { get; set; }
        public DateTime EntryDate { get; set; }
        public string EntryUser { get; set; }
    }

    public class BookingSurvey
    {
        public string TransactionID { get; set; }
        public string SurveyAreaCode { get; set; }
        public DateTime SurveyDate { get; set; }
        public string SurveTimeCode { get; set; }
        public string Actor { get; set; }
    }

    public class SurveyOrder
    {
        public string  ReferenceTypeCode { get; set; }
        public string ReferenceNo { get; set; }
        public string AreaCode { get; set; }
        public DateTime? SurveyDate { get; set; }
        public string SurveyTime { get; set; }
        public string SurveyType { get; set; }
        public string SurveyMode { get; set; }
        public string ApplicationSourceCode { get; set; }
        public string AssignedSurveyorID { get; set; }
        public string ActualSurveyorID { get; set; }
        public string CustomerOnLocation { get; set; }
        public string PhoneNoOnLocation { get; set; }
        public string SurveyAddress { get; set; }
        public string WorkshopCode { get; set; }
        public string OrderNo { get; set; }
        public string LKNo { get; set; }
        public string NameOnPolicy { get; set; }
        public string CreatedBy { get; set; }
        public string ChassisNo { get; set; }
        public string EngineNo { get; set; }
    }

    public class SetOrderNoResult {
        public string UserID { get; set; }
        public bool IsOrderNoExist { get; set; }
        public string OrderNo { get; set; }
    }

    //0232URF2019
    public class PreBookVAModel
    {
        public string VirtualAccountNumber { get; set; }
        public string PolicyType { get; set; }
        public string PolicyNumber { get; set; }
        public DateTime? PolicyPeriodFrom { get; set; }
        public DateTime? PolicyPeriodTo { get; set; }
        public string AccountManager { get; set; }
        public string AccountManagerName { get; set; }
        public string SAAdmin { get; set; }
        public string SAAdminName { get; set; }
        public string PICMobileNumber { get; set; }
        public string CustomerName { get; set; }
        public string IDType { get; set; }
        public string IDNumber { get; set; }
        public string Remarks { get; set; }
        public DateTime? ValidFrom { get; set; }
        public DateTime? ValidTo { get; set; }
        public bool IsNew { get; set; }
        public string CreatedBy { get; set; }
    }

}
