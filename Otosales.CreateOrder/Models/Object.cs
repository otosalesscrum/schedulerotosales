﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otosales.Models
{
    #region WTOrder
    public class WTOrderDetailData
    {
        public WTOrderData WTOrder { get; set; }
        public List<WTDtlObjectData> WTObjectData { get; set; }
        public List<WTDtlMVData> WTMVData { get; set; }
        public List<WTInterest> WTDtlInterest { get; set; }
        public List<WTDtlCoverage> WTDtlCoverage { get; set; }
        public List<WTOrderObjectClause> WTOrderObjectClause { get; set; }
        public List<WTOrderPolicyClause> WTOrderPolicyClause { get; set; }
        public List<WTOrderDtlAddress> WTOrderDtlAddress { get; set; }
        public WTInwardReinsurance WTInward { get; set; }
        public List<WTInwardMember> WTInwardMember { get; set; }
        public List<WTInsured> WTInsured { get; set; }
        //        public List<NSA> NSA { get; set; }
        public List<NoCover> WTNoCover { get; set; }
        public List<OriginalDefect> WTOriginalDefect { get; set; }
        public List<NonStandardAccessories> WTNonStandardAccessories { get; set; }
    }

    public class WTInsured
    {
        public string QuotationNo { get; set; }
        public int ObjectNo { get; set; }
        public int InterestNo { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public decimal SumInsured { get; set; }
        public string Status { get; set; }

    }

    public class WTDtlObjectData
    {
        public string QuotationNo { get; set; }
        public int ObjectNo { get; set; }
        public string RIStatus { get; set; }
        public string Notes { get; set; }
        public string Status { get; set; }
        public DateTime PeriodFrom { get; set; }
        public DateTime CancelDate { get; set; }
        public DateTime PeriodTo { get; set; }
        public string Description { get; set; }
        public decimal BCOwnRetention = 0;
        public decimal BCFacultativeIn = 0;
        public decimal BCSumInsured = 0;
        public decimal BCCoinsuranceIn = 0;
        public decimal BCOriginalSI = 0;
        public decimal BCTotalFacultative = 0;
        public decimal BCCoinsuranceOut = 0;
        public decimal BCTotalTreaty = 0;
        public int StandardReferenceId = 0;
        public string EntryUsr { get; set; }

        public int SurveyStatus = 0;
    }

    public class WTDtlMVData
    {
        public string QuotationNo { get; set; } // char(15)
        public int ObjectNo { get; set; } // numeric
        public string VehicleCode { get; set; } // char(6)
        public string VehicleType { get; set; } // char(6)
        public string BrandCode { get; set; } // char(6)
        public string ModelCode { get; set; } // char(6)
        public string Series { get; set; } // char(50)
        public int TransmissionType { get; set; } // numeric
        public int MfgYear { get; set; } // numeric
        public bool NewCar { get; set; } // bit
        public int Kilometers { get; set; } // numeric
        public string Color { get; set; } // char(6)
        public bool LightColor { get; set; } // bit
        public bool Metalic { get; set; } // bit
        public string ColorOnBPKB { get; set; } // char(50)
        public string ContractNo { get; set; } // char(50)
        public string ClientNo { get; set; } // char(50)
        public int SittingCapacity { get; set; } // numeric
        public string UsageCode { get; set; } // char(6)
        public string GeoAreaCode { get; set; } // char(6)
        public string ChasisNumber { get; set; } // char(30)
        public string EngineNumber { get; set; } // char(30)
        public string RegistrationNumber { get; set; } //{get;set;} // varchar(16)
        public decimal MarketPrice { get; set; } // numeric
        public string CurrId { get; set; } // char(3)
        public string EntryUsr { get; set; } // char(5) 
        public int SurveyCategory { get; set; }
        public int SurveyType { get; set; }
        public string SurveyBranchID { get; set; }
        public string SurveyDate { get; set; }
        public string SurveyStartTime { get; set; }
        public string SurveyEndTime { get; set; }
        public int SurveySlotID { get; set; }
        public string SurveyAddress { get; set; }
        public string SurveyLatitude { get; set; }
        public string SurveyLongitude { get; set; }
        public int SurveyAreaID { get; set; }
        public string SurveyAreaCode { get; set; }
        public string SurveyorID { get; set; }
        public int SkipSurveyF { get; set; }

    }
    public class WTOrderData
    {
        public long WTOrderID { get; set; }
        public string QuotationNo { get; set; }
        public string OrderNo { get; set; }
        public string ProspectID { get; set; }
        public string CustomerID { get; set; }
        public string NameOnPolicy { get; set; }
        public string OrderType { get; set; }
        public string SalesmanID { get; set; }
        public string BranchID { get; set; }
        public string AOSendDate { get; set; }
        public string ProductCode { get; set; }
        public string SegmentCode { get; set; }
        public string AcceptanceID { get; set; }
        public string FinInstitutionCode { get; set; }
        public string BrokerCode { get; set; }
        public string PolicyHolderCode { get; set; }
        public string PayerCode { get; set; }
        public DateTime PeriodFrom { get; set; }
        public DateTime PeriodTo { get; set; }
        public decimal SharePct { get; set; }
        public decimal OurSharePct { get; set; }
        public string OrderStatus { get; set; }
        public string TermCode { get; set; }
        public bool RequestDuplicate { get; set; }
        public string PolicyAddressType { get; set; }
        public string PolicyAddressPostalCode { get; set; }
        public string PolicyAddress { get; set; }
        public string DeliveryToType { get; set; }
        public string DeliveryUpName { get; set; }
        public string DeliveryAddressType { get; set; }
        public string DeliveryAddressTypeGC { get; set; }
        public string DeliveryPostalCode { get; set; }
        public string DeliveryAddress { get; set; }
        public string BillingAddressType { get; set; }
        public string BillingAddressPostalCode { get; set; }
        public string BillingAddress { get; set; }
        public string AgentID { get; set; }
        public string UplinerID { get; set; }
        public string LeaderID { get; set; }
        public string SalesmanDealerID { get; set; }
        public string BranchManagerID { get; set; }
        public string SupervisorID { get; set; }
        public string DealerID { get; set; }
        public bool RowStatus { get; set; }
        public bool IsLocked { get; set; }
        public string LastLockedDate { get; set; }
        public string LastLockedBy { get; set; }
        public string CreatedBy { get; set; }
        public string PreOrderedVANumber { get; set; }
        public DateTime CreatedDate { get; set; }
        public string SubmitUser { get; set; }
        //#Sprint 5
        public int GracePeriod { get; set; }
        public string MouID { get; set; }

    }
    public class WTOrder
    {
        public long WTOrderID { get; set; }
        public string QuotationNo { get; set; }
        public string OrderNo { get; set; }
        public string ProspectID { get; set; }
        public string CustomerID { get; set; }
        public string NameOnPolicy { get; set; }
        public string OrderType { get; set; }
        public string SalesmanID { get; set; }
        public string BranchID { get; set; }
        public string AOSendDate { get; set; }
        public string ProductCode { get; set; }
        public string SegmentCode { get; set; }
        public string AcceptanceID { get; set; }
        public string FinInstitutionCode { get; set; }
        public string BrokerCode { get; set; }
        public string PolicyHolderCode { get; set; }
        public string PayerCode { get; set; }
        public DateTime PeriodFrom { get; set; }
        public DateTime PeriodTo { get; set; }
        public string SharePct { get; set; }
        public decimal OurSharePct { get; set; }
        public string OrderStatus { get; set; }
        public string TermCode { get; set; }
        public bool RequestDuplicate { get; set; }
        public string PolicyAddressType { get; set; }
        public string PolicyAddressPostalCode { get; set; }
        public string PolicyAddress { get; set; }
        public string DeliveryToType { get; set; }
        public string DeliveryUpName { get; set; }
        public string DeliveryAddressType { get; set; }
        public string DeliveryAddressTypeGC { get; set; }
        public string DeliveryPostalCode { get; set; }
        public string DeliveryAddress { get; set; }
        public string BillingAddressType { get; set; }
        public string BillingAddressPostalCode { get; set; }
        public string BillingAddress { get; set; }
        public string AgentID { get; set; }
        public string UplinerID { get; set; }
        public string LeaderID { get; set; }
        public string SalesmanDealerID { get; set; }
        public string BranchManagerID { get; set; }
        public string SupervisorID { get; set; }
        public string DealerID { get; set; }
        public bool RowStatus { get; set; }
        public bool IsLocked { get; set; }
        public string LastLockedDate { get; set; }
        public string LastLockedBy { get; set; }
        public string CreatedBy { get; set; }
        public string PreOrderedVANo { get; set; }
        public string CreatedDate { get; set; }
    }
    public class WTInterest
    {
        public long WTDtlInterestID { get; set; }
        public string QuotationNo { get; set; }
        public int ObjectNo { get; set; }
        public int InterestNo { get; set; }
        public string AdjustableClause { get; set; }
        public string Description { get; set; }
        public string InsuranceType { get; set; }
        public decimal DedFlatAmount { get; set; }
        public DateTime PeriodFrom { get; set; }
        public string Status { get; set; }
        public DateTime PeriodTo { get; set; }
        public DateTime CancelDate { get; set; }
        public string InterestType { get; set; }
        public string InterestCode { get; set; }
        public decimal OriginalSI { get; set; }
        public decimal CoinsuranceIn { get; set; }
        public decimal ClaimableAmount { get; set; }
        public decimal SumInsured { get; set; }
        public decimal FacultativeIn { get; set; }
        public decimal ExchangeRate { get; set; }
        public decimal OrigExchangeRateinalSI { get; set; }
        public decimal StockMaximumPossible { get; set; }
        public string CurrId { get; set; }
        public string DedFlatCurrency { get; set; }
        public string DeductibleCode { get; set; }
        public decimal ParNo { get; set; }
        public string EntryUsr { get; set; }
        public DateTime EntryDt { get; set; }
        public string UpdateUsr { get; set; }
        public DateTime UpdateDt { get; set; }
    }
    public class WTDtlInterest
    {
        public long WTDtlInterestID { get; set; }
        public string QuotationNo { get; set; }
        public int ObjectNo { get; set; }
        public int InterestNo { get; set; }
        public string AdjustableClause { get; set; }
        public string Description { get; set; }
        public string InsuranceType { get; set; }
        public decimal DedFlatAmount { get; set; }
        public string PeriodFrom { get; set; }
        public string Status { get; set; }
        public string PeriodTo { get; set; }
        public string CancelDate { get; set; }
        public string InterestType { get; set; }
        public string InterestCode { get; set; }
        public decimal OriginalSI { get; set; }
        public decimal CoinsuranceIn { get; set; }
        public decimal ClaimableAmount { get; set; }
        public decimal SumInsured { get; set; }
        public decimal FacultativeIn { get; set; }
        public decimal ExchangeRate { get; set; }
        public decimal OrigExchangeRateinalSI { get; set; }
        public decimal StockMaximumPossible { get; set; }
        public string CurrId { get; set; }
        public string DedFlatCurrency { get; set; }
        public string DeductibleCode { get; set; }
        public decimal ParNo { get; set; }
        public string EntryUsr { get; set; }
        public DateTime EntryDt { get; set; }
        public string UpdateUsr { get; set; }
        public DateTime UpdateDt { get; set; }
        //#Sprint 5
        //public int? CalculationMethod { get; set; }
        public int CalculationMethod { get; set; }
        //#Sprint 8        
        public string InterestIDGeneric { get; set; }

    }
    public class WTDtlCoverage
    {
        public long WTDtlCoverageID { get; set; }
        public string QuotationNo { get; set; }
        public int ObjectNo { get; set; }
        public int InterestNo { get; set; }
        public int CoverageNo { get; set; }
        public string InsuranceType { get; set; }
        public string COBId { get; set; }
        public string CurrId { get; set; }
        public string Status { get; set; }
        public string InterestType { get; set; }
        public string CoverageId { get; set; }
        public decimal NetPremium { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime CancelDate { get; set; }
        public decimal Ndays { get; set; }
        public decimal Net1 { get; set; }
        public decimal Net2 { get; set; }
        public decimal Net3 { get; set; }
        public decimal GrossPremium { get; set; }
        public decimal CoverPremium { get; set; }
        public decimal SumInsured { get; set; }
        public decimal Loading { get; set; }
        public int CalcMethod { get; set; }
        public decimal TotalDiscount { get; set; }
        public decimal TotalCommission { get; set; }
        public decimal DedFlatAmount { get; set; }
        public string DedFlatCurrency { get; set; }
        public string DeductibleCode { get; set; }
        public decimal EntryPct { get; set; }
        public decimal Rate { get; set; }
        public decimal Discount { get; set; }
        public decimal MaxSi { get; set; }
        public decimal ExcessRate { get; set; }
        public string EntryUsr { get; set; }
        public DateTime EntryDt { get; set; }
        public string UpdateUsr { get; set; }
        public DateTime UpdateDt { get; set; }
        public decimal AcquisitionCost { get; set; }
        public string AutoApply { get; set; }
        public string InterestId { get; set; }
        //#Sprint 5
        public string BasicCoverageID { get; set; }
        public decimal AgreedValue { get; set; }
        //#Sprint 8
        public string CoverageIDGeneric { get; set; }
        public string InterestIDGeneric { get; set; }
    }

    public class WTOrderObjectClause
    {
        public string QuotationNo { get; set; }
        public decimal ObjectNo { get; set; }
        public decimal ClauseNo { get; set; }
        public string ClauseID { get; set; }
        public string ClauseDesc { get; set; }
        public string Description { get; set; }
        public int IsAutomated { get; set; }
    }

    public class WTOrderPolicyClause
    {
        public string QuotationNo { get; set; }
        //public long OrderNo { get; set; }
        public string ClauseID { get; set; }
        public string Description { get; set; }
        public bool RowStatus { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public int IsAutomated { get; set; }
    }

    public class WTOrderDtlAddress
    {
        public long WTOrderDtlAddressID { get; set; }
        public long WTOrderID { get; set; }
        public string AddressType { get; set; }
        public string SourceType { get; set; }
        public int RenewalNotice { get; set; }
        public string DeliveryCode { get; set; }
        public string Address { get; set; }
        public string RT { get; set; }
        public string RW { get; set; }
        public string PostalCode { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Handphone { get; set; }
        public string ContactPerson { get; set; }
        public int RowStatus { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
    }

    public class NoCover
    {
        public string QuotationNo { get; set; }
        public int ObjectNo { get; set; }
        public int EndorsementNo { get; set; }
        public string Type { get; set; }
        public string NoCoverID { get; set; }
        public string Name { get; set; }
        public string DemageTypeCode { get; set; }
        public string DemageTypeDesc { get; set; }
        public string Description { get; set; }
        public int? Quantity { get; set; }
    }

    public class OriginalDefect
    {
        public string QuotationNo { get; set; }
        public int ObjectNo { get; set; }
        public int ClauseNo { get; set; }
        public int EndorsementNo { get; set; }
        public string PartID { get; set; }
        public string Name { get; set; }
        public string DamageCategory { get; set; }
        public string DamageCategoryDesc { get; set; }
        public string Description { get; set; }
    }

    public class NonStandardAccessories
    {
        public string QuotationNo { get; set; }
        public int ObjectNo { get; set; }
        public int EndorsementNo { get; set; }
        public string AccsCode { get; set; }
        public string AccsCatCode { get; set; }
        public string AccsDescription { get; set; }
        public string AccsPartCode { get; set; }
        public string AccsCatDescription { get; set; }
        public string AccsPartDescription { get; set; }
        public int IncludeTsi { get; set; }
        public string Brand { get; set; }
        public decimal SumInsured { get; set; }
        public int Quantity { get; set; }
        public decimal Premi { get; set; }
        public int Category { get; set; }
    }
    #endregion


    #region NSA

    public class NSA
    {
        public List<NSAItem> NSAItems { get; set; }
    }

    public class NSAItem
    {
        public string CategoryID { get; set; }
        public int ObjectNo { get; set; }
        public string ObjectDescription { get; set; }
        public string Description { get; set; }
        public string Reason { get; set; }
        public string Requester { get; set; }
        public int NSAStatus { get; set; }
    }

    #endregion

    #region Survey

    public class SurveyAddress
    {
        public string ZipCode { get; set; }
        public string CityID { get; set; }
        public string DistrictCode { get; set; }
        public string GeoAreaCode { get; set; }
        public string ZipCodeDescription { get; set; }
        public string DistrictName { get; set; }
        public string CityName { get; set; }
        public string ProvinceID { get; set; }
    }

    public class SurveyRegionCode
    {
        public string GeoAreaCode { get; set; }
        public string SubGeoAreaCode { get; set; }
        public string RegionCode { get; set; }
    }

    public class CheckSurveyDataByChasisEngineModel
    {
        public string AdditionalOrderAcquisitionSurveyID { get; set; }
        public string SurveyorID { get; set; }
        public string SurveyorName { get; set; }
        public string SurveyDate { get; set; }
        public string PICName { get; set; }
        public string PhoneNumber { get; set; }
        public string SurveyLocation { get; set; }
        public string ChassisEngine { get; set; }
        public string Detail { get; set; }
        public string AreaCode { get; set; }
        public string AreaAlias { get; set; }
    }
    #endregion

    #region Customer
    public class PersonalProspect
    {
        public string ProspectID { get; set; }
        public string IDNumber { get; set; }
        public string IDType { get; set; }
        public string ProspectType { get; set; }
        public string ProspectName { get; set; }
        public string BirthDate { get; set; }
        public string Gender { get; set; }
        public string Religion { get; set; }
        public string HomeAddress { get; set; }
        public string HomePostalCode { get; set; }
        public string HomePostalCodeDesc { get; set; }
        public string HomeRT { get; set; }
        public string HomeRW { get; set; }
        public string HomeFax1 { get; set; }
        public string HomeFax2 { get; set; }
        public string Income { get; set; }
        public string MaritalStatus { get; set; }
        public string JobTitle { get; set; }
        public string CompanyName { get; set; }
        public string Email { get; set; }
        public string MobilePhone1 { get; set; }
        public string MobilePhone2 { get; set; }
        public string MobilePhone3 { get; set; }
        public string MobilePhone4 { get; set; }
        public string HomePhone1 { get; set; }
        public string HomePhone2 { get; set; }
        public string OfficePhone1 { get; set; }
        public string OfficeExt1 { get; set; }
        public string OfficeAddress { get; set; }
        public string OfficePostalCode { get; set; }
        public string OfficePostalCodeDesc { get; set; }
        //public List<Images> ImageList { get; set; }
        public string Facebook { get; set; }
        public string Instagram { get; set; }
        public string Twitter { get; set; }
    }

    public class CustomerPersonal
    {
        public string CustomerID { get; set; }
        public string ProspectID { get; set; }
        public string IDNumber { get; set; }
        public string IDType { get; set; }
        public string CustomerType { get; set; }
        public string CustomerName { get; set; }
        public string BirthDate { get; set; }
        public string Gender { get; set; }
        public string Religion { get; set; }
        public string HomeAddress { get; set; }
        public string HomePostalCode { get; set; }
        public string HomePostalCodeDesc { get; set; }
        public string HomeRT { get; set; }
        public string HomeRW { get; set; }
        public string HomeFax1 { get; set; }
        public string HomeFax2 { get; set; }
        public string Income { get; set; }
        public string MaritalStatus { get; set; }
        public string JobTitle { get; set; }
        public string CompanyName { get; set; }
        public string Email { get; set; }
        public string MobilePhone1 { get; set; }
        public string MobilePhone2 { get; set; }
        public string MobilePhone3 { get; set; }
        public string MobilePhone4 { get; set; }
        public string HomePhone1 { get; set; }
        public string HomePhone2 { get; set; }
        public string OfficePhone1 { get; set; }
        public string OfficeExt1 { get; set; }
        public string OfficeAddress { get; set; }
        public string OfficePostalCode { get; set; }
        public string OfficePostalCodeDesc { get; set; }
        public string PreferredCustomerStatus { get; set; }
        //public List<Images> ImageList { get; set; }
        //public int IsVIP { get; set; }
        public string IsVIP { get; set; }
        public string Facebook { get; set; }
        public string Instagram { get; set; }
        public string Twitter { get; set; }
    }
    
    public class CorporateProspect
    {
        public string ProspectID { get; set; }
        public string ProspectType { get; set; }
        public string ProspectName { get; set; }
        public string NameOnNPWP { get; set; }
        public string NPWPNo { get; set; }
        public string NPWPDate { get; set; }
        public string NPWPAddress { get; set; }
        public string SIUPNo { get; set; }
        public string PKPNo { get; set; }
        public string PKPDate { get; set; }
        public string BusinessType { get; set; }
        public string ClientType { get; set; }
        public string CompanyGroup { get; set; }
        public string AssetOwner { get; set; }
        public string OfficeAddress { get; set; }
        public string OfficePostalCode { get; set; }
        public string OfficePostalCodeDesc { get; set; }
        public string OfficePhone1 { get; set; }
        public string OfficePhone2 { get; set; }
        public string OfficePhoneExt { get; set; }
        public string OfficeRT { get; set; }
        public string OfficeRW { get; set; }
        public string OfficeFax1 { get; set; }
        public string OfficeFax2 { get; set; }
        public string PIC { get; set; }
        public string Email { get; set; }
        public string MobilePhone1 { get; set; }
        public string MobilePhone2 { get; set; }
        public string HomeAddress { get; set; }
        public string HomePostalCode { get; set; }
        public string HomePostalCodeDesc { get; set; }
        public string HomePhone1 { get; set; }
        public string HomePhone2 { get; set; }
        //public List<Images> ImageList { get; set; }
    }

    public class CustomerCoorporate
    {
        public string CustomerID { get; set; }
        public string ProspectID { get; set; }
        public string CustomerType { get; set; }
        public string CustomerName { get; set; }
        public string NameOnNPWP { get; set; }
        public string NPWPNo { get; set; }
        public string NPWPDate { get; set; }
        public string NPWPAddress { get; set; }
        public string SIUPNo { get; set; }
        public string PKPNo { get; set; }
        public string PKPDate { get; set; }
        public string BusinessType { get; set; }
        public string ClientType { get; set; }
        public string CompanyGroup { get; set; }
        public string AssetOwner { get; set; }
        public string OfficeAddress { get; set; }
        public string OfficePostalCode { get; set; }
        public string OfficePostalCodeDesc { get; set; }
        public string OfficePhone1 { get; set; }
        public string OfficePhone2 { get; set; }
        public string OfficePhoneExt { get; set; }
        public string OfficeRT { get; set; }
        public string OfficeRW { get; set; }
        public string OfficeFax1 { get; set; }
        public string OfficeFax2 { get; set; }
        public string PIC { get; set; }
        public string Email { get; set; }
        public string MobilePhone1 { get; set; }
        public string MobilePhone2 { get; set; }
        public string HomeAddress { get; set; }
        public string HomePostalCode { get; set; }
        public string HomePostalCodeDesc { get; set; }
        public string HomePhone1 { get; set; }
        public string HomePhone2 { get; set; }
        //public List<Images> ImageList { get; set; }
        //public int IsVIP { get; set; }
        public string IsVIP { get; set; }
    }
    #endregion

    #region Additional Info
    public class AdditionalInfo
    {
        public string AdditionalCode { get; set; }
        public string AdditionalInfoValue { get; set; }
    }

    #endregion

    #region UserLogin
    public class UserLogin
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string DepartmentID { get; set; }
        public string BranchID { get; set; }
        public string EmployeeNo { get; set; }
        public string Email { get; set; }
        public List<string> BusinessRole { get; set; }
    }
    #endregion

    #region Installment
    public class Installment
    {
        public int DayNo { get; set; }
        public decimal InstallPercentage { get; set; }
        public decimal InterestPercentage { get; set; }
    }
    #endregion
}
