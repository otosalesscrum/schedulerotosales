﻿
using a2is.Framework.Monitoring;
using Otosales.CreateOrder.Repository;

using Otosales.Models;
using Otosales.Repository;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Otosales.Models.Constant;

namespace Otosales.Logic
{
    class OrderLogic : CommonLogic
    {
        public static string UserID { get { return CommonLogic.GetSettingUserID(); } }
        private static a2isLogHelper logger = new a2isLogHelper();

        #region Otosales

        public static void SetOtosalesCustomerToProspect(string followUpNo)
        {
            OtosalesOrderRepository OOR = new OtosalesOrderRepository();
            // get ProspectCustomer
            dynamic c = OOR.GetFollowUpCustomer(followUpNo);

            // jika customer personal
            //var jsonData = HttpContext.Current.Request.Form["pPersonalProspect"];
            //var imageReferenceNo = HttpContext.Current.Request.Form["pImageReferenceNo"];
            //PersonalProspect ps = JsonConvert.DeserializeObject<PersonalProspect>(jsonData);
            /*
             * {"ProspectID":"","IDNumber":"12345678901234567890","IDType":"KTP","ProspectType":1,
             * "ProspectName":"BENNY SUsilo","BirthDate":"02/02/2010","Gender":"1","HomeAddress":"home addr 1",
             * "HomePostalCode":"12440","HomePostalCodeDesc":"12440 - KEL. LEBAK BULUS, JAKARTA, DKI JAKARTA, INDONESIA",
             * "HomeRT":"","HomeRW":"","HomeFax1":"","HomeFax2":"","MaritalStatus":"","JobTitle":"","CompanyName":"",
             * "Email":"","MobilePhone1":"1234567890123","MobilePhone2":"","MobilePhone3":"","MobilePhone4":"",
             * "HomePhone1":"","HomePhone2":"","OfficePhone1":"","OfficeExt1":"","OfficeAddress":"",
             * "OfficePostalCode":"","OfficePostalCodeDesc":"","Facebook":"","Twitter":"","Instagram":""}
             */
             
            if (c !=null && c.CustID != null && string.IsNullOrEmpty(c.PolicyOrderNo))
            {
                PersonalProspect ps = null;
                CorporateProspect cp = null;
                bool isPersonalProspect = !Convert.ToBoolean(c.isCompany);

                #region PersonalProspect

                ps = new PersonalProspect()
                {
                    IDNumber = c.IdentityNo,
                    IDType = "KTP", //KTP & KITAS/KITAP jadi satu di Otosales, tp di core beda.
                    ProspectType = c.isCompany ? "2" : "1", // Personal  = 1, Company = 2
                    ProspectName = isPersonalProspect ? c.Name : c.CompanyName,
                    BirthDate = c.CustBirthDay == null ? null : c.CustBirthDay.ToString(DateFormatCustom.Format3),
                    Gender = c.CustGender == null ? null :  Convert.ToString(c.CustGender).ToUpper().Equals("M") ? "1" : Convert.ToString(c.CustGender).ToUpper().Equals("F") ? "2" : null, // Male = 1, Female = 2
                    HomeAddress = c.CustAddress, // ga ada
                    HomePostalCode = c.PostalCode,
                    HomePostalCodeDesc = "",
                    HomeRT = "",
                    HomeRW = "",
                    HomeFax1 = "",
                    HomeFax2 = "",
                    MaritalStatus = "",
                    JobTitle = "",
                    CompanyName = "",
                    Email = c.Email1,
                    MobilePhone1 = c.Phone1,
                    MobilePhone2 = c.Phone2,
                    MobilePhone3 = "",
                    MobilePhone4 = "",
                    HomePhone1 = "",
                    HomePhone2 = "",
                    OfficePhone1 = "",
                    OfficeExt1 = "",
                    OfficeAddress = isPersonalProspect ? "" : c.NPWPaddress,
                    OfficePostalCode = isPersonalProspect ? "" : c.PostalCode,
                    OfficePostalCodeDesc = "",
                    Facebook = "",
                    Twitter = "",
                    Instagram = ""
                };
                #endregion

                #region CorporateProspect
                // jika customer company
                /*
                 pCorporateProspect: {"ProspectID":"","CompanyGroup":"","ProspectType":2,"ProspectName":"Astra BLO",
                 "NameOnNPWP":"Astra BLO","NPWPNo":"12.341.231.2-312.312","NPWPDate":"","NPWPAddress":"BSD CITY",
                 "SIUPNo":"","PKPNo":"12.341.231.2-312.312","PKPDate":"","BusinessType":"","ClientType":"",
                 "OfficeAddress":"BSD CITY","OfficePostalCode":"15310",
                 "OfficePostalCodeDesc":"15310 - BUMI SERPONG DAMAI, TANGERANG, BANTEN, INDONESIA",
                 "OfficePhone1":"","OfficePhone2":"","OfficePhoneExt":"","OfficeRT":"","OfficeRW":"",
                 "OfficeFax1":"","OfficeFax2":"","PIC":"BLO","Email":"","MobilePhone1":"1231231232131231",
                 "MobilePhone2":"","MobilePhone3":"","MobilePhone4":"","HomeAddress":"","HomePostalCode":"",
                 "HomePostalCodeDesc":"","HomePhone1":"","HomePhone2":"","AssetOwner":1}
                pImageReferenceNo: TempRef-000032525327
                 */
                cp = new CorporateProspect()
                {
                    ProspectType = "2",
                    ProspectName = c.CompanyName,
                    NameOnNPWP = c.CompanyName,
                    NPWPNo = c.NPWPno,
                    NPWPDate = c.NPWPdate == null ? "" : c.NPWPdate,
                    NPWPAddress = c.NPWPaddress,
                    OfficeAddress = c.OfficeAddress,
                    OfficePostalCode = c.PostalCode,
                    PIC = c.PICname,
                    MobilePhone1 = c.PICPhoneNo,
                    Email = c.Email,
                    AssetOwner = "1",
                    CompanyGroup = "",
                    PKPNo = c.NPWPno,
                    PKPDate = c.NPWPdate == null ? "" : c.NPWPdate.ToString(CultureInfo.InvariantCulture),
                    BusinessType = "",
                    ClientType = "",
                    OfficePostalCodeDesc = "",
                    OfficePhone1 = "",
                    OfficePhone2 = "",
                    OfficePhoneExt = "",
                    OfficeRT = "",
                    OfficeRW = "",
                    OfficeFax1 = "",
                    OfficeFax2 = "",
                    MobilePhone2 = "",
                    HomeAddress = c.CustAddress,
                    HomePhone1 = "",
                    HomePhone2 = "",
                    HomePostalCode = "",
                    HomePostalCodeDesc = ""
                };

                #endregion
                
                string prospectId = CommonLogic.GetProspectID();
                string UserID = CommonLogic.GetSettingUserID();
                if (!string.IsNullOrWhiteSpace(prospectId))
                {
                    CustomerRepository cr = new CustomerRepository();
                    var a = cr.InsertMasterProspect(UserID, prospectId, ps);
                    if (a)
                    {
                        var b = isPersonalProspect ? cr.InsertPersonalProspect(UserID, prospectId, ps) : cr.InsertCorporateProspect(UserID, prospectId, cp);
                        if (b)
                        {
                            if (!isPersonalProspect)
                            {
                                if (cp.PIC != null)
                                {
                                    //cr.InsertProspectPIC(UserID, prospectId, corporate.PICDetail.Name, corporate.PICDetail.Departement, corporate.PICDetail.JobType, corporate.PICDetail.Info);
                                    cr.InsertProspectPIC(UserID, prospectId, cp.PIC, "", "", "");
                                }
                                if (!string.IsNullOrEmpty(cp.ClientType))
                                {
                                    cr.InsertProspectClientType(UserID, prospectId, cp.ClientType);
                                }
                            }

                            //UPDATE ProspectID ke FollowUp
                            OOR.SetFollowUpProspectID(followUpNo, prospectId);
                            ImageLogic.CopyImageDataToMstImage(followUpNo, prospectId,"");
                        }
                    }
                }

            }

        }

        public static void UpdateRenewalCustomerInfo(string followUpNo) {
            var aab = RepositoryBase.GetAABDB();
            var mbl = RepositoryBase.GetAABMobileDB();
            try
            {
                string qGetCustData = @" SELECT CustIDAAB,ProspectID, CAST(COALESCE(isCompany,0) AS BIT) isCompany,
 CustAddress, PostalCode, Email1, pc.Phone1, pc.Phone2, f.SalesOfficerID, f.CustID
 FROM dbo.FollowUp f 
 INNER JOIN dbo.ProspectCustomer pc
 ON pc.CustID = f.CustID
 WHERE FollowUpNo  = @0";
                List<dynamic> custData = mbl.Fetch<dynamic>(qGetCustData, followUpNo);
                if (custData.Count > 0) {
                    string qGetSOID = @";DECLARE @@BranchId VARCHAR(10)
                                        SELECT @@BranchId=REPLACE(BranchCode,'A','') FROM dbo.SalesOfficer WHERE SalesOfficerID = @0 AND RowStatus = 1
                                        SELECT Salesman_Id FROM BEYONDREPORT.AAB.dbo.Mst_Salesman WHERE Branch_Id = @@BranchId AND Name = 'Stephanus Rumawas' AND Status = 1";
                    List<dynamic> lsSls = mbl.Fetch<dynamic>(qGetSOID, custData.First().SalesOfficerID);
                    string EntryUsr = "";
                    if (lsSls.Count > 0)
                    {
                        EntryUsr = lsSls.First().Salesman_Id;
                    }
                    else if (!string.IsNullOrEmpty(custData.First().SalesOfficerID))
                    {
                        EntryUsr = custData.First().SalesOfficerID;
                    }
                    else {
                        EntryUsr = "OTOSL";
                    }
                    string custID = custData.First().CustIDAAB;
                    if (!string.IsNullOrEmpty(custID))
                    {
                        string qUpdateCust = @"UPDATE dbo.Mst_Customer
                                        SET Home_Address = @1,
                                        Home_PostCode = @2,
                                        Email = @3, updatedt = GETDATE(), updateusr = @4
                                        WHERE Cust_Id = @0";
                        aab.Execute(qUpdateCust, custID, custData.First().CustAddress
                            , custData.First().PostalCode, custData.First().Email1, EntryUsr);
                        if (custData.First().isCompany)
                        {
                            string qGetCompanyData = @"SELECT PICname,PICPhoneNo,NPWPaddress,CONVERT(VARCHAR,NULLIF(NPWPdate,''),103) NPWPdate,NPWPno
                                                FROM dbo.ProspectCompany 
                                                WHERE FollowUpNo = @0 AND CustID = @1 AND RowStatus = 1";
                            List<dynamic> CompanyData = mbl.Fetch<dynamic>(qGetCompanyData, followUpNo, custData.First().CustID);
                            if (CompanyData.Count > 0)
                            {
                                if (!string.IsNullOrEmpty(CompanyData.First().PICname)&& !string.IsNullOrWhiteSpace(CompanyData.First().PICname))
                                {
                                    string qUpdateCustCompany = @";IF EXISTS(SELECT * FROM dbo.Mst_Cust_Pic WHERE Cust_Id = @0)
										BEGIN
											DELETE FROM dbo.Mst_Cust_Pic WHERE Cust_Id = @0
	                                        INSERT INTO dbo.Mst_Cust_Pic
	                                                ( Cust_Id ,
	                                                  Name ,
	                                                  entryusr ,
	                                                  entrydt
	                                                )
	                                        VALUES  ( @0 , -- Prospect_Id - char(11)
	                                                  @1 , -- Name - varchar(50)
	                                                  @2 , -- entryusr - char(5)
	                                                  GETDATE()  -- entrydt - datetime
	                                                )
										END
										ELSE
                                        BEGIN
	                                        INSERT INTO dbo.Mst_Cust_Pic
	                                                ( Cust_Id ,
	                                                  Name ,
	                                                  entryusr ,
	                                                  entrydt
	                                                )
	                                        VALUES  ( @0 , -- Prospect_Id - char(11)
	                                                  @1 , -- Name - varchar(50)
	                                                  @2 , -- entryusr - char(5)
	                                                  GETDATE()  -- entrydt - datetime
	                                                )
                                        END";
                                    aab.Execute(qUpdateCustCompany, custID, CompanyData.First().PICname, EntryUsr);
                                    if(CompanyData.First().NPWPno != null){
                                        qUpdateCustCompany = @"IF EXISTS(SELECT * FROM dbo.Mst_Customer_AdditionalInfo WHERE CustID = @0 AND AdditionalCode = 'NPWN')
BEGIN
	UPDATE dbo.Mst_Customer_AdditionalInfo 
	SET AdditionalInfoValue = @2, ModifiedBy = @1, ModifiedDate = GETDATE()
	WHERE CustID = @0 AND AdditionalCode = 'NPWN'
END
ELSE
BEGIN
	INSERT INTO dbo.Mst_Customer_AdditionalInfo
	        ( CustID ,AdditionalCode ,AdditionalInfoValue ,CreatedBy ,CreatedDate ,RowStatus)
	VALUES  ( @0 ,'NPWN' ,@2 ,@1 ,GETDATE() ,0)
END";
                                        aab.Execute(qUpdateCustCompany, custID, EntryUsr, CompanyData.First().NPWPno);
                                    }

                                    if (CompanyData.First().NPWPdate != null) {
                                        qUpdateCustCompany = @"IF EXISTS(SELECT * FROM dbo.Mst_Customer_AdditionalInfo WHERE CustID = @0 AND AdditionalCode = 'NPDT')
BEGIN
	UPDATE dbo.Mst_Customer_AdditionalInfo 
	SET AdditionalInfoValue = @2, ModifiedBy = @1, ModifiedDate = GETDATE()
	WHERE CustID = @0 AND AdditionalCode = 'NPDT'
END
ELSE
BEGIN
	INSERT INTO dbo.Mst_Customer_AdditionalInfo
	        ( CustID ,AdditionalCode ,AdditionalInfoValue ,CreatedBy ,CreatedDate ,RowStatus)
	VALUES  ( @0 ,'NPDT' ,@2 ,@1 ,GETDATE() ,0)
END";
                                        aab.Execute(qUpdateCustCompany, custID, EntryUsr, CompanyData.First().NPWPdate);
                                    }
                                    if(CompanyData.First().NPWPaddress != null)
                                    {
                                        qUpdateCustCompany = @"IF EXISTS(SELECT * FROM dbo.Mst_Customer_AdditionalInfo WHERE CustID = @0 AND AdditionalCode = 'NPAD')
BEGIN
	UPDATE dbo.Mst_Customer_AdditionalInfo 
	SET AdditionalInfoValue = @2, ModifiedBy = @1, ModifiedDate = GETDATE()
	WHERE CustID = @0 AND AdditionalCode = 'NPAD'
END
ELSE
BEGIN
	INSERT INTO dbo.Mst_Customer_AdditionalInfo
	        ( CustID ,AdditionalCode ,AdditionalInfoValue ,CreatedBy ,CreatedDate ,RowStatus)
	VALUES  ( @0 ,'NPAD' ,@2 ,@1 ,GETDATE() ,0)
END";
                                        aab.Execute(qUpdateCustCompany, custID, EntryUsr, CompanyData.First().NPWPaddress);
                                    }
                                }
                                //if (!string.IsNullOrEmpty(CompanyData.First().PICPhoneNo) && !string.IsNullOrEmpty(CompanyData.First().PICPhoneNo)) {
                                //    string qUpdateCustCompany = @"UPDATE dbo.Mst_Customer_AdditionalInfo
                                //                            set AdditionalInfoValue = @1 --PhoneNo
                                //                            WHERE CustID = @0 AND AdditionalCode = 'MPC1'";
                                //    aab.Execute(qUpdateCustCompany, custID, CompanyData.First().PICPhoneNo);
                                //}
                            }
                        }
                        //else
                        //{
                        //    string qUpdateCustPersonal = @"UPDATE dbo.Mst_Cust_Personal
                        //                                SET HP = @1, HP_2 = @2
                        //                                WHERE Cust_Id = @0";
                        //    aab.Execute(qUpdateCustPersonal, custID, custData.First().Phone1, custData.First().Phone2);
                        //}
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public static SetOrderNoResult SetOtosalesBookingOrderNo(string followUpNo, bool isRenewal)
        {
            SetOrderNoResult result = new SetOrderNoResult();
            string res = "";
            string qGetPolicyOrderNo = @"SELECT TOP 1 1 FROM FollowUp fu
INNER JOIN OrderSimulation os ON fu.FollowUpNo = os.FollowUpNo and os.ApplyF = 1 and COALESCE(PolicyOrderNo,'') <> '' AND fu.FollowUpNo = @0 ";
            bool orderNoExists = false;
            using (var db = RepositoryBase.GetAABMobileDB())
            {
                orderNoExists = db.Fetch<dynamic>(qGetPolicyOrderNo, followUpNo).Count > 0;
            }
            if (!orderNoExists)
            {
                string orderNo = CommonLogic.GetOrderNo();
                //string vaNo = CommonLogic.GetVANo();
                string policyId = CommonLogic.GetPolicyId();
                string policyNo = CommonLogic.GetPolicyNo();

                string qSetOrderVABookingToFollowUp = @"
;IF 1=@4
BEGIN
    UPDATE os 
    SET os.PolicyOrderNo = @1, os.PolicyId = @2
    from FollowUp fu
    INNER JOIN OrderSimulation os on fu.FollowUpNo = os.FollowUpNo and applyf = 1
    where fu.FollowUpNo = @0
END
ELSE
    UPDATE os 
    SET os.PolicyOrderNo = @1, os.PolicyId = @2, os.PolicyNo = @3
    from FollowUp fu
    INNER JOIN OrderSimulation os on fu.FollowUpNo = os.FollowUpNo and applyf = 1
    where fu.FollowUpNo = @0";
                using (var db = RepositoryBase.GetAABMobileDB())
                {
                    db.Execute(qSetOrderVABookingToFollowUp, followUpNo, orderNo, policyId, policyNo, isRenewal);
                    #region insert Image
                    string qGetFUInfo = @";
select os.PolicyOrderNo,fu.RemarkToSA, os.SalesOfficerID, fu.FollowUpInfo,
DocNSA1, DocNSA2, DocNSA3, DocNSA4, DocNSA5
from FollowUP fu 
INNER JOIN OrderSimulation os on fu.FollowUpNo = os.FollowUpNo and os.ApplyF = 1
where fu.FollowUpNo = @0";
                    dynamic c = db.First<dynamic>(qGetFUInfo, followUpNo);
                    OtosalesOrderRepository OOR = new OtosalesOrderRepository();
                    string userId = "";
                    if (c.SalesOfficerID.Length > 5)
                    {
                        userId = CommonLogic.GetSettingUserID();
                    }
                    else
                    {
                        userId = c.SalesOfficerID;
                    }
                    if (Convert.ToInt32(c.FollowUpInfo) == Convert.ToInt32(FollowUpInfo.SendToSA))
                    {
                        OOR.InsertMstOrderSendToSA(c.PolicyOrderNo, c.RemarkToSA, userId);
                    }
                    else if (Convert.ToInt32(c.FollowUpInfo) == Convert.ToInt32(FollowUpInfo.FUPayment)
                        && string.IsNullOrEmpty(c.DocNSA1) && string.IsNullOrEmpty(c.DocNSA2) && string.IsNullOrEmpty(c.DocNSA3)
                        && string.IsNullOrEmpty(c.DocNSA4) && string.IsNullOrEmpty(c.DocNSA5))
                    {
                        OOR.InsertMstOrderFUPayment(c.PolicyOrderNo, c.RemarkToSA, userId);
                        res = CommonLogic.GetSettingUserID();
                    }
                    ImageLogic.CopyImageDataToMstImage(followUpNo, "", c.PolicyOrderNo);
                    #endregion
                }
            }
            result.UserID = res;
            result.IsOrderNoExist = orderNoExists;
            return result;
        }

        public static void setOrderNonActive(string followUpNo) {
            try
            {
                using (var db = RepositoryBase.GetAABMobileDB())
                {
                    string query = @"UPDATE dbo.FollowUp SET RowStatus = 0 WHERE FollowUpNo = @0 AND RowStatus = 1";
                    db.Execute(query, followUpNo);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Deductible

        private static string GetDeductible(string ProductCode, string InterestID, List<ProductInterestCoverageDeductibleClasue> ClauseDeductible)
        {
            string deductibleCode = "";
            foreach (var items in ClauseDeductible)
            {
                if (!string.IsNullOrWhiteSpace(items.DeductibleCode))
                {
                    if (items.IsGroup == 0)
                    {
                        if (items.ProductCode.Trim().ToUpper() == ProductCode.Trim().ToUpper() && items.InterestID.Trim().ToUpper() == InterestID.Trim().ToUpper())
                        {
                            deductibleCode = items.DeductibleCode;
                            break;
                        }
                    }
                    else if (items.IsGroup == 1)
                    {
                        foreach (var member in items.GroupMember)
                        {
                            if (items.ProductCode.Trim().ToUpper() == ProductCode.Trim().ToUpper() && member.Trim().ToUpper() == InterestID.Trim().ToUpper())
                            {
                                deductibleCode = items.DeductibleCode;
                                break;
                            }
                        }
                        if (!string.IsNullOrWhiteSpace(deductibleCode))
                        {
                            break;
                        }
                    }
                }
            }
            return deductibleCode;
        }

        private static string GetDeductible(string ProductCode, string InterestID, string CoverageID, List<ProductInterestCoverageDeductibleClasue> ClauseDeductible)
        {
            string deductibleCode = "";
            foreach (var items in ClauseDeductible)
            {
                if (!string.IsNullOrWhiteSpace(items.DeductibleCode))
                {
                    if (items.IsGroup == 0)
                    {
                        if (string.IsNullOrWhiteSpace(items.CoverageID) && items.ProductCode.Trim().ToUpper() == ProductCode.Trim().ToUpper() && items.InterestID.Trim().ToUpper() == InterestID.Trim().ToUpper())
                        {
                            deductibleCode = items.DeductibleCode;
                            break;
                        }
                        else if (items.ProductCode.Trim().ToUpper() == ProductCode.Trim().ToUpper() && items.InterestID.Trim().ToUpper() == InterestID.Trim().ToUpper() && items.CoverageID.Trim().ToUpper() == CoverageID.Trim().ToUpper())
                        {
                            deductibleCode = items.DeductibleCode;
                            break;
                        }
                    }
                    else if (items.IsGroup == 1)
                    {
                        foreach (var member in items.GroupMember)
                        {
                            if (string.IsNullOrWhiteSpace(items.CoverageID) && items.ProductCode.Trim().ToUpper() == ProductCode.Trim().ToUpper() && member.Trim().ToUpper() == InterestID.Trim().ToUpper())
                            {
                                deductibleCode = items.DeductibleCode;
                                break;
                            }
                            else if (items.ProductCode.Trim().ToUpper() == ProductCode.Trim().ToUpper() && member.Trim().ToUpper() == CoverageID.Trim().ToUpper())
                            {
                                deductibleCode = items.DeductibleCode;
                                break;
                            }
                        }
                        if (!string.IsNullOrWhiteSpace(deductibleCode))
                        {
                            break;
                        }
                    }
                }
            }
            return deductibleCode;
        }

        #endregion

        #region InstallmentInfo
        private static List<InstallmentInfo> GenerateInstallmentInfo(string OrderNo, string ProductCode, string TermCode, DateTime PeriodFrom, int GracePeriod, decimal PolicyFee, decimal StampDuty)
        {
            PolicyRepository pr = new PolicyRepository();
            List<InstallmentInfo> Result = new List<InstallmentInfo>();
            List<Installment> Installment = pr.GetInstallment(ProductCode, TermCode);
            for (int i = 0; i < Installment.Count; i++)
            {
                DateTime DueDate = new DateTime();
                DueDate = DateTime.Now.Date < PeriodFrom ? PeriodFrom : DateTime.Now.Date;
                InstallmentInfo InstallmentInfo = new InstallmentInfo();
                if (i == 0)
                {
                    InstallmentInfo.DueDate = DueDate.AddDays(GracePeriod + Installment[i].DayNo);
                    InstallmentInfo.PolicyFee = PolicyFee;
                    InstallmentInfo.StampDuty = StampDuty;
                }
                else
                {
                    InstallmentInfo.DueDate = DueDate.AddDays(Installment[i].DayNo);
                    InstallmentInfo.PolicyFee = 0;
                    InstallmentInfo.StampDuty = 0;
                }
                InstallmentInfo.OrderNo = IsNA(OrderNo);
                InstallmentInfo.InstallNo = i + 1;
                InstallmentInfo.FeeCurrency = IsNA("IDR");
                InstallmentInfo.InstallPct = Installment[i].InstallPercentage;
                InstallmentInfo.InterestPct = Installment[i].InterestPercentage;
                InstallmentInfo.CommPct = Installment[i].InstallPercentage;
                InstallmentInfo.PaymentMode = CommonRepository.GetGlobalParameter("RETAIL.DEFAULTPAYMENTMODE");
                Result.Add(InstallmentInfo);
            }

            return Result;
        }
        #endregion

        #region Set Order
        
        public static Order SetOrderData(string followUpNo)
        {
            string CurrentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                string UserEntry = CommonLogic.GetSettingUserID();
                //WTOrderDetailData OS = null; 
                Order O = new Order();
                O.ObjectData = new List<ObjectOrderData>();
                O.ObjectDetailData = new List<ObjectOrderDetailData>();
                O.ScoringData = new List<ScoringData>();
                O.InterestData = new List<InterestData>();
                O.CoverageData = new List<CoverageData>();
                O.SurveyInfo = new List<SurveyInfo>();
                O.InsuredData = new List<InsuredData>();
                O.AddressDetail = new List<AddressDetail>();
                O.PersonalProspect = new PersonalCustomer();
                O.PolicyClause = new List<PolicyClause>();
                O.ObjectClause = new List<ObjectClause>();
                O.TransactionAccount = new List<TransactionAccount>();
                O.DocumentData = new List<DocumentData>();
                O.CoverCommision = new List<CoverCommision>();
                O.InstallmentInfo = new List<InstallmentInfo>();
                O.InterestItemData = new List<InterestItemData>();
                O.IndirectInsurance = new IndirectInsurance();
                O.IndirectMemberList = new List<IndirectMember>();
                O.LinkOrderAgency = new LinkOrderAgency();
                O.SurveyImageList = new List<SurveyImage>();
                O.NoCoverList = new List<NoCoverOrdData>();
                O.OriginalDefectList = new List<OriginalDefectOrdData>();
                O.NonStandardAccessoriesList = new List<NonStandardAccessoriesOrdData>();
                O.SurveyOrder = new List<SurveyOrder>();

                PolicyRepository PR = new PolicyRepository();
                List<WTCommission> CommissionList = new List<WTCommission>();
                List<WTCommission> DiscountList = new List<WTCommission>();
                decimal pf = 0;
                decimal sd = 0;

                OtosalesOrderRepository OOR = new OtosalesOrderRepository();
                OrderRepository OR = new OrderRepository();
                var dtl = OOR.GetDetailOrderSimulation(followUpNo);

                Product P = PR.GetProductDetail(dtl.Ord[0].ProductCode);
                CommissionList = OR.GetWTCommission(dtl.Ord[0].ProductCode, null);
                DiscountList = OR.GetWTDiscount(dtl.Ord[0].QuotationNo, dtl.Ord[0].ProductCode, null);
                if (!string.IsNullOrWhiteSpace(dtl.Ord[0].OldPolicyNo))
                {
                    var RenDiscount = OrderRepository.GetRenewalDiscount(dtl.Ord[0].OldPolicyNo);
                    DiscountList.AddRange(RenDiscount);
                }
                decimal TotalPremium = 0;
                DateTime? PeriodFrom = null;
                foreach (var io in dtl.Interest)
                {
                    TotalPremium += io.Premium;
                    if (PeriodFrom == null || PeriodFrom > io.PeriodFrom)
                        PeriodFrom = io.PeriodFrom;
                }
                var policyFee = PR.GetPolicyFee(dtl.Ord[0].ProductCode, TotalPremium);
                if (policyFee.Count > 0)
                {
                    pf = policyFee[0].PolicyAdminFee;
                    sd = policyFee[0].StampDuty;
                }

                if (P != null)
                {
                    //var Insured = OS.WTInsured;
                    //var ObjectData = OS.WTObjectData;
                    //var MV = OS.WTMVData;
                    //var Interest = OS.WTDtlInterest;
                    //var Coverage = OS.WTDtlCoverage;
                    //var NoCover = OS.WTNoCover;
                    //var OriginalDefect = OS.WTOriginalDefect;
                    //var NonStandardAccessories = OS.WTNonStandardAccessories;
                    #region OrderData


                    var OrderExInfo = OR.GetOrderExInfo(dtl.Ord[0].PolicyOrderNo);
                    string PolicyNo = string.Empty;
                    string PolicyID = string.Empty;
                    if (OrderExInfo == null)
                    {
                        PolicyNo = CommonLogic.GetPolicyNo();
                        PolicyID = CommonLogic.GetPolicyId();
                    }
                    else
                    {
                        PolicyNo = OrderExInfo.PolicyNo;
                        PolicyID = OrderExInfo.PolicyID;
                    }

                    string SurveyNo = string.Empty;
                    string salesmanId = string.Empty;
                    string branchId = string.Empty;
                    bool isAgency = OOR.GetIsAgency(dtl.Ord[0].SalesOfficerID);
                    if (isAgency)
                    {
                        dynamic salesman = OOR.GetSalesman(dtl.Ord[0].SalesOfficerID, dtl.Ord[0].ProductCode, dtl.Ord[0].BranchID);
                        salesmanId = salesman.salesmanId;
                        branchId = salesman.branchId;
                    }
                    else {
                        salesmanId = IsNA(OOR.GetSalesmanId(dtl.Ord[0].SalesOfficerID, dtl.Ord[0].ProductCode));
                        branchId = IsNA(OOR.GetBranchId(dtl.Ord[0].SalesOfficerID, dtl.Ord[0].ProductCode, dtl.Ord[0].BranchID));
                    }
                    int CalCMethodOrd = IsNA(OOR.GetCalcMethodOrd(dtl.Ord[0].OrderNo));
                    DateTime TimeNow = OOR.GetCurrentServerTime();
                    DateTime? PerFrom = null;
                    DateTime? PerTo = null;
                    if (dtl.Ord[0].IsRenewal)
                    {
                        List<dynamic> dates = OOR.GetPeriodFromToRenew(dtl.Ord[0].OldPolicyNo);
                        if (dates.First().PeriodFrom != null && dates.First().PeriodTo != null)
                        {
                            PerFrom = dates.First().PeriodFrom;
                            PerTo = dates.First().PeriodTo;
                        }
                        else
                        {
                            PerFrom = dtl.Ord[0].PeriodFrom;
                            PerTo = dtl.Ord[0].PeriodTo;
                        }
                    }
                    else
                    {
                        PerFrom = dtl.Ord[0].PeriodFrom;
                        PerTo = dtl.Ord[0].PeriodTo;
                    }
                    string VANo = OOR.GetVANumber(dtl.Ord[0].OrderNo, IsNA(dtl.Ord[0].VANumber));
                    O.OrderData = new OrderData
                    {
                        OrderNo = IsNA(dtl.Ord[0].PolicyOrderNo),
                        PolicyNo = IsNA(dtl.Ord[0].PolicyNo),
                        PolicyId = IsNA(dtl.Ord[0].PolicyID),
                        OrderType = IsNA(dtl.Ord[0].IsRenewal) ? OrderType.Renew : OrderType.New,
                        ProductCode = IsNA(dtl.Ord[0].ProductCode),
                        //MOUID = IsNA(OS.WTOrder.MouID), 
                        COBId = IsNA(P.COBId),
                        GracePeriod = IsNA(P.GracePeriod),
                        SalesmanId = salesmanId,//IsNA(OS.WTOrder.SalesmanID), -- ini untuk catat produksi, kalo konve / syariah 
                        MarketingDate = IsNA(TimeNow),
                        EffectiveDate = ((DateTime)PeriodFrom).Date,
                        BranchId = branchId,
                        EntryUsr = IsNA(UserEntry),
                        NameOnCard = IsNA(dtl.Ord[0].Name).ToUpper(),
                        NameOnPolicy = dtl.Ord[0].isCompany ? IsNA(dtl.Ord[0].CompanyName).ToUpper() : IsNA(dtl.Ord[0].Name).ToUpper(),
                        SurveyNo = IsNA(SurveyNo),
                        VANumber = VANo,
                        //PreOrderedVANumber = OrderExInfo == null ? IsNA(OS.WTOrder.PreOrderedVANumber) : OrderExInfo.VANo,
                        FeeCurrency = IsNA("IDR"),
                        TermCode = IsNA("TUNAI"),
                        Status = IsNA("A"),
                        OrderStatus = IsNA("50"),//info ke WIA kalo get order acceptance pake yang orderstatus = 50 bukan 99 lagi.
                        FormCode = IsNA("F1"),
                        AcceptanceId = IsNA("DIRECT"),
                        OrderDate = IsNA(TimeNow),
                        PolicyFee = IsNA(pf),
                        OurSharePct = IsNA(decimal.Parse("100")), //OurSharePct : tergantung acceptancenya, kalo direct 100%
                        SharePct = IsNA(decimal.Parse("100")), //SharePct : tergantung acceptancenya, kalo direct 100%
                        SegmentCode = IsNA(dtl.Ord[0].SegmentCode),
                        PeriodFrom = PerFrom != null ? Convert.ToDateTime(PerFrom).Date : DateTime.MinValue,
                        PeriodTo = PerTo != null ? Convert.ToDateTime(PerTo).Date : DateTime.MinValue,
                        TransactionDate = DateTime.Now,
                        FeePayment = "1",
                        BrokerCode = ((string)IsNA(dtl.Ord[0].SalesOfficerID)).Length == 3 ?
                        (IsNA(dtl.Ord[0].SalesDealer) != null && IsNA(dtl.Ord[0].SalesDealer) != "" ?
                        Convert.ToString(dtl.Ord[0].SalesDealer) : (IsNA(dtl.Ord[0].DealerCode) != null && IsNA(dtl.Ord[0].DealerCode) != "") ?
                        dtl.Ord[0].DealerCode : null) : Convert.ToString(dtl.Ord[0].SalesOfficerID), //IsNA(OS.WTOrder.AgentID) == "" ? (IsNA(OS.WTOrder.SalesmanDealerID) != "" ? OS.WTOrder.SalesmanDealerID : OS.WTOrder.DealerID) : OS.WTOrder.AgentID, 
                        StampDuty = sd,
                        FinInstitutionCode = "", //hanya dipakai untuk B2B - paytoparty:  2 B2B
                        ProspectId = IsNA(dtl.Ord[0].ProspectID),
                        CustId = dtl.Ord[0].IsRenewal ? dtl.Ord[0].CustIDAAB : "",// kosong kalo masih prospect OrderRepository.IsCustomer(OS.WTOrder.CustomerID) ? OS.WTOrder.CustomerID : string.Empty,
                        PayerCode = !string.IsNullOrEmpty(dtl.Ord[0].CustIDAAB) ? dtl.Ord[0].CustIDAAB : IsNA(dtl.Ord[0].ProspectID),//OrderRepository.IsCustomer(OS.WTOrder.PayerCode) ? OS.WTOrder.PayerCode : string.Empty,
                        PolicyHolderCode = dtl.Ord[0].IsRenewal ? dtl.Ord[0].CustIDAAB : IsNA(dtl.Ord[0].ProspectID), //OrderRepository.IsCustomer(OS.WTOrder.PolicyHolderCode) ? OS.WTOrder.PolicyHolderCode : string.Empty,
                        RL1 = IsNA(P.RemainderLetter1), // dari mst_product
                        RL2 = IsNA(P.RemainderLetter2),
                        TitanOrderNo = P.ProductType == 6 ? CommonRepository.GetTitanOrderNo() : 0,
                        OldPolicyNo = IsNA(dtl.Ord[0].OldPolicyNo),
                        //CalcMethod = dtl.Ord[0].YearCvg < 1 ? CalculationMethodEnum.Scale : CalculationMethodEnum.Prorate, //Coverage[j].CalcMethod == 1 ? CalculationMethodEnum.Scale : CalculationMethodEnum.Prorate, // kalo kurang dari setahun selalu scale, kalo >=1thn prorate
                        CalcMethod = CalCMethodOrd == 0 ? CalculationMethodEnum.Prorate : CalculationMethodEnum.Scale
                    };
                    #endregion

                    List<WTOrderPolicyClause> PolicyClauseList = new List<WTOrderPolicyClause>();
                    List<string> PolicyClauseListString = new List<string>();
                    for (int h = 0; h < dtl.MV.Count; h++)
                    {
                        #region ObjectOrderData
                        var VehicleObject = OR.GetVehicleDescription(dtl.MV[h].VehicleCode);
                        string VehicleDescription = VehicleObject != null ? string.Format("{0}{1}", VehicleObject.VehicleDescription, dtl.MV[h].RegistrationNumber) : dtl.MV[h].VehicleDescription;

                        ObjectOrderData Obj = new ObjectOrderData()
                        {
                            OrderNo = IsNA(dtl.Ord[0].PolicyOrderNo),
                            ObjectNo = Convert.ToInt32(dtl.MV[h].ObjectNo), //IsNA(ObjectData[h].ObjectNo), //single object mulai dari 1
                            RIStatus = IsNA("D"), // D : gatau kenapa D
                            Status = IsNA("A"),  //IsNA(ObjectData[h].Status), A : Aktif
                            Description = IsNA(VehicleDescription),// IsNA(dtl.ObjData[h].Description), // desc vehicle
                            PeriodFrom = dtl.MV[h].PeriodFrom.Date, // period polis
                            PeriodTo = dtl.MV[h].PeriodTo.Date,
                            EntryUsr = IsNA(UserEntry),
                            //Coinsurance - sementara belum di support karena baru akan di define di sprint 18 re-engineering retail
                            BCCoinsuranceIn = 0, //IsNA(ObjectData[h].BCCoinsuranceIn),// 0 : default
                            BCCoinsuranceOut = 0, //IsNA(ObjectData[h].BCCoinsuranceOut),// 0 : default
                            BCFacultativeIn = 0, //IsNA(ObjectData[h].BCFacultativeIn),// 0 : default
                            BCOriginalSI = 0, //IsNA(ObjectData[h].BCOriginalSI),// 0 : default
                            BCOwnRetention = 0, //IsNA(ObjectData[h].BCOwnRetention), // 0 : default
                            BCSumInsured = 0, //IsNA(ObjectData[h].BCSumInsured),// 0 : default
                            BCTotalTreaty = 0, //IsNA(ObjectData[h].BCTotalTreaty), // null
                            BCTotalFacultative = 0, //IsNA(ObjectData[h].BCTotalFacultative), // 0 : default
                            StandardReferenceId = 0, //IsNA(ObjectData[h].StandardReferenceId), // null
                                                     //SurveyStatus = IsNA(ObjectData[h].SurveyStatus) // ga disini, tapi dari otosurvey
                        };
                        O.ObjectData.Add(Obj);

                        #endregion
                        #region Object Order Detail Data
                        decimal marketprice = OOR.GetVehiclePrice(dtl.MV[h].MarketPrice, dtl.MV[h].VehicleCode, dtl.MV[h].Year, dtl.MV[h].CityCode);
                        ObjectOrderDetailData ObjDetail = new ObjectOrderDetailData()
                        {
                            OrderNo = IsNA(dtl.Ord[0].PolicyOrderNo),
                            ObjectNo = Convert.ToInt32(dtl.MV[h].ObjectNo),
                            VehicleCode = IsNA(dtl.MV[h].VehicleCode).ToUpper(),
                            VehicleType = IsNA(dtl.MV[h].Type).ToUpper(),
                            Series = IsNA(dtl.MV[h].Series).ToUpper(),
                            UsageCode = IsNA(dtl.MV[h].UsageCode),
                            SittingCapacity = Convert.ToInt32(dtl.MV[h].Sitting),
                            MarketPrice = marketprice, //vehicleprice, sebelum di edit
                            MfgYear = Convert.ToInt32(dtl.MV[h].Year),
                            ModelCode = IsNA(dtl.MV[h].ModelCode).ToUpper(),
                            BrandCode = IsNA(dtl.MV[h].BrandCode).ToUpper(),
                            ChasisNumber = IsNA(dtl.MV[h].ChasisNumber),
                            EngineNumber = IsNA(dtl.MV[h].EngineNumber),
                            RegistrationNumber = IsNA(dtl.MV[h].RegistrationNumber),
                            ColorOnBPKB = IsNA(dtl.MV[h].ColorOnBPKB),
                            Color = "COLC00", // DEFAULT karena tidak ada entry
                            NewCar = IsNA(dtl.MV[h].IsNew),
                            GeoAreaCode = IsNA(dtl.MV[h].CityCode), //IsNA(dtl.MV[h].GeoAreaCode),
                            EntryUsr = IsNA(UserEntry),
                            CurrId = IsNA("IDR"),
                            ClientNo = "", //IsNA(MV[h].ClientNo), // kosong. kalo B2B aja
                            ContractNo = "", //IsNA(MV[h].ContractNo), // kosong. kalo B2B aja
                            Kilometers = 0, //IsNA(MV[h].Kilometers), // kosong. 
                            LightColor = false, //IsNA(MV[h].LightColor), // kosong. 
                            TransmissionType = 0,//IsNA(MV[h].TransmissionType), // kosong. 
                            Metalic = false //IsNA(MV[h].Metalic), // kosong. 
                        };
                        O.ObjectDetailData.Add(ObjDetail);

                        #endregion
                        #region  Set Survey Info Data
                        //if (dtl.MV[h].SkipSurveyF == 0) 
                        if (dtl.MV[h].IsNeedSurvey && !dtl.MV[h].IsNSASkipSurvey)
                        {
                            SurveyNo = CommonLogic.GetSurveyNo();
                            OOR.UpdateOrderSimulationSurveyNo(SurveyNo, dtl.Ord[0].PolicyOrderNo);
                            #region Pengecekan Additional Survey Acq, jika dapet additional surveynya, disini set survey dgn status complete.
                            // untuk otosales caranya ga pake additional Survey acq, karena itu code ini tidak digunakan.

                            /*
                            var SvyNo = SurveyNo;
                            if (svyNoCheck)
                            {
                                SvyNo = CommonLogic.GetSurveyNo();
                            }

                            //var surveyAdd = PR.GetSurveyResult(dtl.MV[h].ChasisNumber, dtl.MV[h].EngineNumber); // pengecekan kalo sudah ada surveyadditional, maka bentukin survey dgn status done.
                            if (surveyAdd.Count > 0)
                            {
                                SurveyInfo Survey = new SurveyInfo();

                                Survey.SurveyNo = SvyNo;
                                svyNoCheck = true;
                                Survey.Status = 7;
                                Survey.SurveyType = "SVPLCY";
                                Survey.FormId = "NEWPLC";
                                Survey.ObjectNo = IsNA(MV[h].ObjectNo);

                                Survey.ReferenceNo = IsNA(OS.WTOrder.OrderNo);

                                Survey.Surveyor = surveyAdd[0].SurveyorID;
                                Survey.SurveyDate = DateTime.ParseExact(surveyAdd[0].SurveyDate, DateFormatCustom.Format2, CultureInfo.InvariantCulture);

                                Survey.PicName = surveyAdd[0].PICName;
                                Survey.PhoneNo = surveyAdd[0].PhoneNumber;
                                Survey.Location = IsNA(surveyAdd[0].SurveyLocation);
                                Survey.remarks = IsNA(surveyAdd[0].Detail);
                                Survey.AreaCode = IsNA(surveyAdd[0].AreaCode);

                                Survey.AdditionalOrderAcquisitionSurveyID = IsNA(surveyAdd[0].AdditionalOrderAcquisitionSurveyID);

                                var nationalAreaCode = CommonLogic.GetSettingAreaCodeNasional();// ConfigurationManager.AppSettings["AreaCodeNasional"];

                                if (surveyAdd[0].AreaCode == nationalAreaCode)
                                {
                                    //Survey Dalam
                                    Survey.SurveyMode = "D";
                                }
                                else
                                {
                                    Survey.SurveyMode = "L";
                                }
                                O.SurveyInfo.Add(Survey);

                                var img = ImageLogic.GetTransferImageSurvey(MV[h].ChasisNumber, MV[h].EngineNumber);
                                if (img.Count > 0)
                                {
                                    foreach (var ii in img)
                                    {
                                        SurveyImage s = new SurveyImage();
                                        s.SurveyNo = SvyNo;
                                        s.FileName = ii.FileName;
                                        s.ImageType = ii.ImageType;

                                        O.SurveyImageList.Add(s);
                                    }
                                }

                            } 
                            */
                            #endregion

                            #region Booking Survey

                            dtl.Ord[0].SurveyNo = SurveyNo;
                            SurveyInfo Survey = new SurveyInfo();
                            BookingSurvey BookSurvey = new BookingSurvey();
                            Survey.SurveyNo = SurveyNo;
                            Survey.Status = 1;
                            Survey.SurveyType = "SVPLCY"; //IsNA(Dev.SurveyType);
                            Survey.FormId = "NEWPLC"; //IsNA(Dev.SurveyFormID);
                            Survey.ObjectNo = Convert.ToInt32(dtl.MV[h].ObjectNo);
                            Survey.PicName = IsNA(dtl.Ord[0].Name).ToUpper();
                            Survey.PhoneNo = IsNA(!string.IsNullOrWhiteSpace(dtl.Ord[0].Phone1) ? dtl.Ord[0].Phone1 : dtl.Ord[0].Phone2);
                            Survey.ReferenceNo = IsNA(dtl.Ord[0].PolicyOrderNo);
                            Survey.City = dtl.Survey[0].CityName; //AsuransiAstraAccess.GetCity(OS.SurveyCityID.ToString());
                            Survey.Location = IsNA(dtl.Survey[0].SurveyAddress);
                            SurveyOrder surveyOrd = new SurveyOrder();
                            surveyOrd.ReferenceTypeCode = "REFTYPE3";
                            surveyOrd.ReferenceNo = SurveyNo;
                            surveyOrd.SurveyDate = dtl.OSsurvey[0].SurveyDate;
                            var GATime = OOR.GetTimeCategoryCode(Convert.ToString(dtl.Survey[0].ScheduleTimeID));
                            string ScheduledTime = GATime != null ? GATime.ScheduleTime : string.Empty;
                            surveyOrd.SurveyTime = ScheduledTime;
                            if (dtl.Ord[0].IsRenewal)
                            {
                                surveyOrd.SurveyType = "SVPREN";
                            }
                            else
                            {
                                surveyOrd.SurveyType = "SVPLCY";
                            }
                            surveyOrd.ApplicationSourceCode = "APPSC7";
                            surveyOrd.CustomerOnLocation = dtl.Ord[0].Name;
                            surveyOrd.PhoneNoOnLocation = dtl.Ord[0].Phone1;
                            surveyOrd.SurveyAddress = dtl.OSsurvey[0].SurveyAddress;
                            surveyOrd.WorkshopCode = "";
                            surveyOrd.OrderNo = dtl.Ord[0].PolicyOrderNo;
                            surveyOrd.LKNo = "";
                            surveyOrd.NameOnPolicy = dtl.Ord[0].Name;
                            surveyOrd.CreatedBy = UserID;
                            surveyOrd.ChassisNo = dtl.MV[0].ChasisNumber;
                            surveyOrd.EngineNo = dtl.MV[0].EngineNumber;
                            if (string.IsNullOrWhiteSpace(dtl.Survey[0].SurveyPostalCode))
                            {
                                #region Survey Dalam

                                //Survey Dalam

                                DateTime time = Convert.ToDateTime(ScheduledTime);
                                DateTime sFromDt = new DateTime();
                                if (!string.IsNullOrWhiteSpace(ScheduledTime))
                                {
                                    sFromDt = new DateTime(dtl.Survey[0].SurveyDate.Year, dtl.Survey[0].SurveyDate.Month, dtl.Survey[0].SurveyDate.Day, time.Hour, time.Minute, 0);
                                    Survey.AreaCode = IsNA(CommonLogic.GetSettingNasionalGeoAreaCode());
                                    Survey.SubGeoAreaCode = IsNA(CommonLogic.GetSettingNasionalSubGeoAreaCode());
                                    surveyOrd.AreaCode = Survey.AreaCode;
                                    Survey.RegionCode = IsNA(CommonLogic.GetSettingNasionalRegionCode());
                                    Survey.SurveyMode = "D";
                                    surveyOrd.SurveyMode = Survey.SurveyMode;
                                    // DateTime SurveyDate = new DateTime(OS.CreatedDate.Year, OS.CreatedDate.Month, OS.CreatedDate.Day, 10, 0, 0);
                                    Survey.SurveyDate = sFromDt;
                                    Survey.PostalCode = IsNA(CommonLogic.GetSettingSurveyDalamZipCode());
                                    Survey.FromDate = sFromDt;// Survey.SurveyDate;
                                    Survey.ToDate = sFromDt.AddHours(2);//Survey.SurveyDate;
                                }
                                else
                                {
                                    Survey.FromDate = Survey.SurveyDate;
                                    Survey.ToDate = Survey.SurveyDate;
                                }
                                #endregion
                            }
                            else
                            {
                                #region Survey Luar

                                //Survey Luar 
                                BookSurvey.TransactionID = SurveyNo;
                                BookSurvey.SurveTimeCode = GATime != null ? GATime.TimeCategoryCode : string.Empty;

                                //SurveyAddress sa = OOR.GetSurveyAddress(dtl.Survey[0].SurveyPostalCode);
                                dynamic sa = OOR.GetGeoAreaCode(dtl.Survey[0].SurveyPostalCode);
                                if (sa != null)
                                {
                                    SurveyRegionCode SRC = OOR.GetSurveyRegionCode(sa.GeoAreaCode);
                                    if (SRC != null)
                                    {
                                        Survey.SubGeoAreaCode = SRC.SubGeoAreaCode;
                                        Survey.RegionCode = SRC.RegionCode;
                                    }
                                    Survey.AreaCode = sa.GeoAreaCode;
                                    surveyOrd.AreaCode = Survey.AreaCode;
                                    Survey.SurveyMode = "L";
                                    surveyOrd.SurveyMode = Survey.SurveyMode;
                                    //Survey.SurveyDate = IsValidDate(OS.SurveyScheduleDate);
                                    Survey.PostalCode = IsNA(dtl.Survey[0].SurveyPostalCode);

                                    BookSurvey.SurveyAreaCode = OOR.GetGASurveyAreaCode(sa.GeoAreaCode);
                                }
                                if (!string.IsNullOrWhiteSpace(ScheduledTime))
                                {
                                    var time = TimeSpan.Parse(ScheduledTime);
                                    DateTime sFromDt = new DateTime(dtl.Survey[0].SurveyDate.Year, dtl.Survey[0].SurveyDate.Month, dtl.Survey[0].SurveyDate.Day, time.Hours, time.Minutes, 0);
                                    Survey.SurveyDate = sFromDt;
                                    Survey.FromDate = sFromDt;
                                    Survey.ToDate = sFromDt.AddHours(2);
                                    BookSurvey.SurveyDate = sFromDt;

                                }
                                else
                                {
                                    Survey.FromDate = Survey.SurveyDate;
                                    Survey.ToDate = Survey.SurveyDate;
                                }
                                BookSurvey.Actor = UserID;
                                //O.BookingSurvey.Add(BookSurvey);
                                OOR.InsertBookingSurvey(BookSurvey);

                                #endregion
                            }
                            O.SurveyInfo.Add(Survey);
                            O.SurveyOrder.Add(surveyOrd);
                            //Insert Mst_Order_Mobile
                            #endregion

                        }
                        #endregion
                        #region Set Scoring Factor

                        List<string> ScoringFactor = new List<string> {
                        "GEOGRA","MVUSAG","NEWVHC", "VBRAND", "VHCTYP", "VMODEL", "YEARNO"
                    };
                        foreach (var items in ScoringFactor)
                        {
                            ScoringData SC = new ScoringData();
                            SC.OrderNo = dtl.Ord[0].PolicyOrderNo;
                            SC.ObjectNo = Convert.ToInt32(dtl.MV[h].ObjectNo);
                            SC.FactorCode = items;
                            SC.EntryUsr = UserEntry;
                            switch (items)
                            {
                                case InsFactor.VehicleBrand: SC.InsuranceCode = dtl.MV[h].BrandCode; break;
                                case InsFactor.VehicleModel: SC.InsuranceCode = dtl.MV[h].ModelCode; break;
                                case InsFactor.VehicleType: SC.InsuranceCode = dtl.MV[h].Type; break;
                                case InsFactor.NewVehicle: SC.InsuranceCode = dtl.MV[h].IsNew ? MVNewStatus.New : MVNewStatus.Old; break;
                                case InsFactor.MVUsage: SC.InsuranceCode = dtl.MV[h].UsageCode; break;
                                case InsFactor.GeoArea:
                                    SC.InsuranceCode = dtl.MV[h].CityCode;
                                    //var Rslt = PR.GetAABCodeGeo(dtl.MV[h].GeoAreaCode);
                                    //if (Rslt != null) { SC.InsuranceCode = IsNA(Rslt.Code); };
                                    break;

                            }
                            if (!string.IsNullOrWhiteSpace(SC.InsuranceCode))
                            {
                                O.ScoringData.Add(SC);
                            }
                        }
                        #endregion
                        #region Set NoCover
                        // untuk polis new ga di insert, karena dari otosurvey (kalo ada survey, kalo ga ada survey berarti kosong jg)
                        /*
                        var strDescList = new List<string>();
                        foreach (var item in NoCover)
                        {
                            if (ObjectData[h].ObjectNo == item.ObjectNo)
                            {
                                if (item.Type.ToUpper().TrimEnd() == "PART")
                                    strDescList.Add(item.Name + ' ' + item.Description + ' ' + (item.Quantity ?? 0) + " No Cover Part");
                                else if (item.Type.ToUpper().TrimEnd() == "ACCESSORIES")
                                    strDescList.Add(item.Name + ' ' + item.Description + ' ' + (item.Quantity ?? 0) + " No Cover Acc");
                                NoCoverOrdData nCOD = new NoCoverOrdData();
                                nCOD.OrderNo = IsNA(OS.WTOrder.OrderNo);
                                nCOD.ObjectNo = IsNA(item.ObjectNo);
                                nCOD.EndorsementNo = IsNA(item.EndorsementNo);
                                nCOD.Type = IsNA(item.Type);
                                nCOD.NoCoverID = IsNA(item.NoCoverID);
                                nCOD.Name = IsNA(item.Name);
                                nCOD.DemageType = IsNA(item.DemageTypeCode);
                                nCOD.Description = IsNA(item.Description);
                                nCOD.Quantity = item.Quantity ?? 0;

                                O.NoCoverList.Add(nCOD);
                            }
                        }
                        var strDesc = string.Join("; ", strDescList);
                        */
                        #endregion
                        #region Set Original Defect
                        // untuk polis new ga di insert, karena dari otosurvey (kalo ada survey, kalo ga ada survey berarti kosong jg)
                        /*
                        foreach (var item in OriginalDefect)
                        {
                            if (ObjectData[h].ObjectNo == item.ObjectNo)
                            {
                                OriginalDefectOrdData oDOD = new OriginalDefectOrdData();
                                oDOD.OrderNo = IsNA(OS.WTOrder.OrderNo);
                                oDOD.ObjectNo = IsNA(item.ObjectNo);
                                oDOD.EndorsementNo = IsNA(item.EndorsementNo);
                                oDOD.PartID = IsNA(item.PartID);
                                oDOD.Name = IsNA(item.Name);
                                oDOD.DamageCategory = IsNA(item.DamageCategory);
                                oDOD.Description = IsNA(item.Description);

                                O.OriginalDefectList.Add(oDOD);
                            }
                        }
                        */
                        #endregion
                        #region Set Non StandardAccessories
                        // untuk polis new ga di insert, karena dari otosurvey (kalo ada survey, kalo ga ada survey berarti kosong jg)
                        /*
                        foreach (var item in NonStandardAccessories)
                        {
                            if (ObjectData[h].ObjectNo == item.ObjectNo)
                            {
                                NonStandardAccessoriesOrdData nos = new NonStandardAccessoriesOrdData();
                                nos.OrderNo = IsNA(OS.WTOrder.OrderNo);
                                nos.ObjectNo = IsNA(item.ObjectNo);
                                nos.EndorsementNo = IsNA(item.EndorsementNo);
                                nos.AccsCode = IsNA(item.AccsCode);
                                nos.AccsPartCode = IsNA(item.AccsPartCode);
                                nos.IncludeTsi = IsNA(item.IncludeTsi);
                                nos.Brand = IsNA(item.Brand);
                                nos.SumInsured = IsNA(item.SumInsured);
                                nos.Quantity = IsNA(item.Quantity);
                                nos.Premi = IsNA(item.Premi);
                                nos.Category = IsNA(item.Category);
                                nos.AccsCatCode = IsNA(item.AccsCatCode);

                                O.NonStandardAccessoriesList.Add(nos);
                            }
                        }
                        */
                        #endregion
                        #region Set Interest

                        for (int i = 0; i < dtl.Interest.Count; i++)
                        {
                            if (dtl.MV[h].ObjectNo == dtl.Interest[i].ObjectNo)
                            {
                                #region Automatic Deductible Clause Triggered By Product - Coverage
                                List<ProductInterestCoverageDeductibleClasue> ClauseDeductible = new List<ProductInterestCoverageDeductibleClasue>();
                                OrderLogic ol = new OrderLogic();
                                #region Set CoverageIDGeneric & InterestIDGeneric

                                foreach (var ic in dtl.Coverage)
                                {
                                    ic.CoverageIDGeneric = OOR.GetGenericCoverageID(ic.CoverageId);
                                    ic.InterestIDGeneric = OOR.GetGenericInterestID(ic.InterestID);
                                }
                                foreach (var ic2 in dtl.coverage2)
                                {
                                    ic2.CoverageIDGeneric = OOR.GetGenericCoverageID(ic2.CoverageId);
                                    ic2.InterestIDGeneric = OOR.GetGenericInterestID(ic2.InterestID);
                                }

                                #endregion
                                List<dynamic> tempCoverage = new List<dynamic>();
                                tempCoverage = dtl.coverage2;
                                ClauseDeductible = OR.GetDeductibleClause(tempCoverage, dtl.Ord[0].ProductCode);
                                if (ClauseDeductible.Count > 0)
                                {
                                    //Add Clause
                                    foreach (var items in ClauseDeductible)
                                    {
                                        if (items != null)
                                        {
                                            WTOrderPolicyClause PClause = new WTOrderPolicyClause()
                                            {
                                                QuotationNo = dtl.Ord[0].QuotationNo,
                                                ClauseID = items.ClauseID,
                                                CreatedBy = UserEntry,
                                                IsAutomated = 1
                                            };
                                            if (!string.IsNullOrWhiteSpace(PClause.QuotationNo) && !string.IsNullOrWhiteSpace(PClause.ClauseID))
                                            {
                                                if (!PolicyClauseListString.Contains(PClause.ClauseID))
                                                {
                                                    PolicyClauseListString.Add(PClause.ClauseID);
                                                    PolicyClauseList.Add(PClause);
                                                }
                                            }
                                        }
                                    }
                                    //Add Deductible Interest Level
                                    string PrimaryInterest = string.Empty;
                                    foreach (var interest in dtl.Interest)
                                    {
                                        interest.InterestIDGeneric = OOR.GetGenericInterestID(interest.InterestID);
                                        if (interest.InterestType.Trim().ToUpper() == "P")
                                        {
                                            PrimaryInterest = interest.InterestIDGeneric;
                                        }
                                        string InterestCode = string.Empty;
                                        string DeductibleCode = string.Empty;
                                        InterestCode = interest.InterestType.Trim().ToUpper() == "A" ? PrimaryInterest : interest.InterestIDGeneric;
                                        DeductibleCode = GetDeductible(dtl.Ord[0].ProductCode, InterestCode, ClauseDeductible);
                                        if (!string.IsNullOrWhiteSpace(DeductibleCode))
                                        {
                                            ProductDeductible PD = OR.GetProductDeductible(dtl.Ord[0].ProductCode, DeductibleCode);
                                            if (PD != null)
                                            {
                                                interest.DeductibleCode = DeductibleCode;
                                                decimal FlatAmount = PD != null ? PD.FlatAmount : 0;
                                                interest.DedFlatAmount = interest.InterestType.Trim().ToUpper() == "P" ? FlatAmount : 0;
                                                interest.DedFlatCurrency = "0";
                                            }
                                        }
                                    }
                                    foreach (var coverage in dtl.Coverage)
                                    {
                                        string DeductibleCode = string.Empty;
                                        dynamic decCode = OR.GetDeductibleCode(dtl.Ord[0].ProductCode, coverage.InterestID, coverage.CoverageId);
                                        if (decCode != null)
                                        {
                                            DeductibleCode = decCode.deductiblecode;
                                            if (!string.IsNullOrWhiteSpace(DeductibleCode))
                                            {
                                                ProductDeductible PD = OR.GetProductDeductible(dtl.Ord[0].ProductCode, DeductibleCode);
                                                if (PD != null)
                                                {
                                                    coverage.DeductibleCode = DeductibleCode;
                                                    coverage.DedFlatCurrency = PD != null ? PD.FlatCurrency : string.Empty;
                                                }
                                            }
                                        }
                                    }

                                }
                                #endregion

                                InterestData Int = new InterestData()
                                {
                                    Orderno = IsNA(dtl.Ord[0].PolicyOrderNo),
                                    ObjectNo = Convert.ToInt32(dtl.Interest[i].ObjectNo),
                                    InterestNo = Convert.ToInt32(dtl.Interest[i].InterestNo), // mulai dari 1
                                    InterestCode = IsNA(dtl.Interest[i].InterestID), //casco accs, tplper
                                    InsuranceType = IsNA(dtl.Interest[i].InsuranceType), // dari mst_interest
                                    InterestType = IsNA(dtl.Interest[i].InterestType), // dari mst_interest (casco : P , acs: A, others : L)
                                    PeriodFrom = IsNA(dtl.Interest[i].PeriodFrom.Date),
                                    PeriodTo = IsNA(dtl.Interest[i].PeriodTo.Date),
                                    DeductibleCode = IsNA(dtl.Interest[i].DeductibleCode), // liat logic pas insert ke WT, logicnya pas insert ke cover
                                    DedFlatAmount = IsNA(dtl.Interest[i].DedFlatAmount), // liat logic pas insert ke WT, logicnya pas insert ke cover
                                    DedFlatCurrency = IsNA(dtl.Interest[i].DedFlatCurrency), // liat logic pas insert ke WT, logicnya pas insert ke cover
                                    ExchangeRate = IsNA(1),//IsNA(dtl.Interest[i].ExchangeRate), // 1 : karena slalu idr
                                    CurrId = IsNA("IDR"), // default IDR 
                                    EntryUsr = IsNA(UserEntry),
                                    Status = IsNA("A"),
                                    SumInsured = IsNA(dtl.Interest[i].SumInsured), // dari marketprice di mst_vehicle_price, lalu dirubah AO/SA, angka masuk ksini
                                    OriginalSI = IsNA(dtl.Interest[i].SumInsured), // samain persis sama SumInsured. confirm REL
                                    Description = ""//IsNA(dtl.Interest[i].InterestCode).Equals("CASCO") ? strDesc : "" // set dari strDesc kalo ada nocover

                                };
                                O.InterestData.Add(Int);
                                #region NonStandardAccessories
                                // skip dlu, cek ke otosurvey apakah logic dibawah ini di generate di api otosurvey
                                /*
                                if (IsNA(dtl.Interest[i].InterestType).Equals("A") && IsNA(Interest[i].InterestCode).Equals("ACCESS"))
                                {
                                    int IntItemNox = 1;
                                    foreach (var acces in NonStandardAccessories)
                                    {
                                        if (ObjectData[h].ObjectNo == acces.ObjectNo && acces.IncludeTsi == 0)
                                        {
                                            string ItemDescription = string.Format("{0} {1} {2}", IsNA(acces.AccsDescription), IsNA(acces.AccsCatDescription), IsNA(acces.AccsPartDescription));
                                            ItemDescription = System.Text.RegularExpressions.Regex.Replace(ItemDescription, @"\s+", " ");
                                            if (ItemDescription.Length > 50)
                                            {
                                                ItemDescription = ItemDescription.Substring(0, 50);
                                            }
                                            if (IsNA(acces.Brand).Length > 50)
                                            {
                                                acces.Brand = acces.Brand.Substring(0, 50);
                                            }
                                            InterestItemData iid = new InterestItemData();
                                            iid.OrderNo = IsNA(OS.WTOrder.OrderNo);
                                            iid.ObjectNo = IsNA(acces.ObjectNo);
                                            iid.InterestNo = IsNA(Interest[i].InterestNo);
                                            iid.IntItemNo = IntItemNox;
                                            iid.ItemName = ItemDescription.Trim();
                                            iid.Description = acces.Brand;
                                            iid.Capacity = acces.Quantity;
                                            iid.SumInsured = acces.SumInsured;

                                            IntItemNox++;
                                            O.InterestItemData.Add(iid);
                                        }
                                    }
                                }
                                else if (IsNA(Interest[i].InterestType).Equals("P"))
                                {
                                    int IntItemNox = 1;
                                    foreach (var acces in NonStandardAccessories)
                                    {
                                        if (ObjectData[h].ObjectNo == acces.ObjectNo && acces.IncludeTsi == 1)
                                        {
                                            string ItemDescription = string.Format("{0} {1} {2}", IsNA(acces.AccsDescription), IsNA(acces.AccsCatDescription), IsNA(acces.AccsPartDescription));
                                            ItemDescription = System.Text.RegularExpressions.Regex.Replace(ItemDescription, @"\s+", " ");
                                            if (ItemDescription.Length > 50)
                                            {
                                                ItemDescription = ItemDescription.Substring(0, 50);
                                            }
                                            if (IsNA(acces.Brand).Length > 50)
                                            {
                                                acces.Brand = acces.Brand.Substring(0, 50);
                                            }
                                            InterestItemData iid = new InterestItemData();
                                            iid.OrderNo = IsNA(OS.WTOrder.OrderNo);
                                            iid.ObjectNo = IsNA(acces.ObjectNo);
                                            iid.InterestNo = IsNA(Interest[i].InterestNo);
                                            iid.IntItemNo = IntItemNox;
                                            iid.ItemName = ItemDescription.Trim();
                                            iid.Description = acces.Brand;
                                            iid.Capacity = acces.Quantity;
                                            iid.SumInsured = 0;

                                            IntItemNox++;
                                            O.InterestItemData.Add(iid);
                                        }
                                    }
                                }
                                */
                                #endregion

                                List<CommissionScheme> CS = new List<CommissionScheme>();
                                List<CommissionScheme> DS = new List<CommissionScheme>();
                                PremiumScheme PS = new PremiumScheme();
                                for (int j = 0; j < dtl.Coverage.Count; j++)
                                {
                                    if (dtl.Interest[i].OrderNo == dtl.Coverage[j].OrderNo && dtl.Interest[i].ObjectNo == dtl.Coverage[j].ObjectNo && dtl.Interest[i].InterestNo == dtl.Coverage[j].InterestNo)
                                    {
                                        if (CS.Count == 0)
                                        {
                                            CS = CommissionLogic.InitCommisionList(dtl.Coverage[j].CoverageId, CommissionList);
                                        }
                                        if (DS.Count == 0)
                                        {
                                            DS = CommissionLogic.InitDiscountList(dtl.Coverage[j].CoverageId, DiscountList, false);
                                            if (DS.Count > 0)
                                            {
                                                DS = CommissionLogic.InitFlatDiscount(dtl.Ord[0].ProductCode, dtl.Coverage[j].GrossPremium, DS, dtl.Coverage[j].SumInsured, 1, "DISCOUNT", false);
                                            }

                                        }

                                        PS.CoverPremium = dtl.Coverage[j].CoverPremium;
                                        PS.GrossPremium = dtl.Coverage[j].GrossPremium;
                                        PS.NetPremium = dtl.Coverage[j].NetPremium;
                                        PS.Net1 = dtl.Coverage[j].Net1;
                                        PS.Net2 = dtl.Coverage[j].Net2;
                                        PS.Net3 = dtl.Coverage[j].Net3;

                                        //O.InterestData[i].SumInsured = Coverage[j].SumInsured;
                                        //O.InterestData[i].OriginalSI = Coverage[j].SumInsured;
                                        CoverageData Cov = new CoverageData()
                                        {
                                            OrderNo = IsNA(dtl.Ord[0].PolicyOrderNo),
                                            ObjectNo = Convert.ToInt32(dtl.Coverage[j].ObjectNo),
                                            InterestNo = Convert.ToInt32(dtl.Coverage[j].InterestNo),
                                            CoverageNo = Convert.ToInt32(dtl.Coverage[j].CoverageNo),
                                            InterestID = IsNA(dtl.Interest[i].InterestID),
                                            CoverageId = IsNA(dtl.Coverage[j].CoverageId),
                                            Rate = IsNA(Decimal.Round(dtl.Coverage[j].Rate, 4)),
                                            SumInsured = (IsNA(dtl.Coverage[j].SumInsured)),
                                            NetPremium = IsNA(dtl.Coverage[j].NetPremium),
                                            Net1 = IsNA(dtl.Coverage[j].Net1),
                                            Net2 = IsNA(dtl.Coverage[j].Net2),
                                            Net3 = IsNA(dtl.Coverage[j].Net2),
                                            GrossPremium = IsNA(dtl.Coverage[j].GrossPremium),
                                            CoverPremium = IsNA(dtl.Coverage[j].CoverPremium),
                                            Discount = IsNA(dtl.Coverage[j].GrossPremium - dtl.Coverage[j].NetPremium),
                                            Loading = IsNA(dtl.Coverage[j].LoadingRate),
                                            BeginDate = IsNA(dtl.Coverage[j].BeginDate.Date),
                                            EndDate = IsNA(dtl.Coverage[j].EndDate.Date),
                                            COBId = IsNA(P.COBId),
                                            CurrId = "IDR", //IsNA(Coverage[j].CurrId) == "" ? "IDR" : dt.Coverage[j].CurrId,
                                            InsuranceType = IsNA(dtl.Interest[i].InsuranceType),
                                            InterestType = IsNA(dtl.Interest[i].InterestType),
                                            ExcessRate = dtl.Coverage[j].ExcessRate,
                                            EntryUsr = IsNA(UserEntry),
                                            Status = "A",// IsNA(dtl.Coverage[j].Status),
                                                         //Ndays = IsNA((Coverage[j].EndDate - Coverage[j].BeginDate).Days),
                                            Ndays = Convert.ToDecimal(dtl.Coverage[j].Ndays), // ikutin dari generatean
                                            EntryPct = Convert.ToDecimal(dtl.Coverage[j].Entry_Pct), // ikutin dari generatean
                                            //CalcMethod = dtl.Coverage[j].DiffMonth < 12 ? CalculationMethodEnum.Scale : CalculationMethodEnum.Prorate, //Coverage[j].CalcMethod == 1 ? CalculationMethodEnum.Scale : CalculationMethodEnum.Prorate, // kalo kurang dari setahun selalu scale, kalo >=1thn prorate
                                            CalcMethod = dtl.Coverage[j].Calc_Method == 0 ? CalculationMethodEnum.Prorate : CalculationMethodEnum.Scale,
                                            Maxsi = dtl.Interest[i].SumInsured, //IsNA(dtl.Coverage[j].MaxSi), // ambil max suminsured jika multiyear. ambil dari interest aja pasti sama (original SI)
                                            AcquisitionCost = IsNA(0),//(IsNA(Coverage[j].AcquisitionCost)), // kosongin aja, hanya isi kalo cancellation
                                                                      //DEductible
                                            DeductibleCode = (IsNA(dtl.Coverage[j].DeductibleCode)),
                                            DedFlatAmount = (IsNA(dtl.Coverage[j].DedFlatAmount)),
                                            DedFlatCurrency = (IsNA(dtl.Coverage[j].DedFlatCurrency))


                                            //OriginalRate = (IsNA(Coverage[j].OriginalRate)),
                                            //CoverType = (IsNA(Coverage[j].CoverType)),
                                            //StandardReferenceId = (IsNA(Coverage[j].StandardReferenceID)),

                                        };
                                        O.CoverageData.Add(Cov);
                                        int DiscCommNo = 0;
                                        //SaveDiscountToDataPool(string OrderNo, PremiumScheme PS, int ObjectNumber, int InterestNumber, string CurrencyID, int CoverageNumber, string ActCoverageID, List < CommissionScheme > DiscountList, decimal CoverPremium, string Status)

                                        CoverCommision CDisc = CommissionLogic.SaveDiscountToDataPool(dtl.Ord[0].PolicyOrderNo, PS, (int)dtl.Coverage[j].ObjectNo, (int)dtl.Interest[i].InterestNo, "IDR", (int)dtl.Coverage[j].CoverageNo, dtl.Coverage[j].CoverageId, DS, dtl.Coverage[j].GrossPremium, dtl.Coverage[j].Status);
                                        if (CDisc != null)
                                        {
                                            if (!string.IsNullOrWhiteSpace(CDisc.CommissionCode) && !string.IsNullOrWhiteSpace(CDisc.CoverageId))
                                            {
                                                DiscCommNo = DiscCommNo + 1;
                                                CDisc.ObjectNo = dtl.Coverage[j].ObjectNo;
                                                CDisc.InterestNo = dtl.Coverage[j].InterestNo;
                                                CDisc.CoverageNo = dtl.Coverage[j].CoverageNo;
                                                CDisc.DiscCommNo = DiscCommNo;
                                                O.CoverCommision.Add(CDisc);
                                            }
                                        }
                                        List<CoverCommision> CoverComm = CommissionLogic.SaveCommissionToDataPool(dtl.Ord[0].PolicyOrderNo, PS, dtl.Ord[0].ProductCode, dtl.Coverage[j].CoverageNo, dtl.Interest[i].InterestID, dtl.Coverage[j].InterestNo, "IDR", dtl.Coverage[j].CoverageNo, dtl.Coverage[j].CoverageId, CS, dtl.Coverage[j].Status, false, dtl.Coverage[j].BeginDate, dtl.Coverage[j].EndDate, dtl.Coverage[j].Rate, dtl.Coverage[j].Rate);
                                        if (CoverComm != null)
                                        {

                                            foreach (var CComm in CoverComm)
                                            {
                                                if (!string.IsNullOrWhiteSpace(CComm.CommissionCode) && !string.IsNullOrWhiteSpace(CComm.CoverageId))
                                                {
                                                    DiscCommNo = DiscCommNo + 1;
                                                    CComm.ObjectNo = dtl.Coverage[j].ObjectNo;
                                                    CComm.InterestNo = dtl.Coverage[j].InterestNo;
                                                    CComm.CoverageNo = dtl.Coverage[j].CoverageNo;
                                                    CComm.DiscCommNo = DiscCommNo;
                                                    O.CoverCommision.Add(CComm);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        #endregion
                    }

                    CustomerRepository cs = new CustomerRepository();

                    #region InsuredData

                    int PeriodNo = 0;
                    for (int i = 0; i < dtl.Interest.Count; i++)
                    {
                        int prevObject = 1;
                        int prevInterest = 1;
                        if (i > 0)
                        {
                            prevObject = dtl.Interest[i - 1].ObjectNo;
                            prevInterest = dtl.Interest[i - 1].InterestNo;
                        }
                        if (dtl.Interest[i].ObjectNo != prevObject || dtl.Interest[i].InterestNo != prevInterest)
                        {
                            PeriodNo = 1;
                        }
                        else
                        {
                            PeriodNo = PeriodNo + 1;
                        }

                        InsuredData Ins = new InsuredData()
                        {
                            OrderNo = IsNA(dtl.Ord[0].PolicyOrderNo),
                            ObjectNo = IsNA(dtl.Interest[i].ObjectNo),
                            InterestNo = IsNA(dtl.Interest[i].InterestNo),
                            SumInsured = (IsNA(dtl.Interest[i].SumInsured)),
                            EntryUsr = IsNA(UserEntry),
                            BeginDate = IsNA(dtl.Interest[i].PeriodFrom.Date),
                            EndDate = IsNA(dtl.Interest[i].PeriodTo.Date),
                            PeriodNo = IsNA(PeriodNo),
                            Status = "A"//IsNA(dtl.Interest[i].Status)
                        };
                        O.InsuredData.Add(Ins);

                    }

                    #endregion

                    #region Inward

                    // kaitannya dgn acceptance, selain direct coinsurance out, kita jadi leadernya, membernya company lain. data2 company lainnya indirectinsurance. gausah dimasukin gapapa karena ga ada inputannya
                    /*
                    if (OS.WTInward != null)

                    {
                        var items = OS.WTInward;
                        IndirectInsurance ReAs = new IndirectInsurance()
                        {
                            AcceptanceId = items.AcceptanceId,
                            CalcMethod = items.CalcMethod,
                            CedingCoy = items.CedingCoy,
                            WPCDate = items.WPCDate,
                            FullReceived = items.FullReceived,
                            OurShareAmt = items.OurShareAmt,
                            HandlingFeePct = items.HandlingFeePct,
                            OfferingLetterDate = items.OfferingLetterDate,
                            OfferingLetterNo = items.OfferingLetterNo,
                            OrderNo = items.OrderNo,
                            OriginalRate = items.OriginalRate,
                            OurSharePct = items.OurSharePct,
                            //PeriodFrom = items.PeriodFrom,
                            //PeriodTo = items.PeriodTo,
                            RefPolicyNo = items.RefPolicyNo,
                            RISlipDate = items.RISlipDate,
                            SlipReceiveDate = items.SlipReceiveDate


                        };
                        O.IndirectInsurance = ReAs;
                    }

                    foreach (var mem in OS.WTInwardMember)
                    {
                        IndirectMember ReAsMem = new IndirectMember()
                        {
                            OrderNo = mem.OrderNo,
                            MemberCode = mem.MemberCode,
                            MemberType = mem.MemberType,
                            MemberSharePct = mem.MemberSharePct
                        };
                        if (OS.WTInward.AcceptanceId.Trim().ToUpper() == AcceptanceType.CoinsuranceInward)
                        {
                            if (mem.MemberType.Trim().ToUpper() == "L")
                            {
                                O.IndirectInsurance.CedingCoy = mem.MemberCode;
                            }
                            O.IndirectInsurance.RefPolicyNo = OS.WTInward.CedingCoy;
                            O.IndirectInsurance.AcceptanceId = string.Empty;
                        }

                        O.IndirectMemberList.Add(ReAsMem);
                    }
                    */
                    #endregion

                    #region AddressDetail

                    foreach (var addr in dtl.DtlAddress)
                    {
                        AddressDetail Addr = new AddressDetail
                        {
                            OrderNo = IsNA(dtl.Ord[0].PolicyOrderNo),
                            Address = IsNA(addr.Address),
                            PostalCode = IsNA(addr.PostalCode),
                            ContactPerson = IsNA(addr.ContactPerson),
                            EntryUsr = IsNA(UserEntry),
                            //Phone = IsNA(WTAddr.Phone),
                            Handphone = IsNA(addr.Handphone),
                            AddressType = IsNA(addr.AddressType), // POLADR, BILADR : info dari JSW ini disamain sama POLADR, DELIVR
                            SourceType = addr.SourceType, //(WTAddr.SourceType != null) ? WTAddr.SourceType.Trim().ToUpper() == "OH" ? "" : WTAddr.SourceType : "",// kalo customerhome : CH , kalo office : CO
                            RT = "", //IsNA(WTAddr.RT),
                            RW = "", //IsNA(WTAddr.RW),
                            DeliveryCode = IsNA(addr.DeliveryCode), // null 
                            Fax = "",//IsNA(WTAddr.Fax),
                                     //RenewalNotice = IsNA(WTAddr.RenewalNotice) // null
                        };
                        O.AddressDetail.Add(Addr);
                    }
                    #endregion

                    #region PolicyClause

                    foreach (var items in PolicyClauseList) // ada logic deductible dan clausa otomatis, ketika saveobject
                    {
                        PolicyClause PClause = new PolicyClause()
                        {
                            OrderNo = IsNA(dtl.Ord[0].PolicyOrderNo),
                            ClauseId = IsNA(items.ClauseID),
                            Description = IsNA(items.Description),
                            EntryUsr = IsNA(UserEntry)
                        };
                        if (!string.IsNullOrWhiteSpace(PClause.OrderNo) && !string.IsNullOrWhiteSpace(PClause.ClauseId))
                        {
                            O.PolicyClause.Add(PClause);
                        }
                    }

                    #endregion

                    #region automatic
                    //    List<dynamic> auto = new List<dynamic>();
                    //    foreach (var item in O.CoverageData)
                    //    {
                    //        auto.AddRange(OOR.GetPolicyClauseAutomatic(O.OrderData.ProductCode, item.InterestID, item.CoverageId));
                    //    }

                    //    foreach (var item in auto)
                    //    {
                    //        string clauseID = item.ClauseID;
                    //        if (!string.IsNullOrWhiteSpace(clauseID))
                    //        {
                    //            bool isExist = O.PolicyClause.FindIndex(policy => policy.ClauseId.Equals(clauseID)) >= 0;
                    //            bool isExistInObject = O.ObjectClause.FindIndex(o => o.ClauseId.Equals(clauseID)) >= 0;

                    //            if (!isExist && !isExistInObject)
                    //            {
                    //                PolicyClause pc = new PolicyClause()
                    //                {
                    //                    OrderNo = O.OrderData.OrderNo,
                    //                    ClauseId = item.ClauseID,
                    //                    Description = item.ClauseName
                    //                };

                    //                O.PolicyClause.Add(pc);
                    //            }
                    //        }

                    //    }
                    #endregion

                    #region ObjectClause
                    // harusnya dari otosurvey, triggernya dari accs.
                    /*
                     * foreach (var items in OS.WTOrderObjectClause) 
                    {
                        ObjectClause OClause = new ObjectClause()
                        {
                            OrderNo = IsNA(OS.WTOrder.OrderNo),
                            ClauseId = IsNA(items.ClauseID),
                            Description = IsNA(items.Description),
                            EntryUsr = IsNA(UserEntry),
                            ClauseNo = IsNA(items.ClauseNo),
                            ObjectNo = IsNA(items.ObjectNo)
                        };
                        if (!string.IsNullOrWhiteSpace(OClause.OrderNo) && !string.IsNullOrWhiteSpace(OClause.ClauseId))
                        {
                            O.ObjectClause.Add(OClause);
                        }
                    }
                    */
                    #endregion

                    #region LinkOrderAgency

                    if (P.ProductType == 6 && IsNA(dtl.Ord[0].SalesOfficerID).Length > 3)
                    {
                        TitanAgentInfo Agent = OR.GetTitanAgentInfo(dtl.Ord[0].SalesOfficerID); //OR.GetTitanAgentInfo(dtl.Ord.BrokerCode); 
                        if (Agent != null)
                        {
                            O.LinkOrderAgency = new LinkOrderAgency()
                            {
                                OrderNo = IsNA(dtl.Ord[0].PolicyOrderNo),
                                ReferenceNo = string.Empty,
                                ReferenceType = "TITAN",
                                ClientCode = IsNA(Agent.ClientCode),
                                LeaderClientCode = IsNA(Agent.LeaderCode),
                                UplinerClientCode = IsNA(Agent.UplinerCode),
                                OtherNo = string.Empty,
                                ExpiredSendStatus = 0,
                                SendStatus = 0
                            };
                        }
                    }
                    #endregion
                }
                List<InstallmentInfo> Installment = GenerateInstallmentInfo(dtl.Ord[0].PolicyOrderNo, dtl.Ord[0].ProductCode, "TUNAI", dtl.Ord[0].PeriodFrom.Date, IsNA(P.GracePeriod), pf, sd);
                O.InstallmentInfo.AddRange(Installment); // tetep walaupun tunai.
                TransactionAccount tra = new TransactionAccount()
                {
                    ReferenceNo = dtl.Ord[0].PolicyOrderNo,
                    AccountType = "CC",
                    ReferenceType = "ORD",
                    EntryUsr = UserEntry,
                };
                O.TransactionAccount.Add(tra);

                foreach (dynamic imgS in dtl.SurveyImg)
                {
                    SurveyImage sImg = new SurveyImage();
                    sImg.SurveyNo = dtl.Ord[0].SurveyNo;
                    sImg.ImageCategory = ImageLogic.GetMappedReferenceTypeImage(imgS.ImageType, true);
                    sImg.ImageType = "acceptance";
                    sImg.FileName = imgS.ImageName;
                    sImg.CoreImageID = imgS.CoreImage_ID;
                    O.SurveyImageList.Add(sImg);
                }

                foreach (dynamic ii in dtl.InterestItem)
                {
                    InterestItemData iid = new InterestItemData();
                    iid.OrderNo = O.OrderData.OrderNo;
                    iid.PolicyId = ii.PolicyID;
                    iid.ObjectNo = ii.ObjectNo;
                    iid.InterestNo = ii.InterestNo;
                    iid.IntItemNo = ii.IntItemNo;
                    iid.EndorsementNo = ii.EndorsementNo;
                    iid.OrgEndorsementNo = ii.OrgEndorsementNo;
                    iid.OriginalSI = ii.OriginalSI;
                    iid.InsuranceType = ii.InsuranceType;
                    iid.IntItemCode = ii.IntItemCode;
                    iid.ItemName = ii.ItemName;
                    iid.Description = ii.Description;
                    iid.Capacity = iid.Capacity;
                    iid.SumInsured = ii.SumInsured;
                    iid.DedAmount = ii.DedAmount;
                    iid.MfgYear = ii.MfgYear;
                    iid.Birthdate = ii.BirthDate;
                    iid.ColumnA = ii.ColumnA;
                    iid.ColumnB = ii.ColumnB;
                    iid.ColumnC = ii.ColumnC;
                    iid.ColumnD = ii.ColumnD;
                    iid.ColumnE = ii.ColumnE;
                    iid.Date1 = ii.Date1;
                    iid.Date2 = ii.Date2;
                    iid.Flag1 = ii.Flag1;
                    iid.Flag2 = ii.Flag2;
                    O.InterestItemData.Add(iid);
                }

                int ClauseNo = O.ObjectClause.Count;
                int newClauseNo = ClauseNo;
                foreach (dynamic item in dtl.OriginalDefect)
                {
                    newClauseNo = newClauseNo + 1;
                    ObjectClause oc = new ObjectClause()
                    {
                        OrderNo = O.OrderData.OrderNo,
                        ObjectNo = item.ObjectNo == 0 ? 1 : item.ObjectNo,
                        ClauseNo = newClauseNo,
                        ClauseId = item.ClauseID,
                        Description = item.ClauseDetail
                    };
                    ClauseNo = newClauseNo;
                    O.ObjectClause.Add(oc);
                }

                foreach (dynamic item in dtl.oldOriginalDefect)
                {
                    OriginalDefectOrdData or = new OriginalDefectOrdData();
                    or.OrderNo = O.OrderData.OrderNo;
                    or.ObjectNo = Convert.ToInt32(item.ObjectNo);
                    or.EndorsementNo = Convert.ToInt32(item.EndorsementNo);
                    or.PartID = item.PartID;
                    or.Name = item.Name;
                    or.DamageCategory = item.DamageCategory;
                    or.Description = item.Description;
                    O.OriginalDefectList.Add(or);
                }

                int newClauseNo2 = ClauseNo;
                if (!string.IsNullOrEmpty(O.OrderData.OldPolicyNo))
                {
                    List<dynamic> obj_clauses = OOR.getOldObjectClause(O.OrderData.OldPolicyNo);
                    newClauseNo2 = ClauseNo;
                    foreach (var item in obj_clauses)
                    {
                        string clauseID = item.ClauseID;
                        bool isExist = O.ObjectClause.FindIndex(o => o.ClauseId.Equals(clauseID)) >= 0;

                        if (!isExist)
                        {
                            newClauseNo2 = newClauseNo2 + 1;
                            ObjectClause oc = new ObjectClause()
                            {
                                OrderNo = O.OrderData.OrderNo,
                                //ObjectNo = 1,
                                ObjectNo = dtl.MV[0].ObjectNo,
                                ClauseNo = newClauseNo2,
                                ClauseId = item.ClauseID,
                                Description = item.Description

                            };
                            O.ObjectClause.Add(oc);
                        }
                    }

                    List<dynamic> policy_clauses = OOR.getOldPolicyClause201(O.OrderData.OldPolicyNo);
                    foreach (var item in policy_clauses)
                    {
                        string clauseID = item.ClauseID;
                        bool isExist = O.PolicyClause.FindIndex(policy => policy.ClauseId.Equals(clauseID)) >= 0;
                        bool isExistInObject = O.ObjectClause.FindIndex(o => o.ClauseId.Equals(clauseID)) >= 0;

                        if (!isExist && !isExistInObject)
                        {
                            PolicyClause pc = new PolicyClause()
                            {
                                OrderNo = O.OrderData.OrderNo,
                                ClauseId = item.ClauseID,
                                Description = item.Description
                            };

                            O.PolicyClause.Add(pc);
                        }
                    }
                }
                //TODO Original Defect??
                foreach (dynamic ns in dtl.NonStandardAcc)
                {
                    NonStandardAccessoriesOrdData nao = new NonStandardAccessoriesOrdData();
                    nao.OrderNo = O.OrderData.OrderNo;
                    nao.ObjectNo = ns.Object_No;
                    nao.AccsCode = ns.Accs_Code;
                    nao.AccsCatCode = ns.Accs_Cat_Code;
                    nao.AccsPartCode = ns.Accs_Part_Code;
                    nao.IncludeTsi = ns.Include_Tsi;
                    nao.Brand = ns.Brand;
                    nao.SumInsured = ns.Sum_Insured;
                    nao.Quantity = ns.Quantity;
                    nao.Premi = ns.Premi;
                    nao.Category = ns.Category;
                    nao.EndorsementNo = ns.EndorsementNo;
                    O.NonStandardAccessoriesList.Add(nao);
                }
                foreach (dynamic nc in dtl.NoCover)
                {
                    NoCoverOrdData nocvr = new NoCoverOrdData();
                    nocvr.OrderNo = O.OrderData.OrderNo;
                    nocvr.ObjectNo = nc.ObjectNo;
                    nocvr.Type = nc.Type;
                    nocvr.DemageType = nc.DamageType;
                    nocvr.NoCoverID = nc.NoCoverID;
                    nocvr.Name = nc.Name;
                    nocvr.Description = nc.Description;
                    nocvr.Quantity = nc.Quantity;
                    O.NoCoverList.Add(nocvr);
                }
                return O;
            }
            catch (Exception e)
            {
                logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.StackTrace));
                CommonLogic.InsertErrorLog(followUpNo, CurrentMethod,
                    string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.StackTrace), 1);
                throw e;
            }
        }

        #endregion

        #region Process Order
        public static OrderResult DoSaveOrder(Order Ord, string EntryUser)
        {
            string CurrentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            OrderResult Result = new OrderResult();
            OrderRepository OR = new OrderRepository();
            string TransactionNo;
            /*SPRINT 10 BEGIN*/
            Ord.PendingOrderInfo = OR.IsPendingOrder(Ord.OrderData.OrderNo);
            Transaction Tran = new Transaction();
            TransactionDetail TranDtl = new TransactionDetail();
            PolicyRepository rep = new PolicyRepository();
            if (Ord.PendingOrderInfo != null)
            {
                Tran.TransactionNo = OR.GetTransactionIDEx(Ord.OrderData.OrderNo);
            }
            else
            {
                Tran.TransactionNo = CommonLogic.GetTransactionNo();
            }
            /*SPRINT 10 END*/
            TransactionNo = Tran.TransactionNo;
            if (string.IsNullOrWhiteSpace(Tran.TransactionNo))
            {
                Result.ErrorInfo = ErrorMessages.GetTransactionNoError;
                return Result;
            }

            if (!string.IsNullOrWhiteSpace(Ord.OrderData.CustId))
            {
                Tran.ReferenceType = ReferenceType.Customer;
                Tran.ReferenceId = Ord.OrderData.CustId;
            }
            else
            {
                Tran.ReferenceType = ReferenceType.Prospect;
                Tran.ReferenceId = Ord.OrderData.ProspectId;
            }
            if (EntryUser.Length > 5) {
                EntryUser = CommonLogic.GetSettingUserID();
            }
            OR.TransactionSave(Tran, EntryUser);
            //if (OrderRepository.TransactionSave(Tran, EntryUser) < 1)
            //{
            //    Result.ErrorInfo = ErrorMessages.SaveTransactionError;
            //    return Result;
            //}
            /*SPRINT 10 BEGIN*/
            if (Ord.PendingOrderInfo != null)
            {
                OR.DeleteAABTransaction(Ord.OrderData.OrderNo, Ord.OrderData.TitanOrderNo);
            }
            /*SPRINT 10 END*/
            var saveOrderResult = SaveOrder(Ord, EntryUser);
            if (saveOrderResult.IsSuccess)
            {
                TranDtl.TransactionNo = Tran.TransactionNo;
                TranDtl.TransactionType = "ORDER";
                TranDtl.ReferenceNo = Ord.OrderData.OrderNo;
                OR.TransactionSaveDetail(TranDtl, EntryUser);
            }
            try
            {
                Product product = rep.GetProductDetail(Ord.OrderData.ProductCode);
                if (product != null)
                {
                    if (product.ProductType == 6 && Ord.AgencyInfo != null)
                    {
                        OR.InsertOrderTitan(Ord, EntryUser);
                        OR.AddLnkOrderAgency(OrderNo: Ord.OrderData.OrderNo, ClientCode: Ord.AgencyInfo.AgentID, UplinerCode: Ord.AgencyInfo.UplinerID, LeaderCode: Ord.AgencyInfo.LeaderID, Actor: EntryUser, RefferenceNo: Ord.OrderData.TitanOrderNo.ToString());
                    }
                }
                PolicyRepository policy = new PolicyRepository();
                var salesmanProduct = policy.GetMappingSalesmanProduct(Ord.OrderData.ProductCode, Ord.OrderData.MOUID);
                if (salesmanProduct.Count > 0)
                {

                    //OrderRepository.AddHierarchyCIFTrans(Ord.OrderData.PolicyNo, Ord.OrderData.OrderNo,Ord.SalesmanDealerInfo, EntryUser);
                }
            }
            catch (Exception e)
            {
                logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.StackTrace));
                CommonLogic.InsertErrorLog(Ord.OrderData.OrderNo, CurrentMethod,
                string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.StackTrace), 1);
                Console.WriteLine(e.Message);
            }
            return new OrderResult { OrderNo = Ord.OrderData.OrderNo, ErrorInfo = "" };
        }

        public static OrderResult DoSaveOrderAfterSurvey(Order Ord, string EntryUser)
        {
            string CurrentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            OrderResult Result = new OrderResult();
            OrderRepository OR = new OrderRepository();
            string TransactionNo;
            /*SPRINT 10 BEGIN*/
            Ord.PendingOrderInfo = OR.IsPendingOrder(Ord.OrderData.OrderNo);
            Transaction Tran = new Transaction();
            TransactionDetail TranDtl = new TransactionDetail();
            PolicyRepository rep = new PolicyRepository();
            if (Ord.PendingOrderInfo != null)
            {
                Tran.TransactionNo = OR.GetTransactionIDEx(Ord.OrderData.OrderNo);
            }
            else
            {
                Tran.TransactionNo = CommonLogic.GetTransactionNo();
            }
            /*SPRINT 10 END*/
            TransactionNo = Tran.TransactionNo;
            if (string.IsNullOrWhiteSpace(Tran.TransactionNo))
            {
                Result.ErrorInfo = ErrorMessages.GetTransactionNoError;
                return Result;
            }

            if (!string.IsNullOrWhiteSpace(Ord.OrderData.CustId))
            {
                Tran.ReferenceType = ReferenceType.Customer;
                Tran.ReferenceId = Ord.OrderData.CustId;
            }
            else
            {
                Tran.ReferenceType = ReferenceType.Prospect;
                Tran.ReferenceId = Ord.OrderData.ProspectId;
            }
            if (EntryUser.Length > 5)
            {
                EntryUser = CommonLogic.GetSettingUserID();
            }
            OR.TransactionSave(Tran, EntryUser);
            //if (OrderRepository.TransactionSave(Tran, EntryUser) < 1)
            //{
            //    Result.ErrorInfo = ErrorMessages.SaveTransactionError;
            //    return Result;
            //}
            /*SPRINT 10 BEGIN*/
            if (Ord.PendingOrderInfo != null)
            {
                OR.DeleteAABTransaction(Ord.OrderData.OrderNo, Ord.OrderData.TitanOrderNo);
            }
            /*SPRINT 10 END*/
            var saveOrderResult = SaveOrderAfterSurvey(Ord, EntryUser);
            if (saveOrderResult.IsSuccess)
            {
                TranDtl.TransactionNo = Tran.TransactionNo;
                TranDtl.TransactionType = "ORDER";
                TranDtl.ReferenceNo = Ord.OrderData.OrderNo;
                OR.TransactionSaveDetail(TranDtl, EntryUser);
            }
            try
            {
                Product product = rep.GetProductDetail(Ord.OrderData.ProductCode);
                if (product != null)
                {
                    if (product.ProductType == 6 && Ord.AgencyInfo != null)
                    {
                        OR.InsertOrderTitan(Ord, EntryUser);
                        OR.AddLnkOrderAgency(OrderNo: Ord.OrderData.OrderNo, ClientCode: Ord.AgencyInfo.AgentID, UplinerCode: Ord.AgencyInfo.UplinerID, LeaderCode: Ord.AgencyInfo.LeaderID, Actor: EntryUser, RefferenceNo: Ord.OrderData.TitanOrderNo.ToString());
                    }
                }
                //PolicyRepository policy = new PolicyRepository();
                //var salesmanProduct = policy.GetMappingSalesmanProduct(Ord.OrderData.ProductCode, Ord.OrderData.MOUID);
                //if (salesmanProduct.Count > 0)
                //{

                //    //OrderRepository.AddHierarchyCIFTrans(Ord.OrderData.PolicyNo, Ord.OrderData.OrderNo,Ord.SalesmanDealerInfo, EntryUser);
                //}
            }
            catch (Exception e)
            {
                logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.StackTrace));
                CommonLogic.InsertErrorLog(Ord.OrderData.OrderNo, CurrentMethod,
                string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.StackTrace), 1);
                Console.WriteLine(e.Message);
            }
            return new OrderResult { OrderNo = Ord.OrderData.OrderNo, ErrorInfo = "" };
        }

        private static SaveOrderResult SaveOrder(Order Ord, string EntryUser)
        {
            SaveOrderResult Result = new SaveOrderResult();
            OrderRepository OR = new OrderRepository();

            Result = AddNew(Ord, EntryUser);
            if (Result.IsSuccess)
            {
                OR.AddComSalesTrx(Ord.OrderData.OrderNo);
                if (OR.AddPolicyClause(Ord.PolicyClause, EntryUser))
                {
                    if (OR.AddObjectClause(Ord.ObjectClause, EntryUser))
                    {
                        //Sprint 11 (LAU) fix inserting VA Number to Ord_VANumber
                        SaveSurvey(Ord.SurveyInfo, EntryUser, Ord.OrderData.OrderNo);
                        OR.InsertSurveyOrder(Ord.SurveyOrder);
                        if (string.IsNullOrWhiteSpace(Ord.OrderData.PreOrderedVANumber))
                        {
                            AddVirtualAccount(Ord.OrderData.OrderNo, EntryUser, Ord.OrderData.VANumber);
                        }
                        else
                        {
                            OR.AddOrderVANo(Ord.OrderData.OrderNo, Ord.OrderData.PreOrderedVANumber, EntryUser);

                            //Sprint 11 (LAU) : dipindah dari approve policy: VA Number disimpan saat submit (tidak saat approve) dari Approval.cs: AppNewPolicy AABDB.AddPreOrderedVANo
                            OR.AddPreOrderedVANo(Ord.OrderData.OrderNo, Ord.OrderData.PreOrderedVANumber, EntryUser);
                        }

                        //if (SaveSurvey(Ord.SurveyInfo, EntryUser))
                        //{
                        //    if (string.IsNullOrWhiteSpace(Ord.OrderData.PreOrderedVANumber)){
                        //        AddVirtualAccount(Ord.OrderData.OrderNo, EntryUser);
                        //    }
                        //    else {
                        //        OrderRepository.AddOrderVANo(Ord.OrderData.OrderNo, Ord.OrderData.PreOrderedVANumber, EntryUser);
                        //    }
                        //}
                        //else
                        //{
                        //    Result.IsSuccess = false; Result.ErrorInfo = ErrorMessages.InsertSurveyError;
                        //}

                        OR.SaveTransactionAccount(Ord.TransactionAccount, EntryUser);
                    }
                    else
                    {
                        Result.IsSuccess = false; Result.ErrorInfo = ErrorMessages.InsertObjectClauseError;
                    }
                }
                if (!Ord.OrderData.AcceptanceId.Trim().ToUpper().Equals(AcceptanceType.DIRECT))
                {
                    if (Ord.IndirectInsurance != null)
                    {
                        OR.AddIndirectInsurance(Ord.IndirectInsurance, EntryUser);
                        OR.AddIndirectMember(Ord.IndirectMemberList, EntryUser);
                    }
                }
                else
                {
                    Result.IsSuccess = false; Result.ErrorInfo = ErrorMessages.InsertPolicyClauseError;
                }

            }
            return new SaveOrderResult { IsSuccess = true };
        }

        private static SaveOrderResult SaveOrderAfterSurvey(Order Ord, string EntryUser)
        {
            SaveOrderResult Result = new SaveOrderResult();
            OrderRepository OR = new OrderRepository();

            Result = AddNewAfterSurvey(Ord, EntryUser);
            if (Result.IsSuccess)
            {
                OR.AddComSalesTrx(Ord.OrderData.OrderNo);
                if (OR.AddPolicyClause(Ord.PolicyClause, EntryUser))
                {
                    if (OR.AddObjectClauseAfterSurvey(Ord.ObjectClause, EntryUser))
                    {
                        //Sprint 11 (LAU) fix inserting VA Number to Ord_VANumber
                        //SaveSurvey(Ord.SurveyInfo, EntryUser, Ord.OrderData.OrderNo);
                        //OR.InsertSurveyOrder(Ord.SurveyOrder);
                        if (string.IsNullOrWhiteSpace(Ord.OrderData.PreOrderedVANumber))
                        {
                            AddVirtualAccount(Ord.OrderData.OrderNo, EntryUser, Ord.OrderData.VANumber);
                        }
                        else
                        {
                            OR.AddOrderVANo(Ord.OrderData.OrderNo, Ord.OrderData.PreOrderedVANumber, EntryUser);

                            //Sprint 11 (LAU) : dipindah dari approve policy: VA Number disimpan saat submit (tidak saat approve) dari Approval.cs: AppNewPolicy AABDB.AddPreOrderedVANo
                            OR.AddPreOrderedVANo(Ord.OrderData.OrderNo, Ord.OrderData.PreOrderedVANumber, EntryUser);
                        }

                        OR.SaveTransactionAccount(Ord.TransactionAccount, EntryUser);
                    }
                    else
                    {
                        Result.IsSuccess = false; Result.ErrorInfo = ErrorMessages.InsertObjectClauseError;
                    }
                }
                if (!Ord.OrderData.AcceptanceId.Trim().ToUpper().Equals(AcceptanceType.DIRECT))
                {
                    if (Ord.IndirectInsurance != null)
                    {
                        OR.AddIndirectInsurance(Ord.IndirectInsurance, EntryUser);
                        OR.AddIndirectMember(Ord.IndirectMemberList, EntryUser);
                    }
                }
                else
                {
                    Result.IsSuccess = false; Result.ErrorInfo = ErrorMessages.InsertPolicyClauseError;
                }

            }
            return new SaveOrderResult { IsSuccess = true };
        }

        private static SaveOrderResult AddNew(Order Ord, string EntryUser)
        {
            SaveOrderResult Result = new SaveOrderResult();
            OrderRepository OR = new OrderRepository();

            if (OR.AddOrder(Ord.OrderData, EntryUser, Ord.PendingOrderInfo))
            {
                if (AddObject(Ord.ObjectData, Ord.ScoringData, Ord.ObjectDetailData, Ord.InterestData, Ord.InsuredData, Ord.CoverageData, Ord.NoCoverList, Ord.OriginalDefectList, Ord.NonStandardAccessoriesList, Ord.SurveyImageList, Ord.CoverCommision, Ord.InterestItemData, EntryUser))
                {
                    if (OR.AddDetailAddress(Ord.AddressDetail, EntryUser))
                    {
                        if (OR.AddDocuments(Ord.DocumentData, EntryUser))
                        {
                            if (OR.AddInstallmentInfo(Ord.InstallmentInfo, EntryUser))
                            {
                                Result.IsSuccess = true;
                            }
                            else
                            {
                                Result.IsSuccess = false; Result.ErrorInfo = ErrorMessages.InsertOrderInstallmentError;
                            }
                        }
                        else
                        {
                            Result.IsSuccess = false; Result.ErrorInfo = ErrorMessages.InsertOrderDocumentError;
                        }
                    }
                    else
                    {
                        Result.IsSuccess = false; Result.ErrorInfo = ErrorMessages.InsertOrderAddressError;
                    }
                }
                else
                {
                    Result.IsSuccess = false; Result.ErrorInfo = ErrorMessages.InsertObjectDetailError;
                }
            }
            else
            {
                Result.IsSuccess = false; Result.ErrorInfo = ErrorMessages.InsertMstOrderError;
            }
            return Result;
        }

        private static SaveOrderResult AddNewAfterSurvey(Order Ord, string EntryUser)
        {
            SaveOrderResult Result = new SaveOrderResult();
            OrderRepository OR = new OrderRepository();

            if (OR.AddOrder(Ord.OrderData, EntryUser, Ord.PendingOrderInfo))
            {
                if (AddObjectAfterSurvey(Ord.ObjectData, Ord.ScoringData, Ord.ObjectDetailData, Ord.InterestData, Ord.InsuredData, Ord.CoverageData, Ord.NoCoverList, Ord.OriginalDefectList, Ord.NonStandardAccessoriesList, Ord.SurveyImageList, Ord.CoverCommision, Ord.InterestItemData, EntryUser))
                {
                    if (OR.AddDetailAddress(Ord.AddressDetail, EntryUser))
                    {
                        if (OR.AddDocuments(Ord.DocumentData, EntryUser))
                        {
                            if (OR.AddInstallmentInfo(Ord.InstallmentInfo, EntryUser))
                            {
                                Result.IsSuccess = true;
                            }
                            else
                            {
                                Result.IsSuccess = false; Result.ErrorInfo = ErrorMessages.InsertOrderInstallmentError;
                            }
                        }
                        else
                        {
                            Result.IsSuccess = false; Result.ErrorInfo = ErrorMessages.InsertOrderDocumentError;
                        }
                    }
                    else
                    {
                        Result.IsSuccess = false; Result.ErrorInfo = ErrorMessages.InsertOrderAddressError;
                    }
                }
                else
                {
                    Result.IsSuccess = false; Result.ErrorInfo = ErrorMessages.InsertObjectDetailError;
                }
            }
            else
            {
                Result.IsSuccess = false; Result.ErrorInfo = ErrorMessages.InsertMstOrderError;
            }
            return Result;
        }

        private static bool AddObject(List<ObjectOrderData> Obj, List<ScoringData> Scoring, List<ObjectOrderDetailData> ObjDetail, List<InterestData> Interest, List<InsuredData> Insured, List<CoverageData> Coverage, List<NoCoverOrdData> NoCoverList, List<OriginalDefectOrdData> OriginalDefectList, List<NonStandardAccessoriesOrdData> NonStandardAccessoriesOrdData, List<SurveyImage> SurveyImageList, List<CoverCommision> CoverCommision = null, List<InterestItemData> InterestItem = null, string EntryUser = "")
        {
            OrderRepository OR = new OrderRepository();
            bool IsSuccess = false;
            if (OR.AddObjectOrder(Obj, EntryUser))
            {
                if (OR.AddScoring(Scoring, EntryUser))
                {
                    if (OR.AddObjectOrderDetail(ObjDetail, EntryUser))
                    {
                        OR.AddSurveyImage(SurveyImageList, EntryUser);

                        OR.AddNoCover(NoCoverList, EntryUser);
                        OR.AddOriginalDefect(OriginalDefectList, EntryUser);
                        OR.AddNonStandardAccessories(NonStandardAccessoriesOrdData, EntryUser);
                        if (AddDetailInterest(Interest, Coverage, Insured, EntryUser))
                        {
                            IsSuccess = true;
                            if (CoverCommision != null)
                            {
                                if (OR.AddCoverCommision(CoverCommision, EntryUser))
                                {
                                    IsSuccess = true;
                                }
                                else
                                {
                                    IsSuccess = false;
                                }
                            }
                            if (InterestItem != null)
                            {
                                if (OR.AddInterestItem(InterestItem, EntryUser))
                                {
                                    IsSuccess = true;
                                }
                                else
                                {
                                    IsSuccess = false;
                                }
                            }
                        }
                    }
                }
            }
            return IsSuccess;
        }

        private static bool AddObjectAfterSurvey(List<ObjectOrderData> Obj, List<ScoringData> Scoring, List<ObjectOrderDetailData> ObjDetail, List<InterestData> Interest, List<InsuredData> Insured, List<CoverageData> Coverage, List<NoCoverOrdData> NoCoverList, List<OriginalDefectOrdData> OriginalDefectList, List<NonStandardAccessoriesOrdData> NonStandardAccessoriesOrdData, List<SurveyImage> SurveyImageList, List<CoverCommision> CoverCommision = null, List<InterestItemData> InterestItem = null, string EntryUser = "")
        {
            OrderRepository OR = new OrderRepository();
            bool IsSuccess = false;
            if (OR.AddObjectOrder(Obj, EntryUser))
            {
                if (OR.AddScoring(Scoring, EntryUser))
                {
                    if (OR.AddObjectOrderDetail(ObjDetail, EntryUser))
                    {
                        //OR.AddSurveyImage(SurveyImageList, EntryUser);

                        //OR.AddNoCover(NoCoverList, EntryUser);
                        //OR.AddOriginalDefect(OriginalDefectList, EntryUser);
                        //OR.AddNonStandardAccessories(NonStandardAccessoriesOrdData, EntryUser);
                        if (AddDetailInterest(Interest, Coverage, Insured, EntryUser))
                        {
                            IsSuccess = true;
                            if (CoverCommision != null)
                            {
                                if (OR.AddCoverCommision(CoverCommision, EntryUser))
                                {
                                    IsSuccess = true;
                                }
                                else
                                {
                                    IsSuccess = false;
                                }
                            }
                            //if (InterestItem != null)
                            //{
                            //    if (OR.AddInterestItem(InterestItem, EntryUser))
                            //    {
                            //        IsSuccess = true;
                            //    }
                            //    else
                            //    {
                            //        IsSuccess = false;
                            //    }
                            //}
                        }
                    }
                }
            }
            return IsSuccess;
        }

        private static bool AddDetailInterest(List<InterestData> Interest, List<CoverageData> Coverage, List<InsuredData> Insured, string EntryUser)
        {
            OrderRepository OR = new OrderRepository();
            bool IsSuccess = false;
            if (OR.AddInterest(Interest, EntryUser))
            {
                if (OR.AddCoverage(Coverage, EntryUser))
                {
                    if (OR.AddInsured(Insured, EntryUser))
                    {
                        IsSuccess = true;
                    }
                }
            }
            return IsSuccess;
        }
        private static bool SaveSurvey(List<SurveyInfo> Survey, string EntryUser, string OrderNo)
        {
            OrderRepository OR = new OrderRepository();
            bool isValid = false;
            if (Survey.Count == 0)
                return true;
            if (OR.SurveySaveDetail(Survey, EntryUser))
            {
                if (Survey.Count > 0)
                {
                    if (OR.InsertLnkSurveyOrder(Survey.FirstOrDefault().ReferenceNo, Survey.FirstOrDefault().SurveyType, Survey.FirstOrDefault().SurveyNo, EntryUser))
                    {
                        if (OR.InsertMstOrderMobile(OrderNo, EntryUser)) {
                            isValid = true;
                        }
                    }
                }
            }
            return isValid;
        }

        public static void AddVirtualAccount(string OrderNo, string EntryUser, string VANumber)
        {
            OrderRepository OR = new OrderRepository();
            //string VANo = string.Empty;
            if (!string.IsNullOrEmpty(VANumber))
            {
                var VANo = OR.CheckVA(OrderNo);
                if (string.IsNullOrWhiteSpace(VANo))
                {
                    if (OR.ChekVADataKotor(OrderNo))
                    {
                        OR.AddVA(OrderNo, EntryUser);
                    }
                }
            }
        }

        #endregion
    }
}
