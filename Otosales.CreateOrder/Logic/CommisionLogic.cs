﻿using Otosales.CreateOrder.Repository;
using Otosales.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otosales.Logic
{
    public class CommissionLogic
    {
        public static PremiumScheme CPremiumScheme(decimal CoverPremium)
        {
            PremiumScheme PS = new PremiumScheme()
            {
                GrossPremium = CoverPremium,
                NetPremium = CoverPremium,
                Net1 = CoverPremium,
                Net2 = CoverPremium,
                Net3 = CoverPremium,
                CoverPremium = CoverPremium
            };
            return PS;

        }
        public static PremiumScheme CalculateDiscountPremium(List<CommissionScheme> Discounts, decimal CoverPremium)
        {
            PremiumScheme PS = CPremiumScheme(CoverPremium);
            PS.CoverPremium = CoverPremium;
            for (int i = 0; i < Discounts.Count; i++)
            {
                decimal TotalPercentage = 0;
                decimal TotalFlatAmount = 0;
                if (Discounts[i].Schemes != null)
                {
                    TotalPercentage = mf_CLevel_TotalPercentage(Discounts[i].Schemes);
                    TotalFlatAmount = mf_CLevel_TotalFlatAmount(Discounts[i].Schemes);
                }
                switch (i)
                {
                    case 0:
                        PS.GrossPremium = (PS.CoverPremium * (100 - TotalPercentage) / 100) - TotalFlatAmount;
                        break;
                    case 1:
                        PS.Net1 = (PS.GrossPremium * (100 - TotalPercentage) / 100) - TotalFlatAmount;
                        break;
                    case 2:
                        PS.Net2 = (PS.Net1 * (100 - TotalPercentage) / 100) - TotalFlatAmount;
                        break;
                    case 3:
                        PS.Net3 = (PS.Net2 * (100 - TotalPercentage) / 100) - TotalFlatAmount;
                        break;
                    case 4:
                        PS.NetPremium = (PS.Net3 * (100 - TotalPercentage) / 100) - TotalFlatAmount;
                        break;
                }
                PS.CoverPremium = PS.NetPremium;

            }
            return (PS);
        }

        public static List<CommissionScheme> InitDiscountList(string CoverageID, List<WTCommission> DiscountList, bool AutoApply = false)
        {
            int g_DiscountMap = 1;
            int i;

            int level;
            List<CommissionScheme> DiscountScheme = new List<CommissionScheme>();
            var ApplyFlags = 0;

            for (i = 0; i <= 4; i++)
            {
                CommissionScheme CScheme = new CommissionScheme
                {
                    TotalFlatAmount = 0,
                    TotalPercentage = 0,
                    Level = i,
                    Schemes = new List<CScheme>()
                };
                CommissionScheme CMScheme = new CommissionScheme();
                DiscountScheme.Add(CMScheme);
            }

            for (i = 0; i < DiscountList.Count; i++)
            {
                ApplyFlags = DiscountList[i].ApplyFlags;

                if ((ApplyFlags & g_DiscountMap) == g_DiscountMap)
                {
                    if (string.IsNullOrWhiteSpace(DiscountList[i].CoverageID) || DiscountList[i].CoverageID.Trim().ToUpper() == CoverageID)
                    {
                        level = 0;
                        level = DiscountList[i].Level;


                        if (AutoApply)
                        {
                            CScheme C = new CScheme()
                            {
                                Active = true,
                                ID = DiscountList[i].CommissionID,
                                Name = DiscountList[i].CommissionName,
                                Type = DiscountList[i].CommissionType,
                                Percentage = DiscountList[i].Percentage,
                                FlatAmount = 0,
                                Amount = DiscountList[i].Percentage,
                                PayToCode = "1",
                                ApplyFlags = DiscountList[i].ApplyFlags
                            };
                            if (DiscountScheme[level].Schemes == null)
                            {
                                DiscountScheme[level].Schemes = new List<CScheme>();
                            }
                            DiscountScheme[level].Schemes.Add(C);
                        }
                        else
                        {
                            CScheme C = new CScheme()
                            {
                                Active = true,
                                ID = DiscountList[i].CommissionID,
                                Name = DiscountList[i].CommissionName,
                                Type = DiscountList[i].CommissionType,
                                Percentage = DiscountList[i].Percentage,
                                FlatAmount = 0,
                                Amount = 0,
                                PayToCode = "1",
                                ApplyFlags = DiscountList[i].ApplyFlags
                            };
                            if (DiscountScheme[level].Schemes == null)
                            {
                                DiscountScheme[level].Schemes = new List<CScheme>();
                            }
                            DiscountScheme[level].Schemes.Add(C);
                        }
                    }
                }
            }

            return DiscountScheme;
        }

        public static List<CommissionScheme> InitFlatDiscount(string g_ProductCode, decimal CoverPremium, List<CommissionScheme> CommissionScheme, decimal SumInsured, decimal ExchangeRate, string IDType, bool AutoApply = false)
        {
            int i, j;

            for (i = 0; i < CommissionScheme.Count; i++)
            {
                if (CommissionScheme[i] != null)
                {
                    if (CommissionScheme[i].Schemes != null)
                    {
                        for (j = 0; j < CommissionScheme[i].Schemes.Count; j++)
                        {
                            if (CommissionScheme[i].Schemes[j] != null)
                            {
                                if (CommissionScheme[i].Schemes[j].Percentage == 0)
                                {
                                    CommissionScheme[i].Schemes[j].SI = SumInsured;

                                    CommissionScheme[i].Schemes[j].OriginalFlatAmount = GetFlatAmount(i, CommissionScheme, CoverPremium, g_ProductCode, CommissionScheme[i].Schemes[j].ID, CommissionScheme[i].Schemes[j].SI, ExchangeRate, IDType);
                                    CommissionScheme[i].Schemes[j].FlatAmount = CommissionScheme[i].Schemes[j].OriginalFlatAmount;

                                    if (AutoApply)
                                    {
                                        CommissionScheme[i].Schemes[j].Amount = CommissionScheme[i].Schemes[j].FlatAmount;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return CommissionScheme;
        }

        public static List<CommissionScheme> InitCommisionList(string CoverageID, List<WTCommission> CommissionList, bool AutoApply = false)
        {
            int C_FLAG_SPECIALCOMMISSION = 4096;
            int g_DiscountMap = 1;
            int i;

            int level;
            List<CommissionScheme> DiscountScheme = new List<CommissionScheme>();
            // List<WTCommission> CommissionList = new List<WTCommission>();
            var ApplyFlags = 0;

            for (i = 0; i <= 4; i++)
            {
                CommissionScheme CScheme = new CommissionScheme
                {
                    TotalFlatAmount = 0,
                    TotalPercentage = 0,
                    Level = i,
                    Schemes = new List<CScheme>()
                };
                CommissionScheme CMScheme = new CommissionScheme();
                DiscountScheme.Add(CMScheme);
                //DiscountScheme[i].Schemes = new List<CScheme>();
            }

            for (i = 0; i < CommissionList.Count; i++)
            {
                ApplyFlags = CommissionList[i].ApplyFlags;
                if ((ApplyFlags & g_DiscountMap) == g_DiscountMap)
                {
                    if (string.IsNullOrWhiteSpace(CommissionList[i].CoverageID) || CommissionList[i].CoverageID.Trim().ToUpper() == CoverageID)
                    {
                        level = 0;
                        level = CommissionList[i].Level;

                        if (AutoApply)
                        {
                            CScheme C = new CScheme()
                            {
                                Active = true,
                                ID = CommissionList[i].CommissionID,
                                Name = CommissionList[i].CommissionName,
                                Type = CommissionList[i].CommissionType,
                                Percentage = CommissionList[i].Percentage,
                                FlatAmount = 0,
                                Amount = CommissionList[i].Percentage,
                                //PayToCode = "1",
                                PayToCode = CommissionList[i].PayToParty,
                                ApplyFlags = CommissionList[i].ApplyFlags
                            };
                            if (DiscountScheme[level].Schemes == null)
                            {
                                DiscountScheme[level].Schemes = new List<CScheme>();
                            }
                            DiscountScheme[level].Schemes.Add(C);
                        }

                        else
                        {
                            CScheme C = new CScheme()
                            {
                                Active = true,
                                ID = CommissionList[i].CommissionID,
                                Name = CommissionList[i].CommissionName,
                                Type = CommissionList[i].CommissionType,
                                Percentage = CommissionList[i].Percentage,
                                FlatAmount = 0,
                                Amount = 0,
                                //PayToCode = "1",
                                PayToCode = CommissionList[i].PayToParty,
                                ApplyFlags = CommissionList[i].ApplyFlags
                            };
                            if (DiscountScheme[level].Schemes == null)
                            {
                                DiscountScheme[level].Schemes = new List<CScheme>();
                            }
                            DiscountScheme[level].Schemes.Add(C);

                        }
                    }
                }
            }

            return DiscountScheme;
        }

        public static decimal GetFlatAmount(int Level, List<CommissionScheme> DiscCommList, decimal CoverPremium, string ProductCode, string ID, decimal SumInsured, decimal ExchangeRate, string IDType)
        {
            decimal tmpAmount = 0;

            decimal tmpBCAmount = 0;
            decimal tmpPercentage = 0;

            OrderRepository OR = new OrderRepository();
            var Result = OR.Product_GetFlatCommission(ProductCode, ID, SumInsured * ExchangeRate, IDType);

            if (Result != null)
            {
                foreach (var item in Result)
                {
                    if (item.CommissionPercentage == 0)
                    {
                        tmpBCAmount = item.FlatCommissionAmount;
                        tmpPercentage = item.CommissionPercentage;
                    }
                }
                var PS = CalculateDiscountPremium(DiscCommList, CoverPremium);
                switch (Level)
                {
                    case 0:
                        tmpAmount = (PS.GrossPremium * tmpPercentage / 100) - tmpBCAmount;
                        break;
                    case 1:
                        tmpAmount = (PS.Net1 * tmpPercentage / 100) - tmpBCAmount;
                        break;
                    case 2:
                        tmpAmount = (PS.Net2 * tmpPercentage / 100) - tmpBCAmount;
                        break;
                    case 3:
                        tmpAmount = (PS.Net3 * tmpPercentage / 100) - tmpBCAmount;
                        break;
                    case 4:
                        tmpAmount = (PS.NetPremium * tmpPercentage / 100) - tmpBCAmount;
                        break;
                }

                tmpAmount = tmpBCAmount / ExchangeRate;


            }
            return (tmpAmount);
        }


        private static decimal mf_CLevel_TotalPercentage(List<CScheme> Schemes)
        {
            decimal Total = 0;
            var i = 0;

            for (i = 0; i < Schemes.Count; i++)
            {
                if (Schemes[i].Active && Schemes[i].Percentage != 0)
                {
                    Total += Schemes[i].Percentage;
                }
            }
            return (Total);
        }
        private static decimal mf_CLevel_TotalFlatAmount(List<CScheme> Schemes)
        {
            decimal Total = 0;
            var i = 0;

            for (i = 0; i < Schemes.Count; i++)
            {
                if (Schemes[i].Active && Schemes[i].Percentage == 0)
                {
                    Total += Schemes[i].Amount;
                }
            }
            return (Total);
        }

        public static List<CoverCommision> SaveCommissionToDataPool(string OrderNo, PremiumScheme PS, string ProductCode, int ObjectNumber, string ActInterestID, int InterestNumber, string CurrencyID, int CoverageNumber, string ActCoverageID, List<CommissionScheme> CommissionList, string Status, bool HasSpecialCommission, DateTime CoverFrom, DateTime CoverTo, decimal OriginalRate, decimal Rate)
        {
            List<CoverCommision> CoverComm = new List<CoverCommision>();
            int I = 0;
            decimal RealCommission = 0;
            List<dynamic> arrReturn = new List<dynamic>();
            decimal CommPercentage = 0;
            List<string> arrResult = new List<string>();
            int J = 0;
            bool NonStandard = false;
            bool ApplyCommission = true;
            bool CommissionApplyFlags = true;

            CoverCommision Comm = new CoverCommision();
            int g_DiscountMap = 1;
            int C_FLAG_SPECIALCOMMISSION = 4096;

            for (int i = 0; i < CommissionList.Count; i++)
            {
                if (CommissionList[i].Schemes != null)
                {
                    for (int j = 0; j < CommissionList[i].Schemes.Count; j++)
                    {
                        var CS = CommissionList[i].Schemes[j];

                        ApplyCommission = true;
                        CommissionApplyFlags = ((CS.ApplyFlags & g_DiscountMap) == g_DiscountMap);
                        if (CommissionApplyFlags && ((CS.Active && ((CS.ApplyFlags & C_FLAG_SPECIALCOMMISSION) != C_FLAG_SPECIALCOMMISSION)) || (CS.Active && Status != "A")))
                        {
                            I++;
                            //DiscountCommissionNumber++;
                            Comm = new CoverCommision()
                            {
                                //Name = CS.Name,
                                ObjectNo = ObjectNumber,
                                //DiscCommNo = DiscountCommissionNumber,
                                CoverageId = ActCoverageID,
                                PayToCode = CS.PayToCode,
                                CommissionType = CS.Type,
                                CommissionCode = CS.ID,
                                LevelDisc = i,
                                CoverageNo = CoverageNumber,
                                //Percentage = CS.Percentage == 0 ? CS.Percentage : CS.Amount,
                                Percentage = CS.Percentage != 0 ? CS.Percentage : CS.Amount,
                                CurrId = CurrencyID,
                                InterestNo = InterestNumber,
                                ApplyCode = Convert.ToString(CS.ApplyFlags),
                                OrderNo = OrderNo,

                            };

                            if (CS.Percentage == 0)
                            {
                                RealCommission = CS.Amount;
                            }
                            else
                            {
                                switch (i)
                                {
                                    case 0:
                                        RealCommission = PS.GrossPremium * (CS.Percentage / 100);
                                        break;
                                    case 1:
                                        RealCommission = PS.Net1 * (CS.Percentage / 100);
                                        break;
                                    case 2:
                                        RealCommission = PS.Net2 * (CS.Percentage / 100);
                                        break;
                                    case 3:
                                        RealCommission = PS.Net3 * (CS.Percentage / 100);
                                        break;
                                    case 4:
                                        RealCommission = PS.NetPremium * (CS.Percentage / 100);
                                        break;
                                }
                            }
                            Comm.Amount = Status == "A" ? RealCommission : -1 * RealCommission;
                            Comm.Status = "A";
                            //Comm.NonStandard = 0;                        
                            //if (CS.Percentage == 0)
                            //{
                            //    if (AllowNonStandard(8) && CS.Amount > CS.FlatAmount)
                            //    {
                            //        NonStandard = true;
                            //        //Comm.NonStandard = 1;
                            //    }
                            //}
                            //else
                            //{
                            //    if (AllowNonStandard(8) && CS.Amount > CS.Percentage)
                            //    {
                            //        NonStandard = true;
                            //        //Comm.NonStandard = 1;
                            //}
                            //}
                            CoverComm.Add(Comm);
                        }
                    }

                }

            }
            #region SpecialCommission
            //if (HasSpecialCommission)
            //{

            //    arrResult = CalculateSpecialCommission(ProductCode, ActInterestID, ActCoverageID, FromPeriod, CoverFrom, CoverTo, OriginalRate, Rate);

            //    for (J = 0; J < arrResult.length; J++)
            //    {
            //        if (((arrResult[J].ApplyFlags & g_DiscountMap) == g_DiscountMap) && ((arrResult[J].ApplyFlags & C_FLAG_SPECIALCOMMISSION) == C_FLAG_SPECIALCOMMISSION))
            //        {
            //            I++;
            //            CommissionIndex++;
            //            DiscountCommissionNumber++;
            //            AddInfo(strHeader + CommissionIndex + '.ObjectNumber', ObjectNumber);
            //            AddInfo(strHeader + CommissionIndex + '.InterestNumber', InterestNumber);
            //            AddInfo(strHeader + CommissionIndex + '.DiscountCommissionNumber', DiscountCommissionNumber);
            //            AddInfo(strHeader + CommissionIndex + '.CoverageId', ActCoverageID);
            //            AddInfo(strHeader + CommissionIndex + '.PayToCode', arrResult[J].PayToParty);
            //            AddInfo(strHeader + CommissionIndex + '.CommissionType', '2');
            //            AddInfo(strHeader + CommissionIndex + '.CommissionCode', arrResult[J].ID);
            //            AddInfo(strHeader + CommissionIndex + '.Name', arrResult[J].Name);
            //            AddInfo(strHeader + CommissionIndex + '.LevelDiscount', J);
            //            AddInfo(strHeader + CommissionIndex + '.CoverageNumber', CoverageNumber);
            //            AddInfo(strHeader + CommissionIndex + '.Percentage', arrResult[J].Percentage);
            //            AddInfo(strHeader + CommissionIndex + '.OriginalPercentage', arrResult[J].Percentage);
            //            AddInfo(strHeader + CommissionIndex + '.NonStandard', 0);
            //            AddInfo(strHeader + CommissionIndex + '.CurrencyId', CurrencyID);
            //            switch (arrResult[J].Level)
            //            {
            //                case 0:
            //                    RealCommission = PS.GrossPremium * (arrResult[J].Percentage / 100);
            //                    break;
            //                case 1:
            //                    RealCommission = PS.Net1 * (arrResult[J].Percentage / 100);
            //                    break;
            //                case 2:
            //                    RealCommission = PS.Net2 * (arrResult[J].Percentage / 100);
            //                    break;
            //                case 3:
            //                    RealCommission = PS.Net3 * (arrResult[J].Percentage / 100);
            //                    break;
            //                case 4:
            //                    RealCommission = PS.NetPremium * (arrResult[J].Percentage / 100);
            //                    break;
            //            }
            //            if (Status == 'A')
            //            {
            //                AddInfo(strHeader + CommissionIndex + '.Amount', RealCommission);
            //            }
            //            else
            //            {
            //                AddInfo(strHeader + CommissionIndex + '.Amount', -RealCommission);
            //            }
            //            AddInfo(strHeader + CommissionIndex + '.Status', 'A');
            //        }
            //    }
            //}

            //arrReturn[0] = I;
            //arrReturn[1] = DiscountCommissionNumber;
            //arrReturn[2] = CommissionIndex;
            //arrReturn[3] = NonStandard;
            #endregion

            return CoverComm;
        }
        public static CoverCommision SaveDiscountToDataPool(string OrderNo, PremiumScheme PS, int ObjectNumber, int InterestNumber, string CurrencyID, int CoverageNumber, string ActCoverageID, List<CommissionScheme> DiscountList, decimal CoverPremium, string Status)
        {

            int I = 0;
            decimal lAmount = 0;

            decimal CommPercentage = 0;
            bool NonStandard = false;
            CoverCommision Comm = new CoverCommision();

            for (int i = 0; i < DiscountList.Count; i++)
            {
                if (DiscountList[i].Schemes != null)
                {

                    for (int j = 0; j < DiscountList[i].Schemes.Count; j++)
                    {
                        var DS = DiscountList[i].Schemes[j];

                        if (DS.Active || (DS.Active && Status != "A"))
                        {
                            I++;
                            if (DS.Type == '3')
                            {
                                if (DS.Percentage == 0)
                                {
                                    CommPercentage = 0;
                                }
                                else
                                {
                                    CommPercentage = DS.Percentage - DS.Amount;
                                }
                                Comm = new CoverCommision()
                                {
                                    //Name = CS.Name,
                                    ObjectNo = ObjectNumber,
                                    //DiscCommNo = DiscountCommissionNumber,
                                    CoverageId = ActCoverageID,
                                    PayToCode = DS.PayToCode,
                                    CommissionType = DS.Type,
                                    CommissionCode = DS.ID,
                                    LevelDisc = i,
                                    CoverageNo = CoverageNumber,
                                    Percentage = DS.Percentage != 0 ? DS.Percentage : DS.Amount,
                                    CurrId = CurrencyID,
                                    InterestNo = InterestNumber,
                                    ApplyCode = Convert.ToString(DS.ApplyFlags),
                                    OrderNo = OrderNo,

                                };


                                if (DS.Percentage == 0)
                                {
                                    lAmount = CommPercentage;
                                }
                                else
                                {
                                    switch (i)
                                    {
                                        case 0:
                                            lAmount = PS.GrossPremium * (CommPercentage / 100);
                                            break;
                                        case 1:
                                            lAmount = PS.Net1 * (CommPercentage / 100);
                                            break;
                                        case 2:
                                            lAmount = PS.Net2 * (CommPercentage / 100);
                                            break;
                                        case 3:
                                            lAmount = PS.Net3 * (CommPercentage / 100);
                                            break;
                                        case 4:
                                            lAmount = PS.NetPremium * (CommPercentage / 100);
                                            break;
                                    }
                                }

                                if (Status == "A")
                                {
                                    Comm.Amount = lAmount;
                                }
                                else
                                {
                                    Comm.Amount = -1 * lAmount;
                                }
                                Comm.Status = "A";
                            }

                            //DiscountCommissionNumber++;
                            Comm = new CoverCommision()
                            {
                                //Name = CS.Name,
                                ObjectNo = ObjectNumber,
                                //DiscCommNo = DiscountCommissionNumber,
                                CoverageId = ActCoverageID,
                                InterestNo = InterestNumber,
                                PayToCode = DS.PayToCode,
                                CommissionType = DS.Type,
                                CommissionCode = DS.ID,
                                LevelDisc = i,
                                CoverageNo = CoverageNumber,
                                Percentage = DS.Percentage != 0 ? DS.Percentage : DS.Amount,
                                CurrId = CurrencyID,
                                ApplyCode = Convert.ToString(DS.ApplyFlags),
                                OrderNo = OrderNo,

                            };
                            if (DS.Percentage == 0)
                            {
                                lAmount = DS.Amount;
                            }
                            else
                            {
                                switch (i)
                                {
                                    case 0:
                                        lAmount = PS.CoverPremium * (DS.Percentage / 100);
                                        break;
                                    case 1:
                                        lAmount = PS.GrossPremium * (DS.Percentage / 100);
                                        break;
                                    case 2:
                                        lAmount = PS.Net1 * (DS.Percentage / 100);
                                        break;
                                    case 3:
                                        lAmount = PS.Net2 * (DS.Percentage / 100);
                                        break;
                                    case 4:
                                        lAmount = PS.Net3 * (DS.Percentage / 100);
                                        break;
                                }
                            }

                            if (Status == "A")
                            {
                                Comm.Amount = lAmount;
                            }
                            else
                            {
                                Comm.Amount = -lAmount;
                            }
                            Comm.Status = "A";


                            if (DS.Percentage == 0)
                            {
                                if (AllowNonStandard(4) && DS.Amount > DS.FlatAmount)
                                {
                                    NonStandard = true;
                                    //Comm.NonStandard = 1;
                                }
                            }
                            else
                            {
                                if (AllowNonStandard(4) && DS.Amount > DS.Percentage)
                                {
                                    NonStandard = true;
                                    //Comm.NonStandard = 1;
                                }

                            }
                        }

                    }
                }
            }
            return Comm;
        }
        private static bool AllowNonStandard(int ElementFlag)
        {
            bool Allowed = false;
            var NonStandardFlag = 0;
            int C_ALLOW_NONSTANDARD_CAPTION = 0;

            NonStandardFlag = C_ALLOW_NONSTANDARD_CAPTION;
            Allowed = (NonStandardFlag & ElementFlag) == ElementFlag ? true : false;


            return (Allowed);
        }

    }

    public class PremiumScheme
    {
        public decimal CoverPremium { get; set; }
        public decimal GrossPremium { get; set; }
        public decimal NetPremium { get; set; }
        public decimal Net1 { get; set; }
        public decimal Net2 { get; set; }
        public decimal Net3 { get; set; }
    }

    public class WTCommission
    {
        public string QuotationNo { get; set; }
        public string CommissionID { get; set; }
        public string CommissionName { get; set; }
        public string CoverageID { get; set; }
        public decimal Percentage { get; set; }
        public string PayToParty { get; set; }
        public int Level { get; set; }
        public decimal IndividualTax { get; set; }
        public decimal CorporateTax { get; set; }
        public int CommissionType { get; set; }
        public int ApplyFlags { get; set; }
    }

    public class CScheme
    {
        public bool Active { get; set; }
        public string ID { get; set; }
        public string Name { get; set; }
        public int Type { get; set; }
        public decimal Percentage { get; set; }
        public decimal FlatAmount { get; set; }
        public decimal Amount { get; set; }
        public string PayToCode { get; set; }
        public int ApplyFlags { get; set; }
        public decimal SI = -1;
        public decimal OriginalFlatAmount = -1;
    }

    public class CommissionScheme
    {
        public decimal TotalFlatAmount { get; set; }
        public decimal TotalPercentage { get; set; }
        public int Level { get; set; }
        public List<CScheme> Schemes { get; set; }
    }

    public class Commission
    {
        public string Name { get; set; }
        public int ObjectNumber { get; set; }
        public int InterestNumber { get; set; }
        public int DiscountCommissionNumber { get; set; }
        public string CoverageId { get; set; }
        public int PayToCode { get; set; }
        public int CommissionType = 2;
        public string CommissionCode { get; set; }
        public int LevelDiscount { get; set; }
        public int CoverageNumber { get; set; }
        public decimal Percentage { get; set; }

        public string CurrencyID { get; set; }
        public decimal Amount { get; set; }
        public int NonStandard { get; set; }
        public string Status { get; set; }
    }
}
