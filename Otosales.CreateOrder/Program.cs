﻿using a2is.Framework.Monitoring;
using Otosales.CreateOrder.Repository;
using Otosales.Logic;
using Otosales.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otosales.CreateOrder
{
    class Program
    {
        private static a2isLogHelper logger = new a2isLogHelper();
        static string GetCurrentMethodName()
        {
            return System.Reflection.MethodBase.GetCurrentMethod().Name;

        }

        static void Main(string[] args)
        {
            int schedulertype = Convert.ToInt32(ConfigurationManager.AppSettings["SchedulerType"]);
            if (schedulertype==1)
            {
                CreateOrder();
            }
            else if (schedulertype==2) {
                ReCreateOrder();
            }
        }

        static void HandleError(string followUpNo, string SalesOfficerID, string UserID) {
            string CurrentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            OrderRepository OR = new OrderRepository();
            OtosalesOrderRepository OOR = new OtosalesOrderRepository();
            Order O = new Order();
            bool isRetry = true;
            int TryCount = 0;
            string OrderNo = string.Empty;
            try
            {
                while (isRetry) {
                    try
                    {
                        logger.Debug(string.Format("Function {0} BEGIN:'", CurrentMethod));
                        ObjectRepository ObjRep = new ObjectRepository();
                        OrderRepository OrdRep = new OrderRepository();
                        OrderResult saveOrder = new OrderResult();

                        List<bool> result = new List<bool>();

                        O = OrderLogic.SetOrderData(followUpNo);
                        if (((string)SalesOfficerID).Length > 3)
                            O.AgencyInfo = OR.GetAgencyInfo(SalesOfficerID);

                        OrderNo = O.OrderData.OrderNo;
                        if (string.IsNullOrEmpty(UserID))
                        {
                            UserID = SalesOfficerID;
                        }
                        if (string.IsNullOrEmpty(O.OrderData.OrderNo))
                        {
                            O.OrderData.OrderNo = OR.GetOrderNo(followUpNo);
                        }
                        if (!string.IsNullOrEmpty(O.OrderData.OrderNo)) {
                            saveOrder = OrderLogic.DoSaveOrder(O, UserID);
                            var rslt = OrdRep.UpdateAnalysisOrder(O.OrderData.OrderNo);
                            if (!rslt)
                            {
                                logger.Error("Error while generating Mst_Order.Analysis");
                                CommonLogic.InsertErrorLog(string.IsNullOrEmpty(OrderNo) ? followUpNo : O.OrderData.OrderNo, CurrentMethod,
                                "Error while generating Mst_Order.Analysis", 1);

                            }
                            if (string.IsNullOrWhiteSpace(saveOrder.ErrorInfo))
                            {
                                logger.Debug(string.Format("Function {0} OK!'", CurrentMethod));
                                var OK = new
                                {
                                    status = true,
                                    message = "OK",
                                    data = new { result = true, OrderNo = O.OrderData.OrderNo }
                                };
                                isRetry = false;
                            }
                            else
                            {
                                logger.Debug(string.Format("Function {0} OK!'", CurrentMethod));
                                var OK = new
                                {
                                    status = false,
                                    message = saveOrder.ErrorInfo,
                                };
                            }
                        }
                    }
                    catch (Exception e2)
                    {
                        try
                        {
                            OR.DeleteAABTransaction(OrderNo, O.OrderData.TitanOrderNo);
                            TryCount++;
                            if (TryCount > 3)
                            {
                                isRetry = false;
                            }
                        }
                        catch (Exception e3)
                        {
                            TryCount++;
                            if (TryCount > 3)
                            {
                                isRetry = false;
                            }
                            logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e3.ToString(), e3.Message));
                            CommonLogic.InsertErrorLog(string.IsNullOrEmpty(OrderNo) ? followUpNo : O.OrderData.OrderNo, CurrentMethod,
                            string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e3.ToString(), e3.Message), 1);

                        }
                        TryCount++;
                        if (TryCount > 3)
                        {
                            isRetry = false;
                        }
                        logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e2.ToString(), e2.Message));
                        CommonLogic.InsertErrorLog(string.IsNullOrEmpty(OrderNo) ? followUpNo : O.OrderData.OrderNo, CurrentMethod,
                        string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e2.ToString(), e2.Message), 1);
                        var OK = new { status = false, message = "Server / Network Error. Please contact IT helper to solve this issue.", data = e2 };
                    }
                }
            }
            catch (Exception e)
            {
                TryCount++;
                if (TryCount > 3)
                {
                    isRetry = false;
                }
                logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.Message));
                CommonLogic.InsertErrorLog(string.IsNullOrEmpty(OrderNo) ? followUpNo : O.OrderData.OrderNo, CurrentMethod,
                string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.Message), 1);
                var OK = new { status = false, message = "Server / Network Error. Please contact IT helper to solve this issue.", data = e };
            }
        }

        static void CreateOrder() {
            string OrderNo = string.Empty;
            Order O = new Order();
            string SalesOfficerID = string.Empty;
            string followUpNo = string.Empty;
            string UserID = "";
            string CurrentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                logger.Debug(string.Format("Function {0} BEGIN:'", CurrentMethod));
                ObjectRepository ObjRep = new ObjectRepository();
                OrderRepository OrdRep = new OrderRepository();

                List<bool> result = new List<bool>();

                //BEGIN : BLO
                OrderRepository OR = new OrderRepository();
                OtosalesOrderRepository OOR = new OtosalesOrderRepository();
                List<dynamic> listFollowUp = OOR.GetFollowUpNeedSurveySendToSAandFUPaymentWhereOrderNotCreated();
                foreach (var fu in listFollowUp)
                {
                    try
                    {
                        OrderResult saveOrder = new OrderResult();
                        int IsNeedNSAApproval = 0;

                        followUpNo = fu.FollowUpNo;
                        bool isRenewal = fu.IsRenewal;

                        // insert dlu customer jadi prospect jika new
                        if (!fu.IsRenewal)
                        {
                            OrderLogic.SetOtosalesCustomerToProspect(followUpNo);
                        }
                        else
                        {
                            OrderLogic.UpdateRenewalCustomerInfo(followUpNo);
                            ImageLogic.UploadImageRenewal(followUpNo);
                        }

                        SetOrderNoResult ordNoResult = OrderLogic.SetOtosalesBookingOrderNo(followUpNo, isRenewal);
                        UserID = ordNoResult.UserID;
                        if (!ordNoResult.IsOrderNoExist)
                        {
                            O = OrderLogic.SetOrderData(followUpNo);
                            if (((string)fu.SalesOfficerID).Length > 3)
                                O.AgencyInfo = OR.GetAgencyInfo(fu.SalesOfficerID);

                            if (string.IsNullOrEmpty(UserID))
                            {
                                UserID = fu.SalesOfficerID;
                            }
                            OrderNo = O.OrderData.OrderNo;
                            SalesOfficerID = fu.SalesOfficerID;
                            if (string.IsNullOrEmpty(O.OrderData.OrderNo))
                            {
                                O.OrderData.OrderNo = OR.GetOrderNo(followUpNo);
                            }
                            if (!string.IsNullOrEmpty(O.OrderData.OrderNo))
                            {
                                saveOrder = OrderLogic.DoSaveOrder(O, UserID);
                                var rslt = OrdRep.UpdateAnalysisOrder(O.OrderData.OrderNo);
                                if (!rslt)
                                {
                                    logger.Error("Error while generating Mst_Order.Analysis");
                                    CommonLogic.InsertErrorLog(string.IsNullOrEmpty(OrderNo) ? followUpNo : O.OrderData.OrderNo, CurrentMethod,
                                    "Error while generating Mst_Order.Analysis", 1);
                                }
                                if (string.IsNullOrWhiteSpace(saveOrder.ErrorInfo))
                                {
                                    logger.Debug(string.Format("Function {0} OK!'", CurrentMethod));
                                    var OK = new
                                    {
                                        status = true,
                                        message = "OK",
                                        data = new { result = true, OrderNo = O.OrderData.OrderNo, IsNeedNSAApproval = IsNeedNSAApproval }
                                    };
                                }
                                else
                                {
                                    logger.Debug(string.Format("Function {0} OK!'", CurrentMethod));
                                    var OK = new
                                    {
                                        status = false,
                                        message = saveOrder.ErrorInfo,

                                    };
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        try
                        {
                            if (!string.IsNullOrEmpty(OrderNo))
                            {
                                OR.DeleteAABTransaction(OrderNo, O.OrderData.TitanOrderNo);
                                HandleError(followUpNo, SalesOfficerID, UserID);
                            }
                            else
                            {
                                OrderLogic.setOrderNonActive(followUpNo);
                                CommonLogic.InsertErrorLog(string.IsNullOrEmpty(OrderNo) ? followUpNo : O.OrderData.OrderNo, CurrentMethod,
                                "Set RowStatus = 0. Error :" + string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.Message), 1);
                            }
                        }
                        catch (Exception e2)
                        {
                            logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e2.ToString(), e2.Message));
                            CommonLogic.InsertErrorLog(string.IsNullOrEmpty(OrderNo) ? followUpNo : O.OrderData.OrderNo, CurrentMethod,
                            string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e2.ToString(), e2.Message), 1);
                        }
                        logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.Message));
                        CommonLogic.InsertErrorLog(string.IsNullOrEmpty(OrderNo) ? followUpNo : O.OrderData.OrderNo, CurrentMethod,
                        string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.Message), 1);
                        var OK = new { status = false, message = "Server / Network Error. Please contact IT helper to solve this issue.", data = e };
                    }
                }

                //END : BLO
                logger.Debug(string.Format("Function {0} END:'", CurrentMethod));
            }
            catch (Exception e)
            {
                logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.Message));
                CommonLogic.InsertErrorLog(string.IsNullOrEmpty(OrderNo) ? followUpNo : O.OrderData.OrderNo, CurrentMethod,
                string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.Message), 1);
                var OK = new { status = false, message = "Server / Network Error. Please contact IT helper to solve this issue.", data = e };
            }
        }

        static void ReCreateOrder() {
            string CurrentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            OrderRepository OR = new OrderRepository();
            OtosalesOrderRepository OOR = new OtosalesOrderRepository();
            Order O = new Order();
            string UserID = "";

            string OrderNo = string.Empty;
            try
            {
                logger.Debug(string.Format("Function {0} BEGIN:'", CurrentMethod));
                ObjectRepository ObjRep = new ObjectRepository();
                OrderRepository OrdRep = new OrderRepository();
                OrderResult saveOrder = new OrderResult();

                List<dynamic> listFollowUp = OOR.GetFollowUpSendToSAwithSurvey();
                foreach (var fu in listFollowUp)
                {
                    try
                    {
                        int TitanOrderNo = OR.GetExistTitanOrderNo(fu.Order_No);
                        OR.DeleteAABTransactionAfterSurvey(fu.Order_No,TitanOrderNo);
                        O = OrderLogic.SetOrderData(fu.FollowUpNo);
                        if (((string)fu.SalesOfficerID).Length > 3)
                            O.AgencyInfo = OR.GetAgencyInfo(fu.SalesOfficerID);

                        OrderNo = O.OrderData.OrderNo;
                        if (string.IsNullOrEmpty(UserID))
                        {
                            UserID = fu.SalesOfficerID;
                        }
                        if (string.IsNullOrEmpty(O.OrderData.OrderNo))
                        {
                            O.OrderData.OrderNo = OR.GetOrderNo(fu.FollowUpNo);
                        }
                        if (!string.IsNullOrEmpty(O.OrderData.OrderNo))
                        {
                            saveOrder = OrderLogic.DoSaveOrderAfterSurvey(O, UserID);
                            var rslt = OrdRep.UpdateAnalysisOrder(O.OrderData.OrderNo);
                            if (!rslt)
                            {
                                logger.Error("Error while generating Mst_Order.Analysis");
                                CommonLogic.InsertErrorLog(string.IsNullOrEmpty(OrderNo) ? fu.FollowUpNo : O.OrderData.OrderNo, CurrentMethod,
                                "Error while generating Mst_Order.Analysis", 1);

                            }
                            if (string.IsNullOrWhiteSpace(saveOrder.ErrorInfo))
                            {
                                logger.Debug(string.Format("Function {0} OK!'", CurrentMethod));
                                var OK = new
                                {
                                    status = true,
                                    message = "OK",
                                    data = new { result = true, OrderNo = O.OrderData.OrderNo }
                                };
                            }
                            else
                            {
                                logger.Debug(string.Format("Function {0} OK!'", CurrentMethod));
                                var OK = new
                                {
                                    status = false,
                                    message = saveOrder.ErrorInfo,
                                };
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.Message));
                        CommonLogic.InsertErrorLog(string.IsNullOrEmpty(OrderNo) ? fu.followUpNo : O.OrderData.OrderNo, CurrentMethod,
                        string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.Message), 1);
                    }
                }
            }
            catch (Exception e)
            {
                logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.Message));
            }
        }
    }
}
