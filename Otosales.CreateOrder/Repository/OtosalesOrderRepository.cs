﻿using Otosales.Logic;
using Otosales.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Otosales.Repository;
using static Otosales.Models.Constant;
using System.Dynamic;

namespace Otosales.CreateOrder.Repository
{
    class OtosalesOrderRepository : RepositoryBase
    {
        #region FollowUp
        
        public List<dynamic> GetFollowUpSenToSurveyorOrSA(bool renewal = false)
        {
            string query = @";select fu.* from FollowUP fu
                            inner join OrderSimulation os on fu.FollowUpNo = os.FollowUpNo and os.ApplyF = 1
                            inner join FollowUpStatus fus on fu.FollowUpStatus = fus.StatusCode
                            inner join FollowUpStatusInfo fusi on fu.FollowUpInfo = fusi.InfoCode
                            where (fusi.InfoCode = @0 OR fusi.InfoCode = @1) and ISNULL(IsRenewal,0) = @2
							AND COALESCE(os.PolicyOrderNo,'') = '' AND COALESCE(os.SurveyNo,'') = ''
							ORDER BY fu.CustID"; // fu.FollowUpNo = 2b562b75-c38d-445a-a812-2ff33fe54656

            using (var db = GetAABMobileDB())
            {
                return db.Fetch<dynamic>(query, FollowUpInfo.SendToSurveyor, FollowUpInfo.SendToSA, renewal);
            }
        }

        public List<dynamic> GetFollowUpNeedSurveySendToSAandFUPaymentWhereOrderNotCreated()
        {
            string query = @";select SurveyNo, PolicyOrderNo, os.SalesOfficerID, fu.FollowUpNo , Cast(ISNULL(IsRenewal,0) as bit) [IsRenewal]
                            from FollowUP fu
                            inner join OrderSimulation os on fu.FollowUpNo = os.FollowUpNo and os.ApplyF = 1
                            where COALESCE(os.PolicyOrderNo,'') = '' and fu.FollowUpInfo IN(@0,@1,@2)  
							AND os.RowStatus = 1 AND fu.RowStatus = 1
							"; // fu.FollowUpNo = '1faef67c-9609-453f-a9c9-49409cf7fb0d'

            using (var db = GetAABMobileDB())
            {
                return db.Fetch<dynamic>(query, FollowUpInfo.SendToSurveyor, FollowUpInfo.SendToSA, FollowUpInfo.FUPayment);
            }
        }

        public List<dynamic> GetFollowUpSendToSAwithSurvey()
        {
            #region old
            //            string query = @";SELECT  os.FollowUpNo ,
            //        os.OrderNo ,
            //        os.PolicyOrderNo ,
            //        os.PeriodFrom ,
            //        os.PeriodTo ,
            //        os.SurveyNo ,
            //        os.SalesOfficerID ,
            //        osc.CoverageID ,
            //        ( SELECT DISTINCT
            //                    COALESCE(SUM(CAST(DATEDIFF(MONTH, BeginDate, EndDate)
            //                                 / 12.0 AS DECIMAL(10, 2))), 0) AS YearCoverage
            //          FROM      dbo.OrderSimulationCoverage WITH ( NOLOCK )
            //          WHERE     CoverageID = 'ALLRIK'
            //                    AND InterestNo = 1
            //                    AND OrderNo = os.OrderNo
            //        ) CYear ,
            //        ( SELECT DISTINCT
            //                    COALESCE(SUM(CAST(DATEDIFF(MONTH, BeginDate, EndDate)
            //                                 / 12.0 AS DECIMAL(10, 2))), 0) AS YearCoverage
            //          FROM      dbo.OrderSimulationCoverage WITH ( NOLOCK )
            //          WHERE     CoverageID = 'TLO'
            //                    AND InterestNo = 1
            //                    AND OrderNo = os.OrderNo
            //        ) TYear
            //INTO    #TempUpdateHasilSurvey
            //FROM    AABMobile.dbo.OrderSimulation os
            //        INNER JOIN AABMobile.dbo.FollowUp f ON f.FollowUpNo = os.FollowUpNo
            //        INNER JOIN AABMobile.dbo.OrderSimulationCoverage osc ON osc.OrderNo = os.OrderNo
            //                                                              --AND osc.ObjectNo = 1
            //                                                              AND osc.InterestNo = 1
            //                                                              AND osc.CoverageNo = 1
            //WHERE   os.RowStatus = 1
            //        AND os.ApplyF = 1
            //        AND f.RowStatus = 1
            //        AND f.FollowUpStatus = 2
            //        AND f.FollowUpInfo = 61
            //        AND SurveyNo IS NOT NULL

            //SELECT  a.FollowUpNo ,
            //        a.OrderNo ,
            //        mo.Order_No ,
            //        a.SalesOfficerID
            //FROM    #TempUpdateHasilSurvey a
            //        INNER JOIN BEYONDREPORT.AAB.dbo.Mst_Order mo ON mo.Order_No = a.PolicyOrderNo
            //        INNER JOIN BEYONDREPORT.AAB.dbo.Ord_Dtl_Coverage odc ON mo.Order_No = odc.Order_No
            //                                                              --AND odc.Object_No = 1
            //                                                              AND odc.Interest_No = 1
            //                                                              AND odc.Coverage_No = 1
            //WHERE   ( CAST(a.PeriodFrom AS DATE) <> CAST(mo.Period_From AS DATE)
            //          OR CAST(a.PeriodTo AS DATE) <> CAST(mo.Period_To AS DATE)
            //          OR a.CoverageID <> odc.Coverage_Id
            //          OR a.CYear <> ( SELECT DISTINCT
            //                                    COALESCE(SUM(CAST(DATEDIFF(MONTH,
            //                                                              odc1.Begin_Date,
            //                                                              odc1.End_Date)
            //                                                 / 12.0 AS DECIMAL(10, 2))), 0)
            //                          FROM      BEYONDREPORT.AAB.dbo.Ord_Dtl_Coverage odc1
            //                                    WITH ( NOLOCK )
            //                          WHERE     odc1.Coverage_Id = 'ALLRIK'
            //                                    AND odc1.Interest_Type = 'P'
            //                                    AND odc1.Order_No = mo.Order_No
            //                        )
            //          OR a.TYear <> ( SELECT DISTINCT
            //                                    COALESCE(SUM(CAST(DATEDIFF(MONTH,
            //                                                              odc2.Begin_Date,
            //                                                              odc2.End_Date)
            //                                                 / 12.0 AS DECIMAL(10, 2))), 0)
            //                          FROM      BEYONDREPORT.AAB.dbo.Ord_Dtl_Coverage odc2
            //                                    WITH ( NOLOCK )
            //                          WHERE     odc2.Coverage_Id = 'TLO'
            //                                    AND odc2.Interest_Type = 'P'
            //                                    AND odc2.Order_No = mo.Order_No
            //                        )
            //        )
            //        AND mo.Order_Status = '50'

            //DROP TABLE #TempUpdateHasilSurvey"; // fu.FollowUpNo = 'c09009ee-1a0c-440b-9817-d37bebbab8ee' 
            #endregion
            string query = @";SELECT  os.FollowUpNo ,
        os.OrderNo ,
        os.PolicyOrderNo ,
        os.PeriodFrom ,
        os.PeriodTo ,
        os.SurveyNo ,
        os.SalesOfficerID ,
        osc.CoverageID ,
		mom1.Order_ID,
        ( SELECT DISTINCT
                    COALESCE(SUM(CAST(DATEDIFF(MONTH, BeginDate, EndDate)
                                 / 12.0 AS DECIMAL(10, 2))), 0) AS YearCoverage
          FROM      dbo.OrderSimulationCoverage WITH ( NOLOCK )
          WHERE     CoverageID = 'ALLRIK'
                    AND InterestNo = 1
                    AND OrderNo = os.OrderNo
        ) CYear ,
        ( SELECT DISTINCT
                    COALESCE(SUM(CAST(DATEDIFF(MONTH, BeginDate, EndDate)
                                 / 12.0 AS DECIMAL(10, 2))), 0) AS YearCoverage
          FROM      dbo.OrderSimulationCoverage WITH ( NOLOCK )
          WHERE     CoverageID = 'TLO'
                    AND InterestNo = 1
                    AND OrderNo = os.OrderNo
        ) TYear
INTO    #TempUpdateHasilSurvey
FROM    AABMobile.dbo.OrderSimulation os
        INNER JOIN AABMobile.dbo.FollowUp f ON f.FollowUpNo = os.FollowUpNo
        INNER JOIN AABMobile.dbo.OrderSimulationCoverage osc ON osc.OrderNo = os.OrderNo
                                                              --AND osc.ObjectNo = 1
                                                              AND osc.InterestNo = 1
                                                              AND osc.CoverageNo = 1
		INNER JOIN (​ select Order_No, max(Order_ID) [Order_ID]​
					from BEYONDREPORT.AAB.dbo.mst_order_mobile mom WITH ( NOLOCK )​
					group by Order_No​ ) mom1 ON mom1.Order_No = os.PolicyOrderNo
WHERE   os.RowStatus = 1
        AND os.ApplyF = 1
        AND f.RowStatus = 1
        AND f.FollowUpStatus = 2
        AND f.FollowUpInfo = 61
        AND SurveyNo IS NOT NULL


SELECT  a.FollowUpNo ,
        a.OrderNo ,
        mo.Order_No ,
        a.SalesOfficerID
FROM    #TempUpdateHasilSurvey a
        INNER JOIN BEYONDREPORT.AAB.dbo.Mst_Order mo ON mo.Order_No = a.PolicyOrderNo
        INNER JOIN BEYONDREPORT.AAB.dbo.Ord_Dtl_Coverage odc ON mo.Order_No = odc.Order_No
                                                              --AND odc.Object_No = 1
                                                              AND odc.Interest_No = 1
                                                              AND odc.Coverage_No = 1
		INNER JOIN beyondreport.aab.dbo.mst_order_mobile mom2 ON mom2.Order_No = mo.Order_No AND mom2.Order_ID = a.Order_ID
WHERE   ( CAST(a.PeriodFrom AS DATE) <> CAST(mo.Period_From AS DATE)
          OR CAST(a.PeriodTo AS DATE) <> CAST(mo.Period_To AS DATE)
          OR a.CoverageID <> odc.Coverage_Id
          OR a.CYear <> ( SELECT DISTINCT
                                    COALESCE(SUM(CAST(DATEDIFF(MONTH,
                                                              odc1.Begin_Date,
                                                              odc1.End_Date)
                                                 / 12.0 AS DECIMAL(10, 2))), 0)
                          FROM      BEYONDREPORT.AAB.dbo.Ord_Dtl_Coverage odc1
                                    WITH ( NOLOCK )
                          WHERE     odc1.Coverage_Id = 'ALLRIK'
                                    AND odc1.Interest_Type = 'P'
                                    AND odc1.Order_No = mo.Order_No
                        )
          OR a.TYear <> ( SELECT DISTINCT
                                    COALESCE(SUM(CAST(DATEDIFF(MONTH,
                                                              odc2.Begin_Date,
                                                              odc2.End_Date)
                                                 / 12.0 AS DECIMAL(10, 2))), 0)
                          FROM      BEYONDREPORT.AAB.dbo.Ord_Dtl_Coverage odc2
                                    WITH ( NOLOCK )
                          WHERE     odc2.Coverage_Id = 'TLO'
                                    AND odc2.Interest_Type = 'P'
                                    AND odc2.Order_No = mo.Order_No
                        )
        )
		AND mom2.SA_State = 1
        AND mo.Order_Status = '50'

DROP TABLE #TempUpdateHasilSurvey";

            using (var db = GetAABMobileDB())
            {
                return db.Fetch<dynamic>(query, FollowUpInfo.SendToSurveyor, FollowUpInfo.SendToSA, FollowUpInfo.FUPayment);
            }
        }
        public dynamic GetFollowUpCustomer(string followUpNo, string followUpStatus = null)
        {
            string query = @"select os.PolicyOrderNo, pcs.CustID, pcs.Name, pcs.Phone1, pcs.Email1, pcs.ProspectID, pcs.PostalCode, pcs.CustAddress, pcm.CompanyName,
pcs.PostalCode AS OfficePostalCode, pcm.NPWP, convert(NVARCHAR, pcm.NPWPdate, 103) AS NPWPdate, pcm.NPWPaddress, pcm.OfficeAddress, pcm.PICPhoneNo, pcm.PICname,pcm.Email, CAST(COALESCE(pcs.isCompany,0) AS BIT) isCompany, pcs.IdentityNo,
pcs.CustBirthDay, pcs.CustGender, pcs.Phone2, fu.Remark, os.SalesOfficerID, fu.FollowUpInfo, pcm.NPWPno
from FollowUP fu 
INNER JOIN OrderSimulation os on fu.FollowUpNo = os.FollowUpNo and os.ApplyF = 1
INNER JOIN ProspectCustomer pcs on fu.CustID = pcs.CustID
LEFT JOIN ProspectCompany pcm on pcs.CustID = pcm.CustID
where fu.FollowUpNo = @0";
            if (followUpStatus!=null)
                query += " FollowUpInfo = @1 ";

            using (var db = GetAABMobileDB())
            {
                return db.First<dynamic>(query, followUpNo, FollowUpInfo.SendToSurveyor);
            }
        }


        public int SetFollowUpProspectID(string followUpNo, string prospectID)
        {
            string q = @"UPDATE pcs
SET ProspectID = @0
FROM FollowUp fu INNER JOIN ProspectCustomer pcs on fu.CustID = pcs.CustID
WHERE fu.FollowUpNo = @1";
            using (var db = GetAABMobileDB())
            {
               return db.Execute(q, prospectID, followUpNo);
            }
        }

        #endregion

        #region General
        
        public DateTime GetCurrentServerTime()
        {
            using (var db = GetAABMobileDB())
            {
                return db.First<DateTime>("select getdate()");
            }
        }
        
        public string GetSalesmanId(string salesOfficerID, string productCode)
        {
            string qGetSalesmanId = @"
select s.Salesman_Id, s.Branch_Id, b.Biz_type 
from mst_salesman s 
inner join Mst_Branch b on s.Branch_id = b.Branch_id
inner join Mst_Product p on p.Product_code = @1 and b.Biz_Type = p.biz_type
where s.status = 1 and user_id_otosales = @0 ";
            if (salesOfficerID.Length > 3)
            {
                // lebih dari 3 huruf berarti menggunakan agentcode
                var db = GetAABMobileDB();
                string GetSalesmanId = @";DECLARE @@BranchId VARCHAR(10)
                                        SELECT @@BranchId=REPLACE(BranchCode,'A','') FROM dbo.SalesOfficer WHERE SalesOfficerID = @0 AND RowStatus = 1
                                        SELECT Salesman_Id FROM BEYONDREPORT.AAB.dbo.Mst_Salesman WHERE Branch_Id = @@BranchId AND Name = 'Stephanus Rumawas' AND Status = 1";
                //salesOfficerID = CommonLogic.GetSettingAgencySalesmanId(); //TODO : kalo AGENT harusnya masuk ke SRS yang cabang mana / logicnya per cabangnya SRW ?
                List<dynamic> list = db.Fetch<dynamic>(GetSalesmanId, salesOfficerID);
                if (list.Count > 0)
                {
                    return list.First().Salesman_Id;
                }
                else {
                    return salesOfficerID;
                }
            }
            using (var db = GetAABDB())
            {
                var i = db.Fetch<dynamic>(qGetSalesmanId, salesOfficerID, productCode);
                if (i.Count>0)
                    return i.First().Salesman_Id;
                else
                    return salesOfficerID;
            }
        }

        public string GetBranchId(string salesOfficerID, string productCode, string BranchID)
        {
            string qGetSalesmanId = @"
select s.Salesman_Id, s.Branch_Id, b.Biz_type 
from mst_salesman s 
inner join Mst_Branch b on s.Branch_id = b.Branch_id
inner join Mst_Product p on p.Product_code = @1 and b.Biz_Type = p.biz_type
where s.status = 1 and user_id_otosales = @0 ";
            if (salesOfficerID.Length > 3)
            {
                // lebih dari 3 huruf berarti menggunakan agentcode
                //BranchID = CommonLogic.GetSettingAgencyBranchId(); //TODO : kalo AGENT harusnya masuk ke SRS yang cabang mana / logicnya per cabangnya SRW ?
                if (BranchID.StartsWith("A"))
                {
                    return BranchID.Substring(1, BranchID.Length - 1);
                }
                else {
                    return BranchID;
                }
            }
            using (var db = GetAABDB())
            {
                var i = db.Fetch<dynamic>(qGetSalesmanId, salesOfficerID, productCode);
                if (i.Count>0)
                    return i.First().Branch_Id;
                else
                    return BranchID;
            }
        }

        public dynamic GetSalesman(string salesOfficerID, string productCode, string BranchID) {
            dynamic result = new ExpandoObject();
            var aabDB = GetAABDB();
            var mblDB = GetAABMobileDB();
            try
            {
                string qGetSalesman = @";SELECT Branch_Id, Salesman_Id FROM dbo.Mst_Salesman 
WHERE Name = 'Stephanus Rumawas' AND Branch_Id = @0 AND Status = 1";
                int bizType = aabDB.ExecuteScalar<int>(@";DECLARE @@BizType INT = 0
SELECT @@BizType=Biz_type FROM dbo.Mst_Product WHERE Product_Code = @0 AND Status = 1
SELECT @@BizType", productCode);
                if (bizType == 2)
                {
                    BranchID = mblDB.ExecuteScalar<string>(@";
DECLARE @@BranchCode VARCHAR(6) = ''
SELECT @@BranchCode = BranchCodeSharia FROM dbo.SalesOfficer so 
INNER JOIN dbo.MappingBranchConveToSharia mcs
ON REPLACE(so.BranchCode,'A','')  = mcs.BranchCodeConve
WHERE so.SalesOfficerID = @0 AND so.RowStatus = 1
SELECT @@BranchCode", salesOfficerID);

                }
                if (BranchID.StartsWith("A")) {
                    BranchID = BranchID.Replace("A", "");
                }
                List<dynamic> lsSalesman = aabDB.Fetch<dynamic>(qGetSalesman, BranchID);
                if (lsSalesman.Count > 0)
                {
                    result.salesmanId = lsSalesman.First().Salesman_Id;
                    result.branchId = lsSalesman.First().Branch_Id;
                }
            }
            catch (Exception e)
            {

            }
            return result;
        }

        public int GetCalcMethodOrd(string OrderNo) {
            int calcMethod = 0;
            try
            {
                using (var db = GetAABMobileDB()) {
                    string query = @"SELECT TOP 1 CAST(osc.Calc_Method AS INT) Calc_Method, DATEDIFF(MONTH, os.PeriodFrom, os.PeriodTo) DiffMonth 
FROM dbo.OrderSimulation os INNER JOIN dbo.OrderSimulationInterest osi 
ON osi.OrderNo = os.OrderNo INNER JOIN dbo.OrderSimulationCoverage osc
ON osc.OrderNo = osi.OrderNo AND osc.ObjectNo = osi.ObjectNo AND osc.InterestNo = osi.InterestNo
WHERE osi.OrderNo = @0
AND InterestID = 'CASCO' AND os.RowStatus = 1 AND osi.RowStatus = 1 AND osc.RowStatus = 1";
                    List<dynamic> listCal = db.Fetch<dynamic>(query, OrderNo);
                    if (listCal.Count > 0) {
                        calcMethod = listCal.First().Calc_Method == null ? listCal.First().DiffMonth < 12 ? CalculationMethodEnum.Scale : CalculationMethodEnum.Prorate : listCal.First().Calc_Method;
                    }
                    return calcMethod;
                }
            }
            catch (Exception)
            {
                return calcMethod;
            }
        }

        public string GetGenericCoverageID(string CoverageID)
        {
            dynamic result = null;
            string query = @";DECLARE @@Value AS VARCHAR(100)
                            SELECT  TOP 1 @@Value=ConfigurationValue
                            FROM    dbo.GeneralConfiguration AS gc
                            WHERE   ConfigurationName = 'GENERIC_COVERAGE_ID_MAPPING'
                                AND ReferenceType = 'COVERAGE_ID'
	                            AND ReferenceID=@0
                            SELECT COALESCE (@@Value,@0) AS Result";
            using (var db = GetAABDB())
            {
                result = db.Query<string>(query, CoverageID).SingleOrDefault();
                return result;
            }
        }

        public string GetGenericInterestID(string InterestID)
        {
            dynamic result = null;
            string query = @";DECLARE @@Value AS VARCHAR(100)
                            SELECT  TOP 1 @@Value=ConfigurationValue
                            FROM    dbo.GeneralConfiguration AS gc
                            WHERE   ConfigurationName = 'GENERIC_INTEREST_ID_MAPPING'
                                AND ReferenceType = 'INTEREST_ID'
	                            AND ReferenceID=@0
                            SELECT COALESCE (@@Value,@0) AS Result";
            using (var db = GetAABDB())
            {
                result = db.Query<string>(query, InterestID).SingleOrDefault();
                return result;
            }
        }
        #endregion

        #region Survey
        public string GetCity(string CityID)
        {
            using (var db = GetAABMobileDB())
            {
                string CityDesc = string.Empty;
                string query = @"SELECT TOP 1 Name FROM AsuransiAstra.dbo.City WHERE ID=@0";
                CityDesc = db.Query<string>(query, CityID).SingleOrDefault();
                return CityDesc;
            }
        }
        public SurveyAddress GetSurveyAddress(string ZipCode)
        {
            string query = @"SELECT szc.ZipCode, szc.CityID, szc.DistrictCode, szc.GeoAreaCode, szc.ZipCodeDescription,  md.DistrictName, c.NAME AS CityName, c.PROVINCEID FROM [AABMobile].dbo.SurveyZipCode szc
                            LEFT JOIN AABMobile.dbo.mstDistrict md ON szc.DistrictCode=md.DistrictCode
                            LEFT JOIN Asuransiastra.dbo.City c ON szc.CityID=c.ID
                            WHERE ZipCode=@0";

            using (var db = GetAABMobileDB())
            {
                var result = db.Query<SurveyAddress>(query, ZipCode).FirstOrDefault();
                return result;
            }
        }
        public SurveyRegionCode GetSurveyRegionCode(string RegionCode)
        {
            string query = @"SELECT  TOP 1 mga.Geo_Area_Code AS GeoAreaCode, Sub_Geo_Area_Code AS SubGeoAreaCode, mga.Region_Code AS RegionCode
                            FROM    dbo.Mst_Geo_Area mga
                                    Left JOIN dbo.Mst_Sub_Geo_Area msga ON mga.Geo_Area_Code = msga.Geo_Area_Code                                                                            
		                            WHERE mga.Geo_Area_Code = @0";

            using (var db = GetAABDB())
            {
                var result = db.Query<SurveyRegionCode>(query, RegionCode).FirstOrDefault();
                return result;
            }

        }

        public dynamic GetGATimeCategoryCode(string ScheduleTimeId)
        {
            string cSQL = @"SELECT  TOP 1 TimeCategoryCode, ScheduleTime     
                            FROM    Otocare.dbo.ScheduleTime
                            WHERE   Category = 99
                                    AND ScheduleTimeID = @0";
            using (var db = GetAABMobileDB())
            {
                var result = db.Query<dynamic>(cSQL, ScheduleTimeId).FirstOrDefault();
                return result;
            }
        }

        public dynamic GetTimeCategoryCode(string ScheduleTimeId)
        {
            string cSQL = @"SELECT  TOP 1 TimeCategoryCode, ScheduleTime     
                            FROM    Otocare.dbo.ScheduleTime
                            WHERE   ScheduleTimeID = @0";
            using (var db = GetAABMobileDB())
            {
                var result = db.Query<dynamic>(cSQL, ScheduleTimeId).FirstOrDefault();
                return result;
            }
        }

        private bool SaveSurvey(List<SurveyInfo> Survey, string EntryUser)
        {
            bool isValid = false;
            if (SurveySaveDetail(Survey, EntryUser))
            {
                if (Survey.Count > 0)
                {
                    if (InsertLnkSurveyOrder(Survey.FirstOrDefault().ReferenceNo, Survey.FirstOrDefault().SurveyType, Survey.FirstOrDefault().SurveyNo, EntryUser))
                    {
                        isValid = true;
                    }
                }
            }
            return isValid;
        }

        public bool SurveySaveDetail(List<SurveyInfo> S, string EntryUser)
        {
            int result = 0;
            string query = @"INSERT  INTO dbo.Survey
                                ( Survey_No ,
                                  Object_No ,
                                  External_Surveyor ,
                                  EXT_SURVEYOR_ID ,
                                  Survey_Type ,
                                  Surveyor ,
                                  Survey_Date ,
                                  
                                  Pic_Name ,
                                  Payment_Mode ,
                                  Curr_Id ,
                                  Amount ,
                                  Phone_No ,
                                  Location ,
                                  RT ,
                                  RW ,
                                  Postal_Code ,
                                  Description ,
                                  Form_Id ,
                                  Status ,
                                  Notes ,
                                  Reference_No ,
                                  Area_Code ,
                                  Old_Area_Code ,
                                  Survey_Mode ,
                                  Region_Code ,
                                  Best_Time_Call ,
                                  Reject_Reason ,
                                  Result ,
                                  Policy_No ,
                                  Registration_No ,
                                  Duration ,
                                  Travel_Period ,
                                  OnCall ,
                                  Survey_Reference ,
                                  SubArea_Code ,
                                  Adjuster_Code ,
                                  SubCon_Code ,
                                  Payer_Code ,
                                  Payer_Flag ,
                                  Survey_Result ,
                                  Image_Count ,
                                  EntryUsr ,
                                  EntryDt ,                                  
                                  new_survey_type ,
                                  new_sub_survey_type ,
                                  remarks ,
                                  non_schedule ,
                                  Sub_Geo_Area_Code ,
                                  AddressFromSurvey ,
                                  City ,
                                  SurveyObject ,
                                  
                                  StickerDescription ,
                                  ResultRecommendation,
                                  From_Date,
                                  To_Date
		                        )
                        VALUES  ( @0 , -- Survey_No - char(8)
                                  @1 , -- Object_No - numeric
                                  @2 , -- External_Surveyor - bit
                                  @3 , -- EXT_SURVEYOR_ID - char(11)
                                  @4 , -- Survey_Type - char(6)
                                  @5 , -- Surveyor - char(5)
                                  @6 , -- Survey_Date - datetime                                  
                                  @7 , -- Pic_Name - varchar(100)
                                  @8 , -- Payment_Mode - char(6)
                                  @9 , -- Curr_Id - char(3)
                                  @10 , -- Amount - numeric
                                  @11 , -- Phone_No - varchar(16)
                                  @12 , -- Location - varchar(152)
                                  @13 , -- RT - char(3)
                                  @14 , -- RW - char(3)
                                  @15 , -- Postal_Code - char(8)
                                  @16 , -- Description - varchar(50)
                                  @17 , -- Form_Id - char(6)
                                  @18 , -- Status - char(1)
                                  @19 , -- Notes - text
                                  @20 , -- Reference_No - char(30)
                                  @21 , -- Area_Code - char(6)
                                  @22 , -- Old_Area_Code - char(6)
                                  @23 , -- Survey_Mode - char(1)
                                  @24 , -- Region_Code - char(6)
                                  @25 , -- Best_Time_Call - varchar(128)
                                  @26 , -- Reject_Reason - varchar(128)
                                  @27 , -- Result - text
                                  @28 , -- Policy_No - char(16)
                                  @29 , -- Registration_No - char(16)
                                  @30 , -- Duration - smallint
                                  @31 , -- Travel_Period - smallint
                                  @32 , -- OnCall - char(1)
                                  @33 , -- Survey_Reference - char(20)
                                  @34 , -- SubArea_Code - char(6)
                                  @35 , -- Adjuster_Code - char(11)
                                  @36 , -- SubCon_Code - char(11)
                                  @37 , -- Payer_Code - char(11)
                                  @38 , -- Payer_Flag - char(1)
                                  @39 , -- Survey_Result - char(1)
                                  @40 , -- Image_Count - int
                                  @41 , -- EntryUsr - char(5)
                                  GETDATE() , -- EntryDt - datetime                  
                                  @42 , -- new_survey_type - varchar(6)
                                  @43 , -- new_sub_survey_type - varchar(6)
                                  @44 , -- remarks - varchar(256)
                                  @45 , -- non_schedule - varchar(1)
                                  @46 , -- Sub_Geo_Area_Code - varchar(10)
                                  @47 , -- AddressFromSurvey - varchar(10)
                                  @48 , -- City - varchar(50)
                                  @49 , -- SurveyObject - int
                                  
                                  @50 , -- StickerDescription - varchar(500)
                                  @51 , -- ResultRecommendation - varchar(max)
                                  @52 , --From Date
                                  @53
		                        )";
            using (var db = GetAABDB())
            {
                for (int i = 0; i < S.Count; i++)
                {
                    result += db.Execute(query, IsNA(S[i].SurveyNo), IsNA(S[i].ObjectNo), IsNA(S[i].ExternalSurveyor), IsNA(S[i].EXTSURVEYORID), IsNA(S[i].SurveyType), IsNA(S[i].Surveyor), IsValidSurveyDate(S[i].SurveyDate), IsNA(S[i].PicName), IsNA(S[i].PaymentMode), IsNA(S[i].CurrId), IsNA(S[i].Amount), IsNA(S[i].PhoneNo), IsNA(S[i].Location), IsNA(S[i].RT), IsNA(S[i].RW), IsNA(S[i].PostalCode), IsNA(S[i].Description), IsNA(S[i].FormId), IsNA(S[i].Status), IsNA(S[i].Notes), IsNA(S[i].ReferenceNo), IsNA(S[i].AreaCode), IsNA(S[i].OldAreaCode), IsNA(S[i].SurveyMode), IsNA(S[i].RegionCode), IsNA(S[i].BestTimeCall), IsNA(S[i].RejectReason), IsNA(S[i].Result), IsNA(S[i].PolicyNo), IsNA(S[i].RegistrationNo), IsNA(S[i].Duration), IsNA(S[i].TravelPeriod), IsNA(S[i].OnCall), IsNA(S[i].SurveyReference), IsNA(S[i].SubAreaCode), IsNA(S[i].AdjusterCode), IsNA(S[i].SubConCode), IsNA(S[i].PayerCode), IsNA(S[i].PayerFlag), IsNA(S[i].SurveyResult), IsNA(S[i].ImageCount), EntryUser, IsNA(S[i].newsurveytype), IsNA(S[i].newsubsurveytype), IsNA(S[i].remarks), IsNA(S[i].nonschedule), IsNA(S[i].SubGeoAreaCode), IsNA(S[i].AddressFromSurvey), IsNA(S[i].City), IsNA(S[i].SurveyObject), IsNA(S[i].StickerDescription), IsNA(S[i].ResultRecommendation), IsValidSurveyDate(S[i].FromDate), IsValidSurveyDate(S[i].ToDate));
                }
            }
            return result >= S.Count;
        }

        public bool InsertLnkSurveyOrder(string ReferenceNo, string ReferenceType, string SurveyNo, string EntryUser)
        {
            int result = 0;
            string query = @"INSERT INTO dbo.Lnk_Survey_Order
                                    ( Reference_No ,
                                      Reference_Type ,
                                      Type_Number ,
                                      Survey_No ,
                                      EntryUsr ,
                                      EntryDt 
                                    )
                            VALUES  ( @0 , -- Reference_No - char(16)
                                      @1 , -- Reference_Type - char(6)
                                      0 , -- Type_Number - numeric
                                      @2 , -- Survey_No - char(8)
                                      @3 , -- EntryUsr - char(5)
                                      GETDATE()
                                    )";
            using (var db = GetAABDB())
            {
                result += db.Execute(query, IsNA(ReferenceNo), IsNA(ReferenceType), IsNA(SurveyNo), IsNA(EntryUser));
            }
            return result >= 0;
        }

        #endregion
        
        public dynamic GetDetailOrderSimulation(string followUpNo)
        {
            string qOrd = @"select os.OrderNo, os.QuotationNo, os.ProductCode, os.PolicyOrderNo, CAST(COALESCE(oss.IsNeedSurvey,0) AS BIT) [IsNeedSurvey], os.SalesOfficerID, os.PolicyNo, os.PolicyID, os.BranchCode [BranchID], os.PolicyDeliveryName, os.VANumber,
os.SegmentCode, os.PeriodFrom, os.PeriodTo, sd.CustID AS SalesDealer, d.CustID AS DealerCode, pcs.ProspectID, os.SurveyNo, os.Phone1, os.Phone2, pcs.Name, fu.Remark, Cast(ISNULL(IsRenewal,0) as bit) AS IsRenewal, os.OldPolicyNo,
DATEDIFF(YEAR,PeriodFrom,PeriodTo) AS YearCvg, pcs.CustIDAAB, Cast(ISNULL(pcs.isCompany,0) as bit) AS isCompany, pcm.CompanyName
from FollowUP fu
INNER JOIN OrderSimulation os on fu.FollowUpNo = os.FollowUpNo and ApplyF = 1
INNER JOIN ProspectCustomer pcs on fu.CustID = pcs.CustID
LEFT JOIN ProspectCompany pcm on pcs.CustID = pcm.CustID
LEFT JOIN OrderSimulationSurvey oss on os.OrderNo = oss.OrderNo
LEFT JOIN dbo.SalesmanDealer sd ON sd.SalesmanCode = os.SalesDealer
LEFT JOIN dbo.Dealer d ON d.DealerCode = os.DealerCode
where fu.FollowUpNo = @0";

            string qMV = @";
SELECT TOP 1 c.Description + ' ' + d.Description + ' ' + omv.Year + ' ' + a.Series  AS VehicleDescription,
RegistrationNumber, a.VehicleCode,PeriodFrom,PeriodTo,ObjectNo,omv.Type,omv.Series,
UsageCode,omv.Sitting,omv.Year, mvp.Price [MarketPrice], omv.ModelCode, omv.BrandCode,
ChasisNumber, EngineNumber,IsNew,omv.CityCode,CAST(COALESCE(oss.IsNeedSurvey,0) AS BIT) IsNeedSurvey,ColorOnBPKB,omv.ProductTypeCode,
CAST(COALESCE(IsNSASkipSurvey,0) AS BIT) IsNSASkipSurvey, mvp.Geo_Area_Id
FROM Vehicle a 
LEFT JOIN VehicleBrand c ON c.BrandCode = a.BrandCode 
AND c.ProductTypeCode = a.ProductTypeCode 
LEFT JOIN VehicleModel d ON d.BrandCode = a.BrandCode 
AND d.ProductTypeCode = a.ProductTypeCode AND d.ModelCode = a.ModelCode
LEFT JOIN Vehicle b ON a.VehicleCode=b.VehicleCode AND 
a.BrandCode=b.BrandCode
AND a.ProductTypeCode=b.ProductTypeCode AND a.ModelCode=b.ModelCode 
AND a.Series=b.Series AND a.Type=b.Type 
AND isnull(a.Sitting,0)=isnull(b.Sitting,0) AND a.CityCode=b.CityCode 
AND a.Year > b.Year 
INNER JOIN dbo.OrderSimulationMV omv ON omv.VehicleCode = a.VehicleCode
INNER JOIN dbo.OrderSimulation os ON os.OrderNo = omv.OrderNo
INNER JOIN dbo.FollowUp f ON f.FollowUpNo = os.FollowUpNo
LEFT JOIN dbo.OrderSimulationSurvey oss ON oss.OrderNo = os.OrderNo 
LEFT JOIN BEYONDREPORT.AAB.dbo.Mst_vehicle_price mvp ON mvp.Vehicle_code = omv.VehicleCode
AND mvp.Year = omv.Year AND mvp.Geo_Area_Id = omv.CityCode
AND Effective_date = (SELECT MAX(Effective_date) FROM BEYONDREPORT.AAB.dbo.Mst_vehicle_price mvp2
WHERE mvp2.Vehicle_code = omv.VehicleCode AND Year = omv.Year 
AND mvp2.Geo_Area_Id = omv.CityCode AND mvp2.Status = 1)
WHERE os.ApplyF = 1 AND os.RowStatus = 1 AND omv.RowStatus = 1
AND f.RowStatus = 1 AND os.FollowUpNo = @0 AND b.Year IS NULL 
--AND a.RowStatus = 1 
AND c.RowStatus = 1 AND d.RowStatus = 1
ORDER BY a.LastUpdatedTime DESC";

            string qSurvey = @"SELECT c.Name[CityName], oss.SurveyAddress, oss.SurveyPostalCode, oss.ScheduleTimeID, oss.SurveyDate
FROM FollowUP fu
INNER JOIN OrderSimulation os on fu.FollowUpNo = os.FollowUpNo and ApplyF = 1
LEFT JOIN OrderSimulationSurvey oss on oss.OrderNo = os.OrderNo
LEFT JOIN asuransiastra.dbo.city c on c.ID = oss.CityCode
LEFT JOIN Otocare.dbo.ScheduleTime st on st.ScheduleTimeID = oss.ScheduleTimeID
where fu.FollowUpNo = @0";
            
            string qInterest = @"
select a.OrderNo, CAST(a.ObjectNo AS INT) AS ObjectNo, CAST(a.InterestNo AS INT) AS InterestNo, a.InterestID, b.Insurance_Type [InsuranceType], b.Interest_Type [InterestType], '' [InterestIDGeneric], PeriodFrom, PeriodTo, c.SumInsured, Premium,
Deductible_Code AS DeductibleCode, CAST(0 as numeric(13,4)) DedFlatAmount, '0' DedFlatCurrency
from [dbo].[OrderSimulationInterest] a 
INNER JOIN beyondreport.aab.dbo.Mst_Interest b on a.InterestID = b.Interest_ID and b.status = 1
INNER JOIN (select InterestNo, MAX(SumInsured) [SumInsured] FROM [dbo].[OrderSimulationCoverage] x where x.OrderNo = @0 GROUP BY InterestNo ) c on a.InterestNo = c.InterestNo
where orderno  = @0";

            string qCoverage = @"select osc.OrderNo, CAST(osc.ObjectNo AS INT) AS ObjectNo, CAST(osc.InterestNo AS INT) AS InterestNo, CAST(CoverageNo AS INT) AS CoverageNo, CoverageId,SumInsured,Rate,LoadingRate,Loading,osc.Gross_Premium [GrossPremium]
,osc.Cover_Premium [CoverPremium],osc.Premium [NetPremium], BeginDate, EndDate, datediff(month,BeginDate, EndDate) DiffMonth
, '' [CoverageIDGeneric], '' [InterestIDGeneric], osc.Deductible_Code AS DeductibleCode, '' DedFlatCurrency, CAST(0 as numeric(13,4)) DedFlatAmount,
 0 [ExcessRate], 'A' [Status], osi.InterestID, osc.Ndays, osc.Entry_Pct, Net1, Net2, Net3, CAST(Calc_Method AS INT) Calc_Method
from [dbo].[OrderSimulationCoverage] osc
INNER JOIN dbo.OrderSimulationInterest osi
ON osi.OrderNo = osc.OrderNo AND osi.ObjectNo = osc.ObjectNo AND osi.InterestNo = osc.InterestNo
WHERE osc.OrderNo = @0";

            string qDtlAddress = @";DECLARE @@PolicySentTo VARCHAR(50) 
SELECT @@PolicySentTo = COALESCE(PolicySentTo,'') FROM dbo.OrderSimulation WHERE OrderNo = @0 AND RowStatus = 1
IF(@@PolicySentTo <> '')
BEGIN
	SELECT DISTINCT x.Data AddressType, 
	case x.Data 
		when 'DELIVR' THEN PolicyDeliveryPostalCode
		ELSE IIF(COALESCE(pc.IsCompany,0) = 0, pc.PostalCode, pcm.PostalCode)
	end [PostalCode],
	case x.Data 
		when 'DELIVR' THEN 
			CASE a.PolicySentTo WHEN 'GC' THEN gc.Address
			ELSE PolicyDeliveryAddress END
		ELSE IIF(COALESCE(pc.IsCompany,0) = 0, pc.custAddress, pcm.NPWPaddress)
	end [Address],  
	case x.Data 
		when 'DELIVR' THEN 
			case a.PolicySentTo when 'CS' then IIF(COALESCE(pc.IsCompany,0) = 0, PolicyDeliveryName, CompanyName) 
			when 'CH' then IIF(COALESCE(pc.IsCompany,0) = 0, PolicyDeliveryName, CompanyName) 
			else COALESCE(PolicyDeliveryName,'') end 
		ELSE IIF(COALESCE(pc.IsCompany,0) = 0, PolicyDeliveryName, CompanyName)
	end [ContactPerson], 
	case x.Data 
		when 'DELIVR' THEN 
			case a.PolicySentTo when 'CS' then IIF(COALESCE(pc.IsCompany,0) = 0, pc.Phone1, pcm.PICPhoneno) 
			when 'CH' then IIF(COALESCE(pc.IsCompany,0) = 0, pc.Phone1, pcm.PICPhoneno) 
			when 'BR' then ''
			else IIF(COALESCE(pc.IsCompany,0) = 0, pc.Phone1, pcm.PICPhoneno) end 
		ELSE IIF(COALESCE(pc.IsCompany,0) = 0, pc.Phone1, pcm.PICPhoneno)
	end [Handphone],
	case x.Data 
		when 'DELIVR' THEN 
			case a.PolicySentTo when 'CS' then IIF(COALESCE(pc.IsCompany,0) = 0, 'CH', 'CO') 
			when 'CH' then IIF(COALESCE(pc.IsCompany,0) = 0, 'CH', 'CO') 
			when 'BR' then '' 
			WHEN 'GC' THEN gc.GCCode
			else IIF(COALESCE(pc.IsCompany,0) = 0, 'CH', 'CO') end 
		ELSE IIF(COALESCE(pc.IsCompany,0) = 0, 'CH', 'CO')
	end [SourceType],
	case x.Data 
		when 'DELIVR' THEN 
			case a.PolicySentTo when 'CS' then 'CS' 
			when 'CH' then 'CH'
			when 'BR' then 'BR' 
			WHEN 'GC' THEN 'GC'
			else 'OH' end 
	end [DeliveryCode]
	FROM 
	[dbo].[Fn_AABSplitString] ('POLADR,BILADR,DELIVR',',') x 
	inner join dbo.OrderSimulation a on 1=1
	inner join FollowUp b on a.FollowUpNo = b.FollowUpNo 
	inner join ProspectCustomer pc on pc.CustID = b.CustID
	left join mst_policysentto pst on a.PolicySentTo = pst.PolicySentToDes and pst.rowstatus = 1
	left join ProspectCompany pcm on pcm.CustId = a.CustID
	LEFT JOIN BEYONDREPORT.AAB.dbo.GardaCenter gc ON gc.Name = a.PolicyDeliveryAddress
	where orderno = @0 AND a.RowStatus = 1
END";

            string qSurveyImg = @"SELECT a.FollowUpNo, id.ImageID, a.ImageName, a.ImageType, id.Data, CoreImage_ID FROM 
(
SELECT 
	FollowUpNo,ImageName, ImageType
FROM 
(
select fu.FollowUpNo, IdentityCard, STNK, BSTB1, NPWP
from FollowUp fu WITH (NOLOCK)
LEFT JOIN ProspectCompany pcm WITH (NOLOCK) ON fu.CustID = pcm.CustID
where fu.followupno = @0
)a 
UNPIVOT (
	ImageName FOR ImageType IN (
		IdentityCard, STNK, BSTB1, NPWP
	)
) unpvt
where ImageName <> ''
) a 
INNER JOIN ImageData id WITH (NOLOCK) on ImageName = id.PathFile";

            //        string qInterestItem = @";DECLARE @@DIIMaxEndNo AS INT = 0;
            //;
            //DECLARE @@DIMaxEndNo AS INT = 0;
            //DECLARE @@PolicyID AS VARCHAR(MAX);
            //SELECT TOP 1
            //        @@PolicyID = Policy_ID
            //FROM    Policy
            //WHERE   Policy_No = @0
            //ORDER BY Endorsement_No DESC

            //SELECT TOP 1
            //        @@DIIMaxEndNo = Endorsement_No
            //FROM    Dtl_Interest_Item
            //WHERE   Policy_ID = @@PolicyID
            //ORDER BY Endorsement_No DESC

            //SELECT TOP 1
            //        @@DIMaxEndNo = Endorsement_No
            //FROM    dbo.Dtl_Interest AS di
            //WHERE   Policy_ID = @@PolicyID
            //ORDER BY Endorsement_No DESC

            //SELECT  dii.Policy_Id AS PolicyID ,
            //        dii.Object_No AS ObjectNo ,
            //        dii.Interest_No AS InterestNo ,
            //        dii.Int_Item_No AS IntItemNo ,
            //        dii.Endorsement_No AS EndorsementNo ,
            //        dii.Org_Endorsement_No AS OrgEndorsementNo ,
            //        COALESCE(dii.Original_SI,0) AS OriginalSI ,
            //        dii.Insurance_Type AS InsuranceType ,
            //        dii.Int_Item_Code AS IntItemCode ,
            //        dii.Item_Name AS ItemName ,
            //        dii.Description AS Description ,
            //        dii.Capacity AS Capacity ,
            //        dii.Sum_Insured AS SumInsured ,
            //        COALESCE(dii.Ded_Amount,0) AS DedAmount ,
            //        dii.MFG_Year AS MfgYear ,
            //        dii.Birthdate AS BirthDate ,
            //        dii.ColumnA AS ColumnA ,
            //        dii.ColumnB AS ColumnB ,
            //        dii.ColumnC AS ColumnC ,
            //        dii.ColumnD AS ColumnD ,
            //        dii.ColumnE AS ColumnE ,
            //        dii.Date1 AS Date1 ,
            //        dii.Date2 AS Date2 ,
            //        dii.Flag1 AS Flag1 ,
            //        dii.Flag2 AS Flag2
            //FROM    dbo.Dtl_Interest AS di
            //        INNER JOIN Dtl_interest_Item AS dii ON di.Policy_Id = dii.Policy_Id
            //                                               AND di.Interest_No = dii.Interest_No
            //WHERE   dii.Policy_ID = @@PolicyID
            //        AND dii.Endorsement_No = @@DIIMaxEndNo
            //        AND di.Endorsement_No = @@DIMaxEndNo
            //        AND di.Interest_Type IN ( 'P', 'A' )";

            string qInterestItem = @";SELECT DISTINCT os.OrderNo, os.PolicyID, rdi.Object_No ObjectNo, rdi.Interest_No InterestNo, rdi.Int_Item_No IntItemNo,
rn.Endorsement_No EndorsementNo, 0 OrgEndorsementNo, rdi.Original_SI OriginalSI, rdi.Insurance_Type InsuranceType,
rdi.Int_Item_Code IntItemCode, rdi.Item_Name ItemName, rdi.Description, rdi.Capacity, rdi.Sum_Insured SumInsured,
rdi.Ded_Amount DedAmount, rdi.MFG_Year MfgYear, rdi.BirthDate, rdi.ColumnA, rdi.ColumnB, rdi.ColumnC,
rdi.ColumnD, rdi.ColumnE, rdi.Date1, rdi.Date2, rdi.Flag1, rdi.Flag2
FROM dbo.Ren_Dtl_Interest_Item rdi INNER JOIN dbo.Renewal_Notice rn ON rn.Policy_No = rdi.Policy_No
INNER JOIN BEYONDMOSS.AABMobile.dbo.OrderSimulation os ON os.OldPolicyNo = rn.Policy_No
INNER JOIN BEYONDMOSS.AABMobile.dbo.FollowUp f ON os.FollowUpNo = f.FollowUpNo
WHERE os.OldPolicyNo = @0 AND os.RowStatus = 1 AND os.ApplyF = 1 AND FollowUpStatus IN (@1,@2,@3)";

            string qOriginalDefect = @";DECLARE @@MaxEndNo AS INT = 0;
                                    DECLARE @@MaxEndNoPC AS INT = 0;
                                    DECLARE @@PolicyID AS VARCHAR(MAX) 
                                    SELECT TOP 1
                                            @@PolicyID = Policy_ID
                                    FROM    Policy
                                    WHERE   Policy_No = @0
                                    ORDER BY Endorsement_No DESC
                                    SELECT TOP 1
                                            @@MaxEndNo = Endorsement_No
                                    FROM    Object_Clause
                                    WHERE   Policy_ID = @@PolicyID
                                    ORDER BY Endorsement_No DESC
                                    SELECT TOP 1
                                            @@MaxEndNoPC = Endorsement_No
                                    FROM    Policy_Clause
                                    WHERE   Policy_ID = @@PolicyID
                                    ORDER BY Endorsement_No DESC
                                    SELECT  *
                                    INTO    #TEMP
                                    FROM    ( SELECT    @0 AS PolicyNo ,
                                                        Policy_Id AS PolicyID ,
                                                        Object_No AS ObjectNo ,
                                                        Clause_No AS ClauseNo ,
                                                        Endorsement_No AS EndorsementNo ,
                                                        Org_Endorsement_No AS OrgEndorsementNO ,
                                                        oc.Clause_Id AS ClauseID ,
                                                        oc.Description AS ClauseDetail ,
                                                        oc.Description,
                                                        oc.Description AS NewDescription
                                              FROM      dbo.Object_Clause AS oc
                                                        INNER JOIN dbo.Mst_Clause AS mc ON oc.Clause_iD = mc.Clause_Id
                                              WHERE     oc.Policy_ID = @@PolicyID
                                                        AND oc.Endorsement_No = @@MaxEndNo
                                                        AND oc.Clause_Id = 'OMV99'
                                              UNION ALL
                                              SELECT    @0 AS PolicyNo ,
                                                        Policy_Id AS PolicyID ,
                                                        1 AS ObjectNo ,
                                                        1 AS ClauseNo ,
                                                        Endorsement_No AS EndorsementNo ,
                                                        Org_Endorsement_No AS OrgEndorsementNO ,
                                                        oc.Clause_Id AS ClauseID ,
                                                        oc.Description AS ClauseDetail ,
                                                        oc.Description,
                                                        oc.Description AS NewDescription
                                              FROM      dbo.Policy_Clause AS oc
                                                        INNER JOIN dbo.Mst_Clause AS mc ON oc.Clause_iD = mc.Clause_Id
                                              WHERE     oc.Policy_ID = @@PolicyID
                                                        AND oc.Endorsement_No = @@MaxEndNoPC
                                                        AND oc.Clause_Id = 'OMV99'
                                            ) a
                                    DECLARE @@ClauseOMV99 VARCHAR(8000) ,
                                @@ClauseOMV99Copy VARCHAR(8000) = ''
                            SELECT  @@ClauseOMV99 = Description
                            FROM    #TEMP AS t
                            WHERE   ClauseID = 'OMV99'
                            CREATE TABLE #TEMP2
                                (
                                  id BIGINT IDENTITY(1, 1) ,
                                  NewDescription VARCHAR(8000)
                                )
                            INSERT  INTO #TEMP2
                                    SELECT  Description = ISNULL(STUFF(REPLACE(Data, '$', ''), 1, 1, ''),
                                                                 '')
                                    FROM    dbo.udf_SplitString(@@ClauseOMV99, '|') AS uss
                            DELETE  FROM #TEMP2
                            WHERE   NewDescription = ''
                            SELECT  @@ClauseOMV99Copy = @@ClauseOMV99Copy + CAST(id AS VARCHAR(10)) + '. '
                                    + NewDescription + ' /n<br/> '
                            FROM    ( SELECT    *
                                      FROM      #TEMP2 AS t
                                    ) a
                            SET @@ClauseOMV99Copy = LEFT(@@ClauseOMV99Copy,
                                                         CASE WHEN CHARINDEX(CHAR(10), @@ClauseOMV99Copy) > 0
                                                              THEN LEN(@@ClauseOMV99Copy) - 1
                                                              ELSE LEN(@@ClauseOMV99Copy)
                                                         END)
                            UPDATE  #TEMP
                            SET     NewDescription = @@ClauseOMV99Copy
                            WHERE   PolicyID = @@PolicyID
                                    AND PolicyNo = @0
                                    AND ClauseID = 'OMV99'
                            SELECT  *
                            FROM    #TEMP AS t
                            DROP TABLE #TEMP
                            DROP TABLE #TEMP2";

            string qNonStandardAcc = @"SELECT Object_No,	
                                    Accs_Code,	
                                    Accs_Cat_Code,	
                                    Accs_Part_Code,	
                                    Include_Tsi,	
                                    Brand,	
                                    Sum_Insured,	
                                    Quantity,	
                                    Premi,	
                                    Category,
									CAST(Endorsement_No AS INT) EndorsementNo
                                    FROM dbo.Ren_Dtl_Non_Standard_Accessories rdn
									INNER JOIN dbo.Renewal_Notice rn 
									ON rn.Policy_No = rdn.Policy_No
                                    WHERE rn.Policy_No = @0";
            string qOrdersumulationSurvey = @"
SELECT CityCode,LocationCode,SurveyAddress,SurveyPostalCode, SurveyDate 
FROM dbo.OrderSimulationSurvey oss
INNER JOIN dbo.OrderSimulation os
ON os.OrderNo = oss.OrderNo
WHERE os.FollowUpNo = @0
AND os.ApplyF = 1";
            string qNoCover = @"SELECT ObjectNo,Type,DamageType,NoCoverID,Name,Description,Quantity 
FROM dbo.Ren_Dtl_NoCover where Policy_No = @0";
            string qORDefect = @";SELECT ObjectNo,0 EndorsementNo,PartID,Name,DamageCategory,Description  
FROM dbo.Dtl_Original_Defect dod
INNER JOIN dbo.Policy p ON p.Policy_Id = dod.Policy_Id
AND dod.EndorsementNo = p.Endorsement_No
WHERE p.Policy_No = @0 
AND p.Status = 'A'
AND p.ORDER_STATUS IN ( '11','9' )
AND p.RENEWAL_STATUS = 0
AND ((p.endorsement_no = 0 and p.ORDER_STATUS IN ('11','9')) or p.endorsement_no <> 0 )";
            using (var db = GetAABMobileDB())
            {
                var ord = db.Fetch<dynamic>(qOrd, followUpNo);
                var mv = db.Fetch<dynamic>(qMV, followUpNo);
                var survey = db.Fetch<dynamic>(qSurvey, followUpNo);
                string orderNo = ord.First<dynamic>().OrderNo;
                var interest = db.Fetch<dynamic>(qInterest, orderNo);
                var coverage = db.Fetch<dynamic>(qCoverage, orderNo);
                var coverage2 = db.Fetch<dynamic>(qCoverage, orderNo);
                var address = db.Fetch<dynamic>(qDtlAddress, orderNo);
                var surveyImg = db.Fetch<dynamic>(qSurveyImg, followUpNo);
                var aabDB = GetAABDB();
                List<dynamic> InterestItem = new List<dynamic>();
                List<dynamic> OriginalDefect = new List<dynamic>();
                List<dynamic> oldOriginalDefect = new List<dynamic>();
                List<dynamic> NonStandardAcc = new List<dynamic>();
                List<dynamic> NoCover = new List<dynamic>();
                if (ord.Count > 0) {
                    string oldPolicyNo = ord.First().OldPolicyNo;
                    if (!string.IsNullOrEmpty(oldPolicyNo)) {
                        InterestItem = aabDB.Fetch<dynamic>(qInterestItem, oldPolicyNo, FollowUpInfo.SendToSA, FollowUpInfo.SendToSurveyor, FollowUpInfo.FUPayment);
                        OriginalDefect = aabDB.Fetch<dynamic>(qOriginalDefect, oldPolicyNo);
                        oldOriginalDefect = aabDB.Fetch<dynamic>(qORDefect, oldPolicyNo);
                        NonStandardAcc = aabDB.Fetch<dynamic>(qNonStandardAcc, oldPolicyNo);
                        NoCover = aabDB.Fetch<dynamic>(qNoCover, oldPolicyNo);
                    }
                }
                var OSsurvey = db.Fetch<dynamic>(qOrdersumulationSurvey, followUpNo);
                return new { Ord = ord, MV = mv, Survey = survey, Interest = interest, Coverage = coverage, DtlAddress = address, SurveyImg = surveyImg,
                InterestItem, OriginalDefect, NonStandardAcc, OSsurvey, coverage2, NoCover, oldOriginalDefect};
            }
        }

        public bool UpdateOrderSimulationSurveyNo(string SurveyNo, string PolicyOrderNo) {
            int res = 0;
            try
            {
                string qUpdateOrderSimulation = @"
                        UPDATE dbo.OrderSimulation
                        SET SurveyNo = @0
                        WHERE PolicyOrderNo = @1";
                using (var AABMobileDB = RepositoryBase.GetAABMobileDB())
                {
                    AABMobileDB.Execute(qUpdateOrderSimulation, SurveyNo, PolicyOrderNo);
                }
                return res > 0;
            }
            catch (Exception e)
            {

                throw e;
            }
        }
        public bool InsertMstOrderSendToSA(string OrderNo, string Remaks, string SalesOfficerID)
        {
            string query = "";
            int res = 0;
            var db = GetAABDB();
            try
            {
                query = @"INSERT INTO [AAB].[dbo].[Mst_Order_Mobile] 
                        (Order_No,SA_State,Approval_Status,Approval_Process,Approval_Type,isSO,Remarks,Actual_Date,EntryDt,EntryUsr) 
                        VALUES(@0,1,0,0,0,0,@1,GETDATE(),getdate(),@2)";
                db.Execute(query, OrderNo, Remaks, SalesOfficerID);
                return res > 0;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public bool InsertMstOrderFUPayment(string OrderNo, string Remaks, string SalesOfficerID)
        {
            string query = "";
            int res = 0;
            var db = GetAABDB();
            var byddb = Geta2isBeyondDB();
            var mbldb = GetAABMobileDB();
            try
            {
                string BankName = "";
                string AccountNo = "";
                string AccountHolder = "";
                query = @";DECLARE @@SalesmanCode BIGINT
                        SELECT @@SalesmanCode=SalesDealer FROM dbo.OrderSimulation WHERE PolicyOrderNo = @0
                        SELECT CustID FROM dbo.SalesmanDealer WHERE SalesmanCode = @@SalesmanCode";
                List<dynamic> salesmandealer = mbldb.Fetch<dynamic>(query, OrderNo);
                if (salesmandealer.Count > 0)
                {
                    query = ";EXEC General.usp_DealerValidation @0";
                    List<dynamic> info = byddb.Fetch<dynamic>(query, salesmandealer.First().CustID);
                    if (info.Count > 0)
                    {
                        if (info.First().IsValid)
                        {
                            BankName = info.First().Account_Info_BankName;
                            AccountNo = info.First().Account_Info_AccountNo;
                            AccountHolder = info.First().Account_Info_AccountHolder;
                        }
                    }
                }

                query = @"INSERT INTO [AAB].[dbo].[Mst_Order_Mobile] 
                        (Order_No,SA_State,Approval_Status,Approval_Process,Approval_Type,isSO,Remarks,Actual_Date,EntryDt,EntryUsr,
                          Account_Info_BankName ,
                          Account_Info_AccountNo ,
                          Account_Info_AccountHolder) 
                        VALUES(@0,3,0,0,0,0,@1,GETDATE(),getdate(),@2,@3,@4,@5)";
                db.Execute(query, OrderNo, Remaks, SalesOfficerID, BankName, AccountNo, AccountHolder);
                return res > 0;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<dynamic> getOldObjectClause(string policyNo)
        {
            string query = @";DECLARE @@PolicyID AS VARCHAR(MAX);
                                SELECT TOP 1
                                        @@PolicyID = Policy_ID
                                FROM    Policy
                                WHERE   Policy_No = @0
                                ORDER BY Endorsement_No DESC

                               select distinct oc.Clause_ID AS ClauseID, mc.Description  AS Description --,pc.*, *
                               from policy p
                               inner join dbo.Object_Clause oc on p.Policy_Id = oc.Policy_Id and p.Endorsement_No = oc.Endorsement_No
                               inner join dbo.Mst_Clause mc on oc.Clause_Id = mc.Clause_Id
                               inner join (select max(endorsement_no) 'maxNo' from dbo.Object_Clause where Policy_Id = @@PolicyID) oc1 on oc1.maxNo = p.Endorsement_No
                               where p.Policy_Id = @@PolicyID AND mc.Clause_ID NOT IN (SELECT code AS Clause_ID from Mst_General where type = 'EXC' AND Status = 1)";
            using (var db = GetAABDB())
            {
                return db.Query<dynamic>(query, policyNo).ToList();
            }
        }
        public List<dynamic> getOldPolicyClause201(string policyNo)
        {
            string query = @";DECLARE @@max INT
                            SELECT @@max = MAX(Endorsement_No) FROM Policy WHERE Policy_No = @0
                            DECLARE @@Temp TABLE (PolicyID VARCHAR(16),EndorsementNumber INT, ClauseID VARCHAR(8),ClauseDescription VARCHAR(100),Description VARCHAR(100), CLauseType INT)

                            SELECT * FROM @@Temp
                            INSERT INTO @@Temp 
                            EXEC [dbo].[usp_GetClauseNonAutomatic] @0,@@max

                            SELECT t.* FROM @@Temp t
                            LEFT JOIN Mst_General g on g.code = t.ClauseID AND g.type = 'EXC' AND g.Status = 1";

            using (var db = GetAABDB())
            {
                return db.Query<dynamic>(query, policyNo).ToList();
            }
        }

        public string GetGASurveyAreaCode(string GeoAreaCode)
        {
            string cSQL = @"SELECT  TOP 1 SurveyAreaCode
                        FROM    GardaAkses.GA.SurveyArea
                        WHERE   RowStatus = 0
                                AND GeoAreaCode = @0";
            using (var db = GetAABMobileDB())

            {
                var result = db.Query<string>(cSQL, GeoAreaCode).SingleOrDefault();
                if (result == null)
                {
                    result = "";
                }
                return result;
            }
        }

        public void InsertBookingSurvey(BookingSurvey bs) {
            using (var db = GetAABMobileDB())
            {
                string query = @"INSERT INTO GardaAkses.GA.BookingSurvey
                                        ( TransactionID ,
                                          SurveyAreaCode ,
                                          SurveyDate ,
                                          SurveyTimeCode ,
                                          CreatedBy ,
                                          CreatedDate          
                                        )
                                VALUES  ( @0 , -- TransactionID - varchar(50)
                                          @1 , -- SurveyAreaCode - varchar(20)
                                          @2 , -- SurveyDate - datetime
                                          @3 , -- SurveyTimeCode - varchar(12)
                                          @4 , -- CreatedBy - varchar(50)
                                          GETDATE()  -- CreatedDate - datetime          
                                        )";
                db.Execute(query, bs.TransactionID, bs.SurveyAreaCode, bs.SurveyDate, bs.SurveTimeCode, bs.Actor);
            }
        }

        public string GetAreaCodeSurveyOrder(string CityID) {
            string result = "";
            using (var db = GetAABMobileDB()) {
                string query = "SELECT AABCode FROM Asuransiastra.dbo.Branch WHERE TYPE IN ( 2, 5, 6 ) AND ID = @0";
                List<dynamic> dyn = db.Fetch<dynamic>(query, CityID);
                if (dyn.Count > 0) {
                    result = dyn.First().AABCode;
                }
            }
            return result;
        }

        public List<dynamic> GetPeriodFromToRenew(string PolicyNo)
        {
            string query = @";DECLARE @@Flag INT = 0 
                            DECLARE @@Date DATETIME
                            DECLARE @@DateFrom DATETIME
                            DECLARE @@Day INT
                            DECLARE @@Year INT

                            SELECT @@Flag=IIF(Period_To>GETDATE(),1,0), @@DateFrom=Period_To  FROM dbo.Policy where Policy_No = @0

                            IF(@@Flag=1)
                            BEGIN
	                            SELECT @@Day=DATEDIFF(DAY,PeriodFrom,PeriodTo),@@Year=DATEDIFF(YEAR,PeriodFrom,PeriodTo) FROM BEYONDMOSS.AABMobile.dbo.OrderSimulation WHERE OldPolicyNo = @0
								IF EXISTS(SELECT TOP 1 1 FROM BEYONDMOSS.AABMobile.dbo.OrderSimulation WHERE OldPolicyNo = @0 AND DATEDIFF(YEAR,PeriodFrom,PeriodTo) > 0 AND DATEADD(YEAR,DATEDIFF(YEAR,PeriodFrom,PeriodTo),PeriodFrom) = PeriodTo)
								BEGIN
									SELECT @@Date = DATEADD(YEAR, @@Year, @@DateFrom) 
								END
								ELSE
								BEGIN
									SELECT @@Date = DATEADD(DAY, @@Day, @@DateFrom) 
								END
                            END
                            ELSE
                            BEGIN
	                            SELECT @@DateFrom=NULL,@@Date=NULL
                            END
                            SELECT @@DateFrom AS PeriodFrom,@@Date AS PeriodTo";
            using (var db = GetAABDB())
            {
                List<dynamic> res = db.Fetch<dynamic>(query, PolicyNo);
                return res;
            }
        }

        public List<dynamic> GetPolicyClauseAutomatic(string productCode, string interestID, string Coverageid)
        {
            List<dynamic> result = new List<dynamic>();
            //int EndorsementNo = 0;
            //string query = @";SELECT MAX(Endorsement_No) FROM Policy WHERE Policy_No = @0";
            using (var db = GetAABDB())
            {
                result = db.Query<dynamic>(@";EXEC usp_GetORClauseAutomatic @0,@1,@2", productCode, interestID, Coverageid).ToList();
            }
            return result;
        }

        public dynamic GetGeoAreaCode(string PostalCode) {
            var AABDB = GetAABDB();
            string query = @"SELECT AreaCode GeoAreaCode
                            FROM SurveyManagement.dbo.MappingSurveyAreaZipCode
                            WHERE ZipCode = @0 AND RowStatus = 0";
            return AABDB.First<dynamic>(query, PostalCode);
        }

        public string GetVANumber(string orderno, string VANumber) {
            var db = GetAABMobileDB();
            string qgetOrderType = @"SELECT COALESCE(Biz_type,0) Biz_type, CAST(COALESCE(pc.isCompany,0) AS BIT) isCompany 
									, CAST(COALESCE(pc.IsPayerCompany,0) AS BIT) IsPayerCompany
									, CAST(COALESCE(f.IsRenewal,0) AS BIT) IsRenewal
									FROM dbo.OrderSimulation os
                                    INNER JOIN BEYONDREPORT.AAB.dbo.Mst_Product mp
                                    ON mp.Product_Code = os.ProductCode
                                    INNER JOIN dbo.ProspectCustomer pc
                                    ON pc.CustID = os.CustID
									INNER JOIN dbo.FollowUp f ON f.FollowUpNo = os.FollowUpNo
                                    WHERE os.OrderNo = @0 AND os.RowStatus = 1 AND os.ApplyF = 1";
            List<dynamic> orderType = db.Fetch<dynamic>(qgetOrderType, orderno);
            if (orderType.Count > 0) {
                if (Convert.ToInt32(orderType.First().Biz_type) == 2 || orderType.First().isCompany || orderType.First().IsPayerCompany)
                {
                    VANumber = "";
                }
                else if(string.IsNullOrEmpty(VANumber)){
                    if (!orderType.First().IsRenewal) {
                        VANumber = GetVANo();
                        string qUpdateVAToOrderSimulation = @";UPDATE dbo.OrderSimulation
SET VANumber = @1
WHERE OrderNo = @0";
                        db.Execute(qUpdateVAToOrderSimulation, orderno, VANumber);
                    }
                }
            }
            return VANumber;
        }
        public decimal GetVehiclePrice(dynamic MarketPrice, dynamic VCode, dynamic Year, dynamic CityId)
        {
            decimal price = 0;
            var db = GetAABMobileDB();
            var aabdb = GetAABDB();
            if (MarketPrice == null)
            {
                string cid = Convert.ToString(CityId);
                List<dynamic> ls = aabdb.Fetch<dynamic>("SELECT aab_code FROM dbo.partner_aab_map WHERE partner_code =@0 AND type = 'PRC' AND partner_id = 'OJK'", CityId);
                if (ls.Count > 0)
                {
                    string qgetPrice = @"SELECT Price FROM dbo.Mst_Vehicle_Price 
                                    WHERE Vehicle_code = @0 AND [Year] = @1 AND Geo_Area_Id = @2
									ORDER BY Effective_date DESC";
                    List<dynamic> lsPrice = aabdb.Fetch<dynamic>(qgetPrice, VCode, Year, CityId);
                    if (lsPrice.Count > 0)
                    {
                        price = lsPrice.First().Price;
                    }
                }
                else
                {
                    price = 0;
                }
            }
            else {
                price = MarketPrice;
            }
            return price;
        }

        public bool GetIsAgency(string SalesOfficerID) {
            bool res = false;
            var aabDB = GetAABDB();
            var mblDB = GetAABMobileDB();
            string email = mblDB.ExecuteScalar<string>(@";
DECLARE @@Email VARCHAR(512) = ''
SELECT @@Email=Email FROM dbo.SalesOfficer WHERE SalesOfficerID = @0 AND RowStatus = 1
SELECT @@Email", SalesOfficerID);
            if (!string.IsNullOrEmpty(email)) {
                res = aabDB.ExecuteScalar<bool>(@";
DECLARE @@Count INT = 0
SELECT @@Count = COUNT(1) 
FROM a2isAuthorizationDB.General.ExternalUsers 
WHERE UserID = @0 AND RowStatus = 0
IF(@@Count>0)
BEGIN SELECT CAST(1 AS BIT) END
ELSE 
BEGIN SELECT CAST(0 AS BIT) END", email);
            }
            return res;
        }
    }
}
