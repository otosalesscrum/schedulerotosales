﻿using Otosales.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Otosales.Repository;
namespace Otosales.CreateOrder.Repository
{
    class ObjectRepository : RepositoryBase
    {
        public bool InsertNSA(string orderNo, string quotationNo, NSA nsa, string user)
        {

            string qInsertNSA = @"IF not exists (Select 1 From dbo.WTOrderNSA Where OrderNo=@0 AND ObjectNo=@1 AND QuotationNo=@3 AND NSACategoryCode=@4 And RowStatus=0)
                                  BEGIN                                    
                                  INSERT INTO [dbo].[WTOrderNSA]
                                        ([OrderNo]
                                         ,[ObjectNo]
                                         ,[ObjectDescription]
                                        ,[QuotationNo]
                                        ,[NSACategoryCode]
                                        ,[Status]
                                        ,[Reason]
                                        ,[RowStatus]
                                        ,[CreatedDate]
                                        ,[CreatedBy])
                                    VALUES
                                        (@0 -- <OrderNo, varchar(10),>
                                        ,@1 -- <OrderNo, varchar(10),>
                                        ,@2 -- <OrderNo, varchar(10),>
                                        ,@3 -- <QuotationNo, varchar(25),>
                                        ,@4 -- <NSACategoryCode, varchar(10),>
                                        ,0 -- <Status, smallint,>
                                        ,@5 -- <Reason, varchar(8000),>
                                        ,0 -- <RowStatus, smallint,>
                                        ,GETDATE() -- <CreatedDate, datetime,>
                                        ,@6 -- <CreatedBy, varchar(10),>
                                   
                                ) 
                                END";
            using (var db = GetAABDB())
            {
                var ret = false;
                for (int i = 0; i < nsa.NSAItems.Count(); i++)
                {
                    var result = db.Execute(qInsertNSA,
                                            orderNo,
                                            nsa.NSAItems[i].ObjectNo,
                                            nsa.NSAItems[i].ObjectDescription,
                                            quotationNo,
                                            nsa.NSAItems[i].CategoryID,
                                            nsa.NSAItems[i].Reason,
                                            user);
                    if (result > 0)
                        ret = true;
                }
                if (nsa.NSAItems.Count() == 0)
                    return true;
                return ret;
            }
        }
    }
}
