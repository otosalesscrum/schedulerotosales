﻿
using Otosales.Logic;
using Otosales.Models;
using Otosales.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otosales.CreateOrder.Repository
{
    class OrderRepository : RepositoryBase
    {
        #region General
        
        public dynamic GetVehicleDescription(string VehicleCode)
        {
            string query = @"SELECT  A.vehicle_code AS VehicleCode ,
                            A.vehicle_type AS VehicleType ,
                            D.Description AS TypeDescription ,
                            A.series AS Series ,
                            A.Sitting AS SittingCapacity ,
                            ISNULL(A.Transmission, 9) AS Transmission ,
                            B.description AS BrandName ,
                            B.Brand_ID AS BrandID ,
                            C.description AS ModelName ,
                            C.Model_ID AS ModelID ,
                            ( CASE WHEN LEN(LTRIM(RTRIM(ISNULL(D.[Description], '')))) <> 0
                                   THEN LTRIM(RTRIM(D.[Description])) + ' '
                                   ELSE ''
                              END )
                            + ( CASE WHEN LEN(LTRIM(RTRIM(ISNULL(B.[Description], '')))) <> 0
                                     THEN LTRIM(RTRIM(B.[Description])) + ' '
                                     ELSE ''
                                END )
                            + ( CASE WHEN LEN(LTRIM(RTRIM(ISNULL(C.[Description], '')))) <> 0
                                     THEN LTRIM(RTRIM(C.[Description])) + ' '
                                     ELSE ''
                                END )
                            + ( CASE WHEN LEN(LTRIM(RTRIM(ISNULL(A.[Series], '')))) <> 0
                                     THEN LTRIM(RTRIM(A.[Series])) + ' '
                                     ELSE ''
                                END ) AS VehicleDescription ,
                            A.CC AS CCKendaraan
                    FROM    dbo.mst_vehicle A WITH ( NOLOCK ) ,
                            dbo.mst_vehicle_brand B WITH ( NOLOCK ) ,
                            dbo.mst_vehicle_model C WITH ( NOLOCK ) ,
                            dbo.dtl_ins_factor D WITH ( NOLOCK )
                    WHERE   A.Brand_id = B.Brand_id
                            AND A.Model = C.Model_id
                            AND A.vehicle_type = D.insurance_code
                            AND D.factor_code = 'VHCTYP'
                            AND A.Status = 1
		                    AND Vehicle_code=@0";
            using (var db = GetAABDB())
            {
                return db.Query<dynamic>(query, VehicleCode).FirstOrDefault();

            }
        }

        #endregion


        #region Deductible
        public List<ProductInterestCoverageDeductibleClasue> GetDeductibleClause(List<dynamic> CoverageList, string ProductCode)
        {
            List<string> CovIntGroup = new List<string>();
            List<string> CovIntGroupMember = new List<string>();
            List<ProductInterestCoverageDeductibleClasue> RetVal = new List<ProductInterestCoverageDeductibleClasue>();

            List<dynamic> TempCoverageList = new List<dynamic>();
            TempCoverageList = CoverageList;
            OrderRepository OR = new OrderRepository();
            List<string> InterestCoverageID = new List<string>();
            //Add Coverage ID To Parameter
            InterestCoverageID.AddRange(TempCoverageList.Select(o => (string)o.CoverageIDGeneric));
            //Add Interest ID To Parameter
            InterestCoverageID.AddRange(TempCoverageList.Select(o => (string)o.InterestIDGeneric));

            if (InterestCoverageID.Count > 0)
            {
                List<string> newInterestCoverageID = new List<string>();
                newInterestCoverageID.AddRange(InterestCoverageID.Distinct());
                //Get Group of Interest Coverage, Ex. Interest PADRVR & PAPASS to PAPSDR
                CovIntGroup = OR.GetCoverageGroup(newInterestCoverageID, ProductCode);
            }
            if (CovIntGroup.Count > 0)
            {
                CovIntGroupMember = OR.GetCoverageGroupMember(CovIntGroup, ProductCode);
            }
            foreach (var CovIntSubGroup in CovIntGroupMember)
            {
                for (int i = 0; i < TempCoverageList.Count; i++)
                {
                    if ((CovIntSubGroup.Trim().ToUpper() == TempCoverageList[i].CoverageIDGeneric.Trim().ToUpper()) || (CovIntSubGroup.Trim().ToUpper() == TempCoverageList[i].InterestIDGeneric.Trim().ToUpper()))
                    {
                        TempCoverageList.RemoveAt(i);
                    }
                }
            }
            foreach (var items in TempCoverageList)
            {
                if (CovIntGroupMember.FindIndex(o => string.Equals(items.CoverageIDGeneric.Trim().ToUpper(), o.Trim().ToUpper(), StringComparison.OrdinalIgnoreCase)) < 0)
                {
                    List<ProductInterestCoverageDeductibleClasue> a = OR.GetProductInterestCoverageDeductibleClause(ProductCode, items.InterestIDGeneric, items.CoverageIDGeneric);
                    if (a != null)
                    {
                        RetVal.AddRange(a);
                    }
                }
            }
            RetVal.AddRange(OR.GetProductInterestCoverageDeductibleClauseGrouped(CovIntGroup, ProductCode));

            return RetVal;
        }

        public ProductDeductible GetProductDeductible(string ProductCode, string DeductibleCode)
        {
            string query = @" SELECT Product_Code AS ProductCode ,
                                    Deductible_Code AS DeductibleCode ,
                                    Deductible_Type AS DeductibleType ,
                                    Bc_From_Si AS BaseCurrencyFromSumInsured ,
                                    Bc_To_Si AS BaseCurrencyToSumInsured ,
                                    Cover_Disc_Pct AS CoverDiscountPercentage ,
                                    Flat_Currency AS FlatCurrency ,
                                    Flat_Amount AS FlatAmount ,
                                    Percent_Si AS PercentSumInsured ,
                                    Percent_Claim AS PercentClaim ,
                                    Bc_Min_Amount AS Bc_MinimumAmount ,
                                    Bc_Max_Amount AS Bc_MaximumAmount ,
                                    Description AS Description
                             FROM   dbo.Prd_Deductible
                             WHERE  STATUS <> 0
                                    AND Product_Code = @0 and Deductible_Code=@1";
            using (var db = GetAABDB())
            {
                var result = db.Query<ProductDeductible>(query, ProductCode, DeductibleCode);
                return result.FirstOrDefault();
            }
        }

        public dynamic GetDeductibleCode(string ProductCode, string InterestID, string CoverageID) {
            OtosalesOrderRepository OOR = new OtosalesOrderRepository();
            InterestID = OOR.GetGenericInterestID(InterestID);
            CoverageID = OOR.GetGenericCoverageID(CoverageID);
            using (var db = GetAABDB()) {
                string query = @";DECLARE @@InterestID VARCHAR(20) = @1; 
DECLARE @@CoverageID VARCHAR(20) = @2; 
DECLARE @@ProductCode VARCHAR(20) = @0; 
DECLARE @@BasicInterestID CHAR(10) 

SELECT TOP 1 @@BasicInterestID = mi.interest_id 
FROM   dbo.prd_interest AS pi 
       INNER JOIN dbo.mst_interest AS mi 
               ON mi.interest_id = PI.interest_id 
WHERE  pi.product_code = @@ProductCode 
       AND pi.status = 1 
       AND mi.status = 1 
       AND mi.interest_type = 'P' 

SELECT @@InterestID = CASE COALESCE(interest_type, '') 
                        WHEN 'A' THEN @@BasicInterestID 
                        ELSE mi.interest_id 
                      END 
FROM   dbo.prd_interest AS pi 
       INNER JOIN dbo.mst_interest AS mi 
               ON mi.interest_id = PI.interest_id 
WHERE  pi.product_code = @@ProductCode 
       AND pi.status = 1 
       AND mi.status = 1 
       AND pi.interest_id = @@InterestID 

IF ( COALESCE(@@CoverageID, '') = '' ) 
  BEGIN 
      SELECT TOP 1 @@CoverageID = mc.coverage_id 
      FROM   dbo.prd_interest_coverage AS pic 
             INNER JOIN dbo.mst_coverage AS mc 
                     ON pic.coverage_id = mc.coverage_id 
                        AND mc.status = 1 
      WHERE  product_code = @@ProductCode 
             AND interest_id = @@InterestID 
             AND cover_type = 'BSC' 
  END 

SELECT TOP 1 deductiblecode, 
             pd.description 
FROM   (SELECT TOP 1 a.product_code    AS ProductCode, 
                     a.product_group   AS ProductGroup, 
                     b.deductible_code AS DeductibleCode 
        FROM   dbo.or_clause_group_product a 
               INNER JOIN dbo.or_clause_interest_coverage b 
                       ON a.product_group = b.product_group 
        WHERE  b.status = 1 
               AND b.interest_id = @@InterestID 
               AND b.coverage_id = @@CoverageID 
               AND a.product_code = @@ProductCode 
        UNION 
        SELECT TOP 1 a.product_code    AS ProductCode, 
                     a.product_group   AS ProductGroup, 
                     b.deductible_code AS DeductibleCode 
        FROM   dbo.or_clause_group_product a 
               INNER JOIN dbo.or_clause_interest_coverage b 
                       ON a.product_group = b.product_group 
               INNER JOIN dbo.mapping_interest_coverage_group c 
                       ON a.product_group = b.product_group 
                          AND c.interest_coverage_group_id = b.coverage_id 
                          AND c.mapping_type = 'C' 
        WHERE  b.status = 1 
               AND c.status = 1 
               AND b.interest_id = @@InterestID 
               AND c.interest_coverage_id = @@CoverageID 
               AND a.product_code = @@ProductCode 
        UNION 
        SELECT TOP 1 a.product_code    AS ProductCode, 
                     a.product_group   AS ProductGroup, 
                     b.deductible_code AS DeductibleCode 
        FROM   dbo.or_clause_group_product a 
               INNER JOIN dbo.or_clause_interest_coverage b 
                       ON a.product_group = b.product_group 
               INNER JOIN dbo.mapping_interest_coverage_group c 
                       ON a.product_group = b.product_group 
                          AND c.interest_coverage_group_id = b.interest_id 
                          AND c.mapping_type = 'I' 
        WHERE  b.status = 1 
               AND c.status = 1 
               AND a.product_code = @@ProductCode 
               AND c.interest_coverage_id = @@InterestID 
               AND b.coverage_id = '' 
        UNION 
        SELECT TOP 1 a.product_code    AS ProductCode, 
                     a.product_group   AS ProductGroup, 
                     b.deductible_code AS DeductibleCode 
        FROM   dbo.or_clause_group_product a 
               INNER JOIN dbo.or_clause_interest_coverage b 
                       ON a.product_group = b.product_group 
        WHERE  b.status = 1 
               AND b.interest_id = @@InterestID 
               AND b.coverage_id = '' 
               AND a.product_code = @@ProductCode) aa 
       INNER JOIN dbo.prd_deductible pd 
               ON pd.deductible_code = aa.deductiblecode ";
                var result = db.Query<dynamic>(query,ProductCode,InterestID,CoverageID).ToList();
                if (result != null)
                {
                    return result.FirstOrDefault();
                }
                else {
                    return null;
                }
            }
        }
        #endregion

        #region CoverageGroup

        public List<string> GetCoverageGroup(List<string> InterestCoverageID, string ProductCode)
        {
            List<string> CovGroup = new List<string>();
            string CovID = string.Join(",", InterestCoverageID);
            CovID = CovID.Replace(" ", String.Empty);

            string query = @";DECLARE @@ProductGroup AS VARCHAR(MAX)
                            SELECT TOP 1
                                    @@ProductGroup = Product_Group
                            FROM    dbo.OR_Clause_Group_Product
                            WHERE   Product_Code = @0                            
                            SELECT  Interest_Coverage_Group_ID ,
                                    Product_Group ,
                                    COUNT(*) id
                            INTO    #TEMP
                            FROM    Mapping_Interest_Coverage_Group
                            WHERE   interest_Coverage_ID IN (
                                    SELECT  part
                                    FROM    dbo.Fn_SplitString(RTRIM(LTRIM(@1)),
                                                               ',') )
                                    AND Product_Group = @@ProductGroup
                                    And Status=1
                            GROUP BY Interest_Coverage_Group_ID ,
                                    Product_Group
                            SELECT  COUNT(*) id2 ,
                                    Product_Group ,
                                    Interest_Coverage_Group_ID
                            INTO    #TEMP2
                            FROM    dbo.Mapping_Interest_Coverage_Group AS micg
                            WHERE   Interest_Coverage_Group_ID IN ( SELECT  Interest_Coverage_Group_ID
                                                                    FROM    #TEMP AS t )
                                    AND micg.Product_Group IN ( SELECT  Product_Group
                                                                FROM    #TEMP AS t2 )
                                    And Status=1
                            GROUP BY Product_Group ,
                                    Interest_Coverage_Group_ID
                            SELECT  *
                            FROM    #TEMP AS t
                                    INNER JOIN #TEMP2 AS t2 ON t2.Interest_Coverage_Group_ID = t.Interest_Coverage_Group_ID
                                                               AND t2.Product_Group = t.Product_Group
                                                               AND id = id2
                            DROP TABLE #TEMP
                            DROP TABLE #TEMP2		
";
            using (var db = GetAABDB())
            {
                CovGroup = db.Query<string>(query, ProductCode, CovID).ToList();
                return CovGroup;
            }
        }

        public List<string> GetCoverageGroupMember(List<string> InterestCoverageGroupID, string ProductCode)
        {
            List<string> CovSubGroup = new List<string>();
            string IntCovGroupID = string.Join(",", InterestCoverageGroupID);
            IntCovGroupID = IntCovGroupID.Replace(" ", String.Empty);
            string query = @"
                        SELECT Interest_Coverage_ID FROM dbo.Mapping_Interest_Coverage_Group a 
                        INNER JOIN dbo.OR_Clause_Group_Product b ON b.Product_Group=a.Product_Group
                         WHERE a.Status=1  and a.Interest_Coverage_Group_ID IN (select part from dbo.Fn_SplitString(RTRIM(LTRIM(@0)),',')) AND b.Product_Code=@1";
            using (var db = GetAABDB())
            {
                CovSubGroup = db.Query<string>(query, IntCovGroupID, ProductCode).ToList();
                return CovSubGroup;
            }
        }
        public List<ProductInterestCoverageDeductibleClasue> GetProductInterestCoverageDeductibleClause(string ProductCode, string InterestID, string CoverageID)
        {
            string query = @"SELECT  Distinct PG.Product_Group AS ProductGroup ,
                                    PG.Product_Code AS ProductCode ,
                                    ICIC.Interest_Id AS InterestID ,
                                    ICIC.Coverage_Id AS CoverageID ,
                                    ICIC.Deductible_Code AS DeductibleCode ,
                                    ICIC.Clause_Id AS ClauseID, 
                                    0 AS IsGroup 
                            FROM    dbo.OR_Clause_Group_Product PG
                                    INNER JOIN dbo.OR_Clause_Interest_Coverage ICIC ON ICIC.Product_Group = PG.Product_Group
                            WHERE   PG.Product_Code = @0
                                    AND ( ( ICIC.Interest_Id = @1
                                            AND ICIC.Coverage_Id = @2
                                            AND COALESCE(ICIC.Coverage_Id, '') <> ''
                                          )
                                          OR ( ICIC.Interest_Id = @1
                                               AND COALESCE(ICIC.Coverage_Id, '') = ''
                                             )
                                        )";
            using (var db = GetAABDB())
            {
                var result = db.Query<ProductInterestCoverageDeductibleClasue>(query, ProductCode, InterestID, CoverageID).ToList();
                return result;
            }
        }

        public List<ProductInterestCoverageDeductibleClasue> GetProductInterestCoverageDeductibleClauseGrouped(List<string> InterestCoverageGroupID, string ProductCode)
        {

            string IntCovGroupID = string.Join(",", InterestCoverageGroupID);
            IntCovGroupID = IntCovGroupID.Replace(" ", String.Empty);
            string query = @"SELECT  Distinct c.Product_Group AS ProductGroup,
		                        a.Product_Code AS ProductCode, 
		                        c.Interest_Id AS InterestID,
		                        c.Coverage_Id AS CoverageID,
		                        c.Deductible_Code AS DeductibleCode,
		                        c.Clause_Id AS ClauseID,
                                b.Interest_Coverage_Group_ID AS InterestCoverageGroupID,
		                        1 AS IsGroup				
                        FROM    OR_Clause_Group_Product a
                                INNER JOIN Mapping_Interest_Coverage_Group b ON a.Product_Group = b.Product_Group
                                INNER JOIN dbo.OR_Clause_Interest_Coverage c ON a.Product_Group = c.Product_Group
                        WHERE   a.Product_Code = @0
                                AND b.Status=1 and Interest_Coverage_Group_ID IN (
                                SELECT  part
                                FROM    dbo.Fn_SplitString(RTRIM(LTRIM(@1)), ',') )
                                AND ( ( b.Mapping_Type = 'C'
                                        AND c.Coverage_Id = b.Interest_Coverage_Group_ID
                                      )
                                      OR ( b.Mapping_Type = 'I'
                                           AND c.Interest_Id = b.Interest_Coverage_Group_ID
                                         )
                                    )";
            string query2 = @"SELECT  Interest_Coverage_ID
                            FROM    dbo.Mapping_Interest_Coverage_Group a
                                    INNER JOIN dbo.OR_Clause_Group_Product b ON a.Product_Group = b.Product_Group
                            WHERE   b.Product_Code = @0
                                    AND a.Interest_Coverage_Group_ID = @1 and a.Status=1";
            using (var db = GetAABDB())
            {
                var CovSubGroup = db.Query<ProductInterestCoverageDeductibleClasue>(query, ProductCode, IntCovGroupID).ToList();
                foreach (var items in CovSubGroup)
                {
                    items.GroupMember = db.Query<string>(query2, ProductCode, items.InterestCoverageGroupID).ToList();
                }
                return CovSubGroup;
            }
        }
        
        #endregion

        #region WTOrder

        public WTOrderDetailData GetWTOrderDetailData(string OrderNo)
        {
            WTOrderDetailData WT = new WTOrderDetailData();
            #region qWtOrder
            string qWtOrder = @"SELECT  QuotationNo ,
        OrderNo ,
        ProspectID ,
        CustomerID ,
        NameOnPolicy ,
        OrderType ,
        SalesmanID ,
        BranchID ,
        AOSendDate ,
        ProductCode ,
        SegmentCode ,
        AcceptanceID ,
        FinInstitutionCode ,
        BrokerCode ,
        PolicyHolderCode ,
        PayerCode ,
        PeriodFrom ,
        PeriodTo ,
        SharePct ,
        OurSharePct ,
        OrderStatus ,
        TermCode ,
        RequestDuplicate ,
        PolicyAddressType ,
        DeliveryToType ,
        DeliveryUpName ,
        DeliveryAddressType ,
        DeliveryAddressTypeGC ,
        BillingAddressType ,
        AgentID ,
        UplinerID ,
        LeaderID ,
        SalesmanDealerID ,
        BranchManagerID ,
        SupervisorID ,
        DealerID ,
        PreOrderedVANumber ,
        RowStatus ,
        IsLocked ,
        LastLockedDate ,
        LastLockedBy ,
        OrderSource ,
        Remark ,
        CreatedBy ,
        CreatedDate ,
        ModifiedBy ,
        ModifiedDate,
        SubmitUser,
        GracePeriod,
        MouID
FROM    dbo.WTOrder AS wo
WHERE   OrderNo = @0
        AND RowStatus = 0";
            #endregion
            #region qWtObject
            string qWTObject = @"SELECT  QuotationNo ,
        ObjectNo ,
        Note ,
        CancelDate ,
        Description ,
        PeriodTo ,
        PeriodFrom ,
        RIStatus ,
        BCOwnRetention ,
        BCOriginalSI ,
        BCCoinsuranceIn ,
        Status ,
        BCCoinsuranceOut ,
        BCFacultativeIn ,
        BCSumInsured ,
        RowStatus ,
        EntryUsr ,
        Entrydt ,
        UpdateUsr ,
        UpdateDt
FROM    dbo.WTDtlObject AS wdo
WHERE   QuotationNo = @0 AND RowStatus=0";
            #endregion
            #region qWtOrder
            string qWTMV = @"SELECT  QuotationNo ,
                                ObjectNo ,
                                VehicleCode ,
                                VehicleType ,
                                BrandCode ,
                                ModelCode ,
                                Series ,
                                TransmissionType ,
                                MfgYear ,
                                NewCar ,
                                Kilometers ,
                                Color ,
                                LightColor ,
                                Metalic ,
                                ColorOnBPKB ,
                                ContractNo ,
                                ClientNo ,
                                SittingCapacity ,
                                UsageCode ,
                                GeoAreaCode ,
                                ChasisNumber ,
                                EngineNumber ,
                                RegistrationNumber ,
                                MarketPrice ,
                                CurrId ,
                                RowStatus ,
                                EntryUsr ,
                                EntryDt ,
                                UpdateUsr ,
                                UpdateDt ,
                                SurveyCategory ,
                                IsLimitedAcceptation ,
                                IsCBU ,
                                SurveyCategory ,
	                            SurveyType ,
                                SurveyAreaID ,
                                SurveyAreaCode ,
                                SurveyLatitude ,
                                SurveyLongitude ,
                                SurveyAddress ,
                                SurveySlotID ,
	                            CONVERT(Varchar(10), FORMAT(SurveyDate, 'yyyy-MM-dd')) As SurveyDate ,
					            CONVERT(Varchar(8), CAST(SurveyStartTime As Time)) As SurveyStartTime,
					            CONVERT(Varchar(8), CAST(SurveyEndTime As Time)) As SurveyEndTime,
                                ISNULL(SurveyorID,'') AS SurveyorID,
								ISNULL(SkipSurveyF,1) AS SkipSurveyF
                        FROM    dbo.WTDtlMV AS wdm
                        WHERE   QuotationNo = @0 AND RowStatus=0";
            #endregion
            #region qWTInterest
            string qWTInterest = @"SELECT  QuotationNo ,
        ObjectNo ,
        InterestNo ,
        AdjustableClause ,
        Description ,
        InsuranceType ,
        DedFlatAmount ,
        PeriodFrom ,
        Status ,
        PeriodTo ,
        CancelDate ,
        InterestType ,
        InterestCode ,
        OriginalSI ,
        CoinsuranceIn ,
        ClaimableAmount ,
        SumInsured ,
        FacultativeIn ,
        ExchangeRate ,
        StockMaximumPossible ,
        CurrId ,
        DedFlatCurrency ,
        DeductibleCode ,
        ParNo ,
        RowStatus ,
        EntryUsr ,
        EntryDt ,
        UpdateUsr ,
        UpdateDt
FROM    dbo.WTDtlInterest AS wdm
WHERE   QuotationNo = @0 AND RowStatus=0
";
            #endregion
            #region qWTCoverage
            string qWTCoverage = @"SELECT  QuotationNo ,
        ObjectNo ,
        InterestNo ,
        CoverageNo ,
        InsuranceType ,
        COBId ,
        CurrId ,
        Status ,
        InterestType ,
        CoverageId ,
        NetPremium ,
        BeginDate ,
        EndDate ,
        CancelDate ,
        Ndays ,
        Net1 ,
        Net2 ,
        Net3 ,
        GrossPremium ,
        CoverPremium ,
        SumInsured ,
        Loading ,
        CalcMethod ,
        TotalDiscount ,
        TotalCommission ,
        DedFlatAmount ,
        DedFlatCurrency ,
        DeductibleCode ,
        EntryPct ,
        Rate ,
        Discount ,
        MaxSi ,
        ExcessRate ,
        RowStatus ,
        AcquisitionCost ,
        EntryUsr ,
        EntryDt ,
        UpdateUsr ,
        UpdateDt
FROM    dbo.WTDtlCoverage AS wdc
WHERE   QuotationNo = @0 AND RowStatus=0";
            #endregion
            #region qWTInwardRI
            string qWTInwardRI = @"SELECT  OrderNo ,
        AcceptanceID ,
        FullReceived ,
        CedingCoy ,
        RefPolicy_No ,
        OfferingLetter_Date AS OfferingLetterDate ,
        OfferingLetter_No AS OfferingLetterNo ,
        OurShareAmt ,
        OurSharePct ,
        HandlingFeePct ,
        RISlipDate ,
        SlipReceive_Date AS SlipReceiveDate ,
        WPCDate ,
        PeriodFrom ,
        PeriodTo ,
        OriginalRate ,
        CalcMethod ,
        EntryUsr ,
        EntryDt ,
        UpdateUsr ,
        UpdateDt
FROM    dbo.WTOrdInwardRI
WHERE   OrderNo = @0 And RowStatus=0";
            #endregion
            #region qWTInwardMember
            string qWTInwardMember = @"SELECT  OrderNo,
          MemberCode ,
          MemberType ,
          MemberShare_Pct AS MemberSharePct,
          EntryUsr ,
          EntryDt ,
          UpdateUsr ,
          UpdateDt
FROM    dbo.WTOrdInwardMember AS woim
WHERE   OrderNo = @0 And RowStatus=0";
            #endregion
            #region qWTDtlAddress
            string qWTDtlAddress = @"SELECT  QuotationNo ,
        WTOrderID ,
        AddressType ,
        SourceType ,
        RenewalNotice ,
        DeliveryCode ,
        Address ,
        RT ,
        RW ,
        PostalCode ,
        Phone ,
        Fax ,
        Handphone ,
        ContactPerson ,
        RowStatus ,
        CreatedBy ,
        CreatedDate ,
        ModifiedBy ,
        ModifiedDate
FROM    dbo.WTOrderDtlAddress AS woda
WHERE   QuotationNo = @0 AND rowstatus=0";
            #endregion
            #region qWTObjectClause
            string qWTObjectClause = @"SELECT  
        QuotationNo ,
        ObjectNo ,
        ClauseNo ,
        ClauseID ,
        Description ,
        RowStatus ,
        CreatedBy ,
        CreatedDate ,
        ModifiedBy ,
        ModifiedDate
FROM    dbo.WTOrderObjectClause AS wooc
WHERE   QuotationNo = @0 And rowstatus=0
";
            #endregion
            #region qWTPolicyClause
            string qWTPolicyClause = @"SELECT  QuotationNo ,
        ClauseID ,
        COALESCE(wopc.Description,'') AS Description,
		mc.Description AS ClauseDescription,
        RowStatus ,
        CreatedBy ,
        CreatedDate ,
        ModifiedBy ,
        ModifiedDate
FROM    dbo.WTOrderPolicyClause AS wopc
		INNER JOIN dbo.Mst_Clause AS mc ON mc.Clause_Id=wopc.ClauseID AND mc.Status=1
WHERE   QuotationNo = @0 And RowStatus=0";
            #endregion
            #region qWTInsured
            string qWTInsured = @"SELECT  Distinct
		                    QuotationNo ,
                            ObjectNo ,
                            InterestNo ,        
                            BeginDate ,
                            EndDate ,
                            SumInsured ,
                            Status
                    FROM    dbo.WTDtlCoverage AS wdc
                    WHERE   QuotationNo = @0 AND RowStatus=0";
            #endregion
            #region qWTNoCover
            string qWTNoCover = @"
                SELECT QuotationNo, ObjectNo, EndorsementNo, Type, DamageType AS DemageTypeCode, NoCoverID, Name, 
                Description, Quantity FROM WTNoCover
                WHERE QuotationNo = @0 AND RowStatus=0";
            #endregion
            #region qWTOriginalDefect
            string qWTOriginalDefect = @"SELECT  QuotationNo ,
                                            ObjectNo ,
                                            EndorsementNo ,
                                            PartID ,
                                            Name ,
                                            DamageCategory ,
                                            Description
                                    FROM    dbo.WTOriginalDefect
                                    WHERE   QuotationNo = @0
                                            AND RowStatus = 0";
            #endregion
            #region qWTNonStandardAccessories
            //string qWTNonStandardAccessories = @"
            //                                SELECT QuotationNo ,
            //                                ObjectNo ,
            //                                EndorsementNo ,
            //                                wnsa.AccsCode ,
            //                                COALESCE(AO.AccsDescription,'') AS AccsDescription ,
            //                                wnsa.AccsPartCode ,
            //                             COALESCE(AP.AccsPartDescription,'') AS AccsPartDescription ,
            //                                IncludeTsi ,
            //                                Brand ,
            //                                SumInsured ,
            //                                Quantity ,
            //                                Premi ,
            //                                Category
            //                            FROM    dbo.WTNonStandardAccessories AS wnsa
            //                                LEFT JOIN dbo.AccessoriesOnline AO ON AO.AccsCode = wnsa.AccsCode
            //                             LEFT join AccessoriesPart AP ON AP.AccsPartCode = wnsa.AccsPartCode
            //                            WHERE   QuotationNo = @0
            //                                AND wnsa.RowStatus = 0";
            string qWTNonStandardAccessories = @"SELECT DISTINCT QuotationNo ,
                                                    ObjectNo ,
                                                    EndorsementNo ,
                                                    wnsa.AccsCode ,
                                                    COALESCE(AO.AccsDescription, '') AS AccsDescription,
                                                    wnsa.AccsPartCode ,
		                                            CASE ao.AccsType
                                                                  WHEN 4 THEN x.AccsCatDescription
                                                                  WHEN 5 THEN x.AccsCatDescription
                                                                  ELSE ''
                                                                END AS AccsCatDescription ,      					
		                                             COALESCE(ap.AccsPartDescription, '') AS AccsPartDescription,
                                                    IncludeTsi,
                                                    Brand,
                                                    SumInsured,
                                                    Quantity,
                                                    Premi,
                                                    Category,
                                                    wnsa.AccsCatCode
                                            FROM dbo.WTNonStandardAccessories AS wnsa
                                                 LEFT JOIN dbo.AccessoriesOnline AO ON AO.AccsCode = wnsa.AccsCode AND ao.RowStatus = 1
                                                    LEFT JOIN AccessoriesPart AP ON Ap.AccsCode = wnsa.AccsCode AND AP.AccsPartCode = wnsa.AccsPartCode and ap.RowStatus = 1

                                                    LEFT JOIN (SELECT  ac.AccsCode,
                                                                                    ac.AccsCatCode,
                                                                                    ac.AccsCatDescription,
                                                                                    COALESCE(acp.AccsPartCode, '') AS AccsPartCode,
                                                                                    ac.MaxSICategory,
                                                                                    ac.IsEditable,
                                                                                    acp.QtyDefault,
                                                                                    ac.RowStatus
                                                                            FROM    dbo.AccessoriesCategory AS ac
                                                                                    LEFT JOIN dbo.AccessoriesCategoryPart
                                                                                    AS acp ON acp.AccsCatCode = ac.AccsCatCode
                                                                                              AND acp.RowStatus = 1
                                                                          ) x ON x.AccsCode = ao.AccsCode
                                                                                 AND x.RowStatus = 1
                                                                                 AND x.AccsPartCode = COALESCE(ap.AccsPartCode,
                                                                                                          '') AND x.AccsCode = wnsa.AccsCode and x.AccsCatCode = wnsa.AccsCatCode
                                            WHERE QuotationNo = @0
                                                    AND wnsa.RowStatus = 0";
            #endregion

            using (var db = GetAABDB())
            {
                WT.WTOrder = db.Query<WTOrderData>(qWtOrder, OrderNo).FirstOrDefault();
                if (WT.WTOrder != null)
                {
                    string QuotationNo = WT.WTOrder.QuotationNo;
                    WT.WTObjectData = db.Fetch<WTDtlObjectData>(qWTObject, QuotationNo); ;
                    WT.WTMVData = db.Query<WTDtlMVData>(qWTMV, QuotationNo).ToList();
                    WT.WTDtlInterest = db.Query<WTInterest>(qWTInterest, QuotationNo).ToList();
                    WT.WTDtlCoverage = db.Query<WTDtlCoverage>(qWTCoverage, QuotationNo).ToList();
                    WT.WTOrderDtlAddress = db.Query<WTOrderDtlAddress>(qWTDtlAddress, QuotationNo).ToList();
                    WT.WTOrderObjectClause = db.Query<WTOrderObjectClause>(qWTObjectClause, QuotationNo).ToList();
                    WT.WTOrderPolicyClause = db.Query<WTOrderPolicyClause>(qWTPolicyClause, QuotationNo).ToList();
                    WT.WTInward = db.Query<WTInwardReinsurance>(qWTInwardRI, OrderNo).FirstOrDefault();
                    WT.WTInwardMember = db.Query<WTInwardMember>(qWTInwardMember, OrderNo).ToList();
                    WT.WTInsured = new List<WTInsured>();
                    var Insured = db.Query<WTInsured>(qWTInsured, QuotationNo).ToList();
                    List<WTInsured> TempInsured = new List<WTInsured>();

                    #region Generate Insured
                    for (int i = 0; i < Insured.Count; i++)
                    {
                        bool SamePeriod = false;
                        if (Insured[i].BeginDate.Date.AddYears(1) > Insured[i].EndDate.Date)
                        {
                            int j = i + 1;
                            if (i + 1 < Insured.Count)
                            {
                                if (Insured[i].InterestNo == Insured[j].InterestNo && Insured[i].ObjectNo == Insured[j].ObjectNo && Insured[i].SumInsured == Insured[j].SumInsured)
                                {
                                    if (Insured[i].BeginDate.Date.AddYears(1) >= Insured[j].EndDate.Date)
                                    {
                                        SamePeriod = true;
                                        goto Done;
                                    }
                                }
                            }
                        }
                    Done:
                        if (SamePeriod)
                        {
                            WTInsured wi = Insured[i];
                            wi.EndDate = Insured[i].EndDate;
                            TempInsured.Add(wi);
                            i = i + 1;
                        }
                        else
                        {
                            TempInsured.Add(Insured[i]);
                        }
                    }
                    #endregion
                    WT.WTInsured.AddRange(TempInsured);
                    WT.WTNoCover = db.Query<NoCover>(qWTNoCover, QuotationNo).ToList();
                    WT.WTOriginalDefect = db.Query<OriginalDefect>(qWTOriginalDefect, QuotationNo).ToList();
                    WT.WTNonStandardAccessories = db.Query<NonStandardAccessories>(qWTNonStandardAccessories, QuotationNo).ToList();
                }
            }
            return WT;


        }

        public void UpdateSubmitUser(string OrderNo, string SubmitUser)
        {
            string query = @"UPDATE dbo.WTOrder SET SubmitUser=@0 WHERE OrderNo=@1";
            using (var db = GetAABDB())
            {
                var result = db.Execute(query, SubmitUser, OrderNo);

            }
        }
        public static List<WTCommission> GetRenewalDiscount(string OldPolicyNo)
        {
            string qGetRenDisc = ";Exec dbo.usp_GetPolicyNCB @@OldPolicyNo=@0";

            using (var db = GetAABDB())
            {
                return db.Query<WTCommission>(qGetRenDisc, OldPolicyNo).ToList();
            }
        }
        #endregion

        #region add mst_order


        public bool AddOrder(OrderData O, string EntryUser, PendingOrderInfo ExInfo)
        {
            string query = @"INSERT INTO dbo.Mst_Order
                                ( Order_No ,
                                  Endorsement_No ,
                                  Org_Endorsement_No ,
                                  Order_Type ,
                                  Policy_Id ,
                                  Policy_No ,
                                  Ref_No ,
                                  Order_Date ,
                                  Transaction_Date ,
                                  Marketing_Date ,
                                  Effective_Date ,
                                  Endorsement_Type ,
                                  Endorsement_Code ,
                                  Old_Policy_No ,
                                  Branch_Id ,
                                  COB_Id ,
                                  MOU_ID ,
                                  Product_Code ,
                                  Dept_Id ,
                                  Salesman_Id ,
                                  Segment_Code ,
                                  Acceptance_Id ,
                                  Prospect_Id ,
                                  Cust_Id ,
                                  Fin_Institution_Code ,
                                  Broker_Code ,
                                  Policy_Holder_Code ,
                                  Payer_Code ,
                                  Name_On_Policy ,
                                  Name_On_Card ,
                                  Form_Code ,
                                  Period_From ,
                                  Period_To ,
                                  Auto_Renewal ,
                                  Grace_Period ,
                                  RL1 ,
                                  RL2 ,
                                  Print_Policy ,
                                  Print_Cover_Note ,                                  
                                  MO_Approval ,
                                  MO_Approval_Id ,                                  
                                  Inward_Id ,
                                  OutWard_Id ,
                                  Survey_No ,
                                  Calc_Method ,
                                  Premium_Entry_Pct ,
                                  Share_Pct ,
                                  Our_Share_Pct ,
                                  Renewal_Status ,
                                  Status ,
                                  Order_Status ,
                                  Analysis ,
                                  Term_Code ,
                                  User_Id ,
                                  Fee_Currency ,
                                  Fee_Payment ,
                                  Commission_Payment ,
                                  Policy_Fee ,
                                  Stamp_Duty ,
                                  Notes ,
                                  Reject_Remarks ,
                                  Scheme_Code ,
                                  Non_Standard ,
                                  Period_Approval ,
                                  EntryUsr ,                                          
                                  SalesAI_ID ,
                                  Mst_Policy_No ,
                                  VANumber,
                                  EntryDt ,
                                  UpdateUsr,
                                  UpdateDt
                                )
                        VALUES  ( @0 , -- Order_No - char(15)
                                  @1 , -- Endorsement_No - numeric
                                  @2 , -- Org_Endorsement_No - numeric
                                  @3 , -- Order_Type - char(1)
                                  @4 , -- Policy_Id - char(12)
                                  @5 , -- Policy_No - char(16)
                                  @6 , -- Ref_No - char(20)
                                  @7 , -- Order_Date - datetime
                                  @8 , -- Transaction_Date - datetime
                                  @9 , -- Marketing_Date - datetime
                                  @10 , -- Effective_Date - datetime
                                  @11 , -- Endorsement_Type - char(6)
                                  @12 , -- Endorsement_Code - char(6)
                                  @13 , -- Old_Policy_No - char(16)
                                  @14 , -- Branch_Id - char(3)
                                  @15 , -- COB_Id - char(3)
                                  @16 , -- MOU_ID - char(16)
                                  @17 , -- Product_Code - char(5)
                                  @18 , -- Dept_Id - char(3)
                                  @19 , -- Salesman_Id - char(5)
                                  @20 , -- Segment_Code - char(6)
                                  @21 , -- Acceptance_Id - char(6)
                                  @22 , -- Prospect_Id - char(11)
                                  @23 , -- Cust_Id - char(11)
                                  @24 , -- Fin_Institution_Code - char(11)
                                  @25 , -- Broker_Code - char(11)
                                  @26 , -- Policy_Holder_Code - char(11)
                                  @27 , -- Payer_Code - char(11)
                                  @28 , -- Name_On_Policy - varchar(200)
                                  @29 , -- Name_On_Card - varchar(200)
                                  @30 , -- Form_Code - char(6)
                                  @31 , -- Period_From - datetime
                                  @32 , -- Period_To - datetime
                                  @33 , -- Auto_Renewal - bit
                                  @34 , -- Grace_Period - numeric
                                  @35 , -- RL1 - numeric
                                  @36 , -- RL2 - numeric
                                  @37 , -- Print_Policy - bit
                                  @38 , -- Print_Cover_Note - bit                                                                    
                                  @39 , -- MO_Approval - bit
                                  @40 , -- MO_Approval_Id - char(12)
                                  @41 , -- Inward_Id - char(8)
                                  @42 , -- OutWard_Id - char(8)
                                  @43 , -- Survey_No - char(8)
                                  @44 , -- Calc_Method - char(1)
                                  @45 , -- Premium_Entry_Pct - numeric
                                  @46 , -- Share_Pct - numeric
                                  @47 , -- Our_Share_Pct - numeric
                                  @48 , -- Renewal_Status - bit                                  
                                  @49 , -- Status - char(1)
                                  @50 , -- Order_Status - char(2)                                  
                                  @51 , -- Analysis - text
                                  @52 , -- Term_Code - char(6)
                                  @53 , -- User_Id - char(5)
                                  @54 , -- Fee_Currency - char(3)
                                  @55 , -- Fee_Payment - char(1)
                                  @56 , -- Commission_Payment - char(1)
                                  @57 , -- Policy_Fee - numeric
                                  @58 , -- Stamp_Duty - numeric
                                  @59 , -- Notes - text
                                  @60 , -- Reject_Remarks - text
                                  @61 , -- Scheme_Code - char(6)
                                  @62 , -- Non_Standard - bit
                                  @63 , -- Period_Approval - char(1)
                                  @64 , -- EntryUsr - char(5)
                                        
                                  @65 , -- SalesAI_ID - varchar(11)
                                  @66 , -- Mst_Policy_No - char(16)
                                  @67 , -- VANumber - varchar(30)
                                  @68 , -- EntryDt - datetime  
                                    @69 ,
                                    @70
                                )";
            int result = 0;
            string UpdateUsr = EntryUser;
            DateTime EntryDt = DateTime.Now;
            DateTime UpdateDt = DateTime.Now;
            if (ExInfo != null)
            {
                EntryUser = ExInfo.EntryUser;
                EntryDt = ExInfo.EntryDate;
            }
            using (var db = GetAABDB())
            {
                db.Execute(query, IsNA(O.OrderNo), IsNA(O.EndorsementNo), IsNA(O.OrgEndorsementNo), IsNA(O.OrderType), IsNA(O.PolicyId), IsNA(O.PolicyNo), IsNA(O.RefNo), IsNA(O.OrderDate), IsNA(O.TransactionDate), IsNA(O.MarketingDate), IsNA(O.EffectiveDate), IsNA(O.EndorsementType), IsNA(O.EndorsementCode), IsNA(O.OldPolicyNo), IsNA(O.BranchId), IsNA(O.COBId), IsNA(O.MOUID), IsNA(O.ProductCode), IsNA(O.DeptId), IsNA(O.SalesmanId), IsNA(O.SegmentCode), IsNA(O.AcceptanceId), IsNA(O.ProspectId), IsNA(O.CustId), IsNA(O.FinInstitutionCode), IsNA(O.BrokerCode), IsNA(O.PolicyHolderCode), IsNA(O.PayerCode), IsNA(O.NameOnPolicy), IsNA(O.NameOnCard), IsNA(O.FormCode), O.PeriodFrom, O.PeriodTo, IsNA(O.AutoRenewal), IsNA(O.GracePeriod), IsNA(O.RL1), IsNA(O.RL2), IsNA(O.PrintPolicy), IsNA(O.PrintCoverNote), IsNA(O.MOApproval), IsNA(O.MOApprovalId), IsNA(O.InwardId), O.OutWardId, IsNA(O.SurveyNo), O.CalcMethod, IsNA(O.PremiumEntryPct), IsNA(O.SharePct), IsNA(O.OurSharePct), IsNA(O.RenewalStatus), IsNA(O.Status), IsNA(O.OrderStatus), IsNA(O.Analysis), IsNA(O.TermCode), O.UserId, IsNA(O.FeeCurrency), IsNA(O.FeePayment), IsNA(O.CommissionPayment), IsNA(O.PolicyFee), IsNA(O.StampDuty), IsNA(O.Notes), IsNA(O.RejectRemarks), IsNA(O.SchemeCode), IsNA(O.NonStandard), IsNA(O.PeriodApproval), IsNA(EntryUser), IsNA(O.SalesAIID), IsNA(O.MstPolicyNo), IsNA(O.VANumber),
                    IsNA(EntryDt), IsNA(UpdateUsr), IsNA(UpdateDt));
                if (!string.IsNullOrEmpty(O.VANumber) && !string.IsNullOrWhiteSpace(O.VANumber)) {
                    string qOrdVATRansaction = @"INSERT INTO dbo.Ord_VA_Transaction
			                        ( OrderNo, PolicyNo, VANo, UploadStatus, EntryUsr, EntryDt)
			                VALUES  ( @0, @1, @2, @3, @4, GETDATE())";
                    db.Execute(qOrdVATRansaction, IsNA(O.OrderNo), IsNA(O.PolicyNo), IsNA(O.VANumber), 1, IsNA(O.SalesmanId));
                }
                result++;
            }
            return result > 0;
        }

        public bool AddObjectOrder(List<ObjectOrderData> Obj, string EntryUser)
        {
            string query = @"INSERT  INTO dbo.Ord_Dtl_Object
                                    ( Order_No ,
                                      Object_No ,
                                      RI_Status ,                                     
                                      Status ,
                                      Period_From ,                                      
                                      Period_To ,
                                      Description ,
                                      BC_Own_Retention ,
                                      BC_Facultative_In ,
                                      BC_Sum_Insured ,
                                      BC_Coinsurance_In ,
                                      BC_Original_SI ,
                                      BC_Total_Facultative ,
                                      BC_Coinsurance_Out ,
                                      BC_Total_Treaty ,
                                      Standard_Reference_Id ,
                                      EntryUsr ,
                                      EntryDt   ,
                                      UpdateUsr                                                                             
                                    )
                            VALUES  ( @0 , -- char(15)
                                      @1 , -- numeric
                                      @2, -- char(1)                                     
                                      @3 , -- char(1)
                                      @4 , -- datetime
                                      @5 ,
                                      SUBSTRING(ISNULL(@6, ''), 1, 50) , -- varchar(50)
                                      @7 , -- numeric
                                      @8 , -- numeric
                                      @9 , -- numeric
                                      @10 , -- numeric
                                      @11 , -- numeric
                                      @12 , -- numeric
                                      @13 , -- numeric
                                      @14 , -- numeric          
                                      @15 , -- int
                                      @16 , -- char(5)
                                      GetDate()   ,
                                      ''                                   
                                    )";
            int result = 0;
            using (var db = GetAABDB())
            {
                for (int i = 0; i < Obj.Count; i++)
                {
                    result += db.Execute(query, IsNA(Obj[i].OrderNo), IsNA(Obj[i].ObjectNo), IsNA(Obj[i].RIStatus), IsNA(Obj[i].Status), Obj[i].PeriodFrom, Obj[i].PeriodTo, IsNA(Obj[i].Description), IsNA(Obj[i].BCOwnRetention), IsNA(Obj[i].BCFacultativeIn), IsNA(Obj[i].BCSumInsured), IsNA(Obj[i].BCCoinsuranceIn), Obj[i].BCOriginalSI, IsNA(Obj[i].BCTotalFacultative), IsNA(Obj[i].BCCoinsuranceOut), IsNA(Obj[i].BCTotalTreaty), IsNA(Obj[i].StandardReferenceId), IsNA(EntryUser));
                }

            }
            return result == Obj.Count;
        }

        public bool AddObjectOrderDetail(List<ObjectOrderDetailData> MV, string EntryUser)
        {
            string query = @";DECLARE @@OrderNo AS VARCHAR(4000) ,
                                    @@ObjectNo AS VARCHAR(4000) ,
                                    @@VehicleCode AS VARCHAR(4000) ,
                                    @@VehicleType AS VARCHAR(4000) ,
                                    @@BrandCode AS VARCHAR(4000) ,
                                    @@ModelCode AS VARCHAR(4000) ,
                                    @@Series AS VARCHAR(4000) ,
                                    @@TransmissionType AS VARCHAR(4000) ,
                                    @@MfgYear AS VARCHAR(4000) ,
                                    @@NewCar AS VARCHAR(4000) ,
                                    @@Kilometers AS VARCHAR(4000) ,
                                    @@Color AS VARCHAR(4000) ,
                                    @@LightColor AS VARCHAR(4000) ,
                                    @@Metalic AS VARCHAR(4000) ,
                                    @@ColorOnBPKB AS VARCHAR(4000) ,
                                    @@ContractNo AS VARCHAR(4000) ,
                                    @@ClientNo AS VARCHAR(4000) ,
                                    @@SittingCapacity AS VARCHAR(4000) ,
                                    @@UsageCode AS VARCHAR(4000) ,
                                    @@GeoAreaCode AS VARCHAR(4000) ,
                                    @@ChasisNumber AS VARCHAR(4000) ,
                                    @@EngineNumber AS VARCHAR(4000) ,
                                    @@RegistrationNumber AS VARCHAR(4000) ,
                                    @@MarketPrice AS VARCHAR(4000) ,
                                    @@CurrId AS VARCHAR(4000) ,
                                    @@EntryUsr AS VARCHAR(4000)                                     

                                SELECT  @@OrderNo = @0 ,
                                        @@ObjectNo = @1 ,
                                        @@VehicleCode = @2 ,
                                        @@VehicleType = @3 ,
                                        @@BrandCode = @4 ,
                                        @@ModelCode = @5 ,
                                        @@Series = @6 ,
                                        @@TransmissionType = @7 ,
                                        @@MfgYear = @8 ,
                                        @@NewCar = @9 ,
                                        @@Kilometers = @10 ,
                                        @@Color = @11 ,
                                        @@LightColor = @12 ,
                                        @@Metalic = @13 ,
                                        @@ColorOnBPKB = @14 ,
                                        @@ContractNo = @15 ,
                                        @@ClientNo = @16 ,
                                        @@SittingCapacity = @17 ,
                                        @@UsageCode = @18 ,
                                        @@GeoAreaCode = @19 ,
                                        @@ChasisNumber = @20 ,
                                        @@EngineNumber = @21 ,
                                        @@RegistrationNumber = @22 ,
                                        @@MarketPrice = @23 ,
                                        @@CurrId = @24 ,
                                        @@EntryUsr = @25 
                                       
                                INSERT  INTO dbo.Ord_Dtl_MV
                                        ( Order_No ,
                                          Object_No ,
                                          Vehicle_Code ,
                                          Vehicle_Type ,
                                          Brand_Code ,
                                          Model_Code ,
                                          Series ,
                                          Transmission_Type ,
                                          Mfg_Year ,
                                          New_Car ,
                                          Kilometers ,
                                          Color ,
                                          Light_Color ,
                                          Metalic ,
                                          Color_On_BPKB ,
                                          Contract_No ,
                                          Client_No ,
                                          Sitting_Capacity ,
                                          Usage_Code ,
                                          Geo_Area_Code ,
                                          Chasis_Number ,
                                          Engine_Number ,
                                          Registration_Number ,
                                          Market_Price ,
                                          Curr_Id ,
                                          EntryUsr ,
                                          EntryDt                                          
                                        )
                                VALUES  ( @@OrderNo ,
                                          @@ObjectNo ,
                                          @@VehicleCode ,
                                          @@VehicleType ,
                                          @@BrandCode ,
                                          @@ModelCode ,
                                          @@Series ,
                                          @@TransmissionType ,
                                          @@MfgYear ,
                                          @@NewCar ,
                                          @@Kilometers ,
                                          @@Color ,
                                          @@LightColor ,
                                          @@Metalic ,
                                          @@ColorOnBPKB ,
                                          @@ContractNo ,
                                          @@ClientNo ,
                                          @@SittingCapacity ,
                                          @@UsageCode ,
                                          @@GeoAreaCode ,
                                          @@ChasisNumber ,
                                          @@EngineNumber ,
                                          @@RegistrationNumber ,
                                          @@MarketPrice ,
                                          @@CurrId ,
                                          @@EntryUsr ,
                                          GetDate()                                          
                                        )";
            int result = 0;
            using (var db = GetAABDB())
            {
                for (int i = 0; i < MV.Count; i++)
                {
                    result += db.Execute(query, IsNA(MV[i].OrderNo), IsNA(MV[i].ObjectNo), IsNA(MV[i].VehicleCode), IsNA(MV[i].VehicleType), IsNA(MV[i].BrandCode), IsNA(MV[i].ModelCode), IsNA(MV[i].Series), IsNA(MV[i].TransmissionType), IsNA(MV[i].MfgYear), IsNA(MV[i].NewCar), IsNA(MV[i].Kilometers), IsNA(MV[i].Color), IsNA(MV[i].LightColor), IsNA(MV[i].Metalic), IsNA(MV[i].ColorOnBPKB), IsNA(MV[i].ContractNo), IsNA(MV[i].ClientNo), IsNA(MV[i].SittingCapacity), IsNA(MV[i].UsageCode), IsNA(MV[i].GeoAreaCode), IsNA(MV[i].ChasisNumber), IsNA(MV[i].EngineNumber), IsNA(MV[i].RegistrationNumber), IsNA(MV[i].MarketPrice), IsNA(MV[i].CurrId), IsNA(EntryUser));
                }
            }
            return result == MV.Count;

        }

        #region old
        //public bool AddSurveyImage(List<SurveyImage> SurveyImageList, string EntryUser)
        //{
        //    bool ret = false;
        //    string qDtlSurveyAcceptance = @"INSERT INTO dbo.dtl_survey_acceptance
        //                            ( Survey_No ,
        //                              Doc_No ,
        //                              Document_Type ,
        //                              Doc_DESCRIPTION ,
        //                              EntryUsr ,
        //                              EntryDt 
        //                            )
        //                    VALUES  ( @0 , -- Survey_No - varchar(20)
        //                              @1 , -- Doc_No - int
        //                              @2 , -- Document_Type - varchar(30)
        //                              @3 , -- Doc_DESCRIPTION - varchar(100)
        //                              @4 , -- EntryUsr - char(5)
        //                              GETDATE()  -- EntryDt - datetime
        //                            )";
        //    string qDtlSurveyAcceptanceImg = @"INSERT INTO dbo.dtl_survey_acceptance_Image
        //                                                ( Survey_No ,
        //                                                  Doc_No ,
        //                                                  Image_No ,
        //                                                  Image_ID ,
        //                                                  Image_Name ,
        //                                                  Image ,
        //                                                  Thumbnail ,
        //                                                  EntryUsr ,
        //                                                  EntryDt 
        //                                                )
        //                                        VALUES  ( @0, -- Survey_No - varchar(20)
        //                                                  @1 , -- Doc_No - int
        //                                                  1 , -- Image_No - int
        //                                                  @2 , -- Image_ID - char(12)
        //                                                  @3 , -- Image_Name - varchar(100)
        //                                                  NULL , -- Image - varbinary(max)
        //                                                  NULL , -- Thumbnail - varbinary(max)
        //                                                  @4 , -- EntryUsr - char(5)
        //                                                  GETDATE()  -- EntryDt - datetime
        //                                                )";
        //    string qTempMobileSurveyImage = @"INSERT INTO dbo.TempMobileSurveyImage
        //                                            ( ID ,
        //                                                SurveyNo ,
        //                                                FileName ,
        //                                                EntryDt ,
        //                                                Status 
        //                                            )
        //                                    VALUES  ( NEWID() , -- ID - uniqueidentifier
        //                                                @0 , -- SurveyNo - varchar(50)
        //                                                @1 , -- FileName - varchar(512)
        //                                                GETDATE() , -- EntryDt - datetime
        //                                                0  -- Status - int
        //                                            )";
        //    using (var db = GetAABDB())
        //    {
        //        int docNo = 1;
        //        for (int i = 0; i < SurveyImageList.Count; i++)
        //        {
        //            if (!string.IsNullOrEmpty(SurveyImageList[i].SurveyNo))
        //            {
        //                db.Execute(qDtlSurveyAcceptance,
        //                                            IsNA(SurveyImageList[i].SurveyNo),
        //                                            docNo,
        //                                            IsNA(SurveyImageList[i].ImageType),
        //                                            IsNA(SurveyImageList[i].ImageCategory),
        //                                            EntryUser);
        //                db.Execute(qDtlSurveyAcceptanceImg,
        //                                            IsNA(SurveyImageList[i].SurveyNo),
        //                                            docNo,
        //                                            IsNA(SurveyImageList[i].CoreImageID),
        //                                            IsNA(SurveyImageList[i].FileName),
        //                                            EntryUser);
        //                //db.Execute(qTempMobileSurveyImage,
        //                //                            IsNA(SurveyImageList[i].SurveyNo),
        //                //                            IsNA(SurveyImageList[i].FileName));
        //            }
        //            docNo++;
        //        }
        //        ret = true;
        //    }
        //    return ret;
        //}
        #endregion
        public bool AddSurveyImage(List<SurveyImage> SurveyImageList, string EntryUser)
        {
            bool ret = false;
            string qDtlSurveyAcceptance = @";DECLARE @@DocNo INT
SELECT @@DocNo=Doc_No FROM dbo.dtl_survey_acceptance WHERE Survey_No = @0 AND Doc_DESCRIPTION = @3
IF (@@DocNo > 0)
BEGIN
	UPDATE dbo.dtl_survey_acceptance_Image SET Image_ID = @5, Image_Name = @6
	WHERE Survey_No = @0 AND Doc_No = @@DocNo
END
ELSE
BEGIN
	INSERT INTO dbo.dtl_survey_acceptance
			( Survey_No ,
				Doc_No ,
				Document_Type ,
				Doc_DESCRIPTION ,
				EntryUsr ,
				EntryDt 
			)
	VALUES  ( @0 , -- Survey_No - varchar(20)
				@1 , -- Doc_No - int
				@2 , -- Document_Type - varchar(30)
				@3 , -- Doc_DESCRIPTION - varchar(100)
				@4 , -- EntryUsr - char(5)
				GETDATE()  -- EntryDt - datetime
			)

	INSERT INTO dbo.dtl_survey_acceptance_Image
			( Survey_No ,
				Doc_No ,
				Image_No ,
				Image_ID ,
				Image_Name ,
				Image ,
				Thumbnail ,
				EntryUsr ,
				EntryDt 
			)
	VALUES  ( @0, -- Survey_No - varchar(20)
				@1 , -- Doc_No - int
				1 , -- Image_No - int
				@5 , -- Image_ID - char(12)
				@6 , -- Image_Name - varchar(100)
				NULL , -- Image - varbinary(max)
				NULL , -- Thumbnail - varbinary(max)
				@4 , -- EntryUsr - char(5)
				GETDATE()  -- EntryDt - datetime
			)
END";
            using (var db = GetAABDB())
            {
                int oldDocNo = 0;
                if (SurveyImageList.Count > 0) {
                    oldDocNo = db.ExecuteScalar<int>(@"SELECT COUNT(*) FROM dbo.dtl_survey_acceptance WHERE Survey_No = @0", SurveyImageList[0].SurveyNo);
                }
                int docNo = 1;
                for (int i = 0; i < SurveyImageList.Count; i++)
                {
                    if (!string.IsNullOrEmpty(SurveyImageList[i].SurveyNo))
                    {
                        db.Execute(qDtlSurveyAcceptance,
                                                    IsNA(SurveyImageList[i].SurveyNo),
                                                    docNo,
                                                    IsNA(SurveyImageList[i].ImageType),
                                                    IsNA(SurveyImageList[i].ImageCategory),
                                                    EntryUser,
                                                    IsNA(SurveyImageList[i].CoreImageID),
                                                    IsNA(SurveyImageList[i].FileName));
                    }
                    docNo++;
                }
                ret = true;
            }
            return ret;
        }

        public bool AddNoCover(List<NoCoverOrdData> NoCover, string EntryUser)
        {
            string query = @"IF NOT EXISTS ( SELECT  1
                                        FROM    dbo.Ord_Dtl_NoCover
                                        WHERE   Order_No = @0
                                                AND Object_No = @1
                                                AND Endorsement_No = @2
                                                AND Type = @3
                                                AND NoCover_ID = @4 )
                            BEGIN
                                INSERT  INTO dbo.Ord_Dtl_NoCover
                                        ( Order_No ,
                                          Object_No ,
                                          Endorsement_No ,
                                          Type ,
                                          NoCover_ID ,
                                          Name ,
                                          Description ,
                                          Quantity ,
                                          RowStatus ,
                                          EntryDt ,
                                          EntryUsr ,
                                          DamageType 
                                        )
                                VALUES  ( @0 , -- Order_No - varchar(25)
                                          @1 , -- Object_No - smallint
                                          @2 , -- Endorsement_No - smallint
                                          @3 , -- Type - varchar(20)
                                          @4 , -- NoCover_ID - varchar(30)
                                          @5 , -- Name - varchar(100)
                                          @6 , -- Description - varchar(1)
                                          @7 , -- Quantity - int
                                          0 , -- RowStatus - int
                                          GETDATE() , -- EntryDt - datetime
                                          @8 , -- EntryUsr - varchar(10)
                                          @9   -- DamageType - varchar(6)
                                        )
                            END
                        ELSE
                            BEGIN
                                UPDATE  Ord_Dtl_NoCover
                                SET     Name = @5 ,
                                        Description = @6 ,
                                        Quantity = @7 ,
                                        UpdateDt = GETDATE() ,
                                        UpdateUsr = @8
                                WHERE   Order_No = @0
                                        AND Object_No = @1
                                        AND Endorsement_No = @2
                                        AND Type = @3
                                        AND NoCover_ID = @4
                            END";
            int result = 0;
            using (var db = GetAABDB())
            {
                for (int i = 0; i < NoCover.Count; i++)
                {
                    result += db.Execute(query,
                                                IsNA(NoCover[i].OrderNo),
                                                IsNA(NoCover[i].ObjectNo),
                                                IsNA(NoCover[i].EndorsementNo),
                                                IsNA(NoCover[i].Type),
                                                IsNA(NoCover[i].NoCoverID),
                                                IsNA(NoCover[i].Name),
                                                IsNA(NoCover[i].Description),
                                                IsNA(NoCover[i].Quantity),
                                                EntryUser,
                                                IsNA(NoCover[i].DemageType));
                }
            }
            return result == NoCover.Count;

        }

        public bool AddOriginalDefect(List<OriginalDefectOrdData> originalDefect, string EntryUser)
        {
            string query = @"IF NOT EXISTS ( SELECT  1
                                        FROM    dbo.Ord_Dtl_Original_Defect
                                        WHERE   Order_No = @0
                                                AND Object_No = @1
                                                AND Endorsement_No = @2
                                                AND Part_ID = @3 )
                            BEGIN
                                INSERT  INTO dbo.Ord_Dtl_Original_Defect
                                        ( Order_No ,
                                          Object_No ,
                                          Endorsement_No ,
                                          Part_ID ,
                                          Name ,
                                          Damage_Category ,
                                          Description ,
                                          RowStatus ,
                                          EntryDt ,
                                          EntryUsr 
                                        )
                                VALUES  ( @0 , -- Order_No - varchar(25)
                                          @1 , -- Object_No - smallint
                                          @2 , -- Endorsement_No - smallint
                                          @3 , -- Part_ID - varchar(30)
                                          @4 , -- Name - varchar(50)
                                          @5 , -- Damage_Category - varchar(6)
                                          @6 , -- Description - varchar(100)
                                          0 , -- RowStatus - int
                                          GETDATE() , -- EntryDt - datetime
                                          @7  -- EntryUsr - varchar(10)
                                        )
                            END
                        ELSE
                            BEGIN
                                UPDATE  dbo.Ord_Dtl_Original_Defect
                                SET     Name = @4 ,
                                        Damage_Category = @5 ,
                                        Description = @6 ,
                                        UpdateDt = GETDATE() ,
                                        UpdateUsr = @7
                                WHERE   Order_No = @0
                                        AND Object_No = @1
                                        AND Endorsement_No = @2
                                        AND Part_ID = @3
                            END";
            int result = 0;
            using (var db = GetAABDB())
            {
                for (int i = 0; i < originalDefect.Count; i++)
                {
                    result += db.Execute(query,
                                                IsNA(originalDefect[i].OrderNo),
                                                IsNA(originalDefect[i].ObjectNo),
                                                IsNA(originalDefect[i].EndorsementNo),
                                                IsNA(originalDefect[i].PartID),
                                                IsNA(originalDefect[i].Name),
                                                IsNA(originalDefect[i].DamageCategory),
                                                IsNA(originalDefect[i].Description),
                                                EntryUser);
                }
            }
            return result == originalDefect.Count;

        }


        public bool AddNonStandardAccessories(List<NonStandardAccessoriesOrdData> nonStandardAccessoriesOrdData, string EntryUser)
        {
            string query = @"IF NOT EXISTS ( SELECT  1
                                        FROM    dbo.Ord_Non_Standard_Accessories
                                        WHERE   Order_No = @0
                                                AND Object_No = @1
                                                AND Endorsement_No = @2
                                                AND Accs_Code = @3
                                                AND Accs_Part_Code = @4 )
                            BEGIN
                                INSERT  INTO dbo.Ord_Non_Standard_Accessories
                                        ( Order_No ,
                                          Object_No ,
                                          Endorsement_No ,
                                          Accs_Code ,
                                          Accs_Part_Code ,
                                          Include_Tsi ,
                                          Brand ,
                                          Sum_Insured ,
                                          Quantity ,
                                          Premi ,
                                          Category ,
                                          Row_Status ,
                                          Created_Date ,
                                          Created_By ,
                                          Accs_Cat_Code
                                        )
                                VALUES  ( @0 , -- Order_No - varchar(25)
                                          @1 , -- Object_No - smallint
                                          @2 , -- Endorsement_No - smallint
                                          @3 , -- Accs_Code - varchar(20)
                                          @4 , -- Accs_Part_Code - varchar(30)
                                          @5 , -- Include_Tsi - smallint
                                          @6 , -- Brand - varchar(50)
                                          @7 , -- Sum_Insured - numeric
                                          @8 , -- Quantity - int
                                          @9 , -- Premi - numeric
                                          @10 , -- Category - int
                                          0 , -- Row_Status - int
                                          GETDATE() , -- Created_Date - datetime
                                          @11,  -- Created_By - varchar(10)
                                          @12
                                        )
                            END
                        ELSE
                            BEGIN
                                UPDATE  dbo.Ord_Non_Standard_Accessories
                                SET     Include_Tsi = @5 ,
                                        Brand = @6 ,
                                        Sum_Insured = @7 ,
                                        Quantity = @8 ,
                                        Premi = @9 ,
                                        Category = @10 ,
                                        Modified_Date = GETDATE() ,
                                        Modified_By = @11,
                                        Accs_Cat_Code=@12
                                WHERE   Order_No = @0
                                        AND Object_No = @1
                                        AND Endorsement_No = @2
                                        AND Accs_Code = @3
                                        AND Accs_Part_Code = @4
                            END";
            int result = 0;
            using (var db = GetAABDB())
            {
                for (int i = 0; i < nonStandardAccessoriesOrdData.Count; i++)
                {
                    result += db.Execute(query,
                                                IsNA(nonStandardAccessoriesOrdData[i].OrderNo),
                                                IsNA(nonStandardAccessoriesOrdData[i].ObjectNo),
                                                IsNA(nonStandardAccessoriesOrdData[i].EndorsementNo),
                                                IsNA(nonStandardAccessoriesOrdData[i].AccsCode),
                                                IsNA(nonStandardAccessoriesOrdData[i].AccsPartCode),
                                                IsNA(nonStandardAccessoriesOrdData[i].IncludeTsi),
                                                IsNA(nonStandardAccessoriesOrdData[i].Brand),
                                                IsNA(nonStandardAccessoriesOrdData[i].SumInsured),
                                                IsNA(nonStandardAccessoriesOrdData[i].Quantity),
                                                IsNA(nonStandardAccessoriesOrdData[i].Premi),
                                                IsNA(nonStandardAccessoriesOrdData[i].Category),
                                                EntryUser,
                                                IsNA(nonStandardAccessoriesOrdData[i].AccsCatCode));
                }
            }
            return result == nonStandardAccessoriesOrdData.Count;
        }

        public bool AddScoring(List<ScoringData> Scr, string EntryUser)
        {
            string query = @"INSERT  INTO dbo.Ord_Dtl_Scoring
                                        ( Order_No ,
                                          Object_No ,
                                          Factor_Code ,
                                          Insurance_Code ,
                                          EntryUsr ,
                                          EntryDt 	,
                                          UpdateUsr                                         	          
		                                )
                                VALUES  ( @0 , -- Order_No - char(15)
                                          @1 , -- Object_No - numeric
                                          @2 , -- Factor_Code - char(6)
                                          @3 , -- Insurance_Code - char(6)
                                          @4 , -- EntryUsr - char(5)
                                          GETDATE(),  -- EntryDt - datetime	
                    	                  ''
		                                )";
            using (var db = GetAABDB())
            {
                int result = 0;
                for (int i = 0; i < Scr.Count; i++)
                {
                    result += db.Execute(query, IsNA(Scr[i].OrderNo), IsNA(Scr[i].ObjectNo), IsNA(Scr[i].FactorCode), IsNA(Scr[i].InsuranceCode), EntryUser);
                }
                return result >= Scr.Count;
            }
        }

        public bool AddInterest(List<InterestData> Intr, string EntryUser)
        {
            string query = @"INSERT  INTO dbo.Ord_Dtl_Interest
                            ( Order_no ,
                              Object_No ,
                              Interest_No ,
                              Adjustable_Clause ,
                              Description ,
                              Insurance_Type ,
                              Ded_Flat_Amount ,
                              Period_From ,
                              Status ,
                              Period_To ,                           
                              Interest_Type ,
                              Interest_Code ,
                              Original_SI ,
                              Coinsurance_In ,
                              Claimable_Amount ,
                              Sum_Insured ,
                              Facultative_In ,
                              Exchange_Rate ,
                              Stock_Maximum_Possible ,
                              Curr_Id ,
                              Ded_Flat_Currency ,
                              Deductible_Code ,
                              Par_No ,
                              Standard_Reference_Id ,
                              EntryUsr ,
                              EntryDt 
                            )
                    VALUES  ( @0 , -- Order_no - char(15)
                              @1 , -- Object_No - numeric
                              @2 , -- Interest_No - numeric
                              @3 , -- Adjustable_Clause - char(1)
                              @4 , -- Description - varchar(256)
                              @5 , -- Insurance_Type - char(6)
                              @6 , -- Ded_Flat_Amount - numeric
                              @7 , -- Period_From - datetime
                              @8 , -- Status - char(1)
                              @9 , -- Period_To - datetime                             
                              @10 , -- Interest_Type - char(1)
                              @11 , -- Interest_Code - char(6)
                              @12 , -- Original_SI - numeric
                              @13 , -- Coinsurance_In - numeric
                              @14 , -- Claimable_Amount - numeric
                              @15 , -- Sum_Insured - numeric
                              @16 , -- Facultative_In - numeric
                              @17 , -- Exchange_Rate - numeric
                              @18 , -- Stock_Maximum_Possible - numeric
                              @19 , -- Curr_Id - char(3)
                              @20 , -- Ded_Flat_Currency - char(3)
                              @21 , -- Deductible_Code - char(6)
                              @22 , -- Par_No - numeric
                              @23 , -- Standard_Reference_Id - int
                              @24 , -- EntryUsr - char(5)
                              GetDate()
                            )";

            using (var db = GetAABDB())
            {
                int result = 0;
                for (int i = 0; i < Intr.Count; i++)
                {
                    result += db.Execute(query, IsNA(Intr[i].Orderno), IsNA(Intr[i].ObjectNo), IsNA(Intr[i].InterestNo), IsNA(Intr[i].AdjustableClause), IsNA(Intr[i].Description), IsNA(Intr[i].InsuranceType), IsNA(Intr[i].DedFlatAmount), IsNA(Intr[i].PeriodFrom), IsNA(Intr[i].Status), IsNA(Intr[i].PeriodTo), IsNA(Intr[i].InterestType), IsNA(Intr[i].InterestCode), IsNA(Intr[i].OriginalSI), IsNA(Intr[i].CoinsuranceIn), IsNA(Intr[i].ClaimableAmount), IsNA(Intr[i].SumInsured), IsNA(Intr[i].FacultativeIn), IsNA(Intr[i].ExchangeRate), IsNA(Intr[i].StockMaximumPossible), IsNA(Intr[i].CurrId), IsNA(Intr[i].DedFlatCurrency), IsNA(Intr[i].DeductibleCode), IsNA(Intr[i].ParNo), IsNA(Intr[i].StandardReferenceId), IsNA(EntryUser));
                }
                return result >= Intr.Count;
            }

        }

        public bool AddInsured(List<InsuredData> Ins, string EntryUser)
        {
            string query = @"INSERT  INTO dbo.Ord_Dtl_Insured
                                ( Order_No ,
                                  Object_No ,
                                  Interest_No ,
                                  Period_No ,
                                  Begin_Date ,
                                  End_Date ,
                                  Sum_Insured ,
                                  Status ,
                                  EntryUsr ,
                                  EntryDt 
                                )
                        VALUES  ( @0 , -- Order_No - char(15)
                                  @1 , -- Object_No - numeric
                                  @2 , -- Interest_No - numeric
                                  @3 , -- Period_No - numeric
                                  @4 , -- Begin_Date - datetime
                                  @5 , -- End_Date - datetime
                                  @6 , -- Sum_Insured - numeric
                                  @7 , -- Status - char(1)
                                  @8 , -- EntryUsr - char(5)
                                  GETDATE()  -- EntryDt - datetime          
                                )";

            using (var db = GetAABDB())
            {
                int result = 0;
                for (int i = 0; i < Ins.Count; i++)
                {
                    result += db.Execute(query, IsNA(Ins[i].OrderNo), IsNA(Ins[i].ObjectNo), IsNA(Ins[i].InterestNo), IsNA(Ins[i].PeriodNo), IsNA(Ins[i].BeginDate), IsNA(Ins[i].EndDate), IsNA(Ins[i].SumInsured), IsNA(Ins[i].Status), IsNA(EntryUser));
                }
                return result >= Ins.Count;
            }
        }

        public bool AddInterestItem(List<InterestItemData> IntItem, string EntryUser)
        {
            string query = @"INSERT  INTO dbo.Ord_Dtl_Interest_Item
                                    ( Order_No ,
                                      Object_No ,
                                      Interest_No ,
                                      Int_Item_No ,
                                      Insurance_Type ,
                                      Int_Item_Code ,
                                      Item_Name ,
                                      Description ,
                                      Capacity ,
                                      Original_SI ,
                                      Sum_Insured ,
                                      Ded_Amount ,
                                      MFG_Year ,
                                      Birthdate ,
                                      ColumnA ,
                                      ColumnB ,
                                      ColumnC ,
                                      ColumnD ,
                                      ColumnE ,
                                      Date1 ,
                                      Date2 ,
                                      Flag1 ,
                                      Flag2 ,
                                      EntryUsr ,
                                      EntryDt 
                                    )
                            VALUES  ( @0 , -- Order_No - char(15)
                                      @1 , -- Object_No - numeric
                                      @2 , -- Interest_No - numeric
                                      @3 , -- Int_Item_No - numeric
                                      @4 , -- Insurance_Type - char(6)
                                      @5 , -- Int_Item_Code - char(4)
                                      @6 , -- Item_Name - varchar(50)
                                      @7 , -- Description - varchar(50)
                                      @8 , -- Capacity - numeric
                                      @9 , -- Original_SI - numeric
                                      @10 , -- Sum_Insured - numeric
                                      @11 , -- Ded_Amount - numeric
                                      @12 , -- MFG_Year - numeric
                                      @13 , -- Birthdate - datetime
                                      @14 , -- ColumnA - numeric
                                      @15 , -- ColumnB - numeric
                                      @16 , -- ColumnC - numeric
                                      @17 , -- ColumnD - numeric
                                      @18 , -- ColumnE - numeric
                                      @19 , -- Date1 - datetime
                                      @20 , -- Date2 - datetime
                                      @21 , -- Flag1 - bit
                                      @22 , -- Flag2 - bit
                                      @23 , -- EntryUsr - char(5)
                                      GETDATE()  -- EntryDt - datetime          
                                    )";
            using (var db = GetAABDB())
            {
                int result = 0;
                for (int i = 0; i < IntItem.Count; i++)
                {
                    result += db.Execute(query, IsNA(IntItem[i].OrderNo), IntItem[i].ObjectNo, IntItem[i].InterestNo, IntItem[i].IntItemNo, IsNA(IntItem[i].InsuranceType), IsNA(IntItem[i].IntItemCode), IsNA(IntItem[i].ItemName), IsNA(IntItem[i].Description), IntItem[i].Capacity, IsNA(IntItem[i].OriginalSI), IsNA(IntItem[i].SumInsured), IsNA(IntItem[i].DedAmount), IntItem[i].MFGYear, IntItem[i].Birthdate != null ? IntItem[i].Birthdate.ToString() : "", IntItem[i].ColumnA, IntItem[i].ColumnB, IntItem[i].ColumnC, IntItem[i].ColumnD, IntItem[i].ColumnE, IntItem[i].Date1 != null ? IntItem[i].Date1.ToString() : "", IntItem[i].Date2 != null ? IntItem[i].Date2.ToString() : "", IsNA(IntItem[i].Flag1), IsNA(IntItem[i].Flag2), IsNA(EntryUser));
                }
                return result == IntItem.Count;
            }
        }

        public bool AddCoverage(List<CoverageData> Cov, string EntryUser)
        {
            string query = @"INSERT  INTO dbo.Ord_Dtl_Coverage
                                ( Order_No ,
                                  Object_No ,
                                  Interest_No ,
                                  Coverage_No ,
                                  Insurance_Type ,
                                  COB_Id ,
                                  Curr_Id ,
                                  Status ,
                                  Interest_Type ,
                                  Coverage_Id ,
                                  Net_Premium ,
                                  Begin_Date ,
                                  End_Date ,                                  
                                  Ndays ,
                                  Net1 ,
                                  Net2 ,
                                  Net3 ,
                                  Gross_Premium ,
                                  Cover_Premium ,
                                  Sum_Insured ,
                                  Loading ,
                                  Calc_Method ,
                                  Ded_Flat_Amount ,
                                  Ded_Flat_Currency ,
                                  Deductible_Code ,
                                  Entry_Pct ,
                                  Rate ,
                                  Discount ,
                                  Max_si ,
                                  Excess_Rate ,
                                  Original_Rate ,
                                  Standard_Reference_Id ,
                                  EntryUsr ,
                                  EntryDt ,          
                                  Acquisition_Cost
                                )
                        VALUES  ( @0 , -- Order_No - char(15)
                                  @1 , -- Object_No - numeric
                                  @2 , -- Interest_No - numeric
                                  @3 , -- Coverage_No - numeric
                                  @4 , -- Insurance_Type - char(6)
                                  @5 , -- COB_Id - char(3)
                                  @6 , -- Curr_Id - char(3)
                                  @7 , -- Status - char(1)
                                  @8 , -- Interest_Type - char(1)
                                  @9 , -- Coverage_Id - char(6)
                                  @10 , -- Net_Premium - numeric
                                  @11 , -- Begin_Date - datetime
                                  @12 , -- End_Date - datetime
                                  
                                  @13 , -- Ndays - numeric
                                  @14 , -- Net1 - numeric
                                  @15 , -- Net2 - numeric
                                  @16 , -- Net3 - numeric
                                  @17 , -- Gross_Premium - numeric
                                  @18 , -- Cover_Premium - numeric
                                  @19 , -- Sum_Insured - numeric
                                  @20 , -- Loading - numeric
                                  @21 , -- Calc_Method - varchar(2)
                                  @22 , -- Ded_Flat_Amount - numeric
                                  @23 , -- Ded_Flat_Currency - char(3)
                                  @24 , -- Deductible_Code - char(6)
                                  @25 , -- Entry_Pct - numeric
                                  @26 , -- Rate - numeric
                                  @27 , -- Discount - numeric
                                  @28 , -- Max_si - numeric
                                  @29 , -- Excess_Rate - numeric
                                  @30 , -- Original_Rate - numeric
                                  @31 , -- Standard_Reference_Id - int
                                  @32 , -- EntryUsr - char(5)
                                  GETDATE() , -- EntryDt - datetime          
                                  @33  -- Acquisition_Cost - numeric
                                )";
            using (var db = GetAABDB())
            {
                int result = 0;
                for (int i = 0; i < Cov.Count; i++)
                {
                    result += db.Execute(query, IsNA(Cov[i].OrderNo), IsNA(Cov[i].ObjectNo), IsNA(Cov[i].InterestNo), IsNA(Cov[i].CoverageNo), IsNA(Cov[i].InsuranceType), IsNA(Cov[i].COBId), IsNA(Cov[i].CurrId), IsNA(Cov[i].Status), IsNA(Cov[i].InterestType), IsNA(Cov[i].CoverageId), IsNA(Cov[i].NetPremium), IsNA(Cov[i].BeginDate), IsNA(Cov[i].EndDate), IsNA(Cov[i].Ndays), IsNA(Cov[i].Net1), IsNA(Cov[i].Net2), IsNA(Cov[i].Net3), IsNA(Cov[i].GrossPremium), IsNA(Cov[i].CoverPremium), IsNA(Cov[i].SumInsured), IsNA(Cov[i].Loading), Cov[i].CalcMethod, IsNA(Cov[i].DedFlatAmount), IsNA(Cov[i].DedFlatCurrency), IsNA(Cov[i].DeductibleCode), IsNA(Cov[i].EntryPct), IsNA(Cov[i].Rate), IsNA(Cov[i].Discount), IsNA(Cov[i].Maxsi), IsNA(Cov[i].ExcessRate),
                        null, //Cov[i].OriginalRate, 
                        IsNA(Cov[i].StandardReferenceId), IsNA(EntryUser), IsNA(Cov[i].AcquisitionCost));
                }
                return result >= Cov.Count;
            }

        }

        public bool AddCoverCommision(List<CoverCommision> CCom, string EntryUser)
        {
            string query = @"INSERT  INTO dbo.Ord_Cover_Commission
                            ( Order_No ,
                                  Object_No ,
                                  Interest_No ,
                                  Disc_Comm_No ,
                                  Coverage_Id ,
                                  Pay_To_Code ,
                                  Apply_Code ,
                                  Commission_Type ,
                                  Commission_Code ,
                                  Level_Disc ,
                                  Coverage_No ,
                                  Percentage ,
                                  Curr_Id ,
                                  Amount ,
                                  Status ,
                                  EntryUsr ,
                                  EntryDt                                  
                                )
                        VALUES  ( @0 , -- Order_No - char(15)
                                  @1 , -- Object_No - numeric
                                  @2 , -- Interest_No - numeric
                                  @3 , -- Disc_Comm_No - numeric
                                  @4 , -- Coverage_Id - char(6)
                                  @5 , -- Pay_To_Code - char(11)
                                  @6 , -- Apply_Code - char(1)
                                  @7 , -- Commission_Type - numeric
                                  @8 , -- Commission_Code - char(6)
                                  @9 , -- Level_Disc - numeric
                                  @10 , -- Coverage_No - numeric
                                  @11 , -- Percentage - numeric
                                  @12 , -- Curr_Id - char(3)
                                  @13 , -- Amount - numeric
                                  @14 , -- Status - char(1)
                                  @15 , -- EntryUsr - char(5)
                                  GetDate() -- EntryDt - datetime                                  
                                )";

            using (var db = GetAABDB())
            {
                int result = 0;
                for (int i = 0; i < CCom.Count; i++)
                {
                    result += db.Execute(query, IsNA(CCom[i].OrderNo), IsNA(CCom[i].ObjectNo), IsNA(CCom[i].InterestNo), IsNA(CCom[i].DiscCommNo), IsNA(CCom[i].CoverageId), IsNA(CCom[i].PayToCode), null, IsNA(CCom[i].CommissionType), IsNA(CCom[i].CommissionCode), IsNA(CCom[i].LevelDisc), IsNA(CCom[i].CoverageNo), IsNA(CCom[i].Percentage), IsNA(CCom[i].CurrId), IsNA(CCom[i].Amount), IsNA(CCom[i].Status), IsNA(EntryUser));
                }
                return result >= CCom.Count;
            }
        }

        public bool AddDetailAddress(List<AddressDetail> Addr, string EntryUser)
        {
            string query = @"IF NOT EXISTS(SELECT * FROM dbo.Ord_Dtl_Address WHERE Order_No = @0 AND Address_Type = @1)
					BEGIN
						INSERT  INTO dbo.Ord_Dtl_Address
                                ( Order_No ,
                                  Address_Type ,
                                  Source_Type ,
                                  Renewal_Notice ,
                                  Delivery_Code ,
                                  Address ,
                                  RT ,
                                  RW ,
                                  Postal_Code ,
                                  Phone ,
                                  Fax ,
                                  EntryUsr ,
                                   EntryDt ,
                                  Handphone ,
                                  ContactPerson	,
                                  UpdateUsr                                         			
                                )
                        VALUES  ( @0 , -- Order_No - char(15)
                                  @1 , -- Address_Type - char(6)
                                  @2 , -- Source_Type - char(2)
                                  @3 , -- Renewal_Notice - bit
                                  @4 , -- Delivery_Code - char(6)
                                  @5 , -- Address - varchar(152)
                                  @6 , -- RT - char(3)
                                  @7 , -- RW - char(3)
                                  @8 , -- Postal_Code - char(8)
                                  @9 , -- Phone - varchar(16)
                                  @10 , -- Fax - varchar(16)
                                  @11 , -- EntryUsr - char(5)
                                  GETDATE() ,  -- EntryDt - datetime          
                                  @12 , -- Handphone - varchar(20)
                                  @13,  -- ContactPerson - varchar(30)				
                                 ''
                                )
					END";
            int result = 0;
            using (var db = GetAABDB())
            {
                for (int i = 0; i < Addr.Count; i++)
                {
                    string ContactPerson = IsNA(Addr[i].ContactPerson);
                    if (ContactPerson.Length > 30) {
                        ContactPerson = ContactPerson.Substring(0, 30);
                    }
                    db.Execute(query, IsNA(Addr[i].OrderNo), IsNA(Addr[i].AddressType), IsNA(Addr[i].SourceType), IsNA(Addr[i].RenewalNotice), IsNA(Addr[i].DeliveryCode), IsNA(Addr[i].Address), IsNA(Addr[i].RT), IsNA(Addr[i].RW), IsNA(Addr[i].PostalCode), IsNA(Addr[i].Phone), IsNA(Addr[i].Fax), EntryUser, IsNA(Addr[i].Handphone), ContactPerson);
                    result++;
                }

            }
            return result >= Addr.Count;
        }

        public bool AddDocuments(List<DocumentData> Doc, string EntryUser)
        {
            string query = @"INSERT  INTO dbo.Ord_Dtl_DOC
                                ( Order_No ,
                                  Document_No ,
                                  Document_Id ,
                                  Description ,
                                  Origin ,
                                  PLY ,
                                  Received_Date ,
                                  Expired_Date ,
                                  EntryUsr ,
                                  EntryDt           						
                                )
                        VALUES  ( @0 , -- Order_No - char(15)
                                  @1 , -- Document_No - numeric
                                  @2 , -- Document_Id - char(6)
                                  @3 , -- Description - varchar(50)
                                  @4 , -- Origin - bit
                                  @5 , -- PLY - numeric
                                  @6 , -- Received_Date - datetime
                                  @7 , -- Expired_Date - datetime
                                  @8 , -- EntryUsr - char(5)
                                  GETDATE()  -- EntryDt - datetime          						
                                )";
            int result = 0;
            using (var db = GetAABDB())
            {
                for (int i = 0; i < Doc.Count; i++)
                {
                    result += db.Execute(query, Doc[i].OrderNo, Doc[i].DocumentNo, Doc[i].DocumentId, Doc[i].Description, Doc[i].Origin, Doc[i].PLY, Doc[i].ReceivedDate, Doc[i].ExpiredDate, EntryUser);
                }
            }
            return result >= Doc.Count;
        }

        public bool AddInstallmentInfo(List<InstallmentInfo> Instl, string Actor)
        {
            string query = @"INSERT  INTO dbo.Ord_Install_Info
                            ( Order_No ,
                              Install_No ,
                              Card_No ,
                              Card_Name ,                              
                              CVC ,
                              Bank_Name ,
                              Fee_Currency ,
                              Due_Date ,
                              Install_Pct ,
                              Interest_Pct ,
                              Comm_Pct ,
                              Stamp_Duty ,
                              Policy_Fee ,
                              Payment_Mode ,
                              EntryUsr ,
                              EntryDt 								
                            )
                    VALUES  ( @0 , -- Order_No - char(15)
                              @1 , -- Install_No - numeric
                              @2 , -- Card_No - varchar(20)
                              @3 , -- Card_Name - varchar(30)                              
                              @4 , -- CVC - char(4)
                              @5 , -- Bank_Name - varchar(50)
                              @6 , -- Fee_Currency - char(3)
                              @7 , -- Due_Date - datetime
                              @8 , -- Install_Pct - numeric
                              @9 , -- Interest_Pct - numeric
                              @10 , -- Comm_Pct - numeric
                              @11 , -- Stamp_Duty - numeric
                              @12 , -- Policy_Fee - numeric
                              @13 , -- Payment_Mode - char(6)
                              @14 , -- EntryUsr - char(5)
                              GETDATE()  -- EntryDt - datetime         
                            )";
            int result = 0;
            string PaymentMode = System.Configuration.ConfigurationManager.AppSettings["DefaultPaymentMode"].ToString();
            using (var db = GetAABDB())
            {
                for (int i = 0; i < Instl.Count; i++)
                {
                    //result += db.Execute(query, Instl[i].OrderNo, Instl[i].InstallNo, Instl[i].CardNo, Instl[i].CardName, Instl[i].CVC, Instl[i].BankName, Instl[i].FeeCurrency, Instl[i].DueDate, Instl[i].InstallPct, Instl[i].InterestPct, Instl[i].CommPct, Instl[i].StampDuty, Instl[i].PolicyFee, Instl[i].PaymentMode, Actor);
                    result += db.Execute(query, IsNA(Instl[i].OrderNo), IsNA(Instl[i].InstallNo), IsNA(Instl[i].CardNo), IsNA(Instl[i].CardName), IsNA(Instl[i].CVC), IsNA(Instl[i].BankName), IsNA(Instl[i].FeeCurrency), IsNA(Instl[i].DueDate), IsNA(Instl[i].InstallPct), IsNA(Instl[i].InterestPct), IsNA(Instl[i].CommPct), IsNA(Instl[i].StampDuty), IsNA(Instl[i].PolicyFee), IsNA(Instl[i].PaymentMode), Actor);
                }
            }
            return result >= Instl.Count;
        }
        public void AddComSalesTrx(string OrderNo)
        {
            string query = @"Exec usp_comsales_ord_trx @0";

            using (var db = GetAABDB())
            {
                db.Execute(query, OrderNo);
            }
        }

        public bool AddPolicyClause(List<PolicyClause> PClause, string EntryUser)
        {
            string query = @"IF NOT EXISTS (SELECT * FROM dbo.Ord_Policy_Clause WHERE Order_No = @0 AND Clause_Id = @1)
                            BEGIN
                            INSERT  INTO dbo.Ord_Policy_Clause
                                    ( Order_No ,
                                        Clause_Id ,
                                        Description ,
                                        EntryUsr ,
                                        EntryDt
                                    )
                            VALUES  ( @0 , -- Order_No - char(15)
                                        @1 , -- Clause_Id - char(6)
                                        @2 , -- Description - varchar(512)
                                        @3 , -- EntryUsr - char(5)
                                        GETDATE() -- EntryDt - datetime          
                                    )
                            END";
            int result = 0;
            using (var db = GetAABDB())
            {
                try
                {
                    for (int i = 0; i < PClause.Count; i++)
                    {
                        db.Execute(query, PClause[i].OrderNo, PClause[i].ClauseId, IsNA(PClause[i].Description), EntryUser);
                        result = result + 1;
                    }
                }
                catch (Exception e )
                {
                    result = result - 1;
                }
            }
            return result == PClause.Count;
        }

        public bool AddObjectClause(List<ObjectClause> ObjCl, string EntryUser)
        {
            string query = @"INSERT  INTO dbo.Ord_Object_Clause
                                ( Order_No ,
                                  Object_No ,
                                  Clause_No ,
                                  Clause_Id ,
                                  Description ,
                                  EntryUsr ,
                                  EntryDt ,
                                  UpdateUsr                                         
                                )
                        VALUES  ( @0 , -- Order_No - char(15)
                                  @1 , -- Object_No - numeric
                                  @2 , -- Clause_No - numeric
                                  @3 , -- Clause_Id - char(6)
                                  @4 , -- Description - varchar(512)
                                  @5 , -- EntryUsr - char(5)
                                  GETDATE() , -- EntryDt - datetime          
                                  ''
                                )";
            int result = 0;
            using (var db = GetAABDB())
            {
                for (int i = 0; i < ObjCl.Count; i++)
                {
                    result += db.Execute(query, ObjCl[i].OrderNo, ObjCl[i].ObjectNo, ObjCl[i].ClauseNo, ObjCl[i].ClauseId, ObjCl[i].Description, EntryUser);
                }
            }
            return result == ObjCl.Count;
        }
        public bool AddObjectClauseAfterSurvey(List<ObjectClause> ObjCl, string EntryUser)
        {
            string query = @"DECLARE @@Count INT = 0
SELECT @@Count=COUNT(Object_No) FROM dbo.Ord_Object_Clause WHERE Order_No = @0 AND Description = @4
IF(@@Count=0)
BEGIN
	INSERT  INTO dbo.Ord_Object_Clause
			( Order_No ,
				Object_No ,
				Clause_No ,
				Clause_Id ,
				Description ,
				EntryUsr ,
				EntryDt ,
				UpdateUsr                                         
			)
	VALUES  ( @0 , -- Order_No - char(15)
				@1 , -- Object_No - numeric
				@2 , -- Clause_No - numeric
				@3 , -- Clause_Id - char(6)
				@4 , -- Description - varchar(512)
				@5 , -- EntryUsr - char(5)
				GETDATE() , -- EntryDt - datetime          
				''
			)
END";
            int result = 0;
            using (var db = GetAABDB())
            {
                int oldClauseNo = 0;
                if (ObjCl.Count > 0) {
                    oldClauseNo = db.ExecuteScalar<int>(@"SELECT COUNT(*) FROM dbo.Ord_Object_Clause WHERE Order_No = @0", ObjCl[0].OrderNo);
                }
                for (int i = 0; i < ObjCl.Count; i++)
                {
                    result += db.Execute(query, ObjCl[i].OrderNo, ObjCl[i].ObjectNo, ObjCl[i].ClauseNo+oldClauseNo, ObjCl[i].ClauseId, ObjCl[i].Description, EntryUser);
                }
            }
            return result == ObjCl.Count;
        }
        public bool SaveTransactionAccount(List<TransactionAccount> TransAcc, string EntryUser)
        {
            string query = @"; DECLARE @@Count AS INT = 0
                            SELECT  @@Count = COUNT(*)
                            FROM    Dtl_Transaction_Account
                            WHERE   Reference_No LIKE @0
                            DECLARE @@RefNo AS VARCHAR(100)= Concat(@0,'-',@@Count+1)
                            IF NOT EXISTS (Select 1 From dbo.Dtl_Transaction_Account Where Reference_No=@@RefNo AND Reference_Type=@1)
                            Begin
                            INSERT INTO dbo.Dtl_Transaction_Account
			                        ( Reference_No ,
			                          Reference_Type ,
			                          Account_No ,
			                          Account_Type ,
			                          Account_Name ,
			                          Valid_Thru ,
			                          CVC ,
			                          Bank_Name ,
			                          EntryUsr ,
			                          EntryDt 
			                        )
			                VALUES  ( @@RefNo , -- Reference_No - varchar(30)
			                          @1 , -- Reference_Type - char(3)
			                          @2 , -- Account_No - varchar(30)
			                          @3 , -- Account_Type - char(3)
			                          @4 , -- Account_Name - varchar(30)
			                          @5 , -- Valid_Thru - char(10)
			                          @6 , -- CVC - char(4)
			                          @7 , -- Bank_Name - varchar(50)
			                          @8 , -- EntryUsr - char(5)
			                          GETDATE() -- EntryDt - datetime			          
			                        )
                                    END";
            int result = 0;
            using (var db = GetAABDB())
            {
                for (int i = 0; i < TransAcc.Count; i++)
                {
                    result += db.Execute(query, IsNA(TransAcc[i].ReferenceNo), IsNA(TransAcc[i].ReferenceType), IsNA(TransAcc[i].AccountNo), IsNA(TransAcc[i].AccountType), IsNA(TransAcc[i].AccountName), IsNA(TransAcc[i].ValidThru), IsNA(TransAcc[i].CVC), IsNA(TransAcc[i].BankName), EntryUser);
                }
            }
            return result == TransAcc.Count;
        }

        public bool SurveySaveDetail(List<SurveyInfo> S, string EntryUser)
        {
            int result = 0;
            string query = @"INSERT  INTO dbo.Survey
                                ( Survey_No ,
                                  Object_No ,
                                  External_Surveyor ,
                                  EXT_SURVEYOR_ID ,
                                  Survey_Type ,
                                  Surveyor ,
                                  Survey_Date ,
                                  
                                  Pic_Name ,
                                  Payment_Mode ,
                                  Curr_Id ,
                                  Amount ,
                                  Phone_No ,
                                  Location ,
                                  RT ,
                                  RW ,
                                  Postal_Code ,
                                  Description ,
                                  Form_Id ,
                                  Status ,
                                  Notes ,
                                  Reference_No ,
                                  Area_Code ,
                                  Old_Area_Code ,
                                  Survey_Mode ,
                                  Region_Code ,
                                  Best_Time_Call ,
                                  Reject_Reason ,
                                  Result ,
                                  Policy_No ,
                                  Registration_No ,
                                  Duration ,
                                  Travel_Period ,
                                  OnCall ,
                                  Survey_Reference ,
                                  SubArea_Code ,
                                  Adjuster_Code ,
                                  SubCon_Code ,
                                  Payer_Code ,
                                  Payer_Flag ,
                                  Survey_Result ,
                                  Image_Count ,
                                  EntryUsr ,
                                  EntryDt ,                                  
                                  new_survey_type ,
                                  new_sub_survey_type ,
                                  remarks ,
                                  non_schedule ,
                                  Sub_Geo_Area_Code ,
                                  AddressFromSurvey ,
                                  City ,
                                  SurveyObject ,
                                  
                                  StickerDescription ,
                                  ResultRecommendation,
                                  From_Date,
                                  To_Date
		                        )
                        VALUES  ( @0 , -- Survey_No - char(8)
                                  @1 , -- Object_No - numeric
                                  @2 , -- External_Surveyor - bit
                                  @3 , -- EXT_SURVEYOR_ID - char(11)
                                  @4 , -- Survey_Type - char(6)
                                  @5 , -- Surveyor - char(5)
                                  @6 , -- Survey_Date - datetime                                  
                                  @7 , -- Pic_Name - varchar(100)
                                  @8 , -- Payment_Mode - char(6)
                                  @9 , -- Curr_Id - char(3)
                                  @10 , -- Amount - numeric
                                  @11 , -- Phone_No - varchar(16)
                                  @12 , -- Location - varchar(152)
                                  @13 , -- RT - char(3)
                                  @14 , -- RW - char(3)
                                  @15 , -- Postal_Code - char(8)
                                  @16 , -- Description - varchar(50)
                                  @17 , -- Form_Id - char(6)
                                  @18 , -- Status - char(1)
                                  @19 , -- Notes - text
                                  @20 , -- Reference_No - char(30)
                                  @21 , -- Area_Code - char(6)
                                  @22 , -- Old_Area_Code - char(6)
                                  @23 , -- Survey_Mode - char(1)
                                  @24 , -- Region_Code - char(6)
                                  @25 , -- Best_Time_Call - varchar(128)
                                  @26 , -- Reject_Reason - varchar(128)
                                  @27 , -- Result - text
                                  @28 , -- Policy_No - char(16)
                                  @29 , -- Registration_No - char(16)
                                  @30 , -- Duration - smallint
                                  @31 , -- Travel_Period - smallint
                                  @32 , -- OnCall - char(1)
                                  @33 , -- Survey_Reference - char(20)
                                  @34 , -- SubArea_Code - char(6)
                                  @35 , -- Adjuster_Code - char(11)
                                  @36 , -- SubCon_Code - char(11)
                                  @37 , -- Payer_Code - char(11)
                                  @38 , -- Payer_Flag - char(1)
                                  @39 , -- Survey_Result - char(1)
                                  @40 , -- Image_Count - int
                                  @41 , -- EntryUsr - char(5)
                                  GETDATE() , -- EntryDt - datetime                  
                                  @42 , -- new_survey_type - varchar(6)
                                  @43 , -- new_sub_survey_type - varchar(6)
                                  @44 , -- remarks - varchar(256)
                                  @45 , -- non_schedule - varchar(1)
                                  @46 , -- Sub_Geo_Area_Code - varchar(10)
                                  @47 , -- AddressFromSurvey - varchar(10)
                                  @48 , -- City - varchar(50)
                                  @49 , -- SurveyObject - int
                                  
                                  @50 , -- StickerDescription - varchar(500)
                                  @51 , -- ResultRecommendation - varchar(max)
                                  @52 , --From Date
                                  @53
		                        )";
            string qUpdateAdditionalOrderSurveyInfo = @"Update dbo.AdditionalOrderAcquisitionSurvey 
	                                                        Set SurveyNo=@0, SurveyCreated=1 
	                                                        Where ID=@1";
            using (var db = GetAABDB())
            {
                for (int i = 0; i < S.Count; i++)
                {
                    result += db.Execute(query, IsNA(S[i].SurveyNo), IsNA(S[i].ObjectNo), IsNA(S[i].ExternalSurveyor), IsNA(S[i].EXTSURVEYORID), IsNA(S[i].SurveyType), IsNA(S[i].Surveyor), S[i].SurveyDate, IsNA(S[i].PicName), IsNA(S[i].PaymentMode), IsNA(S[i].CurrId), IsNA(S[i].Amount), IsNA(S[i].PhoneNo), IsNA(S[i].Location), IsNA(S[i].RT), IsNA(S[i].RW), IsNA(S[i].PostalCode), IsNA(S[i].Description), IsNA(S[i].FormId), IsNA(S[i].Status), IsNA(S[i].Notes), IsNA(S[i].ReferenceNo), IsNA(S[i].AreaCode), IsNA(S[i].OldAreaCode), IsNA(S[i].SurveyMode), IsNA(S[i].RegionCode), IsNA(S[i].BestTimeCall), IsNA(S[i].RejectReason), IsNA(S[i].Result), IsNA(S[i].PolicyNo), IsNA(S[i].RegistrationNo), IsNA(S[i].Duration), IsNA(S[i].TravelPeriod), IsNA(S[i].OnCall), IsNA(S[i].SurveyReference), IsNA(S[i].SubAreaCode), IsNA(S[i].AdjusterCode), IsNA(S[i].SubConCode), IsNA(S[i].PayerCode), IsNA(S[i].PayerFlag), IsNA(S[i].SurveyResult), IsNA(S[i].ImageCount), EntryUser, IsNA(S[i].newsurveytype), IsNA(S[i].newsubsurveytype), IsNA(S[i].remarks), IsNA(S[i].nonschedule), IsNA(S[i].SubGeoAreaCode), IsNA(S[i].AddressFromSurvey), IsNA(S[i].City), IsNA(S[i].SurveyObject), IsNA(S[i].StickerDescription), IsNA(S[i].ResultRecommendation), S[i].FromDate, S[i].ToDate);
                    /*if (!string.IsNullOrWhiteSpace(S[i].AdditionalOrderAcquisitionSurveyID)) {
                        using (a2isDBHelper.Database svdb = new a2isDBHelper.Database("a2isMobileSurveyDB"))
                        {
                            svdb.Execute(qUpdateAdditionalOrderSurveyInfo, IsNA(S[i].SurveyNo), S[i].AdditionalOrderAcquisitionSurveyID);
                        }
                    }*/
                }
            }
            return result >= S.Count;
        }
        
        public bool InsertLnkSurveyOrder(string ReferenceNo, string ReferenceType, string SurveyNo, string EntryUser)
        {
            int result = 0;
            string query = @"INSERT INTO dbo.Lnk_Survey_Order
                                    ( Reference_No ,
                                      Reference_Type ,
                                      Type_Number ,
                                      Survey_No ,
                                      EntryUsr ,
                                      EntryDt 
                                    )
                            VALUES  ( @0 , -- Reference_No - char(16)
                                      @1 , -- Reference_Type - char(6)
                                      0 , -- Type_Number - numeric
                                      @2 , -- Survey_No - char(8)
                                      @3 , -- EntryUsr - char(5)
                                      GETDATE()
                                    )";
            using (var db = GetAABDB())
            {
                result += db.Execute(query, IsNA(ReferenceNo), IsNA(ReferenceType), IsNA(SurveyNo), IsNA(EntryUser));
            }
            return result >= 0;
        }

        public string CheckVA(string OrderNo)
        {
            string query = @"SELECT  Top 1 VirtualAccountNo
                            FROM    Finance.AAB2000VirtualAccount
                            WHERE   OrderNo = @0 AND RowStatus = 0 ";
            using (var db = Geta2isBeyondDB())
            {
                var result = db.Query<string>(query, OrderNo).SingleOrDefault();
                return result;
            }
        }

        public void AddVA(string OrderNo, string EntryUser)
        {
            string query = @";exec usp_GET_VATransaction @0";

            string qInsertAABVA = @";DECLARE @@SuccessStatus AS BIT
                                    EXEC [Finance].[usp_UpsertAAB2000VirtualAccount] 
                                    @@PolicyNo = @0, 
                                    @@VirtualAccountNo = @1, 
                                    @@CurrencyCode = @2,
                                    @@PremiumAmount = @3,
                                    @@PolicyStatusCode = @4,
                                    @@InsuredCode = @5,
                                    @@InsuredName = @6,
                                    @@MarketingCode = @7,
                                    @@SalesmanAdminLogID = @8,
                                    @@InternalDistributionCode = @9,
                                    @@PolicyPeriodFrom = @10,
                                    @@PolicyPeriodTo = @11,
                                    @@GracePeriod = @12, 
                                    @@PaymentDueDate = @13,
                                    @@UserName = @14,
                                    @@PolicyNoRenewalBeyond =@15,
                                    @@OrderNo = @16,
                                    @@ChasisNo = @17,
                                    @@EngineNo = @18,
                                    @@PolicyStatus = @19,
                                    @@CustomerName = @20,
                                    @@PoliceNo = @21,
                                    @@SuccessStatus = @@SuccessStatus OUTPUT
                                    SELECT @@SuccessStatus as UploadStatus ";
            //string qInsertAABVA = @"INSERT  INTO Finance.AAB2000VirtualAccount
            //                        ( PolicyNo ,
            //                          VirtualAccountNo ,
            //                          CurrencyCode ,
            //                          PremiumAmount ,
            //                          PolicyStatusCode ,
            //                          InsuredCode ,
            //                          InsuredName ,
            //                          MarketingCode ,
            //                          SalesmanAdminLogID ,
            //                          InternalDistributionCode ,
            //                          PolicyPeriodFrom ,
            //                          PolicyPeriodTo ,
            //                          GracePeriod ,
            //                          PaymentDueDate ,
            //                          PolicyNoRenewalBeyond ,
            //                          OrderNo ,
            //     ChasisNo ,
            //     EngineNo ,
            //     PolicyStatus ,
            //     PoliceNo ,
            //     CustomerName,
            //                          CreatedBy ,
            //                          CreatedDate
            //                        )
            //                VALUES  ( @0 ,
            //                          @1 ,
            //                          @2 ,
            //                          @3 ,
            //                          @4 ,
            //                          @5 ,
            //                          @6 ,
            //                          @7 ,
            //                          @8 ,
            //                          @9 ,
            //                          @10 ,
            //                          @11 ,
            //                          @12 ,
            //                          @13 ,
            //                          @14 , --Revision #1
            //                          @15 ,
            //                         @16 ,
            //                         @17 ,
            //                         @18 ,
            //                         @19 ,
            //                         @20,
            //                          @21 ,
            //                          GETDATE()
            //                        )";

            string qUpsertVALog = @";EXEC [usp_InsertAAB2000VirtualAccount_Log] @@PolicyNo = @0,
                          @@VirtualAccountNo = @1,  
                          @@CurrencyCode = @2, 
                          @@PremiumAmount =  @3, 
                          @@PolicyStatusCode = @4, 
                          @@InsuredCode = @5, 
                          @@InsuredName = @6, 
                          @@MarketingCode = @7, 
                          @@SalesmanAdminLogID = @8, 
                          @@InternalDistributionCode = @9, 
                          @@PolicyPeriodFrom = @10, 
                          @@PolicyPeriodTo = @11, 
                          @@GracePeriod =  @12,  
                          @@PaymentDueDate = @13,                           
                          @@PolicyNoRenewalBeyond =@14, 
                          @@OrderNo = @15, 
                          @@ChasisNo = @16, 
                          @@EngineNo = @17, 
                          @@PolicyStatus = @18, 
                          @@CustomerName = @19, 
                          @@PoliceNo = @20";
            string qUpdateOrderVA = @"UPDATE  Mst_Order
                                        SET VANumber = @0
                                        WHERE Order_No = @1";

            int result = 0;
            VATRansaction VAInfo = null;


            using (var db = GetAABDB())
            {
                VAInfo = db.Query<VATRansaction>(query, OrderNo).FirstOrDefault();
            }
            if (VAInfo != null)
            {
                VAInfo.SalesmanAdminLogID = EntryUser;
                UpdateVaNo(OrderNo, VAInfo.VirtualAccountNo);
                //UpdateVaNoOrderSimulation(OrderNo, VAInfo.VirtualAccountNo);

                using (var db = Geta2isBeyondDB())
                {
                    //result = db.Execute(qInsertAABVA, IsNA(VAInfo.PolicyNo), IsNA(VAInfo.VirtualAccountNo), IsNA(VAInfo.CurrencyCode), IsNA(VAInfo.PremiumAmount), IsNA(VAInfo.PolicyStatusCode), IsNA(VAInfo.InsuredCode), IsNA(VAInfo.InsuredName), IsNA(VAInfo.MarketingCode), IsNA(VAInfo.SalesmanAdminLogID), IsNA(VAInfo.InternalDistributionCode), IsNA(VAInfo.PolicyPeriodFrom), IsNA(VAInfo.PolicyPeriodTo), IsNA(VAInfo.GracePeriod), IsNA(VAInfo.PaymentDueDate), IsNA(VAInfo.PolicyNoRenewalBeyond), IsNA(VAInfo.OrderNo), IsNA(VAInfo.ChasisNo), IsNA(VAInfo.EngineNo), IsNA(VAInfo.PolicyStatus), IsNA(VAInfo.PoliceNo), IsNA(VAInfo.CustomerName), EntryUser);
                    result = db.Query<int>(qInsertAABVA,
                        CommonLogic.IsNA(VAInfo.PolicyNo),
                        CommonLogic.IsNA(VAInfo.VirtualAccountNo),
                        CommonLogic.IsNA(VAInfo.CurrencyCode),
                        CommonLogic.IsNA(VAInfo.PremiumAmount),
                        CommonLogic.IsNA(VAInfo.PolicyStatusCode),
                        CommonLogic.IsNA(VAInfo.InsuredCode),
                        CommonLogic.IsNA(VAInfo.InsuredName),
                        CommonLogic.IsNA(VAInfo.MarketingCode),
                        CommonLogic.IsNA(VAInfo.SalesmanAdminLogID),
                        CommonLogic.IsNA(VAInfo.InternalDistributionCode),
                        CommonLogic.IsNA(VAInfo.PolicyPeriodFrom),
                        CommonLogic.IsNA(VAInfo.PolicyPeriodTo),
                        CommonLogic.IsNA(VAInfo.GracePeriod),
                        CommonLogic.IsNA(VAInfo.PaymentDueDate),
                        EntryUser,
                        CommonLogic.IsNA(VAInfo.PolicyNoRenewalBeyond),
                        CommonLogic.IsNA(VAInfo.OrderNo),
                        CommonLogic.IsNA(VAInfo.ChasisNo),
                        CommonLogic.IsNA(VAInfo.EngineNo),
                        CommonLogic.IsNA(VAInfo.PolicyStatus),
                        CommonLogic.IsNA(VAInfo.CustomerName),
                        CommonLogic.IsNA(VAInfo.PoliceNo)).SingleOrDefault();
                }
                if (result == 0)
                {
                    using (var db = GetAABDB())
                    {
                        db.Query<dynamic>(qUpsertVALog, IsNA(VAInfo.PolicyNo), IsNA(VAInfo.VirtualAccountNo), IsNA(VAInfo.CurrencyCode), IsNA(VAInfo.PremiumAmount), IsNA(VAInfo.PolicyStatusCode), IsNA(VAInfo.InsuredCode), IsNA(VAInfo.InsuredName), IsNA(VAInfo.MarketingCode), IsNA(VAInfo.SalesmanAdminLogID), IsNA(VAInfo.InternalDistributionCode), IsNA(VAInfo.PolicyPeriodFrom), IsNA(VAInfo.PolicyPeriodTo), IsNA(VAInfo.GracePeriod), IsNA(VAInfo.PaymentDueDate), IsNA(VAInfo.PolicyNoRenewalBeyond), IsNA(VAInfo.OrderNo), IsNA(VAInfo.ChasisNo), IsNA(VAInfo.EngineNo), IsNA(VAInfo.PolicyStatus), IsNA(VAInfo.CustomerName), IsNA(VAInfo.PoliceNo)).FirstOrDefault();
                    }
                }
                using (var db = GetAABDB())
                {
                    db.Execute(qUpdateOrderVA, IsNA(VAInfo.VirtualAccountNo), OrderNo);
                   // string qOrdVATRansaction = @"INSERT INTO dbo.Ord_VA_Transaction
			                //        ( OrderNo, PolicyNo, VANo, UploadStatus, EntryUsr, EntryDt)
			                //VALUES  ( @0, @1, @2, @3, @4, GETDATE())";
                   // db.Execute(qOrdVATRansaction, IsNA(OrderNo), IsNA(VAInfo.PolicyNo), IsNA(VAInfo.VirtualAccountNo), IsNA(IsNA(result)), IsNA(VAInfo.MarketingCode));
                }

                if (!string.IsNullOrEmpty(VAInfo.VirtualAccountNo) && !string.IsNullOrWhiteSpace(VAInfo.VirtualAccountNo)) {
                    var byndDB = Geta2isBeyondDB();
                    var aabDB = GetAABDB();
                    string qGetAmount = @";SELECT COALESCE(PremiumAmount, 0) Amount 
                                    FROM Finance.AAB2000VirtualAccount 
                                    WHERE VirtualAccountNo = @0";
                    decimal amount = byndDB.ExecuteScalar<decimal>(qGetAmount, VAInfo.VirtualAccountNo);
                    if (amount == 0) {
                        qGetAmount = @";DECLARE @@PolicyFee AS NUMERIC(20, 4)
                                    DECLARE @@PolicyID AS VARCHAR(12)
                                    DECLARE @@EndorsementNo AS INT
                                    SELECT @@PolicyFee =ISNULL(Policy_Fee+Stamp_Duty,0),@@PolicyID=Policy_Id,@@EndorsementNo =Endorsement_No 
                                    FROM dbo.Mst_Order where Order_No = @0
                                    SELECT  ISNULL(SUM(net_premium), 0)
                                            + @@PolicyFee
                                            + ( SELECT  ISNULL(SUM(Net_Premium
                                                                    + Policy_Fee
                                                                    + Stamp_Duty), 0)
                                                FROM    dbo.Premium AS p ( NOLOCK )
                                                WHERE   policy_id = @@PolicyID
                                                        AND Endorsement_No <> @@EndorsementNo
                                                ) PremiumAmount
                                    FROM    dbo.Ord_Dtl_Coverage AS odc ( NOLOCK )
                                    WHERE   Order_No = @0";
                        decimal newAmount = aabDB.ExecuteScalar<dynamic>(qGetAmount, OrderNo);
                        string qUpdate = @"UPDATE Finance.AAB2000VirtualAccount 
                                        SET PremiumAmount = @1
                                        WHERE VirtualAccountNo = @0";
                        byndDB.Execute(qUpdate, VAInfo.VirtualAccountNo, newAmount);
                    }
                }
            }
            #region qInsertUpdateVAno
            string qInsertUpdateVAno = @";IF EXISTS(SELECT * FROM Finance.VirtualAccountNumber WHERE VirtualAccountNumber = @0 AND RowStatus = 0)
BEGIN 
	UPDATE Finance.VirtualAccountNumber SET PolicyType = @1, PolicyNumber = @2, PolicyPeriodFrom = @3,
	PolicyPeriodTo = @4, AccountManager = @5, AccountManagerName = @6, SAAdmin = @7, SAAdminName = @8,
	PICMobileNumber = @9, CustomerName = @10, IDType = @11, IDNumber = @12, Remarks = @13,
	IsNew = @14, ModifiedBy = @15, ModifiedDate = GETDATE() WHERE VirtualAccountNumber = @0
	AND RowStatus = 0
END";
            #endregion

            #region PreBookVA 0232URF2019
            PreBookVAModel VA = null;
            using (var db = GetAABMobileDB())
            {
                List<dynamic> lsOrd = db.Fetch<dynamic>("SELECT COALESCE(OrderNo,'') OrderNo FROM dbo.OrderSimulation WHERE PolicyOrderNo = @0 AND RowStatus = 1 AND ApplyF = 1", OrderNo);
                if (lsOrd.Count > 0)
                {
                    string transOrderNo = lsOrd.First().OrderNo;
                    VA = db.Query<PreBookVAModel>(@";EXEC usp_GetPreBookVATransactionOtosales @0", transOrderNo).FirstOrDefault();
                }
            }
            if (VA != null)
            {
                using (var db = Geta2isBeyondDB())
                {
                    if (VA.PolicyPeriodFrom != null && VA.PolicyPeriodTo != null)
                    {
                        db.Execute(qInsertUpdateVAno
                            , VA.VirtualAccountNumber
                            , VA.PolicyType
                            , VA.PolicyNumber
                            , VA.PolicyPeriodFrom
                            , VA.PolicyPeriodTo
                            , VA.AccountManager
                            , VA.AccountManagerName
                            , VA.SAAdmin
                            , VA.SAAdminName
                            , VA.PICMobileNumber
                            , VA.CustomerName
                            , VA.IDType
                            , VA.IDNumber
                            , VA.Remarks
                            , VA.IsNew
                            , VA.CreatedBy);
                    }
                }
            }
            #endregion
        }

        public void AddOrderVANo(string OrderNo, string PreOrderedVANo, string UserID)
        {
            string query = @"EXEC dbo.usp_InsertOrderVANumber @@OrderNo = @0, -- char(15)
                            @@VANumber = @1, -- varchar(30)
                            @@UserId = @2 -- char(5)";

            using (var db = GetAABDB())
            {
                db.Execute(query, OrderNo, PreOrderedVANo, UserID);
            }
        }

        public void AddPreOrderedVANo(string OrderNo, string VANumber, string UserID)
        {
            string UserName = "AABVA";
            string query = @";EXEC dbo.usp_GetVAData @@OrderNo = @0, @@VANumber=@1 -- char(15)";

            //Sprint 11: Update insert by SP
            //string qInsertAABVA = @"INSERT  INTO Finance.AAB2000VirtualAccount
            //                        ( PolicyNo ,
            //                          VirtualAccountNo ,
            //                          CurrencyCode ,
            //                          PremiumAmount ,
            //                          PolicyStatusCode ,
            //                          InsuredCode ,
            //                          InsuredName ,
            //                          MarketingCode ,
            //                          SalesmanAdminLogID ,
            //                          InternalDistributionCode ,
            //                          PolicyPeriodFrom ,
            //                          PolicyPeriodTo ,
            //                          GracePeriod ,
            //                          PaymentDueDate ,
            //                          PolicyNoRenewalBeyond ,
            //                          OrderNo ,
            //     ChasisNo ,
            //     EngineNo ,
            //     PolicyStatus ,
            //     PoliceNo ,
            //     CustomerName,
            //                          CreatedBy ,
            //                          CreatedDate
            //                        )
            //                VALUES  ( @0 ,
            //                          @1 ,
            //                          @2 ,
            //                          @3 ,
            //                          @4 ,
            //                          @5 ,
            //                          @6 ,
            //                          @7 ,
            //                          @8 ,
            //                          @9 ,
            //                          @10 ,
            //                          @11 ,
            //                          @12 ,
            //                          @13 ,
            //                          @14 , --Revision #1
            //                          @15 ,
            //     @16 ,
            //     @17 ,
            //     @18 ,
            //     @19 ,
            //     @20,
            //                          @21 ,
            //                          GETDATE()
            //                        )";
            string qInsertAABVA = @";DECLARE @@SuccessStatus AS BIT
                                    EXEC [Finance].[usp_UpsertAAB2000VirtualAccount] 
                                    @@PolicyNo = @0, 
                                    @@VirtualAccountNo = @1, 
                                    @@CurrencyCode = @2,
                                    @@PremiumAmount = @3,
                                    @@PolicyStatusCode = @4,
                                    @@InsuredCode = @5,
                                    @@InsuredName = @6,
                                    @@MarketingCode = @7,
                                    @@SalesmanAdminLogID = @8,
                                    @@InternalDistributionCode = @9,
                                    @@PolicyPeriodFrom = @10,
                                    @@PolicyPeriodTo = @11,
                                    @@GracePeriod = @12, 
                                    @@PaymentDueDate = @13,
                                    @@UserName = @14,
                                    @@PolicyNoRenewalBeyond =@15,
                                    @@OrderNo = @16,
                                    @@ChasisNo = @17,
                                    @@EngineNo = @18,
                                    @@PolicyStatus = @19,
                                    @@CustomerName = @20,
                                    @@PoliceNo = @21,
                                    @@SuccessStatus = @@SuccessStatus OUTPUT
                                    SELECT @@SuccessStatus as UploadStatus ";

            string qUpsertVALog = @";EXEC [usp_InsertAAB2000VirtualAccount_Log] @@PolicyNo = @0,
                          @@VirtualAccountNo = @1,  
                          @@CurrencyCode = @2, 
                          @@PremiumAmount =  @3, 
                          @@PolicyStatusCode = @4, 
                          @@InsuredCode = @5, 
                          @@InsuredName = @6, 
                          @@MarketingCode = @7, 
                          @@SalesmanAdminLogID = @8, 
                          @@InternalDistributionCode = @9, 
                          @@PolicyPeriodFrom = @10, 
                          @@PolicyPeriodTo = @11, 
                          @@GracePeriod =  @12,  
                          @@PaymentDueDate = @13,                           
                          @@PolicyNoRenewalBeyond =@14, 
                          @@OrderNo = @15, 
                          @@ChasisNo = @16, 
                          @@EngineNo = @17, 
                          @@PolicyStatus = @18, 
                          @@CustomerName = @19, 
                          @@PoliceNo = @20";
            string queryUpdate = @"UPDATE  dbo.Mst_Order
                                    SET VANumber = @1
                                    WHERE Order_No = @0";
            string strSQLBeyond = @"UPDATE  Finance.VirtualAccountNumber
                                    SET     PolicyNumber = @1 ,
                                            PolicyPeriodFrom = @2 ,
                                            PolicyPeriodTo = @3 ,
                                            ValidTo = @4
                                    WHERE   VirtualAccountNumber = @0";
            string strSQL = @";EXEC Usp_RetreivePolicyEndorsementCustRequestVABooking @@OrderNo=@0";

            string strSQLBeyond1 = @"INSERT  INTO Finance.CustRequestEndorsementVirtualAccount
                                            (PolicyNo,
                                              EndorsementType,
                                              EndorsementNo,
                                              OrderNo,
                                              VirtualAccountNumber,
                                              CreatedBy,
                                              CreatedDate,
                                              RowStatus
                                            )
                                    VALUES( @0,
                                              @1,
                                              @2,
                                              @3,
                                              @4,
                                              @5,
                                              GETDATE(),
                                              0
                                            )";

            int result = 0;
            VATRansaction VAInfo = null;


            using (var db = GetAABDB())
            {
                VAInfo = db.Query<VATRansaction>(query, OrderNo, VANumber).FirstOrDefault();
            }
            if (VAInfo != null)
            {
                VAInfo.VirtualAccountNo = VANumber;
                //VAInfo.SalesmanAdminLogID = EntryUser;

                using (var db1 = Geta2isBeyondDB())
                {
                    //Sprint 11: Update insert by SP
                    //result = db.Execute(qInsertAABVA, CommonLogic.IsNA(VAInfo.PolicyNo), CommonLogic.IsNA(VAInfo.VirtualAccountNo), CommonLogic.IsNA(VAInfo.CurrencyCode), CommonLogic.IsNA(VAInfo.PremiumAmount), CommonLogic.IsNA(VAInfo.PolicyStatusCode), CommonLogic.IsNA(VAInfo.InsuredCode), CommonLogic.IsNA(VAInfo.InsuredName), CommonLogic.IsNA(VAInfo.MarketingCode), CommonLogic.IsNA(VAInfo.SalesmanAdminLogID), CommonLogic.IsNA(VAInfo.InternalDistributionCode), CommonLogic.IsNA(VAInfo.PolicyPeriodFrom), CommonLogic.IsNA(VAInfo.PolicyPeriodTo), CommonLogic.IsNA(VAInfo.GracePeriod), CommonLogic.IsNA(VAInfo.PaymentDueDate), CommonLogic.IsNA(VAInfo.PolicyNoRenewalBeyond), CommonLogic.IsNA(VAInfo.OrderNo), CommonLogic.IsNA(VAInfo.ChasisNo), CommonLogic.IsNA(VAInfo.EngineNo), CommonLogic.IsNA(VAInfo.PolicyStatus), CommonLogic.IsNA(VAInfo.PoliceNo), CommonLogic.IsNA(VAInfo.CustomerName), EntryUser);
                    result = db1.Query<int>(qInsertAABVA,
                        CommonLogic.IsNA(VAInfo.PolicyNo),
                        CommonLogic.IsNA(VAInfo.VirtualAccountNo),
                        CommonLogic.IsNA(VAInfo.CurrencyCode),
                        CommonLogic.IsNA(VAInfo.PremiumAmount),
                        CommonLogic.IsNA(VAInfo.PolicyStatusCode),
                        CommonLogic.IsNA(VAInfo.InsuredCode),
                        CommonLogic.IsNA(VAInfo.InsuredName),
                        CommonLogic.IsNA(VAInfo.MarketingCode),
                        CommonLogic.IsNA(VAInfo.SalesmanAdminLogID),
                        CommonLogic.IsNA(VAInfo.InternalDistributionCode),
                        CommonLogic.IsNA(VAInfo.PolicyPeriodFrom),
                        CommonLogic.IsNA(VAInfo.PolicyPeriodTo),
                        CommonLogic.IsNA(VAInfo.GracePeriod),
                        CommonLogic.IsNA(VAInfo.PaymentDueDate),
                        UserName,
                        CommonLogic.IsNA(VAInfo.PolicyNoRenewalBeyond),
                        CommonLogic.IsNA(VAInfo.OrderNo),
                        CommonLogic.IsNA(VAInfo.ChasisNo),
                        CommonLogic.IsNA(VAInfo.EngineNo),
                        CommonLogic.IsNA(VAInfo.PolicyStatus),
                        CommonLogic.IsNA(VAInfo.CustomerName),
                        CommonLogic.IsNA(VAInfo.PoliceNo)).SingleOrDefault();
                }
                if (result == 0)
                {
                    using (var db2 = GetAABDB())
                    {
                        db2.Query<dynamic>(qUpsertVALog, CommonLogic.IsNA(VAInfo.PolicyNo), CommonLogic.IsNA(VAInfo.VirtualAccountNo), CommonLogic.IsNA(VAInfo.CurrencyCode), CommonLogic.IsNA(VAInfo.PremiumAmount), CommonLogic.IsNA(VAInfo.PolicyStatusCode), CommonLogic.IsNA(VAInfo.InsuredCode), CommonLogic.IsNA(VAInfo.InsuredName), CommonLogic.IsNA(VAInfo.MarketingCode), CommonLogic.IsNA(VAInfo.SalesmanAdminLogID), CommonLogic.IsNA(VAInfo.InternalDistributionCode), CommonLogic.IsNA(VAInfo.PolicyPeriodFrom), CommonLogic.IsNA(VAInfo.PolicyPeriodTo), CommonLogic.IsNA(VAInfo.GracePeriod), CommonLogic.IsNA(VAInfo.PaymentDueDate), CommonLogic.IsNA(VAInfo.PolicyNoRenewalBeyond), CommonLogic.IsNA(VAInfo.OrderNo), CommonLogic.IsNA(VAInfo.ChasisNo), CommonLogic.IsNA(VAInfo.EngineNo), CommonLogic.IsNA(VAInfo.PolicyStatus), CommonLogic.IsNA(VAInfo.CustomerName), CommonLogic.IsNA(VAInfo.PoliceNo)).FirstOrDefault();
                    }
                }
                using (var db3 = GetAABDB())
                {
                   // string qOrdVATRansaction = @"INSERT INTO dbo.Ord_VA_Transaction
			                //        ( OrderNo, PolicyNo, VANo, UploadStatus, EntryUsr, EntryDt)
			                //VALUES  ( @0, @1, @2, @3, @4, GETDATE())";

                   // db3.Execute(qOrdVATRansaction, CommonLogic.IsNA(OrderNo), CommonLogic.IsNA(VAInfo.PolicyNo), CommonLogic.IsNA(VAInfo.VirtualAccountNo), CommonLogic.IsNA(CommonLogic.IsNA(result)), UserName);
                    db3.Execute(queryUpdate, OrderNo, VAInfo.VirtualAccountNo);
                }
                using (var db4 = Geta2isBeyondDB())
                {
                    db4.Execute(strSQLBeyond, CommonLogic.IsNA(VAInfo.VirtualAccountNo), CommonLogic.IsNA(VAInfo.PolicyNo), CommonLogic.IsNA(VAInfo.PolicyPeriodFrom), CommonLogic.IsNA(VAInfo.PolicyPeriodTo), CommonLogic.IsNA(VAInfo.PolicyPeriodTo));
                }

                string EndorsementType = string.Empty;
                decimal EndorsementNo = 0;
                string tmpPolicyNo = string.Empty;
                using (var db5 = GetAABDB())
                {
                    var rsTemp = db5.Query<dynamic>(strSQL, OrderNo);
                    if (rsTemp != null)
                    {
                        EndorsementType = rsTemp.FirstOrDefault().Endorsement_Type;
                        EndorsementNo = rsTemp.FirstOrDefault().Endorsement_No;
                        tmpPolicyNo = rsTemp.FirstOrDefault().Policy_No;
                    }
                    if (!string.IsNullOrEmpty(EndorsementType))
                        EndorsementType = EndorsementType.TrimEnd();
                    if (!string.IsNullOrEmpty(tmpPolicyNo))
                        tmpPolicyNo = tmpPolicyNo.TrimEnd();

                    if (EndorsementType == "0" && EndorsementNo > 0 && tmpPolicyNo == "")
                    {
                        using (var db6 = Geta2isBeyondDB())
                        {
                            db6.Execute(strSQLBeyond1,
                                CommonLogic.IsNA(VAInfo.PolicyNo),
                                CommonLogic.IsNA(EndorsementType),
                                EndorsementNo,
                                CommonLogic.IsNA(OrderNo),
                                CommonLogic.IsNA(VAInfo.VirtualAccountNo),
                                UserName);
                        }
                    }
                }
            }
        }

        public void AddIndirectInsurance(IndirectInsurance S, string EntryUser)
        {
            int result = 0;
            string query = @"INSERT INTO dbo.Ord_Inward_RI
                                    ( Order_No ,
                                      Acceptance_Id ,
                                      Full_Received ,
                                      Ceding_Coy ,
                                      Ref_Policy_No ,
                                      Offering_Letter_Date ,
                                      Offering_Letter_No ,
                                      Our_Share_Amt ,
                                      Our_Share_Pct ,
                                      Handling_Fee_Pct ,
                                      RI_Slip_Date ,
                                      Slip_Receive_Date ,
                                      WPC_Date ,
                                      Period_From ,
                                      Period_To ,
                                      Original_Rate ,
                                      Calc_Method ,
                                      EntryUsr ,
                                      EntryDt ,
                                      UpdateUsr
                                    )
                            VALUES  ( @0 , -- Order_No - char(15)
                                      @1 , -- Acceptance_Id - char(6)
                                      @2 , -- Full_Received - bit
                                      @3 , -- Ceding_Coy - char(11)
                                      @4 , -- Ref_Policy_No - varchar(30)
                                      @5 , -- Offering_Letter_Date - datetime
                                      @6 , -- Offering_Letter_No - varchar(30)
                                      @7 , -- Our_Share_Amt - numeric
                                      @8 , -- Our_Share_Pct - numeric
                                      @9 , -- Handling_Fee_Pct - numeric
                                      @10 , -- RI_Slip_Date - datetime
                                      @11 , -- Slip_Receive_Date - datetime
                                      @12 , -- WPC_Date - datetime
                                      @13,-- Period_From - datetime
                                      @14 , -- Period_To - datetime
                                      @15 , -- Original_Rate - numeric
                                      @16 , -- Calc_Method - char(1)
                                      @17 , -- EntryUsr - char(5)
                                      GETDATE(),  -- EntryDt - datetime          
                                      ''
                                    )";
            using (var db = GetAABDB())
            {

                result += db.Execute(query, IsNA(S.OrderNo), IsNA(S.AcceptanceId), IsNA(S.FullReceived), IsNA(S.CedingCoy), IsNA(S.RefPolicyNo),
                            IsNullableDate(S.OfferingLetterDate), IsNA(S.OfferingLetterNo), IsNA(S.OurShareAmt), IsNA(S.OurSharePct),
                            IsNA(S.HandlingFeePct), IsNullableDate(S.RISlipDate), IsNullableDate(S.SlipReceiveDate), IsNullableDate(S.WPCDate),
                           IsNullableDate(S.PeriodFrom), IsNullableDate(S.PeriodTo), S.OriginalRate, IsNA(S.CalcMethod),
                            EntryUser);

            }

        }
        public void AddIndirectMember(List<IndirectMember> S, string EntryUser)
        {
            string query = @"INSERT INTO dbo.Ord_Inward_Member
                                    ( Order_No ,
                                      Member_Code ,
                                      Member_Type ,
                                      Member_Share_Pct ,
                                      EntryUsr ,
                                      EntryDt ,
                                      UpdateUsr
                                    )
                            VALUES  ( @0 , -- Order_No - char(15)
                                      @1 , -- Member_Code - char(11)
                                      @2 , -- Member_Type - char(1)
                                      @3 , -- Member_Share_Pct - numeric
                                      @4 , -- EntryUsr - char(5)
                                      GETDATE(),
                                      ''
                                    )";
            using (var db = GetAABDB())
            {
                foreach (var item in S)
                {
                    db.Execute(query, item.OrderNo, item.MemberCode, item.MemberType, item.MemberSharePct, EntryUser);
                }
            }

        }

        public bool UpdateAnalysisOrder(string OrderNo)
        {
            string qAnalysis = @";EXEC dbo.sp_Execute_Order_Analysis_0069URF2018 @@OrderNo = @0";
            string queryUpdate = @"UPDATE  dbo.Mst_Order
                                    SET Analysis = @1, Notes=@1
                                    WHERE Order_No = @0";
            using (var db = GetAABDB())
            {
                try
                {
                    var text = db.Fetch<dynamic>(qAnalysis, OrderNo);
                    if (text.Count() > 0)
                    {
                        db.Execute(queryUpdate, OrderNo, text[0].Info_Text);

                    }
                    return true;
                }
                catch
                {
                    return false;
                }
            }

        }
        public static bool UpdateVaNo(string OrderNo, string vaNo)
        {
            string queryUpdate = @"UPDATE  dbo.Mst_Order
                                    SET VANumber = @1
                                    WHERE Order_No = @0";
            using (var db = GetAABDB())
            {
                var result = db.Execute(queryUpdate, OrderNo, vaNo);
                if (result > 0)
                    return true;
            }
            return false;
        }


        #endregion

        #region Get mst_order

        public dynamic GetOrderExInfo(string OrderNo)
        {

            string query = @"SELECT TOP 1
                                    Policy_No as PolicyNo,
                                    Policy_Id as PolicyID,
                                    VANumber AS VANo
                            FROM    MSt_Order
                            WHERE   Order_No = @0
                                    AND Order_Status = '99'";

            using (var db = GetAABDB())
            {
                return db.Query<dynamic>(query, OrderNo).FirstOrDefault();
            }
        }

        #endregion

        #region mst_customer

        public bool IsCustomer(string OrderNo)
        {

            string query = @"Select Top 1 Cust_ID From Mst_Customer
                                Where Cust_ID =@0";

            using (var db = GetAABDB())
            {
                string CustomerID = db.Query<string>(query, OrderNo).SingleOrDefault();
                return !string.IsNullOrWhiteSpace(CustomerID);
            }
        }
        #endregion

        #region titan

        public TitanAgentInfo GetTitanAgentInfo(string CustomerID)
        {

            #region Query
            string query = @"SELECT  DISTINCT *
                            FROM    ( SELECT    b.Cust_Id AS CustomerID ,
                                                a.Client_Code AS ClientCode ,
                                                RTRIM(b.Name) AS ClientName ,
                                                c.Cust_Id AS UplinerCustomerID ,
                                                a.Upliner_Client_Code AS UplinerCode ,
                                                RTRIM(d.Name) AS UplinerName ,
                                                e.Cust_Id AS LeaderCustomerID ,
                                                a.Leader_Client_Code AS LeaderCode ,
                                                RTRIM(f.Name) AS LeaderName
                                      FROM      dtl_cust_type a WITH ( NOLOCK )
                                                INNER JOIN mst_customer b WITH ( NOLOCK ) ON a.cust_id = b.cust_id
                                                LEFT JOIN dtl_cust_type c WITH ( NOLOCK ) ON a.Upliner_Client_Code = c.Client_Code
                                                LEFT JOIN mst_customer d WITH ( NOLOCK ) ON c.Cust_Id = d.cust_id
                                                LEFT JOIN dtl_cust_type e WITH ( NOLOCK ) ON a.Leader_Client_Code = e.Client_Code
                                                LEFT JOIN mst_customer f WITH ( NOLOCK ) ON e.Cust_Id = f.cust_id
                                      WHERE     a.Client_Type = 'AGENT'
                                    ) AS X
                            WHERE    X.ClientCode = @0";
            #endregion


            using (var db = GetAABDB())
            {
                return db.Query<TitanAgentInfo>(query, CustomerID).FirstOrDefault();
            }
        }

        public bool InsertOrderTitan(Order ord, string UserLogin)
        {
            string insertSQLOrder = @"INSERT  INTO dbo.Orders
                                            ( OrderNo ,
                                              OrderDate ,
                                              Product ,
                                              TType ,
                                              ID ,
                                              Branch ,
                                              CustName ,
                                              SurveyF ,
                                              OrderStatus ,
                                              Remarks ,
                                              SurveyStatus ,
                                              SurveyResult ,
                                              ApprovalStatus ,
                                              ApprovalDate ,
                                              PolicyNo ,
                                              RefNo ,
                                              PaymentStatus ,
                                              PaymentDate ,
                                              LastOpr ,
                                              LastDateModified ,
                                              LastTimeModified ,
                                              GWP ,
                                              EntryDate ,
                                              Inception ,
                                              Expiry ,
                                              HAddress ,
                                              OAddress ,
                                              HPhone ,
                                              OPhone ,
                                              MobilePhone ,
                                              Email ,
                                              ClaimRatio ,
                                              ClaimFrequency ,
                                              TSI ,
                                              Premium
                                            )
                                    VALUES  ( @0 , -- OrderNo - int
                                              @1 , -- OrderDate - smalldatetime
                                              @2 , -- Product - varchar(10)
                                              @3 , -- TType - varchar(20)
                                              @4 , -- ID - varchar(20)
                                              @5 , -- Branch - varchar(10)
                                              @6 , -- CustName - varchar(100)
                                              @7 , -- SurveyF - bit
                                              @8 , -- OrderStatus - varchar(10)
                                              @9 , -- Remarks - varchar(255)
                                              @10 , -- SurveyStatus - varchar(20)
                                              @11 , -- SurveyResult - varchar(20)
                                              @12 , -- ApprovalStatus - varchar(20)
                                              @13 , -- ApprovalDate - smalldatetime
                                              @14 , -- PolicyNo - varchar(50)
                                              @15 , -- RefNo - varchar(50)
                                              @16 , -- PaymentStatus - varchar(20)
                                              @17 , -- PaymentDate - smalldatetime
                                              @18 , -- LastOpr - varchar(10)
                                              @19 , -- LastDateModified - smalldatetime
                                              @20 , -- LastTimeModified - varchar(5)
                                              @21 , -- GWP - float
                                              @22 , -- EntryDate - smalldatetime
                                              @23 , -- Inception - smalldatetime
                                              @24 , -- Expiry - smalldatetime
                                              @25 , -- HAddress - varchar(255)
                                              @26 , -- OAddress - varchar(255)
                                              @27 , -- HPhone - varchar(20)
                                              @28 , -- OPhone - varchar(20)
                                              @29 , -- MobilePhone - varchar(20)
                                              @30 , -- Email - varchar(50)
                                              @31 , -- ClaimRatio - float
                                              @32 , -- ClaimFrequency - float
                                              @33 , -- TSI - float
                                              @34  -- Premium - float
                                    )";
            string insertSQLOrderDetail = @"INSERT INTO dbo.OrderDetail
                                                    ( OrderNo ,
                                                      ObjectNo ,
                                                      Inception ,
                                                      Expiry ,
                                                      Coverage ,
                                                      Brand ,
                                                      Model ,
                                                      PoliceNum ,
                                                      ChasisNum ,
                                                      MachineNum ,
                                                      Description ,
                                                      Location ,
                                                      Usage ,
                                                      CarType ,
                                                      TSI ,
                                                      Premium ,
                                                      ManYear 
                                                    )
                                            VALUES  ( @0 , -- OrderNo - int
                                                      @1 , -- ObjectNo - int
                                                      @2 , -- Inception - smalldatetime
                                                      @3 , -- Expiry - smalldatetime
                                                      @4 , -- Coverage - varchar(20)
                                                      @5 , -- Brand - varchar(100)
                                                      @6 , -- Model - varchar(100)
                                                      @7 , -- PoliceNum - varchar(50)
                                                      @8 , -- ChasisNum - varchar(50)
                                                      @9 , -- MachineNum - varchar(50)
                                                      @10 , -- Description - varchar(100)
		                                              @11 , -- Location - varchar(20)
                                                      @12 , -- Usage - varchar(20)
                                                      @13 , -- CarType - varchar(20)
                                                      @14 , -- TSI - float
                                                      @15 , -- Premium - float
                                                      @16  -- ManYear - varchar(10)
                                                    )";
            string UpdateTitanSeqNo = @"UPDATE dbo.SysSeqNo SET SeqNo = @0 WHERE ObjName = 'Orders' ";
            using (var db = Geta2isTitanDB())
            {
                CustomerRepository custRepo = new CustomerRepository();
                PolicyRepository policyRepo = new PolicyRepository();
                string CustomerName = string.Empty;
                string HomeAddress = string.Empty;
                string OfficeAddress = string.Empty;
                string HomePhone = string.Empty;
                string OfficePhone = string.Empty;
                string MobilePhone = string.Empty;
                string Email = string.Empty;
                if (!string.IsNullOrEmpty(ord.OrderData.CustId))
                    CustomerName = custRepo.GetCustomerInfo(ord.OrderData.CustId, out HomeAddress, out OfficeAddress, out HomePhone, out OfficePhone, out MobilePhone, out Email);
                else if (!string.IsNullOrEmpty(ord.OrderData.ProspectId))
                    CustomerName = custRepo.GetProspectInfo(ord.OrderData.ProspectId, out HomeAddress, out OfficeAddress, out HomePhone, out OfficePhone, out MobilePhone, out Email);

                bool surveyF = false;
                if (ord.SurveyInfo.Count() > 0)
                {
                    surveyF = true;

                }

                db.Execute(UpdateTitanSeqNo, ord.OrderData.TitanOrderNo);
                var result = db.Execute(insertSQLOrder,
                                                ord.OrderData.TitanOrderNo,
                                                ord.OrderData.OrderDate,
                                                ord.OrderData.ProductCode,
                                                "NEW",
                                                ord.OrderData.BrokerCode,
                                                ord.OrderData.BranchId,
                                                CustomerName,
                                                surveyF,
                                                "NOK",
                                                ord.OrderData.Notes,
                                                "",
                                                "",
                                                "",
                                                null,
                                                "",
                                                ord.OrderData.OrderNo,
                                                "",
                                                null,
                                                UserLogin,
                                                DateTime.Now,
                                                (DateTime.Now.Hour + ":" + DateTime.Now.Minute),
                                                0,
                                                DateTime.Now,
                                                ord.OrderData.PeriodFrom,
                                                ord.OrderData.PeriodTo,
                                                HomeAddress,
                                                OfficeAddress,
                                                HomePhone,
                                                OfficePhone,
                                                MobilePhone,
                                                Email,
                                                0,
                                                0,
                                                ord.InterestData.Select(e => e.SumInsured).Sum(),
                                                ord.CoverageData.Select(e => e.NetPremium).Sum()
                                                );
                if (result > 0)
                {
                    foreach (var item in ord.ObjectDetailData)
                    {
                        string coverageID = string.Empty;
                        if (ord.CoverageData.Select(e => e.CoverageId == "ALLRIK" && e.ObjectNo == item.ObjectNo).Count() > 0)
                            coverageID = "ALLRIK";
                        else if (ord.CoverageData.Select(e => e.CoverageId == "TLO" && e.ObjectNo == item.ObjectNo).Count() > 0)
                            coverageID = "TLO";
                        string Brand = string.Empty;
                        string Model = string.Empty;
                        var vehicleList = policyRepo.GetVehicleDetail(item.VehicleCode);
                        if (vehicleList.Count > 0)
                        {
                            Brand = vehicleList[0].BrandName;
                            Model = vehicleList[0].ModelName;
                        }
                        db.Execute(insertSQLOrderDetail,
                                                ord.OrderData.TitanOrderNo,
                                                item.ObjectNo,
                                                ord.OrderData.PeriodFrom,
                                                ord.OrderData.PeriodTo,
                                                coverageID,
                                                Brand,
                                                Model,
                                                "",
                                                item.ChasisNumber,
                                                item.EngineNumber,
                                                "",
                                                item.GeoAreaCode,
                                                item.UsageCode,
                                                item.VehicleType,
                                                ord.InterestData.Where(e => e.ObjectNo == item.ObjectNo).Select(e => e.SumInsured).Sum(),
                                                ord.CoverageData.Where(e => e.ObjectNo == item.ObjectNo).Select(e => e.NetPremium).Sum(),
                                                item.MfgYear
                                                );
                    }

                    return true;
                }
            }
            return false;
        }

        public int AddLnkOrderAgency(string OrderNo, string RefferenceNo = "", string OtherNo = "", string ClientCode = "", string UplinerCode = "", string LeaderCode = "", string Actor = "")
        {
            int result = 0;
            #region Query
            string query = @"INSERT INTO dbo.Lnk_Order_Agency
                                        ( Order_No ,
                                          Reference_No ,
                                          Reference_Type ,
                                          Other_No ,
                                          Client_Code ,
                                          Upliner_Client_Code ,
                                          Leader_Client_Code ,
                                          Send_Status ,
                                          Expired_Send_Status ,
		                                  EntryUsr,
		                                  EntryDt
                                        )
                                VALUES  ( @0 , -- Order_No - char(15)
                                          @1 , -- Reference_No - varchar(50)
                                          'TITAN' , -- Reference_Type - char(6)
                                          @2 , -- Other_No - varchar(50)
                                          @3 , -- Client_Code - varchar(22)
                                          @4 , -- Upliner_Client_Code - varchar(22)
                                          @5 , -- Leader_Client_Code - varchar(22)
                                          0 , -- Send_Status - tinyint
                                          0 , -- Expired_Send_Status - tinyint
                                          @6 , -- EntryUsr - char(5)
                                          GETDATE() -- EntryDt - datetime
                                        )";
            #endregion


            using (var db = GetAABDB())
            {
                result = db.Execute(query, OrderNo, RefferenceNo, OtherNo, ClientCode, UplinerCode, LeaderCode, Actor);
            }
            return result;
        }



        #endregion

        #region VANumber
        public dynamic ValidatePreOrderedVA(string VirtualAccountNo)
        {
            string query = @"SELECT  RTRIM(ISNULL(VirtualAccountNumber,'')) AS VirtualAccountNumber,
                            RTRIM(ISNULL(PolicyNumber,'')) AS PolicyNumber
                            FROM    Finance.VirtualAccountNumber AS van
                            WHERE   RowStatus = 0
                                    AND PolicyType = 'NEW'
                                    AND LEN(ISNULL(PolicyNumber, '')) = 0
                                    AND ValidTo >= CAST(GETDATE() AS DATE)
                                    AND VirtualAccountNumber = @0 ";
            using (var db = Geta2isBeyondDB())
            {
                return db.Query<dynamic>(query, VirtualAccountNo).FirstOrDefault();
            }
        }

        public dynamic CheckPreOrderedVAByOrderNO(string OrderNo, string VirtualAccountNo)
        {
            string query = @"SELECT TOP 1
                                    RTRIM(ISNULL(a.VirtualAccountNumber, '')) AS VANumber ,
                                    RTRIM(ISNULL(a.PolicyNumber, '')) AS PolicyNo
                            FROM    Finance.VirtualAccountNumber a
                            WHERE   a.PolicyNumber = @0
                                    AND a.VirtualAccountNumber = @1 ";
            string qPolicy = @"Select Top 1 Policy_No From Mst_Order Where Order_No=@0";
            string PolicyNo = string.Empty;
            using (var db = GetAABDB())
            {
                PolicyNo = db.Query<string>(qPolicy, OrderNo).SingleOrDefault();
            }
            if (string.IsNullOrWhiteSpace(PolicyNo))
            {
                return null;
            }
            else
            {
                using (var db = Geta2isBeyondDB())
                {
                    return db.Fetch<dynamic>(query, PolicyNo, VirtualAccountNo);
                }
            }
        }

        #endregion

        #region Agency
        public AgencyInfo GetAgencyInfo(string agentCode)
        {
            //TODO : ini diganti jadi query ke AABMobileDB, tapi sekarang UplinerID dan LeaderID ambil darimana?
            int result = 0;
            #region Query
            string query = @"SELECT  top 1 *
FROM    ( SELECT    b.Cust_Id AS AgentID,
    a.Client_Code AS ClientCode,
                    RTRIM(b.Name) AS ClientName,                                 
                    c.Cust_Id AS UplinerID,
    a.Upliner_Client_Code AS UplinerCode ,
    RTRIM(d.Name) AS UplinerName,
    e.Cust_Id AS LeaderID,
    a.Leader_Client_Code AS LeaderCode ,                  
    RTRIM(f.Name) AS LeaderName,
	b.Cust_Type AgentType                    
            FROM      dtl_cust_type a WITH ( NOLOCK )
                    INNER JOIN mst_customer b WITH ( NOLOCK ) ON a.cust_id = b.cust_id                    
    LEFT JOIN dtl_cust_type c WITH ( NOLOCK ) ON a.Upliner_Client_Code=c.Client_Code
                    LEFT JOIN mst_customer d WITH ( NOLOCK ) ON c.Cust_Id = d.cust_id                    
    LEFT JOIN dtl_cust_type e WITH ( NOLOCK ) ON a.Leader_Client_Code=e.Client_Code
                    LEFT JOIN mst_customer f WITH ( NOLOCK ) ON e.Cust_Id = f.cust_id                    
            WHERE     a.Client_Type = 'AGENT'
        ) AS X
Where X.AgentID IN (@0) OR X.ClientCode IN (@0)";
            #endregion

            using (var db = GetAABDB())
            {
                return db.Query<AgencyInfo>(query, agentCode).FirstOrDefault();
            }
        }

        public int GetExistTitanOrderNo(string OrderNo) {
            try
            {
                string query = @"DECLARE @@OrderNo INT
SELECT @@OrderNo=OrderNo FROM dbo.Orders WHERE RefNo = @0
IF(@@OrderNo IS NOT NULL AND @@OrderNo <> 0)
BEGIN
	SELECT @@OrderNo
END
ELSE
BEGIN
	SELECT 0
END";
                using (var db = Geta2isTitanDB())
                {
                    return db.ExecuteScalar<int>(query, OrderNo);
                }
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        #endregion

        #region Salesman

        public List<dynamic> GetMappingSalesmanProduct(string productCode, string MouID)
        {
            string query = @"SELECT  Comm_Id AS CommissionID,
		                            Product_Code AS ProductCode,
                                    Pay_To_Party  AS PayToParty
                            FROM    dbo.Prd_Commission AS pc ( NOLOCK )
                            WHERE   Product_Code = @0
                                    AND Pay_To_Party IN ( 3, 6, 7, 8 )
                                    AND Status = 1
                           Union ALL
                           SELECT  Commission_Code AS CommissionID,
		                            MOU_ID AS ProductCode,
                                    Pay_To_Party  AS PayToParty
                            FROM    dbo.Mou_Commission AS pc ( NOLOCK )
                            WHERE   Mou_ID = @1
                                    AND Pay_To_Party IN ( 3, 6, 7, 8 )
                                                        ";
            using (var db = GetAABDB())
            {
                return db.Fetch<dynamic>(query, productCode, MouID);
            }
        }

        #endregion


        #region Commission

        public dynamic Product_GetFlatCommission(string ProductCode, string ID, decimal BCSI, string IDType)
        {

            string query = @"SELECT  a.comm_id AS CommID ,
                                    a.bc_from_si AS BaseCurrencySumInsured ,
                                    a.percentage AS Percentage ,
                                    a.bc_amount AS BaseCurrencyAmount ,
                                    a.status AS Status
                            FROM    mst_flat_commission a
                            WHERE   a.status <> 0
                                    AND a.comm_id = @0
                                    AND a.bc_from_si =@1
                            ORDER BY a.bc_from_si DESC	";
            using (var db = GetAABDB())
            {
                var result = db.Query<dynamic>(query, ID, BCSI).FirstOrDefault();
                return result;
            }
        }

        public List<WTCommission> GetWTCommission(string ProductCode, string MouID)
        {

            string query = @"SELECT DISTINCT
                                    *
                            FROM    ( SELECT    pc.Apply_Flags AS ApplyFlags ,
                                                PC.Comm_Id AS CommissionID ,
                                                mc.Name AS CommissionName ,
                                                mc.Percentage AS Percentage ,
                                                pc.Pay_To_Party AS PayToParty ,
                                                mc.level_Disc AS Level ,
                                                mc.Tax_Indiv AS IndividualTax ,
                                                mc.Tax_Corp AS CorporateTax ,
                                                mc.Commission_Type AS CommissionType
                                      FROM      Prd_Commission pc
                                                INNER JOIN Mst_Commission mc ON pc.Comm_ID = mc.COMM_ID
                                      WHERE     pc.Status = 1
                                                AND mc.Status = 1
                                                AND mc.Commission_Type IN ( '2', '4' )
                                                AND pc.Product_Code = @0
                                      UNION ALL
                                      SELECT    pc.Apply_Flags AS ApplyFlags ,
                                                PC.Commission_Code AS CommissionID ,
                                                mc.Name AS CommissionName ,
                                                mc.Percentage AS Percentage ,
                                                pc.Pay_To_Party AS PayToParty ,
                                                mc.level_Disc AS Level ,
                                                mc.Tax_Indiv AS IndividualTax ,
                                                mc.Tax_Corp AS CorporateTax ,
                                                mc.Commission_Type AS CommissionType
                                      FROM      dbo.MOU_Commission AS pc
                                                INNER JOIN Mst_Commission mc ON pc.Commission_Code = mc.COMM_ID
                                      WHERE     mc.Status = 1
                                                AND mc.Commission_Type IN ( '2', '4' )
                                                AND pc.MOU_ID = @1
                                    ) AS X";
            using (var db = GetAABDB())
            {
                if (string.IsNullOrWhiteSpace(MouID))
                {
                    MouID = ProductCode;
                }
                var result = db.Query<WTCommission>(query, ProductCode, MouID).ToList();
                return result;
            }
        }
        public List<WTCommission> GetWTDiscount(string QuotationNo, string ProductCode, string MouID)
        {

            string query = @"SELECT DISTINCT
                                *
                        FROM    ( SELECT    pc.Apply_Flags AS ApplyFlags ,
                                            PC.Comm_Id AS CommissionID ,
                                            mc.Name AS CommissionName ,
                                            wc.Percentage AS Percentage ,
                                            pc.Pay_To_Party AS PayToParty ,
                                            mc.level_Disc AS Level ,
                                            mc.Tax_Indiv AS IndividualTax ,
                                            mc.Tax_Corp AS CorporateTax ,
                                            mc.Commission_Type AS CommissionType
                                  FROM      Prd_Commission pc
                                            INNER JOIN Mst_Commission mc ON pc.Comm_ID = mc.COMM_ID
                                            INNER JOIN dbo.WTCommission AS wc ON pc.Comm_Id = wc.CommisionID
                                                                                 AND wc.QuotationNo = @0
                                                                                 AND wc.RowStatus = 0
                                                                                AND wc.Percentage >0
                                            LEFT JOIN dbo.Prd_Profit_Renewal_Disc AS rcd ON rcd.Comm_ID = pc.COMM_ID and pc.product_code=rcd.Product_Code
                                  WHERE     pc.Status = 1
                                            AND mc.Status = 1
                                            AND mc.Commission_Type IN ( '1' )
                                            AND COALESCE(rcd.Comm_ID, '') = ''
                                            AND pc.Product_Code = @1
                                  UNION ALL
                                  SELECT    pc.Apply_Flags AS ApplyFlags ,
                                            PC.Commission_Code AS CommissionID ,
                                            mc.Name AS CommissionName ,
                                            wc.Percentage AS Percentage ,
                                            pc.Pay_To_Party AS PayToParty ,
                                            mc.level_Disc AS Level ,
                                            mc.Tax_Indiv AS IndividualTax ,
                                            mc.Tax_Corp AS CorporateTax ,
                                            mc.Commission_Type AS CommissionType
                                  FROM      dbo.MOU_Commission AS pc
                                            INNER JOIN Mst_Commission mc ON pc.Commission_Code = mc.COMM_ID
                                            INNER JOIN dbo.WTCommission AS wc ON pc.Commission_Code = wc.CommisionID
                                                                                 AND wc.QuotationNo = @0
                                                                                 AND wc.RowStatus = 0
                                                                                AND wc.Percentage >0
                                            LEFT JOIN dbo.Prd_Profit_Renewal_Disc AS rcd ON rcd.Comm_ID = pc.Commission_Code and pc.mou_id=rcd.Product_Code
                                  WHERE     mc.Status = 1
                                            AND COALESCE(rcd.Comm_ID, '') = ''
                                            AND mc.Commission_Type IN ( '1' )
                                            AND pc.MOU_ID = @2
                                ) AS X Where 1=1";
            string qGetRenDisc = "Select LTRIM(RTRIM(CODE)) From Mst_General Where type='CRD'";
            using (var db = GetAABDB())
            {
                List<string> RenDisc = db.Query<string>(qGetRenDisc).ToList();
                foreach (var vRenDisc in RenDisc)
                {
                    query = string.Format("{0} AND X.CommissionID NOT LIKE '{1}%'", query, vRenDisc);
                }
                if (string.IsNullOrWhiteSpace(MouID))
                {
                    MouID = ProductCode;
                }
                var result = db.Query<WTCommission>(query, QuotationNo, ProductCode, MouID).ToList();
                return result;
            }
        }

        #endregion

        #region Survey

        public void UpdateAdditionalOrderSurveyInfo(string AdditionalOrderAcquisitionSurveyID, string SurveyNo)
        {

            string qUpdateAdditionalOrderSurveyInfo = @"Update dbo.AdditionalOrderAcquisitionSurvey 
	                                                        Set SurveyNo=@0, SurveyCreated=1 
	                                                        Where ID=@1";
            using (var db = GetAABDB())
            {

                if (!string.IsNullOrWhiteSpace(AdditionalOrderAcquisitionSurveyID) && !string.IsNullOrWhiteSpace(SurveyNo))
                {
                    using (var svdb = Geta2isMobileSurveyDB())
                    {
                        svdb.Execute(qUpdateAdditionalOrderSurveyInfo, IsNA(SurveyNo), AdditionalOrderAcquisitionSurveyID);
                    }
                }
            }
        }

        #endregion

        #region Order Transaction
        public PendingOrderInfo IsPendingOrder(string OrderNo)
        {

            string query = @"SELECT TOP 1
                                    Order_No as OrderNo,
                                    EntryUsr as EntryUser,
                                    EntryDt as EntryDate
                            FROM    MSt_Order
                            WHERE   Order_No = @0
                                    AND Order_Status = '99'";

            using (var db = GetAABDB())
            {
                return db.Query<PendingOrderInfo>(query, OrderNo).FirstOrDefault();

            }
        }

        public string GetTransactionIDEx(string OrderNo)
        {
            string TN = string.Empty;
            string query = @"SELECT TOP 1
                                    dt.Transaction_No
                            FROM    dbo.MSt_Order mo
                                    INNER JOIN dbo.Dtl_Transaction dt ON mo.Order_No = dt.Reference_No
                                                                         AND Transaction_Type = 'ORDER'
                            WHERE   Order_No = @0
                                    ";

            using (var db = GetAABDB())
            {
                TN = db.Query<string>(query, OrderNo).SingleOrDefault();

            }
            return TN;
        }

        public void DeleteAABTransaction(string OrderNo, int TitanOrderNo)
        {
            string query = @"DECLARE @@OrderNo AS Varchar(MAX)
	                        SET @@OrderNo=@0
	                        IF (COALESCE(@@OrderNo,'')<>'')
	                        BEGIN		
                            DELETE FROM dbo.ord_install_info WHERE Order_No=@@OrderNo
                            DELETE FROM dbo.dtl_transaction_account WHERE reference_no like @@OrderNo + '%' and reference_type='ORD'
                            DELETE FROM dbo.ord_dtl_doc WHERE Order_No=@@OrderNo
	                        DELETE FROM dbo.ord_dtl_address WHERE Order_No=@@OrderNo
	                        DELETE FROM dbo.ord_add_info WHERE Order_No=@@OrderNo
	                        DELETE FROM dbo.ord_inward_ri WHERE Order_No=@@OrderNo
	                        DELETE FROM dbo.ord_inward_member WHERE Order_No=@@OrderNo		 
	                        DELETE FROM dbo.ord_dtl_coverage WHERE Order_No=@@OrderNo
	                        DELETE FROM dbo.lnk_order_agency WHERE Order_No=@@OrderNo				
                            DELETE FROM dbo.ord_dtl_interest WHERE Order_No=@@OrderNo	
                            DELETE FROM dbo.ord_dtl_interest_item WHERE Order_No=@@OrderNo
	                        DELETE FROM dbo.ord_dtl_insured WHERE Order_No=@@OrderNo
	                        DELETE FROM dbo.ord_cover_commission WHERE Order_No=@@OrderNo		 
                            DELETE FROM dbo.ord_policy_clause WHERE Order_No=@@OrderNo
	                        DELETE FROM dbo.ord_dtl_deleted_policy_clause WHERE Order_No=@@OrderNo
	                        DELETE FROM dbo.ord_object_clause WHERE Order_No=@@OrderNo		
                            DELETE FROM dbo.ord_dtl_deleted_object_clause WHERE Order_No=@@OrderNo	
                            DELETE FROM dbo.ord_dtl_mv WHERE Order_No=@@OrderNo
	                        DELETE FROM dbo.ord_dtl_scoring WHERE Order_No=@@OrderNo			
                            DELETE FROM dbo.ord_dtl_object WHERE Order_No=@@OrderNo
	                        DELETE FROM comsales_ord_trx WHERE Order_Num=@@OrderNo
                            DELETE FROM dbo.Ord_Dtl_Original_Defect WHERE Order_No=@@OrderNo
                            DELETE FROM dbo.Ord_Dtl_NoCover WHERE Order_No=@@OrderNo
                            DELETE FROM dbo.Ord_Non_Standard_Accessories WHERE Order_No=@@OrderNo
	                        DELETE FROM dbo.mst_order WHERE Order_No=@@OrderNo
							DELETE FROM dbo.Survey WHERE Reference_No=@@OrderNo
                            DELETE FROM dbo.Mst_Order_Bussiness_Source WHERE Order_No=@@OrderNo
                            DELETE FROM dbo.Ord_VA_Transaction WHERE OrderNo=@@OrderNo 
	                        END";
            string queryTitan = @"
                                    DELETE dbo.Orders where OrderNo = @0
                                    DELETE dbo.OrderDetail where OrderNo = @0
                                ";
            string queryDeleteAAB2000VirtualAccount = @"UPDATE Finance.AAB2000VirtualAccount 
                                                        SET RowStatus =
                                                        (Select Coalesce(Max(RowStatus), 0) From Finance.AAB2000VirtualAccount where OrderNo = @0 )+1
                                                         where OrderNo = @0 and RowStatus = 0";
            using (var db = GetAABDB())
            {
                db.Execute(query, OrderNo);
            }
            using (var db1 = Geta2isTitanDB())
            {
                if (TitanOrderNo != 0)
                {
                    db1.Execute(queryTitan, TitanOrderNo);
                }
            }
            using (var db2 = Geta2isBeyondDB())
            {
                db2.Execute(queryDeleteAAB2000VirtualAccount, OrderNo);
            }
        }

        public void DeleteAABTransactionAfterSurvey(string OrderNo, int TitanOrderNo)
        {
            string query = @";DELETE FROM dbo.Ord_Inward_Member WHERE Order_No = @0
DELETE FROM dbo.Ord_Inward_RI WHERE Order_No = @0
DELETE FROM dbo.AAB2000VirtualAccount_Log WHERE OrderNo = @0
--DELETE FROM dbo.Ord_Object_Clause WHERE Order_No = @0
DELETE FROM dbo.Ord_Policy_Clause WHERE Order_No = @0
DELETE FROM dbo.Ord_Install_Info WHERE Order_No = @0
DELETE FROM dbo.Ord_Dtl_DOC WHERE Order_No = @0
DELETE FROM dbo.Ord_Dtl_Address WHERE Order_No = @0
--DELETE FROM dbo.Ord_Dtl_Interest_Item WHERE Order_No = @0
DELETE FROM dbo.Ord_Cover_Commission where Order_No = @0
DELETE FROM dbo.Ord_Dtl_Insured WHERE Order_No = @0
DELETE FROM dbo.Ord_Dtl_Coverage WHERE Order_No = @0
DELETE FROM dbo.Ord_Dtl_Interest WHERE Order_no = @0
DELETE FROM dbo.Ord_Dtl_MV WHERE Order_No = @0
DELETE FROM dbo.Ord_Dtl_Scoring WHERE Order_No = @0
DELETE FROM dbo.Ord_Dtl_Object WHERE Order_No = @0
DELETE FROM dbo.Ord_VA_Transaction WHERE OrderNo = @0
DELETE FROM dbo.Mst_Order WHERE Order_No = @0
DELETE FROM dbo.Lnk_Order_Agency WHERE Order_No = @0";
            string queryTitan = @"
                                    DELETE dbo.Orders where OrderNo = @0
                                    DELETE dbo.OrderDetail where OrderNo = @0
                                ";
            string queryDeleteAAB2000VirtualAccount = @"UPDATE Finance.AAB2000VirtualAccount 
                                                        SET RowStatus =
                                                        (Select Coalesce(Max(RowStatus), 0) From Finance.AAB2000VirtualAccount where OrderNo = @0 )+1
                                                         where OrderNo = @0 and RowStatus = 0";
            using (var db = GetAABDB())
            {
                db.Execute(query, OrderNo);
            }
            using (var db1 = Geta2isTitanDB())
            {
                if (TitanOrderNo != 0)
                {
                    db1.Execute(queryTitan, TitanOrderNo);
                }
            }
            using (var db2 = Geta2isBeyondDB())
            {
                db2.Execute(queryDeleteAAB2000VirtualAccount, OrderNo);
            }
        }

        public int TransactionSave(Transaction Trans, string EntryUser)
        {
            int result = 0;
            string query = @"if NOt exists(Select Top 1 * From dbo.[Transaction] Where Transaction_No=@0 and Reference_id=@1 and Reference_Type=@2)
                            BEGIN
                            INSERT INTO dbo.[Transaction]
                                                  ( Transaction_No ,
                                                                  Reference_Id ,
                                                                  Reference_Type ,
                                                                  EntryUsr ,
                                                                  EntryDt
                                                                )
                                                        VALUES  ( @0 , -- Transaction_No - char(11)
                                                                  @1 , -- Reference_Id - varchar(20)
                                                                  @2 , -- Reference_Type - char(6)
                                                                  @3 , -- EntryUsr - char(5)
                                                                  GetDate()  -- EntryDt - datetime
                                                                )
                            END";
            using (var db = GetAABDB())
            {
                result = db.Execute(query, Trans.TransactionNo, Trans.ReferenceId, Trans.ReferenceType, EntryUser);
            }
            return result;
        }

        public int TransactionSaveDetail(TransactionDetail Trans, string EntryUser)
        {
            int result = 0;
            string query = @"if NOt exists(Select Top 1 * From dbo.[DTL_TRANSACTION] Where Transaction_No=@0 and Reference_No=@1 and Transaction_Type=@2)
                                BEGIN
                                INSERT INTO DBO.[DTL_TRANSACTION] (Transaction_No, Reference_No, Transaction_Type, EntryUsr, EntryDt) 
                                                            VALUES (@0, @1, @2, @3, GetDate())
                                END
";
            using (var db = GetAABDB())
            {
                result = db.Execute(query, Trans.TransactionNo, Trans.ReferenceNo, Trans.TransactionType, EntryUser);
            }
            return result;
        }

        public bool InsertMstOrderMobile(string OrderNo, string UserName)
        {
            int result = 0;
            try
            {
                string query = @"INSERT INTO dbo.Mst_Order_Mobile
        ( Order_No ,
          Account_Info_BankName ,
          Account_Info_AccountNo ,
          Account_Info_AccountHolder ,
		  Approval_Status,
		  Approval_Process,
          isSO ,
          Approval_Type ,
          Actual_Date ,
          EntryDt ,
          EntryUsr, 
          SA_State
        )
VALUES  ( @0 , -- Order_No - char(15)
          NULL , -- Account_Info_BankName - varchar(60)
          NULL , -- Account_Info_AccountNo - varchar(255)
          NULL , -- Account_Info_AccountHolder - varchar(255)
          0 , -- Approval_Status - int
          0 , -- Approval_Process - bit
          0 , -- isSO - bit
          0 , -- Approval_Type - int
          GETDATE() , -- Actual_Date - datetime
          GETDATE() , -- EntryDt - datetime
          @1 , -- EntryUsr - char(5)
          NULL  -- SA_State - smallint
        )";
                using (var db = GetAABDB())
                {
                    db.Execute(query, OrderNo, UserName);
                    result = result + 1;
                }
            }
            catch (Exception e)
            {

                throw e;
            }
            return result > 0;
        }

        public bool InsertSurveyOrder(List<SurveyOrder> param)
        {
            int result = 0;
            try
            {
                string query = @";IF NOT EXISTS(SELECT * FROM SurveyManagement.dbo.SurveyOrder WHERE ReferenceNo = @1)
                                BEGIN
                                    INSERT INTO SurveyManagement.dbo.SurveyOrder
                                            ( ReferenceTypeCode ,
                                              ReferenceNo  ,
                                              AreaCode  ,
                                              SurveyDate  ,
                                              SurveyTime  ,
                                              SurveyType  ,
                                              SurveyMode  ,
                                              ApplicationSourceCode  ,
                                              AssignmentStatusCode  ,
                                              AssignedSurveyorID  ,
                                              ActualSurveyDate  ,
                                              ActualSurveyorID  ,
                                              CustomerOnLocation  ,
                                              PhoneNoOnLocation  ,
                                              SurveyAddress  ,
                                              WorkshopCode  ,
                                              Remark  ,
                                              OrderNo  ,
                                              LKNo  ,
                                              IsReassignF  ,
                                              IsRescheduleF ,
		                                      NameOnPolicy ,
                                              CreatedBy ,
                                              CreatedDate ,
                                              RowStatus ,
                                              ChassisNo ,
                                              EngineNo
                                            )
                                    VALUES  ( @0 , -- ReferenceTypeCode - varchar(10)
                                              @1 , -- ReferenceNo - varchar(50)
                                              @2 , -- AreaCode - varchar(11)
                                              @3 , -- SurveyDate - date
                                              @4 , -- SurveyTime - time
                                              @5 , -- SurveyType - varchar(6)
                                              @6 , -- SurveyMode - char(1)
                                              @7 , -- ApplicationSourceCode - varchar(10)
                                              'ASSCODE2' , -- AssignmentStatusCode - varchar(10)
                                              NULL , -- AssignedSurveyorID - varchar(10)
                                              NULL , -- ActualSurveyDate - date
                                              NULL , -- ActualSurveyorID - varchar(10)
                                              @8 , -- CustomerOnLocation - varchar(100)
                                              @9 , -- PhoneNoOnLocation - varchar(100)
                                              @10 , -- SurveyAddress - varchar(100)
                                              @11 , -- WorkshopCode - varchar(20)
                                              '' , -- Remark - varchar(5000)
                                              @12 , -- OrderNo - varchar(25)
                                              @13 , -- LKNo - varchar(15)
                                              0 , -- IsReassignF - smallint
                                              0 , -- IsRescheduleF - smallint
                                              @14 , -- NameOnPolicy - varchar(100)
                                              @15 , -- CreatedBy - varchar(20)
                                              GETDATE() , -- CreatedDate - datetime
                                              0 , -- RowStatus - smallint
                                              @16 , -- ChassisNo - varchar(30)
                                              @17  -- EngineNo - varchar(30)
                                            )
                                END";
                using (var db = GetAABDB())
                {
                    foreach (SurveyOrder bs in param)
                    {
                        db.Execute(query,
                            /*@0*/bs.ReferenceTypeCode,
                            /*@1*/bs.ReferenceNo,
                            /*@2*/bs.AreaCode,
                            /*@3*/bs.SurveyDate,
                            /*@4*/bs.SurveyTime,
                            /*@5*/bs.SurveyType,
                            /*@6*/bs.SurveyMode,
                            /*@7*/bs.ApplicationSourceCode,
                            /*@8*/bs.CustomerOnLocation,
                            /*@9*/bs.PhoneNoOnLocation,
                            /*@10*/bs.SurveyAddress,
                            /*@11*/bs.WorkshopCode,
                            /*@12*/bs.OrderNo,
                            /*@13*/bs.LKNo,
                            /*@14*/bs.NameOnPolicy,
                            /*@15*/bs.CreatedBy,
                            /*@16*/bs.ChassisNo,
                            /*@17*/bs.EngineNo);
                    }
                    result = result + 1;
                }
            }
            catch (Exception e)
            {

                throw e;
            }
            return result == param.Count;
        }

        public static bool UpdateVaNoOrderSimulation(string OrderNo, string vaNo)
        {
            string qSelect = "SELECT COALESCE(VANumber,'') VANumber FROM dbo.OrderSimulation WHERE PolicyOrderNo = @0";
            string queryUpdate = @"UPDATE dbo.OrderSimulation 
                                SET VANumber = @1
                                WHERE PolicyOrderNo = @0";
            using (var db = GetAABMobileDB())
            {
                string VANumber = db.ExecuteScalar<string>(qSelect, OrderNo);
                if(string.IsNullOrEmpty(VANumber))
                {
                    var result = db.Execute(queryUpdate, OrderNo, vaNo);
                    if (result > 0)
                        return true;
                }
            }
            return false;
        }

        public bool ChekVADataKotor(string OrderNo)
        {
            string query = "";
            var mobiledb = GetAABMobileDB();
            var aadb = GetAABDB();
            string oldPolicyNo = mobiledb.ExecuteScalar<string>(
                "SELECT COALESCE(OldPolicyNo,'') FROM dbo.OrderSimulation WHERE PolicyOrderNo = @0", OrderNo);
            if (!string.IsNullOrEmpty(oldPolicyNo)) {
                query = "SELECT * FROM dbo.Excluded_Renewal_Policy WHERE Policy_No = @0";
                List<dynamic> lsRen = aadb.Fetch<dynamic>(query, oldPolicyNo);
                if (lsRen.Count > 0) {
                    string VaNo = mobiledb.ExecuteScalar<string>(
                        "SELECT COALESCE(VANumber,'') FROM dbo.OrderSimulation WHERE OldPolicyNo = @0", oldPolicyNo);
                    if (string.IsNullOrEmpty(VaNo))
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        #endregion

        public string GetOrderNo(string followUpNo) {
            var db = GetAABMobileDB();
            try
            {
                string query = @"SELECT COALESCE(PolicyOrderNo,'') FROM dbo.Ordersimulation WHERE FollowUpNo = @0";
                return db.ExecuteScalar<string>(query, followUpNo);
            }
            catch (Exception)
            {
                return "";
            }
        }
    }
}
