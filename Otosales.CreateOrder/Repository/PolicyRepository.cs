﻿using Otosales.Logic;
using Otosales.Models;
using System;
using System.Collections.Generic;
using System.Linq;

using Otosales.Repository;

namespace Otosales.CreateOrder.Repository
{
    class PolicyRepository : RepositoryBase
    {
        public List<Installment> GetInstallment(string ProductCode, string TermCode)
        {
            string query = @"SELECT A.DAY_NO AS DayNo ,
                                        A.INSTALL_PCT AS InstallPercentage ,
                                        A.INTEREST_PCT AS InterestPercentage ,
                                        A.TERM_CODE AS TermCode
                                FROM    PRD_INSTALLMENT AS A
                                WHERE   A.PRODUCT_CODE = @0
                                        AND Term_Code = @1";


            using (var db = GetAABDB())
            {
                var result = db.Query<Installment>(query, ProductCode, TermCode).ToList();
                return result;
            }
        }
        public Product GetProductDetail(string ProductCode)
        {
            string query = @"SELECT  a.Product_Code AS ProductCode ,
                                    a.New_Product_code AS NewProductCode ,
                                    RTRIM(A.COB_Id) AS COBId ,                                   
                                    Z.Mou_id AS MouID ,
                                    X.Name AS COBName ,
                                    a.Description AS Description,
                                    X.Description AS COBDescription ,
                                    a.Valid_From AS ValidFrom ,
                                    a.Valid_To AS ValidTo ,
                                    RTRIM(a.Mou_No) AS MOUNumber ,
                                    a.Max_Customer_Object AS MaximumCustomerObject ,
                                    a.Used_Liability AS UsedLiability ,
                                    a.Maximum_Liability AS MaximumLiablity ,
                                    a.Check_Limit AS CheckLimit ,
                                    a.Currency_Id AS DefaultCurrencyId ,
                                    a.Bc_Policy_Fee AS BaseCurrencyPolicyFee ,
                                    a.Bc_Min_Premium AS BaseCurrencyMinimumPremium ,
                                    a.MO_Approve AS MOApprove ,
                                    a.Profit_Sharing_Calc AS ProfitSharingCalculation ,
                                    a.Renewal_Disc_Calc AS RenewalDiscountCalculation ,
                                    a.Issue_Card_To AS IssueCardTo ,
                                    a.Card_Type AS CardType ,
                                    a.Policy_to AS PolicyTo ,
                                    a.Grace_Period AS GracePeriod ,
                                    a.RL1 AS RemainderLetter1 ,
                                    a.RL2 AS RemainderLetter2 ,
                                    a.Print_Cover_Note AS PrintCoverNote ,
                                    a.Print_Note AS PrintNote ,
                                    a.All_Branch AS AllBranch ,
                                    a.Scheme_ID AS SchemeID ,
                                    a.Department_ID AS DepatmentID ,
                                    a.Salesman_ID AS SalesmanID ,
                                    ISNULL(a.Back_Date_Allowance, 0) AS BackDateAllowance ,
                                    a.Cust_ID AS CustomerID ,
                                    a.Customer_Group AS CustomerGroup ,
                                    a.Customer_POS AS CustomerPos ,
                                    a.Fin_Institution_ID AS FinancialInstitutionID ,
                                    a.Fin_Institution_Group AS FinancialInstitutionGroup ,
                                    a.Fin_Institution_Party AS FinancialInstitutionParty ,
                                    a.Fin_Institution_Pos AS FinancialInstitutionPos ,
                                    a.Broker_Code AS BrokerID ,
                                    a.Broker_Group AS BrokerGroup ,
                                    a.Broker_Party AS BrokerParty ,
                                    a.Broker_Pos AS BrokerPos ,
                                    a.Policy_Holder_Party AS PolicyHolderParty ,
                                    a.Policy_Holder_Pos AS PolicyHolderPos ,
                                    a.Payer_Party AS PayerParty ,
                                    a.Payer_Pos AS PayerPos ,
                                    a.Stock_Maximum_Possible AS StockMaximumPossible ,
                                    a.Stock_Refund_Limit AS StockRefundLimit ,
                                    a.Stock_Adj_Clause1 AS StockAdjustibleClause1 ,
                                    a.Stock_Adj_Clause2 AS StockAdjustibleClause2 ,
                                    a.premiumcalcFlags1 AS CustomerPremiumCalculationFlags ,
                                    a.premiumcalcFlags2 AS InternalPremiumCalculationFlags ,
                                    a.premiumcalcFlags3 AS OtherPremiumCalculationFlags ,
                                    a.PrintedPlyFlags AS PrintedPlyFlags ,
                                    a.PrintFlags1 AS NewPolicyPrintFlags ,
                                    a.PrintFlags2 AS RenewPolicyPrintFlags ,
                                    a.PrintFlags3 AS EndorsementPrintFlags ,
                                    a.PrintFlags4 AS CancellationPrintFlags ,
                                    a.day_calculation_method AS DayCalculationMethod ,
                                    a.Renewal_Flags AS RenewalFlags ,
                                    a.Payment_Flags AS PaymentFlags ,
                                    a.Production_Flags AS ProductionFlags ,
                                    a.Non_Standard_Flags AS NonStandardFlags ,
                                    a.Product_Type AS ProductType ,
                                    a.Status ,
                                    ISNULL(a.Coverage_Flags, 0) AS CoverageFlags ,
                                    a.Claim_Flags AS ClaimFlags ,
                                    a.OwnerFlags ,
                                    a.Grace_Period_Flag AS GracePeriodFlag ,
                                    a.Grace_Period_Max AS GracePeriodMax ,
                                    a.Acquisition_method AS AcquisitionMethod ,
                                    a.Acquisition_Cost AS AcquisitionCost ,
                                    a.Max_Acquisition AS MaxAcquisition ,
                                    a.Bonus_Expired_Period AS BonusExpiredPeriod ,
                                    a.Bonus_Calculation_Period AS BonusCalculationPeriod ,
                                    a.Biz_Type AS BizType ,
                                    a.Receipt_Flag AS ReceiptFlag ,
                                    ISNULL(a.isScoringAreaOJK, 0) AS isScoringAreaOJK ,
                                    ISNULL(a.LoadingType, 0) AS LoadingType,
                                    ( CASE WHEN ( SELECT TOP 1
                                        MP.Product_Code
                                                    FROM      dbo.Mst_Product MP
                                                            INNER JOIN dbo.Partner_Document_Template PDT ON PDT.Product_Code = MP.Product_Code
                                                    WHERE     PDT.Partner_ID = 'LEX'
                                                            AND MP.Status = 1
                                                            AND MP.Product_Code = @0
                                                ) IS NOT NULL THEN 1
                                            ELSE 0
                                        END ) AS IsLexus,
                                    (SELECT TOP 1 Pay_To_Party FROM dbo.Prd_Commission WHERE Product_Code = a.Product_Code) AS PayToParty
                            FROM    dbo.Mst_Product AS a
                                    Inner join dbo.Mst_COB AS x on X.COB_Id = A.COB_Id
                                    Left join dbo.MSt_Mou AS Z On a.Product_code=Z.Product_code                         
                                    WHERE  a.Product_Code = @0
                            ";
            using (var db = GetAABDB())
            {
                return db.Query<Product>(query, ProductCode).FirstOrDefault();
            }
        }

        public List<dynamic> GetPolicyFee(string ProductCode, decimal TotalPremi)
        {
            string query = @"SELECT TOP 1
                                    Policy_Fee1 AS PolicyAdminFee,
		                            Stamp_Duty1 AS StampDuty,
		                            Lower_Limit AS LowerLimit
                            FROM    PRD_POLICY_FEE
                            WHERE   Product_Code = @0
                                    AND Order_Type = 1
                                    AND Lower_Limit <= @1
                                    AND STATUS = 1
                            ORDER BY Lower_Limit ";
            using (var db = GetAABDB())
            {
                return db.Query<dynamic>(query, ProductCode, TotalPremi).ToList();
            }
        }

        public List<CheckSurveyDataByChasisEngineModel> GetSurveyResult(string ChassisNo, string EngineNo)
        {
            List<CheckSurveyDataByChasisEngineModel> result = new List<CheckSurveyDataByChasisEngineModel>();
            string query = @" SELECT  TOP 1 
                            Convert(Varchar(max),ID) AS AdditionalOrderAcquisitionSurveyID,
                            UPPER(RTRIM(ISNULL(SurveyorID,''))) AS SurveyorID ,
                            CONVERT(DATE, InsertedDate) AS SurveyDate ,
                            RTRIM(ISNULL(Name,'')) AS PICName,
                            RTRIM(ISNULL(PhoneNumber,'')) AS PhoneNumber,
                            RTRIM(ISNULL(SurveyLocation,'')) AS SurveyLocation,
                            RTRIM(ISNULL(ChassisEngine,'')) AS ChassisEngine,
                            RTRIM(ISNULL(Detail,'')) AS Detail,
                            '' AS AreaCode,
                            '' AS AreaAlias
                            FROM    dbo.AdditionalOrderAcquisitionSurvey
                            WHERE   ChassisEngine = @0 AND InsertedDate >= DATEADD(dd,DATEDIFF(dd,0,DATEADD(mm,-3,GETDATE())),0)
                            ORDER BY InsertedDate DESC";
            using (var db = Geta2isMobileSurveyDB())
            {
                List<CheckSurveyDataByChasisEngineModel> check1 = db.Fetch<CheckSurveyDataByChasisEngineModel>(query, ChassisNo);
                if (check1.Count <= 0)
                {
                    List<CheckSurveyDataByChasisEngineModel> check2 = db.Fetch<CheckSurveyDataByChasisEngineModel>(query, EngineNo);
                    if (check2.Count <= 0)
                        return result;
                    else
                        result = check2;
                }
                else
                    result = check1;


                if (result.Count() > 0)
                {
                    using (var db1 = Geta2isSurveyManagementDB())
                    {
                        var strArea = @"SELECT  TOP 1 a.AreaCode AS AreaCode ,
                                    RTRIM(ISNULL(c.Alias, '')) AreaAlias
                            FROM    dbo.SurveyorSchedule a
                                    INNER JOIN dbo.MasterSurveyArea b ON b.AreaCode = a.AreaCode
                                    INNER JOIN dbo.AreaAlias c ON a.AreaCode = c.AreaCode
                            WHERE   a.RowStatus = 0
                                    AND SurveyorID = @0
                                    AND ScheduleDate = CONVERT(DATE, @1)";
                        var strUserName = @"SELECT TOP 1 RTRIM(ISNULL(User_Name,'')) AS SurveyorName FROM AAB.dbo.Mst_User WHERE User_Id = @0";
                        var name = db1.Fetch<dynamic>(strUserName, result[0].SurveyorID);
                        if (name.Count > 0)
                        {
                            result[0].SurveyorName = name[0].SurveyorName;
                        }

                        var area = db1.Fetch<dynamic>(strArea, result[0].SurveyorID, DateTime.Parse(result[0].SurveyDate).Date);
                        if (area.Count > 0)
                        {
                            result[0].AreaCode = area[0].AreaCode;
                            result[0].AreaAlias = area[0].AreaAlias;
                        }
                        else
                        {
                            var nationalAreaCode = CommonLogic.GetSettingAreaCodeNasional();//  System.Configuration.ConfigurationManager.AppSettings["AreaCodeNasional"];
                            result[0].AreaCode = nationalAreaCode;
                            result[0].AreaAlias = "National";
                        }
                        result[0].SurveyDate = DateTime.Parse(result[0].SurveyDate).ToString("dd/MM/yyyy");
                    }
                }
                return result;
            }
        }

        public dynamic GetAABCodeGeo(string GeoCode)
        {
            string qGetAABCodeGeo = @"SELECT aab_code AS Code FROM dbo.partner_aab_map  WHERE partner_code =@0 AND type = 'GEO' AND partner_id = 'OJK'";

            using (var db = GetAABDB())
            {
                var result = db.Query<dynamic>(qGetAABCodeGeo, GeoCode).FirstOrDefault();
                return result;
            }
        }

        public dynamic GetVehicleDetail(string vehicleCode)
        {
            string query = @"SELECT  *
                                FROM    ( SELECT    A.vehicle_code AS VehicleCode ,
                                                    A.vehicle_type AS VehicleType ,
                                                    D.Description AS TypeDescription ,
                                                    A.series AS Series ,
                                                    A.Sitting AS SittingCapacity ,
                                                    ISNULL(A.Transmission, 9) AS Transmission ,
                                                    B.description AS BrandName ,
                                                    B.Brand_ID AS BrandID ,
                                                    C.description AS ModelName ,
                                                    C.Model_ID AS ModelID ,
                                                    ( CASE WHEN LEN(LTRIM(RTRIM(ISNULL(D.[Description], '')))) <> 0
                                                           THEN LTRIM(RTRIM(D.[Description])) + ' '
                                                           ELSE ''
                                                      END )
                                                    + ( CASE WHEN LEN(LTRIM(RTRIM(ISNULL(B.[Description], '')))) <> 0
                                                             THEN LTRIM(RTRIM(B.[Description])) + ' '
                                                             ELSE ''
                                                        END )
                                                    + ( CASE WHEN LEN(LTRIM(RTRIM(ISNULL(C.[Description], '')))) <> 0
                                                             THEN LTRIM(RTRIM(C.[Description])) + ' '
                                                             ELSE ''
                                                        END )
                                                    + ( CASE WHEN LEN(LTRIM(RTRIM(ISNULL(A.[Series], '')))) <> 0
                                                             THEN LTRIM(RTRIM(A.[Series])) + ' '
                                                             ELSE ''
                                                        END )
                                                    + ( CASE WHEN LEN(LTRIM(RTRIM(ISNULL(A.Brand_id, '')))) <> 0
                                                             THEN LTRIM(RTRIM(A.Brand_id))
                                                             ELSE ''
                                                        END )
                                                    AS VehicleDescription ,
                                                    A.CC AS CCKendaraan
                                          FROM      dbo.mst_vehicle A WITH ( NOLOCK ) ,
                                                    dbo.mst_vehicle_brand B WITH ( NOLOCK ) ,
                                                    dbo.mst_vehicle_model C WITH ( NOLOCK ) ,
                                                    dbo.dtl_ins_factor D WITH ( NOLOCK )
                                          WHERE     A.Brand_id = B.Brand_id
                                                    AND A.Model = C.Model_id
                                                    AND A.vehicle_type = D.insurance_code
                                                    AND D.factor_code = 'VHCTYP'
                                                    AND A.Status = 1
                                        ) AS X
                                        where X.VehicleCode LIKE @0 + '%' ";
            using (var db = GetAABDB())
            {
                return db.Fetch<dynamic>(query, vehicleCode);
            }
        }

        public List<dynamic> GetMappingSalesmanProduct(string productCode, string MouID)
        {
            string query = @"SELECT  Comm_Id AS CommissionID,
		                            Product_Code AS ProductCode,
                                    Pay_To_Party  AS PayToParty
                            FROM    dbo.Prd_Commission AS pc ( NOLOCK )
                            WHERE   Product_Code = @0
                                    AND Pay_To_Party IN ( 3, 6, 7, 8 )
                                    AND Status = 1
                           Union ALL
                           SELECT  Commission_Code AS CommissionID,
		                            MOU_ID AS ProductCode,
                                    Pay_To_Party  AS PayToParty
                            FROM    dbo.Mou_Commission AS pc ( NOLOCK )
                            WHERE   Mou_ID = @1
                                    AND Pay_To_Party IN ( 3, 6, 7, 8 )
                                                        ";
            using (var db = GetAABDB())
            {
                return db.Fetch<dynamic>(query, productCode, MouID);
            }
        }

    }

}
