﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.VisualBasic;
using System.Globalization;
using System.Data;
using Otosales.Repository;
using Otosales.Models;

namespace Otosales.Repository
{
    class CustomerRepository : RepositoryBase
    {
        public dynamic SearchCIF(int pageNo, int pageSize, string searchBy, string keyword, string sortOption, string sortDirection, CIFType CIFType, string BizType="")
        {
            string query = string.Empty;            
            using (var db = GetAABDB())
            {
                switch (CIFType)
                {
                    case Models.CIFType.Personal:
                        query = @";
                                    SELECT  *
                                    FROM    ( SELECT    RTRIM(a.Cust_Id) AS Code ,
                                                        RTRIM(a.Name) AS Name ,
                                                        ISNULL(b.id_card, '') AS KTP ,
                                                        ISNULL(CONVERT(VARCHAR, a.Birth_Date, 103), '') AS BirthDate ,
                                                        RTRIM(ISNULL(a.Home_Address, '')) AS HomeAddress ,
                                                        'Customer' AS Type ,
                                                        a.Cust_Type AS CustType
                                              FROM      dbo.Mst_Customer a (NOLOCK)
                                                        INNER JOIN dbo.Mst_Cust_Personal b (NOLOCK) ON b.Cust_Id = a.Cust_Id
					                                    WHERE a.Cust_Type = '1'
                                              UNION
                                              SELECT    RTRIM(a.Prospect_Id) AS Code ,
                                                        RTRIM(a.Name) AS Name ,
                                                        ISNULL(b.id_card, '') AS KTP ,
                                                        ISNULL(CONVERT(VARCHAR, a.Birth_Date, 103), '') AS BirthDate ,
                                                        RTRIM(ISNULL(a.Home_Address, '')) AS HomeAddress ,
                                                        'Prospect' AS Type ,
                                                        a.Prospect_Type AS CustType
                                              FROM      dbo.Mst_Prospect AS a (NOLOCK)
                                                        INNER JOIN dbo.Mst_Prospect_Personal AS b (NOLOCK) ON b.Prospect_Id = a.Prospect_Id
                                              WHERE     ( Cust_Id = ''
                                                          OR Cust_Id IS NULL
                                                        ) AND a.Prospect_Type = '1'
                                            ) AS X
                                    WHERE   CustType = '1'
                                    ";
                        switch (searchBy)
                        {
                            case "Code":
                                query += " AND X.Code = @0 ";
                                break;
                            case "Name":
                                query += " AND X.Name Like @0 + '%' ";
                                break;
                            case "KTP":
                                query += " AND X.KTP = @0 ";
                                break;
                            case "BirthDate":
                                query += " AND X.BirthDate = CONVERT(VARCHAR, @0, 103) ";
                                break;
                        }
                        switch (sortOption)
                        {
                            case "0":
                                query += " Order By X.Code " + sortDirection;
                                break;
                            case "1":
                                query += " Order By X.Name " + sortDirection;
                                break;
                            case "2":
                                query += " Order BY X.KTP " + sortDirection;
                                break;
                            case "3":
                                query += " Order BY X.BirthDate " + sortDirection;
                                break;
                            case "4":
                                query += " Order BY X.HomeAddress " + sortDirection;
                                break;
                            default:
                                query += " Order By X.Code Desc ";
                                break;
                        }
                        break;
                    case Models.CIFType.Corporate:
                        query = @"  SELECT  *
                                    FROM    ( SELECT    RTRIM(a.Cust_Id) AS Code ,
                                                        RTRIM(a.Name) AS Name ,
                                                        b.AdditionalInfoValue AS NPWP ,
                                                        RTRIM(ISNULL(a.Office_Address, '')) AS OfficeAddress ,
					                                    'Customer' AS Type,
					                                    a.Cust_Type AS CustType
                                              FROM      dbo.Mst_Customer a
                                                        LEFT JOIN Mst_Customer_AdditionalInfo b ON a.Cust_Id = b.CustID
                                                                                                  AND b.AdditionalCode = 'NPWN'
                                              UNION
                                              SELECT    RTRIM(a.Prospect_Id) AS Code ,
                                                        RTRIM(a.Name) AS Name ,
                                                        b.AdditionalInfoValue AS NPWP ,
                                                        RTRIM(ISNULL(a.Office_Address, '')) AS OfficeAddress ,
					                                    'Prospect' AS Type,
					                                    a.Prospect_Type AS CustType
                                              FROM      dbo.Mst_Prospect AS a
                                                        LEFT JOIN Mst_Prospect_AdditionalInfo b ON a.Prospect_ID = b.ProspectID
                                                                                                  AND b.AdditionalCode = 'NPWN'
		                                      WHERE     (a.Cust_Id = '' or a.Cust_Id is NULL)
                                            ) AS X WHERE CustType = '2' ";
                        switch (searchBy)
                        {
                            case "Code":
                                query += " AND X.Code = @0 ";
                                break;
                            case "Name":
                                query += " AND X.Name Like @0 + '%' ";
                                break;
                            case "NPWP":
                                query += " AND X.NPWP = @0 ";
                                break;
                        }
                        switch (sortOption)
                        {
                            case "0":
                                query += " Order By X.Code " + sortDirection;
                                break;
                            case "1":
                                query += " Order By X.Name " + sortDirection;
                                break;
                            case "2":
                                query += " Order By X.NPWP " + sortDirection;
                                break;
                            case "3":
                                query += " Order By X.OfficeAddress " + sortDirection;
                                break;
                            default:
                                query += " Order By X.Code Desc ";
                                break;
                        }
                        break;
                    case Models.CIFType.FinancialInstitution:
                        query = @" SELECT * FROM (SELECT  RTRIM(a.Cust_ID) AS Code ,
                                            RTRIM(a.Name) AS Name ,
                                            b.Client_Type AS TYPE ,
                                            CASE WHEN a.Cust_Type = '1' THEN RTRIM(ISNULL(a.Home_Address, ''))
                                                 ELSE RTRIM(ISNULL(a.Office_Address, ''))
                                            END AS Address
                                            {2}
                                    FROM    Mst_Customer a
                                            INNER JOIN Mst_Cust_Type b ON a.Cust_Id = b.Cust_Id
                                            {0}
                                    WHERE   b.Client_Type IN ( 'BANKIN', 'LEASIN' ) {1} ) X Where 1=1 ";
                        switch (searchBy)
                        {
                            case "Code":
                                query = string.Format(query, @" LEFT JOIN dbo.Mst_Cust_Personal c ON c.Cust_Id = a.Cust_Id
                                                                LEFT JOIN Mst_Customer_AdditionalInfo d ON a.Cust_Id = d.CustID AND d.AdditionalCode = 'NPWN'", 
                                                              " AND a.Cust_ID = @0 ",
                                                             @" ,RTRIM(ISNULL(d.AdditionalInfoValue,'')) AS NPWP,
                                                                 RTRIM(ISNULL(c.id_card, '')) AS KTP,
                                                                 ISNULL(FORMAT(a.Birth_Date, 'yyyy/MM/dd'), '') AS BirthDate ");
                                break;
                            case "Name":
                                query = string.Format(query, @" LEFT JOIN dbo.Mst_Cust_Personal c ON c.Cust_Id = a.Cust_Id
                                                                LEFT JOIN Mst_Customer_AdditionalInfo d ON a.Cust_Id = d.CustID AND d.AdditionalCode = 'NPWN'",
                                                              " AND a.Name LIKE @0 + '%' ",
                                                             @" ,RTRIM(ISNULL(d.AdditionalInfoValue,'')) AS NPWP,
                                                                 RTRIM(ISNULL(c.id_card, '')) AS KTP,
                                                                 ISNULL(FORMAT(a.Birth_Date, 'yyyy/MM/dd'), '') AS BirthDate ");
                                break;
                            case "NPWP":
                                query = string.Format(query, @" LEFT JOIN Mst_Customer_AdditionalInfo c ON a.Cust_Id = c.CustID
                                                                                       AND c.AdditionalCode = 'NPWN' ", 
                                                              " AND a.Cust_Type = '2' AND c.AdditionalInfoValue = @0 ",
                                                              " ,RTRIM(ISNULL(c.AdditionalInfoValue,'')) AS NPWP ");
                                break;
                            case "KTP":
                                query = string.Format(query, @" INNER JOIN dbo.Mst_Cust_Personal c ON c.Cust_Id = a.Cust_Id ", 
                                                              " AND a.Cust_Type = '1' AND c.id_card = @0 " ,
                                                             @" ,RTRIM(ISNULL(c.id_card, '')) AS KTP,
                                                                ISNULL(FORMAT(a.Birth_Date, 'yyyy/MM/dd'), '') AS BirthDate ");
                                break;
                            case "BirthDate":
                                query = string.Format(query,  " INNER JOIN dbo.Mst_Cust_Personal c ON c.Cust_Id = a.Cust_Id ", 
                                                              " AND a.Cust_Type = '1' AND a.Birth_Date = CONVERT(DATE, @0, 103) ",
                                                             @" ,RTRIM(ISNULL(c.id_card, '')) AS KTP,
                                                                ISNULL(FORMAT(a.Birth_Date, 'yyyy/MM/dd'), '') AS BirthDate ");
                                break;
                        }
                        switch (sortOption)
                        {
                            case "0":
                                query += " Order By X.Code " + sortDirection;
                                break;
                            case "1":
                                query += " Order By X.Name " + sortDirection;
                                break;
                            case "2":
                                query += " Order By X.Address " + sortDirection;
                                break;
                            default:
                                query += " Order By X.Code Desc";
                                break;
                        }
                        break;
                    case Models.CIFType.Payer:
                        query = @" SELECT * FROM ( SELECT  RTRIM(a.Cust_ID) AS Code ,
                                            RTRIM(a.Name) AS Name ,
                                            a.Cust_Type AS TYPE ,
                                            CASE WHEN a.Cust_Type = 1 THEN RTRIM(ISNULL(a.Home_Address, ''))
                                                 ELSE RTRIM(ISNULL(a.Office_Address, ''))
                                            END AS Address
                                            {2}
                                    FROM    dbo.Mst_Customer a
                                            {0}
                                    WHERE   1=1 {1}) X Where 1=1 ";
                        switch (searchBy)
                        {
                            case "Code":
                                query = string.Format(query, @" LEFT JOIN dbo.Mst_Cust_Personal c ON c.Cust_Id = a.Cust_Id
                                                                LEFT JOIN Mst_Customer_AdditionalInfo d ON a.Cust_Id = d.CustID AND d.AdditionalCode = 'NPWN'",
                                                              " AND a.Cust_ID = @0 ",
                                                             @" ,RTRIM(ISNULL(d.AdditionalInfoValue,'')) AS NPWP,
                                                                 RTRIM(ISNULL(c.id_card, '')) AS KTP,
                                                                 ISNULL(FORMAT(a.Birth_Date, 'yyyy/MM/dd'), '') AS BirthDate ");
                                break;
                            case "Name":
                                query = string.Format(query, @" LEFT JOIN dbo.Mst_Cust_Personal c ON c.Cust_Id = a.Cust_Id
                                                                LEFT JOIN Mst_Customer_AdditionalInfo d ON a.Cust_Id = d.CustID AND d.AdditionalCode = 'NPWN'",
                                                              " AND a.Name = @0 ",
                                                             @" ,RTRIM(ISNULL(d.AdditionalInfoValue,'')) AS NPWP,
                                                                 RTRIM(ISNULL(c.id_card, '')) AS KTP,
                                                                 ISNULL(FORMAT(a.Birth_Date, 'yyyy/MM/dd'), '') AS BirthDate ");
                                break;
                            case "NPWP":
                                query = string.Format(query, @" LEFT JOIN Mst_Customer_AdditionalInfo c ON a.Cust_Id = c.CustID
                                                                                       AND c.AdditionalCode = 'NPWN' ",
                                                              " AND a.Cust_Type = '2' AND c.AdditionalInfoValue = @0 ",
                                                              " ,RTRIM(ISNULL(c.AdditionalInfoValue,'')) AS NPWP ");
                                break;
                            case "KTP":
                                query = string.Format(query, @" INNER JOIN dbo.Mst_Cust_Personal c ON c.Cust_Id = a.Cust_Id ",
                                                              " AND a.Cust_Type = '1' AND c.id_card = @0 ",
                                                             @" ,RTRIM(ISNULL(c.id_card, '')) AS KTP,
                                                                ISNULL(FORMAT(a.Birth_Date, 'yyyy/MM/dd'), '') AS BirthDate ");
                                break;
                            case "BirthDate":
                                query = string.Format(query, " INNER JOIN dbo.Mst_Cust_Personal c ON c.Cust_Id = a.Cust_Id ",
                                                              " AND a.Cust_Type = '1' AND a.Birth_Date = CONVERT(DATE, @0, 103) ",
                                                             @" ,RTRIM(ISNULL(c.id_card, '')) AS KTP,
                                                                ISNULL(FORMAT(a.Birth_Date, 'yyyy/MM/dd'), '') AS BirthDate ");
                                break;
                        }
                        switch (sortOption)
                        {
                            case "0":
                                query += " Order By X.Code " + sortDirection;
                                break;
                            case "1":
                                query += " Order By X.Name " + sortDirection;
                                break;
                            case "2":
                                query += " Order By X.Address " + sortDirection;
                                break;
                            default:
                                query += " Order By X.Code Desc ";
                                break;
                        }
                        break;
                    case Models.CIFType.Salesman:
                        query = @"SELECT RTRIM(a.Salesman_ID) AS Code ,
                                    RTRIM(a.Name) AS Name, 
                                    'SALESMAN' AS Type,
                                    a.Status AS Status 
                                     FROM MST_SALESMAN A 
                                     LEFT OUTER JOIN MST_Department C ON A.Dept_Id=C.Dept_Id,
                                     mst_branch_area B,
                                     Mst_Branch D WHERE A.STATUS=1 
                                     AND A.BRANCH_ID=B.BRANCH_ID 
                                     AND A.BRANCH_ID=D.BRANCH_ID 
                                     AND D.biz_type= @0 ";
                        switch (searchBy)
                        {
                            case "Code":
                                query += " AND a.Salesman_ID = @1 ";
                                break;
                            case "Name":
                                query += " AND a.Name Like @1 + '%' ";
                                break;
                        }
                        switch (sortOption)
                        {
                            case "0":
                                query += " Order By a.Salesman_ID " + sortDirection;
                                break;
                            case "1":
                                query += " Order By a.Name " + sortDirection;
                                break;
                            default:
                                query += " Order By a.Salesman_ID Desc ";
                                break;
                        }
                        break;
                    case Models.CIFType.Reinsrurer:
                        query = @" SELECT RTRIM(a.Cust_ID) AS Code ,
                                    RTRIM(a.Name) AS Name ,
                                    b.Client_Type AS Type
                                 FROM   Mst_Customer a
                                        INNER JOIN Mst_Cust_Type b ON a.Cust_Id = b.Cust_Id 
                                 WHERE b.Client_Type = 'REINSR' ";
                        switch (searchBy)
                        {
                            case "Code":
                                query += " AND a.Cust_Id = @0 ";
                                break;
                            case "Name":
                                query += " AND a.Name Like @0 + '%' ";
                                break;
                        }
                        switch (sortOption)
                        {
                            case "0":
                                query += " Order By a.Cust_Id " + sortDirection;
                                break;
                            case "1":
                                query += " Order By a.Name " + sortDirection;
                                break;
                            default:
                                query += " Order By a.Cust_Id Desc ";
                                break;
                        }
                        break;
                    case Models.CIFType.SalesmanDealer:
                        query = @" SELECT  RTRIM(a.Cust_Id) AS Code ,
                                        RTRIM(a.Name) AS Name ,
                                        b.Client_Type AS ClientType
                                FROM    dbo.Mst_Customer AS a
                                        INNER JOIN Mst_Cust_Type b ON a.Cust_Id = b.Cust_Id
                                        INNER JOIN dbo.ComSales_Salesman c ON c.Cust_ID = a.Cust_Id
                                WHERE   b.Client_Type IN ( 'DEALER', 'BROKER' )
                                        AND c.Salesman_Level = 'DSL' ";
                        switch (searchBy)
                        {
                            case "Code":
                                query += " AND a.Cust_Id =@0 ";
                                break;
                            case "Name":
                                query += " AND a.Name Like @0 + '%' ";
                                break;
                            case "Client_Type":
                                query += " AND b.Client_Type IN (@0) ";
                                break;
                        }
                        switch (sortOption)
                        {
                            case "0":
                                query += " Order By a.Cust_Id " + sortDirection;
                                break;
                            case "1":
                                query += " Order By a.Name " + sortDirection;
                                break;
                            case "2":
                                query += " Order By b.Client_Type " + sortDirection;
                                break;
                            default:
                                query += " Order By a.Cust_Id Desc ";
                                break;
                        }
                        break;
                    case Models.CIFType.Dealer:
                        query = @" SELECT RTRIM(a.Cust_Id) AS Code,
                                    RTRIM(a.Name) AS Name,
                                    b.Client_Type AS ClientType
                                    FROM dbo.Mst_Customer AS a 
                                    INNER JOIN Mst_Cust_Type b ON a.Cust_Id = b.Cust_Id
                                    WHERE b.Client_Type IN ('DEALER', 'BROKER') ";
                        switch (searchBy)
                        {
                            case "Code":
                                query += " AND a.Cust_Id =@0 ";
                                break;
                            case "Name":
                                query += " AND a.Name Like @0 + '%' ";
                                break;
                            case "Client_Type":
                                query += " AND b.Client_Type IN (@0) ";
                                break;
                        }
                        switch (sortOption)
                        {
                            case "0":
                                query += " Order By a.Cust_Id " + sortDirection;
                                break;
                            case "1":
                                query += " Order By a.Name " + sortDirection;
                                break;
                            case "2":
                                query += " Order By b.Client_Type " + sortDirection;
                                break;
                            default:
                                query += " Order By a.Cust_Id Desc ";
                                break;
                        }
                        break;
                    case Models.CIFType.TitanAgent:
                        query = @" SELECT    RTRIM(a.Client_Code) AS Code ,
                                        RTRIM(b.Name) AS Name,
                                        a.Client_Type AS ClientType
                                FROM      dtl_cust_type a WITH ( NOLOCK )
                                        INNER JOIN mst_customer b WITH ( NOLOCK ) ON a.cust_id = b.cust_id                                                        
                                WHERE     a.Client_Type = 'AGENT' ";
                        switch (searchBy)
                        {
                            case "Code":
                                query += " AND a.Client_Code = @0 ";
                                break;
                            case "Name":
                                query += " AND b.Name Like @0 + '%' ";
                                break;
                        }
                        switch (sortOption)
                        {
                            case "0":
                                query += " Order By a.Client_Code " + sortDirection;
                                break;
                            case "1":
                                query += " Order By b.Name " + sortDirection;
                                break;
                            default:
                                query += " Order By a.Client_Code Desc ";
                                break;
                        }
                        break;
                    case Models.CIFType.PolicyHolder:
                        query = @" SELECT * FROM (SELECT  RTRIM(a.Cust_Id) AS Code ,
                                            RTRIM(a.Name) AS Name ,
                                            a.Cust_Type AS Type ,
                                            CASE WHEN a.Cust_Type = '1' THEN RTRIM(ISNULL(a.Home_Address, ''))
                                                 ELSE RTRIM(ISNULL(a.Office_Address, ''))
                                            END AS Address
                                    {2}
                                    FROM    dbo.Mst_Customer a 
                                    {0} 
                                    where 1 = 1 {1} 
                                    ) X
                                    WHERE   1 = 1  ";
                        switch (searchBy)
                        {
                            case "Code":
                                query = string.Format(query, @" LEFT JOIN dbo.Mst_Cust_Personal c ON c.Cust_Id = a.Cust_Id
                                                                LEFT JOIN Mst_Customer_AdditionalInfo d ON a.Cust_Id = d.CustID AND d.AdditionalCode = 'NPWN'",
                                                              " AND a.Cust_ID = @0 ",
                                                             @" ,RTRIM(ISNULL(d.AdditionalInfoValue,'')) AS NPWP,
                                                                 RTRIM(ISNULL(c.id_card, '')) AS KTP,
                                                                 ISNULL(FORMAT(a.Birth_Date, 'yyyy/MM/dd'), '') AS BirthDate ");
                                break;
                            case "Name":
                                query = string.Format(query, @" LEFT JOIN dbo.Mst_Cust_Personal c ON c.Cust_Id = a.Cust_Id
                                                                LEFT JOIN Mst_Customer_AdditionalInfo d ON a.Cust_Id = d.CustID AND d.AdditionalCode = 'NPWN'",
                                                              " AND a.Name LIKE  @0 + '%'  ",
                                                             @" ,RTRIM(ISNULL(d.AdditionalInfoValue,'')) AS NPWP,
                                                                 RTRIM(ISNULL(c.id_card, '')) AS KTP,
                                                                 ISNULL(FORMAT(a.Birth_Date, 'yyyy/MM/dd'), '') AS BirthDate ");
                                break;
                            case "NPWP":
                                query = string.Format(query, @" LEFT JOIN Mst_Customer_AdditionalInfo c ON a.Cust_Id = c.CustID
                                                                                       AND c.AdditionalCode = 'NPWN' ",
                                                              " AND a.Cust_Type = '2' AND c.AdditionalInfoValue = @0 ",
                                                              " ,RTRIM(ISNULL(c.AdditionalInfoValue,'')) AS NPWP ");
                                break;
                            case "KTP":
                                query = string.Format(query, @" INNER JOIN dbo.Mst_Cust_Personal c ON c.Cust_Id = a.Cust_Id ",
                                                              " AND a.Cust_Type = '1' AND c.id_card = @0 ",
                                                             @" ,RTRIM(ISNULL(c.id_card, '')) AS KTP,
                                                                ISNULL(FORMAT(a.Birth_Date, 'yyyy/MM/dd'), '') AS BirthDate ");
                                break;
                            case "BirthDate":
                                query = string.Format(query, " INNER JOIN dbo.Mst_Cust_Personal c ON c.Cust_Id = a.Cust_Id ",
                                                              " AND a.Cust_Type = '1' AND a.Birth_Date = CONVERT(DATE, @0, 103) ",
                                                             @" ,RTRIM(ISNULL(c.id_card, '')) AS KTP,
                                                                ISNULL(FORMAT(a.Birth_Date, 'yyyy/MM/dd'), '') AS BirthDate ");
                                break;
                        }
                        switch (sortOption)
                        {
                            case "0":
                                query += " Order By X.Code " + sortDirection;
                                break;
                            case "1":
                                query += " Order By X.Name " + sortDirection;
                                break;
                            case "3":
                                query += " Order By X.Address " + sortDirection;
                                break;
                            default:
                                query += " Order By X.Code Desc ";
                                break;
                        }
                        break;
                }
                dynamic result = null;
                if (CIFType == Models.CIFType.SalesmanDealer)
                {
                    #region 0140URF2018 - sprint 8 - next sprint di rollback
                    //dynamic result = null;

                    //string qBeyond = @"SELECT    RTRIM(cname.CustomerCode) AS CustomerID 
                    //                    FROM      BeyondDB.General.HierarchyCustomerDealer ghcd
                    //                            INNER JOIN BeyondDB.General.Customer cname ON cname.CustomerID = ghcd.CustomerID
                    //                            INNER JOIN BeyondDB.General.Customer dname ON dname.CustomerID = ghcd.DealerID
                    //                            INNER JOIN BeyondDB.General.Customer spvname ON spvname.CustomerID = ghcd.SupervisorID
                    //                            INNER JOIN BeyondDB.General.Customer bhname ON bhname.CustomerID = ghcd.BranchHeadID
                    //                    WHERE     ghcd.RowStatus = 0 AND cname.CustomerCode IN (SELECT  value FROM    PolicyInsurance.fn_Split(@0, ','))";
                    //string qAAB = @"SELECT    RTRIM(a.Cust_ID) AS Code ,
                    //                    RTRIM(a.Name) AS Name ,
                    //                    b.Client_Type AS CustomerType
                    //            FROM      Mst_Customer a
                    //                    INNER JOIN Mst_Cust_Type b ON a.Cust_Id = b.Cust_Id
                    //            WHERE   b.Client_Type IN ( 'BROKER', 'DEALER' ) AND a.Cust_Id IN (
                    //                    SELECT  part
                    //                    FROM    dbo.Fn_SplitString(RTRIM(LTRIM(@0)),
                    //                                               ',') )";
                    //var SDAAB = db.Query<dynamic>(query, keyword).ToList();
                    //List<string> CustIDAAB = new List<string>();
                    //List<string> ValidCustID = new List<string>();
                    //foreach (var sdAAB in SDAAB)
                    //{
                    //    if (!string.IsNullOrWhiteSpace(sdAAB.Code)) 
                    //    {
                    //        if (sdAAB.CustomerType.Trim().ToUpper() == "DEALER")
                    //        {
                    //            CustIDAAB.Add(sdAAB.Code.Trim());
                    //        }
                    //        else {
                    //            ValidCustID.Add(sdAAB.Code.Trim());
                    //        }
                    //    }
                    //};
                    //if (CustIDAAB.Count > 0)
                    //{
                    //    string strCustIDAAB = string.Join(",", CustIDAAB);
                    //    using (var db2 = Geta2isBeyondDB())
                    //    {
                    //        List<string> DealerCustID = db2.Query<string>(qBeyond, strCustIDAAB).ToList();
                    //        ValidCustID.AddRange(DealerCustID);
                    //        //return db2.Page<dynamic>(pageNo, pageSize, qBeyond, strCustIDAAB);
                    //    }
                    //}
                    //if (ValidCustID.Count > 0)
                    //{
                    //    string strValidCustID = string.Join(",", ValidCustID);
                    //    using (var db2 = GetAABDB())
                    //    {                            
                    //        result= db2.Page<dynamic>(pageNo, pageSize, qAAB, strValidCustID);
                    //    }
                    //}
                    //return result;
                    #endregion
                    return result = db.Page<dynamic>(pageNo, pageSize, query, keyword);
                }
                if (CIFType == Models.CIFType.Salesman) {
                    return result = db.Page<dynamic>(pageNo, pageSize, query, BizType, keyword);
                }
                else
                {
                    return result = db.Page<dynamic>(pageNo, pageSize, query, keyword);
                }
            }
        }
        public dynamic GetCustomerType(string CustomerID)
        {
            string query = @"Select A.CUST_ID AS CustomerID,
                                    RTRIM(A.Name) AS CustomerName ,
                                    A.Cust_Type AS CustomerType                                    
                                    from dbo.MST_CUSTOMER A (nolock) 
                                    Where A.CUST_ID=@0 ";
            using (var db = GetAABDB())
            {               
                return db.Query<dynamic>(query, CustomerID).FirstOrDefault();
            }
        }

        public dynamic GetCustomerClientType(string CustomerID)
        {
            string query = @"SELECT  RTRIM(a.Cust_ID) AS CustomerID ,
                                            RTRIM(a.Name) AS Name ,
                                            b.Client_Type AS ClientType
                                    FROM    Mst_Customer a
                                            INNER JOIN Mst_Cust_Type b ON a.Cust_Id = b.Cust_Id
                                    WHERE   b.Client_Type IN ( 'BROKER', 'DEALER' )
                                            AND a.Cust_Id =@0";
            using (var db = GetAABDB())
            {
                return db.Query<dynamic>(query, CustomerID).FirstOrDefault();
            }
        }

        public dynamic GetProspectType(string ProspectID)
        {
            string query = @"Select A.Prospect_ID AS ProspectID,
                                    A.Prospect_Type AS ProspectType
                                    from dbo.MST_Prospect A (nolock) 
                                    Where A.Prospect_id=@0 ";
            using (var db = GetAABDB())
            {
                return db.Query<dynamic>(query, ProspectID).FirstOrDefault();
            }
        }

        public dynamic GetPersonalCustomer(string customerID)
        {
            dynamic result = null;
            string query = @"SELECT  RTRIM(a.Cust_Id) AS CustomerID ,
                                    RTRIM(ISNULL(a.prospect_Id, '')) AS ProspectID ,
                                    RTRIM(ISNULL(c.id_card, '')) AS IDNumber ,
                                    ISNULL(( SELECT RTRIM(COALESCE(AdditionalInfoValue, ''))
                                             FROM   dbo.Mst_Customer_AdditionalInfo
                                             WHERE  CustID = a.Cust_Id
                                                    AND AdditionalCode = 'IDCT'
                                           ), '') AS IDType ,
                                    RTRIM(ISNULL(a.Cust_Type, '')) AS CustomerType ,
                                    RTRIM(ISNULL(a.[Name], '')) AS CustomerName ,
                                    ISNULL(FORMAT(a.Birth_Date, 'yyyy/MM/dd'), '') AS BirthDate ,
                                    CASE WHEN c.Sex = 'M' THEN 1
                                         WHEN c.Sex = 'F' THEN 2
                                         ELSE RTRIM(ISNULL(c.Sex, ''))
                                    END AS Gender ,
                                    RTRIM(ISNULL(c.religion, '')) AS Religion ,
                                    RTRIM(ISNULL(a.Home_Address, '')) AS HomeAddress ,
                                    RTRIM(ISNULL(a.Home_PostCode, '')) AS HomePostalCode ,
                                    COALESCE(( SELECT   RTRIM(a.Home_PostCode) + ' - ' + p.Description
                                                        + ', ' + p1.Description + ', ' + p2.Description
                                                        + ', ' + p3.Description
                                               FROM     dbo.Mst_Postal p
                                                        LEFT JOIN dbo.Mst_General p1 ON p.City = p1.code
                                                                                        AND p1.type = 'CTY'
                                                        LEFT JOIN dbo.Mst_General p2 ON p.Province = p2.code
                                                                                        AND p2.type = 'PRV'
                                                        LEFT JOIN dbo.Mst_General p3 ON p.Country = p3.code
                                                                                        AND p3.type = 'CON'
                                               WHERE    p.PostCode = a.Home_PostCode
                                             ), a.Home_PostCode) AS HomePostalCodeDesc ,
                                    RTRIM(ISNULL(a.Home_Rt, '')) AS HomeRT ,
                                    RTRIM(ISNULL(a.Home_Rw, '')) AS HomeRW ,
                                    RTRIM(ISNULL(a.Home_Fax1, '')) AS HomeFax1 ,
                                    RTRIM(ISNULL(a.Home_Fax2, '')) AS HomeFax2 ,
                                    ISNULL(( SELECT RTRIM(COALESCE(AdditionalInfoValue, ''))
                                             FROM   dbo.Mst_Customer_AdditionalInfo
                                             WHERE  CustID = a.Cust_Id
                                                    AND AdditionalCode = 'INCM'
                                           ), '') AS Income ,
                                    CASE WHEN c.Marital = 'S' THEN 1
                                         WHEN c.Marital = 'M' THEN 2
                                         WHEN c.Marital = 'W' THEN 3
                                         ELSE RTRIM(ISNULL(c.Marital, ''))
                                    END AS MaritalStatus ,
                                    RTRIM(ISNULL(c.Job_Title, '')) AS JobTitle ,
                                    RTRIM(ISNULL(c.Company_Name, '')) AS CompanyName ,
                                    RTRIM(ISNULL(a.Email, '')) AS Email ,
                                    RTRIM(ISNULL(c.HP, '')) AS MobilePhone1 ,
                                    RTRIM(ISNULL(c.HP_2, '')) AS MobilePhone2 ,
                                    RTRIM(ISNULL(c.HP_3, '')) AS MobilePhone3 ,
                                    RTRIM(ISNULL(c.HP_4, '')) AS MobilePhone4 ,
                                    RTRIM(ISNULL(a.Home_Phone1, '')) AS HomePhone1 ,
                                    RTRIM(ISNULL(a.Home_Phone2, '')) AS HomePhone2 ,
                                    RTRIM(ISNULL(a.Office_Phone1, '')) AS OfficePhone1 ,
                                    RTRIM(ISNULL(a.Office_Ext1, '')) AS OfficeExt1 ,
                                    RTRIM(ISNULL(a.Office_Address, '')) AS OfficeAddress ,
                                    RTRIM(ISNULL(a.Office_PostCode, '')) AS OfficePostalCode ,
                                    COALESCE(( SELECT   RTRIM(a.Office_PostCode) + ' - ' + p.Description
                                                        + ', ' + p1.Description + ', ' + p2.Description
                                                        + ', ' + p3.Description
                                               FROM     dbo.Mst_Postal p
                                                        LEFT JOIN dbo.Mst_General p1 ON p.City = p1.code
                                                                                        AND p1.type = 'CTY'
                                                        LEFT JOIN dbo.Mst_General p2 ON p.Province = p2.code
                                                                                        AND p2.type = 'PRV'
                                                        LEFT JOIN dbo.Mst_General p3 ON p.Country = p3.code
                                                                                        AND p3.type = 'CON'
                                               WHERE    p.PostCode = a.Office_PostCode
                                             ), a.Office_PostCode) AS OfficePostalCodeDesc ,
                                    ISNULL(b.Status, '') AS PreferredCustomerStatus ,
                                    CASE WHEN ISNULL(( SELECT   Cust_ID
                                                       FROM     dbo.Mst_Customer_Category
                                                       WHERE    Cust_ID = a.Cust_Id
                                                                AND Category_Code = 'VIP'
                                                     ), '') = '' THEN 0
                                         ELSE 1
                                    END AS IsVIP ,
                                    ISNULL(( SELECT RTRIM(COALESCE(AdditionalInfoValue, ''))
                                             FROM   dbo.Mst_Customer_AdditionalInfo
                                             WHERE  CustID = a.Cust_Id
                                                    AND AdditionalCode = 'FCBK'
                                           ), '') AS Facebook ,
                                    ISNULL(( SELECT RTRIM(COALESCE(AdditionalInfoValue, ''))
                                             FROM   dbo.Mst_Customer_AdditionalInfo
                                             WHERE  CustID = a.Cust_Id
                                                    AND AdditionalCode = 'TWTR'
                                           ), '') AS Twitter ,
                                    ISNULL(( SELECT RTRIM(COALESCE(AdditionalInfoValue, ''))
                                             FROM   dbo.Mst_Customer_AdditionalInfo
                                             WHERE  CustID = a.Cust_Id
                                                    AND AdditionalCode = 'INTG'
                                           ), '') AS Instagram
                            FROM    dbo.Mst_Customer a
                                    INNER JOIN dbo.Mst_Cust_Personal c ON a.Cust_Id = c.Cust_Id
                                    LEFT JOIN dbo.Mst_Preferred_Customer b ON a.Cust_Id = b.Cust_Id
                            WHERE   a.Cust_Type = 1
                                    AND A.Cust_Id = @0";
            using (var db = GetAABDB())
            {
                var a = db.Fetch<dynamic>(query, customerID).ToList();
                if (a.Count > 0)
                    result = a[0];
            }
            return result;
        }

        public dynamic GetCorporateCustomer(string customerID)
        {
            dynamic result = null;
            string query = @"SELECT  RTRIM(COALESCE(a.Cust_Id, '')) AS CustomerID ,
                            RTRIM(COALESCE(a.prospect_Id, '')) AS ProspectID ,
                            RTRIM(COALESCE(a.Cust_Type, '')) AS CustomerType ,
                            RTRIM(COALESCE(a.[NAME], '')) AS CustomerName ,
                            ISNULL(( SELECT RTRIM(COALESCE(AdditionalInfoValue, ''))
                                     FROM   dbo.Mst_Customer_AdditionalInfo
                                     WHERE  CustID = a.Cust_Id
                                            AND AdditionalCode = 'NOWP'
                                   ), '') AS NameOnNPWP ,
                            ISNULL(( SELECT RTRIM(COALESCE(AdditionalInfoValue, ''))
                                     FROM   dbo.Mst_Customer_AdditionalInfo
                                     WHERE  CustID = a.Cust_Id
                                            AND AdditionalCode = 'NPWN'
                                   ), '') AS NPWPNo ,
                            ISNULL(( SELECT RTRIM(COALESCE(AdditionalInfoValue, ''))
                                     FROM   dbo.Mst_Customer_AdditionalInfo
                                     WHERE  CustID = a.Cust_Id
                                            AND AdditionalCode = 'NPDT'
                                   ), '') AS NPWPDate ,
                            ISNULL(( SELECT RTRIM(COALESCE(AdditionalInfoValue, ''))
                                     FROM   dbo.Mst_Customer_AdditionalInfo
                                     WHERE  CustID = a.Cust_Id
                                            AND AdditionalCode = 'NPAD'
                                   ), '') AS NPWPAddress ,
                            ISNULL(( SELECT RTRIM(COALESCE(AdditionalInfoValue, ''))
                                     FROM   dbo.Mst_Customer_AdditionalInfo
                                     WHERE  CustID = a.Cust_Id
                                            AND AdditionalCode = 'SINR'
                                   ), '') AS SIUPNo ,
                            ISNULL(( SELECT RTRIM(COALESCE(AdditionalInfoValue, ''))
                                     FROM   dbo.Mst_Customer_AdditionalInfo
                                     WHERE  CustID = a.Cust_Id
                                            AND AdditionalCode = 'PKPN'
                                   ), '') AS PKPNo ,
                            ISNULL(( SELECT RTRIM(COALESCE(AdditionalInfoValue, ''))
                                     FROM   dbo.Mst_Customer_AdditionalInfo
                                     WHERE  CustID = a.Cust_Id
                                            AND AdditionalCode = 'PKPD'
                                   ), '') AS PKPDate ,
                            RTRIM(COALESCE(f.Business_Type, '')) AS BusinessType ,
                            RTRIM(COALESCE(b.Client_Type, '')) AS ClientType ,
                            RTRIM(COALESCE(a.Office_Address, '')) AS OfficeAddress ,
                            RTRIM(COALESCE(a.Office_PostCode, '')) AS OfficePostalCode ,
                            COALESCE(( SELECT    RTRIM(a.Office_PostCode) + ' - ' + p.Description + ', '
                                        + p1.Description + ', ' + p2.Description + ', '
                                        + p3.Description
                              FROM      dbo.Mst_Postal p
                                        LEFT JOIN dbo.Mst_General p1 ON p.City = p1.code
                                                                        AND p1.type = 'CTY'
                                        LEFT JOIN dbo.Mst_General p2 ON p.Province = p2.code
                                                                        AND p2.type = 'PRV'
                                        LEFT JOIN dbo.Mst_General p3 ON p.Country = p3.code
                                                                        AND p3.type = 'CON'
                              WHERE     p.PostCode = a.Office_PostCode
                            ),RTRIM(a.Office_PostCode)) AS OfficePostalCodeDesc ,
                            RTRIM(COALESCE(a.Office_Phone1, '')) AS OfficePhone1 ,
                            RTRIM(COALESCE(a.Office_Phone2, '')) AS OfficePhone2 ,
                            RTRIM(COALESCE(a.Office_Ext1, '')) AS OfficePhoneExt ,
                            RTRIM(COALESCE(a.Office_Rt, '')) AS OfficeRT ,
                            RTRIM(COALESCE(a.Office_Rw, '')) AS OfficeRW ,
                            RTRIM(COALESCE(a.Office_Fax1, '')) AS OfficeFax1 ,
                            RTRIM(COALESCE(a.Office_Fax2, '')) AS OfficeFax2 ,
                            RTRIM(COALESCE(c.NAME, '')) AS PIC ,
                            RTRIM(COALESCE(a.Email, '')) AS Email ,
                            ISNULL(( SELECT RTRIM(COALESCE(AdditionalInfoValue, ''))
                                     FROM   dbo.Mst_Customer_AdditionalInfo
                                     WHERE  CustID = a.Cust_Id
                                            AND AdditionalCode = 'MPC1'
                                   ), '') AS MobilePhone1 ,
                            ISNULL(( SELECT RTRIM(COALESCE(AdditionalInfoValue, ''))
                                     FROM   dbo.Mst_Customer_AdditionalInfo
                                     WHERE  CustID = a.Cust_Id
                                            AND AdditionalCode = 'MPC2'
                                   ), '') AS MobilePhone2 ,
                            RTRIM(COALESCE(a.Home_Address, '')) AS HomeAddress ,
                            RTRIM(COALESCE(a.Home_PostCode, '')) AS HomePostalCode ,
                            COALESCE(( SELECT    RTRIM(a.Home_PostCode) + ' - ' + p.Description + ', '
                                        + p1.Description + ', ' + p2.Description + ', '
                                        + p3.Description
                              FROM      dbo.Mst_Postal p
                                        LEFT JOIN dbo.Mst_General p1 ON p.City = p1.code
                                                                        AND p1.type = 'CTY'
                                        LEFT JOIN dbo.Mst_General p2 ON p.Province = p2.code
                                                                        AND p2.type = 'PRV'
                                        LEFT JOIN dbo.Mst_General p3 ON p.Country = p3.code
                                                                        AND p3.type = 'CON'
                              WHERE     p.PostCode = a.Home_PostCode
                            ),RTRIM(a.Office_PostCode)) AS HomePostalCodeDesc ,
                            RTRIM(COALESCE(a.Home_Phone1, '')) AS HomePhone1 ,
                            RTRIM(COALESCE(a.Home_Phone1, '')) AS HomePhone2 ,
                            CASE WHEN ISNULL(( SELECT   Cust_ID
                                               FROM     dbo.Mst_Customer_Category
                                               WHERE    Cust_ID = a.Cust_Id
                                                        AND Category_Code = 'VIP'
                                             ), '') = '' THEN 0
                                 ELSE 1
                            END AS IsVIP ,
                            RTRIM(ISNULL(a.Cust_Group, '')) AS CompanyGroup,
                            ISNULL(( SELECT RTRIM(COALESCE(AdditionalInfoValue, ''))
                                     FROM   dbo.Mst_Customer_AdditionalInfo
                                     WHERE  CustID = a.Cust_Id
                                            AND AdditionalCode = 'ASOW'
                                   ), '') AS AssetOwner
                    FROM    dbo.Mst_Customer a
                            LEFT JOIN Mst_Cust_Corporate f ON a.Cust_Id = f.cust_id
		                    LEFT JOIN mst_cust_type b ON a.Cust_Id = b.Cust_Id
                            LEFT JOIN Mst_Cust_Pic c ON a.Cust_Id = c.Cust_id        
                    WHERE   a.Cust_Type = 2
                            AND A.CUST_ID = @0";

            using (var db = GetAABDB())
            {
                var a = db.Fetch<dynamic>(query, customerID).ToList();
                if (a.Count > 0)
                    result = a[0];
            }
            return result;
        }

        public dynamic GetPersonalProspect(string ProspectID)
        {
            dynamic result = null;
            string query = @"SELECT  RTRIM(a.Prospect_Id) AS ProspectID ,
                                        RTRIM(b.id_card) AS IDNumber ,
                                        ISNULL(( SELECT RTRIM(COALESCE(AdditionalInfoValue, ''))
                                                 FROM   dbo.Mst_Prospect_AdditionalInfo
                                                 WHERE  ProspectID = a.Prospect_Id
                                                        AND AdditionalCode = 'IDCT'
                                               ), '') AS IDType ,
                                        Prospect_Type AS ProspectType ,
                                        RTRIM(a.Name) AS ProspectName ,
                                        ISNULL(FORMAT(a.Birth_Date, 'yyyy/MM/dd'), '') AS BirthDate ,
                                        b.Sex AS Gender ,
                                        b.Religion AS Religion ,
                                        RTRIM(a.Home_Address) AS HomeAddress ,
                                        RTRIM(a.Home_PostCode) AS HomePostalCode ,
                                        ( SELECT    RTRIM(a.Home_PostCode) + ' - ' + p.Description + ', '
                                                    + p1.Description + ', ' + p2.Description + ', '
                                                    + p3.Description
                                          FROM      dbo.Mst_Postal p
                                                    LEFT JOIN dbo.Mst_General p1 ON p.City = p1.code
                                                                                    AND p1.type = 'CTY'
                                                    LEFT JOIN dbo.Mst_General p2 ON p.Province = p2.code
                                                                                    AND p2.type = 'PRV'
                                                    LEFT JOIN dbo.Mst_General p3 ON p.Country = p3.code
                                                                                    AND p3.type = 'CON'
                                          WHERE     p.PostCode = a.Home_PostCode
                                        ) AS HomePostalCodeDesc ,
                                        COALESCE(a.Home_Rt, '') AS HomeRT ,
                                        COALESCE(a.Home_Rw, '') AS HomeRW ,
                                        COALESCE(a.Home_Fax1, '') AS HomeFax1 ,
                                        COALESCE(a.Home_Fax2, '') AS HomeFax2 ,
                                        ISNULL(( SELECT RTRIM(COALESCE(AdditionalInfoValue, ''))
                                                 FROM   dbo.Mst_Prospect_AdditionalInfo
                                                 WHERE  ProspectID = a.Prospect_Id
                                                        AND AdditionalCode = 'INCM'
                                               ), '') AS Income ,
                                        b.Marital AS MaritalStatus ,
                                        b.Job_Title AS JobTitle ,
                                        b.Company_Name AS CompanyName ,
                                        RTRIM(a.Email) AS Email ,
                                        COALESCE(b.HP, '') AS MobilePhone1 ,
                                        COALESCE(b.HP_2, '') AS MobilePhone2 ,
                                        COALESCE(b.HP_3, '') AS MobilePhone3 ,
                                        COALESCE(b.HP_4, '') AS MobilePhone4 ,
                                        COALESCE(a.Home_Phone1, '') AS HomePhone1 ,
                                        COALESCE(a.Home_Phone2, '') AS HomePhone2 ,
                                        a.Office_Phone1 AS OfficePhone1 ,
                                        a.Office_Ext1 AS OfficeExt1 ,
                                        RTRIM(a.Office_Address) AS OfficeAddress ,
                                        RTRIM(a.Office_PostCode) AS OfficePostalCode ,
                                        ( SELECT    RTRIM(a.Office_PostCode) + ' - ' + p.Description + ', '
                                                    + p1.Description + ', ' + p2.Description + ', '
                                                    + p3.Description
                                          FROM      dbo.Mst_Postal p
                                                    LEFT JOIN dbo.Mst_General p1 ON p.City = p1.code
                                                                                    AND p1.type = 'CTY'
                                                    LEFT JOIN dbo.Mst_General p2 ON p.Province = p2.code
                                                                                    AND p2.type = 'PRV'
                                                    LEFT JOIN dbo.Mst_General p3 ON p.Country = p3.code
                                                                                    AND p3.type = 'CON'
                                          WHERE     p.PostCode = a.Office_PostCode
                                        ) AS OfficePostalCodeDesc ,
                                        ISNULL(( SELECT RTRIM(COALESCE(AdditionalInfoValue, ''))
                                                 FROM   dbo.Mst_Prospect_AdditionalInfo
                                                 WHERE  ProspectID = a.Prospect_Id
                                                        AND AdditionalCode = 'FCBK'
                                               ), '') AS Facebook ,
                                        ISNULL(( SELECT RTRIM(COALESCE(AdditionalInfoValue, ''))
                                                 FROM   dbo.Mst_Prospect_AdditionalInfo
                                                 WHERE  ProspectID = a.Prospect_Id
                                                        AND AdditionalCode = 'TWTR'
                                               ), '') AS Twitter ,
                                        ISNULL(( SELECT RTRIM(COALESCE(AdditionalInfoValue, ''))
                                                 FROM   dbo.Mst_Prospect_AdditionalInfo
                                                 WHERE  ProspectID = a.Prospect_Id
                                                        AND AdditionalCode = 'INTG'
                                               ), '') AS Instagram
                                FROM    dbo.Mst_Prospect a
                                        LEFT JOIN dbo.Mst_Prospect_Personal b ON b.Prospect_Id = a.Prospect_Id
                                WHERE   a.Prospect_Type = 1
                                        AND a.Prospect_Id = @0
                            ";

            using (var db = GetAABDB())
            {
                var a= db.Fetch<dynamic>(query, ProspectID).ToList();
                if (a.Count > 0)
                    result = a[0];
            }
            return result;
        }

        public dynamic GetCorporateProspect(string customerID)
        {
            dynamic result = null;
            string query = @"SELECT  RTRIM(COALESCE(a.Prospect_Id, '')) AS ProspectID ,
                                    RTRIM(COALESCE(a.Prospect_Type, '')) AS ProspectType ,
                                    RTRIM(COALESCE(a.[Name], '')) AS ProspectName ,
                                    ISNULL(( SELECT RTRIM(COALESCE(AdditionalInfoValue, ''))
                                                FROM   dbo.Mst_Prospect_AdditionalInfo
                                                WHERE  ProspectID = a.Prospect_Id
                                                    AND AdditionalCode = 'NOWP'
                                            ), '') AS NameOnNPWP ,
                                    ISNULL(( SELECT RTRIM(COALESCE(AdditionalInfoValue, ''))
                                                FROM   dbo.Mst_Prospect_AdditionalInfo
                                                WHERE  ProspectID = a.Prospect_Id
                                                    AND AdditionalCode = 'NPWN'
                                            ), '') AS NPWPNo ,
                                    ISNULL(( SELECT RTRIM(COALESCE(AdditionalInfoValue, ''))
                                                FROM   dbo.Mst_Prospect_AdditionalInfo
                                                WHERE  ProspectID = a.Prospect_Id
                                                    AND AdditionalCode = 'NPDT'
                                            ), '') AS NPWPDate ,
                                    ISNULL(( SELECT RTRIM(COALESCE(AdditionalInfoValue, ''))
                                                FROM   dbo.Mst_Prospect_AdditionalInfo
                                                WHERE  ProspectID = a.Prospect_Id
                                                    AND AdditionalCode = 'NPAD'
                                            ), '') AS NPWPAddress ,
                                    ISNULL(( SELECT RTRIM(COALESCE(AdditionalInfoValue, ''))
                                                FROM   dbo.Mst_Prospect_AdditionalInfo
                                                WHERE  ProspectID = a.Prospect_Id
                                                    AND AdditionalCode = 'SINR'
                                            ), '') AS SIUPNo ,
                                    ISNULL(( SELECT RTRIM(COALESCE(AdditionalInfoValue, ''))
                                                FROM   dbo.Mst_Prospect_AdditionalInfo
                                                WHERE  ProspectID = a.Prospect_Id
                                                    AND AdditionalCode = 'PKPN'
                                            ), '') AS PKPNo ,
                                    ISNULL(( SELECT RTRIM(COALESCE(AdditionalInfoValue, ''))
                                                FROM   dbo.Mst_Prospect_AdditionalInfo
                                                WHERE  ProspectID = a.Prospect_Id
                                                    AND AdditionalCode = 'PKPD'
                                            ), '') AS PKPDate ,
                                    RTRIM(COALESCE(f.Business_Type, '')) AS BusinessType ,
                                    RTRIM(COALESCE(b.Client_Type, '')) AS ClientType ,
                                    RTRIM(ISNULL(a.Prospect_Group, '')) AS CompanyGroup,
                                    RTRIM(COALESCE(a.Office_Address, '')) AS OfficeAddress ,
                                    RTRIM(COALESCE(a.Office_PostCode, '')) AS OfficePostalCode ,
                                    ( SELECT    RTRIM(a.Office_PostCode) + ' - ' + p.Description + ', '
                                                + p1.Description + ', ' + p2.Description + ', '
                                                + p3.Description
                                        FROM      dbo.Mst_Postal p
                                                LEFT JOIN dbo.Mst_General p1 ON p.City = p1.code
                                                                                AND p1.type = 'CTY'
                                                LEFT JOIN dbo.Mst_General p2 ON p.Province = p2.code
                                                                                AND p2.type = 'PRV'
                                                LEFT JOIN dbo.Mst_General p3 ON p.Country = p3.code
                                                                                AND p3.type = 'CON'
                                        WHERE     p.PostCode = a.Office_PostCode
                                    ) AS OfficePostalCodeDesc ,
                                    RTRIM(COALESCE(a.Office_Phone1, '')) AS OfficePhone1 ,
                                    RTRIM(COALESCE(a.Office_Phone2, '')) AS OfficePhone2 ,
                                    RTRIM(COALESCE(a.Office_Ext1, '')) AS OfficePhoneExt ,
                                    RTRIM(COALESCE(a.Office_Rt, '')) AS OfficeRT ,
                                    RTRIM(COALESCE(a.Office_Rw, '')) AS OfficeRW ,
                                    RTRIM(COALESCE(a.Office_Fax1, '')) AS OfficeFax1 ,
                                    RTRIM(COALESCE(a.Office_Fax2, '')) AS OfficeFax2 ,
                                    RTRIM(COALESCE(c.Name, '')) AS PIC ,
                                    RTRIM(COALESCE(a.Email, '')) AS Email ,
                                    ISNULL(( SELECT RTRIM(COALESCE(AdditionalInfoValue, ''))
                                                FROM   dbo.Mst_Prospect_AdditionalInfo
                                                WHERE  ProspectID = a.Prospect_Id
                                                    AND AdditionalCode = 'MPC1'
                                            ), '') AS MobilePhone1 ,
                                    ISNULL(( SELECT RTRIM(COALESCE(AdditionalInfoValue, ''))
                                                FROM   dbo.Mst_Prospect_AdditionalInfo
                                                WHERE  ProspectID = a.Prospect_Id
                                                    AND AdditionalCode = 'MPC2'
                                            ), '') AS MobilePhone2 ,
                                    RTRIM(COALESCE(a.Home_Address, '')) AS HomeAddress ,
                                    RTRIM(COALESCE(a.Home_PostCode, '')) AS HomePostalCode ,
                                    ( SELECT    RTRIM(a.Home_PostCode) + ' - ' + p.Description + ', '
                                                + p1.Description + ', ' + p2.Description + ', '
                                                + p3.Description
                                        FROM      dbo.Mst_Postal p
                                                LEFT JOIN dbo.Mst_General p1 ON p.City = p1.code
                                                                                AND p1.type = 'CTY'
                                                LEFT JOIN dbo.Mst_General p2 ON p.Province = p2.code
                                                                                AND p2.type = 'PRV'
                                                LEFT JOIN dbo.Mst_General p3 ON p.Country = p3.code
                                                                                AND p3.type = 'CON'
                                        WHERE     p.PostCode = a.Home_PostCode
                                    ) AS HomePostalCodeDesc ,
                                    RTRIM(COALESCE(a.Home_Phone1, '')) AS HomePhone1 ,
                                    RTRIM(COALESCE(a.Home_Phone2, '')) AS HomePhone2 ,
                                    ISNULL(( SELECT RTRIM(COALESCE(AdditionalInfoValue, ''))
                                                FROM   dbo.Mst_Prospect_AdditionalInfo
                                                WHERE  ProspectID = a.Prospect_Id
                                                    AND AdditionalCode = 'ASOW'
                                            ), '') AS AssetOwner 
                            FROM    dbo.MST_Prospect a
                                    LEFT JOIN dbo.Mst_Prospect_type b ON a.Prospect_Id = b.Prospect_Id
                                    LEFT JOIN dbo.Mst_Prospect_Corporate f ON a.Prospect_Id = f.Prospect_Id
                                    LEFT JOIN Mst_prospect_Pic c ON a.Prospect_Id = c.Prospect_Id
                            WHERE   a.Prospect_Type = 2
                                    AND a.Prospect_Id = @0 
                            ";

            using (var db = GetAABDB())
            {
                var a =  db.Fetch<dynamic>(query, customerID).ToList();
                if (a.Count > 0)
                    result = a[0];
            }
            return result;
        }        

        public dynamic ValidatePersonalCustomer(int pageNo, int pageSize, PersonalProspect ps, string sortOption, string sortDirection)
        {
                                                                           
            string query = @"SELECT  *
                            FROM    ( SELECT    A.CUST_ID AS CustomerID ,
                                                A.NAME AS CustomerName ,
                                                A.BIRTH_DATE AS BirthDate ,
                                                B.id_card AS CardNumber ,
                                                --C.ID_Card_Type AS CardType ,
                                                'CUSTOMER' AS CustomerDataType
                                      FROM      dbo.MST_CUSTOMER A
                                                INNER JOIN Mst_Cust_personal B ON A.Cust_ID = B.Cust_ID                                                
                                      WHERE     A.BIRTH_DATE = @0 
                                                
                                    UNION ALL
                                    SELECT    A.CUST_ID AS CustomerID ,
                                                A.NAME AS CustomerName ,
                                                A.BIRTH_DATE AS BirthDate ,
                                                B.id_card AS CardNumber ,
                                                --C.ID_Card_Type AS CardType ,
                                                'CUSTOMER' AS CustomerDataType
                                      FROM      dbo.MST_CUSTOMER A
                                                INNER JOIN Mst_Cust_personal B ON A.Cust_ID = B.Cust_ID                                                
                                      WHERE     B.id_card = @1
                                    ) X ";
            using (var db = GetAABDB())
            {
                switch (sortOption)
                {
                    case "0":
                        query += " Order By X.CustomerID " + sortDirection;
                        break;
                    case "1":
                        query += " Order By X.CustomerName " + sortDirection;
                        break;
                    case "2":
                        query += " Order By X.BirthDate " + sortDirection;
                        break;
                    case "3":
                        query += " Order By X.CardNumber " + sortDirection;
                        break;
                    case "4":
                        query += " Order By X.CardType " + sortDirection;
                        break;
                    default:
                        query += " Order By X.CustomerName Desc ";
                        break;
                }
                DateTime birthDate = DateTime.ParseExact(ps.BirthDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                //return db.Page<dynamic>(pageNo, pageSize, query, birthDate.Day, birthDate.Month, birthDate.Year, ps.IDNumber);
                db.CommandTimeout=60;
                //return db.Query<dynamic>(query, birthDate.Day, birthDate.Month, birthDate.Year, ps.IDNumber).ToList();
                return db.Query<dynamic>(query, birthDate.Date, ps.IDNumber).ToList();
            }

        }

        public dynamic ValidateCorporateCustomer(int pageNo, int pageSize, CorporateProspect corp, string sortOption, string sortDirection)
        {


            string query = @";DECLARE @@NPWPNO AS VARCHAR(20) ,
                                @@SIUPNO AS VARCHAR(20) 
                            SET @@NPWPNO = @0
                            SET @@SIUPNO = @1
                            SELECT  *
                            FROM    ( SELECT    a.Cust_Id AS CustomerID ,
                                                a.Name AS CustomerName ,
                                                CASE WHEN b.AdditionalInfoValue <> ''
                                                            AND c.AdditionalInfoValue <> ''
                                                        THEN b.AdditionalInfoValue + ' ,'
                                                            + c.AdditionalInfoValue
                                                        WHEN b.AdditionalInfoValue <> ''
                                                        THEN b.AdditionalInfoValue
                                                        WHEN c.AdditionalInfoValue <> ''
                                                        THEN c.AdditionalInfoValue
                                                END AS IDNumber ,
                                                CASE WHEN b.AdditionalInfoValue <> ''
                                                            AND c.AdditionalInfoValue <> ''
                                                        THEN 'NPWP, SIUP'
                                                        WHEN b.AdditionalInfoValue <> '' THEN 'NPWP'
                                                        WHEN c.AdditionalInfoValue <> '' THEN 'SIUP'
                                                END AS IDType ,
                                                'CUSTOMER' AS CustomerDataType
                                        FROM      dbo.Mst_Customer a
                                                LEFT JOIN dbo.Mst_Customer_AdditionalInfo b ON a.Cust_Id = b.CustID
                                                                                            AND b.AdditionalCode = 'NPWN'
                                                LEFT JOIN dbo.Mst_Customer_AdditionalInfo c ON a.Cust_Id = c.CustID
                                                                                            AND c.AdditionalCode = 'SINR'
                                        WHERE     ( b.AdditionalInfoValue = @@NPWPNO
                                                    AND b.AdditionalInfoValue <> ''
                                                )
                                                OR ( c.AdditionalInfoValue = @@SIUPNO
                                                        AND c.AdditionalInfoValue <> ''
                                                    )
                                    ) X ";
            using (var db = GetAABDB())
            {
                switch (sortOption)
                {
                    case "0":
                        query += " Order By X.CustomerID " + sortDirection;
                        break;
                    case "1":
                        query += " Order By X.CustomerName " + sortDirection;
                        break;
                    case "2":
                        query += " Order By X.IDNumber " + sortDirection;
                        break;
                    case "3":
                        query += " Order By X.IDType " + sortDirection;
                        break;                    
                }

                //return db.Page<dynamic>(pageNo, pageSize, query, corp.NPWPNo, corp.SIUPNo);
                return db.Query<dynamic>(query, corp.NPWPNo != null ? (corp.NPWPNo.Replace(".", "").Replace("-", "")):"", corp.SIUPNo).ToList();
            }

        }
        
        public bool InsertMasterProspect(string UserID, string prospectID, PersonalProspect ps) {
            bool result = false;
            string qInsertMasterProspect = @"INSERT  INTO dbo.Mst_Prospect
                                                    ( Prospect_Id ,
                                                      Name ,
                                                      Prospect_Type ,
                                                      Email ,
                                                      Joint_Date ,
                                                      Birth_Date ,
                                                      Home_Address ,
                                                      Home_PostCode ,
                                                      Home_Phone1 ,
                                                      Office_Phone1 ,
                                                      Office_Ext1 ,                                                         
                                                      Office_Address,
                                                      Office_PostCode,
                                                      entryusr ,
                                                      entrydt         		          
		                                             )
                                            VALUES  ( @0 , -- Prospect_Id - char(11)
                                                      @1 , -- Name - char(50)
                                                      @2 , -- Prospect_Type - char(1)
                                                      @3 , -- Email - varchar(50)
                                                      GETDATE() , -- Joint_Date - datetime	
                                                      CONVERT(DATETIME, @4, 103) , --Birth_Date	          
                                                      @5 , -- Home_Address - varchar(152)
                                                      @6 , -- Home_PostCode - char(8)
                                                      @7 , -- Home_Phone1 - varchar(16)
                                                      @8 , -- Office_Phone1 - varchar(16)
                                                      @9 , -- Office_Ext1 - varchar(6)     
                                                      @10,
                                                      @11,
                                                      @12, -- entryusr - char(5)
                                                      GETDATE() -- entrydt - datetime						
                                                    )
                                            ";
            using (var db = GetAABDB())
            {
                dynamic date = null;
                if (!string.IsNullOrEmpty(ps.BirthDate))
                {
                    date = Convert.ToDateTime(ps.BirthDate);
                }
                else {
                    date = ps.BirthDate;
                }
                var a = db.Execute(qInsertMasterProspect, 
                                            prospectID, 
                                            ps.ProspectName.ToUpper(), 
                                            ps.ProspectType,
                                            ps.Email,
                                            date, 
                                            ps.HomeAddress==null ? "": ps.HomeAddress.ToUpper(), 
                                            ps.HomePostalCode,
                                            ps.HomePhone1,
                                            ps.OfficePhone1, 
                                            ps.OfficeExt1, 
                                            ps.OfficeAddress,
                                            ps.OfficePostalCode,
                                            UserID
                                           );
                if (a > 0)
                    result = true;
            }
            return result;
        }

        public bool InsertMasterProspect(string UserID, string prospectID, CorporateProspect corp)
        {
            bool result = false;
            string qInsertMasterProspect = @"INSERT INTO dbo.Mst_Prospect
		                                            ( Prospect_Id ,
		                                              Name ,
		                                              Prospect_Type ,
                                                      Prospect_Group ,
		                                              Email ,
		                                              Joint_Date ,		          	          
		                                              Home_Address ,
		                                              Home_Phone1 ,
		                                              Office_Address ,
		                                              Office_PostCode ,
		                                              Office_Phone1 ,
		                                              Office_Ext1 ,
		                                              entryusr ,
		                                              entrydt           		          
		                                            )
		                                    VALUES  ( @0 , -- Prospect_Id - char(11)
		                                              @1 , -- Name - char(50)
                                                      @2 , -- Prospect_Type - char(1)
		                                              @3 , -- Prospect_Group - char(6)
		                                              @4 , -- Email - varchar(50)
		                                              GETDATE() , -- Joint_Date - datetime	
                                                      @5 , -- Home_Address - varchar(152)
		                                              @6 , -- Home_Phone1 - varchar(16)
		                                              @7 , -- Office_Address - varchar(152)
		                                              @8 , -- Office_PostCode - char(8)
		                                              @9 , -- Office_Phone1 - varchar(16)
		                                              @10 , -- Office_Ext1 - varchar(6)       
		                                              @11 , -- entryusr - char(5)
		                                              GETDATE() -- entrydt - datetime		          		         
		                                            )
                                            ";
            
            using (var db = GetAABDB())
            {
                var a  =  db.Execute(qInsertMasterProspect, 
                                    prospectID, 
                                    corp.ProspectName.ToUpper(), 
                                    2, 
                                    corp.CompanyGroup,
                                    corp.Email,
                                    corp.HomeAddress != null ? corp.HomeAddress.ToUpper() : "",
                                    corp.HomePhone1,
                                    corp.OfficeAddress.ToUpper(), 
                                    corp.OfficePostalCode, 
                                    corp.OfficePhone1, 
                                    corp.OfficePhoneExt,
                                    UserID);
                if (a > 0)
                    result = true;
            }
            return result;
        }

        public bool InsertPersonalProspect(string UserID, string prospectID, PersonalProspect ps)
        {
            bool result = false;
            string qInsertPersonalProspect = @" INSERT INTO dbo.Mst_Prospect_Personal
                                                        ( Prospect_Id ,
                                                          HP ,
                                                          Company_Name ,
                                                          Job_Title ,
                                                          Sex ,
                                                          Marital ,
                                                          religion ,
                                                          id_card ,
                                                          entryusr ,
                                                          HP_2 ,
                                                          HP_3 ,
                                                          HP_4 ,
                                                          entrydt
                                                        )
                                                VALUES  ( @0 , -- Prospect_Id - char(11)
                                                          @1 , -- HP - varchar(16)
                                                          @2 , -- Company_Name - varchar(50)
                                                          @3 , -- Job_Title - char(6)
                                                          @4 , -- Sex - char(1)
                                                          @5 , -- Marital - char(1)
                                                          @6 , -- religion - char(6)
                                                          @7 , -- id_card - varchar(30)
                                                          @8 , -- entryusr - char(5)
                                                          @9 , -- HP_2 - varchar(16)
                                                          @10 , -- HP_3 - varchar(16)
                                                          @11 , -- HP_4 - varchar(16)
                                                          GETDATE()  -- entrydt - datetime
                                                        )
                                                ";

            

            string qGetMstAdditonalInfo = @"SELECT AdditionalCode FROM dbo.Mst_Additional_Cust AS mac
                                                WHERE CustType = @0";
            string qInsertPersonalProspectAdd = @";DECLARE @@ProspectID VARCHAR(11) = @0 ,
                    @@AdditionalCode VARCHAR(5) = @1,
                    @@AdditionalInfoValue VARCHAR(255) = @2,
                    @@CreatedBy VARCHAR(50) = @3
                IF NOT EXISTS(SELECT *
                                FROM    dbo.Mst_Prospect_AdditionalInfo AS mcai
                                WHERE   ProspectID = @@ProspectID
                                        AND AdditionalCode = @@AdditionalCode)
                    BEGIN

                        INSERT INTO dbo.Mst_Prospect_AdditionalInfo
                                (ProspectID,
                                  AdditionalCode,
                                  AdditionalInfoValue,
                                  CreatedBy,
                                  CreatedDate
                                )
                        VALUES( @@ProspectID, --CustID - char(11)
                                  @@AdditionalCode, --AdditionalCode - varchar(5)
                                  @@AdditionalInfoValue, --AdditionalInfoValue - varchar(255)
                                  @@CreatedBy, --CreatedBy - varchar(50)
                                  GETDATE()-- CreatedDate - datetime
                                )
                    END
                ELSE
                    UPDATE  dbo.Mst_Prospect_AdditionalInfo
                    SET     AdditionalInfoValue = @@AdditionalInfoValue
                    WHERE   ProspectID = @@ProspectID
                            AND AdditionalCode = @@AdditionalCode";
            
            using (var db = GetAABDB())
            {
                var a = db.Execute(qInsertPersonalProspect,
                                        prospectID,
                                        ps.MobilePhone1,
                                        ps.CompanyName,
                                        ps.JobTitle,
                                        ps.Gender,
                                        ps.MaritalStatus,
                                        ps.Religion,
                                        ps.IDNumber,
                                        UserID,
                                        ps.MobilePhone2,
                                        ps.MobilePhone3,
                                        ps.MobilePhone4);
                if (a > 0)
                {
                    var MstAdditionalInfo = db.Query<dynamic>(qGetMstAdditonalInfo, 1).ToList();
                    List<AdditionalInfo> ListAdditionalInfo = new List<AdditionalInfo>();
                    for (int i = 0; i < MstAdditionalInfo.Count; i++)
                    {
                        #region add additional data
                        AdditionalInfo AI = new AdditionalInfo();
                        AI.AdditionalCode = MstAdditionalInfo[i].AdditionalCode;
                        if (i == 0)
                        {
                            AI.AdditionalInfoValue = ps.IDType;
                        }
                        else if (i == 1)
                        {
                            AI.AdditionalInfoValue = ps.Income;
                        }
                        else if (i == 2)
                        {
                            AI.AdditionalInfoValue = ps.Facebook;
                        }
                        else if (i == 3)
                        {
                            AI.AdditionalInfoValue = ps.Twitter;
                        }
                        else if (i == 4)
                        {
                            AI.AdditionalInfoValue = ps.Instagram;
                        }
                        ListAdditionalInfo.Add(AI);
                        #endregion
                    }
                
                    for (int j = 0; j < ListAdditionalInfo.Count(); j++)
                    {
                        var InsMstCustAdditional = db.Execute(qInsertPersonalProspectAdd, prospectID,
                            ListAdditionalInfo[j].AdditionalCode, (ListAdditionalInfo[j].AdditionalInfoValue != null ? ListAdditionalInfo[j].AdditionalInfoValue : ""), UserID);
                    }                    
                    result = true;
                }
            }
            return result;
        }
               
        public bool InsertProspectClientType(string UserID, string prospectID, string ClientType="")
        {
            bool result = false;
            string qInsertTypeProspect = @"INSERT  INTO dbo.Mst_Prospect_type
                                                    ( Prospect_Id ,
                                                      Client_Type ,
                                                      entryusr ,
                                                      entrydt
                                                    )
                                            VALUES  ( @0 ,
                                                      @1 ,
                                                      @2 ,
                                                      GETDATE()
                                                    )
                                        ";
            using (var db = GetAABDB())
            {
                var a =  db.Execute(qInsertTypeProspect, prospectID, ClientType, UserID);
                if (a > 0)
                    result = true;
            }
            return result;
        }

        public int UpdateProspectClientType(string UserID, string prospectID, string ClientType = "")
        {
            string qUpdateProspectClientType = @" UPDATE  dbo.Mst_Prospect_type
                                                SET     Client_Type = @1,
                                                        updateusr = @2 ,
                                                        updatedt = GETDATE()
                                                WHERE   Prospect_Id = @0
                                                ";
            using (var db = GetAABDB())
            {
                return db.Execute(qUpdateProspectClientType, prospectID, ClientType, UserID);
            }
        }

        public int UpdateCustomerClientType(string UserID, string CustID, string ClientType = "")
        {
            string qUpdateCustomerClientType =
                    @"IF EXISTS ( SELECT  1
                                    FROM    dbo.Mst_Cust_Type
                                    WHERE   Cust_Id = @0 )
                            BEGIN
                                UPDATE  dbo.Mst_Cust_Type
                                SET     Client_Type = CASE WHEN RTRIM(ISNULL(Client_Type, '')) <> ''
                                                            THEN Client_Type
                                                            ELSE @1
                                                        END ,
                                        updateusr = @2 ,
                                        updatedt = GETDATE()
                                WHERE   Cust_Id = @0
                            END
                        ELSE
                            BEGIN
                                INSERT  INTO dbo.Mst_Cust_Type
                                        ( Cust_Id ,
                                            Client_Type ,
                                            entryusr ,
                                            entrydt
                                        )
                                VALUES  ( @0 , -- Cust_Id - char(11)
                                            @1 , -- Client_Type - char(6)
                                            @2 , -- entryusr - char(5)
                                            GETDATE()  -- entrydt - datetime
                                        )
                            END ";
            using (var db = GetAABDB())
            {
                return db.Execute(qUpdateCustomerClientType, CustID, ClientType, UserID);
            }
        }

        public bool InsertCorporateProspect(string UserID, string prospectID, CorporateProspect corp)
        {
            bool result = false;
            string qInsertCorporateProspect = @"
                                                INSERT INTO dbo.Mst_Prospect_Corporate
                                                        ( Prospect_Id ,
                                                          Business_Type ,
                                                          EntryUsr ,
                                                          EntryDt 
                                                        )
                                                VALUES  ( @0 , -- Prospect_Id - char(11)
                                                          @1 , -- Business_Type - char(6)
                                                          @2 , -- EntryUsr - char(5)
                                                          GETDATE()  -- EntryDt - datetime
                                                        )
                                                ";
            
            string qGetMstAdditonalInfo = @"SELECT AdditionalCode FROM dbo.Mst_Additional_Cust AS mac
                                                WHERE CustType = @0";
            string qInsertPersonalProspectAdd = @";DECLARE @@ProspectID VARCHAR(11) = @0 ,
                    @@AdditionalCode VARCHAR(5) = @1,
                    @@AdditionalInfoValue VARCHAR(255) = @2,
                    @@CreatedBy VARCHAR(50) = @3
                IF NOT EXISTS(SELECT *
                                FROM    dbo.Mst_Prospect_AdditionalInfo AS mcai
                                WHERE   ProspectID = @@ProspectID
                                        AND AdditionalCode = @@AdditionalCode)
                    BEGIN

                        INSERT INTO dbo.Mst_Prospect_AdditionalInfo
                                (ProspectID,
                                  AdditionalCode,
                                  AdditionalInfoValue,
                                  CreatedBy,
                                  CreatedDate
                                )
                        VALUES( @@ProspectID, --CustID - char(11)
                                  @@AdditionalCode, --AdditionalCode - varchar(5)
                                  @@AdditionalInfoValue, --AdditionalInfoValue - varchar(255)
                                  @@CreatedBy, --CreatedBy - varchar(50)
                                  GETDATE()-- CreatedDate - datetime
                                )
                    END
                ELSE
                    UPDATE  dbo.Mst_Prospect_AdditionalInfo
                    SET     AdditionalInfoValue = @@AdditionalInfoValue
                    WHERE   ProspectID = @@ProspectID
                            AND AdditionalCode = @@AdditionalCode";

            using (var db = GetAABDB())
            {
                var MstAdditionalInfo = db.Query<dynamic>(qGetMstAdditonalInfo, 2).ToList();
                List<AdditionalInfo> ListAdditionalInfo = new List<AdditionalInfo>();
                for (int i = 0; i < MstAdditionalInfo.Count; i++)
                {
                    #region add additional data
                    AdditionalInfo AI = new AdditionalInfo();
                    AI.AdditionalCode = MstAdditionalInfo[i].AdditionalCode;
                    switch (i)
                    {
                        case 0:
                            AI.AdditionalInfoValue = corp.NPWPNo != null ? (corp.NPWPNo.Replace(".","").Replace("-","")) : "";
                            break;
                        case 1:
                            AI.AdditionalInfoValue = corp.MobilePhone1;
                            break;
                        case 2:
                            AI.AdditionalInfoValue = corp.MobilePhone2;
                            break;
                        case 3:
                            AI.AdditionalInfoValue = corp.NameOnNPWP != null ? corp.NameOnNPWP.ToUpper() : "";
                            break;
                        case 4:
                            AI.AdditionalInfoValue = corp.NPWPAddress;
                            break;
                        case 5:
                            AI.AdditionalInfoValue = corp.NPWPDate;
                            break;
                        case 6:
                            AI.AdditionalInfoValue = corp.PKPDate;
                            break;
                        case 7:
                            AI.AdditionalInfoValue = corp.PKPNo != null ? (corp.PKPNo.Replace(".", "").Replace("-", "")) : "";
                            break;
                        case 8:
                            AI.AdditionalInfoValue = corp.SIUPNo;
                            break;
                        case 9:
                            AI.AdditionalInfoValue = corp.AssetOwner;
                            break;
                    }
                    ListAdditionalInfo.Add(AI);
                    #endregion
                }

                var b = db.Execute(qInsertCorporateProspect,
                                            prospectID,
                                            corp.BusinessType,
                                            UserID);
                if (b > 0)
                {
                    for (int j = 0; j < ListAdditionalInfo.Count(); j++)
                    {
                        var InsMstCustAdditional = db.Execute(qInsertPersonalProspectAdd, prospectID,
                            ListAdditionalInfo[j].AdditionalCode, (ListAdditionalInfo[j].AdditionalInfoValue != null ? ListAdditionalInfo[j].AdditionalInfoValue : ""), UserID);
                    }
                    result = true;
                }
            }
            return result;
        }

        public bool InsertProspectPIC(string UserID, string prospectID, string Name, string Departement, string JobTitle, string Info)
        {
            bool result = false;
            string qInsertCorporateProspect =
                    @"INSERT INTO dbo.Mst_Prospect_Pic
				        ( Prospect_Id ,
				          Name ,
				          Dept ,
				          Job_Title ,
				          Info ,
				          entryusr ,
				          entrydt 				          
				        )
				VALUES  ( @0 , -- Prospect_Id - char(11)
				          @1 , -- Name - varchar(50)
				          @2 , -- Dept - varchar(6)
				          @3 , -- Job_Title - char(6)
				          @4 , -- Info - varchar(50)
				          @5 , -- entryusr - char(5)
				          GETDATE()  -- entrydt - datetime				          
				        )";
            using (var db = GetAABDB())
            {
                var a =  db.Execute(qInsertCorporateProspect, prospectID, Name, Departement, JobTitle, Info, UserID);
                if (a > 0)
                    result = true;
            }
            return result;
        }

        public int UpdateProspectPIC(string UserID, string prospectID, string Name, string Departement, string JobTitle, string Info)
        {
            string qUpdateProspectPIC = @"UPDATE  dbo.Mst_Prospect_Pic
                        SET     Name = @1,
                                Dept = @2,
                                Job_Title = @3,
                                Info = @4,
                                updateusr = @5 ,
                                updatedt = GETDATE()
                        WHERE   Prospect_Id = @0";
            using (var db = GetAABDB())
            {
                return db.Execute(qUpdateProspectPIC, prospectID, Name, Departement, JobTitle, Info, UserID);
            }
        }

        public int UpdateCustomerPIC(string UserID, string CustID, string Name, string Departement, string JobTitle, string Info)
        {
            string qUpdateCustomerPIC = @"IF EXISTS ( SELECT 1
                                                    FROM    dbo.Mst_Cust_Pic
                                                    WHERE   Cust_Id = @0 )
                                            BEGIN
                                                UPDATE  dbo.Mst_Cust_Pic
                                                SET     Name = CASE WHEN RTRIM(ISNULL(Name, '')) <> '' THEN Name
                                                                    ELSE @1
                                                               END ,
                                                        Dept = CASE WHEN RTRIM(ISNULL(Dept, '')) <> '' THEN Dept
                                                                    ELSE @2
                                                               END ,
                                                        Job_Title = CASE WHEN RTRIM(ISNULL(Job_Title, '')) <> ''
                                                                         THEN Job_Title
                                                                         ELSE @3
                                                                    END ,
                                                        Info = CASE WHEN RTRIM(ISNULL(Info, '')) <> '' THEN Info
                                                                    ELSE @4
                                                               END ,
                                                        updateusr = @5 ,
                                                        updatedt = GETDATE()
                                                WHERE   Cust_Id = @0
                                            END
                                        ELSE
                                            BEGIN 
                                                INSERT  INTO dbo.Mst_Cust_Pic
                                                        ( Cust_Id ,
                                                          Name ,
                                                          Dept ,
                                                          Job_Title ,
                                                          Info ,
                                                          entryusr ,
                                                          entrydt
                                                        )
                                                VALUES  ( @0 , -- Cust_Id - char(11)
                                                          @1 , -- Name - varchar(50)
                                                          @2 , -- Dept - varchar(6)
                                                          @3 , -- Job_Title - char(6)
                                                          @4 , -- Info - varchar(50)
                                                          @5 , -- entryusr - char(5)
                                                          GETDATE()  -- entrydt - datetime
                                                        )
                                            END";
            using (var db = GetAABDB())
            {
                return db.Execute(qUpdateCustomerPIC, CustID, Name, Departement, JobTitle, Info, UserID);
            }
        }

        public bool UpdatePersonalCustomer(string UserID, CustomerPersonal pc)
        {
            bool result = false;
            string qUpdateMasterCustomer = @"UPDATE  dbo.Mst_Customer
                                                SET     Name = CASE WHEN RTRIM(ISNULL(Name,'')) <> '' THEN Name
                                                                    ELSE @1
                                                               END ,
                                                        Email = CASE WHEN RTRIM(ISNULL(Email,'')) <> '' THEN Email
                                                                     ELSE @2
                                                                END ,
                                                        Birth_Date = CASE WHEN RTRIM(ISNULL(Birth_Date,'')) <> '' THEN Birth_Date
                                                                          ELSE Convert(datetime,@3,103)
                                                                     END ,
                                                        Home_Address = CASE WHEN RTRIM(ISNULL(Home_Address,'')) <> '' THEN Home_Address
                                                                            ELSE @4
                                                                       END ,
                                                        Home_PostCode = CASE WHEN RTRIM(ISNULL(Home_PostCode,'')) <> '' THEN Home_PostCode
                                                                             ELSE @5
                                                                        END ,
                                                        Home_Phone1 = CASE WHEN RTRIM(ISNULL(Home_Phone1,'')) <> '' THEN Home_Phone1
                                                                           ELSE @6
                                                                      END ,
                                                        Office_Phone1 = CASE WHEN RTRIM(ISNULL(Office_Phone1,'')) <> '' THEN Office_Phone1
                                                                             ELSE @7
                                                                        END ,
                                                        Office_Ext1 = CASE WHEN RTRIM(ISNULL(Office_Ext1,'')) <> '' THEN Office_Ext1
                                                                           ELSE @8
                                                                      END ,
                                                        Office_Address = CASE WHEN RTRIM(ISNULL(Office_Address,'')) <> '' THEN Office_Address
                                                                             ELSE @9
                                                                        END ,
                                                        Office_PostCode = CASE WHEN RTRIM(ISNULL(Office_PostCode,'')) <> '' THEN Office_PostCode
                                                                           ELSE @10
                                                                      END ,
                                                        updateusr = @11 ,
                                                        updatedt = GETDATE() 
                                                WHERE   Cust_Id = @0
                        ";
            string qUpdatePersonalCustomer = @"
                                                UPDATE  dbo.Mst_Cust_Personal
                                                    SET HP = CASE WHEN RTRIM(ISNULL(HP,'')) <> '' THEN HP
                                                                  ELSE @1
                                                             END ,
                                                    HP_2 = CASE WHEN RTRIM(ISNULL(HP_2,'')) <> '' THEN HP_2
                                                                ELSE @2
                                                            END ,
                                                    HP_3 = CASE WHEN RTRIM(ISNULL(HP_3,'')) <> '' THEN HP_3
                                                                ELSE @3
                                                            END ,
                                                    HP_4 = CASE WHEN RTRIM(ISNULL(HP_4,'')) <> '' THEN HP_4
                                                                ELSE @4
                                                           END ,
                                                    Company_Name = CASE WHEN RTRIM(ISNULL(Company_Name,'')) <> ''
                                                                        THEN Company_Name
                                                                        ELSE @5
                                                                    END ,
                                                    Job_Title = CASE WHEN RTRIM(ISNULL(Job_Title,'')) <> '' THEN Job_Title
                                                                        ELSE @6
                                                                END ,
                                                    Sex = CASE WHEN RTRIM(ISNULL(Sex,'')) <> '' THEN Sex
                                                                ELSE @7
                                                            END ,
                                                    Marital = CASE WHEN RTRIM(ISNULL(Marital,'')) <> '' THEN Marital
                                                                    ELSE @8
                                                                END ,
                                                    religion = CASE WHEN RTRIM(ISNULL(religion,'')) <> '' THEN religion
                                                                    ELSE @9
                                                                END ,
                                                    id_card = CASE WHEN RTRIM(ISNULL(id_card,'')) <> '' THEN id_card
                                                                    ELSE @10
                                                                END ,
                                                    updateusr = @11 ,
                                                    updatedt = GETDATE()
                                                WHERE Cust_Id = @0";
            string qGetMstAdditonalInfo = @"SELECT AdditionalCode FROM dbo.Mst_Additional_Cust AS mac
                                                WHERE CustType = @0";
            string qInsertPersonalCustAddEdit = @";DECLARE @@CustID VARCHAR(11) = @0 ,
                    @@AdditionalCode VARCHAR(5) = @1,
                    @@AdditionalInfoValue VARCHAR(255) = @2,
                    @@CreatedBy VARCHAR(50) = @3
                IF NOT EXISTS(SELECT *
                                FROM    dbo.Mst_Customer_AdditionalInfo AS mcai
                                WHERE   CustID = @@CustID
                                        AND AdditionalCode = @@AdditionalCode)
                    BEGIN

                        INSERT INTO dbo.Mst_Customer_AdditionalInfo
                                (CustID,
                                  AdditionalCode,
                                  AdditionalInfoValue,
                                  CreatedBy,
                                  CreatedDate
                                )
                        VALUES( @@CustID, --CustID - char(11)
                                  @@AdditionalCode, --AdditionalCode - varchar(5)
                                  @@AdditionalInfoValue, --AdditionalInfoValue - varchar(255)
                                  @@CreatedBy, --CreatedBy - varchar(50)
                                  GETDATE()-- CreatedDate - datetime
                                )
                    END
                ELSE
                    UPDATE  dbo.Mst_Customer_AdditionalInfo
                    SET     AdditionalInfoValue = @@AdditionalInfoValue
                    WHERE   CustID = @@CustID
                            AND AdditionalCode = @@AdditionalCode";
            using (var db = GetAABDB())
            {
                var a =  db.Execute(qUpdateMasterCustomer,
                                        pc.CustomerID,
                                        pc.CustomerName.ToUpper(),
                                        pc.Email,
                                        pc.BirthDate,
                                        pc.HomeAddress.ToUpper(),
                                        pc.HomePostalCode,
                                        pc.HomePhone1,
                                        pc.OfficePhone1,
                                        pc.OfficeExt1,
                                        pc.OfficeAddress,
                                        pc.OfficePostalCode,
                                        UserID
                                        );
                if (a > 0)
                {
                    var b = db.Execute(qUpdatePersonalCustomer,
                                        pc.CustomerID,
                                        pc.MobilePhone1,
                                        pc.MobilePhone2,
                                        pc.MobilePhone3,
                                        pc.MobilePhone4,
                                        pc.CompanyName,
                                        pc.JobTitle,
                                        pc.Gender,
                                        pc.MaritalStatus,
                                        pc.Religion,
                                        pc.IDNumber,
                                        UserID
                                        );
                    if (b > 0)
                    {
                        var MstAdditionalInfo = db.Query<dynamic>(qGetMstAdditonalInfo, 1).ToList();

                        List<AdditionalInfo> ListAdditionalInfo = new List<AdditionalInfo>();
                        for (int i = 0; i < MstAdditionalInfo.Count; i++)
                        {
                            #region add additional data
                            AdditionalInfo AI = new AdditionalInfo();
                            AI.AdditionalCode = MstAdditionalInfo[i].AdditionalCode;
                            if (i == 0)
                            {
                                AI.AdditionalInfoValue = pc.IDType;
                            }
                            else if (i == 1)
                            {
                                AI.AdditionalInfoValue = pc.Income;
                            }
                            else if (i == 2)
                            {
                                AI.AdditionalInfoValue = pc.Facebook;
                            }
                            else if (i == 3)
                            {
                                AI.AdditionalInfoValue = pc.Twitter;
                            }
                            else if (i == 4)
                            {
                                AI.AdditionalInfoValue = pc.Instagram;
                            }
                            ListAdditionalInfo.Add(AI);
                            #endregion
                        }

                        for (int j = 0; j < ListAdditionalInfo.Count(); j++)
                        {
                            var InsMstCustAdditional = db.Execute(qInsertPersonalCustAddEdit, pc.CustomerID,
                                ListAdditionalInfo[j].AdditionalCode, (ListAdditionalInfo[j].AdditionalInfoValue != null ? ListAdditionalInfo[j].AdditionalInfoValue : ""), UserID);
                        }
                        result = true;
                    }
                }
            }
            return result;
        }

        public bool UpdatePersonalProspect(string UserID, PersonalProspect pc)
        {
            bool result = false;
            string qUpdateMasterProspect = @"UPDATE  dbo.Mst_Prospect
                                                SET     Name = @1,
                                                        Email = @2,
                                                        Birth_Date = Convert(datetime,@3,103),
                                                        Home_Address = @4,
                                                        Home_PostCode = @5,
                                                        Home_Phone1 = @6,
                                                        Office_Phone1 = @7,
                                                        Office_Ext1 = @8,
                                                        Office_Address =@9,
	                                                    Office_PostCode=@10,
                                                        updateusr = @11 ,
                                                        updatedt = GETDATE()
                                                WHERE   Prospect_Id = @0 ";

            string qUpdatePersonalProspect = @" UPDATE  dbo.Mst_Prospect_Personal
                                                SET     HP =  @1,
                                                        HP_2 = @2 ,
                                                        HP_3 = @3 ,
                                                        HP_4 = @4 ,
                                                        Company_Name = @5,
                                                        Job_Title = @6,
                                                        Sex = @7,
                                                        Marital =  @8,
                                                        religion = @9,
                                                        id_card = @10,
                                                        updateusr = @11,
                                                        updatedt = GETDATE()
                                                WHERE   Prospect_Id = @0
                                            ";
            string qGetMstAdditonalInfo = @"SELECT AdditionalCode FROM dbo.Mst_Additional_Cust AS mac
                                                WHERE CustType = @0";
            string qInsertPersonalCustAddEdit = @";DECLARE @@ProspectID VARCHAR(11) = @0 ,
                    @@AdditionalCode VARCHAR(5) = @1,
                    @@AdditionalInfoValue VARCHAR(255) = @2,
                    @@CreatedBy VARCHAR(50) = @3
                IF NOT EXISTS(SELECT *
                                FROM    dbo.Mst_Prospect_AdditionalInfo AS mcai
                                WHERE   ProspectID = @@ProspectID
                                        AND AdditionalCode = @@AdditionalCode)
                    BEGIN

                        INSERT INTO dbo.Mst_Prospect_AdditionalInfo
                                (ProspectID,
                                  AdditionalCode,
                                  AdditionalInfoValue,
                                  CreatedBy,
                                  CreatedDate
                                )
                        VALUES( @@ProspectID, --CustID - char(11)
                                  @@AdditionalCode, --AdditionalCode - varchar(5)
                                  @@AdditionalInfoValue, --AdditionalInfoValue - varchar(255)
                                  @@CreatedBy, --CreatedBy - varchar(50)
                                  GETDATE()-- CreatedDate - datetime
                                )
                    END
                ELSE
                    UPDATE  dbo.Mst_Prospect_AdditionalInfo
                    SET     AdditionalInfoValue = @@AdditionalInfoValue
                    WHERE   ProspectID = @@ProspectID
                            AND AdditionalCode = @@AdditionalCode";

            using (var db = GetAABDB())
            {
                var a = db.Execute(qUpdateMasterProspect,
                                        pc.ProspectID,
                                        pc.ProspectName.ToUpper(),
                                        pc.Email,
                                        pc.BirthDate,
                                        pc.HomeAddress.ToUpper(),
                                        pc.HomePostalCode,
                                        pc.HomePhone1,
                                        pc.OfficePhone1,
                                        pc.OfficeExt1,
                                        pc.OfficeAddress,
                                        pc.OfficePostalCode,
                                        UserID);
                if (a > 0)
                {
                    var b = db.Execute(qUpdatePersonalProspect,
                                        pc.ProspectID,
                                        pc.MobilePhone1,
                                        pc.MobilePhone2,
                                        pc.MobilePhone3,
                                        pc.MobilePhone4,
                                        pc.CompanyName,
                                        pc.JobTitle,
                                        pc.Gender,
                                        pc.MaritalStatus,
                                        pc.Religion,
                                        pc.IDNumber,
                                        UserID);

                    if (b > 0)
                    {
                        var MstAdditionalInfo = db.Query<dynamic>(qGetMstAdditonalInfo, 1).ToList();

                        List<AdditionalInfo> ListAdditionalInfo = new List<AdditionalInfo>();
                        for (int i = 0; i < MstAdditionalInfo.Count; i++)
                        {
                            #region add additional data
                            AdditionalInfo AI = new AdditionalInfo();
                            AI.AdditionalCode = MstAdditionalInfo[i].AdditionalCode;
                            if (i == 0)
                            {
                                AI.AdditionalInfoValue = pc.IDType;
                            }
                            else if (i == 1)
                            {
                                AI.AdditionalInfoValue = pc.Income;
                            }
                            else if (i == 2)
                            {
                                AI.AdditionalInfoValue = pc.Facebook;
                            }
                            else if (i == 3)
                            {
                                AI.AdditionalInfoValue = pc.Twitter;
                            }
                            else if (i == 4)
                            {
                                AI.AdditionalInfoValue = pc.Instagram;
                            }
                            ListAdditionalInfo.Add(AI);
                            #endregion
                        }

                        for (int j = 0; j < ListAdditionalInfo.Count(); j++)
                        {
                            var InsMstCustAdditional = db.Execute(qInsertPersonalCustAddEdit, pc.ProspectID,
                                ListAdditionalInfo[j].AdditionalCode, (ListAdditionalInfo[j].AdditionalInfoValue != null ? ListAdditionalInfo[j].AdditionalInfoValue : ""), UserID);
                        }
                        result = true;
                    }
                }
            }

            return result;
        }

        public bool UpdateCorporateCustomer(string UserID, CustomerCoorporate pc)
        {
            bool result = false;
            string qUpdateMasterCustomer = @"UPDATE  dbo.Mst_Customer
                                                SET     Name = CASE WHEN RTRIM(ISNULL(Name, '')) <> '' THEN Name
                                                                    ELSE @1
                                                                END ,
                                                        Email = CASE WHEN RTRIM(ISNULL(Email, '')) <> '' THEN Email
                                                                        ELSE @2
                                                                END ,
                                                        Home_Address = CASE WHEN RTRIM(ISNULL(Home_Address, '')) <> '' THEN Home_Address
                                                                            ELSE @3
                                                                        END,
                                                        Home_Phone1 = CASE WHEN RTRIM(ISNULL(Home_Phone1, '')) <> '' THEN Home_Phone1
                                                                            ELSE @4
                                                                        END,
                                                        Office_Address = CASE WHEN RTRIM(ISNULL(Office_Address, '')) <> '' THEN Office_Address
                                                                                ELSE @5
                                                                            END ,
                                                        Office_PostCode = CASE WHEN RTRIM(ISNULL(Office_PostCode, '')) <> '' THEN Office_PostCode
                                                                                ELSE @6
                                                                            END ,
                                                        Office_Phone1 = CASE WHEN RTRIM(ISNULL(Office_Phone1, '')) <> '' THEN Office_Phone1
                                                                                ELSE @7
                                                                        END ,
                                                        Office_Ext1 = CASE WHEN RTRIM(ISNULL(Office_Ext1, '')) <> '' THEN Office_Ext1
                                                                            ELSE @8
                                                                        END ,
                                                        Cust_Group = @9 ,
                                                        updateusr = @10 ,
                                                        updatedt = GETDATE() 
                                                WHERE   Cust_Id = @0
                                            ";
            string qUpdateCorporateCustomer = @" UPDATE  dbo.Mst_Cust_Corporate
                                                SET     business_type = CASE WHEN RTRIM(ISNULL(business_type, '')) <> '' THEN business_type
                                                                                ELSE @1
                                                                        END ,
                                                        updateusr = @2 ,
                                                        updatedt = GETDATE()
                                                WHERE   Cust_Id = @0 ";

            string qGetMstAdditonalInfo = @"SELECT AdditionalCode FROM dbo.Mst_Additional_Cust AS mac
                                                WHERE CustType = @0";
            string qInsertCorporateCustAddEdit = @";DECLARE @@CustID VARCHAR(11) = @0 ,
                    @@AdditionalCode VARCHAR(5) = @1,
                    @@AdditionalInfoValue VARCHAR(255) = @2,
                    @@CreatedBy VARCHAR(50) = @3
                IF NOT EXISTS(SELECT *
                                FROM    dbo.Mst_Customer_AdditionalInfo AS mcai
                                WHERE   CustID = @@CustID
                                        AND AdditionalCode = @@AdditionalCode)
                    BEGIN

                        INSERT INTO dbo.Mst_Customer_AdditionalInfo
                                (CustID,
                                  AdditionalCode,
                                  AdditionalInfoValue,
                                  CreatedBy,
                                  CreatedDate
                                )
                        VALUES( @@CustID, --CustID - char(11)
                                  @@AdditionalCode, --AdditionalCode - varchar(5)
                                  @@AdditionalInfoValue, --AdditionalInfoValue - varchar(255)
                                  @@CreatedBy, --CreatedBy - varchar(50)
                                  GETDATE()-- CreatedDate - datetime
                                )
                    END
                ELSE
                    UPDATE  dbo.Mst_Customer_AdditionalInfo
                    SET     AdditionalInfoValue = @@AdditionalInfoValue
                    WHERE   CustID = @@CustID
                            AND AdditionalCode = @@AdditionalCode";

            using (var db = GetAABDB())
            {
                var a = db.Execute(qUpdateMasterCustomer,
                                        pc.CustomerID,
                                        pc.CustomerName.ToUpper(),
                                        pc.Email,
                                        pc.HomeAddress.ToUpper(),
                                        pc.HomePhone1,
                                        pc.OfficeAddress.ToUpper(),
                                        pc.OfficePostalCode,
                                        pc.OfficePhone1,
                                        pc.OfficePhoneExt,
                                        pc.CompanyGroup,
                                        UserID);
                if (a > 0)
                {
                    var b = db.Execute(qUpdateCorporateCustomer,
                                        pc.CustomerID,
                                        pc.BusinessType,
                                        UserID);
                    //if (b > 0) {
                        var MstAdditionalInfo = db.Query<dynamic>(qGetMstAdditonalInfo, 2).ToList();
                        List<AdditionalInfo> ListAdditionalInfo = new List<AdditionalInfo>();
                        for (int i = 0; i < MstAdditionalInfo.Count; i++)
                        {
                            #region add additional data
                            AdditionalInfo AI = new AdditionalInfo();
                            AI.AdditionalCode = MstAdditionalInfo[i].AdditionalCode;
                            switch (i)
                            {
                                case 0:
                                    AI.AdditionalInfoValue = pc.NPWPNo.Replace(".", "").Replace("-", ""); ;
                                    break;
                                case 1:
                                    AI.AdditionalInfoValue = pc.MobilePhone1;
                                    break;
                                case 2:
                                    AI.AdditionalInfoValue = pc.MobilePhone2;
                                    break;
                                case 3:
                                    AI.AdditionalInfoValue = pc.NameOnNPWP.ToUpper();
                                    break;
                                case 4:
                                    AI.AdditionalInfoValue = pc.NPWPAddress;
                                    break;
                                case 5:
                                    AI.AdditionalInfoValue = pc.NPWPDate;
                                    break;
                                case 6:
                                    AI.AdditionalInfoValue = pc.PKPDate;
                                    break;
                                case 7:
                                    AI.AdditionalInfoValue = pc.PKPNo.Replace(".", "").Replace("-", "");
                                    break;
                                case 8:
                                    AI.AdditionalInfoValue = pc.SIUPNo;
                                    break;
                                case 9:
                                    AI.AdditionalInfoValue = pc.AssetOwner;
                                    break;
                            }
                            ListAdditionalInfo.Add(AI);
                            #endregion
                        }
                        for (int j = 0; j < ListAdditionalInfo.Count(); j++)
                        {
                            var InsMstCustAdditional = db.Execute(qInsertCorporateCustAddEdit, pc.CustomerID,
                                ListAdditionalInfo[j].AdditionalCode, (ListAdditionalInfo[j].AdditionalInfoValue != null ? ListAdditionalInfo[j].AdditionalInfoValue : ""), UserID);
                        }
                        result = true;
                    //}
                }
            }
            return result;
        }

        public bool UpdateCorporateProspect(string UserID, CorporateProspect pc)
        {
            bool result = false;
            string qUpdateMasterProspect = @"UPDATE  dbo.Mst_Prospect
                                                SET     Name = @1,
                                                        Email = @2,
                                                        Prospect_Group = @3,
                                                        Home_Address = @4,
                                                        Home_Phone1 = @5,
                                                        Office_Address = @6,
                                                        Office_PostCode = @7,
                                                        Office_Phone1 = @8,
                                                        Office_Ext1 = @9,
                                                        updateusr = @10,
                                                        updatedt = GETDATE()
                                                WHERE   Prospect_Id = @0 ";

            string qUpdateCorporateProspect = @" UPDATE  dbo.Mst_Prospect_Corporate
                                                SET     Business_Type = @1,
                                                        UpdateUsr = @2 ,
                                                        UpdateDt = GETDATE()
                                                WHERE   Prospect_Id = @0 ";
            string qGetMstAdditonalInfo = @"SELECT AdditionalCode FROM dbo.Mst_Additional_Cust AS mac
                                                WHERE CustType = @0";
            string qInsertPersonalProspectAdd = @";DECLARE @@ProspectID VARCHAR(11) = @0 ,
                    @@AdditionalCode VARCHAR(5) = @1,
                    @@AdditionalInfoValue VARCHAR(255) = @2,
                    @@CreatedBy VARCHAR(50) = @3
                IF NOT EXISTS(SELECT *
                                FROM    dbo.Mst_Prospect_AdditionalInfo AS mcai
                                WHERE   ProspectID = @@ProspectID
                                        AND AdditionalCode = @@AdditionalCode)
                    BEGIN

                        INSERT INTO dbo.Mst_Prospect_AdditionalInfo
                                (ProspectID,
                                  AdditionalCode,
                                  AdditionalInfoValue,
                                  CreatedBy,
                                  CreatedDate
                                )
                        VALUES( @@ProspectID, --CustID - char(11)
                                  @@AdditionalCode, --AdditionalCode - varchar(5)
                                  @@AdditionalInfoValue, --AdditionalInfoValue - varchar(255)
                                  @@CreatedBy, --CreatedBy - varchar(50)
                                  GETDATE()-- CreatedDate - datetime
                                )
                    END
                ELSE
                    UPDATE  dbo.Mst_Prospect_AdditionalInfo
                    SET     AdditionalInfoValue = @@AdditionalInfoValue
                    WHERE   ProspectID = @@ProspectID
                            AND AdditionalCode = @@AdditionalCode";

            using (var db = GetAABDB())
            {
                var a = db.Execute(qUpdateMasterProspect,
                                        pc.ProspectID,
                                        pc.ProspectName.ToUpper(),
                                        pc.Email,
                                        pc.CompanyGroup,
                                        pc.HomeAddress.ToUpper(),
                                        pc.HomePhone1,
                                        pc.OfficeAddress.ToUpper(),
                                        pc.OfficePostalCode,
                                        pc.OfficePhone1,
                                        pc.OfficePhoneExt,
                                        UserID);
                if (a > 0) {
                    var b = db.Execute(qUpdateCorporateProspect,
                                        pc.ProspectID,
                                        pc.BusinessType,
                                        UserID);
                    if (b > 0) {
                        var MstAdditionalInfo = db.Query<dynamic>(qGetMstAdditonalInfo, 2).ToList();
                        List<AdditionalInfo> ListAdditionalInfo = new List<AdditionalInfo>();
                        for (int i = 0; i < MstAdditionalInfo.Count; i++)
                        {
                            #region add additional data
                            AdditionalInfo AI = new AdditionalInfo();
                            AI.AdditionalCode = MstAdditionalInfo[i].AdditionalCode;
                            switch (i)
                            {
                                case 0:
                                    AI.AdditionalInfoValue = pc.NPWPNo != null ? (pc.NPWPNo.Replace(".", "").Replace("-", "")) : "";
                                    break;
                                case 1:
                                    AI.AdditionalInfoValue = pc.MobilePhone1;
                                    break;
                                case 2:
                                    AI.AdditionalInfoValue = pc.MobilePhone2;
                                    break;
                                case 3:
                                    AI.AdditionalInfoValue = pc.NameOnNPWP != null ? pc.NameOnNPWP.ToUpper() : "";
                                    break;
                                case 4:
                                    AI.AdditionalInfoValue = pc.NPWPAddress;
                                    break;
                                case 5:
                                    AI.AdditionalInfoValue = pc.NPWPDate;
                                    break;
                                case 6:
                                    AI.AdditionalInfoValue = pc.PKPDate;
                                    break;
                                case 7:
                                    AI.AdditionalInfoValue = pc.PKPNo != null ? (pc.PKPNo.Replace(".", "").Replace("-", "")) :"";
                                    break;
                                case 8:
                                    AI.AdditionalInfoValue = pc.SIUPNo;
                                    break;
                                case 9:
                                    AI.AdditionalInfoValue = pc.AssetOwner;
                                    break;
                            }
                            ListAdditionalInfo.Add(AI);
                            #endregion
                        }

                        for (int j = 0; j < ListAdditionalInfo.Count(); j++)
                        {
                            var InsMstCustAdditional = db.Execute(qInsertPersonalProspectAdd, pc.ProspectID,
                                ListAdditionalInfo[j].AdditionalCode, (ListAdditionalInfo[j].AdditionalInfoValue != null ? ListAdditionalInfo[j].AdditionalInfoValue : ""), UserID);
                        }
                        result = true;
                    }
                }
            }
            return result;
        }

        public dynamic GetSalesmanDetail(string SalesmanID, string BizType)
        {
            string query = @"Select * From (SELECT  A.SALESMAN_ID AS SalesmanID ,
                                A.NAME AS SalesmanName ,
                                B.BRANCH_ID AS BranchID ,
                                B.Nama AS BranchName ,
                                B.branch_area_id AS BranchAreaID ,
                                B.Branch_Area_Name AS BranchAreaName ,
                                A.TYPE AS SalesmanType ,
                                A.JOINTDATE AS JointDate ,
                                A.INACTIVEDATE AS InactiveDate ,
                                A.REFSALESMAN AS RefSalesmanID ,
                                A.REFMITRA AS RefMitra ,
                                A.INCENTIF AS Incentive ,
                                A.Dept_id AS DepartmentID ,
                                C.Name AS DepartmentName,
								D.Address AS BranchAddress,
                                D.PostCode AS BranchPostalCode,
                                D.Biz_Type AS BizType,
                                ( SELECT    RTRIM(D.PostCode) + ' - ' + p.Description + ', '
											+ p1.Description + ', ' + p2.Description + ', '
											+ p3.Description
									FROM      dbo.Mst_Postal p
											LEFT JOIN dbo.Mst_General p1 ON p.City = p1.code
																			AND p1.type = 'CTY'
											LEFT JOIN dbo.Mst_General p2 ON p.Province = p2.code
																			AND p2.type = 'PRV'
											LEFT JOIN dbo.Mst_General p3 ON p.Country = p3.code
																			AND p3.type = 'CON'
									WHERE     p.PostCode = D.PostCode
								) AS BranchPostalCodeDesc
                        FROM    MST_SALESMAN A
                                LEFT JOIN MST_Department C ON A.Dept_Id = C.Dept_Id 
                                LEFT join mst_branch_area B ON A.BRANCH_ID = B.BRANCH_ID
                                LEFT join Mst_Branch D ON A.BRANCH_ID = D.BRANCH_ID 								
                        WHERE   A.STATUS = 1) X
                    Where X.SalesmanID=@0 AND X.BizType=@1";
            using (var db = GetAABDB())
            {                
                return db.Query<dynamic>(query, SalesmanID, BizType).FirstOrDefault();
            }
        }

        public List<SalesmanDealerInfo> GetSalesmanDealerDetail(string CustomerID="")
        {
            string query = @"SELECT ghcd.CustomerID, 
                    cname.CustomerCode, 
                    RTRIM(cname.CustomerName) CustomerName, 
                    ghcd.DealerID, 
                    dname.CustomerCode DealerCode, 
                    RTRIM(dname.CustomerName) DealerName, 
                    ghcd.SupervisorID, 
                    spvname.CustomerCode SupervisorCode, 	
                    RTRIM(spvname.CustomerName) SupervisorName, 
                    ghcd.BranchHeadID, 
                    bhname.CustomerCode BranchHeadCode, 
                    RTRIM(bhname.CustomerName) BranchHeadName  
                    FROM General.HierarchyCustomerDealer ghcd  
                    INNER JOIN General.Customer cname ON cname.CustomerID = ghcd.CustomerID  
                    INNER JOIN General.Customer dname ON dname.CustomerID = ghcd.DealerID  
                    INNER JOIN General.Customer spvname ON spvname.CustomerID = ghcd.SupervisorID  
                    INNER JOIN General.Customer bhname ON bhname.CustomerID = ghcd.BranchHeadID  
                    WHERE ghcd.RowStatus = 0 ";
            if (!string.IsNullOrEmpty(CustomerID))
                query = query + " AND cname.CustomerCode=@0 ";
            using (var db = Geta2isBeyondDB())
            {
                if (!string.IsNullOrEmpty(CustomerID))
                    return db.Query<SalesmanDealerInfo>(query, CustomerID).ToList();
                else
                    return db.Query<SalesmanDealerInfo>(query).ToList();
            }
        }

        public List<SalesmanDealerInfo> GetSalesmanDealerDetailLEXUS(string CustomerID = "")
        {
            string query = @"
                SELECT CASE WHEN css.Salesman_Code IS NULL THEN mc.Cust_Id
                             ELSE css.Salesman_Code
                        END AS CustomerCode ,
		                Dealer_ID AS DealerCode ,
                        mc.Name AS CustomerName ,
		                mc2.Name AS DealerName ,
                        '' AS SupervisorCode ,
                        '' AS SupervisorName ,
                        '' AS BranchHeadCode ,
                        '' AS BranchHeadName ,
                        mc2.Cust_Id ,
                        Salesman_Code
                FROM    dbo.Mst_Customer mc
		                LEFT JOIN  dbo.ComSales_Salesman AS css ON mc.Cust_Id = css.Cust_ID
                        LEFT JOIN dbo.Mst_Customer AS mc2 ON mc2.Cust_Id = css.Dealer_ID ";
            if (!string.IsNullOrEmpty(CustomerID))
                query = query + " WHERE mc.Cust_Id=@0 ";

            using (var db = GetAABDB())
            {
                if (!string.IsNullOrWhiteSpace(CustomerID))
                    return db.Query<SalesmanDealerInfo>(query, CustomerID).ToList();
                else
                    return db.Query<SalesmanDealerInfo>(query).ToList();
            }
        }
        public dynamic GetSalesmanDealerDetailInfo(string id)
        {
            List<dynamic> result = new List<dynamic>();
            using (var db = GetAABDB())
            {
                string query = @" SELECT    RTRIM(a.Cust_ID) AS Code ,
                                RTRIM(a.Name) AS Name ,
                                b.Client_Type AS CustomerType
                        FROM      Mst_Customer a
                                INNER JOIN Mst_Cust_Type b ON a.Cust_Id = b.Cust_Id
                        WHERE   b.Client_Type IN ( 'BROKER', 'DEALER' ) AND a.Cust_Id = @0 ";

                string qBeyond = @"SELECT    RTRIM(cname.CustomerCode) AS CustomerID 
                                        FROM      BeyondDB.General.HierarchyCustomerDealer ghcd
                                                INNER JOIN BeyondDB.General.Customer cname ON cname.CustomerID = ghcd.CustomerID
                                                INNER JOIN BeyondDB.General.Customer dname ON dname.CustomerID = ghcd.DealerID
                                                INNER JOIN BeyondDB.General.Customer spvname ON spvname.CustomerID = ghcd.SupervisorID
                                                INNER JOIN BeyondDB.General.Customer bhname ON bhname.CustomerID = ghcd.BranchHeadID
                                        WHERE     ghcd.RowStatus = 0 AND cname.CustomerCode IN (SELECT  value FROM    PolicyInsurance.fn_Split(@0, ','))";
                string qAAB = @"SELECT    RTRIM(a.Cust_ID) AS Code ,
                                    RTRIM(a.Name) AS Name ,
                                    b.Client_Type AS CustomerType
                            FROM      Mst_Customer a
                                    INNER JOIN Mst_Cust_Type b ON a.Cust_Id = b.Cust_Id
                            WHERE   b.Client_Type IN ( 'BROKER', 'DEALER' ) AND a.Cust_Id IN (
                                    SELECT  part
                                    FROM    dbo.Fn_SplitString(RTRIM(LTRIM(@0)),
                                                                ',') )";
                var SDAAB = db.Query<dynamic>(query, id).ToList();
                List<string> CustIDAAB = new List<string>();
                List<string> ValidCustID = new List<string>();
                foreach (var sdAAB in SDAAB)
                {
                    if (!string.IsNullOrWhiteSpace(sdAAB.Code))
                    {
                        if (sdAAB.CustomerType.Trim().ToUpper() == "DEALER")
                        {
                            CustIDAAB.Add(sdAAB.Code.Trim());
                        }
                        else
                        {
                            ValidCustID.Add(sdAAB.Code.Trim());
                        }
                    }
                };
                if (CustIDAAB.Count > 0)
                {
                    string strCustIDAAB = string.Join(",", CustIDAAB);
                    using (var db2 = Geta2isBeyondDB())
                    {
                        List<string> DealerCustID = db2.Query<string>(qBeyond, strCustIDAAB).ToList();
                        ValidCustID.AddRange(DealerCustID);
                    }
                }

                if (ValidCustID.Count > 0)
                {
                    string strValidCustID = string.Join(",", ValidCustID);
                    using (var db2 = GetAABDB())
                    {
                        result = db2.Fetch<dynamic>(qAAB, strValidCustID);
                    }
                }

                return result;
            }
        }


        public dynamic GetTitanAgentDetail(string CustomerID)
        {
            string query = @"SELECT  *
                            FROM    ( SELECT    b.Cust_Id AS CustomerID,
					                            a.Client_Code AS ClientCode,
                                                RTRIM(b.Name) AS ClientName,					                            
                                                c.Cust_Id AS UplinerCustomerID,
					                            a.Upliner_Client_Code AS UplinerCode ,
					                            RTRIM(d.Name) AS UplinerName,
					                            e.Cust_Id AS LeaderCustomerID,
					                            a.Leader_Client_Code AS LeaderCode ,                  
					                            RTRIM(f.Name) AS LeaderName                    
                                      FROM      dtl_cust_type a WITH ( NOLOCK )
                                                INNER JOIN mst_customer b WITH ( NOLOCK ) ON a.cust_id = b.cust_id                    
					                            LEFT JOIN dtl_cust_type c WITH ( NOLOCK ) ON a.Upliner_Client_Code=c.Client_Code
                                                LEFT JOIN mst_customer d WITH ( NOLOCK ) ON c.Cust_Id = d.cust_id                    
					                            LEFT JOIN dtl_cust_type e WITH ( NOLOCK ) ON a.Leader_Client_Code=e.Client_Code
                                                LEFT JOIN mst_customer f WITH ( NOLOCK ) ON e.Cust_Id = f.cust_id                    
                                      WHERE     a.Client_Type = 'AGENT'
                                    ) AS X
                            Where X.CustomerID=@0 OR X.ClientCode=@0";
            using (var db = GetAABDB())
            {
                return db.Query<dynamic>(query, CustomerID).FirstOrDefault();
            }
        }

        public UserLogin GetUserLogin(string UserID, string MenuName)
        {
            UserLogin result = new UserLogin();
            string qGetUserDetail = @"SELECT Top 1 A.User_Id as UserId, 
                                        A.User_Name as UserName,
                                        A.Dept_ID as DepartmentID, 
                                        A.Branch_Id as BranchID,                                        
                                        A.Employee_No as EmployeeNo,
                                        A.Email_Address as Email
                                        FROM DBO.Mst_User as A
                                        WHERE User_Id=@0";
            string qGetBizRole = @";Exec [dbo].[usp_GetUserBusinessRole]
                                        @@UserID =@0,
                                        @@MenuName=@1";
            string qGetConfiguration = @"
                    Select Top 1 GC.ConfigurationValue As ConfigurationValue
	                    From GeneralConfiguration GC
	                    Where GC.RowStatus = 0
                            AND GC.ReferenceType = @0		                    
		                    AND GC.ReferenceID = @1";            

            using (var db = GetAABDB())
            {
                var UserDetail = db.Query<dynamic>(qGetUserDetail, UserID).FirstOrDefault();
                string a2isMenuName = db.Query<string>(qGetConfiguration, "G5-MENU-NAME", MenuName).SingleOrDefault();                
                if (UserDetail != null) {
                    result.UserId = UserDetail.UserId;
                    result.UserName = UserDetail.UserName;
                    result.BranchID = UserDetail.BranchID;
                    result.DepartmentID = UserDetail.DepartmentID;
                    result.EmployeeNo = UserDetail.EmployeeNo;
                    result.Email = UserDetail.Email;
                    result.BusinessRole = new List<string>();
                    result.BusinessRole = db.Query<string>(qGetBizRole, UserID, a2isMenuName).ToList();
                }
                return result;
            }
        }

        public dynamic GetBrokerDeaalerDetail(string CustomeriD)
        {
            string qGetUserDetail = @"SELECT  RTRIM(a.Cust_ID) AS Code ,
                                                RTRIM(a.Name) AS Name ,
                                                b.Client_Type AS CustomerType
                                        FROM    Mst_Customer a
                                                INNER JOIN Mst_Cust_Type b ON a.Cust_Id = b.Cust_Id                                        
                                                AND a.Cust_Id =@0";
            using (var db = GetAABDB())
            {
                return db.Query<dynamic>(qGetUserDetail, CustomeriD).FirstOrDefault();
            }
        }

        public dynamic GetPolicyHolderPersonalDetail(string CustomerID)
        {
            string query = @"Select RTRIM(a.Cust_Id) AS CustomerID ,
                                    RTRIM(ISNULL(a.prospect_Id,'')) AS ProspectID ,
                                    RTRIM(ISNULL(a.Cust_Type,'')) AS CustomerType ,
                                    RTRIM(ISNULL(a.[Name],'')) AS CustomerName ,
                                    RTRIM(ISNULL(a.Home_Address,'')) AS HomeAddress ,
                                    RTRIM(ISNULL(a.Home_PostCode,'')) AS HomePostalCode ,
                                    ( SELECT    RTRIM(a.Home_PostCode) + ' - ' + p.Description + ', '
                                                + p1.Description + ', ' + p2.Description + ', '
                                                + p3.Description
                                      FROM      dbo.Mst_Postal p
                                                LEFT JOIN dbo.Mst_General p1 ON p.City = p1.code
                                                                                AND p1.type = 'CTY'
                                                LEFT JOIN dbo.Mst_General p2 ON p.Province = p2.code
                                                                                AND p2.type = 'PRV'
                                                LEFT JOIN dbo.Mst_General p3 ON p.Country = p3.code
                                                                                AND p3.type = 'CON'
                                      WHERE     p.PostCode = a.Home_PostCode
                                    ) AS HomePostalCodeDesc ,
                                    RTRIM(ISNULL(a.Home_Rt,'')) AS HomeRT ,
                                    RTRIM(ISNULL(a.Home_Rw,'')) AS HomeRW ,
                                    RTRIM(ISNULL(a.Home_Fax1,'')) AS HomeFax1 ,
                                    RTRIM(ISNULL(a.Home_Fax2,'')) AS HomeFax2 ,
                                    RTRIM(ISNULL(c.HP,'')) AS MobilePhone1 ,
                                    RTRIM(ISNULL(c.HP_2,'')) AS MobilePhone2 ,
                                    RTRIM(ISNULL(c.HP_3,'')) AS MobilePhone3 ,
                                    RTRIM(ISNULL(c.HP_4,'')) AS MobilePhone4 ,
                                    RTRIM(ISNULL(a.Home_Phone1,'')) AS HomePhone1 ,
                                    RTRIM(ISNULL(a.Home_Phone2,'')) AS HomePhone2 ,
                                    RTRIM(ISNULL(a.Office_Phone1,'')) AS OfficePhone1 ,
                                    RTRIM(ISNULL(a.Office_Ext1,'')) AS OfficeExt1 ,
                                    RTRIM(ISNULL(a.Office_Address,'')) AS OfficeAddress ,
                                    RTRIM(ISNULL(a.Office_PostCode,'')) AS OfficePostalCode ,
                                    ( SELECT    RTRIM(a.Office_PostCode) + ' - ' + p.Description + ', '
                                                + p1.Description + ', ' + p2.Description + ', '
                                                + p3.Description
                                      FROM      dbo.Mst_Postal p
                                                LEFT JOIN dbo.Mst_General p1 ON p.City = p1.code
                                                                                AND p1.type = 'CTY'
                                                LEFT JOIN dbo.Mst_General p2 ON p.Province = p2.code
                                                                                AND p2.type = 'PRV'
                                                LEFT JOIN dbo.Mst_General p3 ON p.Country = p3.code
                                                                                AND p3.type = 'CON'
                                      WHERE     p.PostCode = a.Office_PostCode
                                    ) AS OfficePostalCodeDesc
                            FROM    dbo.Mst_Customer a
                                    LEFT JOIN dbo.Mst_Preferred_Customer b ON a.Cust_Id = b.Cust_Id
                                    LEFT JOIN dbo.Mst_Cust_Personal c ON a.Cust_Id = c.Cust_Id
                            WHERE   a.Cust_Type = 1
                                    AND A.Cust_Id = @0";
            using (var db = GetAABDB())
            {
                return db.Query<dynamic>(query, CustomerID).FirstOrDefault();
            }
        }

        public dynamic GetPolicyHolderCorporateDetail(string CustomerID)
        {
            string query = @"SELECT  RTRIM(COALESCE(a.Cust_Id, '')) AS CustomerID ,
                                    RTRIM(COALESCE(a.prospect_Id, '')) AS ProspectID ,
                                    RTRIM(COALESCE(a.[NAME], '')) AS CustomerName ,
                                    RTRIM(COALESCE(a.Office_Address, '')) AS OfficeAddress ,
                                    RTRIM(COALESCE(a.Office_PostCode, '')) AS OfficePostalCode ,
                                    ( SELECT    RTRIM(a.Office_PostCode) + ' - ' + p.Description + ', '
                                                + p1.Description + ', ' + p2.Description + ', '
                                                + p3.Description
                                      FROM      dbo.Mst_Postal p
                                                LEFT JOIN dbo.Mst_General p1 ON p.City = p1.code
                                                                                AND p1.type = 'CTY'
                                                LEFT JOIN dbo.Mst_General p2 ON p.Province = p2.code
                                                                                AND p2.type = 'PRV'
                                                LEFT JOIN dbo.Mst_General p3 ON p.Country = p3.code
                                                                                AND p3.type = 'CON'
                                      WHERE     p.PostCode = a.Office_PostCode
                                    ) AS OfficePostalCodeDesc ,
                                    RTRIM(COALESCE(a.Office_Phone1, '')) AS OfficePhone1 ,
                                    RTRIM(COALESCE(a.Office_Phone2, '')) AS OfficePhone2 ,
                                    RTRIM(COALESCE(a.Office_Ext1, '')) AS OfficePhoneExt ,
                                    RTRIM(COALESCE(a.Office_Rt, '')) AS OfficeRT ,
                                    RTRIM(COALESCE(a.Office_Rw, '')) AS OfficeRW ,
                                    RTRIM(COALESCE(a.Office_Fax1, '')) AS OfficeFax1 ,
                                    RTRIM(COALESCE(a.Office_Fax2, '')) AS OfficeFax2 ,
                                    ISNULL(( SELECT RTRIM(COALESCE(AdditionalInfoValue, ''))
                                             FROM   dbo.Mst_Customer_AdditionalInfo
                                             WHERE  CustID = a.Cust_Id
                                                    AND AdditionalCode = 'MPC1'
                                           ), '') AS MobilePhone1 ,
                                    ISNULL(( SELECT RTRIM(COALESCE(AdditionalInfoValue, ''))
                                             FROM   dbo.Mst_Customer_AdditionalInfo
                                             WHERE  CustID = a.Cust_Id
                                                    AND AdditionalCode = 'MPC2'
                                           ), '') AS MobilePhone2
                            FROM    dbo.Mst_Customer a
                                    LEFT JOIN mst_cust_type b ON a.Cust_Id = b.Cust_Id
                                    LEFT JOIN Mst_Cust_Pic c ON a.Cust_Id = c.Cust_id
                                    LEFT JOIN Mst_Cust_Corporate f ON a.Cust_Id = f.cust_id
                            WHERE   a.Cust_Type = 2
                                    AND A.CUST_ID = @0";
            using (var db = GetAABDB())
            {
                return db.Query<dynamic>(query, CustomerID).FirstOrDefault();
            }
        }

        public List<dynamic> ValidateAgenTitan(string CustomerID)
        {
            string query = @"SELECT TOP 1 ID AS Code ,
                                    Name AS Name
                            FROM    dbo.AIF
                            WHERE   RTRIM(ISNULL(AgentStatus, '')) <> 'NON-ACTIVE'
                                    AND RTRIM(ISNULL(EvalStatus, '')) <> 'TERMINATED'
                                    AND XAgentID = @0";
            using (var db = Geta2isTitanDB())
            {
                return db.Fetch<dynamic>(query, CustomerID);
            }
        }

        public string GetCustomerInfo(string CustID, out string HomeAddress, out string OfficeAddress, out string HomePhone, out string OfficePhone, out string MobilePhone, out string Email) {
            string strSQL = @"SELECT  RTRIM(ISNULL(a.Name, '')) AS CustomerName ,
                                    RTRIM(ISNULL(a.Home_Address, '')) AS HomeAddress ,
                                    RTRIM(ISNULL(a.Office_Address, '')) AS OfficeAddress ,
                                    RTRIM(ISNULL(a.Home_Phone1, ISNULL(Home_Phone2, ''))) AS HomePhone ,
                                    RTRIM(ISNULL(a.Office_Phone1, ISNULL(Office_Phone2, ''))) AS OfficePhone ,
                                    CASE WHEN Cust_Type = '1' THEN ( SELECT TOP 1
                                                                            RTRIM(ISNULL(HP, ''))
                                                                     FROM   dbo.Mst_Cust_Personal
                                                                     WHERE  Cust_Id = a.Cust_Id
                                                                   )
                                         WHEN Cust_Type = '2'
                                         THEN ( SELECT TOP 1
                                                        RTRIM(ISNULL(AdditionalInfoValue, ''))
                                                FROM    dbo.Mst_Customer_AdditionalInfo
                                                WHERE   Cust_Id = a.Cust_Id
                                                        AND AdditionalCode = 'MPC1'
                                              )
                                    END AS MobilePhone,
		                            RTRIM(ISNULL(a.Email, '')) AS Email
                            FROM    dbo.Mst_Customer a WHERE a.Cust_Id = @0 ";
            string CustName = string.Empty;
            HomeAddress = string.Empty;
            OfficeAddress = string.Empty;
            HomePhone = string.Empty;
            OfficePhone = string.Empty;
            MobilePhone = string.Empty;
            Email = string.Empty;
            using (var db = GetAABDB())
            {
                var result =  db.Fetch<dynamic>(strSQL, CustID);
                if (result.Count > 0) {
                    CustName = result[0].CustomerName;
                    HomeAddress = result[0].HomeAddress;
                    OfficeAddress = result[0].OfficeAddress;
                    HomePhone = result[0].HomePhone;
                    OfficePhone = result[0].OfficePhone;
                    MobilePhone = result[0].MobilePhone;
                    Email = result[0].Email;
                }
            }
            return CustName;
        }

        public string GetProspectInfo(string ProspectID, out string HomeAddress, out string OfficeAddress, out string HomePhone, out string OfficePhone, out string MobilePhone, out string Email)
        {
            string strSQL = @"SELECT  RTRIM(ISNULL(a.Name, '')) AS ProspectName ,
                                    RTRIM(ISNULL(a.Home_Address, '')) AS HomeAddress ,
                                    RTRIM(ISNULL(a.Office_Address, '')) AS OfficeAddress ,
                                    RTRIM(ISNULL(a.Home_Phone1, ISNULL(Home_Phone2, ''))) AS HomePhone ,
                                    RTRIM(ISNULL(a.Office_Phone1, ISNULL(Office_Phone2, ''))) AS OfficePhone ,
                                    CASE WHEN Prospect_Type = '1' THEN ( SELECT TOP 1
                                                                            RTRIM(ISNULL(HP, ''))
                                                                     FROM   dbo.Mst_Prospect_Personal
                                                                     WHERE  Prospect_Id = a.Prospect_Id
                                                                   )
                                         WHEN Prospect_Type = '2'
                                         THEN ( SELECT TOP 1
                                                        RTRIM(ISNULL(AdditionalInfoValue, ''))
                                                FROM    dbo.Mst_Prospect_AdditionalInfo
                                                WHERE   ProspectID = a.Prospect_Id
                                                        AND AdditionalCode = 'MPC1'
                                              )
                                    END AS MobilePhone,
		                            RTRIM(ISNULL(a.Email, '')) AS Email
                            FROM    dbo.Mst_Prospect a
                            WHERE   a.Prospect_Id = @0";
            string ProspectName = string.Empty;
            HomeAddress = string.Empty;
            OfficeAddress = string.Empty;
            HomePhone = string.Empty;
            OfficePhone = string.Empty;
            MobilePhone = string.Empty;
            Email = string.Empty;
            using (var db = GetAABDB())
            {
                var result = db.Fetch<dynamic>(strSQL, ProspectID);
                if (result.Count > 0)
                {
                    ProspectName = result[0].ProspectName;
                    HomeAddress = result[0].HomeAddress;
                    OfficeAddress = result[0].OfficeAddress;
                    HomePhone = result[0].HomePhone;
                    OfficePhone = result[0].OfficePhone;
                    MobilePhone = result[0].MobilePhone;
                    Email = result[0].Email;
                }
            }
            return ProspectName;
        }
    }
}