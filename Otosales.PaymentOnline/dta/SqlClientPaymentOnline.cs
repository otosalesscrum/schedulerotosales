﻿using a2is.Framework.API;
using a2is.Framework.DataAccess;
using a2is.Framework.Monitoring;
using Otosales.PaymentOnline.dto;
using Otosales.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otosales.PaymentOnline.dta
{
    public class SqlClientPaymentOnline : a2isAPIController
    {
        public void InsertLog(string ReportName, string ErrorDescription, bool ResultCode)
        {
            int maxField = 8000;
            var _log = new a2isLogHelper();
            string LogStatus = string.Empty;
            List<String> errorList = new List<String>();
            LogStatus = ResultCode ? "Success" : "Failed";

            dynamic result = null;

            if (ErrorDescription.Length > maxField)
            {
                int count = (ErrorDescription.Length / maxField) + 1;
                for (int i = 0; i < count; i++)
                {
                    maxField = ErrorDescription.Length < maxField ? ErrorDescription.Length : maxField;
                    errorList.Add(ErrorDescription.Substring(0, maxField));
                    ErrorDescription = ErrorDescription.Remove(0, maxField);
                }


                int rowIndex = 0;

                foreach (var errorItem in errorList)
                {
                    string EntryUser = "Report Refund";
                    string query = string.Format(@"INSERT INTO dbo.ReportErrorLog 
                                                (ReportName, ErrorDescription, EntryDate, EntryUser, LogStatus, ParentLogID, isSent)
                                                VALUES (@0, @1, GETDATE(), @2, @3, @4, '0')");
                    using (a2isDBHelper.Database db = RepositoryBase.GetAABMobileDB())
                    {
                        result = db.Execute(query, ReportName, errorItem, EntryUser, LogStatus, rowIndex != 0 ? result : 0);
                        _log.Debug("Function InsertErrorLog OK! ");
                    }

                    if (rowIndex == 0)
                    {
                        result = null;
                        using (a2isDBHelper.Database db = RepositoryBase.GetAABMobileDB())
                        {
                            query = string.Format(@" SELECT TOP 1 ReportErrorLogID FROM dbo.ReportErrorLog ORDER BY EntryDate DESC");
                            result = db.Query<long>(query).ToList();
                        }
                    }
                    rowIndex++;
                }
            }
            else
            {
                string EntryUser = "Payment Online";
                string query = string.Format(@"INSERT INTO dbo.ReportErrorLog 
                                                (ReportName, ErrorDescription, EntryDate, EntryUser, LogStatus, ParentLogID, isSent)
                                                VALUES (@0, @1, GETDATE(), @2, @3, '0', '0')");
                using (a2isDBHelper.Database db = RepositoryBase.GetAABMobileDB())
                {
                    result = db.Execute(query, ReportName, ErrorDescription, EntryUser, LogStatus);
                    _log.Debug("Function InsertErrorLog OK! ");
                }
            }
        }

        public List<PaymentOnlineEntity> GetPaymentOnlineData(string cvStartDate, string cvEndDate)
        {
            var result = new List<PaymentOnlineEntity>();

            //var VAData = GetPaymentOnlineVA(cvStartDate, cvEndDate);
            var NonVAData = GetPaymentOnlineNonVA(cvStartDate, cvEndDate);

            //result.AddRange(VAData);
            result.AddRange(NonVAData);

            var newResult = AddAdditionalData(result.OrderBy(x => x.OrderNo).ToList());

            return newResult;
        }

        private List<PaymentOnlineEntity> GetPaymentOnlineVA(string cvStartDate, string cvEndDate)
        {
            var result = new List<PaymentOnlineEntity>();
            var helperList = new List<PaymentOnlineEntity>();
            var AABhelperList = new List<PaymentOnlineEntity>();
            string query = string.Empty;

            #region Get VA Number List

            using (a2isDBHelper.Database db = new a2isDBHelper.Database("a2isAsuransiAstraDB"))
            {
                query = string.Format(@";SELECT  b.VANumber AS NomorVA                                                 
                                        FROM    GODigital.OrderSimulationPayment a
                                        INNER JOIN godigital.ordersimulation b ON a.OrderNo = b.OrderNo
                                        INNER JOIN GODigital.BankPaymentMethod c ON a.BankPaymentMethodCode = c.BankPaymentMethodCode
                                        INNER JOIN GODigital.Bank ba ON ba.BankCode = c.BankCode
                                        INNER JOIN godigital.PaymentMethod d ON c.PaymentMethodCode = d.PaymentMethodCode
                                        INNER JOIN GODigital.PaymentGatewayRequestStatusMapping e ON e.PaymentRequestParameterCode = c.PaymentRequestParameterCode AND e.IsAuthorizedF = a.IsAuthorized AND a.PaymentStatus = e.PaymentRequestStatusCode
                                        INNER JOIN GODigital.MstPaymentStatus f ON f.PaymentStatusCode = e.PaymentStatusCode
                                        INNER JOIN GODigital.OrderVABooking AS g ON g.OrderNo = b.PolicyOrderNo
                                        WHERE    a.RowStatus = 0 AND b.RowStatus = 0 AND c.RowStatus = 0 AND d.RowStatus = 0 AND e.RowStatus = 0 
                                                 AND f.RowStatus = 0 AND g.RowStatus = 0 AND ba.RowStatus = 0
                                                 AND a.CreatedDate BETWEEN Convert(DATETIME,'{0}') AND CONVERT(DATETIME,'{1}')
		                                         AND a.ResultCD = '0000'
                                                 AND a.ResultCD2 = '0000'
                                                 AND a.IsProceedByDBUrl = 1        
                                                 AND ( d.PaymentMethodCode = '02' AND COALESCE(a.ReceiptCode, '') = '0000' AND COALESCE(a.AuthNo, '') = '0000' AND a.IsAuthorized = 1 )
                                                 AND f.PaymentStatusCode IN ( 'PD', 'HA' )
                                        GROUP BY b.VANumber",
                                                 cvStartDate, cvEndDate);

                result = db.Query<PaymentOnlineEntity>(query).ToList();
            }

            #endregion

            #region Collect data VA from Permata Virtual Account

            using (a2isDBHelper.Database db = new a2isDBHelper.Database("a2isBeyondDB"))
            {
                query = string.Format(@";SELECT  VI_VANumber, VI_TraceNo 
                                         INTO	 #VANonReversal
                                         FROM    Finance.PermataVirtualAccountHistory 
                                         WHERE   RowStatus = 0 AND VI_VANumber IN ( '{0}' ) AND 
                                                 VI_TransactionDate BETWEEN '{1}' AND '{2}'
                                         GROUP BY VI_VANumber, VI_TraceNo
                                         HAVING COUNT(VI_TraceNo) = 1;

                                            SELECT  PVAH.PolicyNo ,
                                                    PVAH.VoucherNo ,
                                                    VS.VoucherStatusDescription AS VoucherStatus ,
                                                    CONVERT(CHAR(10), V.TransactionDate, 126) AS VoucherDate ,
                                                    CONVERT(CHAR(10), V.SettlementDate, 126) AS SettleDate ,
                                                    'Outstanding' AS SettleStatus ,
                                                    PVAH.VI_VANumber AS NomorVA ,
                                                    V.Remark AS Remarks ,
                                                    V.VoucherID ,
		                                            CONVERT(nvarchar(MAX),PVAH.CreatedDate,120) AS PaymentTime 
                                            INTO    #VATable
                                            FROM Finance.PermataVirtualAccountHistory AS PVAH 
                                            INNER JOIN #VANonReversal AS VNR ON VNR.VI_VANumber = PVAH.VI_VANumber AND VNR.VI_TraceNo = PVAH.VI_TraceNo
                                            INNER JOIN Finance.Voucher AS V ON PVAH.VoucherNo = V.VoucherNo
                                            INNER JOIN General.VoucherStatus AS VS ON VS.VoucherStatusID = V.VoucherStatusID
                                            WHERE   V.RowStatus = 0
                                                    AND VS.RowStatus = 0
                                                    AND VS.ActiveStatusID = 1
                                            ORDER BY PVAH.PolicyNo , PVAH.VI_VANumber;

                                         UPDATE VNR SET SettleStatus = CASE WHEN ( ISNULL(FN.Nominal,0) - ISNULL(fn.Diff,0) - ISNULL(FNA.PaidAmount,0) ) = 0 THEN 'Settled' ELSE 'Outstanding' END,
                                                        Remarks = CASE WHEN ISNULL(VNR.Remarks,'') = '' THEN FN.Remarks ELSE VNR.Remarks END                                                        
                                         FROM    #VATable AS VNR
                                         INNER JOIN Finance.VoucherDetail AS VD ON VNR.VoucherID = VD.VoucherID
                                         INNER JOIN Finance.FinanceNotes AS FN ON FN.FinanceNoteID = VD.FinanceNoteID
                                         INNER JOIN Finance.FinanceNoteInfos AS FNI ON FNI.FinanceNoteID = FN.FinanceNoteID
                                         INNER JOIN Finance.FinanceNoteAdditional AS FNA ON FNA.NoteNumber = FN.NoteNumber
                                         WHERE VD.RowStatus = 0 AND FN.RowStatus = 0 AND FNI.RowStatus = 0 AND FNA.RowStatus = 0

                                         SELECT * FROM #VATable AS VT

                                         DROP TABLE #VANonReversal
                                         DROP TABLE #VATable", String.Join("','", result.Select(x => x.NomorVA.Trim()).ToArray()), cvStartDate, cvEndDate);

                result = db.Query<PaymentOnlineEntity>(query).ToList();
            }

            #endregion

            #region Collect Additional Data for VA

            if (result.Count > 0)
                using (a2isDBHelper.Database db = new a2isDBHelper.Database("a2isAsuransiAstraDB"))
                {
                    query = string.Format(@"SELECT  g.PolicyNo,
		                                        b.PolicyOrderNo AS OrderNo,
                                                d.PaymentMethodCode AS PaymentMethodCode ,
                                                d.PaymentMethodDescription AS PaymentMethodDescription ,
                                                c.BankCode AS BankCode ,
                                                ba.BankName AS BankName ,
                                                PaymentType = CASE WHEN COALESCE(c.PaymentInstallmentIntervalCode, '') = '' THEN 'Full Payment' ELSE 'Installment' END ,
                                                Installment = CASE WHEN COALESCE(c.PaymentInstallmentIntervalCode, '') = '' THEN 0 ELSE c.InstallmentIntervalTimes END ,
                                                c.MDRRate AS MDR , 
                                                CASE WHEN d.PaymentCategory = 'CC' THEN a.AuthNo
			                                         WHEN d.PaymentCategory = 'DD' AND a.ReceiptCode <> '' THEN a.ReceiptCode
			                                         WHEN d.PaymentCategory = 'DD' AND ISNULL(a.ReceiptCode,'') = '' THEN a.mRefNo
			                                         ELSE '' END AS ReferenceNo ,
		                                        a.ReceiptCode,
		                                        a.mRefNo,
		                                        b.VANumber AS NomorVA,
                                                '' AS VoucherNo,
												a.TransactionID,
												a.CardNo 
                                        FROM    GODigital.OrderSimulationPayment a
                                        INNER JOIN godigital.ordersimulation b ON a.OrderNo = b.OrderNo
                                        INNER JOIN GODigital.BankPaymentMethod c ON a.BankPaymentMethodCode = c.BankPaymentMethodCode
                                        INNER JOIN GODigital.Bank ba ON ba.BankCode = c.BankCode
                                        INNER JOIN godigital.PaymentMethod d ON c.PaymentMethodCode = d.PaymentMethodCode
                                        INNER JOIN GODigital.PaymentGatewayRequestStatusMapping e ON e.PaymentRequestParameterCode = c.PaymentRequestParameterCode AND e.IsAuthorizedF = a.IsAuthorized AND a.PaymentStatus = e.PaymentRequestStatusCode
                                        INNER JOIN GODigital.MstPaymentStatus f ON f.PaymentStatusCode = e.PaymentStatusCode
                                        INNER JOIN GODigital.OrderVABooking AS g ON g.OrderNo = b.PolicyOrderNo                                        
                                        WHERE   a.RowStatus = 0 AND b.RowStatus = 0 AND c.RowStatus = 0 AND d.RowStatus = 0 AND e.RowStatus = 0
                                                AND f.RowStatus = 0 AND g.RowStatus = 0 AND ba.RowStatus = 0                                                
		                                        AND a.ResultCD = '0000'
                                                AND a.ResultCD2 = '0000'
                                                AND a.IsProceedByDBUrl = 1        
                                                --AND ( d.PaymentMethodCode = '02' AND COALESCE(a.ReceiptCode, '') = '0000' AND COALESCE(a.AuthNo, '') = '0000' AND a.IsAuthorized = 1 )
                                                AND f.PaymentStatusCode IN ( 'PD', 'HA' )
                                                AND b.VANumber IN ('{0}')
                                        GROUP BY g.PolicyNo, b.PolicyOrderNo, d.PaymentMethodCode, d.PaymentMethodDescription, c.BankCode, ba.BankName, 
                                                 c.PaymentInstallmentIntervalCode, c.InstallmentIntervalTimes, c.MDRRate, d.PaymentCategory, a.AuthNo, 
                                                 a.ReceiptCode, a.mRefNo, b.VANumber, a.TransactionID, a.CardNo", string.Join("','", result.Select(x => x.NomorVA).ToArray()));

                    helperList = db.Query<PaymentOnlineEntity>(query).ToList();
                }

            #endregion
            foreach (var item in result)
            {
                if (helperList.Exists(x => x.NomorVA.Trim() == item.NomorVA.Trim()))
                {
                    var firstData = helperList.Where(x => x.NomorVA.Trim() == item.NomorVA.Trim()).FirstOrDefault();
                    item.OrderNo = firstData.OrderNo;
                    item.PaymentMethodCode = firstData.PaymentMethodCode;
                    item.PaymentMethodDescription = firstData.PaymentMethodDescription;
                    item.BankCode = firstData.BankCode;
                    item.BankName = firstData.BankName;
                    //item.PaymentType = firstData.PaymentType;
                    item.Installment = firstData.Installment;
                    item.MDR = firstData.MDR;
                    item.ReferenceNo = firstData.ReferenceNo;
                    item.TransactionID = firstData.TransactionID;
                    item.CardNo = firstData.CardNo;
                }
            }

            #region 009URF2020
            if (result.Count > 0)
            {
                AABhelperList = GetCustData(result);
            }
            foreach (var item in result)
            {
                if (AABhelperList.Exists(x => x.OrderNo.Trim() == item.OrderNo.Trim()))
                {
                    var firstData = AABhelperList.Where(x => x.OrderNo.Trim() == item.OrderNo.Trim()).FirstOrDefault();
                    item.CustomerType = firstData.CustomerType;
                }
            }
            #endregion

            return result;
        }

        private List<PaymentOnlineEntity> GetPaymentOnlineNonVA(string cvStartDate, string cvEndDate)
        {
            var result = new List<PaymentOnlineEntity>();
            var beyondData = new List<PaymentOnlineEntity>();
            var AABData = new List<PaymentOnlineEntity>();

            string query = string.Empty;

            #region Function to retrieve data non VA
            using (a2isDBHelper.Database db = RepositoryBase.GetAABMobileDB())
            {
                query = string.Format(@"SELECT  b.PolicyNo, 		                                        
		                                        b.PolicyOrderNo AS OrderNo,
                                                d.PaymentMethodCode AS PaymentMethodCode ,
                                                d.PaymentMethodDescription AS PaymentMethodDescription ,
                                                c.BankCode AS BankCode ,
                                                ba.BankName AS BankName ,
                                                PaymentType = CASE WHEN COALESCE(c.PaymentInstallmentIntervalCode, '') = '' THEN 'Full Payment' ELSE 'Installment' END ,
                                                Installment = CASE WHEN COALESCE(c.PaymentInstallmentIntervalCode, '') = '' THEN 0 ELSE c.InstallmentIntervalTimes END ,
                                                c.MDRRate AS MDR , 
                                                CASE WHEN d.PaymentCategory = 'CC' THEN a.AuthNo
			                                         WHEN d.PaymentCategory = 'DD' AND a.ReceiptCode <> '' THEN a.ReceiptCode
			                                         WHEN d.PaymentCategory = 'DD' AND ISNULL(a.ReceiptCode,'') = '' THEN a.mRefNo
			                                         ELSE '' END AS ReferenceNo ,
		                                        a.ReceiptCode,
		                                        a.mRefNo,
		                                        b.VANumber AS NomorVA ,
                                                '' AS Settle ,
                                                '' AS SettleDate ,
                                                CASE WHEN h.VoucherRC <> '' THEN h.VoucherRC ELSE h.VoucherRO END AS VoucherNo ,
                                                '' AS VoucherStatus ,
                                                '' AS VoucherDate ,
                                                '' AS Remarks,
												a.TransactionID,
												a.CardNo,
												b.ProductTypeCode ProductType
                                        FROM    dbo.OrderSimulationPayment a
                                        INNER JOIN dbo.ordersimulation b ON a.OrderNo = b.OrderNo
                                        INNER JOIN Asuransiastra.GODigital.BankPaymentMethod c ON a.BankPaymentMethodCode = c.BankPaymentMethodCode
                                        INNER JOIN Asuransiastra.GODigital.Bank ba ON ba.BankCode = c.BankCode
                                        INNER JOIN Asuransiastra.GODigital.PaymentMethod d ON c.PaymentMethodCode = d.PaymentMethodCode
                                        INNER JOIN Asuransiastra.GODigital.PaymentGatewayRequestStatusMapping e ON e.PaymentRequestParameterCode = c.PaymentRequestParameterCode AND e.IsAuthorizedF = a.IsAuthorized AND a.PaymentStatus = e.PaymentRequestStatusCode
                                        INNER JOIN Asuransiastra.GODigital.MstPaymentStatus f ON f.PaymentStatusCode = e.PaymentStatusCode
                                        LEFT JOIN dbo.VoucherPayment AS h ON h.TransactionID = a.TransactionID AND h.OrderNo = b.PolicyOrderNo
                                        WHERE   a.RowStatus = 1 AND b.RowStatus = 1 AND c.RowStatus = 0 AND d.RowStatus = 0 AND e.RowStatus = 0 
                                                AND f.RowStatus = 0 AND ba.RowStatus = 0                                                
                                                AND a.CreatedDate BETWEEN Convert(DATETIME,'{0}') AND CONVERT(DATETIME,'{1}')                                                
		                                        AND a.ResultCD = '0000'
                                                AND a.ResultCD2 = '0000'
                                                AND a.IsProceedByDBUrl = 1        
                                                AND ( ( d.PaymentMethodCode = '04' AND ( COALESCE(a.ReceiptCode, '') <> '' OR COALESCE(a.mRefNo, '') <> '' ) AND a.IsAuthorized = 1) OR
                                                      ( d.PaymentMethodCode = '01' AND COALESCE(a.AuthNo, '') <> '' AND ( ( c.PaymentType = 'F' ) OR ( c.PaymentType = 'I' AND a.IsAuthorized = 1 ) ) ) )
                                                AND f.PaymentStatusCode IN ( 'PD', 'HA' , 'NA' )                                                
                                        GROUP BY b.PolicyNo, b.PolicyOrderNo, d.PaymentMethodCode, d.PaymentMethodDescription, c.BankCode, ba.BankName, 
                                                 c.PaymentInstallmentIntervalCode, c.InstallmentIntervalTimes, c.MDRRate, d.PaymentCategory, a.AuthNo, 
                                                 a.ReceiptCode, a.mRefNo, b.VANumber, h.VoucherRC, h.VoucherRO, a.TransactionID, a.CardNo, b.ProductTypeCode",
                                                 cvStartDate, cvEndDate);

                result = db.Query<PaymentOnlineEntity>(query).ToList();
            }

            if (result.Count > 0)
            {
                using (a2isDBHelper.Database db = RepositoryBase.Geta2isBeyondDB())
                {
                    query = string.Format(@"SELECT   V.VoucherNo,
                                                     LEFT(CONVERT(VARCHAR, V.CreatedDate, 120), 10) AS VoucherDate,
                                                     VS.VoucherStatusDescription AS VoucherStatus,
		                                             LEFT(CONVERT(VARCHAR, V.SettlementDate, 120), 10) AS SettleDate,
		                                             CASE WHEN ( SUM(FN.Nominal) - SUM(fn.Diff) - SUM(FNA.PaidAmount) ) = 0 THEN 'Settled' ELSE 'Outstanding' END SettleStatus ,
                                                     CASE WHEN ISNULL(V.Remark,'') <> '' THEN V.Remark ELSE FN.Remarks END AS Remarks
                                            FROM    Finance.Voucher AS V
                                            INNER JOIN Finance.VoucherDetail AS VD ON VD.VoucherID = V.VoucherID
                                            INNER JOIN Finance.FinanceNotes AS FN ON FN.FinanceNoteID = VD.FinanceNoteID
                                            INNER JOIN Finance.FinanceNoteInfos AS FNI ON FNI.FinanceNoteID = FN.FinanceNoteID
                                            INNER JOIN Finance.FinanceNoteAdditional AS FNA ON FNA.NoteNumber = FN.NoteNumber
                                            INNER JOIN General.VoucherStatus AS VS ON VS.VoucherStatusID = V.VoucherStatusID
                                            WHERE   FN.RowStatus = 0 AND FNI.RowStatus = 0 AND FNA.RowStatus = 0
                                                    --AND ( ( FN.TpJournal = 'PRM' AND FN.TpSubJournal = 'DRC' ) OR ( FN.TpJournal = 'SPR' AND FN.TpSubJournal = 'DRC' ) )
                                                    AND V.VoucherNo IN ('{0}')				
                                            GROUP BY V.VoucherNo, V.CreatedDate, VS.VoucherStatusDescription, V.SettlementDate, V.Remark, FN.Remarks;",
                                            String.Join("','", result.Where(y => !string.IsNullOrEmpty(y.VoucherNo)).Select(x => x.VoucherNo?.Trim()).ToArray()));
                    beyondData = db.Query<PaymentOnlineEntity>(query).ToList();
                }
            }

            List<String> existVANoList = new List<String>();
            using (a2isDBHelper.Database db = RepositoryBase.Geta2isBeyondDB())
            {
                query = string.Format(@";SELECT  PVA.VI_VANumber
                                         FROM    PolicyInsurance.PermataVirtualAccount AS PVA
                                         INNER JOIN PolicyInsurance.PermataVirtualAccountSettlement AS PVAS ON PVAS.PermataVirtualAccountID = PVA.PermataVirtualAccountID
                                         WHERE PVA.VI_VANumber IN('{0}') GROUP BY PVA.VI_VANumber", String.Join("','", beyondData.Where(y => !string.IsNullOrEmpty(y.NomorVA)).Select(x => x.NomorVA?.Trim()).ToArray()));
                existVANoList = db.Query<String>(query).ToList();
            }

            if (existVANoList.Count > 0)
            {
                var filteredData = from b in beyondData
                                   where !existVANoList.Contains(b.NomorVA)
                                   select b;

                beyondData = filteredData.ToList();
            }

            //0090URF2020
            if (result.Count > 0)
            {
                AABData = GetCustData(result);
            }

            foreach (var item in result)
            {
                if (beyondData.Exists(x => x.VoucherNo?.Trim() == item.VoucherNo?.Trim()))
                {
                    var firstData = beyondData.Where(x => x.VoucherNo.Trim() == item.VoucherNo.Trim()).FirstOrDefault();
                    item.SettleStatus = string.IsNullOrEmpty(firstData.SettleStatus) ? string.Empty : firstData.SettleStatus;
                    item.SettleDate = string.IsNullOrEmpty(firstData.SettleDate) ? string.Empty : Convert.ToDateTime(firstData.SettleDate).ToString("yyyy-MM-dd");
                    item.VoucherNo = string.IsNullOrEmpty(firstData.VoucherNo) ? string.Empty : firstData.VoucherNo;
                    item.VoucherStatus = string.IsNullOrEmpty(firstData.VoucherStatus) ? string.Empty : firstData.VoucherStatus;
                    item.VoucherDate = string.IsNullOrEmpty(firstData.VoucherDate) ? string.Empty : Convert.ToDateTime(firstData.VoucherDate).ToString("yyyy-MM-dd");
                    item.Remarks = string.IsNullOrEmpty(firstData.Remarks) ? string.Empty : firstData.Remarks;
                }
                if (AABData.Exists(x => x.OrderNo.Trim() == item.OrderNo.Trim()))
                {
                    var firstData = AABData.Where(x => x.OrderNo.Trim() == item.OrderNo.Trim()).FirstOrDefault();
                    item.CustomerType = firstData.CustomerType;
                }
            }

            //Get IsAuthorized or Capture
            using (a2isDBHelper.Database db = RepositoryBase.GetAABMobileDB())
            {
                //PaymentStatus = 0 --> Capture
                //PaymentStatus = 5 --> Authorized
                query = string.Format(@"SELECT  OS.PolicyOrderNo AS OrderNo , CONVERT(NVARCHAR(MAX),TransactionDate,120) AS PaymentTime ,
                                                CASE WHEN OSP.PaymentStatus = '0' THEN 'Capture' 
							                         WHEN OSP.PaymentStatus = '5' THEN 'Authorized'
							                         ELSE '' END AS AuthOrCaptureCC
                                        FROM    dbo.OrderSimulation AS OS
                                        INNER JOIN dbo.OrderSimulationPayment AS OSP ON OSP.OrderNo = OS.OrderNo
                                        INNER JOIN Asuransiastra.GODigital.BankPaymentMethod AS BPM ON BPM.BankPaymentMethodCode = OSP.BankPaymentMethodCode                                        
                                        WHERE   OS.RowStatus = 1
                                                AND OSP.RowStatus = 1
                                                AND BPM.RowStatus = 0		                                        
                                                AND BPM.PaymentRequestParameterCode IN ( 'CCINSTPYMNTBBCA','CCINSTPYMNTBMRI','CCINSTPYMNTBNLI','CCFULLPYMNTDIRECT','CCFULLPYMNTCAPTURE','CCINSTPYMNT','CLICKPYMNTBMRI' )
		                                        AND OS.PolicyOrderNo IN ('{0}')", string.Join("','", result.Where(y => !string.IsNullOrEmpty(y.OrderNo)).Select(x => x.OrderNo).ToArray()));
                var helperList = db.Query<PaymentOnlineEntity>(query).ToList();

                foreach (var item in result)
                    if (helperList.Exists(x => x.OrderNo?.Trim() == item.OrderNo?.Trim()))
                    {
                        item.AuthOrCaptureCC = helperList.Where(x => x.OrderNo == item.OrderNo).FirstOrDefault().AuthOrCaptureCC;
                        item.PaymentTime = helperList.Where(x => x.OrderNo == item.OrderNo).FirstOrDefault().PaymentTime;
                    }
            }

            #endregion

            return result;
        }

        #region 0092 - GetCustType
        private List<PaymentOnlineEntity> GetCustData(List<PaymentOnlineEntity> prevData)
        {
            using (var db = RepositoryBase.GetAABDB())
            {
                string qGetCustType = string.Format(@"SELECT 
                                                    CASE WHEN Cust_Type = '1' THEN 'Personal'
			                                                    WHEN Cust_Type = '2' THEN 'Company'
                                                    ELSE '' END CustomerType,
                                                    mo.Order_No OrderNO
                                                    FROM dbo.Mst_Order mo
                                                    INNER JOIN dbo.Mst_Customer mc
                                                    ON mc.Cust_Id = mo.Cust_Id
                                                    WHERE mo.Order_No IN('{0}')", string.Join("','", prevData.Select(x => x.OrderNo).ToArray()));
                return db.Query<PaymentOnlineEntity>(qGetCustType).ToList();
            }
        }
        #endregion

        private List<PaymentOnlineEntity> AddAdditionalData(List<PaymentOnlineEntity> prevData)
        {
            var newData = new List<PaymentOnlineEntity>();
            var query = string.Empty;

            //Get Premi
            using (a2isDBHelper.Database db = RepositoryBase.GetAABDB())
            {
                query = string.Format(@"SELECT Premium, Discount, mo.Order_No OrderNo 
                                        ,(SELECT TOP 1 COALESCE(TotalPremi,0)+COALESCE(Policy_Fee1,0)+COALESCE(Stamp_Duty1,0) FROM dbo.PRD_POLICY_FEE ppf 
	                                        WHERE ppf.Product_Code=mo.Product_Code AND ppf.Order_Type=1 
	                                        AND ppf.Lower_limit<=odi.Sum_Insured AND ppf.Status=1 ORDER BY ppf.Lower_Limit DESC) TotalPremi
                                        , ms.Name SegmentCode
                                        FROM dbo.Mst_Order mo
                                        INNER JOIN (
                                        SELECT SUM(Gross_Premium) Premium, SUM(Net3) TotalPremi, SUM(Gross_Premium)- SUM(Net3) Discount, Order_No 
                                        FROM dbo.Ord_Dtl_Coverage GROUP BY Order_No) a
                                        ON a.Order_No = mo.Order_No
                                        INNER JOIN dbo.Ord_Dtl_Interest odi 
                                        ON mo.Order_No = odi.Order_no AND odi.Interest_Code = 'CASCO' AND odi.Interest_Type = 'P'
                                        INNER JOIN dbo.Mapping_Product_SOB mps
                                        ON mps.Product_code = mo.Product_Code AND mps.RowStatus=0
                                        INNER JOIN dbo.Mst_SOB ms ON mps.SOB_ID = ms.SOB_ID AND ms.SOB_ID=mo.Segment_Code AND ms.status=1
                                        WHERE mo.Order_No IN ('{0}')", string.Join("','", prevData.Select(x => x.OrderNo).ToArray()));
                var helperList = db.Query<PaymentOnlineEntity>(query).ToList();

                foreach (var item in prevData)
                    if (helperList.Exists(x => x.OrderNo?.Trim() == item.OrderNo?.Trim()))
                    {
                        item.Premium = helperList.Where(x => x.OrderNo?.Trim() == item.OrderNo?.Trim()).FirstOrDefault().Premium;
                        item.Discount = helperList.Where(x => x.OrderNo?.Trim() == item.OrderNo?.Trim()).FirstOrDefault().Discount;
                        item.TotalPremi = helperList.Where(x => x.OrderNo?.Trim() == item.OrderNo?.Trim()).FirstOrDefault().TotalPremi;
                        item.segmentCode = helperList.Where(x => x.OrderNo?.Trim() == item.OrderNo?.Trim()).FirstOrDefault().segmentCode;
                    }
            }

            using (a2isDBHelper.Database db = RepositoryBase.Geta2isBeyondDB())
            {
                query = string.Format(@"SELECT  B.BankName AS BankAcquiringName, V.AABAccountNo AS BankAcquiringAccountNo, V.Nominal - V.Diff AS VoucherAmount, V.VoucherNo
                                        FROM    Finance.Voucher AS V
                                        INNER JOIN General.AABAccount AS AA ON AA.AABAccountID = V.AABAccountID
                                        INNER JOIN General.Bank AS B ON B.BankID = AA.BankID
                                        WHERE V.RowStatus = 0 AND AA.RowStatus = 0 AND B.RowStatus = 0 AND B.ActiveStatusID = 1 AND V.VoucherNo IN ('{0}')", string.Join("','", prevData.Select(x => x.VoucherNo).ToArray()));
                var helperList = db.Query<PaymentOnlineEntity>(query).ToList();

                foreach (var item in prevData)
                    if (helperList.Exists(x => x.VoucherNo == item.VoucherNo))
                    {
                        item.BankAcquiringName = helperList.Where(x => x.VoucherNo == item.VoucherNo).FirstOrDefault().BankAcquiringName;
                        item.BankAcquiringAccountNo = helperList.Where(x => x.VoucherNo == item.VoucherNo).FirstOrDefault().BankAcquiringAccountNo;
                        item.VoucherAmount = helperList.Where(x => x.VoucherNo == item.VoucherNo).FirstOrDefault().VoucherAmount;
                    }
            }

            newData.AddRange(prevData);

            return newData;
        }
    }
}
