﻿using a2is.Framework.API;
using a2is.Framework.Monitoring;
using A2isMessaging;
using Microsoft.Reporting.WebForms;
using Otosales.PaymentOnline.dta;
using Otosales.PaymentOnline.dto;
using Otosales.Repository;
using OtosalesConsole.Logic;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace Otosales.PaymentOnline
{
    class Program: a2isAPIController
    {
        #region Properties

        private static readonly a2isLogHelper _log = new a2isLogHelper();
        private static string REPORT_DATE_TYPE = ConfigurationManager.AppSettings["DateType"].ToString(); //D or M or Y

        private string messageError = "An error has occurred. Please try again later";
        private string messageEmptyParam = "Error has occurred due to invalid parameter format";
        #endregion 

        #region Credential Class
        [Serializable]
        public sealed class MyReportServerCredentials :
        IReportServerCredentials
        {
            public WindowsIdentity ImpersonationUser
            {
                get
                {
                    // Use the default Windows user.  Credentials will be
                    // provided by the NetworkCredentials property.
                    return null;
                }
            }

            public ICredentials NetworkCredentials
            {
                get
                {
                    // Read the user information from the Web.config file.  
                    // By reading the information on demand instead of 
                    // storing it, the credentials will not be stored in 
                    // session, reducing the vulnerable surface area to the
                    // Web.config file, which can be secured with an ACL.

                    // User name
                    string userName =
                        WebConfigurationManager.AppSettings
                            ["MyReportViewerUser"];

                    if (string.IsNullOrEmpty(userName))
                        throw new Exception(
                            "Missing user name from web.config file");

                    // Password
                    string password =
                        WebConfigurationManager.AppSettings
                            ["MyReportViewerPassword"];

                    if (string.IsNullOrEmpty(password))
                        throw new Exception(
                            "Missing password from web.config file");

                    // Domain
                    string domain =
                        WebConfigurationManager.AppSettings
                            ["MyReportViewerDomain"];

                    if (string.IsNullOrEmpty(domain))
                        throw new Exception(
                            "Missing domain from web.config file");

                    return new NetworkCredential(userName, password, domain);
                }
            }

            public bool GetFormsCredentials(out Cookie authCookie,
                        out string userName, out string password,
                        out string authority)
            {
                authCookie = null;
                userName = null;
                password = null;
                authority = null;

                // Not using form credentials
                return false;
            }
        }
        #endregion
        static void Main(string[] args)
        {
            var actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            var _sqlClientPaymentOnlineService = new SqlClientPaymentOnline();

            try
            {
                _log.Error("Function " + actionName + " BEGIN : ");
                int backDateCount = Convert.ToInt32(ConfigurationManager.AppSettings["BackDateCount"].ToString());
                string dateType = ConfigurationManager.AppSettings["DateType"].ToString();
                bool isManual = Convert.ToBoolean(ConfigurationManager.AppSettings["IsManualGenerate"].ToString());

                DateTime endDate = DateTime.Now;
                DateTime startDate = new DateTime(1900, 1, 1);
                string cvStartDate = string.Empty;
                string cvEndDate = string.Empty;

                if (isManual)
                {
                    cvStartDate = ConfigurationManager.AppSettings["ReportFromDate"].ToString();
                    cvEndDate = ConfigurationManager.AppSettings["ReportToDate"].ToString();
                }
                else
                {
                    if (dateType == "D")
                    {
                        //startDate = endDate.AddDays(-backDateCount);
                        startDate = new DateTime(endDate.Year, endDate.Month, 1, 0, 0, 0);
                        endDate = new DateTime(endDate.Year, endDate.Month, endDate.Day, 23, 59, 59);

                        cvStartDate = startDate.ToString("yyyy-MM-dd HH:mm:ss.fff", System.Globalization.CultureInfo.InvariantCulture);
                        cvEndDate = endDate.AddDays(-1).ToString("yyyy-MM-dd HH:mm:ss.fff", System.Globalization.CultureInfo.InvariantCulture);

                        //vStartDate = DateTime.ParseExact(cvStartDate, "yyyy-MM-dd HH:mm:ss",null);
                        //vEndDate = DateTime.ParseExact(cvEndDate, "yyyy-MM-dd HH:mm:ss",null);
                    }
                    else if (dateType == "M")
                    {
                        startDate = endDate.AddMonths(-backDateCount);

                        cvStartDate = startDate.ToString("yyyy-MM-dd HH:mm:ss.fff", System.Globalization.CultureInfo.InvariantCulture);
                        cvEndDate = endDate.ToString("yyyy-MM-dd HH:mm:ss.fff", System.Globalization.CultureInfo.InvariantCulture);

                        //vStartDate = DateTime.ParseExact(cvStartDate, "yyyy-MM-dd HH:mm:ss",null);
                        //vEndDate = DateTime.ParseExact(cvEndDate, "yyyy-MM-dd HH:mm:ss",null);
                    }
                    else if (dateType == "Y")
                    {
                        startDate = endDate.AddYears(-backDateCount);

                        cvStartDate = startDate.ToString("yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                        cvEndDate = endDate.ToString("yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);

                        //vStartDate = DateTime.ParseExact(cvStartDate, "yyyy-MM-dd HH:mm:ss",System.Globalization.CultureInfo.InvariantCulture);
                        //vEndDate = DateTime.ParseExact(cvEndDate, "yyyy-MM-dd HH:mm:ss",System.Globalization.CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        startDate = endDate.AddDays(-backDateCount);

                        cvStartDate = startDate.ToString("yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                        cvEndDate = endDate.ToString("yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);

                        //vStartDate = DateTime.ParseExact(cvStartDate, "yyyy-MM-dd HH:mm:ss",System.Globalization.CultureInfo.InvariantCulture);
                        //vEndDate = DateTime.ParseExact(cvEndDate, "yyyy-MM-dd HH:mm:ss",System.Globalization.CultureInfo.InvariantCulture);
                    }
                }

                var paymentOnlineList = _sqlClientPaymentOnlineService.GetPaymentOnlineData(cvStartDate, cvEndDate);

                var paymentOnlineDataTable = ConvertPaymentOnlineMethodListIntoDataTable(paymentOnlineList);

                var paymentOnlineDoc = ExportUsingSimpleExcleWriter(paymentOnlineDataTable);

                A2isCommonResult res = sendEmail(paymentOnlineDoc, dateType);


                //string res = "Report Succesfully Created";
                string attachname = String.Format(ConfigurationManager.AppSettings["Attachmentname"].ToString(), DateTime.Now.ToString("ddMMyyyy"));
                _sqlClientPaymentOnlineService.InsertLog(attachname, res.ResultDesc, res.ResultCode);

                _log.Error("Function " + actionName + " END : ");
            }
            catch (Exception ex)
            {
                _log.Error("Function " + actionName + " Error : " + ex.ToString());
                string attachname = String.Format(ConfigurationManager.AppSettings["AttachmentName"].ToString(), DateTime.Now.ToString("ddMMyyyy"));
                string exc = ex.ToString();
                _sqlClientPaymentOnlineService.InsertLog(attachname, exc, false);
            }
        }

        private static A2isCommonResult sendEmail(byte[] document, string dateType)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                A2isMessageParam param = new A2isMessageParam();
                var fileExt = ConfigurationManager.AppSettings["FileExtension"].ToString();

                string sTime = string.Empty;
                if (dateType == "M")
                {
                    sTime = DateTime.Now.ToString("MMyyyy");
                    param.MessageFrom = ConfigurationManager.AppSettings["EmailFrom"].ToString();
                    param.MessageTo = Logic.CommonLogic.GetApplicationParametersValue("OTOSALES-EMAILTO-PAYMENTONLINE-MONTLY")[0];
                    param.MessageCC = Logic.CommonLogic.GetApplicationParametersValue("OTOSALES-EMAILCC-PAYMENTONLINE-MONTLY")[0];
                    param.MessageBcc = Logic.CommonLogic.GetApplicationParametersValue("OTOSALES-EMAILBCC-PAYMENTONLINE-MONTLY")[0];
                }
                else
                {
                    sTime = DateTime.Now.ToString("ddMMyyyy");
                    param.MessageFrom = ConfigurationManager.AppSettings["EmailFrom"].ToString();
                    param.MessageTo = Logic.CommonLogic.GetApplicationParametersValue("OTOSALES-EMAILTO-PAYMENTONLINE-DAILY")[0];
                    param.MessageCC = Logic.CommonLogic.GetApplicationParametersValue("OTOSALES-EMAILCC-PAYMENTONLINE-DAILY")[0];
                    param.MessageBcc = Logic.CommonLogic.GetApplicationParametersValue("OTOSALES-EMAILBCC-PAYMENTONLINE-DAILY")[0];
                }

                param.ApplicationRequest = "Otosales";
                param.ModuleRequest = "OrderPayment";
                param.MessageType = "MAIL"; // case sensitive type ={'MAIL','SMS'};
                param.MessageBody = REPORT_DATE_TYPE == "D" ? getMailBodyDaily() : getMailBodyMonthly();
                param.MessageSchedule = DateTime.Now.ToString(A2isMessaging.A2isCommonStatic.FormatDateTimeUI); // datetime dengan format dd/MM/yyyy HH:mm, tidak mandatory
                param.MessageSubject = "Report Payment Online Otosales " + sTime;
                param.MessagePriority = 0; //isinya 1 dan 0, kl tidak di isi 1 defaultnya  0
                param.MessageAttachment.Add(new A2isMessagingAttachment
                {
                    AttachmentDescription = String.Format(ConfigurationManager.AppSettings["AttachmentName"].ToString(), REPORT_DATE_TYPE == "D" ? DateTime.Now.ToString("ddMMyyyy") : DateTime.Now.ToString("MMMM yyyy")),
                    FileByte = document,
                    //FileContentType = fileContentType,
                    FileExtension = fileExt
                }); // isinya dari class MailAttachment A2isMessaging, dengan file sudah jadi format byte[]

                A2isCommonResult result = Messaging.ProcessQueuing(param); // ini untuk kirim.
                return result;
            }
            catch (Exception ex)
            {
                //_log.Error("Function " + actionName + " Error : " + e.ToString());
                throw ex;
            }
        }

        private static string getMailBodyDaily()
        {
            string today = DateTime.Now.ToString("dd-MM-yyyy");
            StringBuilder MailBody = new StringBuilder();
            MailBody.Append("Yth Bapak/Ibu,");
            MailBody.AppendLine("<br>");
            MailBody.AppendLine("<br>");
            MailBody.Append("Berikut terlampir data pembayaran online untuk Otosales harian, silakan dilanjutkan next proses.");
            MailBody.AppendLine("<br>");
            MailBody.AppendLine("<br>");
            MailBody.Append("Atas perhatian dan kerjasamanya, kami ucapkan terima kasih.");
            return MailBody.ToString();
        }

        private static string getMailBodyMonthly()
        {
            //string paramName = System.Configuration.ConfigurationManager.AppSettings["ParamName"].ToString();
            //string gardaAksesPhoneNo = System.Configuration.ConfigurationManager.AppSettings["gardaAksesPhoneNo"].ToString();
            //string asuransiastraPublicWebUrl = System.Configuration.ConfigurationManager.AppSettings["asuransiastraPublicWebUrl"].ToString();

            string today = DateTime.Now.ToString("dd-MM-yyyy");
            StringBuilder MailBody = new StringBuilder();
            MailBody.Append("Yth Bapak/Ibu,");
            MailBody.AppendLine("<br>");
            MailBody.AppendLine("<br>");
            MailBody.Append("Berikut terlampir data pembayaran online untuk Otosales bulan ini, silakan dilanjutkan next proses.");
            MailBody.AppendLine("<br>");
            MailBody.AppendLine("<br>");
            MailBody.Append("Atas perhatian dan kerjasamanya, kami ucapkan terima kasih.");
            return MailBody.ToString();
        }

        private static DataTable ConvertPaymentOnlineMethodListIntoDataTable(List<PaymentOnlineEntity> paymentOnlineList)
        {
            var dt = new DataTable();
            dt.Columns.Add("OrderNo", typeof(string));
            dt.Columns.Add("BankName", typeof(string));
            dt.Columns.Add("PaymentMethod", typeof(string));
            dt.Columns.Add("PaymentType", typeof(string));
            dt.Columns.Add("Installment", typeof(int));
            dt.Columns.Add("MDR", typeof(float));
            dt.Columns.Add("TransactionID", typeof(string));
            dt.Columns.Add("NomorKartuKredit", typeof(string));
            dt.Columns.Add("PaymentTime", typeof(string));
            dt.Columns.Add("AuthOrCaptureCC", typeof(string));
            dt.Columns.Add("ReferenceNo", typeof(string));
            dt.Columns.Add("NomorVA", typeof(string));
            dt.Columns.Add("BankAcquiringName", typeof(string));
            dt.Columns.Add("BankAcquiringAccountNo", typeof(string));
            dt.Columns.Add("SettleStatus", typeof(string));
            dt.Columns.Add("SettleDate", typeof(string));
            dt.Columns.Add("VoucherNo", typeof(string));
            dt.Columns.Add("VoucherStatus", typeof(string));
            //0219 URF 2019
            dt.Columns.Add("SegmentCode", typeof(string));
            dt.Columns.Add("CustomerType", typeof(string));
            //0219 URF 2019
            dt.Columns.Add("VoucherDate", typeof(string));
            dt.Columns.Add("VoucherAmount", typeof(decimal));
            dt.Columns.Add("Premium", typeof(decimal));
            dt.Columns.Add("Remarks", typeof(string));
            //0247 URF 2019
            dt.Columns.Add("ProductType", typeof(string));
            dt.Columns.Add("Discount", typeof(decimal));
            dt.Columns.Add("TotalPremium", typeof(decimal));

            if (paymentOnlineList.Count == 0)
            {
                DataRow dr = dt.NewRow();
                dr["OrderNo"] = "No data";
                dt.Rows.Add(dr);
            }
            else
                foreach (var paymentOnline in paymentOnlineList)
                {
                    DataRow dr = dt.NewRow();

                    dr["OrderNo"] = paymentOnline.OrderNo;
                    dr["BankName"] = paymentOnline.BankName;
                    dr["PaymentMethod"] = paymentOnline.PaymentMethodDescription;
                    dr["PaymentType"] = paymentOnline.PaymentType;
                    dr["Installment"] = paymentOnline.Installment;
                    dr["MDR"] = paymentOnline.MDR;
                    dr["TransactionID"] = paymentOnline.TransactionID;
                    dr["NomorKartuKredit"] = paymentOnline.CardNo;
                    dr["PaymentTime"] = paymentOnline.PaymentTime;
                    dr["AuthOrCaptureCC"] = paymentOnline.AuthOrCaptureCC;
                    dr["ReferenceNo"] = paymentOnline.ReferenceNo;
                    dr["NomorVA"] = paymentOnline.NomorVA;
                    dr["BankAcquiringName"] = paymentOnline.BankAcquiringName;
                    dr["BankAcquiringAccountNo"] = paymentOnline.BankAcquiringAccountNo;
                    dr["SettleStatus"] = paymentOnline.SettleStatus;
                    dr["SettleDate"] = paymentOnline.SettleDate;
                    dr["VoucherNo"] = paymentOnline.VoucherNo;
                    dr["VoucherStatus"] = paymentOnline.VoucherStatus;
                    //0219 URF 2019
                    dr["SegmentCode"] = paymentOnline.segmentCode;
                    dr["CustomerType"] = paymentOnline.CustomerType;
                    //0219 URF 2019
                    dr["VoucherDate"] = paymentOnline.VoucherDate;
                    dr["VoucherAmount"] = paymentOnline.VoucherAmount;
                    dr["Premium"] = paymentOnline.Premium;
                    dr["Remarks"] = paymentOnline.Remarks;
                    //0247 URF 2019
                    dr["ProductType"] = paymentOnline.ProductType;
                    dr["Discount"] = paymentOnline.Discount;
                    dr["TotalPremium"] = paymentOnline.TotalPremi;

                    dt.Rows.Add(dr);
                }

            return dt;
        }

        public static byte[] ExportUsingSimpleExcleWriter(DataTable table)
        {
            byte[] resultDoc = null;
            int col = 0;
            int row = 0;

            string _directoryPath = string.Empty;
            string _fileName = string.Empty;

            _directoryPath = ConfigurationManager.AppSettings["FilePath"];
            switch (REPORT_DATE_TYPE)
            {
                case "D":
                    _directoryPath = string.Concat(_directoryPath, "Konvensional\\Otosales\\ReportDailyPaymentOnline\\", DateTime.Now.ToString("yyyy"), "\\", DateTime.Now.ToString("MMMM"), "\\");
                    _fileName = string.Format(ConfigurationManager.AppSettings["AttachmentName"], DateTime.Now.ToString("ddMMyyyy"));
                    break;
                case "M":
                    _directoryPath = string.Concat(_directoryPath, "Konvensional\\Otosales\\ReportMonthlyPaymentOnline\\", DateTime.Now.ToString("yyyy"), "\\");
                    _fileName = string.Format(ConfigurationManager.AppSettings["AttachmentName"], DateTime.Now.ToString("MMyyyy"));
                    break;
            }

            if (!Directory.Exists(_directoryPath))
                Directory.CreateDirectory(_directoryPath);

            FileStream stream = new FileStream(string.Concat(_directoryPath, _fileName), FileMode.OpenOrCreate);
            ExcelWriterLogic writer = new ExcelWriterLogic(stream);

            writer.BeginWrite();

            foreach (DataColumn dataColumn in table.Columns)
            {
                writer.WriteCell(0, col, dataColumn.ColumnName);
                col++;
            }

            row = 1;
            foreach (DataRow dataRow in table.Rows)
            {
                col = 0;
                foreach (DataColumn dataColumn in table.Columns)
                {
                    writer.WriteCell(row, col, dataRow[dataColumn].ToString());
                    col++;
                }
                row++;
            }

            writer.EndWrite();
            stream.Close();

            resultDoc = File.ReadAllBytes(string.Concat(_directoryPath, _fileName));
            return resultDoc;
        }
    }
}
