﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otosales.PaymentOnline.dto
{
    public class PaymentOnlineEntity
    {
        public string PolicyNo { get; set; }

        public string TransactionID { get; set; }
        public string OrderNo { get; set; }
        public string PaymentMethodCode { get; set; }
        public string PaymentMethodDescription { get; set; }
        public string BankCode { get; set; }
        public string BankName { get; set; }
        public string PaymentType { get; set; }
        public int Installment { get; set; }
        public float MDR { get; set; }
        public string PaymentTime { get; set; }
        public string AuthOrCaptureCC { get; set; }
        public string ReferenceNo { get; set; }
        public string NomorVA { get; set; }
        public string BankAcquiringName { get; set; }
        public string BankAcquiringAccountNo { get; set; }
        public decimal PaidAmount { get; set; }
        public decimal SettledAmount { get; set; }
        public string SettleStatus { get; set; }
        public string SettleDate { get; set; }
        public string VoucherNo { get; set; }
        public string VoucherStatus { get; set; }
        public string VoucherDate { get; set; }
        public decimal VoucherAmount { get; set; }
        public decimal Premium { get; set; }
        public string Remarks { get; set; }
        //0219 URF 2019
        public string segmentCode { get; set; }
        //0247 URF 2019
        public string ProductType { get; set; }
        public decimal Discount { get; set; }
        public decimal TotalPremi { get; set; }

        //0090URF2020
        public string CardNo { get; set; }
        public string CustomerType { get; set; }
    }

}
