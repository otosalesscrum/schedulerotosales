﻿using a2is.Framework.DataAccess;
using Otosales.ReportFollowUp.dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otosales.ReportFollowUp.dta
{
    public class FollowUpAccess
    {
        public static List<FollowUpRenew> GetFollowUpRenew(string cvStartDate, string cvEndDate)
        {
            var result = new List<FollowUpRenew>();
            var rhelper = new List<FollowUpRenew>();
            string query = string.Empty;

            #region Get Follow Up New Data AABMobile

            using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobileDB"))
            {
                query = string.Format(@"; 
                          SELECT  COALESCE(LTRIM(RTRIM(os.orderNo)), '') AS orderNo ,
        os.TotalPremium AS premi ,
        COALESCE(LTRIM(RTRIM(os.policyOrderNo)), '') AS policyOrderNo ,
        COALESCE(LTRIM(RTRIM(os.SalesOfficerID)), '') AS SalesOfficerID ,
        os.EntryDate AS orderDate ,
        COALESCE(LTRIM(RTRIM(pc.Name)), '') AS prospectName ,
        CASE WHEN osm.isNew = 1 THEN 'New'
             ELSE 'Used'
        END AS isUsedCar ,
        COALESCE(LTRIM(RTRIM(osm.VehicleCode)), '') AS vcode ,
        COALESCE(LTRIM(RTRIM(vb.Description)), '') AS vBrand ,
        COALESCE(LTRIM(RTRIM(osm.Year)), '') AS vYear ,
        COALESCE(LTRIM(RTRIM(vm.Description)), '') AS vType ,
        COALESCE(LTRIM(RTRIM(osm.Series)), '') AS vSeries ,
        COALESCE(LTRIM(RTRIM(u.Description)), '') AS usage ,
        COALESCE(LTRIM(RTRIM(r.Description)), '') AS region ,
        COALESCE(LTRIM(RTRIM(osm.RegistrationNumber)), '') AS RegistrationNumber ,
        COALESCE(LTRIM(RTRIM(osm.[EngineNumber])), '') AS engineNumber ,
        COALESCE(LTRIM(RTRIM(osm.[ChasisNumber])), '') AS chasisNumber ,
        CASE WHEN mp.Biz_type = 2 THEN 'Garda Oto Syariah'
             ELSE pt.Description
        END AS productType ,
        COALESCE(LTRIM(RTRIM(os.ProductCode)), '') AS productCode ,
        CONCAT(( CASE WHEN COALESCE(os.ComprePeriod, 0) = 0 THEN ''
                      ELSE CONCAT(os.ComprePeriod, ' Tahun Comprehensive ')
                 END ), ( CASE WHEN COALESCE(os.TLOPeriod, 0) = 0 THEN ''
                               ELSE CONCAT(os.TLOPeriod, ' Tahun TLO')
                          END )) AS BasicCover ,
        osm.SumInsured ,
        COALESCE(LTRIM(RTRIM(fus.Description)), '') AS fuStatus ,
        fu.LastFollowUpDate AS fuDate ,
        COALESCE(LTRIM(RTRIM(fu.FollowUpNotes)), '') AS fuNotes ,
        COALESCE(LTRIM(RTRIM(fusi.Description)), '') AS fuReason ,
        COALESCE(MAX(LTRIM(RTRIM(fuh.SeqNo))), '') AS callFreq ,
        CASE WHEN COALESCE(os.IsNeedDocRep, 0) = 1 THEN 'YES'
             ELSE 'NO'
        END AS isDocRep ,
        CASE WHEN pc.SalesDealer = os.SalesDealer THEN 'NO'
             ELSE 'YES'
        END AS isSalesDealerChange ,
        os.EntryDate AS transactionDate ,
        COALESCE(LTRIM(RTRIM(os.PolicyNo)), '') AS PolicyNo ,
        os.PeriodFrom AS newPeriodFrom ,
        os.PeriodTo AS newPeriodTo ,
        'Renewable' AS isRenewable ,
        COALESCE(LTRIM(RTRIM(dn.Description)), '') AS newDealerName ,
        COALESCE(LTRIM(RTRIM(sd.Description)), '') AS newSalesmanDealerName ,
        COALESCE(LTRIM(RTRIM(os.OldPolicyNo)), '') AS OldPolicyNo,
		fu.Remark,
		fusn.Description AS FollowUpNotes
FROM    [dbo].[OrderSimulation] os
        INNER JOIN [dbo].[FollowUp] fu ON fu.FollowUpNo = os.FollowUpNo
        INNER JOIN dbo.Mst_Product mp ON mp.Product_Code = os.ProductCode
        INNER JOIN [dbo].[OrderSimulationMV] osm ON os.OrderNo = osm.OrderNo
        INNER JOIN [dbo].[ProspectCustomer] pc ON pc.CustID = os.CustID
        INNER JOIN [dbo].[VehicleBrand] vb ON vb.BrandCode = osm.BrandCode
        INNER JOIN [dbo].[VehicleModel] vm ON vm.ModelCode = osm.ModelCode
        INNER JOIN BeyondReport.AAB.[dbo].[Dtl_Ins_Factor] u ON osm.UsageCode = u.insurance_code
                                                              AND u.factor_code = 'mvusag'
                                                              AND u.status = 1
        INNER JOIN [dbo].[Region] r ON r.RegionCode = osm.CityCode
        INNER JOIN [dbo].[ProductType] pt ON pt.InsuranceType = os.InsuranceType
        INNER JOIN [dbo].[FollowUpStatus] fus ON fu.[FollowUpStatus] = fus.StatusCode
        INNER JOIN [dbo].[FollowUpStatusInfo] fusi ON fusi.[InfoCode] = fu.[FollowUpInfo]
		INNER JOIN dbo.FollowUpStatusNotes fusn ON fusn.NotesCode = fu.FollowUpNotes
        LEFT JOIN [dbo].[FollowUpHistory] fuh ON fuh.FollowUpNo = fu.FollowUpNo
        LEFT JOIN [dbo].[SalesmanDealer] sd ON sd.SalesmanCode = os.SalesDealer
        LEFT JOIN [dbo].[Dealer] dn ON dn.DealerCode = os.DealerCode
        LEFT JOIN [dbo].[SalesOfficer] so ON so.SalesOfficerID = os.SalesOfficerID
WHERE   os.EntryDate BETWEEN '{0}'
                     AND     '{1}'
        AND COALESCE(fu.IsRenewal, 0) = 1
GROUP BY os.orderNo ,
        os.TotalPremium ,
        os.policyOrderNo ,
        os.EntryDate ,
        pc.Name ,
        os.SalesOfficerID ,
        osm.isNew ,
        osm.VehicleCode ,
        vb.Description ,
        osm.Year ,
        vm.Description ,
        osm.Series ,
        u.Description ,
        r.Description ,
        osm.RegistrationNumber ,
        osm.[EngineNumber] ,
        osm.[ChasisNumber] ,
        mp.Biz_type ,
        pt.Description ,
        os.ProductCode ,
        os.ComprePeriod ,
        os.TLOPeriod ,
        osm.SumInsured ,
        fus.Description ,
        fu.LastFollowUpDate ,
        fu.FollowUpNotes ,
        fusi.Description ,
        os.IsNeedDocRep ,
        pc.SalesDealer ,
        os.PolicyNo ,
        os.OldPolicyNo ,
        os.PeriodFrom ,
        os.PeriodTo ,
        dn.Description ,
        sd.Description ,
        pc.SalesDealer ,
        os.SalesDealer,
		fu.Remark,
		fusn.Description

                ", cvStartDate, cvEndDate);

                result = db.Query<FollowUpRenew>(query).ToList();
            }
            #endregion

            #region Get Follow Up Old Data From AAB
            if (result.Count > 0)
            {
                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AAB2000DB"))
                {
                    query = string.Format(@";select coalesce(LTRIM(RTRIM(p.policy_no)),'') as OldPolicyNo
	                        ,coalesce(LTRIM(RTRIM(p.salesman_id)),'') as oldSalesOfficerID
	                        ,p.Period_From as oldPeriodFrom
	                        ,p.Period_To as oldPeriodTo
	                        ,coalesce(LTRIM(RTRIM(mc.Name)),'') as oldDealerName
	                        ,coalesce(LTRIM(RTRIM(mc1.Name)),'') as oldSalesmanDealerName
	                        ,coalesce(LTRIM(RTRIM(rdo.claim_Frequency)),'') as claimFreq
	                        ,coalesce(LTRIM(RTRIM(rdo.claim_ratio)),'') as ClaimRatio
                            ,COALESCE(LTRIM(RTRIM(o.uplinerName)),'-') as oldUplinerName
	                        ,COALESCE(LTRIM(RTRIM(o.leaderName)),'-') as oldLeaderName	
                        from [AAB].[dbo].[Policy] p
						inner join mst_product mp on mp.product_code = p.product_code
                        left join [AAB].[dbo].comsales_salesman cs WITH (NOLOCK) ON p.broker_code = cs.cust_id
                        LEFT JOIN AAB.[dbo].mst_customer mc WITH (NOLOCK) ON mc.cust_id = cs.dealer_id
                        left join AAB.[dbo]. mst_customer mc1 WITH (NOLOCK) on mc1.Cust_id = p.broker_code
                        LEFT JOIN AAB.dbo.ren_dtl_object rdo WITH (NOLOCK)  on rdo.policy_no=p.Policy_No
                        left join [AAB].[dbo].mst_salesman msl WITH (NOLOCK)  on msl.salesman_id = p.salesman_id
                        left join (SELECT    b.Cust_Id AS CustomerID,
		                                    a.Client_Code AS ClientCode,
						                                    RTRIM(b.Name) AS ClientName,                                 
						                                    c.Cust_Id AS UplinerCustomerID,
		                                    a.Upliner_Client_Code AS UplinerCode ,
		                                    RTRIM(d.Name) AS UplinerName,
		                                    e.Cust_Id AS LeaderCustomerID,
		                                    a.Leader_Client_Code AS LeaderCode ,                  
		                                    RTRIM(f.Name) AS LeaderName                
                                    FROM      AAB.dbo.dtl_cust_type a WITH ( NOLOCK )
                                    INNER JOIN AAB.dbo.mst_customer b WITH ( NOLOCK ) ON a.cust_id = b.cust_id                    
                                    LEFT JOIN AAB.dbo.dtl_cust_type c WITH ( NOLOCK ) ON a.Upliner_Client_Code=c.Client_Code
                                    LEFT JOIN AAB.dbo.mst_customer d WITH ( NOLOCK ) ON c.Cust_Id = d.cust_id                    
                                    LEFT JOIN AAB.dbo.dtl_cust_type e WITH ( NOLOCK ) ON a.Leader_Client_Code=e.Client_Code
                                    LEFT JOIN AAB.dbo.mst_customer f WITH ( NOLOCK ) ON e.Cust_Id = f.cust_id                    
                                    WHERE     a.Client_Type = 'AGENT') o on o.CustomerID = p.salesman_id OR o.ClientCode = p.salesman_id
                        where p.Policy_No in ('{0}')",
                    String.Join("','", result.Where(y => !string.IsNullOrEmpty(y.oldPolicyNo)).Select(x => x.oldPolicyNo.Trim()).ToArray()));

                    rhelper = db.Query<FollowUpRenew>(query).ToList();
                }
            }

            foreach (var item in result)
            {
                if (rhelper.Exists(x => !string.IsNullOrEmpty(x.oldPolicyNo) && !string.IsNullOrEmpty(item.oldPolicyNo) && x.oldPolicyNo.Trim() == item.oldPolicyNo.Trim()))
                {
                    var firstData = rhelper.Where(x => x.oldPolicyNo.Trim() == item.oldPolicyNo.Trim()).FirstOrDefault();
                    item.oldDealerName = firstData.oldDealerName;
                    item.oldPeriodFrom = firstData.oldPeriodFrom;
                    item.oldPeriodTo = firstData.oldPeriodTo;
                    item.oldSalesmanDealerName = firstData.oldSalesmanDealerName;
                    item.oldLeadersName = firstData.oldLeadersName;
                    item.oldUplineName = firstData.oldUplineName;
                    item.claimFreq = firstData.claimFreq;
                    item.claimRatio = firstData.claimRatio;
                }
            }

            #endregion

            #region add additional data to result
            result = AddAdditionalDataRenew(result);
            result = result.OrderBy(x => x.orderDate).ToList();
            #endregion

            return result;
        }
        
        public static List<FollowUpNew> GetFollowUpNew(string cvStartDate, string cvEndDate)
        {
            var result = new List<FollowUpNew>();
            string query = string.Empty;

            #region Get Follow Up New Data AABMobile

            using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobileDB"))
            {
                query = string.Format(@";
                        SELECT 
	                        coalesce(LTRIM(RTRIM(os.orderNo)),'') as orderNo
                            ,os.TotalPremium as premi
                            ,coalesce(LTRIM(RTRIM(os.policyOrderNo)),'') as policyOrderNo
	                        ,coalesce(LTRIM(RTRIM(os.SalesOfficerID)),'') as SalesOfficerID
	                        ,os.EntryDate as orderDate
	                        ,coalesce(LTRIM(RTRIM(pc.Name)),'') as prospectName
	                        ,COALESCE(LTRIM(RTRIM(d.Description)),'-') as dealerName
	                        ,COALESCE(LTRIM(RTRIM(sd.Description)),'-') as salesmanDealerName
	                        ,CASE WHEN LEN(RTRIM(os.SalesOfficerID)) > 3 THEN coalesce(so.Name,'')
		                        ELSE '-' END as agentName
	                        ,CASE WHEN osm.isNew = 1 THEN 'New'
		                        ELSE 'Used' END
	                        as isUsedCar
	                        ,coalesce(LTRIM(RTRIM(osm.VehicleCode)),'') as vcode
	                        ,coalesce(LTRIM(RTRIM(vb.Description)),'') as vBrand
	                        ,coalesce(LTRIM(RTRIM(osm.Year)),'') as vYear
	                        ,coalesce(LTRIM(RTRIM(vm.Description)),'') as vType
	                        ,coalesce(LTRIM(RTRIM(osm.Series)),'') as vSeries
	                        ,coalesce(LTRIM(RTRIM(u.Description)),'') as usage
	                        ,coalesce(LTRIM(RTRIM(r.Description)),'') as region
	                        ,coalesce(LTRIM(RTRIM(osm.RegistrationNumber)),'') as RegistrationNumber
	                        ,coalesce(LTRIM(RTRIM(osm.[EngineNumber])),'') as engineNumber
                            ,coalesce(LTRIM(RTRIM(osm.[ChasisNumber])),'') as chasisNumber
	                        ,CASE WHEN mp.Biz_type = 2 THEN 'Garda Oto Syariah' 
			                        ELSE pt.Description END
	                        as productType
	                        ,coalesce(LTRIM(RTRIM(os.ProductCode)),'') as productCode
	                        ,CONCAT((CASE WHEN COALESCE(os.ComprePeriod,0) = 0 THEN ''
		                        ELSE CONCAT(os.ComprePeriod,' Tahun Comprehensive ') END),(CASE WHEN COALESCE(os.TLOPeriod,0) = 0 THEN ''
		                        ELSE CONCAT(os.TLOPeriod,' Tahun TLO') END))
	                        as BasicCover
	                        ,osm.SumInsured
	                        ,coalesce(LTRIM(RTRIM(fus.Description)),'') as fuStatus
	                        ,fu.LastFollowUpDate as fuDate
	                        ,coalesce(LTRIM(RTRIM(fu.FollowUpNotes)),'') as fuNotes
	                        ,coalesce(LTRIM(RTRIM(fusi.Description)),'') as fuReason
	                        ,coalesce(MAX(LTRIM(RTRIM(fuh.SeqNo))),'') as callFreq
	                        ,CASE WHEN COALESCE(os.IsNeedDocRep,0) = 1 THEN 'YES'
		                        ELSE 'NO' END AS isDocRep
	                        ,CASE WHEN pc.SalesDealer = os.SalesDealer THEN 'NO'
		                        ELSE 'YES' END
	                        as isSalesDealerChange
                            ,os.EntryDate as transactionDate
	                        ,coalesce(LTRIM(RTRIM(os.PolicyNo)),'') as PolicyNo
	                        ,os.PeriodFrom as PeriodFrom
	                        ,os.PeriodTo as PeriodTo
                        FROM [dbo].[OrderSimulation] os
                        INNER JOIN dbo.Mst_Product mp ON mp.Product_Code = os.ProductCode
                        INNER JOIN [dbo].[OrderSimulationMV] osm ON os.OrderNo = osm.OrderNo
                        INNER JOIN [dbo].[ProspectCustomer] pc ON pc.CustID = os.CustID
                        INNER JOIN [dbo].[VehicleBrand] vb ON vb.BrandCode = osm.BrandCode
                        INNER JOIN [dbo].[VehicleModel] vm on vm.ModelCode = osm.ModelCode
                        INNER JOIN BeyondReport.AAB.[dbo].[Dtl_Ins_Factor] u on osm.UsageCode = u.insurance_code and u.factor_code = 'mvusag' and u.status = 1
                        INNER JOIN [dbo].[SalesOfficer] so ON so.SalesOfficerID = os.SalesOfficerID
                        INNER JOIN [dbo].[Region] r ON r.RegionCode = osm.CityCode
                        INNER JOIN [dbo].[ProductType] pt ON pt.InsuranceType = os.InsuranceType
                        INNER JOIN [dbo].[FollowUp] fu ON fu.FollowUpNo = os.FollowUpNo
                        INNER JOIN [dbo].[FollowUpStatus] fus ON fu.[FollowUpStatus] = fus.StatusCode
                        INNER JOIN [dbo].[FollowUpStatusInfo] fusi ON fusi.[InfoCode] = fu.[FollowUpInfo]
                        LEFT JOIN [dbo].[FollowUpHistory] fuh ON fuh.FollowUpNo = fu.FollowUpNo
                        LEFT JOIN [dbo].[Dealer] d ON d.DealerCode = os.DealerCode
                        LEFT JOIN [dbo].[SalesmanDealer] sd ON sd.SalesmanCode = os.SalesDealer
                        WHERE os.EntryDate BETWEEN '{0}' AND '{1}'
	                        AND COALESCE(fu.IsRenewal,0) = 0
                        GROUP BY os.orderNo,os.policyOrderNo,os.TotalPremium,os.EntryDate,pc.Name,d.Description,os.SalesOfficerID,so.Name,osm.isNew,osm.VehicleCode
	                        ,vb.Description,osm.Year,vm.Description,osm.Series,u.Description,r.Description,osm.RegistrationNumber
	                        ,osm.[EngineNumber],osm.[ChasisNumber],mp.Biz_type,pt.Description,os.ProductCode,os.ComprePeriod,os.TLOPeriod,osm.SumInsured
	                        ,fus.Description,fu.LastFollowUpDate,fu.FollowUpNotes,fusi.Description,os.IsNeedDocRep,pc.SalesDealer,os.SalesDealer,os.PolicyNo,os.PeriodFrom,os.PeriodTo
                            ,sd.Description", cvStartDate,cvEndDate);

                result = db.Query<FollowUpNew>(query).ToList();
            }
            #endregion
            #region add additional data to result
            result = AddAdditionalDataNew(result);
            result = result.OrderBy(x => x.orderDate).ToList();
            #endregion
            return result;
        }

        public static List<RawData> GetRawData(string cvStartDate, string cvEndDate)
        {
            var result = new List<RawData>();
            var orderno = new List<string>();
            string query = string.Empty;

            #region Get data order from AABMobile 
            //using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobileDB"))
            //{
            //    query = string.Format(@";SELECT PolicyOrderNo  FROM [AABMobile].[dbo].[OrderSimulation] os
            //                          INNER JOIN FollowUp fu ON os.FollowUpNo = fu.FollowUpNo 
            //                          WHERE os.[EntryDate] BETWEEN '{0}' and '{1}'
            //                          AND os.PolicyOrderNo IS NOT NULL
            //                          AND fu.[FollowUpStatus] = '13'", cvStartDate, cvEndDate);
            //    orderno = db.Query<string>(query).ToList();
            //}
            using (a2isDBHelper.Database db = new a2isDBHelper.Database("AAB2000DB"))
            {
                query = string.Format(@";SELECT distinct mo.order_no FROM policy p
									  INNER JOIN mst_order mo ON mo.policy_id = p.policy_id
									  --INNER JOIN beyondmoss.aabmobile.dbo.ordersimulation os on mo.order_no = os.policyorderno
									  where mo.order_status IN ('9','11') and p.Status = 'A'
									  and p.Production_Date BETWEEN '{0}' and '{1}'", cvStartDate, cvEndDate);
                orderno = db.Query<string>(query).ToList();
            }
            using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobileDB"))
            {
                query = string.Format(@";SELECT PolicyOrderNo FROM [AABMobile].[dbo].[OrderSimulation]
                                      WHERE PolicyOrderNo IN('{0}')", string.Join("','", orderno.ToArray()));
                orderno = db.Query<string>(query).ToList();
            }
            #endregion

            #region Get Raw Data From AAB

            using (a2isDBHelper.Database db = new a2isDBHelper.Database("AAB2000DB"))
            {
                #region query lama
                //             query = string.Format(@";SELECT DISTINCT coalesce(LTRIM(RTRIM(dt.Transaction_No)),'') as transactionNo
                //       ,coalesce(LTRIM(RTRIM(mo.Policy_no)),'') as policyNo
                //       ,coalesce(LTRIM(RTRIM(mo.endorsement_no)),'-') as endorsementNo
                //       ,coalesce(LTRIM(RTRIM(mo.Name_On_Policy)),'') as nameOnPolicy
                //       ,CASE WHEN mo.order_type = '1' THEN 'New'
                //            WHEN mo.order_type = '2' THEN 'Renew'
                //            WHEN mo.order_type = '3' THEN 'Endorse'
                //            WHEN mo.order_type = '4' THEN 'Cancel'
                //        ELSE '' END
                //       as orderType
                //             ,mo.Period_From as periodFrom
                //       ,mo.Period_To as periodTo
                //       ,mo.entryDt as inputDate
                //       ,p.production_date as approveDate
                //       ,coalesce(LTRIM(RTRIM(mo.Product_code)),'') as productCode
                //       ,coalesce(replace(LTRIM(RTRIM(mp.description)),'–','-'),'') as productName
                //       ,coalesce(LTRIM(RTRIM(mo.segment_code)),'') as segmentCode
                //       ,coalesce(LTRIM(RTRIM(mo.EntryUsr)),'') as entryUser
                //       ,coalesce(LTRIM(RTRIM(mb.nama)),'') as branchName
                //       ,di.sum_insured as sumInsured
                //       ,coalesce(LTRIM(RTRIM(ms.Name)),'') as businessSource
                //       ,coalesce(LTRIM(RTRIM(dm.vehicle_code)),'') as vCode
                //       ,coalesce(LTRIM(RTRIM(vb.description)),'') as vBrand
                //       ,concat(coalesce(LTRIM(RTRIM(vm.description)),''),' ',coalesce(LTRIM(RTRIM(dm.series)),'')) as objectDescription
                //       ,dm.mfg_year as mfgYear
                //       ,coalesce(LTRIM(RTRIM(dm.chasis_number)),'') as chasisNumber
                //       ,coalesce(LTRIM(RTRIM(dm.engine_number)),'') as engineNumber
                //       ,coalesce(LTRIM(RTRIM(dm.registration_number)),'') as registrationNumber
                //       ,STUFF((
                //			SELECT ',' + data.Total_Year + ' Year ' + CASE WHEN data.Coverage_id = 'ALLRIK' THEN 'Comprehensive' ELSE 'Total Loss Only' END
                //			FROM (select CAST(count(*) AS VARCHAR) as Total_Year, Coverage_Id 
                //						from Dtl_Interest di1 
                //						inner join dtl_coverage dc1 on dc1.interest_no = di1.interest_no
                //						and di1.Policy_Id = dc1.Policy_Id 
                //						and di1.interest_id = 'CASCO' 
                //						and dc1.coverage_id in ('ALLRIK','TLO')
                //						where di1.policy_id = di.policy_id 
                //						group by coverage_id) data
                //			FOR XML PATH('')
                //			), 1, 1, '') as basicCover
                //       ,coalesce(LTRIM(RTRIM(cs.dealer_id)),'-') as dealerID
                //       ,coalesce(LTRIM(RTRIM(mc.name)),'-') as dealerBroker
                //       ,coalesce(LTRIM(RTRIM(nullif(mo.Broker_Code,''))),'-') as salesmanCode
                //	,coalesce(LTRIM(RTRIM(mc1.Name)),'-') as salesmanName
                //       ,coalesce(LTRIM(RTRIM(x.uplinerName)),'-') as uplinerName
                //       ,coalesce(LTRIM(RTRIM(x.leaderName)),'-') as leaderName
                //	,case when mp.product_type = '6' then mo.Broker_code else coalesce(LTRIM(RTRIM(msl.User_Id_Otosales)),'') end as userOtosales
                //         from mst_order mo
                //         inner join policy p WITH (NOLOCK) on p.policy_id = mo.policy_id
                //         inner join mst_branch mb WITH (NOLOCK) on mb.branch_id = mo.branch_id
                //         inner join mst_product mp WITH (NOLOCK) on mp.product_code = mo.product_code
                //         inner join mst_sob ms WITH (NOLOCK) on ms.sob_id = mo.segment_code
                //         inner join dtl_mv dm WITH (NOLOCK) on dm.policy_id = mo.policy_id and p.endorsement_no = dm.endorsement_no
                //         inner join mst_vehicle_brand vb WITH (NOLOCK) on vb.brand_id = dm.brand_code
                //         inner join mst_vehicle_model vm WITH (NOLOCK) on vm.model_id = dm.model_code 
                //inner join dtl_interest di WITH (NOLOCK) on di.policy_id = mo.policy_id and di.interest_type = 'P' and di.interest_id = 'CASCO'
                //         inner join dtl_coverage dc WITH (NOLOCK) on dc.policy_id = mo.policy_id and di.interest_no = dc.interest_no and dc.coverage_id IN ('ALLRIK','TLO') 
                //left join mst_customer mc1 WITH (NOLOCK) on mc1.Cust_id = mo.broker_code
                //left join [AAB].[dbo].comsales_salesman cs WITH (NOLOCK) ON mo.broker_code = cs.cust_id
                //LEFT JOIN AAB.[dbo].mst_customer mc WITH (NOLOCK) ON mc.cust_id = cs.dealer_id
                //         left join mst_cust_type mct WITH (NOLOCK) on mct.cust_id = mc.Cust_id and mct.client_type IN ('DEALER','BROKER')
                //         left join mst_salesman msl WITH (NOLOCK) on msl.salesman_id = mo.salesman_id
                //         left join (SELECT    b.Cust_Id AS CustomerID,
                //	                                a.Client_Code AS ClientCode,
                //					                                RTRIM(b.Name) AS ClientName,                                 
                //					                                c.Cust_Id AS UplinerCustomerID,
                //	                                a.Upliner_Client_Code AS UplinerCode ,
                //	                                RTRIM(d.Name) AS UplinerName,
                //	                                e.Cust_Id AS LeaderCustomerID,
                //	                                a.Leader_Client_Code AS LeaderCode ,                  
                //	                                RTRIM(f.Name) AS LeaderName                    
                //			                                FROM      dtl_cust_type a WITH ( NOLOCK )
                //			                                INNER JOIN mst_customer b WITH ( NOLOCK ) ON a.cust_id = b.cust_id                    
                //			                                LEFT JOIN dtl_cust_type c WITH ( NOLOCK ) ON a.Upliner_Client_Code=c.Client_Code
                //			                                LEFT JOIN mst_customer d WITH ( NOLOCK ) ON c.Cust_Id = d.cust_id                    
                //			                                LEFT JOIN dtl_cust_type e WITH ( NOLOCK ) ON a.Leader_Client_Code=e.Client_Code
                //			                                LEFT JOIN mst_customer f WITH ( NOLOCK ) ON e.Cust_Id = f.cust_id                    
                //			                                WHERE     a.Client_Type = 'AGENT'
                //		                                ) AS X ON  X.CustomerID = mo.cust_id OR X.ClientCode = mo.cust_id
                //         left join Dtl_Transaction dt on dt.Reference_No = mo.Ref_No 
                //where mo.order_status IN ('9','11')
                //             and mo.entryDt BETWEEN '{0}' and '{1}'
                //	and mo.order_no in ('{2}')
                //	and p.Status = 'A'", cvStartDate, cvEndDate, string.Join("','", orderno.ToArray()));
                #endregion query lama

                query = string.Format(@"; SELECT DISTINCT coalesce(LTRIM(RTRIM(dt.Transaction_No)),'') as transactionNo
		                                ,coalesce(LTRIM(RTRIM(mo.Policy_no)),'') as policyNo
                                        ,coalesce(LTRIM(RTRIM(mo.order_no)),'') as orderNo
		                                ,coalesce(LTRIM(RTRIM(mo.endorsement_no)),'-') as endorsementNo
		                                ,coalesce(LTRIM(RTRIM(mo.Name_On_Policy)),'') as nameOnPolicy
		                                ,CASE WHEN mo.order_type = '1' THEN 'New'
			                                    WHEN mo.order_type = '2' THEN 'Renew'
			                                    WHEN mo.order_type = '3' THEN 'Endorse'
			                                    WHEN mo.order_type = '4' THEN 'Cancel'
			                                ELSE '' END
		                                as orderType
                                        ,mo.Period_From as periodFrom
		                                ,mo.Period_To as periodTo
		                                ,mo.entryDt as inputDate
		                                ,p.production_date as approveDate
		                                ,coalesce(LTRIM(RTRIM(mo.Product_code)),'') as productCode
		                                ,coalesce(replace(LTRIM(RTRIM(mp.description)),'–','-'),'') as productName
		                                ,coalesce(LTRIM(RTRIM(ms.Name)),'') as segmentCode
		                                ,coalesce(LTRIM(RTRIM(mo.EntryUsr)),'') as entryUser
		                                ,coalesce(LTRIM(RTRIM(mb.nama)),'') as branchName
		                                ,di.sum_insured as sumInsured
		                                ,coalesce(LTRIM(RTRIM(ms.Name)),'') as businessSource
		                                ,coalesce(LTRIM(RTRIM(dm.vehicle_code)),'') as vCode
		                                ,coalesce(LTRIM(RTRIM(vb.description)),'') as vBrand
		                                ,concat(coalesce(LTRIM(RTRIM(vm.description)),''),' ',coalesce(LTRIM(RTRIM(dm.series)),'')) as objectDescription
		                                ,dm.mfg_year as mfgYear
		                                ,coalesce(LTRIM(RTRIM(dm.chasis_number)),'') as chasisNumber
		                                ,coalesce(LTRIM(RTRIM(dm.engine_number)),'') as engineNumber
		                                ,coalesce(LTRIM(RTRIM(dm.registration_number)),'') as registrationNumber
		                                ,coalesce(LTRIM(RTRIM(cs.dealer_id)),'-') as dealerID
		                                ,coalesce(LTRIM(RTRIM(mc.name)),'-') as dealerBroker
		                                ,coalesce(LTRIM(RTRIM(nullif(mo.Broker_Code,''))),'-') as salesmanCode
				                        ,coalesce(LTRIM(RTRIM(mc1.Name)),'-') as salesmanName
				                        ,case when mp.product_type = '6' then mo.Broker_code else coalesce(LTRIM(RTRIM(msl.User_Id_Otosales)),'') end as userOtosales
				                        ,mo.cust_id
				                        ,di.policy_id
			                        INTO #temptable
                                    from mst_order mo
                                    inner join policy p WITH (NOLOCK) on p.policy_id = mo.policy_id
                                    inner join mst_branch mb WITH (NOLOCK) on mb.branch_id = mo.branch_id
                                    inner join mst_product mp WITH (NOLOCK) on mp.product_code = mo.product_code
                                    inner join mst_sob ms WITH (NOLOCK) on ms.sob_id = mo.segment_code
                                    inner join dtl_mv dm WITH (NOLOCK) on dm.policy_id = mo.policy_id and p.endorsement_no = dm.endorsement_no
                                    inner join mst_vehicle_brand vb WITH (NOLOCK) on vb.brand_id = dm.brand_code
                                    inner join mst_vehicle_model vm WITH (NOLOCK) on vm.model_id = dm.model_code 
			                        inner join dtl_interest di WITH (NOLOCK) on di.policy_id = mo.policy_id and di.interest_type = 'P' and di.interest_id = 'CASCO'
                                    inner join dtl_coverage dc WITH (NOLOCK) on dc.policy_id = mo.policy_id and di.interest_no = dc.interest_no and dc.coverage_id IN ('ALLRIK','TLO') 
			                        left join mst_customer mc1 WITH (NOLOCK) on mc1.Cust_id = mo.broker_code
			                        left join [AAB].[dbo].comsales_salesman cs WITH (NOLOCK) ON mo.broker_code = cs.cust_id
			                        LEFT JOIN AAB.[dbo].mst_customer mc WITH (NOLOCK) ON mc.cust_id = cs.dealer_id
                                    left join mst_cust_type mct WITH (NOLOCK) on mct.cust_id = mc.Cust_id and mct.client_type IN ('DEALER','BROKER')
                                    left join mst_salesman msl WITH (NOLOCK) on msl.salesman_id = mo.salesman_id
                                    left join Dtl_Transaction dt on dt.Reference_No = mo.Ref_No 
			                        where mo.order_no IN ('{0}')

	                        SELECT ta.* 
			                        ,coalesce(LTRIM(RTRIM(x.uplinerName)),'-') as uplinerName
		                            ,coalesce(LTRIM(RTRIM(x.leaderName)),'-') as leaderName
	                        INTO #temp2
	                        FROM #temptable ta 
	                        left join (SELECT    b.Cust_Id AS CustomerID,
				                                                        a.Client_Code AS ClientCode,
								                                                        RTRIM(b.Name) AS ClientName,                                 
								                                                        c.Cust_Id AS UplinerCustomerID,
				                                                        a.Upliner_Client_Code AS UplinerCode ,
				                                                        RTRIM(d.Name) AS UplinerName,
				                                                        e.Cust_Id AS LeaderCustomerID,
				                                                        a.Leader_Client_Code AS LeaderCode ,                  
				                                                        RTRIM(f.Name) AS LeaderName                    
						                                                        FROM      dtl_cust_type a WITH ( NOLOCK )
						                                                        INNER JOIN mst_customer b WITH ( NOLOCK ) ON a.cust_id = b.cust_id                    
						                                                        LEFT JOIN dtl_cust_type c WITH ( NOLOCK ) ON a.Upliner_Client_Code=c.Client_Code
						                                                        LEFT JOIN mst_customer d WITH ( NOLOCK ) ON c.Cust_Id = d.cust_id                    
						                                                        LEFT JOIN dtl_cust_type e WITH ( NOLOCK ) ON a.Leader_Client_Code=e.Client_Code
						                                                        LEFT JOIN mst_customer f WITH ( NOLOCK ) ON e.Cust_Id = f.cust_id                    
						                                                        WHERE     a.Client_Type = 'AGENT'
					                                                        ) AS X ON  X.CustomerID = ta.cust_id OR X.ClientCode = ta.cust_id

	                        SELECT t2.transactionNo,t2.policyNo,t2.orderNo,t2.endorsementNo, t2.orderType,t2.periodFrom,t2.periodTo,
	                        t2.inputDate,t2.approveDate,t2.productCode,t2.productName,t2.segmentCode,t2.entryUser,t2.branchName,
	                        t2.sumInsured,t2.businessSource,t2.vCode,t2.vBrand,t2.objectDescription,t2.mfgYear,t2.chasisNumber,
	                        t2.engineNumber,t2.registrationNumber,t2.dealerID,t2.dealerBroker,t2.salesmanCode,t2.salesmanName,
	                        t2.userOtosales,t2.uplinerName,t2.leaderName,
	                        STUFF((SELECT ',' + data.Total_Year + ' Year ' + CASE WHEN data.Coverage_id = 'ALLRIK' THEN 'Comprehensive' ELSE 'Total Loss Only' END
						        FROM (select CAST(count(*) AS VARCHAR) as Total_Year, Coverage_Id 
									        from Dtl_Interest di1 
									        inner join dtl_coverage dc1 on dc1.interest_no = di1.interest_no
									        and di1.Policy_Id = dc1.Policy_Id 
									        and di1.interest_id = 'CASCO' 
									        and dc1.coverage_id in ('ALLRIK','TLO')
									        where di1.policy_id = t2.policy_id 
									        group by coverage_id) data
						        FOR XML PATH('')
						        ), 1, 1, '') as basicCover
	                        FROM #temp2 t2
                            ORDER BY  t2.inputDate

	                        DROP TABLE #temptable
	                        DROP TABLE #temp2", string.Join("','", orderno.ToArray()));

                result = db.Query<RawData>(query).ToList();
            }
            #endregion

            #region add additional data to result
            result = AddAdditionalDataRaw(result);
            #endregion

            return result;
        }

        private static List<FollowUpRenew> AddAdditionalDataRenew(List<FollowUpRenew> prevData)
        {
            var newData = new List<FollowUpRenew>();
            var query = string.Empty;

            #region Get other adjustment
            using (a2isDBHelper.Database db = new a2isDBHelper.Database("AAB2000DB"))
            {
                query = string.Format(@"SELECT mo.order_no as PolicyOrderNo,LTRIM(RTRIM(moa.Reason)) as otherAdjustment
                                        FROM [AAB].[dbo].[Mst_Order] mo
                                        inner join [AAB].[dbo].[Mst_Order_Mobile_Approval] moa on moa.order_no = mo.order_no
                                        where ApprovalType = 'ADJUST'
                                        and mo.order_no in ('{0}')", 
                            string.Join("','", prevData.Select(x => x.policyOrderNo).ToArray()));
                var helperList = db.Query<FollowUpRenew>(query).ToList();

                foreach (var item in prevData)
                    if (helperList.Exists(x => x.policyOrderNo == item.policyOrderNo))
                    {
                        item.otherAdjustment = helperList.Where(x => x.policyOrderNo == item.policyOrderNo).FirstOrDefault().otherAdjustment;
                    }
            }
            #endregion

      //      #region Get premium
      //      using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobileDB"))
      //      {
      //          query = string.Format(@";SELECT os.orderno,
						//b.Premium as premi
      //                  From ordersimulation os 
      //                  inner join orderSimulationinterest a on os.orderno = a.orderno
      //                  inner join orderSimulationcoverage b on a.orderno=b.orderno and a.interestno=b.interestno and b.RowStatus=1 
      //                  where a.RowStatus=1 AND os.orderno IN ('{0}')",
      //                  String.Join("','", prevData.Where(y => !string.IsNullOrEmpty(y.orderNo)).Select(x => x.orderNo.Trim()).ToArray()));

      //          var helperList = db.Query<FollowUpRenew>(query).ToList();
      //          foreach (var item in prevData)
      //              if (helperList.Exists(x => x.orderNo == item.orderNo))
      //              {
      //                  item.premi = helperList.Where(x => x.orderNo == item.orderNo).FirstOrDefault().premi;
      //              }
      //      }
      //      #endregion

            #region Get upline and leader name
            using (a2isDBHelper.Database db = new a2isDBHelper.Database("AAB2000DB"))
            {
                query = string.Format(@"SELECT COALESCE(uplinerName,'-') as newUplineName
                                        ,COALESCE(leaderName,'-') as newLeadersName			
		                            FROM  mst_order mo 
		                            LEFT JOIN  ( SELECT    b.Cust_Id AS CustomerID,
				                        a.Client_Code AS ClientCode,
								                        RTRIM(b.Name) AS ClientName,                                 
								                        c.Cust_Id AS UplinerCustomerID,
				                        a.Upliner_Client_Code AS UplinerCode ,
				                        RTRIM(d.Name) AS UplinerName,
				                        e.Cust_Id AS LeaderCustomerID,
				                        a.Leader_Client_Code AS LeaderCode ,                  
				                        RTRIM(f.Name) AS LeaderName                    
						                        FROM      AAB.dbo.dtl_cust_type a WITH ( NOLOCK )
						                        INNER JOIN AAB.dbo.mst_customer b WITH ( NOLOCK ) ON a.cust_id = b.cust_id                    
						                        LEFT JOIN AAB.dbo.dtl_cust_type c WITH ( NOLOCK ) ON a.Upliner_Client_Code=c.Client_Code
						                        LEFT JOIN AAB.dbo.mst_customer d WITH ( NOLOCK ) ON c.Cust_Id = d.cust_id                    
						                        LEFT JOIN AAB.dbo.dtl_cust_type e WITH ( NOLOCK ) ON a.Leader_Client_Code=e.Client_Code
						                        LEFT JOIN AAB.dbo.mst_customer f WITH ( NOLOCK ) ON e.Cust_Id = f.cust_id                    
						                        WHERE     a.Client_Type = 'AGENT'
					                        ) AS X ON  X.CustomerID = mo.Salesman_Id OR X.ClientCode = mo.Salesman_Id
									WHERE mo.Order_No IN ('{0}')",
                            string.Join("','", prevData.Select(x => x.policyOrderNo).ToArray()));
                var helperList = db.Query<FollowUpRenew>(query).ToList();

                foreach (var item in prevData)
                    if (helperList.Exists(x => x.policyOrderNo == item.policyOrderNo))
                    {
                        item.newUplineName = helperList.Where(x => x.policyOrderNo == item.policyOrderNo).FirstOrDefault().newUplineName;
                        item.newLeadersName = helperList.Where(x => x.policyOrderNo == item.policyOrderNo).FirstOrDefault().newLeadersName;
                    }
            }
            #endregion
            newData.AddRange(prevData);

            return prevData;
        }

        private static List<FollowUpNew> AddAdditionalDataNew(List<FollowUpNew> prevData)
        {
            var newData = new List<FollowUpNew>();
            var query = string.Empty;

            #region Get other adjustment
            using (a2isDBHelper.Database db = new a2isDBHelper.Database("AAB2000DB"))
            {
                query = string.Format(@"SELECT mo.order_no as PolicyOrderNo,LTRIM(RTRIM(moa.Reason)) as otherAdjustment
                                        FROM [AAB].[dbo].[Mst_Order] mo
                                        inner join [AAB].[dbo].[Mst_Order_Mobile_Approval] moa on moa.order_no = mo.order_no
                                        where ApprovalType = 'ADJUST'
                                        and mo.order_no in ('{0}')",
                            string.Join("','", prevData.Select(x => x.policyOrderNo).ToArray()));
                var helperList = db.Query<FollowUpNew>(query).ToList();

                foreach (var item in prevData)
                    if (helperList.Exists(x => x.policyOrderNo == item.policyOrderNo))
                    {
                        item.otherAdjustment = helperList.Where(x => x.policyOrderNo == item.policyOrderNo).FirstOrDefault().otherAdjustment;
                    }
            }
            #endregion

            #region Get upline and leader name
            using (a2isDBHelper.Database db = new a2isDBHelper.Database("AAB2000DB"))
            {
                query = string.Format(@"SELECT COALESCE(uplinerName,'-') as uplinerName
                                        ,COALESCE(leaderName,'-') as leaderName			
		                          FROM  mst_order mo 
		                          LEFT JOIN  ( SELECT    b.Cust_Id AS CustomerID,
				                        a.Client_Code AS ClientCode,
								                        RTRIM(b.Name) AS ClientName,                                 
								                        c.Cust_Id AS UplinerCustomerID,
				                        a.Upliner_Client_Code AS UplinerCode ,
				                        RTRIM(d.Name) AS UplinerName,
				                        e.Cust_Id AS LeaderCustomerID,
				                        a.Leader_Client_Code AS LeaderCode ,                  
				                        RTRIM(f.Name) AS LeaderName                    
						                        FROM      AAB.dbo.dtl_cust_type a WITH ( NOLOCK )
						                        INNER JOIN AAB.dbo.mst_customer b WITH ( NOLOCK ) ON a.cust_id = b.cust_id                    
						                        LEFT JOIN AAB.dbo.dtl_cust_type c WITH ( NOLOCK ) ON a.Upliner_Client_Code=c.Client_Code
						                        LEFT JOIN AAB.dbo.mst_customer d WITH ( NOLOCK ) ON c.Cust_Id = d.cust_id                    
						                        LEFT JOIN AAB.dbo.dtl_cust_type e WITH ( NOLOCK ) ON a.Leader_Client_Code=e.Client_Code
						                        LEFT JOIN AAB.dbo.mst_customer f WITH ( NOLOCK ) ON e.Cust_Id = f.cust_id                    
						                        WHERE     a.Client_Type = 'AGENT'
					                        ) AS X ON  X.CustomerID = mo.Salesman_Id OR X.ClientCode = mo.Salesman_Id
									WHERE mo.Order_No IN ('{0}')",
                            string.Join("','", prevData.Select(x => x.policyOrderNo).ToArray()));
                var helperList = db.Query<FollowUpNew>(query).ToList();

                foreach (var item in prevData)
                    if (helperList.Exists(x => x.policyOrderNo == item.policyOrderNo))
                    {
                        item.uplineName = helperList.Where(x => x.policyOrderNo == item.policyOrderNo).FirstOrDefault().uplineName;
                        item.leadersName = helperList.Where(x => x.policyOrderNo == item.policyOrderNo).FirstOrDefault().leadersName;
                    }
            }
            #endregion

            //#region Get premium
            //using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobileDB"))
            //{
            //    query = string.Format(@"select os.orderno,
						      //          SUM(b.Premium) as premi
            //                      From ordersimulation os 
		          //                inner join orderSimulationinterest a on os.orderno = a.orderno
		          //                inner join orderSimulationcoverage b on a.orderno=b.orderno and a.interestno=b.interestno and b.RowStatus=1 
            //                      where a.RowStatus=1 AND os.orderno IN ('{0}')
            //                      GROUP BY os.orderno",
            //                string.Join("','", prevData.Select(x => x.orderNo).ToArray()));
            //    var helperList = db.Query<FollowUpNew>(query).ToList();

            //    foreach (var item in prevData)
            //        if (helperList.Exists(x => x.orderNo == item.orderNo))
            //        {
            //            item.premi = helperList.Where(x => x.orderNo == item.orderNo).FirstOrDefault().premi;
            //        }
            //}
            //#endregion
            newData.AddRange(prevData);

            return prevData;
        }
        private static List<RawData> AddAdditionalDataRaw(List<RawData> prevData)
        {
            var rawData = new List<RawData>();
            var query = string.Empty;

            #region Get other adjustment
            using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobileDB"))
            {
                query = string.Format(@"SELECT Name as marketingName, salesOfficerID as userOtosales
                                        FROM AABMobile.dbo.SalesOfficer
                                        where salesOfficerID in ('{0}')",
                            string.Join("','", prevData.Select(x => x.userOtosales).ToArray()));
                var helperList = db.Query<RawData>(query).ToList();

                foreach (var item in prevData)
                    if (helperList.Exists(x => x.userOtosales == item.userOtosales))
                    {
                        item.marketingName = helperList.Where(x => x.userOtosales == item.userOtosales).FirstOrDefault().marketingName;
                    }
            }
            #endregion

            rawData.AddRange(prevData);

            return prevData;
        }
        public static List<SalesOfficer> GetSalesOfficer()
        {
            var salesAO = new List<SalesOfficer>();
            string query = string.Empty;
            #region get data sales
            using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobileDB"))
            {
                query = string.Format(@";SELECT SalesOfficerID, Email, Name, Role
                                  FROM AABMobile.dbo.SalesOfficer
                                  WHERE Role IN('SALESOFFICER','TELERENEWAL','TELESALES')
                                  ");
                salesAO = db.Query<SalesOfficer>(query).ToList();
            }
            #endregion
            
            return salesAO;
        }

        public static List<SalesOfficer> GetUplinerOfficer()
        {
            var Upliner = new List<SalesOfficer>();
            string query = string.Empty;
            #region get data mgr
            using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobileDB"))
            {
                query = string.Format(@";SELECT SalesOfficerID, Email, Name, Role
                                  FROM AABMobile.dbo.SalesOfficer
                                  WHERE Role IN ('BRANCHMGR','TELEMGR','SECHEADMGR','AGENCYMGR','TELESALESMGR')
                                  ");
                Upliner = db.Query<SalesOfficer>(query).ToList();
            }
            #endregion

            return Upliner;
        }

        public static List<string> GetSubOrdinates(string salesOfficerID)
        {
            var subOrdinates = new List<string>();
            string query = string.Empty;
            #region get subordinates
            //using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobileDB"))
            //{
            //    query = string.Format(@";exec dbo.usp_GetOtosalesSubOrdinate '{0}'", salesOfficerID);
            //    subOrdinates = db.Query<string>(query).ToList();
            //}

            using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobileDB"))
            {
                query = string.Format(@";DECLARE @@SuperiorID as varchar(50) =  '{0}'
                                        DECLARE @@RoleCode as varchar(50)
	                                        select TOP 1 @@RoleCode = Role from SalesOfficer where SalesOfficerID = @@SuperiorID and RowStatus = 1

	                                        IF @@RoleCode = 'TELEMGR'
		                                        SELECT DISTINCT Param [UserID] FROM AABHead WHERE [Type] = 'TELE' and Value = @@SuperiorID
	                                        ELSE IF @@RoleCode = 'SALESSECHEAD'
		                                        SELECT DISTINCT Param [UserID] FROM AABHead WHERE [Type] = 'SALESSECHEAD' and Value = @@SuperiorID
	                                        ELSE IF @@RoleCode = 'AGENCYMGR'
		                                        SELECT DISTINCT Param [UserID] FROM AABHead WHERE [Type] = 'AGENCY' and Value = @@SuperiorID
	                                        ELSE IF @@RoleCode = 'BRANCHMGR'
		                                        SELECT so.SalesOfficerID FROM AABHead ah
		                                        INNER JOIN salesofficer so ON so.branchcode = ah.Param
		                                        WHERE [Type] = 'BRANCH' and Value = @@SuperiorID and so.rowstatus = 1 and ah.rowstatus = 1
		                                        and role in ('SALESOFFICER')
											ELSE IF @@RoleCode = 'TELESALESMGR'
												SELECT SalesOfficerID FROM dbo.SalesOfficer WHERE Role = 'TELESALES'
	                                        ELSE
		                                        select * from (select ''[UserID] ) a where 1=0
                                        ", salesOfficerID);
                subOrdinates = db.Query<string>(query).ToList();
            }
            #endregion
            return subOrdinates;
        }



    }
}
