﻿
using System;

namespace Otosales.ReportFollowUp.dto
{
    public class FollowUpRenew
    {
        public string nomor { get; set; }
        public DateTime orderDate { get; set; }
        public string prospectName { get; set; }
        public string isRenewable { get; set; }
        public string claimRatio { get; set; }
        public string claimFreq { get; set; }
        public string oldDealerName { get; set; }
        public string newDealerName { get; set; }
        public string oldSalesmanDealerName { get; set; }
        public string newSalesmanDealerName { get; set; }
        public string oldUplineName { get; set; }
        public string newUplineName { get; set; }
        public string oldLeadersName { get; set; }
        public string newLeadersName { get; set; }
        public string isUsedCar { get; set; }
        public string vCode { get; set; }
        public string vBrand { get; set; }
        public string vYear { get; set; }
        public string vType { get; set; }
        public string vSeries { get; set; }
        public string usage { get; set; }
        public string region { get; set; }
        public string registrationNumber { get; set; }
        public string chasisNumber { get; set; }
        public string engineNumber { get; set; }
        public string productType { get; set; }
        public string productCode { get; set; }
        public string basicCover { get; set; }
        public decimal sumInsured { get; set; }
        public decimal premi { get; set; }
        public string fuStatus { get; set; }
        public string fuDate { get; set; }
        public string fuReason { get; set; }
        public string fuNotes { get; set; }
        public string callFreq { get; set; }
        public string isPolicyBeforePay { get; set; }
        public string isDocRep { get; set; }
        public string isSalesDealerChange { get; set; }
        public string otherAdjustment { get; set; }
        public DateTime transactionDate { get; set; }
        public string oldPolicyNo { get; set; }
        public string newPolicyNo { get; set; }
        public DateTime oldPeriodFrom { get; set; }
        public DateTime newPeriodFrom { get; set; }
        public DateTime oldPeriodTo { get; set; }
        public DateTime newPeriodTo { get; set; }
        public string SalesOfficerID { get; set; }
        public string policyOrderNo { get; set; }
        public string orderNo { get; set; }
        public string remark { get; set; }
        public string followUpNotes { get; set; }
    }
}
