﻿
using System;

namespace Otosales.ReportFollowUp.dto
{
    public class FollowUpNew
    {
        public int nomor { get; set; }
        public DateTime orderDate { get; set; }
        public string prospectName { get; set; }
        public string dealerName { get; set; }
        public string salesmanDealerName { get; set; }
        public string agentName { get; set; }
        public string uplineName { get; set; }
        public string leadersName { get; set; }
        public string isUsedCar { get; set; }
        public string vCode { get; set; }
        public string vBrand { get; set; }
        public string vYear { get; set; }
        public string vType { get; set; }
        public string vSeries { get; set; }
        public string usage { get; set; }
        public string region { get; set; }
        public string registrationNumber { get; set; }
        public string chasisNumber { get; set; }
        public string engineNumber { get; set; }
        public string productType { get; set; }
        public string productCode { get; set; }
        public string basicCover { get; set; }
        public decimal sumInsured { get; set; }
        public decimal premi { get; set; }
        public string fuStatus { get; set; }
        public string fuDate { get; set; }
        public string fuReason { get; set; }
        public string fuNotes { get; set; }
        public string callFreq { get; set; }
        public string isDocRep { get; set; }
        public string isSalesDealerChange { get; set; }
        public string otherAdjustment { get; set; }
        public DateTime transactionDate { get; set; }
        public string policyNo { get; set; }
        public DateTime periodFrom { get; set; }
        public DateTime periodTo { get; set; }
        public string orderNo { get; set; }
        public string SalesOfficerID { get; set; }
        public string policyOrderNo { get; set; }

    }
}
