﻿namespace Otosales.ReportFollowUp.dto
{
    public class SalesOfficer
    {
        public string SalesOfficerID { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }
        public string Name { get; set; }
    }
}
