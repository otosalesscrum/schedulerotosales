﻿

using System;

namespace Otosales.ReportFollowUp.dto
{
    public class RawData
    {
        public int nomor { get; set; }
        public string transactionNo { get; set; }
        public string policyNo { get; set; }
        public string orderNo { get; set; }
        public string endorsementNo { get; set; }
        public string nameOnPolicy { get; set; }
        public string orderType { get; set; }
        public DateTime periodFrom { get; set; }
        public DateTime periodTo { get; set; }
        public DateTime inputDate { get; set; }
        public DateTime approveDate { get; set; }
        public string productCode { get; set; }
        public string productName { get; set; }
        public string segmentCode { get; set; }
        public string entryUser { get; set; }
        public string branchName { get; set; }
        public decimal sumInsured { get; set; }
        public string businessSource { get; set; }
        public string vCode { get; set; }
        public string vBrand { get; set; }
        public string objectDescription { get; set; }
        public int mfgYear { get; set; }
        public string chasisNumber { get; set; }
        public string engineNumber { get; set; }
        public string registrationNumber { get; set; }
        public string basicCover { get; set; }
        public string dealerID { get; set; }
        public string dealerBroker { get; set; }
        public string salesmanCode { get; set; }
        public string salesmanName { get; set; }
        public string uplinerName { get; set; }
        public string leaderName { get; set; }
        public string userOtosales { get; set; }
        public string marketingName { get; set; }
    }
}
