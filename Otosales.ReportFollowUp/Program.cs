﻿using a2is.Framework.Monitoring;
using A2isMessaging;
using OfficeOpenXml;
using Otosales.ReportFollowUp.dta;
using Otosales.ReportFollowUp.dto;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otosales.ReportFollowUp
{
    class Program
    {
        #region Properties
        private static readonly a2isLogHelper _log = new a2isLogHelper();
        private static string REPORT_DATE_TYPE = ConfigurationManager.AppSettings["DateType"].ToString(); //D or M or Y
        static bool isDirect = false;
        #endregion

        static void Main(string[] args)
        {
            isDirect = ConfigurationManager.AppSettings["IsDirectSend"].ToString().Equals("1"); // ini untuk kirim.
            ReportFollowUpNew();
            ReportFollowUpRenew();
            ReportRawData();
        }

        public static void ReportFollowUpRenew()
        {
            var actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;

            DateTime endDate = DateTime.Now;
            bool isFirstDayOfMonth = endDate.Date == new DateTime(endDate.Year, endDate.Month, 1).Date;
            try
            {
                _log.Info("Function " + actionName + " BEGIN : ");
                string dateType = ConfigurationManager.AppSettings["DateType"].ToString();
                bool isManual = Convert.ToBoolean(ConfigurationManager.AppSettings["IsManualGenerate"].ToString());

                DateTime startDate = new DateTime(1900, 1, 1);
                string cvStartDate = string.Empty;
                string cvEndDate = string.Empty;

                if (isManual)
                {
                    cvStartDate = ConfigurationManager.AppSettings["ReportFromDate"].ToString();
                    cvEndDate = ConfigurationManager.AppSettings["ReportToDate"].ToString();

                    bool isMonthly = ConfigurationManager.AppSettings["DateType"].ToString().ToUpper() == "M";
                    if (isMonthly)
                    {
                        startDate = new DateTime(endDate.Year, endDate.Month, 1).Date;
                        endDate = new DateTime(endDate.Year, endDate.Month, endDate.Day, 23, 59, 59);
                        cvStartDate = startDate.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture);
                        cvEndDate = endDate.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture);
                    }
                    SendAttachmentFURenew(cvStartDate, cvEndDate, isMonthly);
                }
                else
                {
                    if (isFirstDayOfMonth)
                    {
                        startDate = endDate.AddMonths(-1).Date;
                        cvStartDate = startDate.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture);
                        cvEndDate = endDate.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture);
                        SendAttachmentFURenew(cvStartDate, cvEndDate);
                    } else
                    {
                        startDate = new DateTime(endDate.Year, endDate.Month, 1).Date;
                        endDate = new DateTime(endDate.Year, endDate.Month, endDate.Day, 23, 59, 59);

                        cvStartDate = startDate.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture);
                        cvEndDate = endDate.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture);

                        SendAttachmentFURenew(cvStartDate, cvEndDate);
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error("Function " + actionName + " Error : " + ex.ToString());
                string attachname = String.Format(ConfigurationManager.AppSettings["AttachmentNameRenew"].ToString(), isFirstDayOfMonth ? DateTime.Now.ToString("MMyyyy", CultureInfo.InvariantCulture) : DateTime.Now.ToString("ddMMyyyy", CultureInfo.InvariantCulture));
                string exc = ex.ToString();
            }
        }

        private static void SendAttachmentFURenew(string cvStartDate, string cvEndDate, bool isMonthly = false)
        {
            var actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                List<FollowUpRenew> fuRenewList = new List<FollowUpRenew>();
                List<SalesOfficer> salesList = new List<SalesOfficer>();
                List<SalesOfficer> uplineList = new List<SalesOfficer>();
                List<FollowUpRenew> dataList = new List<FollowUpRenew>();
                
                fuRenewList = FollowUpAccess.GetFollowUpRenew(cvStartDate, cvEndDate);

                salesList = FollowUpAccess.GetSalesOfficer();
                foreach (var sales in salesList)
                {
                    dataList.AddRange(fuRenewList.Where(x => !string.IsNullOrEmpty(x.SalesOfficerID) && x.SalesOfficerID.Equals(sales.SalesOfficerID, StringComparison.InvariantCultureIgnoreCase)));
                    if (dataList.Count > 0)
                    {
                        var fuRenewDataTable = ConvertFURenewIntoDataTable(dataList);
                        //var fuRenewDoc = ExportUsingEPPlusRenew(fuRenewDataTable, isMonthly);
                        var fuRenewDoc = ExportUsingSimpleExcelWriterRenew(fuRenewDataTable, sales.Name, isMonthly);
                        sendEmailRenew(fuRenewDoc, sales.Email, sales.Name, isMonthly);
                    }
                    dataList.Clear();
                }
                uplineList = FollowUpAccess.GetUplinerOfficer();
                foreach (var up in uplineList)
                {
                    List<string> subOrdinateList = new List<string>();
                    subOrdinateList = FollowUpAccess.GetSubOrdinates(up.SalesOfficerID);
                    if (subOrdinateList.Count > 0)
                    {
                        dataList.AddRange(fuRenewList.Where(x => !string.IsNullOrEmpty(x.SalesOfficerID) && subOrdinateList.Contains(x.SalesOfficerID.ToUpper().Trim())));
                        if (dataList.Count > 0)
                        {
                            var fuRenewDataTable = ConvertFURenewIntoDataTable(dataList);
                            //var fuRenewDoc = ExportUsingEPPlusRenew(fuRenewDataTable, isMonthly);
                            var fuRenewDoc = ExportUsingSimpleExcelWriterRenew(fuRenewDataTable, up.Name, isMonthly);
                            sendEmailRenew(fuRenewDoc, up.Email, up.Name, isMonthly);
                        }
                        dataList.Clear();
                    }
                }

                string res = "Report Succesfully Created";
                _log.Info("Function " + actionName + " END : ");
            }
            catch (Exception ex)
            {
                _log.Error("Function " + actionName + " Error : " + ex.ToString());
                string exc = ex.ToString();
            }
        }

        private static DataTable ConvertFURenewIntoDataTable(List<FollowUpRenew> fuRenewList)
        {
            var dt = new DataTable();
            dt.Columns.Add("Nomor", typeof(string));
            dt.Columns.Add("Order Date", typeof(DateTime));
            dt.Columns.Add("Prospect name", typeof(string));
            dt.Columns.Add("Marketing Code", typeof(string));
            dt.Columns.Add("Renewable / Not Renewable", typeof(string));
            dt.Columns.Add("Claim Ratio", typeof(string));
            dt.Columns.Add("Claim Frequency", typeof(string));
            dt.Columns.Add("Old Dealer Name", typeof(string));
            dt.Columns.Add("New dealer name", typeof(string));
            dt.Columns.Add("Old Salesman Dealer Name", typeof(string));
            dt.Columns.Add("New salesman dealer name", typeof(string));
            dt.Columns.Add("Old Upline Name", typeof(string));
            dt.Columns.Add("New Upline Name", typeof(string));
            dt.Columns.Add("Old Leaders Name", typeof(string));
            dt.Columns.Add("New Leaders Name", typeof(string));
            dt.Columns.Add("New / used car", typeof(string));
            dt.Columns.Add("Vcode", typeof(string));
            dt.Columns.Add("Brand", typeof(string));
            dt.Columns.Add("Year", typeof(string));
            dt.Columns.Add("Type", typeof(string));
            dt.Columns.Add("Series", typeof(string));
            dt.Columns.Add("Usage", typeof(string));
            dt.Columns.Add("Region", typeof(string));
            dt.Columns.Add("Registration number", typeof(string));
            dt.Columns.Add("Chasis number", typeof(string));
            dt.Columns.Add("Engine number", typeof(string));
            dt.Columns.Add("Product type", typeof(string));
            dt.Columns.Add("Product code", typeof(string));
            dt.Columns.Add("Basic cover", typeof(string));
            dt.Columns.Add("Sum insured", typeof(decimal));
            dt.Columns.Add("Premi", typeof(decimal));
            dt.Columns.Add("FU status", typeof(string));
            dt.Columns.Add("FU Date", typeof(string));
            dt.Columns.Add("Reason", typeof(string));
            dt.Columns.Add("Notes", typeof(string));
            dt.Columns.Add("Call Frequency", typeof(string));
            dt.Columns.Add("Is Polis Terbit Sebelum Bayar", typeof(string));
            dt.Columns.Add("Is Dokumen Rep", typeof(string));
            dt.Columns.Add("Is Perubahan Nama Salesman / Dealer", typeof(string));
            dt.Columns.Add("Other Adjustment", typeof(string));
            dt.Columns.Add("Transaction Date", typeof(DateTime));
            dt.Columns.Add("Old Policy No", typeof(string));
            dt.Columns.Add("New Policy No", typeof(string));
            dt.Columns.Add("Period From (Old Policy)", typeof(DateTime));
            dt.Columns.Add("Period To (Old Policy)", typeof(DateTime));
            dt.Columns.Add("Period From (New Policy)", typeof(DateTime));
            dt.Columns.Add("Period To (New Policy)", typeof(DateTime));
            dt.Columns.Add("Remark", typeof(string));
            dt.Columns.Add("Follow Up Notes", typeof(string));

            if (fuRenewList.Count == 0)
            {
                DataRow dr = dt.NewRow();
                dr["Prospect Name"] = "No data";
                dt.Rows.Add(dr);
            }
            else
            {
                int count = 1;
                foreach (var fuRenew in fuRenewList)
                {
                    DataRow dr = dt.NewRow();
                    dr["Nomor"] = count++;
                    dr["Order Date"] = fuRenew.orderDate;
                    dr["Prospect name"] = fuRenew.prospectName;
                    dr["Marketing Code"] = fuRenew.SalesOfficerID;
                    dr["Renewable / Not Renewable"] = fuRenew.isRenewable;
                    dr["Claim Ratio"] = fuRenew.claimRatio;
                    dr["Claim Frequency"] = fuRenew.claimFreq;
                    dr["Old Dealer Name"] = fuRenew.oldDealerName;
                    dr["New dealer name"] = fuRenew.newDealerName;
                    dr["Old Salesman Dealer Name"] = fuRenew.oldSalesmanDealerName;
                    dr["New salesman dealer name"] = fuRenew.newSalesmanDealerName;
                    dr["Old Upline Name"] = fuRenew.oldUplineName;
                    dr["New Upline Name"] = fuRenew.newUplineName;
                    dr["Old Leaders Name"] = fuRenew.oldLeadersName;
                    dr["New Leaders Name"] = fuRenew.newLeadersName;
                    dr["New / used car"] = fuRenew.isUsedCar;
                    dr["Vcode"] = fuRenew.vCode;
                    dr["Brand"] = fuRenew.vBrand;
                    dr["Year"] = fuRenew.vYear;
                    dr["Type"] = fuRenew.vType;
                    dr["Series"] = fuRenew.vSeries;
                    dr["Usage"] = fuRenew.usage;
                    dr["Region"] = fuRenew.region;
                    dr["Registration number"] = fuRenew.registrationNumber;
                    dr["Chasis number"] = fuRenew.chasisNumber;
                    dr["Engine number"] = fuRenew.engineNumber;
                    dr["Product type"] = fuRenew.productType;
                    dr["Product code"] = fuRenew.productCode;
                    dr["Basic cover"] = fuRenew.basicCover;
                    dr["Sum insured"] = ToMoney(fuRenew.sumInsured);
                    dr["Premi"] = ToMoney(fuRenew.premi);
                    dr["FU status"] = fuRenew.fuStatus;
                    dr["FU Date"] = fuRenew.fuDate;
                    dr["Reason"] = fuRenew.fuReason;
                    dr["Notes"] = fuRenew.fuNotes;
                    dr["Call Frequency"] = fuRenew.callFreq;
                    dr["Is Polis Terbit Sebelum Bayar"] = fuRenew.isPolicyBeforePay;
                    dr["Is Dokumen Rep"] = fuRenew.isDocRep;
                    dr["Is Perubahan Nama Salesman / Dealer"] = fuRenew.isSalesDealerChange;
                    dr["Other Adjustment"] = fuRenew.otherAdjustment;
                    dr["Transaction Date"] = fuRenew.transactionDate;
                    dr["Old Policy No"] = fuRenew.oldPolicyNo;
                    dr["New Policy No"] = fuRenew.newPolicyNo;
                    dr["Period From (Old Policy)"] = fuRenew.oldPeriodFrom;
                    dr["Period To (Old Policy)"] = fuRenew.oldPeriodTo;
                    dr["Period From (New Policy)"] = fuRenew.newPeriodFrom;
                    dr["Period To (New Policy)"] = fuRenew.newPeriodTo;
                    dr["Remark"] = fuRenew.remark;
                    dr["Follow Up Notes"] = fuRenew.followUpNotes;

                    dt.Rows.Add(dr);
                }
            }
            return dt;
        }
        public static byte[] ExportUsingSimpleExcelWriterRenew(DataTable table, string salesName, bool isMonthly = false)
        {
            byte[] resultDoc = null;
            int col = 0;
            int row = 0;

            string _directoryPath = string.Empty;
            string _fileName = string.Empty;

            var fileExt = ConfigurationManager.AppSettings["FileExtension"].ToString();

            _directoryPath = ConfigurationManager.AppSettings["FilePath"];
            if (!isMonthly)
            {
                _directoryPath = string.Concat(_directoryPath, "\\Otosales\\ReportFURenew\\", DateTime.Now.ToString("yyyy", CultureInfo.InvariantCulture), "\\", DateTime.Now.ToString("MMMM"), "\\");
                _fileName = string.Format(ConfigurationManager.AppSettings["AttachmentNameRenew"], DateTime.Now.ToString("ddMMyyyy", CultureInfo.InvariantCulture) + " " + salesName + " " + fileExt);
            }
            else
            {
                _directoryPath = string.Concat(_directoryPath, "\\Otosales\\ReportFURenew\\", DateTime.Now.ToString("yyyy", CultureInfo.InvariantCulture), "\\");
                _fileName = string.Format(ConfigurationManager.AppSettings["AttachmentNameRenew"], DateTime.Now.ToString("MMMM yyyy", CultureInfo.InvariantCulture) + " " + salesName + " " + fileExt);

            }

            if (!Directory.Exists(_directoryPath))
                Directory.CreateDirectory(_directoryPath);

            FileStream stream = new FileStream(string.Concat(_directoryPath, _fileName), FileMode.OpenOrCreate);
            ExcelWriter writer = new ExcelWriter(stream);


            writer.BeginWrite();

            foreach (DataColumn dataColumn in table.Columns)
            {
                writer.WriteCell(0, col, dataColumn.ColumnName);
                col++;
            }

            row = 1;
            foreach (DataRow dataRow in table.Rows)
            {
                col = 0;
                foreach (DataColumn dataColumn in table.Columns)
                {
                    if (col == 0 || col == 18)
                    {
                        writer.WriteCell(row, col, Convert.ToInt32(dataRow[dataColumn]));
                    }
                    else if (col == 29 || col == 30)
                    {
                        writer.WriteCell(row, col, Convert.ToDouble(dataRow[dataColumn]));
                    }
                    else
                    {
                        writer.WriteCell(row, col, dataRow[dataColumn].ToString());
                    }

                    col++;
                }
                row++;
            }

            writer.EndWrite();
            stream.Close();
            

            resultDoc = File.ReadAllBytes(string.Concat(_directoryPath, _fileName));
            return resultDoc;
        }
        public static byte[] ExportUsingEPPlusRenew(DataTable table, bool isMonthly = false)
        {
            byte[] resultDoc = null;
            int col = 0;
            int row = 0;

            string _directoryPath = string.Empty;
            string _fileName = string.Empty;

            var fileExt = ConfigurationManager.AppSettings["FileExtension"].ToString();

            _directoryPath = ConfigurationManager.AppSettings["FilePath"];
            if (!isMonthly)
            {
                _directoryPath = string.Concat(_directoryPath, "\\Otosales\\ReportFURenew\\", DateTime.Now.ToString("yyyy", CultureInfo.InvariantCulture), "\\", DateTime.Now.ToString("MMMM"), "\\");
                _fileName = string.Format(ConfigurationManager.AppSettings["AttachmentNameRenew"], DateTime.Now.ToString("ddMMyyyy", CultureInfo.InvariantCulture) + fileExt);
            }
            else
            {
                _directoryPath = string.Concat(_directoryPath, "\\Otosales\\ReportFURenew\\", DateTime.Now.ToString("yyyy", CultureInfo.InvariantCulture), "\\");
                _fileName = string.Format(ConfigurationManager.AppSettings["AttachmentNameRenew"], DateTime.Now.ToString("MMMM yyyy", CultureInfo.InvariantCulture) + fileExt);

            }

            if (!Directory.Exists(_directoryPath))
                Directory.CreateDirectory(_directoryPath);

            using (ExcelPackage pck = new ExcelPackage())
            {
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Report Follow Up Renew");
                ws.Cells["A1"].LoadFromDataTable(table, true);

                FileStream stream = new FileStream(string.Concat(_directoryPath, _fileName), FileMode.OpenOrCreate);
                pck.SaveAs(stream);
                stream.Close();
                resultDoc = File.ReadAllBytes(string.Concat(_directoryPath, _fileName));

            }

            return resultDoc;
        }

        private static void sendEmailRenew(byte[] document, string emailTo, string salesName, bool isMonthly = false)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            var _fuAccess = new FollowUpAccess();
            _log.Info(actionName + " Function start : ");
            string subject = "Report Follow Up Renewal Otosales " + (isMonthly ? DateTime.Now.ToString("MMyyyy", CultureInfo.InvariantCulture) : DateTime.Now.ToString("ddMMyyyy", CultureInfo.InvariantCulture));

            try
            {
                A2isMessageParam param = new A2isMessageParam();
                string today = DateTime.Now.ToString("ddMMyyyy", CultureInfo.InvariantCulture);
                var fileExt = ConfigurationManager.AppSettings["FileExtension"].ToString();
                //DateTime scheduleTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Date, 08, 00, 00, 000);

                param.ApplicationRequest = "FollowUpReport";
                param.ModuleRequest = "FURenew";
                param.MessageType = "MAIL"; // case sensitive type ={'MAIL','SMS'};
                param.MessageFrom = ConfigurationManager.AppSettings["EmailFrom"].ToString();
                param.MessageTo = emailTo;
                //param.MessageTo = ConfigurationManager.AppSettings["EmailTo"].ToString();
                //param.MessageCC = ConfigurationManager.AppSettings["EmailCC"].ToString();
                //param.MessageBcc = ConfigurationManager.AppSettings["EmailBCC"].ToString();
                param.MessageBody = getMailBodyFURenewDaily();
                param.MessageSchedule = DateTime.Now.ToString(A2isMessaging.A2isCommonStatic.FormatDateTimeUI);
                param.MessageDisplayName = "a2isinfo";
                param.MessageSubject = subject + " - " + salesName;
                param.MessagePriority = 1; //isinya 1 dan 0, kl tidak di isi 1 defaultnya  0
                param.MessageAttachment.Add(new A2isMessagingAttachment
                {
                    AttachmentDescription = String.Format("Report Follow Up Renewal Otosales {0}", DateTime.Now.ToString("ddMMyyyy", CultureInfo.InvariantCulture)),
                    FileByte = document,
                    //FileContentType = fileContentType,
                    FileExtension = fileExt
                }); // isinya dari class MailAttachment A2isMessaging, dengan file sudah jadi format byte[]

                A2isCommonResult result = Messaging.ProcessQueuing(param, isDirect);
                if (result.ResultCode)
                {
                    _log.Debug(string.Format("Send email success, queue code {0}", result.ResultObject));
                }
                else
                {
                    _log.Error(result.ResultDesc);
                }
            }
            catch (Exception ex)
            {
                _log.Error("Function " + actionName + " Error : " + ex.ToString());
            }
        }
        private static string getMailBodyFURenewDaily()
        {
            string today = DateTime.Now.ToString("ddMMyyyy", CultureInfo.InvariantCulture);
            StringBuilder MailBody = new StringBuilder();
            MailBody.Append("Yth Bapak/Ibu,");
            MailBody.AppendLine("<br>");
            MailBody.AppendLine("<br>");
            MailBody.Append(String.Format("Berikut terlampir data order renewal as per {0}, silakan dilanjutkan next proses.", today));
            MailBody.AppendLine("<br>");
            MailBody.AppendLine("<br>");
            MailBody.Append("Atas perhatian dan kerjasamanya, kami ucapkan terima kasih.");
            return MailBody.ToString();
        }


        public static void ReportFollowUpNew()
        {
            var actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;

            DateTime endDate = DateTime.Now;
            bool isFirstDayOfMonth = endDate.Date == new DateTime(endDate.Year, endDate.Month, 1).Date;
            try
            {
                _log.Info("Function " + actionName + " BEGIN : ");
                string dateType = ConfigurationManager.AppSettings["DateType"].ToString();
                bool isManual = Convert.ToBoolean(ConfigurationManager.AppSettings["IsManualGenerate"].ToString());

                DateTime startDate = new DateTime(1900, 1, 1);
                string cvStartDate = string.Empty;
                string cvEndDate = string.Empty;

                if (isManual)
                {
                    cvStartDate = ConfigurationManager.AppSettings["ReportFromDate"].ToString();
                    cvEndDate = ConfigurationManager.AppSettings["ReportToDate"].ToString();

                    bool isMonthly = ConfigurationManager.AppSettings["DateType"].ToString().ToUpper() == "M";
                    if (isMonthly)
                    {
                        startDate = new DateTime(endDate.Year, endDate.Month, 1).Date;
                        endDate = new DateTime(endDate.Year, endDate.Month, endDate.Day, 23, 59, 59);
                        cvStartDate = startDate.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture);
                        cvEndDate = endDate.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture);
                    }
                    SendAttachmentFUNew(cvStartDate, cvEndDate, isMonthly);
                }
                else
                {
                    if (isFirstDayOfMonth)
                    {
                        startDate = endDate.AddMonths(-1).Date;
                        cvStartDate = startDate.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture);
                        cvEndDate = endDate.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture);
                        SendAttachmentFUNew(cvStartDate, cvEndDate);

                    } else
                    {
                        startDate = new DateTime(endDate.Year, endDate.Month, 1).Date;
                        endDate = new DateTime(endDate.Year, endDate.Month, endDate.Day, 23, 59, 59);

                        cvStartDate = startDate.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture);
                        cvEndDate = endDate.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture);

                        SendAttachmentFUNew(cvStartDate, cvEndDate);
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error("Function " + actionName + " Error : " + ex.ToString());
                string attachname = String.Format(ConfigurationManager.AppSettings["AttachmentName"].ToString(), isFirstDayOfMonth ? DateTime.Now.ToString("MMyyyy", CultureInfo.InvariantCulture) : DateTime.Now.ToString("ddMMyyyy", CultureInfo.InvariantCulture));
                string exc = ex.ToString();
            }
        }

        private static void SendAttachmentFUNew(string cvStartDate, string cvEndDate, bool isMonthly = false)
        {
            var actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;

            try
            {
                List<FollowUpNew> fuNewList = new List<FollowUpNew>();
                List<SalesOfficer> salesList = new List<SalesOfficer>();
                List<SalesOfficer> uplineList = new List<SalesOfficer>();
                List<FollowUpNew> dataList = new List<FollowUpNew>();

                fuNewList = FollowUpAccess.GetFollowUpNew(cvStartDate, cvEndDate);

                salesList = FollowUpAccess.GetSalesOfficer();
                foreach (var sales in salesList)
                {
                    dataList.AddRange(fuNewList.Where(x => !string.IsNullOrEmpty(x.SalesOfficerID) && x.SalesOfficerID.Equals(sales.SalesOfficerID, StringComparison.InvariantCultureIgnoreCase)));
                    if (dataList.Count > 0)
                    {
                        var fuNewDataTable = ConvertFUNewIntoDataTable(dataList);
                        //var fuNewDoc = ExportUsingEPPlusNew(fuNewDataTable, isMonthly);
                        var fuNewDoc = ExportUsingSimpleExcelWriterNew(fuNewDataTable, sales.Name, isMonthly);
                        sendEmailNew(fuNewDoc, sales.Email, sales.Name, isMonthly);
                    }
                    dataList.Clear();
                }
                uplineList = FollowUpAccess.GetUplinerOfficer();
                foreach(var up in uplineList)
                {
                    List<string> subOrdinateList = new List<string>();
                    subOrdinateList = FollowUpAccess.GetSubOrdinates(up.SalesOfficerID);
                    if(subOrdinateList.Count > 0)
                    {
                        dataList.AddRange(fuNewList.Where(x => !string.IsNullOrEmpty(x.SalesOfficerID) && subOrdinateList.Contains(x.SalesOfficerID.ToUpper().Trim())));
                        if (dataList.Count > 0)
                        {
                            var fuNewDataTable = ConvertFUNewIntoDataTable(dataList);
                            //var fuNewDoc = ExportUsingEPPlusNew(fuNewDataTable, isMonthly);
                            var fuNewDoc = ExportUsingSimpleExcelWriterNew(fuNewDataTable, up.Name, isMonthly);
                            sendEmailNew(fuNewDoc, up.Email, up.Name, isMonthly);
                        }
                        dataList.Clear();
                    }
                }

                string res = "Report Succesfully Created";
                _log.Info("Function " + actionName + " END : ");
            }
            catch (Exception ex)
            {
                _log.Error("Function " + actionName + " Error : " + ex.ToString());
                string exc = ex.ToString();
            }
        }

        private static DataTable ConvertFUNewIntoDataTable(List<FollowUpNew> fuNewList)
        {
            var dt = new DataTable();
            dt.Columns.Add("Nomor", typeof(string));
            dt.Columns.Add("Order Date", typeof(DateTime));
            dt.Columns.Add("Prospect name", typeof(string));
            dt.Columns.Add("Marketing Code", typeof(string));
            dt.Columns.Add("Dealer Name", typeof(string));
            dt.Columns.Add("Salesman Dealer Name", typeof(string));
            dt.Columns.Add("Agent Name", typeof(string));
            dt.Columns.Add("Upline Name", typeof(string));
            dt.Columns.Add("Leaders Name", typeof(string));
            dt.Columns.Add("New / used car", typeof(string));
            dt.Columns.Add("Vcode", typeof(string));
            dt.Columns.Add("Brand", typeof(string));
            dt.Columns.Add("Year", typeof(string));
            dt.Columns.Add("Type", typeof(string));
            dt.Columns.Add("Series", typeof(string));
            dt.Columns.Add("Usage", typeof(string));
            dt.Columns.Add("Region", typeof(string));
            dt.Columns.Add("Registration number", typeof(string));
            dt.Columns.Add("Chasis number", typeof(string));
            dt.Columns.Add("Engine number", typeof(string));
            dt.Columns.Add("Product type", typeof(string));
            dt.Columns.Add("Product code", typeof(string));
            dt.Columns.Add("Basic cover", typeof(string));
            dt.Columns.Add("Sum insured", typeof(decimal));
            dt.Columns.Add("Premi", typeof(decimal));
            dt.Columns.Add("FU status", typeof(string));
            dt.Columns.Add("FU Date", typeof(string));
            dt.Columns.Add("Reason", typeof(string));
            dt.Columns.Add("Notes", typeof(string));
            dt.Columns.Add("Call Frequency", typeof(string));
            dt.Columns.Add("Is Dokumen Rep", typeof(string));
            dt.Columns.Add("Is Perubahan Nama Salesman / Dealer", typeof(string));
            dt.Columns.Add("Other Adjustment", typeof(string));
            dt.Columns.Add("Transaction Date", typeof(DateTime));
            dt.Columns.Add("Policy No", typeof(string));
            dt.Columns.Add("Period From", typeof(DateTime));
            dt.Columns.Add("Period To", typeof(DateTime));

            if (fuNewList.Count == 0)
            {
                DataRow dr = dt.NewRow();
                dr["Prospect Name"] = "No data";
                dt.Rows.Add(dr);
            }
            else
            {
                int count = 1;
                foreach (var fuNew in fuNewList)
                {
                    DataRow dr = dt.NewRow();
                    dr["Nomor"] = count++;
                    dr["Order Date"] = fuNew.orderDate;
                    dr["Prospect name"] = fuNew.prospectName;
                    dr["Marketing Code"] = fuNew.SalesOfficerID;
                    dr["Dealer Name"] = fuNew.dealerName;
                    dr["Salesman dealer name"] = fuNew.salesmanDealerName;
                    dr["Agent Name"] = fuNew.agentName;
                    dr["Upline Name"] = fuNew.uplineName;
                    dr["Leaders Name"] = fuNew.leadersName;
                    dr["New / used car"] = fuNew.isUsedCar;
                    dr["Vcode"] = fuNew.vCode;
                    dr["Brand"] = fuNew.vBrand;
                    dr["Year"] = fuNew.vYear;
                    dr["Type"] = fuNew.vType;
                    dr["Series"] = fuNew.vSeries;
                    dr["Usage"] = fuNew.usage;
                    dr["Region"] = fuNew.region;
                    dr["Registration number"] = fuNew.registrationNumber;
                    dr["Chasis number"] = fuNew.chasisNumber;
                    dr["Engine number"] = fuNew.engineNumber;
                    dr["Product type"] = fuNew.productType;
                    dr["Product code"] = fuNew.productCode;
                    dr["Basic cover"] = fuNew.basicCover;
                    dr["Sum insured"] = ToMoney(fuNew.sumInsured);
                    dr["Premi"] = ToMoney(fuNew.premi);
                    dr["FU status"] = fuNew.fuStatus;
                    dr["FU Date"] = fuNew.fuDate;
                    dr["Reason"] = fuNew.fuReason;
                    dr["Notes"] = fuNew.fuNotes;
                    dr["Call Frequency"] = fuNew.callFreq;
                    dr["Is Dokumen Rep"] = fuNew.isDocRep ;
                    dr["Is Perubahan Nama Salesman / Dealer"] = fuNew.isSalesDealerChange ;
                    dr["Other Adjustment"] = fuNew.otherAdjustment;
                    dr["Transaction Date"] = fuNew.transactionDate ;
                    dr["Policy No"] = fuNew.policyNo ;
                    dr["Period From"] = fuNew.periodFrom ;
                    dr["Period To"] = fuNew.periodTo ;

                    dt.Rows.Add(dr);
                }
            }
            return dt;
        }
        public static byte[] ExportUsingSimpleExcelWriterNew(DataTable table, string salesName, bool isMonthly = false)
        {
            byte[] resultDoc = null;
            int col = 0;
            int row = 0;

            string _directoryPath = string.Empty;
            string _fileName = string.Empty;

            var fileExt = ConfigurationManager.AppSettings["FileExtension"].ToString();

            _directoryPath = ConfigurationManager.AppSettings["FilePath"];
            if (!isMonthly)
            {
                _directoryPath = string.Concat(_directoryPath, "\\Otosales\\ReportFUNew\\", DateTime.Now.ToString("yyyy", CultureInfo.InvariantCulture), "\\", DateTime.Now.ToString("MMMM"), "\\");
                _fileName = string.Format(ConfigurationManager.AppSettings["AttachmentNameNew"], DateTime.Now.ToString("ddMMyyyy", CultureInfo.InvariantCulture) + " " + salesName + " " + fileExt);
            }
            else
            {
                _directoryPath = string.Concat(_directoryPath, "\\Otosales\\ReportFUNew\\", DateTime.Now.ToString("yyyy", CultureInfo.InvariantCulture), "\\");
                _fileName = string.Format(ConfigurationManager.AppSettings["AttachmentNameNew"], DateTime.Now.ToString("MMMM yyyy", CultureInfo.InvariantCulture) + " " + salesName + " " + fileExt);

            }

            if (!Directory.Exists(_directoryPath))
                Directory.CreateDirectory(_directoryPath);

            FileStream stream = new FileStream(string.Concat(_directoryPath, _fileName), FileMode.OpenOrCreate);
            ExcelWriter writer = new ExcelWriter(stream);


            writer.BeginWrite();

            foreach (DataColumn dataColumn in table.Columns)
            {
                writer.WriteCell(0, col, dataColumn.ColumnName);
                col++;
            }

            row = 1;
            foreach (DataRow dataRow in table.Rows)
            {
                col = 0;
                foreach (DataColumn dataColumn in table.Columns)
                {
                    if (col == 0 || col == 12)
                    {
                        writer.WriteCell(row, col, Convert.ToInt32(dataRow[dataColumn]));
                    }
                    else if (col == 23 || col==24)
                    {
                        writer.WriteCell(row, col, Convert.ToDouble(dataRow[dataColumn]));
                    }
                    else
                    {
                        writer.WriteCell(row, col, dataRow[dataColumn].ToString());
                    }

                    col++;
                }
                row++;
            }

            writer.EndWrite();
            stream.Close();

            //Excel.Application xlApp;
            //Excel.Workbook xlWorkBook;
            //Excel.Worksheet xlWorkSheet;
            //Excel.Range range;

            //xlApp = new Excel.Application();
            //xlWorkBook = xlApp.Workbooks.Open(_directoryPath + @"\" + _fileName, 0, true, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
            //xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            //range = xlWorkSheet.UsedRange;
            //xlWorkBook.SaveAs(_directoryPath + @"\" + _fileName, ".xls", null, null, true);

            resultDoc = File.ReadAllBytes(string.Concat(_directoryPath, _fileName));
            return resultDoc;
        }

        public static byte[] ExportUsingEPPlusNew(DataTable table, bool isMonthly = false)
        {
            byte[] resultDoc = null;
            int col = 0;
            int row = 0;

            string _directoryPath = string.Empty;
            string _fileName = string.Empty;

            var fileExt = ConfigurationManager.AppSettings["FileExtension"].ToString();

            _directoryPath = ConfigurationManager.AppSettings["FilePath"];
            if (!isMonthly)
            {
                _directoryPath = string.Concat(_directoryPath, "\\Otosales\\ReportFUNew\\", DateTime.Now.ToString("yyyy", CultureInfo.InvariantCulture), "\\", DateTime.Now.ToString("MMMM"), "\\");
                _fileName = string.Format(ConfigurationManager.AppSettings["AttachmentNameNew"], DateTime.Now.ToString("ddMMyyyy", CultureInfo.InvariantCulture) + fileExt);
            }
            else
            {
                _directoryPath = string.Concat(_directoryPath, "\\Otosales\\ReportFUNew\\", DateTime.Now.ToString("yyyy", CultureInfo.InvariantCulture), "\\");
                _fileName = string.Format(ConfigurationManager.AppSettings["AttachmentNameNew"], DateTime.Now.ToString("MMMM yyyy", CultureInfo.InvariantCulture) + fileExt);

            }

            if (!Directory.Exists(_directoryPath))
                Directory.CreateDirectory(_directoryPath);

            using (ExcelPackage pck = new ExcelPackage())
            {
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Report Follow Up New");
                ws.Cells["A1"].LoadFromDataTable(table, true);

                FileStream stream = new FileStream(string.Concat(_directoryPath, _fileName), FileMode.OpenOrCreate);
                pck.SaveAs(stream);
                stream.Close();
                resultDoc = File.ReadAllBytes(string.Concat(_directoryPath, _fileName));

            }

            return resultDoc;
        }

        private static void sendEmailNew(byte[] document, string emailTo, string salesName,bool isMonthly = false)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            var _fuAccess = new FollowUpAccess();
            _log.Info(actionName + " Function start : ");
            string subject = "Report Follow Up New Otosales " + (isMonthly ? DateTime.Now.ToString("MMyyyy", CultureInfo.InvariantCulture) : DateTime.Now.ToString("ddMMyyyy", CultureInfo.InvariantCulture));

            try
            {
                A2isMessageParam param = new A2isMessageParam();
                string today = DateTime.Now.ToString("ddMMyyyy", CultureInfo.InvariantCulture);
                var fileExt = ConfigurationManager.AppSettings["FileExtension"].ToString();
                //DateTime scheduleTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Date, 08, 00, 00, 000);

                param.ApplicationRequest = "FollowUpReport";
                param.ModuleRequest = "FURenew";
                param.MessageType = "MAIL"; // case sensitive type ={'MAIL','SMS'};
                param.MessageFrom = ConfigurationManager.AppSettings["EmailFrom"].ToString();
                param.MessageTo = emailTo;
                //param.MessageTo = ConfigurationManager.AppSettings["EmailTo"].ToString();
                //param.MessageCC = ConfigurationManager.AppSettings["EmailCC"].ToString();
                //param.MessageBcc = ConfigurationManager.AppSettings["EmailBCC"].ToString();
                param.MessageBody = getMailBodyFUNewDaily();
                param.MessageSchedule = DateTime.Now.ToString(A2isMessaging.A2isCommonStatic.FormatDateTimeUI);
                param.MessageDisplayName = "a2isinfo";
                param.MessageSubject = subject + " - " + salesName;
                param.MessagePriority = 1; //isinya 1 dan 0, kl tidak di isi 1 defaultnya  0
                param.MessageAttachment.Add(new A2isMessagingAttachment
                {
                    AttachmentDescription = String.Format("Report Follow Up New Otosales {0}", DateTime.Now.ToString("ddMMyyyy", CultureInfo.InvariantCulture)),
                    FileByte = document,
                    //FileContentType = fileContentType,
                    FileExtension = fileExt
                }); // isinya dari class MailAttachment A2isMessaging, dengan file sudah jadi format byte[]

                A2isCommonResult result = Messaging.ProcessQueuing(param, isDirect);
                if (result.ResultCode)
                {
                    _log.Debug(string.Format("Send email success, queue code {0}", result.ResultObject));
                }
                else
                {
                    _log.Error(result.ResultDesc);
                }
            }
            catch (Exception ex)
            {
                _log.Error("Function " + actionName + " Error : " + ex.ToString());
            }
        }
        private static string getMailBodyFUNewDaily()
        {
            string today = DateTime.Now.ToString("ddMMyyyy", CultureInfo.InvariantCulture);
            StringBuilder MailBody = new StringBuilder();
            MailBody.Append("Yth Bapak/Ibu,");
            MailBody.AppendLine("<br>");
            MailBody.AppendLine("<br>");
            MailBody.Append(String.Format("Berikut terlampir data order new as per {0}, silakan dilanjutkan next proses.",today));
            MailBody.AppendLine("<br>");
            MailBody.AppendLine("<br>");
            MailBody.Append("Atas perhatian dan kerjasamanya, kami ucapkan terima kasih.");
            return MailBody.ToString();
        }

        public static void ReportRawData()
        {
            var actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;

            DateTime endDate = DateTime.Now;
            bool isFirstDayOfMonth = endDate.Date == new DateTime(endDate.Year, endDate.Month, 1).Date;
            try
            {
                _log.Info("Function " + actionName + " BEGIN : ");
                string dateType = ConfigurationManager.AppSettings["DateType"].ToString();
                bool isManual = Convert.ToBoolean(ConfigurationManager.AppSettings["IsManualGenerate"].ToString());

                DateTime startDate = new DateTime(1900, 1, 1);
                string cvStartDate = string.Empty;
                string cvEndDate = string.Empty;

                if (isManual)
                {
                    cvStartDate = ConfigurationManager.AppSettings["ReportFromDate"].ToString();
                    cvEndDate = ConfigurationManager.AppSettings["ReportToDate"].ToString();

                    bool isMonthly = ConfigurationManager.AppSettings["DateType"].ToString().ToUpper() == "M";
                    if (isMonthly)
                    {
                        startDate = new DateTime(endDate.Year, endDate.Month, 1).Date;
                        endDate = new DateTime(endDate.Year, endDate.Month, endDate.Day, 23, 59, 59);
                        cvStartDate = startDate.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture);
                        cvEndDate = endDate.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture);
                    }
                    SendAttachmentRawData(cvStartDate, cvEndDate, isMonthly);
                }
                else
                {
                    if (isFirstDayOfMonth)
                    {
                        startDate = endDate.AddMonths(-1).Date;
                        cvStartDate = startDate.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture);
                        cvEndDate = endDate.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture);
                        SendAttachmentRawData(cvStartDate, cvEndDate, false);

                    } else
                    {
                        startDate = new DateTime(endDate.Year, endDate.Month, 1).Date;
                        endDate = new DateTime(endDate.Year, endDate.Month, endDate.Day, 23, 59, 59);

                        cvStartDate = startDate.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture);
                        cvEndDate = endDate.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture);

                        SendAttachmentRawData(cvStartDate, cvEndDate);
                    }

                }
            }
            catch (Exception ex)
            {
                _log.Error("Function " + actionName + " Error : " + ex.ToString());
                string attachname = String.Format(ConfigurationManager.AppSettings["AttachmentName"].ToString(), isFirstDayOfMonth ? DateTime.Now.ToString("MMyyyy", CultureInfo.InvariantCulture) : DateTime.Now.ToString("ddMMyyyy", CultureInfo.InvariantCulture));
                string exc = ex.ToString();
            }
        }

        private static void SendAttachmentRawData(string cvStartDate, string cvEndDate, bool isMonthly = false)
        {
            var actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;

            try
            {
                List<RawData> rawList = new List<RawData>();
                List<SalesOfficer> salesList = new List<SalesOfficer>();
                List<SalesOfficer> uplineList = new List<SalesOfficer>();
                List<RawData> dataList = new List<RawData>();

                rawList = FollowUpAccess.GetRawData(cvStartDate, cvEndDate);

                salesList = FollowUpAccess.GetSalesOfficer();
                foreach (var sales in salesList)
                {
                    dataList.AddRange(rawList.Where(x => !string.IsNullOrEmpty(x.userOtosales) && x.userOtosales.Trim().Equals(sales.SalesOfficerID.Trim(), StringComparison.InvariantCultureIgnoreCase)));
                    if (dataList.Count > 0)
                    {
                        var rawDataTable = ConvertRawDataIntoDataTable(dataList);
                        //var rawDoc = ExportUsingEPPlusRaw(rawDataTable, isMonthly);
                        var rawDoc = ExportUsingSimpleExcelWriterRaw(rawDataTable, sales.Name, isMonthly);
                        sendEmailRaw(rawDoc, sales.Email, sales.Name, isMonthly);
                    }
                    dataList.Clear();
                }
                uplineList = FollowUpAccess.GetUplinerOfficer();
                foreach (var up in uplineList)
                {
                    List<string> subOrdinateList = new List<string>();
                    subOrdinateList = FollowUpAccess.GetSubOrdinates(up.SalesOfficerID);
                    if (subOrdinateList.Count > 0)
                    {
                        dataList.AddRange(rawList.Where(x => !string.IsNullOrEmpty(x.userOtosales) && subOrdinateList.Contains(x.userOtosales.ToUpper().Trim())));
                        if (dataList.Count > 0)
                        {
                            var rawDataTable = ConvertRawDataIntoDataTable(dataList);
                            //var rawDoc = ExportUsingEPPlusRaw(rawDataTable, isMonthly);
                            var rawDoc = ExportUsingSimpleExcelWriterRaw(rawDataTable, up.Name, isMonthly);
                            sendEmailRaw(rawDoc, up.Email, up.Name, isMonthly);
                        }
                        dataList.Clear();
                    }
                }

                string res = "Report Succesfully Created";
                _log.Info("Function " + actionName + " END : ");
            }
            catch (Exception ex)
            {
                _log.Error("Function " + actionName + " Error : " + ex.ToString());
                string exc = ex.ToString();
            }
        }

        private static DataTable ConvertRawDataIntoDataTable(List<RawData> rawList)
        {
            var dt = new DataTable();
            dt.Columns.Add("Nomor", typeof(string));
            dt.Columns.Add("Transaction No", typeof(string));
            dt.Columns.Add("Policy No", typeof(string));
            dt.Columns.Add("Order No", typeof(string));
            dt.Columns.Add("Endorsement No", typeof(string));
            dt.Columns.Add("Name on Policy", typeof(string));
            dt.Columns.Add("Order Type 1", typeof(string));
            dt.Columns.Add("Period From", typeof(DateTime));
            dt.Columns.Add("Period To", typeof(DateTime));
            dt.Columns.Add("Input Date", typeof(DateTime));
            dt.Columns.Add("Approve Date", typeof(DateTime));
            dt.Columns.Add("Product Code", typeof(string));
            dt.Columns.Add("Product Name", typeof(string));
            //dt.Columns.Add("Segment Code - Description", typeof(string));
            dt.Columns.Add("Entry User", typeof(string));
            dt.Columns.Add("Marketing Code - Name", typeof(string));
            dt.Columns.Add("Branch Name", typeof(string));
            dt.Columns.Add("Sum Insured", typeof(string));
            dt.Columns.Add("Business Source", typeof(string));
            dt.Columns.Add("Vehicle Code", typeof(string));
            dt.Columns.Add("Brand", typeof(string));
            dt.Columns.Add("Object Description", typeof(string));
            dt.Columns.Add("Mfg Year", typeof(int));
            dt.Columns.Add("Chasis number", typeof(string));
            dt.Columns.Add("Engine number", typeof(string));
            dt.Columns.Add("Registration number", typeof(string));
            dt.Columns.Add("Coverage", typeof(string));
            dt.Columns.Add("Dealer ID", typeof(string));
            dt.Columns.Add("Dealer / Broker", typeof(string));
            dt.Columns.Add("Salesman Dealer Code", typeof(string));
            dt.Columns.Add("Salesman Dealer Name", typeof(string));
            dt.Columns.Add("Upliner Name", typeof(string));
            dt.Columns.Add("Leader Name", typeof(string));


            if (rawList.Count == 0)
            {
                DataRow dr = dt.NewRow();
                dr["Name on Policy"] = "No data";
                dt.Rows.Add(dr);
            }
            else
            {
                int count = 1;
                foreach (var raw in rawList)
                {
                    DataRow dr = dt.NewRow();
                    dr["Nomor"] = count++;
                    dr["Transaction No"] = raw.transactionNo ;
                    dr["Policy No"] = raw.policyNo ;
                    dr["Order No"] = raw.orderNo;
                    dr["Endorsement No"] = raw.endorsementNo ;
                    dr["Name on Policy"] = raw.nameOnPolicy ;
                    dr["Order Type 1"] = raw.orderType ;
                    dr["Period From"] = raw.periodFrom ;
                    dr["Period To"] = raw.periodTo ;
                    dr["Input Date"] = raw.inputDate ;
                    dr["Approve Date"] = raw.approveDate ;
                    dr["Product Code"] = raw.productCode ;
                    dr["Product Name"] = raw.productName ;
                    //dr["Segment Code - Description"] = raw.segmentCode ;
                    dr["Entry User"] = raw.entryUser;
                    dr["Marketing Code - Name"] = raw.userOtosales + " - " +raw.marketingName;
                    dr["Branch Name"] = raw.branchName ;
                    dr["Sum Insured"] = ToMoney(raw.sumInsured);
                    dr["Business Source"] = raw.businessSource ;
                    dr["Vehicle Code"] = raw.vCode ;
                    dr["Brand"] = raw.vBrand ;
                    dr["Object Description"] = raw.objectDescription ;
                    dr["Mfg Year"] = raw.mfgYear;
                    dr["Chasis Number"] = raw.chasisNumber ;
                    dr["Engine Number"] = raw.engineNumber ;
                    dr["Registration Number"] = raw.registrationNumber ;
                    dr["Coverage"] = raw.basicCover ;
                    dr["Dealer ID"] = raw.dealerID ;
                    dr["Dealer / Broker"] = raw.dealerBroker ;
                    dr["Salesman Dealer Code"] = raw.salesmanCode ;
                    dr["Salesman Dealer Name"] = raw.salesmanName ;
                    dr["Upliner Name"] = raw.uplinerName ;
                    dr["Leader Name"] = raw.leaderName ;

                    dt.Rows.Add(dr);
                }
            }
            return dt;
        }
        public static byte[] ExportUsingSimpleExcelWriterRaw(DataTable table, string salesName, bool isMonthly = false)
        {
            byte[] resultDoc = null;
            int col = 0;
            int row = 0;

            string _directoryPath = string.Empty;
            string _fileName = string.Empty;

            var fileExt = ConfigurationManager.AppSettings["FileExtension"].ToString();

            _directoryPath = ConfigurationManager.AppSettings["FilePath"];
            if (!isMonthly)
            {
                _directoryPath = string.Concat(_directoryPath, "\\Otosales\\ReportRawData\\", DateTime.Now.ToString("yyyy", CultureInfo.InvariantCulture), "\\", DateTime.Now.ToString("MMMM"), "\\");
                _fileName = string.Format(ConfigurationManager.AppSettings["AttachmentNameRaw"], DateTime.Now.ToString("ddMMyyyy", CultureInfo.InvariantCulture) + " " + salesName + " " + fileExt);
            }
            else
            {
                _directoryPath = string.Concat(_directoryPath, "\\Otosales\\ReportRawData\\", DateTime.Now.ToString("yyyy", CultureInfo.InvariantCulture), "\\");
                _fileName = string.Format(ConfigurationManager.AppSettings["AttachmentNameRaw"], DateTime.Now.ToString("MMMM yyyy", CultureInfo.InvariantCulture) + " " + salesName + " " + fileExt);

            }

            if (!Directory.Exists(_directoryPath))
                Directory.CreateDirectory(_directoryPath);

            FileStream stream = new FileStream(string.Concat(_directoryPath, _fileName), FileMode.OpenOrCreate);
            ExcelWriter writer = new ExcelWriter(stream);


            writer.BeginWrite();

            foreach (DataColumn dataColumn in table.Columns)
            {
                writer.WriteCell(0, col, dataColumn.ColumnName);
                col++;
            }

            row = 1;
            foreach (DataRow dataRow in table.Rows)
            {
                col = 0;
                foreach (DataColumn dataColumn in table.Columns)
                {
                    if (col==0 || col==22)
                    {
                        writer.WriteCell(row, col, Convert.ToInt32(dataRow[dataColumn]));
                    }
                    else if (col==17)
                    {
                        writer.WriteCell(row, col, Convert.ToDouble(dataRow[dataColumn]));
                    }
                    else
                    {
                        writer.WriteCell(row, col, dataRow[dataColumn].ToString());
                    }

                col++;
                }
                row++;
            }

            writer.EndWrite();
            stream.Close();

            //Excel.Application xlApp;
            //Excel.Workbook xlWorkBook;
            //Excel.Worksheet xlWorkSheet;
            //Excel.Range range;

            //xlApp = new Excel.Application();
            //xlWorkBook = xlApp.Workbooks.Open(_directoryPath + @"\" + _fileName, 0, true, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
            //xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            //range = xlWorkSheet.UsedRange;
            //xlWorkBook.SaveAs(_directoryPath + @"\" + _fileName, ".xls", null, null, true);

            resultDoc = File.ReadAllBytes(string.Concat(_directoryPath, _fileName));
            return resultDoc;
        }
        public static byte[] ExportUsingEPPlusRaw(DataTable table, bool isMonthly = false)
        {
            byte[] resultDoc = null;
            int col = 0;
            int row = 0;

            string _directoryPath = string.Empty;
            string _fileName = string.Empty;

            var fileExt = ConfigurationManager.AppSettings["FileExtension"].ToString();

            _directoryPath = ConfigurationManager.AppSettings["FilePath"];
            if (!isMonthly)
            {
                _directoryPath = string.Concat(_directoryPath, "\\Otosales\\ReportRawData\\", DateTime.Now.ToString("yyyy", CultureInfo.InvariantCulture), "\\", DateTime.Now.ToString("MMMM"), "\\");
                _fileName = string.Format(ConfigurationManager.AppSettings["AttachmentNameRaw"], DateTime.Now.ToString("ddMMyyyy", CultureInfo.InvariantCulture) + fileExt);
            }
            else
            {
                _directoryPath = string.Concat(_directoryPath, "\\Otosales\\ReportRawData\\", DateTime.Now.ToString("yyyy", CultureInfo.InvariantCulture), "\\");
                _fileName = string.Format(ConfigurationManager.AppSettings["AttachmentNameRaw"], DateTime.Now.ToString("MMMM yyyy", CultureInfo.InvariantCulture) + fileExt);

            }

            if (!Directory.Exists(_directoryPath))
                Directory.CreateDirectory(_directoryPath);

            using (ExcelPackage pck = new ExcelPackage())
            {
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Report Daily Raw Data");
                //ws.Column(0).Style.Numberformat.Format = "0";
                //ws.Column(6).Style.Numberformat.Format = "yyyy-MM-dd hh:mm:ss";
                //ws.Column(7).Style.Numberformat.Format = "yyyy-MM-dd hh:mm:ss";
                //ws.Column(8).Style.Numberformat.Format = "yyyy-MM-dd hh:mm:ss";
                //ws.Column(9).Style.Numberformat.Format = "yyyy-MM-dd hh:mm:ss";

                ws.Cells["A1"].LoadFromDataTable(table, true);

                FileStream stream = new FileStream(string.Concat(_directoryPath, _fileName), FileMode.OpenOrCreate);
                pck.SaveAs(stream);
                stream.Close();
                resultDoc = File.ReadAllBytes(string.Concat(_directoryPath, _fileName));

            }

            return resultDoc;
        }

        private static void sendEmailRaw(byte[] document, string emailTo, string salesName,bool isMonthly = false)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            var _fuAccess = new FollowUpAccess();
            _log.Info(actionName + " Function start : ");
            string subject = "Report Daily Raw Data Otosales " + (isMonthly ? DateTime.Now.ToString("MMyyyy", CultureInfo.InvariantCulture) : DateTime.Now.ToString("ddMMyyyy", CultureInfo.InvariantCulture));

            try
            {
                A2isMessageParam param = new A2isMessageParam();
                string today = DateTime.Now.ToString("ddMMyyyy", CultureInfo.InvariantCulture);
                var fileExt = ConfigurationManager.AppSettings["FileExtension"].ToString();
                //DateTime scheduleTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Date, 08, 00, 00, 000);

                param.ApplicationRequest = "ReportRawData";
                param.ModuleRequest = "FURenew";
                param.MessageType = "MAIL"; // case sensitive type ={'MAIL','SMS'};
                param.MessageFrom = ConfigurationManager.AppSettings["EmailFrom"].ToString();
                param.MessageTo = emailTo;
                //param.MessageTo = ConfigurationManager.AppSettings["EmailTo"].ToString();
                //param.MessageCC = ConfigurationManager.AppSettings["EmailCC"].ToString();
                //param.MessageBcc = ConfigurationManager.AppSettings["EmailBCC"].ToString();
                param.MessageBody = getMailBodyRawDataDaily();
                param.MessageSchedule = DateTime.Now.ToString(A2isMessaging.A2isCommonStatic.FormatDateTimeUI);
                param.MessageDisplayName = "a2isinfo";
                param.MessageSubject = subject + " - " + salesName;
                param.MessagePriority = 1; //isinya 1 dan 0, kl tidak di isi 1 defaultnya  0
                param.MessageAttachment.Add(new A2isMessagingAttachment
                {
                    AttachmentDescription = String.Format("Report Daily Raw Data Otosales {0}", DateTime.Now.ToString("ddMMyyyy", CultureInfo.InvariantCulture)),
                    FileByte = document,
                    //FileContentType = fileContentType,
                    FileExtension = fileExt
                }); // isinya dari class MailAttachment A2isMessaging, dengan file sudah jadi format byte[]

                A2isCommonResult result = Messaging.ProcessQueuing(param, isDirect);
                if (result.ResultCode)
                {
                    _log.Debug(string.Format("Send email success, queue code {0}", result.ResultObject));
                }
                else
                {
                    _log.Error(result.ResultDesc);
                }
            }
            catch (Exception ex)
            {
                _log.Error("Function " + actionName + " Error : " + ex.ToString());
            }
        }
        private static string getMailBodyRawDataDaily()
        {
            string today = DateTime.Now.ToString("ddMMyyyy", CultureInfo.InvariantCulture);
            StringBuilder MailBody = new StringBuilder();
            MailBody.Append("Yth Bapak/Ibu,");
            MailBody.AppendLine("<br>");
            MailBody.AppendLine("<br>");
            MailBody.Append(String.Format("Berikut terlampir data order raw data Otosales as per {0}, silakan dilanjutkan next proses.", today));
            MailBody.AppendLine("<br>");
            MailBody.AppendLine("<br>");
            MailBody.Append("Atas perhatian dan kerjasamanya, kami ucapkan terima kasih.");
            return MailBody.ToString();
        }

        public static string ToMoney(decimal amount)
        {
            //return String.Format(CultureInfo.CreateSpecificCulture("id-id"), "{0:N}", amount);
            return String.Format(CultureInfo.CreateSpecificCulture("en-us"), "{0:N}", amount);
        }
    }
}
