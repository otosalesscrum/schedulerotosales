﻿using Otosales.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Otosales.Approval.Repository
{
    class LoggingRepository : RepositoryBase
    {
        public static int Info(DateTime requestDate, string requestMessage, DateTime responseDate, string responseMessage, string reference = "")
        {
            string qInsertLog = @"INSERT INTO GardaMobile.Otosales.ApproveOrderRequestLog([RequestDate],[RequestMessage],[ResponseDate],[ResponseMessage],[Reference] )
VALUES(@0,@1,@2,@3,@4)
";
            using (var db = GetAABMobileDB())
            {
                return db.Execute(qInsertLog, requestDate, requestMessage, responseDate, responseMessage, reference);
            }
        }

        public static string GetRequestMessage(HttpMethod method, string url, AuthenticationHeaderValue authHeader, string body)
        {
            return string.Format(" {0} {1}{2} {3}{4} {5}", method, url, Environment.NewLine
                , authHeader != null ? authHeader.Scheme + " " + authHeader.Parameter : "", Environment.NewLine
                , body);
        }

        public static string GetResponseMessage(HttpStatusCode statusCode, string url, string response)
        {
            return string.Format(" {0} {1}{2} {3}", statusCode, url, Environment.NewLine
                , response);
        }

    }
}
