﻿using a2is.Framework.Monitoring;
using Otosales.Approval.Models;
using Otosales.Logic;
using Otosales.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Otosales.Models.Constant;

namespace Otosales.Approval.Repository
{
    class OtosalesOrderRepository : RepositoryBase
    {
        private static a2isLogHelper logger = new a2isLogHelper();
        public List<dynamic> GetFollowUpSendToSAorNeedApprovalAndSubmitBySAStatus()
        {
            //Tanya JSW, mst_oder_mobile.approval_status :  0 : ?, 1 : approved, 2 : ? 
            //            string qGet = @"SELECT mom1.Order_No, mom1.Approval_Status, mom1.Approval_Process, Email_SA, CAST(COALESCE(fu.IsRenewal,0) AS BIT) [IsRenewal], os.SalesOfficerID
            //FROM (
            //select Order_No, max(Order_ID) [Order_ID]
            //from dbo.mst_order_mobile mom WITH ( NOLOCK )
            //group by Order_No
            //) a 
            //INNER JOIN dbo.mst_order_mobile mom1 WITH ( NOLOCK ) ON a.Order_ID = mom1.Order_ID 
            //INNER JOIN dbo.mst_order mo WITH ( NOLOCK ) ON mo.order_no = mom1.Order_No and mo.Order_Status = '50' AND mo.Order_Type = 1
            //INNER JOIN beyondmoss.aabmobile.dbo.OrderSimulation os WITH ( NOLOCK ) ON os.PolicyOrderNo = mom1.Order_No and os.ApplyF = 1
            //INNER JOIN beyondmoss.aabmobile.dbo.FollowUp fu WITH ( NOLOCK ) ON fu.FollowUpNo = os.FollowUpNo and fu.FollowUpInfo IN (@0,@1) and COALESCE(fu.IsRenewal,0) = 0
            //WHERE Approval_Status = 0 and Approval_Process = 0
            //and mom1.SA_State = @2";
            string qGet = @";
select fu.IsRenewal, fu.FollowUpNo
into #temp1
from beyondmoss.aabmobile.dbo.FollowUp fu WITH ( NOLOCK ) 
Where fu.FollowUpInfo IN (@0,@1) and COALESCE(fu.IsRenewal,0) = 0
and fu.rowstatus = 1

SELECT followupno, SalesOfficerID, PolicyOrderNo, OldPolicyNo 
INTO #temp2
FROM beyondmoss.aabmobile.dbo.OrderSimulation os
where OldPolicyNo IS NULL AND PolicyOrderNo IS NOT NULL
AND ApplyF = 1 and RowStatus = 1

SELECT SalesOfficerID, [IsRenewal], PolicyOrderNo into #temp
FROM #temp1 a
INNER JOIN #temp2 b ON a.followupno = b.followupno
​
select mom1.Order_No, mom1.Approval_Status, mom1.Approval_Process, Email_SA, CAST(COALESCE(t.IsRenewal,0) AS BIT) [IsRenewal], SalesOfficerID​​, mo.Order_Type
from #temp t ​
inner join (​
select Order_No, max(Order_ID) [Order_ID]​
from dbo.mst_order_mobile mom WITH ( NOLOCK )​
group by Order_No​
) a  ON t.PolicyOrderNo = a.Order_No​
INNER JOIN dbo.mst_order_mobile mom1 WITH ( NOLOCK ) ON a.Order_ID = mom1.Order_ID ​
INNER JOIN dbo.mst_order mo WITH ( NOLOCK ) ON mo.order_no = mom1.Order_No and mo.Order_Status = '50' AND mo.Order_Type = 1​
WHERE mom1.SA_State = @2 and Approval_Status = 0 and Approval_Process = 0​

DROP TABLE #temp1
DROP TABLE #temp2
DROP TABLE #temp​";
            using (var db = GetAABDB())
            {
                return db.Fetch<dynamic>(qGet, FollowUpInfo.SendToSA, FollowUpInfo.NeedApproval, SAState.SubmitBySA);
            }
        }

        public List<dynamic> GetFollowUpWithFUPembayaran()
        {
            //Tanya JSW, mst_oder_mobile.approval_status :  0 : ?, 1 : approved, 2 : ? 
            string qGet = @";
select fu.IsRenewal, fu.FollowUpNo
into #temp1
from beyondmoss.aabmobile.dbo.FollowUp fu WITH ( NOLOCK ) 
Where fu.FollowUpInfo IN (@0,@1) and COALESCE(fu.IsRenewal,0) = 1
and fu.rowstatus = 1

SELECT followupno, SalesOfficerID, PolicyOrderNo, OldPolicyNo 
INTO #temp2
FROM beyondmoss.aabmobile.dbo.OrderSimulation os
where OldPolicyNo IS NOT NULL AND PolicyOrderNo IS NOT NULL
AND ApplyF = 1 and RowStatus = 1

SELECT SalesOfficerID, [IsRenewal], PolicyOrderNo into #temp
FROM #temp1 a
INNER JOIN #temp2 b ON a.followupno = b.followupno

SELECT mom1.Order_No, mom1.Approval_Status, mom1.Approval_Process, Email_SA, CAST(COALESCE(t.IsRenewal,0) AS BIT) [IsRenewal], t.SalesOfficerID, mo.Order_Type
FROM dbo.mst_order mo WITH ( NOLOCK ) 
INNER JOIN #temp t ON t.PolicyOrderNo = mo.Order_No AND mo.Order_Type = 2
LEFT JOIN (
select Order_No, max(Order_ID) [Order_ID]
from dbo.mst_order_mobile mom WITH ( NOLOCK ) 
group by Order_No
) a ON mo.order_no = a.Order_No 
LEFT JOIN dbo.mst_order_mobile mom1 WITH ( NOLOCK ) ON a.Order_ID = mom1.Order_ID 
WHERE Approval_Status = 0 and Approval_Process = 0 AND mo.Order_Status = '50'
and 1 = case when mom1.Order_ID is not null then IIF(mom1.SA_State IN (@2,@3), 1,0) else 1 end

DROP TABLE #temp1
DROP TABLE #temp2
DROP TABLE #temp";
            using (var db = GetAABDB())
            {
                return db.Fetch<dynamic>(qGet, FollowUpInfo.FUPayment, FollowUpInfo.NeedApproval, SAState.SubmitBySA, SAState.RenewalNoSA);
            }
        }


        public dynamic GetDataCommissionSameDay(string orderNo)
        {
            //TODO : confirm REL apakah commisssion same day ini harus BSO & PSO atau salah satu aja udah bisa pembentukan komisi.
            string query = @"select top 1 Salesman_Id,Product_Code,Branch_Id 
from mst_order mo
INNER JOIN Mst_General mg1 WITH ( NOLOCK ) ON mg1.type = 'BSO' and mg1.code = mo.Branch_Id and mg1.status = 1
INNER JOIN Mst_General mg2 WITH ( NOLOCK ) ON mg2.type = 'PSO' and mg2.code = mo.Product_code and mg2.status = 1
where order_no = @0
";
            using (var db = GetAABDB())
            {
                return db.FirstOrDefault<dynamic>(query, orderNo);
            }
        }
        public dynamic GetDataPolicyCreatedBeforePayment(string orderNo)
        {
            //TODO : tanya ke JSW, dia simpen ga di table ORD ? kalo iya baca flagnya di table ORD jangan ke ordersimulation, btw disini juga blm ada flag IsPolicyCreatedBeforePayment
            string query = @" SELECT TOP 1 1 FROM OrderSimulation where PolicyOrderNo = @0 and ApplyF = 1 and IsPolicyIssuedBeforePaying = 1 ";
            using (var db = GetAABMobileDB())
            {
                return db.FirstOrDefault<dynamic>(query, orderNo);
            }
        }
        public dynamic GetDataPolicyDeliveryAddressNotCustomer(string orderNo)
        {
            //TODO : query ke flag yang dibikin JSW di table ord_dtl_address
            string query = @" SELECT TOP 1 1 FROM ord_dtl_address WITH ( NOLOCK ) where order_No = @0 and Address_Type = 'DELIVR' and Delivery_Code NOT IN ('CS','CH')";
            using (var db = GetAABDB())
            {
                return db.FirstOrDefault<dynamic>(query, orderNo);
            }
        }
        public dynamic GetDataSalesmanDealerChanged(string orderNo)
        {
            //TODO : query ke flag yang dibikin JSW di table ord_dtl_address
            string query = @" select top 1 p1.order_no [old_order_no], mo.Order_No [new_order_no], mo1.Broker_Code [old_broker_code], mo.Broker_Code [new_broker_code]
from Mst_Order mo 
INNER JOIN ComSales_Salesman cs WITH ( NOLOCK ) ON mo.Broker_Code = cs.Cust_ID
INNER JOIN Policy p1 WITH ( NOLOCK ) on p1.Policy_No = mo.Old_Policy_No
INNER JOIN Mst_Order mo1 WITH ( NOLOCK ) on mo1.Order_No = p1.Order_No
where mo.Order_No like @0 and mo.Broker_Code <> '' and mo.Order_Type = '2' and mo.Broker_Code <> mo1.Broker_Code";
            using (var db = GetAABDB())
            {
                return db.FirstOrDefault<dynamic>(query, orderNo);
            }
        }

        public MstOrderMobileApprovalState GetMstOrderMobileApprovalState(string orderNo)
        {
            //TODO : query ke mst_order_mobile_approval where status = 1 dan approvalstatus = 1 MstOrderMobileApprovalStatus.Approve;
            // TODO : compare SUM(approvalstatus) dgn count( where status = 1 ) kalo sama valid.
            string query = @"SELECT COUNT(*) [Total], COALESCE(SUM(IIF(ApprovalStatus = @1, 1, 0)),0) [SumApproved],
COALESCE(SUM(IIF(ApprovalStatus = @2 AND ApprovalType = @3, 1, 0)),0) [SumRejected]
FROM mst_order_mobile_approval omap WITH ( NOLOCK ) 
WHERE Order_No = @0 AND [Status] = 1";

            using (var db = GetAABDB())
            {
                var item = db.FirstOrDefault<dynamic>(query, orderNo, ApprovalStatus.APPROVE, ApprovalStatus.REJECT_OR_REVISE, ApprovalType.NextLimit);
                if(item.Total == 0 || (item.Total > 0 && item.Total == item.SumApproved))
                {
                    return MstOrderMobileApprovalState.APPROVED;
                }
                else if (item.Total != 0 && item.SumRejected > 0)
                {
                    return MstOrderMobileApprovalState.REJECTED;
                }
                else
                {
                    return MstOrderMobileApprovalState.INPROGRESS;
                }
            }
        }

        public bool GetIsPolicyCreatedBeforePayment(string orderNo)
        {
            //TODO: benerin querynya krna blm tau cara ambil flag 'Polis Terbit sebelum bayar' apakah bs dr table ORD, atau dari ordersimulation otosales? 
            string query = @"SELECT TOP 1 1 FROM OrderSimulation WHERE PolicyOrderNo = @0 and ApplyF = 1 and COALESCE(IsPolicyIssuedBeforePaying,0) = 1";

            using (var db = GetAABMobileDB())
            {
                return db.Fetch<dynamic>(query, orderNo).Count > 0;
            }
        }

        public OrderPaymentCustom GetTotalBayarVA(string VANumber, DateTime transactionDate, string orderno)
        {
            var mbldb = GetAABMobileDB();
            string qGetDate = @";DECLARE @@IsRenewal INT
            SELECT @@IsRenewal=COALESCE(IsRenewal,0) FROM dbo.OrderSimulation os 
            INNER JOIN dbo.FollowUp f 
            ON f.FollowUpNo = os.FollowUpNo
            WHERE PolicyOrderNo = @0
            IF(@@IsRenewal=0)
            BEGIN
	            SELECT EntryDate StartDate, DATEADD(DAY,CAST(ParamValue AS INT),EntryDate) EndDate FROM dbo.OrderSimulation os
	            INNER JOIN dbo.ApplicationParameters ap ON 1=1 AND ParamName = 'END-CHECKVAPAYMENT'
	            WHERE PolicyOrderNo = @0
            END
            ELSE
            BEGIN
	            SELECT DATEADD(DAY,CAST(ap1.ParamValue AS INT),p.Period_To) StartDate, 
	            DATEADD(DAY,CAST(ap2.ParamValue AS INT),DATEADD(DAY,CAST(ap1.ParamValue AS INT),p.Period_To)) EndDate
	            FROM BEYONDREPORT.AAB.dbo.Mst_Order mo
	            INNER JOIN BEYONDREPORT.AAB.dbo.Policy p ON p.Policy_No = mo.Old_Policy_No
	            INNER JOIN dbo.ApplicationParameters ap1 ON 1=1 AND ap1.ParamName = 'START-CHECKVAPAYMENT'
	            INNER JOIN dbo.ApplicationParameters ap2 ON 1=1 AND ap2.ParamName = 'END-CHECKVAPAYMENT'
	            WHERE mo.Order_No = @0
            END";
            List<dynamic> lsDate = mbldb.Fetch<dynamic>(qGetDate, orderno);
            using (var db = Geta2isBeyondDB())
            {
                string trnsDate = "";
                string exprDate = "";

                if (transactionDate != null)
                {
                    string dayadd = "14"; //GeneralAsuransiAstraAccess.GetApplicationParameters("ORDER-EXPIRED-DAY-ADD");
                    string hour = "00";//GeneralAsuransiAstraAccess.GetApplicationParameters("ORDER-EXPIRED-HOUR");
                    string minute = "00";//GeneralAsuransiAstraAccess.GetApplicationParameters("ORDER-EXPIRED-MINUTE");
                    string second = "00";//GeneralAsuransiAstraAccess.GetApplicationParameters("ORDER-EXPIRED-SECOND");

                    // BEGIN 0043 Hotfix 2019 by BLO :  - Bugfix exception when the transactionDate.Day = last day of month (example : 31 ) 
                    //DateTime expDate = new DateTime(transactionDate.Year, transactionDate.Month,
                    //    transactionDate.Day + Int32.Parse(dayadd), Int32.Parse(hour), Int32.Parse(minute), Int32.Parse(second), 0);
                    DateTime expDate = new DateTime(transactionDate.Year, transactionDate.Month,
                        transactionDate.Day, Int32.Parse(hour), Int32.Parse(minute), Int32.Parse(second), 0);
                    expDate = expDate.AddDays(Convert.ToDouble(dayadd));
                    // END 0043 Hotfix 2019 by BLO - Bugfix exception when the transactionDate.Day = last day of month (example : 31 ) 

                    trnsDate = lsDate.Count>0 ? lsDate.First().StartDate.ToString("yyyy-MM-dd HH:mm:ss.ff") : transactionDate.ToString("yyyy-MM-dd HH:mm:ss.ff");
                    exprDate = lsDate.Count > 0 ? lsDate.First().EndDate.ToString("yyyy-MM-dd HH:mm:ss.ff") : expDate.ToString("yyyy-MM-dd HH:mm:ss.ff");

                }

                string cSQLGetTotalBayarVA = @";WITH    cte
                                                        AS ( SELECT PermataVirtualAccountSettlementID ,
                                                                    a.VI_TraceNo
                                                            FROM     PolicyInsurance.PermataVirtualAccountSettlement a
                                                                    INNER JOIN PolicyInsurance.PermataVirtualAccount b ON a.PermataVirtualAccountID = b.PermataVirtualAccountID
                                                            WHERE    b.VI_VANumber IN (
                                                                        SELECT  value
                                                                        FROM    PolicyInsurance.fn_Split(@0, ',') 
                                                                     )
                                                        --AND a.PermataVirtualAccountPaymentSourceCode = '02'
		                                                AND a.VI_TransactionDate BETWEEN @1 AND @2
                                                            )
                                                SELECT  MAX(pvas.PermataVirtualAccountSettlementID) as PermataVirtualAccountSettlementID,
                                                        SUM(pvas.VI_Amount) AS TotalBayar ,
                                                        MAX(pvas.VI_TransactionDate) AS MaxTransactionDate ,
                                                        MAX(pva.BillAmount) AS TotalBill
                                                FROM    cte
                                                        INNER JOIN PolicyInsurance.PermataVirtualAccountSettlement AS pvas ON pvas.PermataVirtualAccountSettlementID = cte.PermataVirtualAccountSettlementID
                                                                                                            AND pvas.VI_TraceNo = cte.VI_TraceNo
                                                        INNER JOIN PolicyInsurance.PermataVirtualAccount AS pva ON pva.PermataVirtualAccountID = pvas.PermataVirtualAccountID
                                                WHERE   IsReversal = 0";
                return db.Query<OrderPaymentCustom>(cSQLGetTotalBayarVA, VANumber, trnsDate, exprDate).FirstOrDefault();
            }

        }

        public PermataVirtualAccountOvershortage GetVAOverShortStage(string Curr)
        {
            string cSQlGetOvershortstage = @";SELECT  PermataVirtualAccountOvershortageID, 
		                                            CurrencyCode, 
		                                            LowerLimitAmount, 
		                                            UpperLimitAmount, 
		                                            CreatedBy, 
		                                            CreatedDate, 
		                                            ModifiedBy, 
		                                            ModifiedDate 
                                            FROM	PolicyInsurance.PermataVirtualAccountOvershortage
                                            WHERE	CurrencyCode = @0";
            PermataVirtualAccountOvershortage obj = new PermataVirtualAccountOvershortage();
            using (var db = Geta2isBeyondDB())
            {
                obj = db.Query<PermataVirtualAccountOvershortage>(cSQlGetOvershortstage, Curr).FirstOrDefault();
            }
            return obj;
        }

        public bool GetIsPaymentSettled(string orderNo)
        {
            string CurrentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            dynamic O = null;
            using (var db = GetAABDB())
            {
                string CustID = db.ExecuteScalar<string>("SELECT COALESCE(Cust_Id,'') FROM dbo.Mst_Order WHERE Order_No = @0",orderNo);
                string qGetOrder = @"";
                if (string.IsNullOrEmpty(CustID))
                {
                    qGetOrder = @";
SELECT mo.Order_No, mo.VANumber, mo.EntryDt, SUM(odc.Gross_Premium) as GrossPremium, SUM(odc.Gross_Premium) - SUM(odc.Net_Premium) as Discount, 
SUM(odc.Net_Premium) as Net_Premium, mo.Policy_Fee + mo.Stamp_Duty as AdminFee, sum(net_premium) + mo.Policy_Fee + mo.Stamp_Duty as TotalPremium,
Prospect_Type, Policy_No, Biz_type
FROM mst_order mo WITH(NOLOCK)
INNER JOIN ord_Dtl_Coverage odc WITH(NOLOCK) on mo.Order_no = odc.Order_No  and mo.order_no = @0
LEFT JOIN dbo.Mst_Prospect mp on mp.Prospect_Id = mo.Prospect_Id
LEFT JOIN dbo.Mst_Product mpd on mpd.Product_Code = mo.Product_Code
GROUP BY mo.Order_No, mo.Policy_Fee, mo.Stamp_Duty, mo.VANumber, mo.EntryDt, Prospect_Type, Policy_No, Biz_type";
                }
                else {
                    qGetOrder = @";
SELECT mo.Order_No, mo.VANumber, mo.EntryDt, SUM(odc.Gross_Premium) as GrossPremium, SUM(odc.Gross_Premium) - SUM(odc.Net_Premium) as Discount, 
SUM(odc.Net_Premium) as Net_Premium, mo.Policy_Fee + mo.Stamp_Duty as AdminFee, sum(net_premium) + mo.Policy_Fee + mo.Stamp_Duty as TotalPremium,
Cust_Type Prospect_Type, Policy_No, Biz_type
FROM mst_order mo WITH(NOLOCK)
INNER JOIN ord_Dtl_Coverage odc WITH(NOLOCK) on mo.Order_no = odc.Order_No  and mo.order_no = @0
LEFT JOIN dbo.Mst_Customer mp on mp.Cust_Id = mo.Cust_Id
LEFT JOIN dbo.Mst_Product mpd on mpd.Product_Code = mo.Product_Code
GROUP BY mo.Order_No, mo.Policy_Fee, mo.Stamp_Duty, mo.VANumber, mo.EntryDt, Cust_Type, Policy_No, Biz_type";
                }
                O = db.FirstOrDefault<dynamic>(qGetOrder, orderNo);
            }
            if (O != null) {
                string qGetIsPayerCompany = @"SELECT CAST(COALESCE(IsPayerCompany,0) AS BIT) IsPayerCompany FROM dbo.ProspectCustomer pc 
INNER JOIN dbo.OrderSimulation os ON os.CustID = pc.CustID
WHERE PolicyOrderNo = @0 AND os.RowStatus = 1 AND os.ApplyF = 1";
                var mbldb = GetAABMobileDB();
                List<dynamic> ls = mbldb.Fetch<dynamic>(qGetIsPayerCompany, orderNo);
                bool IsPayerCompany = false;
                if (ls.Count > 0) {
                    IsPayerCompany = ls.First().IsPayerCompany;
                }
                if (!string.IsNullOrEmpty(O.VANumber)&& !string.IsNullOrWhiteSpace(O.VANumber))
                {
                    //OrderPaymentCustom objVA = GetTotalBayarVA(O.VANumber, O.EntryDt, O.Order_No);
                    //List<OrderPaymentCustom> listPayment = new List<OrderPaymentCustom>();
                    //if (objVA != null) { listPayment.Add(objVA); }
                    //decimal totalBayar = listPayment.Sum(xx => xx.TotalBayar);
                    decimal totalBayar = OrdLogic.GetTotalPaidPayment(O.Order_No, true);
                    var overShortage = GetVAOverShortStage("IDR");

                    decimal AsuransiAstraBill = O.TotalPremium;
                    decimal totalDibayar = totalBayar;

                    decimal LebihBayar = 0;
                    decimal KurangBayar = 0;


                    if (totalDibayar > AsuransiAstraBill)
                    {
                        LebihBayar = totalDibayar - AsuransiAstraBill;
                    }

                    else if (totalDibayar < AsuransiAstraBill)
                    {
                        KurangBayar = AsuransiAstraBill - totalDibayar;
                    }

                    if (LebihBayar > overShortage.LowerLimitAmount ||
                        (KurangBayar <= overShortage.LowerLimitAmount && LebihBayar <= overShortage.LowerLimitAmount))
                        return true;
                }
                else 
                {
                    string qGetSettlement = @"SELECT Coalesce(Nominal, 0)-Coalesce(Diff, 0)-Coalesce(PaidAmount, 0) AS OutstandingAmount
                                        FROM Finance.FinanceNotes AS fn WITH (NOLOCK) ---cek settlement nota
                                            INNER JOIN Finance.FinanceNoteInfos fni WITH (NOLOCK) ON fni.FinanceNoteID = fn.FinanceNoteID
                                            INNER JOIN Finance.FinanceNoteAdditional fna WITH (NOLOCK) ON fna.NoteNumber = fn.NoteNumber
                                        WHERE fn.RowStatus = 0
                                            AND fna.RowStatus = 0
                                            AND fni.RowStatus = 0
                                            AND PolicyNo= @0
                                            AND TpJournal = 'PRM'
                                            AND TpSubJournal = 'DRC'";
                    var AABDB = GetAABDB();
                    var ByndDB = Geta2isBeyondDB();
                    List<dynamic> getSettlement = ByndDB.Fetch<dynamic>(qGetSettlement, O.Policy_No);
                    if (getSettlement.Count > 0)
                    {
                        if (Convert.ToDecimal(getSettlement.First().OutstandingAmount) == 0)
                        {
                            return true;
                        }
                    }
                    qGetSettlement = @"SELECT CAST(COALESCE(isVerified,0) AS BIT) isVerified FROM dbo.Remark_Buktibayar WHERE Order_No = @0";
                    getSettlement = AABDB.Fetch<dynamic>(qGetSettlement, orderNo);
                    if (getSettlement.Count > 0) {
                        if (getSettlement.First().isVerified) {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        
        public int UpdateFollowUpToNeedApproval(string orderNo)
        {
            string CurrentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                int res = 0;
                string query = @"SELECT FollowUpStatus,FollowUpInfo,OrderNo FROM FollowUp fu 
                            INNER JOIN OrderSimulation os ON fu.FollowUpNo = os.FollowUpNo and os.ApplyF = 1
                            WHERE os.PolicyOrderNo = @0";
                string qUpdateFollowUpToNeedApproval = @"
UPDATE fu
SET FollowUpInfo = @1, LastUpdatedTime = GETDATE()
FROM FollowUp fu 
INNER JOIN OrderSimulation os ON fu.FollowUpNo = os.FollowUpNo and os.ApplyF = 1
WHERE os.PolicyOrderNo = @0 AND FollowUpInfo <> @1
";
                using (var db = GetAABMobileDB())
                {
                    List<dynamic> ls = db.Fetch<dynamic>(query, orderNo);
                    if (ls.Count > 0)
                    {
                        if (ls.First().FollowUpInfo != Convert.ToInt32(FollowUpInfo.NeedApproval))
                        {
                            db.Execute(qUpdateFollowUpToNeedApproval, orderNo, FollowUpInfo.NeedApproval);
                            OrdLogic.UpdateOrderMobile(ls.First().OrderNo);
                            res++;
                        }
                    }
                    return res;
                }
            }
            catch (Exception e)
            {
                logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.Message));
                return 0;
            }
        }

        public int AddMstOrderMobileApproval(string orderNo, string approvalType, int approvalStatus, string reason, string entryUsr, string roleCodeSalesmanOtosales)
        {
            string qInsert = @"DECLARE @@addHistory as bit, @@approvalStatus as int
SELECT @@addHistory = 1
IF EXISTS (select 1 from mst_order_mobile_approval where Order_No = @0 and ApprovalType = @1)
BEGIN
    IF EXISTS (SELECT 1 FROM Mst_Order_Mobile_Approval WHERE Order_No = @0 and ApprovalType = @1 and ( ApprovalStatus = @4 or ApprovalStatus = @2))
        SELECT @@addHistory = 0

	UPDATE Mst_Order_Mobile_Approval
	SET	UpdateUsr = @3, ApprovalStatus = @2, UpdateDt = GETDATE(), Status = 1, RoleCode = @5, Reason = @6
	WHERE Order_No = @0 and ApprovalType = @1 and ApprovalStatus NOT IN (@2, @4)
END
ELSE
BEGIN
	INSERT INTO Mst_Order_Mobile_Approval(Order_No,ApprovalType,ApprovalStatus,Reason,Status,RoleCode,EntryDt,EntryUsr)
	SELECT @0, @1, @2, @6, 1, @5,GETDATE(), @3
END

IF @@addHistory = 1
    INSERT INTO Mst_Order_Mobile_ApprovalHistory(Order_No,ApprovalType,ApprovalStatus,Reason,Status,UserID,RoleCode,EntryDt,EntryUsr)
    SELECT @0, @1, @2, @6, 1, @3, @5, GETDATE(), @3";
            int result = 0;
            using (var db = GetAABDB())
            {
                result = db.Execute(qInsert, orderNo, approvalType, approvalStatus, entryUsr, ApprovalStatus.APPROVE, roleCodeSalesmanOtosales, reason);
            }
            UpdateFollowUpToNeedApproval(orderNo);
            return result;
        }

        public int DeactivateMstOrderMobileApproval(string orderNo, string approvalType, string entryUsr)
        {
            string qInsert = @"
IF EXISTS ( SELECT 1 FROM Mst_Order_Mobile_Approval WHERE Order_No = @0 and ApprovalType = @1  )
BEGIN
    UPDATE Mst_Order_Mobile_Approval
    SET	UpdateUsr = @2, UpdateDt = GETDATE(), Status = 0
    WHERE Order_No = @0 and ApprovalType = @1
    INSERT INTO Mst_Order_Mobile_ApprovalHistory(Order_No,ApprovalType,ApprovalStatus,Status,UserID,EntryDt,EntryUsr)
    SELECT @0, @1, null, 0, @2, GETDATE(), @2
END
";
            using (var db = GetAABDB())
            {
                string qCheck = @"SELECT Status FROM dbo.Mst_Order_Mobile_Approval WHERE Order_no = @0 AND ApprovalType = @1";
                List<dynamic> ls = db.Fetch<dynamic>(qCheck, orderNo, approvalType);
                if (ls.Count > 0) {
                    if (ls.First().Status) {
                        return db.Execute(qInsert, orderNo, approvalType, entryUsr);
                    }
                }
                return 0;
            }
        }

        public int RejectOrder(string orderNo, string entryUsr)
        {
            string qInsertMstOrderMobile = @"
INSERT INTO Mst_Order_Mobile (Order_No,Approval_Status,Approval_Process,isSO,Approval_Type,Actual_Date,EntryDt,EntryUsr)
SELECT @0, @1, 1, 0, 0, GETDATE(), GETDATE(), @2";
            string qUpdateMstOrder = @"UPDATE Mst_Order SET order_status = @1, UpdateDt = GETDATE(), UpdateUsr = @2 WHERE Order_No = @0"; //TODO : Reject remarks gw ga isi ya? tanya REL / FNU
            string qUpdateFollowUpStatus = @"UPDATE fu
SET FollowUpStatus = @1, LastUpdatedTime = GETDATE()
FROM OrderSimulation os 
INNER JOIN FollowUp fu on os.ApplyF = 1 and fu.FollowUpNo = os.FollowUpNo and os.PolicyOrderNo = @0";

            int result = 0;
            using (var db = GetAABDB())
            {   
                result += db.Execute(qInsertMstOrderMobile, orderNo, MstOrderMobileApprovalStatus.Reject, entryUsr);
                result += db.Execute(qUpdateMstOrder, orderNo, Constants.POLICY_REJECTED, entryUsr);
            }
            using (var db = GetAABMobileDB())
            {
                result += db.Execute(qUpdateFollowUpStatus, orderNo, FollowUpStatus.OrderRejectedStatusCode);
            }
            return result;
        }

        public int AddApprovedPolicyFlagToMstOrderMobile(string orderNo, string entryUsr)
        {
            string qInsertMstOrderMobile = @"
INSERT INTO Mst_Order_Mobile (Order_No,Approval_Status,Approval_Process,isSO,Approval_Type,Actual_Date,EntryDt,EntryUsr)
SELECT @0, @1, 1, 0, 0, GETDATE(), GETDATE(), @2";
            int result = 0;
            using (var db = GetAABDB())
            {
                result = db.Execute(qInsertMstOrderMobile, orderNo, MstOrderMobileApprovalStatus.Approve, entryUsr);
            }
            return result;
        }

        public int UpdateMstOrderTO99(string orderNo, string entryUsr)
        {
            string qUpdateMstOrder = @"UPDATE Mst_Order SET order_status = '99', UpdateDt = GETDATE(), UpdateUsr = @1 WHERE Order_No = @0 and order_status <> '99'"; //TODO : Reject remarks gw ga isi ya? tanya REL / FNU
            int result = 0;
            using (var db = GetAABDB())
            {
                result = db.Execute(qUpdateMstOrder, orderNo, entryUsr);
            }
            return result;
        }
        public int UpdateMstOrderTo50(string orderNo, string entryUsr)
        {
            string qUpdateMstOrder = @"UPDATE Mst_Order SET order_status = '50', UpdateDt = GETDATE(), UpdateUsr = @1 WHERE Order_No = @0 and order_status <> '50'"; //TODO : Reject remarks gw ga isi ya? tanya REL / FNU
            int result = 0;
            using (var db = GetAABDB())
            {
                result = db.Execute(qUpdateMstOrder, orderNo, entryUsr);
            }
            return result;
        }

        public string GetSalesOfficerIDRoleCode(string salesOfficerID)
        {
            string qGetRoleCode = @";
IF LEN(@0) > 3
    SELECT[Role] AS[RoleCode] FROM beyondmoss.aabmobile.dbo.salesofficer where SalesOfficerID = @0 and RowStatus = 1
ELSE
    select b.RoleCode from a2isauthorizationdb.general.userapplicationroles a
    inner join a2isAuthorizationDB.General.Roles b on a.RoleID = b.RoleID and UserID = @0 and a.RowStatus = 0
    where applicationid = 3";
            using (var db = GetAABDB())
            {
                var r = db.Fetch<string>(qGetRoleCode, salesOfficerID);
                if (r != null)
                    return string.Join(",", r);
                else
                    return string.Empty;
            }

        }

        public bool GetIsWtOrderExist(string OrderNo, string UserID)
        {
            //TODO: benerin querynya krna blm tau cara ambil flag 'Polis Terbit sebelum bayar' apakah bs dr table ORD, atau dari ordersimulation otosales? 
            string query = @"SELECT TOP 1 1 FROM dbo.WTOrder WHERE OrderNo = @0";
            var dbMobile = GetAABMobileDB();
            var dbAAB = GetAABDB();
            int res = 0;
            if (dbAAB.Fetch<dynamic>(query, OrderNo).Count == 0)
            {
                query = @"SELECT QuotationNo FROM dbo.OrderSimulation os
                        INNER JOIN dbo.FollowUp f 
                        ON f.FollowUpNo = os.FollowUpNo AND f.CustID = os.CustID
                        WHERE os.PolicyOrderNo = @0 AND os.RowStatus = 1 AND os.ApplyF = 1";
                string QuotationNo = dbMobile.ExecuteScalar<dynamic>(query,OrderNo);
                query = @";EXEC [dbo].[usp_CopyOrderDataToWTOrder] @0, -- varchar(30)
                            @1, -- varchar(50) followup
                            @2 -- varchar(50)";
                dbAAB.Execute(query, OrderNo, QuotationNo, UserID);
                res++;
            }
            return res > 0;
        }

        public void UpdatePeriodMstOrder(string OrderNo, string UpdateUsr, bool isRenew)
        {
            string query = "";
            var AAB = GetAABDB();
            var mbl = GetAABMobileDB();
            bool isProcess = true;
            try
            {
                if (isRenew) {
                    isProcess = AAB.ExecuteScalar<bool>(@";
DECLARE @@SurveyDate DATETIME
DECLARE @@OrderDate DATETIME
SELECT @@SurveyDate=Survey_Date FROM dbo.Survey where Reference_No = @0
SELECT @@OrderDate=Period_From FROM dbo.Mst_Order where Order_No = @0
IF(@@SurveyDate>@@OrderDate)
BEGIN
	SELECT CAST(1 AS BIT)
END
ELSE
BEGIN
	SELECT CAST(0 AS BIT)
END", OrderNo);
                }
                if (isProcess)
                {
                    query = @"SELECT Status,Survey_Date FROM dbo.Survey where Reference_No = @0";
                    List<dynamic> surveys = AAB.Fetch<dynamic>(query, OrderNo);
                    if (surveys.Count > 0)
                    {
                        int status = Convert.ToInt32(surveys.First().Status);
                        if (status == 7)
                        {
                            string ordNo = mbl.ExecuteScalar<dynamic>("SELECT OrderNo FROM AABMobile.dbo.OrderSimulation WHERE PolicyOrderNo = @0", OrderNo);
                            mbl.Execute("UPDATE dbo.OrderSimulationSurvey SET SurveyDate = @1 WHERE OrderNo = @0", ordNo, surveys.First().Survey_Date);
                            query = @";EXEC dbo.usp_Shift_Period_Mst_Order @0, @1";
                            AAB.Execute(query, OrderNo, surveys.First().Survey_Date);
                            query = @";EXEC dbo.usp_ShiftPeriodOtosalesOrder @0, @1";
                            mbl.Execute(query, ordNo, surveys.First().Survey_Date);
                            #region old
                            //List<dynamic> ListOsCvg = AAB.Fetch<dynamic>(@"SELECT ObjectNo,InterestNo,CoverageNo,CoverageID FROM BEYONDMOSS.AABMobile.dbo.OrderSimulationCoverage osc
                            //                                                           INNER JOIN BEYONDMOSS.AABMobile.dbo.OrderSimulation os 
                            //                                                           ON os.OrderNo = osc.OrderNo
                            //                                                           WHERE os.PolicyOrderNo = @0", OrderNo);
                            //foreach (dynamic oscvg in ListOsCvg)
                            //{
                            //    query = @";DECLARE @@Days INT
                            //                           SELECT @@Days = DATEDIFF(DAY,Period_From,@1) FROM BEYONDREPORT.AAB.dbo.Mst_Order WHERE Order_No = @0
                            //                           UPDATE dbo.OrderSimulationCoverage SET BeginDate = DATEADD(DAY,@@Days,BeginDate),
                            //                           EndDate = DATEADD(DAY,@@Days,EndDate) WHERE OrderNo = @2 AND ObjectNo = @3 AND InterestNo = @4 AND CoverageNo = @5 AND CoverageID = @6";
                            //    mbl.Execute(query, OrderNo, surveys.First().Survey_Date, ordNo, oscvg.ObjectNo, oscvg.InterestNo, oscvg.CoverageNo, oscvg.CoverageID);
                            //}
                            //List<dynamic> ListOsInt = AAB.Fetch<dynamic>(@"SELECT ObjectNo,InterestNo,InterestID FROM BEYONDMOSS.AABMobile.dbo.OrderSimulationInterest osi 
                            //                                                           INNER JOIN BEYONDMOSS.AABMobile.dbo.OrderSimulation os 
                            //                                                           ON os.OrderNo = osi.OrderNo
                            //                                                           WHERE os.PolicyOrderNo = @0", OrderNo);
                            //foreach (dynamic osint in ListOsInt)
                            //{
                            //    query = @";DECLARE @@Days INT
                            //                           SELECT @@Days = DATEDIFF(DAY,Period_From,@1) FROM BEYONDREPORT.AAB.dbo.Mst_Order WHERE Order_No = @0
                            //                           UPDATE dbo.OrderSimulationInterest SET PeriodFrom = DATEADD(DAY,@@Days,PeriodFrom),
                            //                           PeriodTo = DATEADD(DAY,@@Days,PeriodTo) WHERE OrderNo = @2 AND ObjectNo = @3 AND InterestNo = @4 AND InterestID = @5";
                            //    mbl.Execute(query, OrderNo, surveys.First().Survey_Date, ordNo, osint.ObjectNo, osint.InterestNo, osint.InterestID);
                            //}
                            //query = @";DECLARE @@Days INT
                            //                       SELECT @@Days = DATEDIFF(DAY,Period_From,@1) FROM dbo.Mst_Order WHERE Order_No = @0
                            //                       UPDATE BEYONDMOSS.AABMobile.dbo.OrderSimulation SET PeriodFrom = DATEADD(DAY,@@Days,PeriodFrom),
                            //                       PeriodTo = DATEADD(DAY,@@Days,PeriodTo) WHERE PolicyOrderNo = @0";
                            //AAB.Execute(query, OrderNo, surveys.First().Survey_Date);
                            //List<dynamic> ListObject = AAB.Fetch<dynamic>("SELECT Order_No, Object_No FROM dbo.Ord_Dtl_Object where Order_No = @0", OrderNo);
                            //foreach (dynamic obj in ListObject)
                            //{
                            //    query = @";DECLARE @@Days INT
                            //                           SELECT @@Days = DATEDIFF(DAY,Period_From,@1) FROM dbo.Mst_Order WHERE Order_No = @0
                            //                           UPDATE dbo.Ord_Dtl_Object
                            //                           set Period_From = DATEADD(DAY,@@Days,Period_From), Period_To = DATEADD(DAY,@@Days,Period_To)
                            //                           where Order_No = @0 AND Object_No = @2";
                            //    AAB.Execute(query, OrderNo, surveys.First().Survey_Date, obj.Object_No);
                            //}
                            //List<dynamic> ListInsured = AAB.Fetch<dynamic>("SELECT Object_No, Interest_No, Period_No FROM dbo.Ord_Dtl_Insured where Order_No = @0", OrderNo);
                            //foreach (dynamic ins in ListInsured)
                            //{
                            //    query = @";DECLARE @@Days INT
                            //                           SELECT @@Days = DATEDIFF(DAY,Period_From,@1) FROM dbo.Mst_Order WHERE Order_No = @0
                            //                           UPDATE dbo.Ord_Dtl_Insured
                            //                           set Begin_Date = DATEADD(DAY,@@Days,Begin_Date), End_Date = DATEADD(DAY,@@Days,End_Date)
                            //                           where Order_No = @0 AND Object_No = @2 AND Interest_No = @3 AND Period_No = @4";
                            //    AAB.Execute(query, OrderNo, surveys.First().Survey_Date, ins.Object_No, ins.Interest_No, ins.Period_No);
                            //}
                            //List<dynamic> ListCoverage = AAB.Fetch<dynamic>("SELECT Interest_No,Coverage_No FROM dbo.Ord_Dtl_Coverage WHERE Order_no = @0", OrderNo);
                            //foreach (dynamic cvg in ListCoverage)
                            //{
                            //    query = @";DECLARE @@Days INT
                            //                           SELECT @@Days = DATEDIFF(DAY,Period_From,@1) FROM dbo.Mst_Order WHERE Order_no = @0
                            //                           UPDATE dbo.Ord_Dtl_Coverage
                            //                           set Begin_Date = DATEADD(DAY,@@Days,Begin_Date), End_Date = DATEADD(DAY,@@Days,End_Date)
                            //                           where Order_No = @0 AND Interest_No = @2 AND Coverage_No = @3";
                            //    AAB.Execute(query, OrderNo, surveys.First().Survey_Date, cvg.Interest_No, cvg.Coverage_No);
                            //}
                            //List<dynamic> ListInterestNo = AAB.Fetch<dynamic>("SELECT Interest_No FROM dbo.Ord_Dtl_Interest WHERE Order_no = @0", OrderNo);
                            //foreach (dynamic no in ListInterestNo)
                            //{
                            //    query = @";DECLARE @@Days INT
                            //            SELECT @@Days = DATEDIFF(DAY,Period_From,@1) FROM dbo.Mst_Order WHERE Order_no = @0
                            //            UPDATE dbo.Ord_Dtl_Interest 
                            //            set Period_From = DATEADD(DAY,@@Days,Period_From), Period_To = DATEADD(DAY,@@Days,Period_To)
                            //            where Order_No = @0 AND Interest_No = @2";
                            //    AAB.Execute(query, OrderNo, surveys.First().Survey_Date, no.Interest_No);
                            //}
                            //query = @";DECLARE @@Days INT
                            //        SELECT @@Days = DATEDIFF(DAY,Period_From,@1) FROM dbo.Mst_Order WHERE Order_no = @0
                            //        UPDATE dbo.Mst_Order 
                            //        set Period_From = DATEADD(DAY,@@Days,Period_From), Period_To = DATEADD(DAY,@@Days,Period_To),
                            //        Effective_Date = DATEADD(DAY,@@Days,Effective_Date)
                            //        where Order_No = @0";
                            //AAB.Execute(query, OrderNo, surveys.First().Survey_Date);
                            #endregion
                            #region new
                            //bool loop = true;
                            //int itr = 0;
                            //int DateDiff = 0;
                            //int level = 0;
                            //while (loop) {
                            //    try
                            //    {
                            //        DateDiff = AAB.ExecuteScalar<int>(@"SELECT DATEDIFF(DAY,Period_From,@1) FROM dbo.Mst_Order WHERE Order_No = @0", OrderNo, surveys.First().Survey_Date);
                            //        level = UpdatePeriod(OrderNo, surveys.First().Survey_Date, DateDiff, 0);
                            //        loop = false;

                            //    }
                            //    catch (Exception e)
                            //    {
                            //        if (DateDiff != 0) {
                            //            UpdatePeriod(OrderNo, surveys.First().Survey_Date, -DateDiff, level);
                            //        }
                            //        itr++;
                            //        if (itr > 3) {
                            //            loop = false;
                            //        }
                            //    }
                            //}
                            #endregion
                        }
                    }
                }
            }
            catch (Exception e)
            {
                //throw e;
            }
        }

        public int UpdatePeriod(string OrderNo, dynamic SurveyDate, int DateDiff, int level) {
            try
            {
                int res = 0;
                string query = @"";
                var AAB = GetAABDB();
                var mbl = GetAABMobileDB();
                string ordNo = "";
                if (level == 0 || level >= res) {
                    ordNo = AAB.ExecuteScalar<dynamic>("SELECT OrderNo FROM BEYONDMOSS.AABMobile.dbo.OrderSimulation WHERE PolicyOrderNo = @0", OrderNo);
                    mbl.Execute("UPDATE dbo.OrderSimulationSurvey SET SurveyDate = @1 WHERE OrderNo = @0", ordNo, SurveyDate);
                }
                res++;
                if (level == 0 || level >= res)
                {
                    List<dynamic> ListOsCvg = AAB.Fetch<dynamic>(@"SELECT ObjectNo,InterestNo,CoverageNo,CoverageID FROM BEYONDMOSS.AABMobile.dbo.OrderSimulationCoverage osc
                                                                    INNER JOIN BEYONDMOSS.AABMobile.dbo.OrderSimulation os 
                                                                    ON os.OrderNo = osc.OrderNo
                                                                    WHERE os.PolicyOrderNo = @0", OrderNo);
                    foreach (dynamic oscvg in ListOsCvg)
                    {
                        query = @";DECLARE @@Days INT = @1
                                    UPDATE dbo.OrderSimulationCoverage SET BeginDate = DATEADD(DAY,@@Days,BeginDate),
                                    EndDate = DATEADD(DAY,@@Days,EndDate) WHERE OrderNo = @2 AND ObjectNo = @3 AND InterestNo = @4 AND CoverageNo = @5 AND CoverageID = @6";
                        mbl.Execute(query, OrderNo, DateDiff, ordNo, oscvg.ObjectNo, oscvg.InterestNo, oscvg.CoverageNo, oscvg.CoverageID);
                    }
                }
                res++;
                if (level == 0 || level >= res)
                {
                    List<dynamic> ListOsInt = AAB.Fetch<dynamic>(@"SELECT ObjectNo,InterestNo,InterestID FROM BEYONDMOSS.AABMobile.dbo.OrderSimulationInterest osi 
                                                                    INNER JOIN BEYONDMOSS.AABMobile.dbo.OrderSimulation os 
                                                                    ON os.OrderNo = osi.OrderNo
                                                                    WHERE os.PolicyOrderNo = @0", OrderNo);
                    foreach (dynamic osint in ListOsInt)
                    {
                        query = @";DECLARE @@Days INT = @1
                                    UPDATE dbo.OrderSimulationInterest SET PeriodFrom = DATEADD(DAY,@@Days,PeriodFrom),
                                    PeriodTo = DATEADD(DAY,@@Days,PeriodTo) WHERE OrderNo = @2 AND ObjectNo = @3 AND InterestNo = @4 AND InterestID = @5";
                        mbl.Execute(query, OrderNo, DateDiff, ordNo, osint.ObjectNo, osint.InterestNo, osint.InterestID);
                    }
                }
                res++;
                if (level == 0 || level >= res)
                {
                    query = @";DECLARE @@Days INT = @1
                                UPDATE BEYONDMOSS.AABMobile.dbo.OrderSimulation SET PeriodFrom = DATEADD(DAY,@@Days,PeriodFrom),
                                PeriodTo = DATEADD(DAY,@@Days,PeriodTo) WHERE PolicyOrderNo = @0";
                    AAB.Execute(query, OrderNo, DateDiff);
                }
                res++;
                if (level == 0 || level >= res)
                {
                    List<dynamic> ListObject = AAB.Fetch<dynamic>("SELECT Order_No, Object_No FROM dbo.Ord_Dtl_Object where Order_No = @0", OrderNo);
                    foreach (dynamic obj in ListObject)
                    {
                        query = @";DECLARE @@Days INT = @1
                                    SELECT @@Days = DATEDIFF(DAY,Period_From,@1) FROM dbo.Mst_Order WHERE Order_No = @0
                                    UPDATE dbo.Ord_Dtl_Object
                                    set Period_From = DATEADD(DAY,@@Days,Period_From), Period_To = DATEADD(DAY,@@Days,Period_To)
                                    where Order_No = @0 AND Object_No = @2";
                        AAB.Execute(query, OrderNo, DateDiff, obj.Object_No);
                    }
                }
                res++;
                if (level == 0 || level >= res)
                {
                    List<dynamic> ListInsured = AAB.Fetch<dynamic>("SELECT Object_No, Interest_No, Period_No FROM dbo.Ord_Dtl_Insured where Order_No = @0", OrderNo);
                    foreach (dynamic ins in ListInsured)
                    {
                        query = @";DECLARE @@Days INT = @1
                                    UPDATE dbo.Ord_Dtl_Insured
                                    set Begin_Date = DATEADD(DAY,@@Days,Begin_Date), End_Date = DATEADD(DAY,@@Days,End_Date)
                                    where Order_No = @0 AND Object_No = @2 AND Interest_No = @3 AND Period_No = @4";
                        AAB.Execute(query, OrderNo, DateDiff, ins.Object_No, ins.Interest_No, ins.Period_No);
                    }
                }
                res++;
                if (level == 0 || level >= res)
                {
                    List<dynamic> ListCoverage = AAB.Fetch<dynamic>("SELECT Interest_No,Coverage_No FROM dbo.Ord_Dtl_Coverage WHERE Order_no = @0", OrderNo);
                    foreach (dynamic cvg in ListCoverage)
                    {
                        query = @";DECLARE @@Days INT = @1
                                    UPDATE dbo.Ord_Dtl_Coverage
                                    set Begin_Date = DATEADD(DAY,@@Days,Begin_Date), End_Date = DATEADD(DAY,@@Days,End_Date)
                                    where Order_No = @0 AND Interest_No = @2 AND Coverage_No = @3";
                        AAB.Execute(query, OrderNo, DateDiff, cvg.Interest_No, cvg.Coverage_No);
                    }
                }
                res++;
                if (level == 0 || level >= res)
                {
                    List<dynamic> ListInterestNo = AAB.Fetch<dynamic>("SELECT Interest_No FROM dbo.Ord_Dtl_Interest WHERE Order_no = @0", OrderNo);
                    foreach (dynamic no in ListInterestNo)
                    {
                        query = @";DECLARE @@Days INT = @1
                                    UPDATE dbo.Ord_Dtl_Interest 
                                    set Period_From = DATEADD(DAY,@@Days,Period_From), Period_To = DATEADD(DAY,@@Days,Period_To)
                                    where Order_No = @0 AND Interest_No = @2";
                        AAB.Execute(query, OrderNo, DateDiff, no.Interest_No);
                    }
                }
                res++;
                if (level == 0 || level >= res)
                {
                    query = @";DECLARE @@Days INT = @1
                                    UPDATE dbo.Mst_Order 
                                    set Period_From = DATEADD(DAY,@@Days,Period_From), Period_To = DATEADD(DAY,@@Days,Period_To),
									Effective_Date = DATEADD(DAY,@@Days,Effective_Date)
                                    where Order_No = @0";
                    AAB.Execute(query, OrderNo, DateDiff);
                }
                res++;
                return res;
            }
            catch (Exception e)
            {
                if (DateDiff != 0)
                {
                    UpdatePeriod(OrderNo, SurveyDate, -DateDiff, level);
                }
                return 0;
                //throw e;
            }
        }

        public List<dynamic> GetOrderWithNextLimitApproval()
        {
            string[] parts = CommonLogic.GetSettingExcludeOrderStatusNextLimit().Split(',');
            //Tanya JSW, mst_oder_mobile.approval_status :  0 : ?, 1 : approved, 2 : ? 
            string qGet = @"SELECT DISTINCT mo.Order_No, ms.User_Id_Otosales SalesOfficerID, mo.Order_Type FROM dbo.Mst_Order_Mobile_Approval oma WITH (NOLOCK)
INNER JOIN dbo.Mst_Order mo WITH (NOLOCK) ON mo.Order_No = oma.Order_No 
AND mo.Order_Status NOT IN (@1)
AND ApprovalType = @0 AND ApprovalStatus = 1
LEFT JOIN dbo.Mst_Salesman ms ON ms.Salesman_Id = mo.Salesman_Id
WHERE ApprovalUsr IS NOT NULL";
            using (var db = GetAABDB())
            {
                return db.Fetch<dynamic>(qGet, ApprovalType.NextLimit, parts);
            }
        }
        public string GetApprovalUser(string OrderNo)
        {
            //Tanya JSW, mst_oder_mobile.approval_status :  0 : ?, 1 : approved, 2 : ? 
            string qGet = @"SELECT COALESCE(ApprovalUsr,'') AS ApprovalUsr 
                            FROM dbo.Mst_Order_Mobile_Approval 
                            WHERE ApprovalType = @1
                            AND Order_No = @0";
            using (var db = GetAABDB())
            {
                string res = "";
                List<dynamic> list = db.Fetch<dynamic>(qGet, OrderNo, ApprovalType.NextLimit);
                if (list.Count > 0)
                {
                    res = list.First().ApprovalUsr;
                }
                return res;
            }
        }

        public string GetPrevStatus(string OrderNo)
        {
            //Tanya JSW, mst_oder_mobile.approval_status :  0 : ?, 1 : approved, 2 : ? 
            string qGet = @"SELECT Order_Status FROM dbo.Mst_Order WHERE Order_No = @0";
            using (var db = GetAABDB())
            {
                string res = "";
                List<dynamic> list = db.Fetch<dynamic>(qGet, OrderNo);
                if (list.Count > 0)
                {
                    res = list.First().Order_Status;
                }
                return res;
            }
        }
        public int RollBackMstOrderStatus(string orderNo, string entryUsr, string PrevStatus)
        {
            string qUpdateMstOrder = @"UPDATE Mst_Order SET order_status = @2, UpdateDt = GETDATE(), UpdateUsr = @1 WHERE Order_No = @0 and order_status <> @2"; //TODO : Reject remarks gw ga isi ya? tanya REL / FNU
            int result = 0;
            using (var db = GetAABDB())
            {
                result = db.Execute(qUpdateMstOrder, orderNo, entryUsr, PrevStatus);
            }
            return result;
        }

        public int UpdateFollowUpToFUPayment(string orderNo)
        {
            string CurrentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                int res = 0;
                string qUpdateFollowUpToFUPayment = @"
UPDATE fu
SET FollowUpInfo = @1, LastUpdatedTime = GETDATE()
FROM FollowUp fu 
INNER JOIN OrderSimulation os ON fu.FollowUpNo = os.FollowUpNo and os.ApplyF = 1
WHERE os.PolicyOrderNo = @0 AND FollowUpInfo <> @1
";
                using (var db = GetAABMobileDB())
                {
                    var mobileDB = GetAABMobileDB();
                    string qOrder = @"SELECT OrderNo,FollowUpInfo FROM dbo.OrderSimulation os 
                                INNER JOIN dbo.FollowUp f
                                ON f.FollowUpNo = os.FollowUpNo
                                WHERE PolicyOrderNo = @0";
                    List<dynamic> ord = mobileDB.Fetch<dynamic>(qOrder, orderNo);
                    if (ord.Count > 0)
                    {
                        if (ord.First().FollowUpInfo != Convert.ToInt32(FollowUpInfo.FUPayment))
                        {
                            db.Execute(qUpdateFollowUpToFUPayment, orderNo, FollowUpInfo.FUPayment);
                            OrdLogic.UpdateOrderMobile(ord.First().OrderNo);
                        }
                        res++;
                    }
                    return res;
                }

            }
            catch (Exception e)
            {
                logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.Message));
                return 0;
            }
        }

        public bool IsTelerenewalUser(string orderNo)
        {
            string qIsTelerewal = @";DECLARE @@SOID VARCHAR(100)
                                    SELECT @@SOID=SalesOfficerID FROM dbo.OrderSimulation WHERE PolicyOrderNo = @0
                                    SELECT Role FROM dbo.SalesOfficer WHERE SalesOfficerID = @@SOID";
            bool rslt = false;
            using (var db = GetAABMobileDB())
            {
                List<dynamic> list = db.Fetch<dynamic>(qIsTelerewal, orderNo);
                if (list.Count > 0) {
                    string role = Convert.ToString(list.First().Role);
                    if (!string.IsNullOrEmpty(role)) {
                        if (role.Trim().ToUpper().Equals("TELERENEWAL")) {
                            rslt = true;
                        }
                    }
                }
            }
            return rslt;
        }

        public List<dynamic> GetOrderApprovalGEN5()
        {
            string qGet = @"SELECT mom1.Order_No, mom1.Approval_Status,	mom1.Approval_Process, CAST(IIF(mo.order_type=1,0,1) AS BIT) [IsRenewal],
							ms.User_Id_Otosales AS SalesOfficerID, mo.Order_Type
	                        FROM (
			                        select Order_No, max(Order_ID) [Order_ID]
			                        from dbo.mst_order_mobile mom WITH ( NOLOCK )
									WHERE mom.Approval_Status = 0 and Approval_Process = 0
			                        group by Order_No
	                        ) a
	                        INNER JOIN dbo.mst_order_mobile mom1 WITH ( NOLOCK ) ON a.Order_ID = mom1.Order_ID
	                        INNER JOIN dbo.mst_order mo WITH ( NOLOCK ) ON mo.order_no = mom1.Order_No and mo.Order_Status = '50'
							INNER JOIN dbo.WTOrder wo WITH ( NOLOCK ) ON wo.orderno = mo.Order_No and wo.RowStatus = 0 and wo.OrderSource = 0
							LEFT JOIN dbo.Mst_Salesman ms WITH ( NOLOCK ) ON ms.Salesman_Id = mo.Salesman_Id AND ms.Status = 1 WHERE mom1.SA_State = 2";
            using (var db = GetAABDB())
            {
                return db.Fetch<dynamic>(qGet, SAState.SubmitBySA);
            }
        }

        public bool IsSkipApprovalNonLimit(string OrderNo) {
            var MobileDB = GetAABMobileDB();
            var AABDB = GetAABDB();
            int res = 0;
            string qGetOrder = @"SELECT COALESCE(SegmentCode,'') SegmentCode, COALESCE(ProductCode,'') ProductCode FROM dbo.OrderSimulation WHERE PolicyOrderNo = @0";
            List<dynamic> ord = MobileDB.Fetch<dynamic>(qGetOrder, OrderNo);
            if (ord.Count > 0) {
                string qIsProkhus = @"SELECT COUNT(*) FROM dbo.Mapping_Product_SOB mpsob
                                    INNER JOIN dbo.Mapping_Segment ms
                                    ON mpsob.SOB_ID = ms.SegmentCode
                                    WHERE SegmentCode = @0 AND 
                                    (SegmentCode2 = 'SG2016' OR SegmentCode2 = 'SG2017')";
                res = AABDB.ExecuteScalar<int>(qIsProkhus, ord.First().SegmentCode);
                if (res == 0) {
                    string qIsLexus = @"SELECT count(*) FROM Partner_Document_Template where Product_Code=@0 AND Partner_Id='LEX'";
                    res = AABDB.ExecuteScalar<int>(qIsLexus, ord.First().ProductCode);
                }
                if (res == 0) {
                    string qIsDutyFree = @";SELECT COUNT(*) FROM dbo.Mst_Product mp
INNER JOIN dbo.Fn_AABSplitString(
(SELECT ParamValue 
FROM BEYONDMOSS.AABMobile.dbo.ApplicationParameters
WHERE ParamName = 'OTOSALES-PRODUCT-DUTY-FREE'),',') ap
ON ap.Data = mp.Product_Code 
WHERE Product_Code = @0 AND mp.Status = 1";
                    res = AABDB.ExecuteScalar<int>(qIsDutyFree, ord.First().ProductCode);
                }
            }
            return res>0;
        }
        public bool GetDataCommission(string orderNo)
        {
            var MobileDB = GetAABMobileDB();
            var AABDB = GetAABDB();
            int res = 0;
            string qGetOrder = @"SELECT ProductCode FROM dbo.OrderSimulation WHERE PolicyOrderNo = @0";
            List<dynamic> ord = MobileDB.Fetch<dynamic>(qGetOrder, orderNo);
            if (ord.Count > 0)
            {
                string qIsProkhus = @"SELECT COUNT(*) FROM dbo.Mst_Product WHERE Product_Type = 6 AND Product_Code = @0";
                res = AABDB.ExecuteScalar<int>(qIsProkhus, ord.First().ProductCode);
            }
            return res > 0;
        }

        public bool IsGen5Order(string OrderNo) {
            var MobileDB = GetAABMobileDB();
            string qGetOrder = @"SELECT * FROM dbo.OrderSimulation WHERE PolicyOrderNo = @0";
            List<dynamic> ls = MobileDB.Fetch<dynamic>(qGetOrder, OrderNo);
            if (ls.Count == 0) {
                return true;
            }
            return false;
        }

        public bool IsPolicyCreated(string OrderNo) {
            var db = GetAABDB();
            string qSelect = @"SELECT Order_Status FROM dbo.Policy where Order_No = @0";
            List<dynamic> list = db.Fetch<dynamic>(qSelect, OrderNo);
            if (list.Count > 0)
            {
                string qUpdate = @"UPDATE dbo.Mst_Order SET Order_Status = @0 WHERE Order_No = @1";
                db.Execute(qUpdate, list.First().Order_Status, OrderNo);
                return true;
            }
            return false;
        }
    }
}
