﻿using a2is.Framework.Monitoring;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Otosales.Approval.Logic;
using Otosales.Approval.Models;
using Otosales.Approval.Repository;
using Otosales.Logic;
using System;
using System.Collections.Generic;
using static Otosales.Models.Constant;

namespace Otosales.Approval
{
    class Program
    {
        private static a2isLogHelper logger = new a2isLogHelper();

        enum OrderType
        {
            NEW,
            RENEW,
            APPROVALNEXTLIMIT,
            GEN5
        }


        static void Main(string[] args)
        {
            /*
             * TODO : 
             * 1. select & from mst_order_mobile where SA_state = 1 (submit by sa) & select * from followup where status = sendtosa
             * - cek butuh approval apa ga? apa aja yang perlu di bikin approvalnya? insert ke mst_order_mobile_approval
             * - bikin push notification untuk yang PIC yang harus approve (atau mgkin perlu dikirim email karena push notif bisa jadi ga kebentuk kalo ga pernah login atau ga punya fcmid)
             * 2. setelah jalan proses pengecekan approval, update status order jadi 99 atas orderno itu jalanin approval polis.
             * 3. setelah approve dan jadi polis, CALL API untuk trigger transfer commission same day (jika ada).
             * - pastiin aja insert ke mst_order_mobile, approval_status = 1 (1 : approve, 2 : reject), approval_process = 1
             * 
             * KALO APPROVAL SEMUA DIREJECT apa yang bisa dilakukan ? apakah order_status gw ganti jadi cancel?
             * - jika order reject, approval statusnya : 2 nanti scheduler mobileapproval akan update order_statusnya jadi 6.
             * 
             * 
             * REVIEW ULANG SEMUA CODINGAN YANG JOIN KE MST_ORDER apakah harus ada pengecekan and " mo.Status = 'A' AND mo.Order_Status not in ('9', '10', '11', '6') "
             */
            //ProcessApproval(OrderType.NEW);
            //ProcessApproval(OrderType.RENEW);
            //ProcessApproval(OrderType.APPROVALNEXTLIMIT);
            //ProcessApproval(OrderType.GEN5);
            ProcessApproval(GetOrderType(CommonLogic.GetSettingOrderType()));
        }


        static void ProcessApproval(OrderType orderType)
        {
            string CurrentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                string autoApprovalOrderUrl = CommonLogic.GetSettingAutoApprovalOrderURL();
                logger.Debug(string.Format("Function {0} with orderType {1} BEGIN:'", CurrentMethod, orderType == OrderType.NEW ? 1 : 2));
                string UserID = CommonLogic.GetSettingUserID();
                OtosalesOrderRepository OOR = new OtosalesOrderRepository();
                List<dynamic> items = null;
                switch (orderType)
                {
                    case OrderType.NEW:
                        items = OOR.GetFollowUpSendToSAorNeedApprovalAndSubmitBySAStatus(); 
                        break;
                    case OrderType.RENEW:
                        items = OOR.GetFollowUpWithFUPembayaran(); 
                        break;
                    case OrderType.APPROVALNEXTLIMIT:
                        items = OOR.GetOrderWithNextLimitApproval(); 
                        break;
                    case OrderType.GEN5:
                        items = OOR.GetOrderApprovalGEN5(); 
                        break;
                }
                foreach (var o in items)
                {
                    try
                    {
                        string orderNo = o.Order_No;
                        string salesOfficerID = o.SalesOfficerID;
                        string roleCode = OOR.GetSalesOfficerIDRoleCode(salesOfficerID);
                        bool IsProkhusOrLexus = OOR.IsSkipApprovalNonLimit(orderNo);
                        bool IsGen5Order = OOR.IsGen5Order(orderNo);
                        OrderType newOrdtype = orderType;
                        if (!IsProkhusOrLexus)
                        {
                            if (orderType == OrderType.GEN5)
                            {
                                if (!IsGen5Order)
                                {
                                    continue;
                                }
                                newOrdtype = o.IsRenewal ? OrderType.RENEW : OrderType.NEW;
                            }
                            else
                            {
                                newOrdtype = orderType;
                            }

                            var comm = OOR.GetDataCommissionSameDay(orderNo); 
                            if (comm != null)
                            {
                                OOR.AddMstOrderMobileApproval(orderNo, ApprovalType.CommissionSameDay, ApprovalStatus.WAITING, null, UserID, roleCode);
                                CommonLogic.CreatePushNotification(Otosales.Models.PushNotificationCategory.NEEDAPPROVALCOMMISSIONSAMEDAY, orderNo);
                            }
                            else
                                OOR.DeactivateMstOrderMobileApproval(orderNo, ApprovalType.CommissionSameDay, UserID);

                            var komisi = OOR.GetDataCommission(orderNo); 
                            if (komisi)
                            {
                                OOR.AddMstOrderMobileApproval(orderNo, ApprovalType.Commission, ApprovalStatus.WAITING, null, UserID, roleCode);
                                CommonLogic.CreatePushNotification(Otosales.Models.PushNotificationCategory.NEEDAPPROVALADJUSTMENT, orderNo); //Pembentukan PushNotif Sama dengan Adjustment
                            }
                            else
                                OOR.DeactivateMstOrderMobileApproval(orderNo, ApprovalType.Commission, UserID);

                            List<string> listApprovalAdjustmentReason = new List<string>();
                            var pdelivr = OOR.GetDataPolicyDeliveryAddressNotCustomer(orderNo); 
                            if (pdelivr != null)
                                listApprovalAdjustmentReason.Add(ApprovalAdjustmentReason.PolicyDeliveryAddress);
                            if (newOrdtype == OrderType.RENEW)
                            {
                                var policyCreatedBeforePayment = OOR.GetDataPolicyCreatedBeforePayment(orderNo); 
                                if (policyCreatedBeforePayment != null)
                                    listApprovalAdjustmentReason.Add(ApprovalAdjustmentReason.PolicyTerbitSebelumBayar);
                                if (!OOR.IsTelerenewalUser(orderNo))
                                {
                                    var salesmanDealerChanged = OOR.GetDataSalesmanDealerChanged(orderNo); 
                                    if (salesmanDealerChanged != null)
                                        listApprovalAdjustmentReason.Add(ApprovalAdjustmentReason.PerubahanDataSalesman);
                                }
                            }

                            if (listApprovalAdjustmentReason.Count > 0)
                            {
                                OOR.AddMstOrderMobileApproval(orderNo, ApprovalType.Adjustment, ApprovalStatus.WAITING, string.Join(",", listApprovalAdjustmentReason), UserID, roleCode);
                                CommonLogic.CreatePushNotification(Otosales.Models.PushNotificationCategory.NEEDAPPROVALADJUSTMENT, orderNo);
                            }
                            else
                                OOR.DeactivateMstOrderMobileApproval(orderNo, ApprovalType.Adjustment, UserID);
                        }

                        bool policyApproved = false;
                        var approvalState = OOR.GetMstOrderMobileApprovalState(orderNo);
                        if (approvalState == MstOrderMobileApprovalState.APPROVED || IsProkhusOrLexus)
                        {
                            bool isPTSB = false;
                            bool isPaymentSettled = false;
                            string prevStatus = OOR.GetPrevStatus(orderNo);
                            if (newOrdtype == OrderType.RENEW) isPTSB = OOR.GetIsPolicyCreatedBeforePayment(orderNo);
                            if (newOrdtype == OrderType.RENEW)
                            {
                                isPaymentSettled = OOR.GetIsPaymentSettled(orderNo);
                                if (!isPaymentSettled && !isPTSB)
                                    OOR.UpdateFollowUpToFUPayment(orderNo);
                            }
                            if (newOrdtype == OrderType.NEW || (newOrdtype == OrderType.RENEW && (isPTSB || isPaymentSettled)) || newOrdtype == OrderType.APPROVALNEXTLIMIT)
                            {
                                //Check wt if not exist [dbo].[usp_CopyOrderDataToWTOrder] 
                                OOR.GetIsWtOrderExist(orderNo, UserID);
                                //update dlo mst_order.order_status = 99 before call AutoApproveOrder.
                                if (newOrdtype != OrderType.APPROVALNEXTLIMIT)
                                {
                                    OOR.UpdateMstOrderTO99(orderNo, UserID);
                                }

                                if (!IsGen5Order)
                                {
                                    bool isRenew = false;
                                    if (Convert.ToInt32(o.Order_Type) == 2) {
                                        isRenew = true;
                                    }
                                    OOR.UpdatePeriodMstOrder(orderNo, UserID, isRenew);
                                }
                                // do auto approval limit (call api).
                                try
                                {
                                    if (!OOR.IsPolicyCreated(orderNo))
                                    {
                                        string ApprovalUser = OOR.GetApprovalUser(orderNo);
                                        string body = string.Format("pOrderNo={0}&pIsNeedNSAApproval={1}&pTriggerApps={2}&pApprovalUser={3}", o.Order_No, "0", "otosales", ApprovalUser);// TODO : confirm ke jsw / fnu / rel ini gw hardcode 0 gpp?. pEntryUsr ini tambahan blo baru, info ke dev core (fnu) biar bisa di support.
                                        string json = NetworkLogic.POST(autoApprovalOrderUrl, body, NetworkLogic.GetToken());
                                        JToken jToken = JToken.Parse(json);
                                        if (jToken != null && !string.IsNullOrEmpty(jToken["status"].ToString()) && Convert.ToString(jToken["status"]).ToLower().Equals("true"))
                                        {
                                            var r = JsonConvert.DeserializeObject<AutoApproveOrderResult>(jToken["data"].ToString());
                                            if (r != null && Convert.ToString(r.result).ToLower().Equals("true"))
                                            {
                                                if (r.ResultStatus == AutoApproveOrderResultStatus.PolicyApproved)
                                                {
                                                    policyApproved = true;
                                                    // selesai tinggal insert di mst_order_mobile status approvenya
                                                    OOR.AddApprovedPolicyFlagToMstOrderMobile(orderNo, UserID);
                                                    // sama mestinya update followupstatus polisnya jadi PolicyCreated, tapi dibikin di Otosales.MonitorOrder aja biar disana handle push notif sekalian

                                                }
                                                else if (r.ResultStatus == AutoApproveOrderResultStatus.WaitingApprovalLimit)
                                                {
                                                    // insert ke mst_order_mobile_approval
                                                    OOR.AddMstOrderMobileApproval(orderNo, ApprovalType.NextLimit, ApprovalStatus.WAITING, r.OrderApprovalUser, UserID, roleCode);
                                                    CommonLogic.CreatePushNotification(Otosales.Models.PushNotificationCategory.NEEDAPPROVALNEXTLIMIT, orderNo);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            logger.Error(string.Format("Error occured when call api AutoApprovalOrderUrl : {0} , body : {1} ", autoApprovalOrderUrl, body));
                                            OOR.RollBackMstOrderStatus(orderNo, UserID, prevStatus);
                                        }
                                    }
                                }
                                catch (Exception e)
                                {
                                    OOR.RollBackMstOrderStatus(orderNo, UserID, prevStatus);
                                }
                            }
                        }
                        else if (approvalState == MstOrderMobileApprovalState.REJECTED)
                        {
                            OOR.RejectOrder(orderNo, UserID);
                        }

                        //jika comm || pdelivr ga kosong : ganti status fu jadi need approval
                        if ((!policyApproved && approvalState != MstOrderMobileApprovalState.REJECTED && approvalState != MstOrderMobileApprovalState.APPROVED) || approvalState == MstOrderMobileApprovalState.INPROGRESS)
                        {
                            OOR.UpdateFollowUpToNeedApproval(orderNo);
                        }
                    }
                    catch (Exception e)
                    {
                        logger.Error(string.Format("Function {0} + Error : {1},  Message: {2}", CurrentMethod, e.ToString(), e.StackTrace));
                    }
                }
                logger.Debug(string.Format("Function {0} END:'", CurrentMethod));
            }
            catch (Exception e)
            {
                logger.Error(string.Format("Function {0} + Error : {1},  Message: {2},  Trace: {2}", CurrentMethod, e.ToString(), e.StackTrace));
            }
        }

        static OrderType GetOrderType(string OrdType) {
            OrderType res = new OrderType();
            if (OrdType.ToUpper().Equals("NEW")) {
                res = OrderType.NEW;
            }
            else if (OrdType.ToUpper().Equals("RENEW")) {
                res = OrderType.RENEW;
            }
            else if (OrdType.ToUpper().Equals("APPROVALNEXTLIMIT"))
            {
                res = OrderType.APPROVALNEXTLIMIT;
            }
            else if (OrdType.ToUpper().Equals("GEN5"))
            {
                res = OrderType.GEN5;
            }
            return res;
        }
    }
}
