﻿using System;

namespace Otosales.Approval.Models
{
    public class AutoApproveOrderResult
    {
        public bool result { get; set; }
        public string OrderNo { get; set; }
        public string PolicyNo { get; set; }
        public int ResultStatus { get; set; }
        public string RealizeUser { get; set; }
        public string OrderApprovalUser { get; set; }
        public string ResultMessage { get; set; }
    }

    public class OrderPaymentCustom
    {
        public decimal TotalBayar { get; set; }
        public DateTime MaxTransactionDate { get; set; }
        public decimal TotalBill { get; set; }
    }
    public class PermataVirtualAccountOvershortage
    {
        public int PermataVirtualAccountOvershortageID { get; set; }
        public string CurrencyCode { get; set; }
        public decimal LowerLimitAmount { get; set; }
        public decimal UpperLimitAmount { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
    }

}
