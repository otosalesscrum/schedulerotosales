﻿using Newtonsoft.Json.Linq;
using Otosales.Approval.Properties;
using Otosales.Approval.Repository;
using Otosales.Logic;
using System;
using System.Net.Http;
using System.Text;

namespace Otosales.Approval.Logic
{
    public static class NetworkLogic
    {
        public static string GET(string url)
        {
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("","");
                var response = client.GetAsync(url).Result;
                return response.Content.ReadAsStringAsync().Result;
            }
        }

        public static string POST(string url, string body, string token = null, bool retry = true)
        {
            using (HttpClient client = new HttpClient())
            {
                if (!string.IsNullOrEmpty(token))
                    client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
                DateTime requestDate = DateTime.Now;
                string requestMessage = LoggingRepository.GetRequestMessage(HttpMethod.Post, url, client.DefaultRequestHeaders.Authorization, body);
                var response = client.PostAsync(url, new StringContent(body, Encoding.UTF8, "application/x-www-form-urlencoded")).Result;
                DateTime responseDate = DateTime.Now;
                string rs = null;
                if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized && retry) {
                    ResetToken();
                    return POST(url, body, GetToken(), false);
                }
                else
                    rs = response.Content.ReadAsStringAsync().Result;
                
                string responseMessage = LoggingRepository.GetResponseMessage(response.StatusCode, url, rs);
                LoggingRepository.Info(requestDate, requestMessage, responseDate, responseMessage);
                return rs;


            }
        }
        
        private static string _token;
        private static DateTime _tokenExpired;

        public static void ResetToken()
        {
            _token = null;
            _tokenExpired = DateTime.MinValue;
            Settings.Default.TempData = null;
            Settings.Default.Save();
        }
        public static string GetToken()
        {
            if (!string.IsNullOrEmpty(_token) && _tokenExpired != DateTime.MinValue && _tokenExpired > DateTime.Now)
                return _token;

            string json = Settings.Default.TempData;
            if (!string.IsNullOrEmpty(json))
            {
                try
                {
                    JToken jToken = JToken.Parse(json);
                    int validHours = int.Parse(jToken["validHours"].ToString());
                    DateTime entryDate = DateTime.Parse(jToken["entryDate"].ToString());
                    string token = jToken["token"].ToString();
                    if (DateTime.Now < entryDate.AddHours(validHours-1))
                        return token;
                }
                catch (Exception){ }
            }

            string url = CommonLogic.GetSettingLoginURL();
            string[] account = CommonLogic.GetSettingLoginAcc().Split(':');
            string body = string.Format("username={0}&password={1}&applicationCode=1", account[0], account[1]);
            using (HttpClient client = new HttpClient())
            {
                DateTime requestDate = DateTime.Now;
                string requestMessage = LoggingRepository.GetRequestMessage(HttpMethod.Post, url, client.DefaultRequestHeaders.Authorization, body);
                var response = client.PostAsync(url, new StringContent(body, Encoding.UTF8, "application/x-www-form-urlencoded")).Result;
                DateTime responseDate = DateTime.Now;
                string result = string.Empty;
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    result = response.Content.ReadAsStringAsync().Result;
                    JToken jToken = JToken.Parse(result);
                    if (Convert.ToString(jToken["status"]).ToLower().Equals("true"))
                    {
                        jToken["entryDate"] = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
                        Settings.Default.TempData = jToken.ToString();
                        Settings.Default.Save();
                        return jToken["token"].ToString();
                    }
                }
                string responseMessage = LoggingRepository.GetResponseMessage(response.StatusCode, url, result);
                LoggingRepository.Info(requestDate, requestMessage, responseDate, responseMessage);
                return null;
            }
            
        }

    }
}
